#include "filter.h"
#include "types.h"
#include <math.h>
const float Pi = 3.1415926535897932385;
const float sqrt2 = 1.4142135623730950488;
struct filter_struct_butterworth{
	u8 in_use;
	float gain;
	float coeff_ax[3];
	float coeff_by[3];
	float xv[3];
	float yv[3];
};
struct filter_struct lc1_fltr = {0,0},
	lc2_fltr = {0,0},
	gyro1_fltr = {0,0},
	gyro2_fltr = {0,0};
struct filter_struct_butterworth reserved_bt_struct[8];
void* init_butterworth_filter(const int samplerate, const int cutoff)
{
    u32 i = 0;
	for(; i < sizeof(reserved_bt_struct)/sizeof(struct filter_struct_butterworth); ++i){
		if(reserved_bt_struct[i].in_use == 0){
			float QcRaw = (2 * Pi * cutoff) / samplerate;
			float QcWarp = tanf(QcRaw);
			reserved_bt_struct[i].gain = 1 / (1+sqrt2/QcWarp + 2/(QcWarp*QcWarp));
			reserved_bt_struct[i].coeff_by[2] = (1 - sqrt2/QcWarp + 2/(QcWarp*QcWarp)) * reserved_bt_struct[i].gain;
			reserved_bt_struct[i].coeff_by[1] = (2 - 2 * 2/(QcWarp*QcWarp)) * reserved_bt_struct[i].gain;
			reserved_bt_struct[i].coeff_by[0] = 1;
			reserved_bt_struct[i].coeff_ax[0] = 1 * reserved_bt_struct[i].gain;
			reserved_bt_struct[i].coeff_ax[1] = 2 * reserved_bt_struct[i].gain;
			reserved_bt_struct[i].coeff_ax[2] = 1 * reserved_bt_struct[i].gain;
			reserved_bt_struct[i].in_use = 1;
			return (reserved_bt_struct + i);
		}
	}
	return 0;
}
float next_value_butterworth_filter(struct filter_struct_butterworth *bt_filter, float sample)
{
	bt_filter->xv[2] = bt_filter->xv[1]; bt_filter->xv[1] = bt_filter->xv[0];
	bt_filter->xv[0] = sample;
	bt_filter->yv[2] = bt_filter->yv[1]; bt_filter->yv[1] = bt_filter->yv[0];	
	bt_filter->yv[0] = (bt_filter->coeff_ax[0] * bt_filter->xv[0] + bt_filter->coeff_ax[1]
						* bt_filter->xv[1] + bt_filter->coeff_ax[2] * bt_filter->xv[2]
						- bt_filter->coeff_by[1] * bt_filter->yv[0]
						- bt_filter->coeff_by[2] * bt_filter->yv[1]);
	
	return bt_filter->yv[0];
}
int init_filter_struct(struct filter_struct* filter,enum filter_type type,const int samplerate, const int cutoff)
{
	filter->filter = type;
	switch(type){
	case BUTTERWORTH_FILTER :
		filter->filter_priv = init_butterworth_filter(samplerate,cutoff);
		if(filter->filter_priv)
			return 0;
		else
			return -1;
		break;
	default:
		return -1;
		break;
	}
}
float filter_value(struct filter_struct* filter, float value)
{
	switch(filter->filter){
	case BUTTERWORTH_FILTER :
		if(filter->filter_priv){
			return next_value_butterworth_filter((struct filter_struct_butterworth*)filter->filter_priv,value);
		}
		else
			return 0;
		break;
	default:
		return 0;
		break;
	}
}

#ifndef __FILTER_H__
#define __FILTER_H__

enum filter_type{
	BUTTERWORTH_FILTER = 1
};

struct filter_struct{
	enum filter_type filter;
	void* filter_priv;
};
#ifdef __cplusplus
extern "C" {
#endif

int init_filter_struct(struct filter_struct* filter,enum filter_type type, const int samplerate, const int cutoff);
float filter_value(struct filter_struct* filter, float value);
#ifdef __cplusplus
}
#endif
#endif

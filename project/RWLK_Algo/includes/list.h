#ifndef __LIST_H__
#define __LIST_H__

//heavily inspired from linux list;

#define BUILD_BUG_ON_ZERO(e) (sizeof(struct { int:-!!(e); }))

#ifndef __same_type
# define __same_type(a, b) __builtin_types_compatible_p(typeof(a), typeof(b))
#endif

#define __must_be_array(a)      BUILD_BUG_ON_ZERO(__same_type((a), &(a)[0]))

#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]) + __must_be_array(arr))


#define LIST_HEAD_INIT(list_name){&(list_name),&(list_name)}
/**
 * list_head - struct containing the list entry
 * @next : the next entry
 * @prev : the previous entry.
 */
struct list_head{
	struct list_head *next, *prev;
};
/**
 * list_add - Add an element to the list
 * @_new : the new list_head entry
 * @prev : the previous list_head entry of the list
 * @next : the next list_head entry of the list
 */
static inline void list_add(struct list_head *_new,struct list_head *prev, struct list_head *next)
{
	next->prev = _new;
	_new->next = next;
	_new->prev = prev;
	prev->next = _new;
}

/**
 * offset_of - evaluate the offset in bytes of a given member (struct or union)
 * @type : the struct name
 * @member : member of which we calculate the offset.
 */
#define offset_of(type, member) ((size_t) &((type*)0)->member)

/**
 * container_of - return the real adress of the container containing member
 * @ptr : address of the member
 * @type : the struct name
 * @member : member of which we calculate the offset.
 */
#define container_of(ptr,type,member) ({					\
			const typeof(((type*)0)->member) * sptr = (ptr);	\
			(type*)((char*)sptr - offset_of(type,member));})
/**
* list_entry - get the struct (type) of the list
* @ptr:        the list_head of the element.
* @type:       the type of the struct this is embedded in.
* @member:     the name of the list_head member within the struct.
*
* We calculate the offset of the list_head member, and then we compute the real addr of the the struct
* Note, that list is expected to be not empty.
*/
#define list_entry(ptr,type,member) \
	container_of(ptr,type,member)

#define list_first_entry(ptr,type,member)\
	list_entry((ptr)->next,type,member)
#define list_last_entry(ptr, type, member) \
	list_entry((ptr)->prev, type, member)

#define list_next_entry(pos,member)\
	list_entry((pos)->member.next,typeof(*(pos)), member)

static inline void init_list_head(struct list_head *list)
{
	list->next = list;
	list->prev = list;
}
/**
 * list_add_head - Add an element at the begining of the list
 * @_new : the new list_head entry
 * @prev : the previous list_head entry
 * @next : the next list_head entry
 */
static inline void list_add_head(struct list_head *_new, struct list_head *head)
{
	list_add(_new,head,head->next);
}
/**
 * list_add_tail - Add an element at the end of the list
 * @_new : the new list_head entry
 * @prev : the previous list_head entry
 * @next : the next list_head entry
 */
static inline void list_add_tail(struct list_head *_new, struct list_head *head)
{
	list_add(_new,head->prev,head);
}
/**
 * list_remove - Remove an element from the list
 * @prev : the previous list_head entry
 * @next : the next list_head entry
 * Careful, used inside a for_each loop lead to undefine behaviour. The removed element should have his
 * member also reseted.
 */
static inline void list_remove(struct list_head *prev, struct list_head *next)
{
	next->prev = prev;
	prev->next = next;
}
/**
 * list_remove - Remove an element from the list
 * @entry : the list_head entry
 * Careful, used inside a for_each loop lead to undefine behaviour. The removed element should have his
 * member also reseted.
 */
static inline void list_remove_entry(struct list_head* entry)
{
	list_remove(entry->prev,entry->next);
}
/**
 * list_empty - Return wheter a list is empty or note
 * @head : the list_head head
 * 
 */
static inline int list_is_empty(const struct list_head *head)
{
	return(head->next == head);
}
/**
 * list_move_tail - Move an element from on list the the tail of the other
 * @list : current list
 * @head : the new list_head head
 * 
 */
static inline void list_move_tail(struct list_head *list,struct list_head *head)
{
	list_remove_entry(list);
	list_add_tail(list,head);
}

#define list_for_each(pos,head) \
	for(pos = (head)->prev;pos != (head); pos = pos->next)

#define list_for_each_entry(pos,head,member)				\
	for (pos = list_first_entry(head,typeof(*pos), member); \
		 &pos->member != (head);							\
		 pos = list_next_entry(pos,member))
#define list_for_each_entry_safe(pos,n,head,member)			\
	for (pos = list_first_entry(head,typeof(*pos), member), \
			 n = list_next_entry(pos, member);				\
		 &pos->member != (head);							\
		 pos = n, n = list_next_entry(n,member))

static inline int list_empty(const struct list_head *head)
{
	return head->next == head;
}
#endif

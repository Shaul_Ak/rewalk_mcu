#ifndef __LIST_SORT_H__
#define __LIST_SORT_H__

#include <types.h>
#include <compiler.h>
struct list_head;

#ifdef __cplusplus
extern "C"{
#endif
void list_sort(void *priv, struct list_head *head,
		int (*cmp)(void *priv, struct list_head *a,
				   struct list_head *b));
#ifdef __cplusplus
}
#endif
#endif

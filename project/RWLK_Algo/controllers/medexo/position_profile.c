#include "position_profile.h"
#include "gyro_detection.h"
#include "pre_tension.h"


#define DEBUG 1

void reset_internal_vals_in_pf_controller(struct struct_pf_controller* my_struct_pf_controller){
	my_struct_pf_controller->pull_state = PF_PRETENSION_MODE;
	my_struct_pf_controller->f_max = -INFINITY_VALUE;		
	
	my_struct_pf_controller->flag_high_force_warning = 0;
	my_struct_pf_controller->stamp_start_active_pull = 0;
	my_struct_pf_controller->stamp_start_pushout = 0;	

	my_struct_pf_controller->v_pull_prev = my_struct_pf_controller->v_pull_init;
	reset_array_with_first_elem(my_struct_pf_controller->peak_forces, ARRAY_SIZE(my_struct_pf_controller->peak_forces));
	my_struct_pf_controller->average_peak_force = average(my_struct_pf_controller->peak_forces, ARRAY_SIZE(my_struct_pf_controller->peak_forces));
 	my_struct_pf_controller->f_max = -INFINITY_VALUE;
 	
 	my_struct_pf_controller->flag_reached_hard_stop = 0;
 	my_struct_pf_controller->flag_high_force_warning = 0; 	
}

void init_struct_position_profile(struct struct_position_profile *my_struct_position_profile)
{
	my_struct_position_profile->onset_percentage_pf = 40;
	my_struct_position_profile->f_pf_des = 100;
	my_struct_position_profile->p_df_des = 10;
	my_struct_position_profile->ctype = V2_CONTROLLER;
	my_struct_position_profile->flag_controller_changed = 0;	
	my_struct_position_profile->v_pull_df = 1.5 * SLACK_VELOCITY;
	my_struct_position_profile->v_push_out_df = 1.5 * SLACK_VELOCITY;	
	my_struct_position_profile->flag_df_active = 1;
	my_struct_position_profile->flag_pf_active = 1;
}


void init_struct_pf_controller(struct struct_pf_controller* my_struct_pf_controller)
{
	my_struct_pf_controller->p_pull_command = 0;	
	my_struct_pf_controller->v_command = SLACK_VELOCITY;	
	my_struct_pf_controller->p_max_pull = 0;	
	my_struct_pf_controller->v_pull_init = 0;
	my_struct_pf_controller->v_pull_prev = 0;
	my_struct_pf_controller->v_push_out = PULL_2_PUSHOUT_VELOCITY * SLACK_VELOCITY;	
	my_struct_pf_controller->v_pretension = SLACK_VELOCITY;
	
	s32 i = 0;
	for (i = 0; i < ARRAY_SIZE(my_struct_pf_controller->peak_forces); i++) {
		my_struct_pf_controller->peak_forces[i] = 0.0;
	}

	my_struct_pf_controller->f_max = -INFINITY_VALUE;
	my_struct_pf_controller->average_peak_force = 0.0;
	my_struct_pf_controller->pull_state = 0;
	my_struct_pf_controller->flag_reached_hard_stop = 0;	
	
	my_struct_pf_controller->flag_reached_desired_force = 0;
	my_struct_pf_controller->flag_high_force_warning = 0;
	
	my_struct_pf_controller->t_init_velocity_ramp = 0;
	my_struct_pf_controller->t_init_high_force_warning = 0;	

	my_struct_pf_controller->stamp_start_active_pull = 0;
	my_struct_pf_controller->stamp_start_pushout = 0;
	
}

void init_struct_df_controller(struct struct_df_controller* my_struct_df_controller)
{
	my_struct_df_controller->p_pull_command = 0;	
	my_struct_df_controller->v_command = SLACK_VELOCITY;			
	my_struct_df_controller->t_init_velocity_ramp = 0;		

	
}


void update_struct_pf_controller(float f_meas_, u32 t_ms_, float pull_curr, float p_lim_, const struct struct_gyro_detection* my_struct_gyro_detection,
									const struct struct_ftf_angle_detection* my_struct_ftf_angle_detection, const struct struct_pretension* my_struct_pf_pretension, 
									const struct struct_position_profile* my_struct_position_profile, struct struct_pf_controller* my_struct_pf_controller)
{
	// When stamp_start_active_pull is on, prepare to ramp up velocity and collect f_max, and flip back to 0.
	if (my_struct_pf_controller->stamp_start_active_pull) {
		my_struct_pf_controller->t_init_velocity_ramp = t_ms_;	
		my_struct_pf_controller->f_max = -INFINITY_VALUE;		
		my_struct_pf_controller->stamp_start_active_pull = 0;		
	}

	// When stamp_start_pushout is on, prepare to ramp up velocity and flip back to 0.
	if (my_struct_pf_controller->stamp_start_pushout) {
		my_struct_pf_controller->t_init_velocity_ramp = t_ms_;			
		my_struct_pf_controller->stamp_start_pushout = 0;		
	}

	// When npfl is confirmed, prepare to ramp up velocity, and update v_pull_init based on average peak force from previous strides.
	if (my_struct_gyro_detection->stamp_confirmed_npfl) {
		my_struct_pf_controller->t_init_velocity_ramp = t_ms_;	
		
		// Update default pull velocity/position command based on average peak force			
		if (my_struct_gyro_detection->count_pfl_after_last_stop > 0){
			
			// Flag when average_peak_force is over 90% of desired force for the first time
			if (!my_struct_pf_controller->flag_reached_desired_force && my_struct_pf_controller->peak_forces[PEAK_FORCE_ARRAY_SIZE-1] > 0.9 * my_struct_position_profile->f_pf_des)
				my_struct_pf_controller->flag_reached_desired_force = 1;
			
			if (!my_struct_pf_controller->flag_reached_hard_stop) {
					
				if (my_struct_pf_controller->flag_reached_desired_force) {

					if (my_struct_pf_controller->average_peak_force > my_struct_position_profile->f_pf_des) {
						float f_err = my_struct_pf_controller->average_peak_force - my_struct_position_profile->f_pf_des;
						// To avoid unsigned int becomes negative.
						if (f_err * CONSTANT_VELOCITY_DEFAULT_ADJUSTMENT_GAIN < my_struct_pf_controller->v_pull_init)
							my_struct_pf_controller->v_pull_init = my_struct_pf_controller->v_pull_init - f_err * CONSTANT_VELOCITY_DEFAULT_ADJUSTMENT_GAIN;
						else
							my_struct_pf_controller->v_pull_init = MIN_VELOCITY;
					}

					if (my_struct_pf_controller->average_peak_force < my_struct_position_profile->f_pf_des) {
						float f_err = my_struct_position_profile->f_pf_des - my_struct_pf_controller->average_peak_force;
						my_struct_pf_controller->v_pull_init = my_struct_pf_controller->v_pull_init + f_err * CONSTANT_VELOCITY_DEFAULT_ADJUSTMENT_GAIN;
					}				

					// Pull velocity should be v_min < velocity < V_max
					if (my_struct_pf_controller->v_pull_init < MIN_VELOCITY) my_struct_pf_controller->v_pull_init = MIN_VELOCITY;					
					else if (my_struct_pf_controller->v_pull_init > MAX_VELOCITY) my_struct_pf_controller->v_pull_init = MAX_VELOCITY;			
					
				}
				else {
					// Before reached target force, adjust velocity faster until reach 90% of target force					
					float f_err = my_struct_position_profile->f_pf_des - my_struct_pf_controller->peak_forces[PEAK_FORCE_ARRAY_SIZE-1];

					my_struct_pf_controller->v_pull_init = my_struct_pf_controller->v_pull_init + f_err * CONSTANT_VELOCITY_FAST_ADJUSTMENT_GAIN;				
					
					// Pull velocity should be v_min < velocity < V_max
					if (my_struct_pf_controller->v_pull_init < MIN_VELOCITY) my_struct_pf_controller->v_pull_init = MIN_VELOCITY;					
					else if (my_struct_pf_controller->v_pull_init > MAX_VELOCITY) my_struct_pf_controller->v_pull_init = MAX_VELOCITY;			
					
				}
				
				// Maximum pull position command before reaching target is  maximum available pull
				my_struct_pf_controller->p_max_pull = p_lim_ - my_struct_pf_pretension->p_pret_command;

			}			
		}
	}

	if(my_struct_gyro_detection->flag_is_walking) {

		if (my_struct_gyro_detection->gait_state == NON_PARETIC_SUPPORT) {			
			// pull position & velocity command
			my_struct_pf_controller->p_pull_command = 0;
			my_struct_pf_controller->v_command = my_struct_pf_controller->v_push_out;	

			// Switch pull state to PF_PRETENSION_MODE
			if (my_struct_pf_controller->pull_state != PF_PRETENSION_MODE) my_struct_pf_controller->pull_state = PF_PRETENSION_MODE;			
		}

		else if (my_struct_gyro_detection->gait_state == PARETIC_SUPPORT) {

			switch(my_struct_pf_controller->pull_state){

			case PF_PRETENSION_MODE:
				// pull position & velocity command		
				my_struct_pf_controller->p_pull_command = 0;	
				my_struct_pf_controller->v_command = my_struct_pf_controller->v_pretension;				
					
				// Switch pull_state to PF_CONTANT_PULL_VELOCITY_MODE when active pull condition is satisfied.
				if (my_struct_ftf_angle_detection->stamp_confirmed_np_mid_swing) {
					my_struct_pf_controller->pull_state = PF_CONSTANT_PULL_VELOCITY_MODE;								
					my_struct_pf_controller->stamp_start_active_pull = 1;		
					my_struct_pf_controller->f_max = -INFINITY_VALUE;	
					my_struct_pf_controller->flag_high_force_warning = 0;			
				}

				break;

			case PF_CONSTANT_PULL_VELOCITY_MODE:
				if (f_meas_ > my_struct_pf_controller->f_max) my_struct_pf_controller->f_max = f_meas_;					

				// Velocity and position command
				my_struct_pf_controller->p_pull_command = my_struct_pf_controller->p_max_pull;
				my_struct_pf_controller->v_command = my_struct_pf_controller->v_pull_init;
								
				// Switch pull_state
				// after treached desired force once, switch to adaptive pull velocity mode when current force measurement is > 30% of desired force or it took too much time in this state.
				if (my_struct_pf_controller->flag_reached_desired_force){
					if (my_struct_pf_controller->f_max > 0.3 * my_struct_position_profile->f_pf_des || my_struct_gyro_detection->gait_state == NON_PARETIC_SUPPORT) {
						my_struct_pf_controller->pull_state = PF_ADAPTIVE_PULL_VELOCITY_MODE;					
						my_struct_pf_controller->v_pull_prev = my_struct_pf_controller->v_command;						
					}
				}
				// Before reached desired force, switch directly directly to pushout mode when current force measurement < 80% of peak force
				else{

					if (!my_struct_pf_controller->flag_high_force_warning && my_struct_pf_controller->f_max > my_struct_position_profile->f_pf_des) {
						my_struct_pf_controller->flag_high_force_warning = 1;
						my_struct_pf_controller->t_init_high_force_warning = t_ms_;					
						my_struct_pf_controller->p_max_pull = pull_curr;		
						my_struct_pf_controller->p_pull_command = my_struct_pf_controller->p_max_pull;								
					}

					if ((my_struct_pf_controller->f_max > MINIMUN_PEAK_FORCE && f_meas_ < 0.8 * my_struct_pf_controller->f_max) || (my_struct_pf_controller->flag_high_force_warning && t_ms_-my_struct_pf_controller->t_init_high_force_warning > MAXIMUM_HIGH_FORCE_WARNING_DURATION)
						||  my_struct_gyro_detection->gait_state == NON_PARETIC_SUPPORT)
					{
						my_struct_pf_controller->pull_state = PF_PUSHOUT_MODE;		
						my_struct_pf_controller->stamp_start_pushout = 1;						
						my_struct_pf_controller->flag_high_force_warning = 0;
						push_data_in_array(my_struct_pf_controller->peak_forces, my_struct_pf_controller->f_max, PEAK_FORCE_ARRAY_SIZE);

						if (my_struct_pf_controller->peak_forces[0] > 0) my_struct_pf_controller->average_peak_force = average(my_struct_pf_controller->peak_forces, PEAK_FORCE_ARRAY_SIZE);					
						else my_struct_pf_controller->average_peak_force = my_struct_pf_controller->f_max;					
					}

						// Update pushout velocity based on maximum velocity command 
						my_struct_pf_controller->v_push_out = PULL_2_PUSHOUT_VELOCITY * my_struct_pf_controller->v_command;
						if (my_struct_pf_controller->v_push_out < SLACK_VELOCITY) my_struct_pf_controller->v_push_out = SLACK_VELOCITY;
						if (my_struct_pf_controller->v_push_out > MAX_VELOCITY) my_struct_pf_controller->v_push_out =MAX_VELOCITY;

						// Elmo position trajectory generation causes issues when pretension velocity is too slow, so let lower bound to be slack velocity before reached desired force.						
						my_struct_pf_controller->v_pretension = PULL_2_PRETENSION_VELOCITY * my_struct_pf_controller->v_command;
						if (my_struct_pf_controller->v_pretension < SLACK_VELOCITY) my_struct_pf_controller->v_pretension = SLACK_VELOCITY;
						if (my_struct_pf_controller->v_pretension > MAX_VELOCITY) my_struct_pf_controller->v_pretension = MAX_VELOCITY;
				}

				break;

			case PF_ADAPTIVE_PULL_VELOCITY_MODE:
				if (my_struct_pf_controller->f_max < f_meas_) my_struct_pf_controller->f_max = f_meas_;					

				if (!my_struct_pf_controller->flag_high_force_warning && my_struct_pf_controller->f_max > my_struct_position_profile->f_pf_des) {
					my_struct_pf_controller->flag_high_force_warning = 1;
					my_struct_pf_controller->t_init_high_force_warning = t_ms_;					
					my_struct_pf_controller->p_max_pull = pull_curr;										
				}

				// pull position/velocity command
				my_struct_pf_controller->p_pull_command = my_struct_pf_controller->p_max_pull;
				
				if (my_struct_pf_controller->flag_high_force_warning){
					my_struct_pf_controller->v_command = my_struct_pf_controller->v_pull_prev;
					if (my_struct_pf_controller->v_command < SLACK_VELOCITY) my_struct_pf_controller->v_command = SLACK_VELOCITY;
				}
				else{
					// Regulate pull velocity based on how far measured force is from desired force.
					if (f_meas_ > 0.8 * my_struct_position_profile->f_pf_des) {
						float f_err = f_meas_ - 0.8 * my_struct_position_profile->f_pf_des;
						my_struct_pf_controller->v_command = my_struct_pf_controller->v_pull_prev - ADAPTIVE_VELOCITY_REDUCTION_GAIN * f_err;

						if (my_struct_pf_controller->v_command < MIN_VELOCITY) my_struct_pf_controller->v_command = MIN_VELOCITY;					
					}
					else {	
						float f_err = 0.8 * my_struct_position_profile->f_pf_des - f_meas_;
						my_struct_pf_controller->v_command = my_struct_pf_controller->v_pull_prev + ADAPTIVE_VELOCITY_RISE_GAIN * f_err;
						if (my_struct_pf_controller->v_command > MAX_VELOCITY) my_struct_pf_controller->v_command = MAX_VELOCITY;													

						// Update pushout velocity based on maximum velocity command 
						my_struct_pf_controller->v_push_out = PULL_2_PUSHOUT_VELOCITY * my_struct_pf_controller->v_command;
						if (my_struct_pf_controller->v_push_out < SLACK_VELOCITY) my_struct_pf_controller->v_push_out = SLACK_VELOCITY;
						if (my_struct_pf_controller->v_push_out > MAX_VELOCITY) my_struct_pf_controller->v_push_out =MAX_VELOCITY;

						my_struct_pf_controller->v_pretension = PULL_2_PRETENSION_VELOCITY * my_struct_pf_controller->v_command;
						if (my_struct_pf_controller->v_pretension < SLACK_VELOCITY) my_struct_pf_controller->v_pretension = SLACK_VELOCITY;
						if (my_struct_pf_controller->v_pretension > MAX_VELOCITY) my_struct_pf_controller->v_pretension = MAX_VELOCITY;
					}

				}	
				// Save velocity for next loop
				my_struct_pf_controller->v_pull_prev = my_struct_pf_controller->v_command;

				// Switch pull_state to pushout mode when condition is satisfied.
				if (f_meas_ < 0.8 * my_struct_pf_controller->f_max || (my_struct_pf_controller->flag_high_force_warning && t_ms_-my_struct_pf_controller->t_init_high_force_warning > MAXIMUM_HIGH_FORCE_WARNING_DURATION)
					|| my_struct_gyro_detection->gait_state == NON_PARETIC_SUPPORT) {
					my_struct_pf_controller->pull_state = PF_PUSHOUT_MODE;
					my_struct_pf_controller->stamp_start_pushout = 1;
		
					my_struct_pf_controller->flag_high_force_warning = 0;
					my_struct_pf_controller->t_init_velocity_ramp = t_ms_;											
					
					if (abs(pull_curr+my_struct_pf_pretension->p_pret_command - p_lim_) < 5) my_struct_pf_controller->flag_reached_hard_stop = 1;					
					else my_struct_pf_controller->flag_reached_hard_stop = 0;

					push_data_in_array(my_struct_pf_controller->peak_forces, my_struct_pf_controller->f_max, PEAK_FORCE_ARRAY_SIZE);

					if (my_struct_pf_controller->peak_forces[0] > 0) my_struct_pf_controller->average_peak_force = average(my_struct_pf_controller->peak_forces, PEAK_FORCE_ARRAY_SIZE);					
					else my_struct_pf_controller->average_peak_force = my_struct_pf_controller->f_max;
				}

				break;

			case PF_PUSHOUT_MODE:
				my_struct_pf_controller->p_pull_command = 0;			
				my_struct_pf_controller->v_command = my_struct_pf_controller->v_push_out;																			
				
				break;
				
			default:
				my_struct_pf_controller->p_pull_command = 0;		
				my_struct_pf_controller->v_command	= SLACK_VELOCITY;
				break;
			}
		}
	
		// Limit pull command
		if (my_struct_pf_controller->p_pull_command + my_struct_pf_pretension->p_pret_command > p_lim_) {
			my_struct_pf_controller->p_pull_command = p_lim_ - my_struct_pf_pretension->p_pret_command;
		}	
	}
	else {
		// pull position & velocity command			
		my_struct_pf_controller->p_pull_command = 0;
		my_struct_pf_controller->v_command = SLACK_VELOCITY;
		reset_internal_vals_in_pf_controller(my_struct_pf_controller);
	}
}


void update_struct_df_controller(u32 t_ms_, float p_lim_, const struct struct_gyro_detection* my_struct_gyro_detection, const struct struct_pretension* my_struct_df_pretension, const struct struct_position_profile* my_struct_position_profile,  struct struct_df_controller* my_struct_df_controller){
		
	if (my_struct_gyro_detection->stamp_confirmed_npfl || my_struct_gyro_detection->stamp_confirmed_pfl){
		my_struct_df_controller->t_init_velocity_ramp = t_ms_;
	}

	if (my_struct_gyro_detection->flag_is_walking) {
		if (my_struct_gyro_detection->gait_state == NON_PARETIC_SUPPORT) {
			my_struct_df_controller->p_pull_command = my_struct_position_profile->p_df_des;			
			my_struct_df_controller->v_command = (float) my_struct_position_profile->v_pull_df;			
		}
		else if (my_struct_gyro_detection->gait_state == PARETIC_SUPPORT) {
			my_struct_df_controller->p_pull_command = 0;			
			my_struct_df_controller->v_command = my_struct_position_profile->v_push_out_df;		
		}
	}
	else{
		my_struct_df_controller->p_pull_command = 0;
		my_struct_df_controller->v_command = SLACK_VELOCITY;						
	}

	///////// Software Limits ////////////////////
	if (my_struct_df_controller->p_pull_command + my_struct_df_pretension->p_pret_command > p_lim_) {
		my_struct_df_controller->p_pull_command = p_lim_ - my_struct_df_pretension->p_pret_command;
	}	
	
}

void change_pf_controller_type(void* arg1, u8* arg2, u8 size)
{
	struct struct_position_profile* p_profile = (struct struct_position_profile *)(arg1);
#ifdef __WYSS_FW__
	MEDEXO_DEBUG("Allright cb ptens  called with %p, and %p, and %i also %p set to %i\r\n",
				 arg1,arg2,size,arg1, arg2[0]);
#endif
	if (size == 4 && arg2[0] < 2) {
		p_profile->ctype = arg2[0];
		p_profile->flag_controller_changed = 1;
#ifdef __WYSS_FW__
		MEDEXO_DEBUG("PARAM SET %i\r\n",p_profile->ctype);
#endif
	}
}


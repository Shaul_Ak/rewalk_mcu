#include "gyro_detection.h"

float weighted_average(float arr[], float weight[], int arr_size){
   int i; float weighted_sum= 0, numerator = 0;
   
   for (i=0; i<arr_size; i++){
	 weighted_sum += arr[i]*weight[i];
	 numerator += weight[i];
   }

   return(weighted_sum/numerator);
}

float average(float arr[], int arr_size){
   int i; float sum= 0;

   for (i=0; i<arr_size; i++) sum += arr[i];	    

   return(sum/arr_size);
}

void push_data_in_array (float arr[], float d, int arr_size) {
   int i;
   
   for (i=0; i<arr_size-1; i++){
	 arr[i] = arr[i+1];	 
   }
   arr[arr_size-1] = d;
}

void reset_array_with_first_elem(float arr[], int arr_size) {
   int i;
   
   for (i=0; i<arr_size; i++){
	 arr[i] = arr[0];	 
   }   
}


void init_struct_gyro_detection(struct struct_gyro_detection* my_struct_gyro_detection){
	my_struct_gyro_detection->stamp_confirmed_pfl = 0;	
	my_struct_gyro_detection->stamp_confirmed_npfl = 0;	
	my_struct_gyro_detection->time_at_last_pfl = 0;
	my_struct_gyro_detection->time_at_second_last_pfl = 0;
	my_struct_gyro_detection->time_at_last_npfl = 0;	
	my_struct_gyro_detection->time_after_last_pfl = 0;
	my_struct_gyro_detection->time_after_last_npfl = 0;
	my_struct_gyro_detection->time_at_pfl_candidate = 0;
	my_struct_gyro_detection->time_at_npfl_candidate = 0;		
	my_struct_gyro_detection->time_at_last_stop = 0;			
	my_struct_gyro_detection->time_at_last_pfc = 0;
	my_struct_gyro_detection->time_at_last_npfc= 0 ;
	my_struct_gyro_detection->time_at_pfc_candidate = 0;
	my_struct_gyro_detection->time_at_npfc_candidate = 0;	
	my_struct_gyro_detection->time_stop_confirmation_threshold = STOP_CONFIRM_TIME_2_STRIDE_TIME*DEFAULT_STRIDE_TIME;

	my_struct_gyro_detection->gyro_npfl_detection_threshold = MINIMUM_GYRO_FL_DETECTION_THRESHOLD;
	my_struct_gyro_detection->gyro_pfl_detection_threshold = MINIMUM_GYRO_FL_DETECTION_THRESHOLD;
	my_struct_gyro_detection->gyro_max_np = -INFINITY_VALUE;	
	my_struct_gyro_detection->gyro_max_p = -INFINITY_VALUE;	
	my_struct_gyro_detection->gyro_min_np = INFINITY_VALUE;	
	my_struct_gyro_detection->gyro_min_p = INFINITY_VALUE;		
	my_struct_gyro_detection->gyro_pfc_candidate = -INFINITY_VALUE;	
	my_struct_gyro_detection->gyro_npfc_candidate = -INFINITY_VALUE;
	my_struct_gyro_detection->gait_state = PARETIC_SUPPORT;
	my_struct_gyro_detection->count_pfl_after_last_stop = 0;
	my_struct_gyro_detection->count_pfl_total = 0;	
	my_struct_gyro_detection->flag_searching_peak = 0;
	my_struct_gyro_detection->flag_is_walking = 0;	

	u32 i;
	for (i = 0; i < GYRO_ARRAY_SIZE; i++) {
		my_struct_gyro_detection->gyros_at_npfl[i] = 0;
		my_struct_gyro_detection->gyros_at_pfl[i] = 0;
	}

	// Weight vector for weighted average calculation. Weigh more on the latest data point (w1 < w2 < w3).
	my_struct_gyro_detection->weight_vector[0] = W1;
	my_struct_gyro_detection->weight_vector[1] = W2;	
	my_struct_gyro_detection->weight_vector[2] = W3;		
	
}

void init_struct_ftf_angle_detection(struct struct_ftf_angle_detection* my_ftf_angle_detection_struct){
	my_ftf_angle_detection_struct->stamp_confirmed_np_mid_swing = 0;
	my_ftf_angle_detection_struct->stamp_confirmed_p_mid_swing = 0;
	my_ftf_angle_detection_struct->ftf_ang_max_np = -INFINITY_VALUE;
	my_ftf_angle_detection_struct->ftf_ang_min_np = INFINITY_VALUE;
	my_ftf_angle_detection_struct->ftf_ang_max_p = -INFINITY_VALUE;
	my_ftf_angle_detection_struct->ftf_ang_min_p = INFINITY_VALUE;		
	my_ftf_angle_detection_struct->flag_searching_mid_swing_np = 0;
	my_ftf_angle_detection_struct->flag_searching_mid_swing_p = 0;
}

void init_struct_gait_param(struct struct_gait_param* my_struct_gait_param){
	
	u32 i;
	for (i = 0; i < 3; i++) {
		my_struct_gait_param->stride_times[i] = DEFAULT_STRIDE_TIME;		
	}

	my_struct_gait_param->p_double_support_time = 0.1 * DEFAULT_STRIDE_TIME;
	my_struct_gait_param->p_single_support_time = 0.4 * DEFAULT_STRIDE_TIME;
	my_struct_gait_param->np_double_support_time = 0.1 * DEFAULT_STRIDE_TIME;
	my_struct_gait_param->np_single_support_time = 0.4 * DEFAULT_STRIDE_TIME; 
	my_struct_gait_param->p_step_time = 0.5 * DEFAULT_STRIDE_TIME; 
	my_struct_gait_param->np_step_time = 0.5 * DEFAULT_STRIDE_TIME; 
	my_struct_gait_param->p_support_time = 0.5 * DEFAULT_STRIDE_TIME; 
	my_struct_gait_param->np_support_time = 0.5 * DEFAULT_STRIDE_TIME; 
}

void detect_foot_lift_off(float gyro_np_, float gyro_p_, u32 time_, struct struct_gyro_detection* my_struct_gyro_detection)
{	
	
	// Reset foot-lift(toe-off) detection confirmation stamp if it's on.
	if(my_struct_gyro_detection->stamp_confirmed_npfl) my_struct_gyro_detection->stamp_confirmed_npfl = 0;
	if(my_struct_gyro_detection->stamp_confirmed_pfl) my_struct_gyro_detection->stamp_confirmed_pfl = 0;
	
	// Calculate time elapsed after last non-paretic/paretic foot lift (toe-off)
	if (my_struct_gyro_detection->time_at_last_npfl != 0) my_struct_gyro_detection->time_after_last_npfl = time_ - my_struct_gyro_detection->time_at_last_npfl;
	if (my_struct_gyro_detection->time_at_last_pfl != 0) my_struct_gyro_detection->time_after_last_pfl = time_ - my_struct_gyro_detection->time_at_last_pfl;
	
	// In paretic support (paretic single & double support), look for paretic foot lift-off (pfl) and send a stamp when pfl is confirmed.
	if (my_struct_gyro_detection->gait_state == PARETIC_SUPPORT) {
		
		// Look for pfl only after negative non-paretic gyro signal becomes positive again after being negative.
		if (!my_struct_gyro_detection->flag_searching_peak){
			
			if (gyro_np_ < my_struct_gyro_detection->gyro_min_np) my_struct_gyro_detection->gyro_min_np  = gyro_np_;
			// Flag flag_searching_peak when negative non-paretic gyro signal becomes positive again after being negative.
			if (my_struct_gyro_detection->gyro_min_np < GYRO_NEGATIVE_CONFIRMATION_THRESHOLD && gyro_np_ > 0) {
				my_struct_gyro_detection->flag_searching_peak = 1;		
				my_struct_gyro_detection->gyro_min_np = INFINITY_VALUE;							
			}
		}
		else{
			
			if ((my_struct_gyro_detection->gyro_max_p < gyro_p_)) {
				my_struct_gyro_detection->gyro_max_p = gyro_p_;
				my_struct_gyro_detection->time_at_pfl_candidate = time_;
			}	

			// Once pfl detection is confirmed, send confirmation stamp, add pfl count, save when last pfl is detected, and switch to non-paretic support state.
			if (my_struct_gyro_detection->gyro_max_p > my_struct_gyro_detection->gyro_pfl_detection_threshold && gyro_p_ < 0) {
				// Update output params
				my_struct_gyro_detection->stamp_confirmed_pfl = 1;
				my_struct_gyro_detection->time_at_second_last_pfl = my_struct_gyro_detection->time_at_last_pfl;
				my_struct_gyro_detection->time_at_last_pfl = my_struct_gyro_detection->time_at_pfl_candidate;
				my_struct_gyro_detection->time_after_last_pfl = time_ - my_struct_gyro_detection->time_at_last_pfl;
				my_struct_gyro_detection->count_pfl_after_last_stop++;
				my_struct_gyro_detection->count_pfl_total++;
				// Switch gait state
				my_struct_gyro_detection->gait_state = NON_PARETIC_SUPPORT;			
							
				// Update peak detection threshold based on weighted average of last three peak gyro measurements.
				push_data_in_array(my_struct_gyro_detection->gyros_at_pfl, my_struct_gyro_detection->gyro_max_p, GYRO_ARRAY_SIZE);
				my_struct_gyro_detection->gyro_pfl_detection_threshold = PEAK_DETECTION_THRESHOLD_RATE * weighted_average(my_struct_gyro_detection->gyros_at_pfl,my_struct_gyro_detection->weight_vector,GYRO_ARRAY_SIZE);			
				if (my_struct_gyro_detection->gyro_pfl_detection_threshold < MINIMUM_GYRO_FL_DETECTION_THRESHOLD) my_struct_gyro_detection->gyro_pfl_detection_threshold = MINIMUM_GYRO_FL_DETECTION_THRESHOLD;			

				// Reset detection params
				my_struct_gyro_detection->gyro_max_p = -INFINITY_VALUE; 
				my_struct_gyro_detection->flag_searching_peak = 0;
			}
		}
		
	}

	// In non-paretic support (non-paretic single & double supoport), look for non-paretic foot lift (npfl) and send a stamp when npfl is confirmed.	
	// Same idea as paretic-support.
	if (my_struct_gyro_detection->gait_state == NON_PARETIC_SUPPORT) {

		if(!my_struct_gyro_detection->flag_searching_peak){				
			
			if (gyro_p_ < my_struct_gyro_detection->gyro_min_p) my_struct_gyro_detection->gyro_min_p  = gyro_p_;

			if (my_struct_gyro_detection->gyro_min_p < GYRO_NEGATIVE_CONFIRMATION_THRESHOLD && gyro_p_ > 0) {
				my_struct_gyro_detection->flag_searching_peak = 1;	
				my_struct_gyro_detection->gyro_min_p = INFINITY_VALUE;							
			}
		}
		else{
			
			if ((my_struct_gyro_detection->gyro_max_np < gyro_np_)) {
				my_struct_gyro_detection->gyro_max_np = gyro_np_;
				my_struct_gyro_detection->time_at_npfl_candidate = time_;
			}
			
			if (my_struct_gyro_detection->gyro_max_np > my_struct_gyro_detection->gyro_npfl_detection_threshold && gyro_np_ < 0) {

				my_struct_gyro_detection->stamp_confirmed_npfl = 1;
				my_struct_gyro_detection->time_at_last_npfl = my_struct_gyro_detection->time_at_npfl_candidate;
				my_struct_gyro_detection->time_after_last_npfl = time_ - my_struct_gyro_detection->time_at_last_npfl;
				my_struct_gyro_detection->gait_state = PARETIC_SUPPORT;			

				push_data_in_array(my_struct_gyro_detection->gyros_at_npfl, my_struct_gyro_detection->gyro_max_np, GYRO_ARRAY_SIZE);
				my_struct_gyro_detection->gyro_npfl_detection_threshold = PEAK_DETECTION_THRESHOLD_RATE * weighted_average(my_struct_gyro_detection->gyros_at_npfl,my_struct_gyro_detection->weight_vector,GYRO_ARRAY_SIZE);			

				if (my_struct_gyro_detection->gyro_npfl_detection_threshold < MINIMUM_GYRO_FL_DETECTION_THRESHOLD) my_struct_gyro_detection->gyro_npfl_detection_threshold = MINIMUM_GYRO_FL_DETECTION_THRESHOLD;			

				my_struct_gyro_detection->gyro_max_np = -INFINITY_VALUE;
				my_struct_gyro_detection->flag_searching_peak = 0;
			}
		}
	}

	// Check if wearer is walking. if pfl/npfl isn't detected for time_stop_confirmation_threshold, confirm wearer stopped.
	if (my_struct_gyro_detection->count_pfl_after_last_stop > 0)
	{
		if (min(my_struct_gyro_detection->time_after_last_pfl, my_struct_gyro_detection->time_after_last_npfl) > my_struct_gyro_detection->time_stop_confirmation_threshold) {
			my_struct_gyro_detection->flag_is_walking = 0;
			
			if (my_struct_gyro_detection->count_pfl_after_last_stop > 0) {
				my_struct_gyro_detection->count_pfl_after_last_stop = 0;
				my_struct_gyro_detection->time_at_last_stop = time_;
			}

		}
		else {
			my_struct_gyro_detection->flag_is_walking = 1;
		}
	}
	else {
		my_struct_gyro_detection->flag_is_walking = 0;			
	}
}

void detect_mid_swing(s8 handle_mode_,float ftf_ang_np_, float ftf_ang_p_, const struct struct_gyro_detection* my_struct_gyro_detection, struct struct_ftf_angle_detection* my_struct_ftf_angle_detection)
{		
	// Reset confirmation stamp when it's on.
	if (my_struct_ftf_angle_detection->stamp_confirmed_np_mid_swing) my_struct_ftf_angle_detection->stamp_confirmed_np_mid_swing = 0;
	if (my_struct_ftf_angle_detection->stamp_confirmed_p_mid_swing) my_struct_ftf_angle_detection->stamp_confirmed_p_mid_swing = 0;

	if (handle_mode_ != ACTIVE_MODE){
		// Reset internal vars when not active_mode
		my_struct_ftf_angle_detection->flag_searching_mid_swing_p = 0;
		my_struct_ftf_angle_detection->flag_searching_mid_swing_np = 0;
		my_struct_ftf_angle_detection->ftf_ang_min_p = INFINITY_VALUE;	
		my_struct_ftf_angle_detection->ftf_ang_min_np = INFINITY_VALUE;	
	}
	else{
		// In paretic support (paretic single/double support), Look for minimum np ftf angle, and confirm non-paretic mid swing when non-pareti ftf angle rises by certain percentage of np ftf angle.
		if (my_struct_gyro_detection->gait_state == PARETIC_SUPPORT) {
			// Reset flag_searching_mid_swing_p for next NON_PARETIC_SUPPORT
			if (!my_struct_ftf_angle_detection->flag_searching_mid_swing_p) my_struct_ftf_angle_detection->flag_searching_mid_swing_p = 1;

			if (my_struct_ftf_angle_detection->flag_searching_mid_swing_np && (my_struct_ftf_angle_detection->ftf_ang_min_np > ftf_ang_np_)) my_struct_ftf_angle_detection->ftf_ang_min_np = ftf_ang_np_;		

			if (my_struct_ftf_angle_detection->flag_searching_mid_swing_np && ftf_ang_np_ > RISE_RATE_4_MID_SWING_CONFIRMATION * my_struct_ftf_angle_detection->ftf_ang_min_np) {
				my_struct_ftf_angle_detection->stamp_confirmed_np_mid_swing = 1;
				my_struct_ftf_angle_detection->ftf_ang_min_np = INFINITY_VALUE;			
				my_struct_ftf_angle_detection->flag_searching_mid_swing_np = 0;
			}
		}

		// In non-paretic support (non-paretic single/double support), Look for minimum p ftf angle, and confirm paretic mid swing when p ftf angle rises by certain percentage of minimum p ftf angle.
		// Same idea as paretic support
		if (my_struct_gyro_detection->gait_state == NON_PARETIC_SUPPORT) {
			if(!my_struct_ftf_angle_detection->flag_searching_mid_swing_np) my_struct_ftf_angle_detection->flag_searching_mid_swing_np = 1;

			if (my_struct_ftf_angle_detection->flag_searching_mid_swing_p && (my_struct_ftf_angle_detection->ftf_ang_min_p > ftf_ang_p_)) my_struct_ftf_angle_detection->ftf_ang_min_p = ftf_ang_p_;		

			if (my_struct_ftf_angle_detection->flag_searching_mid_swing_p && ftf_ang_p_ > RISE_RATE_4_MID_SWING_CONFIRMATION * my_struct_ftf_angle_detection->ftf_ang_min_p) {
				my_struct_ftf_angle_detection->stamp_confirmed_p_mid_swing = 1;
				my_struct_ftf_angle_detection->ftf_ang_min_p = INFINITY_VALUE;			
				my_struct_ftf_angle_detection->flag_searching_mid_swing_p = 0;
			}
		}
	}
}

void get_time_at_foot_contact(float gyro_np_, float gyro_p_, u32 time_, struct struct_gyro_detection* my_struct_gyro_detection)
{		
	if(my_struct_gyro_detection->stamp_confirmed_pfl){
		my_struct_gyro_detection->time_at_last_npfc = my_struct_gyro_detection->time_at_npfc_candidate;
		my_struct_gyro_detection->gyro_pfc_candidate = -INFINITY_VALUE;
	} 
	if(my_struct_gyro_detection->stamp_confirmed_npfl){
		my_struct_gyro_detection->time_at_last_pfc = my_struct_gyro_detection->time_at_pfc_candidate;
		my_struct_gyro_detection->gyro_npfc_candidate = -INFINITY_VALUE;
	} 
	
	// In paretic support (paretic single/double support), 
	if (my_struct_gyro_detection->gait_state == PARETIC_SUPPORT && my_struct_gyro_detection->flag_searching_peak) {
		if (gyro_np_ > my_struct_gyro_detection->gyro_npfc_candidate){
			my_struct_gyro_detection->gyro_npfc_candidate = gyro_np_;
			my_struct_gyro_detection->time_at_npfc_candidate = time_;
		}	
	}

	// In non-paretic support (non-paretic single/double support), 
	if (my_struct_gyro_detection->gait_state == NON_PARETIC_SUPPORT  && my_struct_gyro_detection->flag_searching_peak) {
		if (gyro_p_ > my_struct_gyro_detection->gyro_pfc_candidate){
			my_struct_gyro_detection->gyro_pfc_candidate = gyro_p_;
			my_struct_gyro_detection->time_at_pfc_candidate = time_;
		}	
	}
}



void update_struct_gait_param(s8 handle_state_, struct struct_gyro_detection* my_struct_gyro_detection, struct struct_gait_param* my_struct_gait_param)
{
	// Gait parameters are calculated only when wearer is walking in active mode
	if (my_struct_gyro_detection->flag_is_walking && handle_state_ == ACTIVE_MODE) {

		if (my_struct_gyro_detection->count_pfl_after_last_stop > MIN_PFL_COUNT_FOR_GAIT_PARAM_UPDATE){
			// When paretic foot lift detection is confirmed, calculate paretic stride time (duration between consecutive paretic toe offs), and paretic double/single support time.
			if (my_struct_gyro_detection->stamp_confirmed_pfl) {
				
				if(my_struct_gyro_detection->time_at_last_npfc > my_struct_gyro_detection->time_at_last_stop){			
					my_struct_gait_param->p_double_support_time = my_struct_gyro_detection->time_at_last_pfl-my_struct_gyro_detection->time_at_last_npfc;

					if (my_struct_gyro_detection->time_at_last_pfc > my_struct_gyro_detection->time_at_last_stop && my_struct_gyro_detection->time_at_last_npfc > my_struct_gyro_detection->time_at_last_pfc)
						my_struct_gait_param->p_step_time = my_struct_gyro_detection->time_at_last_npfc-my_struct_gyro_detection->time_at_last_pfc;				
				}

				if(my_struct_gyro_detection->time_at_last_npfl > my_struct_gyro_detection->time_at_last_stop &&  my_struct_gyro_detection->time_at_last_npfc > my_struct_gyro_detection->time_at_last_npfl)
					my_struct_gait_param->p_single_support_time = my_struct_gyro_detection->time_at_last_npfc-my_struct_gyro_detection->time_at_last_npfl;

				if (my_struct_gyro_detection->time_at_last_npfl > my_struct_gyro_detection->time_at_last_stop && my_struct_gyro_detection->time_at_last_pfl > my_struct_gyro_detection->time_at_last_npfl)
					my_struct_gait_param->p_support_time = my_struct_gyro_detection->time_at_last_pfl - my_struct_gyro_detection->time_at_last_npfl;				
				
				// Update time_stop_confirmation threshold based on stride times acquired from three previous strides.
				if (my_struct_gyro_detection->time_at_second_last_pfl > my_struct_gyro_detection->time_at_last_stop && my_struct_gyro_detection->time_at_last_pfl > my_struct_gyro_detection->time_at_second_last_pfl) {
					float last_stride_time = my_struct_gyro_detection->time_at_last_pfl - my_struct_gyro_detection->time_at_second_last_pfl;
				
					if (last_stride_time < my_struct_gyro_detection->time_stop_confirmation_threshold){
						push_data_in_array(my_struct_gait_param->stride_times, last_stride_time, ARRAY_SIZE(my_struct_gait_param->stride_times));
						my_struct_gyro_detection->time_stop_confirmation_threshold = STOP_CONFIRM_TIME_2_STRIDE_TIME * average(my_struct_gait_param->stride_times, ARRAY_SIZE(my_struct_gait_param->stride_times));					
					}
				}

			}

			// When non-paretic foot lift detection is confirmed, calculate non-paretic support time.
			if (my_struct_gyro_detection->stamp_confirmed_npfl) {

				if(my_struct_gyro_detection->time_at_last_pfc > my_struct_gyro_detection->time_at_last_stop){
					my_struct_gait_param->np_double_support_time = my_struct_gyro_detection->time_at_last_npfl-my_struct_gyro_detection->time_at_last_pfc;

					if (my_struct_gyro_detection->time_at_last_npfc > my_struct_gyro_detection->time_at_last_stop  && my_struct_gyro_detection->time_at_last_pfc > my_struct_gyro_detection->time_at_last_npfc)
						my_struct_gait_param->np_step_time = my_struct_gyro_detection->time_at_last_pfc-my_struct_gyro_detection->time_at_last_npfc;				
				}

				if(my_struct_gyro_detection->time_at_last_pfl > my_struct_gyro_detection->time_at_last_stop &&  my_struct_gyro_detection->time_at_last_pfc > my_struct_gyro_detection->time_at_last_pfl)
					my_struct_gait_param->np_single_support_time = my_struct_gyro_detection->time_at_last_pfc-my_struct_gyro_detection->time_at_last_pfl;			

				if (my_struct_gyro_detection->time_at_last_pfl > my_struct_gyro_detection->time_at_last_stop && my_struct_gyro_detection->time_at_last_npfl > my_struct_gyro_detection->time_at_last_pfl)
					my_struct_gait_param->np_support_time = my_struct_gyro_detection->time_at_last_npfl - my_struct_gyro_detection->time_at_last_pfl;											
			}
		}

	}
	else {		
			if (my_struct_gyro_detection->count_pfl_after_last_stop > 0) reset_array_with_first_elem(my_struct_gait_param->stride_times, ARRAY_SIZE(my_struct_gait_param->stride_times));			
	}
	
}


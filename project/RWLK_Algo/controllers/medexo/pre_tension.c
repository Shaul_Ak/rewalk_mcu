#include "pre_tension.h"
#include "position_profile.h"
#include "gyro_detection.h"


void init_struct_pretension(struct struct_pretension *my_struct_pretension)
{
	my_struct_pretension->p_pret_command = 0;
	
	my_struct_pretension->f_pret = -INFINITY_VALUE;
	my_struct_pretension->p_pret_des_next = 0;
	my_struct_pretension->p_pret_des_curr = 0;	

	my_struct_pretension->flag_initial_setting_completed = 0;
	my_struct_pretension->t_init_pretension_detected = 0;
}

void update_struct_df_pretension(float f_meas_,float p_lim_,const struct struct_gyro_detection *my_struct_gyro_detection, struct struct_pretension *my_struct_pretension)
{

	if (my_struct_gyro_detection->flag_is_walking) {
		// Get df force at paretic foot lift (pfl), and update pretension position for next stride
		if (my_struct_gyro_detection->stamp_confirmed_pfl) {
			my_struct_pretension->f_pret = f_meas_;
			
			if (my_struct_pretension->f_pret > PRETENSION_FORCE_UPPER_BOUND) {
				float f_err = my_struct_pretension->f_pret - TARGET_PRETENSION_FORCE;
				my_struct_pretension->p_pret_des_next = my_struct_pretension->p_pret_des_curr - PRETENSION_POSITION_ADJUSTMENT_GAIN * f_err;

				if (my_struct_pretension->p_pret_des_next < 0) my_struct_pretension->p_pret_des_next = my_struct_pretension->p_pret_des_curr;					
			}
			else if ( my_struct_pretension->f_pret < PRETENSION_FORCE_LOWER_BOUND) {
				float f_err = TARGET_PRETENSION_FORCE - my_struct_pretension->f_pret;
				my_struct_pretension->p_pret_des_next = my_struct_pretension->p_pret_des_curr + PRETENSION_POSITION_ADJUSTMENT_GAIN * f_err;

				if (my_struct_pretension->p_pret_des_next > p_lim_) my_struct_pretension->p_pret_des_next = my_struct_pretension->p_pret_des_curr;					
			}

		}

		if (my_struct_gyro_detection->gait_state == PARETIC_SUPPORT) {
			my_struct_pretension->p_pret_des_curr = my_struct_pretension->p_pret_des_next;
			my_struct_pretension->p_pret_command = my_struct_pretension->p_pret_des_curr;
		}		
		else if (my_struct_gyro_detection->gait_state == NON_PARETIC_SUPPORT) {
			my_struct_pretension->p_pret_command = my_struct_pretension->p_pret_des_curr;
		}
	}
	// If gyro detection algorithm confirms that wearer is not walking, release cable by EXTRA_PRETENSION_PUSHOUT [mm]
	else{
		my_struct_pretension->p_pret_command = my_struct_pretension->p_pret_des_curr-EXTRA_PRETENSION_PUSHOUT;
		if (my_struct_pretension->p_pret_command <0) my_struct_pretension->p_pret_command = 0;
	}
	
}

void update_struct_pf_pretension(float f_meas_, float p_lim_, const struct struct_gyro_detection *my_struct_gyro_detection, const struct struct_pf_controller *my_struct_pf_controller, struct struct_pretension *my_struct_pretension)
{
	if (my_struct_gyro_detection->flag_is_walking) {
		
		if (my_struct_pf_controller->stamp_start_active_pull)
		 {				
			my_struct_pretension->f_pret = f_meas_;
		
			if (my_struct_pretension->f_pret > PRETENSION_FORCE_UPPER_BOUND) {
				float f_err = my_struct_pretension->f_pret - TARGET_PRETENSION_FORCE;
				my_struct_pretension->p_pret_des_next = my_struct_pretension->p_pret_des_curr - PRETENSION_POSITION_ADJUSTMENT_GAIN * f_err;

				if (my_struct_pretension->p_pret_des_next < 0) my_struct_pretension->p_pret_des_next = my_struct_pretension->p_pret_des_curr;
				
			}
			else if (my_struct_pretension->f_pret < PRETENSION_FORCE_LOWER_BOUND) {
				float f_err = TARGET_PRETENSION_FORCE - my_struct_pretension->f_pret;
				my_struct_pretension->p_pret_des_next = my_struct_pretension->p_pret_des_curr + PRETENSION_POSITION_ADJUSTMENT_GAIN * f_err;

				if (my_struct_pretension->p_pret_des_next > p_lim_) my_struct_pretension->p_pret_des_next = my_struct_pretension->p_pret_des_curr;				
			}				
		}

		if (my_struct_gyro_detection->gait_state == PARETIC_SUPPORT && my_struct_pf_controller->pull_state != PF_PUSHOUT_MODE){
			my_struct_pretension->p_pret_command = my_struct_pretension->p_pret_des_curr;
		}
		
		else if (my_struct_pf_controller->pull_state == PF_PUSHOUT_MODE || my_struct_gyro_detection->gait_state == NON_PARETIC_SUPPORT) {
			my_struct_pretension->p_pret_des_curr = my_struct_pretension->p_pret_des_next;
			my_struct_pretension->p_pret_command = my_struct_pretension->p_pret_des_curr - EXTRA_PRETENSION_PUSHOUT;
			if (my_struct_pretension->p_pret_command < 0) my_struct_pretension->p_pret_command = 0;			
		}	
	}
	else{
		my_struct_pretension->p_pret_command = my_struct_pretension->p_pret_des_curr-EXTRA_PRETENSION_PUSHOUT;			
		if (my_struct_pretension->p_pret_command < 0) my_struct_pretension->p_pret_command = 0;				
	}
	
}

void get_static_pretension_position(float f_meas_, float p_curr_, u32 t_ms_,struct struct_pretension *my_struct_pretension)
{	
	if (!my_struct_pretension->flag_initial_setting_completed && f_meas_ > TARGET_PRETENSION_FORCE){ //TARGET_PRETENSION_FORCE = 7.5 N
		my_struct_pretension->flag_initial_setting_completed = 1;
		my_struct_pretension->t_init_pretension_detected = t_ms_;
		my_struct_pretension->p_pret_command = p_curr_;
		my_struct_pretension->p_pret_des_curr = my_struct_pretension->p_pret_command;
		my_struct_pretension->p_pret_des_next = my_struct_pretension->p_pret_command;

	}
}



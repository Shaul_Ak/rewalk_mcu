#ifndef __CONTROLLER_CONFIG_H__
#define __CONTROLLER_CONFIG_H__

#include "types.h"
#include <list.h>
#include <Sensor/RWLK_Sensor_Data.h>


#define Abs(a)      (((a) <  0 ) ? -(a) : (a))
#define Min(a, b)   (((a) < (b)) ?  (a) : (b))
#define Max(a, b)   (((a) > (b)) ?  (a) : (b))
#define min(a, b)   Min(a, b)
#define max(a, b)   Max(a, b)


#ifdef __MEDEXO_ELMO_API__
#ifdef __REVC__
#define __WYSS_FW__
#endif
#include <medexo_defines.h>
#include "types.h"
#include <compiler.h>
#include <elmo.h>
#include <motor.h>
#include <motor_information.h>
#include <bluetooth.h>
#include <thermistors.h>
#include <systick.h>
#include <controller.h>
#include <fan.h>
#include "medexo_controller_bluetooth.h"
#include "medexo_controller_protocol.h"
#include "protocol.h"
#include "can_protocol.h"
/**
 * \defgroup MEDEXO_API
 */
 /** @{
 */
/** Return demanded position in mm. */
#define get_motor_demanded_position_mm(m_id) get_motor_driver_struct(&elmocmd,m_id)->elmo_inf.demanded_position_mm
/** Return actual position in mm. */
#define get_motor_actual_position_mm(m_id) get_motor_driver_struct(&elmocmd,m_id)->elmo_inf.actual_position_mm
/** Return actual velocity in mm sec. */
#define get_motor_actual_velocity_mm_sec(m_id) get_motor_driver_struct(&elmocmd,m_id)->elmo_inf.actual_velocity_mm_sec
/** Return Motor actual current. */
#define get_motor_actual_current(m_id) get_motor_driver_struct(&elmocmd,m_id)->elmo_inf.actual_current
/** Return Motor DC Bus voltage. */
#define get_motor_dc_bus_voltage(m_id) get_motor_driver_struct(&elmocmd,m_id)->elmo_inf.voltage
/** Return max position in counts. */
#define get_motor_max_position_counts(m_id) get_motor_driver_struct(&elmocmd,m_id)->command.max_position
/** Return position offset in counts. */
#define get_motor_position_offset_mm(m_id) get_motor_driver_struct(&elmocmd,m_id)->rom.position_offset_mm
/** Return direction of mouvement. */
#define get_motor_rotation_direction(m_id) get_motor_driver_struct(&elmocmd,m_id)->rom.dir
/** Move Motor to position (mm) with velocity. */
#define motor_move_motor_mm(m_id,pos,vel) single_motor_move_mm(&elmocmd,pos,vel,m_id)
/** Return PF LC. */
#define pf_lc(ctr) get_pf_lc(&ctr->can_prot,ctr->parietic_side)
/** Return DF LC. */
#define df_lc(ctr) get_df_lc(&ctr->can_prot,ctr->parietic_side)
/** Return Gyro P. */
#define gyro_p(ctr) get_gyro_p_value(&ctr->can_prot,ctr->parietic_side)
/** Return Gyro NP. */
#define gyro_np(ctr) get_gyro_np_value(&ctr->can_prot,ctr->parietic_side)
/** FLT angle P. */
#define ftf_angle_p(ctr) get_ftf_angle_p_value(&ctr->can_prot,ctr->parietic_side)
/** FLT angle NP. */
#define ftf_angle_np(ctr) get_ftf_angle_np_value(&ctr->can_prot,ctr->parietic_side)
/** Return true if handle is active. */
#define handle_active(ctr) handle_in_active_mode(&ctr->can_prot)
/** Return true if handle is ptens. */
#define handle_ptens(ctr) handle_in_ptens_mode(&ctr->can_prot)
/** Set smoothing factor. @todo will be modified as Nikos pushes the latest smoothing function */
//#define mx_set_smoothing_factor(m_id,sf) single_motor_set_smoothing_factor(&elmocmd,m_id, sf);
/** @} */

#else

#define get_motor_demanded_position_mm(m_id)    RWLK_get_motor_demanded_position_mm(m_id)
/** Return actual position in mm. */
#define get_motor_actual_position_mm(m_id)      RWLK_get_motor_actual_position_mm(m_id)
/** Return actual velocity in mm sec. */
#define get_motor_actual_velocity_mm_sec(m_id)  RWLK_get_motor_actual_velocity_mm_sec(m_id)
/** Return Motor DC Bus voltage. */
#define get_motor_dc_bus_voltage(m_id)          RWLK_get_motor_voltage(m_id)
/** Return max position in counts. */
#define get_motor_max_position_counts(m_id)     RWLK_get_motor_max_position_counts(m_id)
/** Return position offset in counts. */
#define get_motor_position_offset_mm(m_id)      RWLK_get_motor_position_offset_mm(m_id)
/** Return direction of mouvement. */
#define get_motor_rotation_direction(m_id)      RWLK_get_motor_rotation_direction(m_id)
/** Move Motor to position (mm) with velocity. */
#define motor_move_motor_mm(m_id,pos,vel)       RWLK_single_motor_move_mm(m_id,pos,vel)
/** Return PF LC. */
#define pf_lc(ctr)                              RWLK_get_sensor_pf_lc(&ctr->sensor_prot,ctr->parietic_side)
/** Return DF LC. */
#define df_lc(ctr)                              RWLK_get_sensor_df_lc(&ctr->sensor_prot,ctr->parietic_side)
/** Return Gyro P. */
#define gyro_p(ctr)                             RWLK_get_sensor_gyro_p_value(&ctr->sensor_prot,ctr->parietic_side)
/** Return Gyro NP. */
#define gyro_np(ctr)                            RWLK_get_sensor_gyro_np_value(&ctr->sensor_prot,ctr->parietic_side)
/** FLT angle P. */
#define ftf_angle_p(ctr)                        RWLK_get_sensor_ftf_angle_p_value(&ctr->sensor_prot,ctr->parietic_side)
/** FLT angle NP. */
#define ftf_angle_np(ctr)                       RWLK_get_sensor_ftf_angle_np_value(&ctr->sensor_prot,ctr->parietic_side)
/** Return true if handle is active. */
#define handle_active(ctr)                      RWLK_handle_in_active_mode(ctr)
/** Return true if handle is ptens. */
#define handle_ptens(ctr)                       RWLK_handle_in_ptens_mode(&ctr->sensor_prot)

#endif
#endif /*__CONTROLLER_CONFIG_H__*/
 

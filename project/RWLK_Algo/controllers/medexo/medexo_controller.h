#ifndef __MEDEXO_CONTROLLER_H__
#define __MEDEXO_CONTROLLER_H__

/**
 * @file medexo_controller.h
 * @author Nicolas Menard, Jaehyun Bae
 * @date 1 Oct 2015
 * @brief Contains data structure and methods for interface between low-level and controller
 *
 * 
 */
#include <controller_config.h>
#include "gyro_detection.h"
#include "position_profile.h"
#include "pre_tension.h"

#include <filter.h>
#include <Sensor/RWLK_Sensor_Data.h>
#include <Motor/RWLK_motor.h>


/**
 * @brief Paretic side enumeration.
 *
 * This is used by several functions to report what is the current afected side.
 * 
 */
enum medexo_controller_pariectic_side{
	MEDEXO_CONTROLLER_LEFT_PARIETIC = 0,  			/**< User is Left side Paretic */
	MEDEXO_CONTROLLER_RIGHT_PARIETIC, 				/**< User is Right side Paretic */
	MEDEXO_CONTROLLER_PARETIC_SIDE_NOT_SPECIFIED 	/**< Paretic side not specified (Default) */
};


/**
 * @brief Structure containing sensors inputs
 *
 * This structure is passed to several part of the controller to ensure data are consistent during same iteration
 */
struct struct_sensor_input{
	float lc_pf;
	float lc_df;
	float gyro_p;
	float gyro_np;
	float ftf_angle_p;
	float ftf_angle_np;
	float ftf_angle_p_offset;
	float ftf_angle_np_offset;	
};
/**
 * @brief Principal structure of medexo controller
 *
 * This structure contains all data member necessary for runing the medexo controller
 */
struct medexo_controller{
	struct struct_sensor_input my_struct_sensor_input; // 2017

	// From gyro_detection.h
	struct struct_gyro_detection my_struct_gyro_detection; // 2017
	struct struct_gait_param my_struct_gait_param; // 2017
	struct struct_ftf_angle_detection my_struct_ftf_angle_detection; // 2017	
	// From position_profile.h
	struct struct_position_profile my_struct_position_profile_input;
	
	struct struct_pf_controller my_struct_pf_controller;
	struct struct_df_controller my_struct_df_controller;
	// From pre_tension.h
	struct struct_pretension my_struct_pf_pretension;
	struct struct_pretension my_struct_df_pretension;
	
	
	enum medexo_controller_pariectic_side parietic_side;

	struct filter_struct lc1_fltr;
	struct filter_struct lc2_fltr;
	
	
	s8 flag_setting_initial_pretension;
	enum medexo_handle_mode handle_mode;//s8 handle_mode;
#ifdef __WYSS_FW__
	
	s32 streaming;
	enum motor_id motor_pf;
	enum motor_id motor_df;
	/*temporary bluetooth*/
	struct medexo_bt_data_packet packet;
	struct bluetooth_packet inc;
	struct protocol_msg inc_temp;
	struct protocol s_protocol;
	struct can_protocol_struct can_prot;
	u32 last_pakt_time;
#else 
	struct sensor_protocol_struct sensor_prot;
	int  motor_pf;
    int  motor_df;

#endif
};
struct bluetooth_struct;

/**
 * @defgroup EXTERN_OBJECT
 */
 /** @{
 */
/** The medexo_controller main object*/
extern struct medexo_controller med_ctr;
/** Loop execution time in ns (resolution 25ns) for streaming*/
extern u64 loop_time;
/** @} */


void init_struct_sensor_input(struct struct_sensor_input* my_struct_sensor_input);

/**
 * @defgroup MEDEXO_HIGH_LEVEL_CONTROLLER_WRAPPER_GROUP
 * Functions that calls sub-functions and excute them
 * @{
 */
 

/**
 * @brief This function update pf trajectory.
 * 
 * 
 * @param ctr A pointer to a \ref medexo_controller structure
 * @return The function doesn't return anything.
 *
 */
void medexo_controller_do_pf(struct medexo_controller *ctr, u32 t_ms);
/**
 * @brief This function update df trajectory.
 * 
 * 
 * @param ctr A pointer to a \ref medexo_controller structure
 * @return The function doesn't return anything.
 *
 */
void medexo_controller_do_df(struct medexo_controller *ctr, u32 t_ms);
/**
 * @brief This set the initial pretension for pf when an handle button is pushed.
 * 
 * 
 * @param ctr A pointer to a \ref medexo_controller structure
 * @param t_ms Time in ms
 * @return The function doesn't return anything.
 *
 */
void medexo_controller_set_init_pf_pretension(struct medexo_controller *ctr, u32 t_ms);
/**
 * @brief This set the initial pretension for df when an handle button is pushed.
 * 
 * 
 * @param ctr A pointer to a \ref medexo_controller structure
 * @param t_ms Time in ms
 * @return The function doesn't return anything.
 *
 */
void medexo_controller_set_init_df_pretension(struct medexo_controller *ctr, u32 t_ms);

/**
 * @brief Main wrapper function that runs high-level controller
 * 
 * 
 * @param ctr A pointer to a medexo_controller
 * @param t_ms Time in msec
 * @return The function doesn't return anything.
 *
 */
void medexo_controller_update(void * controller, u32 t_ms);

s32 medexo_controller_setup(void * controller);

/** @} */

#endif /*__MEDEXO_CONTROLLER_H__*/

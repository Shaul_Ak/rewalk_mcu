#ifndef __PRE_TENSION_H__
#define __PRE_TENSION_H__
/**
 * @file pre_tension.h
 * @author  Jaehyun Bae,Nicolas Menard
 * @date 1 Oct 2015
 * @brief Contains data structures and method for PF and DF trajectory generation
 *
 * 
 */
#include <controller_config.h>

/**
 * @defgroup PRETENSION_ALGORITHM_GROUP
 * Functions Constant values used for setting pretension
 * @{
 */ 
 
/** Pull velocity in initial pretension setting routine ( medexo_controller_set_init_df_pretension() / medexo_controller_set_init_pf_pretension() ) [mm/s] */
#define INITIAL_PRETENSION_SETTING_VELOCITY 15
/** Extra pushout for pf cable not to disturb wearer during swing [mm] */
#define EXTRA_PRETENSION_PUSHOUT 15 // was 20000 in count
/** Pretension adjustment gain [mm/N]. adjustment = gain*force error */
#define PRETENSION_POSITION_ADJUSTMENT_GAIN 0.3
/** pretension force upper bound (pretension force should be lower than X) [N] */
#define PRETENSION_FORCE_UPPER_BOUND 10
/** pretension force lower bound (pretension force should be bigger than X) [N] */
#define PRETENSION_FORCE_LOWER_BOUND 5
/** Target pretension force at specific point. (paretic foot lift for DF, active pull start timing for PF) [N] */
#define TARGET_PRETENSION_FORCE 7.5

/** @} */

/**
 * @addtogroup HIGH_LEVEL_CONTROLLER_STRUCTURE_GROUP
 * @{
 */ 

/**
 * @brief Structure to set up pretention position
 *
 * This structure is used for DF and PF pretension setting seperately. In other word, two different objects using this struct is made for DF and PF indipendently.
 */
 struct struct_pretension {

	float p_pret_command;				/**< Pretension position command [mm]. */
	float p_pret_des_next;  			/**< Desired Pretension position for next stride [mm]. */
	float p_pret_des_curr;				/**< Desired Pretension position in current stride [mm]. */		
	float f_pret;						/**< Pretension force measurement [N]. */
	s8 flag_initial_setting_completed;	/**< Flag if initial pretension setting is completed. */
	u32 t_init_pretension_detected;		/**< Time when initial pretension setting is completed [msec]. */
	bool    initial_pretension;				// ReWalk Flag
};

/** @} */

struct struct_position_profile;
struct struct_gyro_detection;
struct struct_gait_param;
struct struct_pf_controller;

#ifdef __cplusplus
extern "C"{
#endif

/**
 * @addtogroup HIGH_LEVEL_CONTROLLER_STRUCTURE_GROUP
 * @{
 */

/**
 * @brief Initialize struct_pretension struct.
 * 
 * @param my_struct_pretension a pointer to struct_pretension
 * @return The function doesn't return anything.
 *
 */
void init_struct_pretension(struct struct_pretension *my_struct_pretension);

 /** @} */

/**
 * @addtogroup PRETENSION_ALGORITHM_GROUP
 * @{
 */

/**
 * @brief Calculate and setup pretension cable position for PF actuation
 * 
 - Objectives:
 	-# This function takes PF loadcell measurement at the beginning of active PF pull, and update PF pretension position for next stride
 	-# This function updates struct_pretension during ACTIVE_MODE @see medexo_controller.my_struct_pf_pretension
	.
 - Inputs:
 	-#  f_meas_
 	-#  p_lim_
 	-# From struct_gyro_detection
 		-#  struct_gyro_detection.flag_is_walking
 		-#  struct_gyro_detection.gait_state
 		.
 	-# From struct_pf_controller
 		-#  struct_pf_controller.stamp_start_active_pull
 		-#  struct_pf_controller.pull_state
 		.
 	.
 - Outputs: All saved in struct_pretension
 	-#  struct_pretension.p_pret_command
 	.
 - Internal variables: All saved in struct_pretension
 	-#  struct_pretension.p_pret_des_next
 	-#  struct_pretension.p_pret_des_curr
 	-#  struct_pretension.f_pret
	 .
 . 
 * @param f_meas_ PF loadcell measurement
 * @param p_lim_ Maximum PF motor travel [mm]
 * @param struct_gyro_detection Constant pointer to struct_gyro_detection 
 * @param struct_pf_controller  Constant pointer to struct_pf_controller
 * @param struct_pf_pretension  pointer to struct_pretension
 * @return The function doesn't return anything. 
 */
 void update_struct_pf_pretension(float f_meas_, float p_lim_, 
 								  const struct struct_gyro_detection *my_struct_gyro_detection, const struct struct_pf_controller *my_struct_pf_controller, struct struct_pretension *my_struct_pretension);

/**
 * @brief Calculate and setup pretension cable position for DF actuation
 * 
 - Objectives:
 	-# This function takes DF loadcell measurement at the beginning of active DF pull(Paretic foot lift), and update DF pretension position for next stride
 	-# This function updates struct_pretension during ACTIVE_MODE. @see medexo_controller.my_struct_df_pretension
	.
 - Inputs:
 	-#  f_meas_
 	-#  p_lim_
 	-# From struct_gyro_detection
 		-#  struct_gyro_detection.flag_is_walking
 		-#  struct_gyro_detection.gait_state
 		.
 	.
 - Outputs: All saved in struct_pretension
 	-#  struct_pretension.p_pret_command
 	.
 - Internal variables: All saved in struct_pretension
 	-#  struct_pretension.p_pret_des_next
 	-#  struct_pretension.p_pret_des_curr
 	-#  struct_pretension.f_pret
	 .
 . 
 * @param f_meas_ DF loadcell measurement
 * @param p_lim_ Maximum DF motor travel [mm]
 * @param struct_gyro_detection Constant pointer to struct_gyro_detection 
 * @param struct_pf_pretension  pointer to struct_pretension
 * @return The function doesn't return anything. 
 */
void update_struct_df_pretension(float f_meas_,float p_lim_,const struct struct_gyro_detection *my_struct_gyro_detection, struct struct_pretension *my_struct_pretension);				  


/**
 * @brief Acquire pretension position when wearer is in static position with neatral ankle angle (standing straight)
 * 
 - Objectives:
 	-# This function reads loadcell measurement when wearer is standing straight
 	-# This function throws confirmation stamp of initial pretension setup.
	.
 - Inputs:
 	-#  f_meas_
 	-#  p_curr_
 	-# t_ms_
 	.
 - Outputs: All saved in my_struct_pretension
 	-# struct_pretension.p_pret_command
 	-# struct_pretension.flag_initial_setting_completed
 	-# struct_pretension.p_pret_des_curr
 	-# struct_pretension.p_pret_des_next
 	.
 - Internal variables: All saved in struct_pretension
 	-#  struct_pretension.t_init_pretension_detected
 	.
 . 
 * @param f_meas_  loadcell measurement
 * @param p_curr_ Current motor position [mm]
 * @param t_ms_ time [msec]
 * @param struct_pf_pretension  pointer to struct_pretension
 * @return The function doesn't return anything. 
 */
void get_static_pretension_position(float f_meas_, float p_curr_, u32 t_ms_,struct struct_pretension *my_struct_pretension);


/** @} */


#ifdef __cplusplus
}
#endif
#endif /*__PRE_TENSION_H__*/

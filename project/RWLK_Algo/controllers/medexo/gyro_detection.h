#ifndef __GYRO_DETECTION_H__
#define __GYRO_DETECTION_H__
/**
 * @file gyro_detection.h
 * @author Jaehyun Bae, Nicolas Menard
 * @date 6 Mar 2017
 * @brief Contains data structure and methods for the gait detection algorithm.
 */
#include <controller_config.h>

/**
 * @defgroup GAIT_DETECTION_GROUP 
 * Functions and constants used for gait detection and gait parameter calculation. 
 * @{
 */

 /** Represent an infinite value for gyro/force measurement */
#define INFINITY_VALUE 10000
/** Default Stride time [msec]. */
#define DEFAULT_STRIDE_TIME 1200
/** Minimum peak detection threshold for foot lift detection [rad/sec] */
#define MINIMUM_GYRO_FL_DETECTION_THRESHOLD 0.5
/** Threshold to confirm gyro signal is negative. have some offset under 0 to ensure gyro signal is really negative [rad/sec] */
#define GYRO_NEGATIVE_CONFIRMATION_THRESHOLD -0.2
/** Array size containing gyro measurement @see struct_gyro_detection.gyros_at_npfl @see struct_gyro_detection.gyros_at_pfl*/
#define GYRO_ARRAY_SIZE 3
/** Array size containing temporal variables @see struct_gait_param.stride_times @see struct_gait_param.p_support_times, @see struct_gait_param.np_support_times.*/
#define GAIT_PARAM_ARRAY_SIZE 3
/** Conversion rate from average peak gyro measurement to peak detection threshold. */
#define PEAK_DETECTION_THRESHOLD_RATE 0.3
/** Conversion rate from estimated stride time to stop confirmation time duration. */
#define STOP_CONFIRM_TIME_2_STRIDE_TIME 2.5
/** Drop rate for peak gyro signal confirmation. Algorithm confirms gyro signal peak once current signal drops to X% of peak signal */
#define DROP_RATE_4_GYRO_PEAK_CONFIRMATION 0.6
/** Rise rate for mid swing confirmation. Algorithm confirms mid-swing when ftf angle rises X% from minimum val */
#define RISE_RATE_4_MID_SWING_CONFIRMATION 0.5
/** Minimum number of strides for updating gait param. update_struct_gait_param() updates gait params only when stride count is bigger than this */
#define MIN_PFL_COUNT_FOR_GAIT_PARAM_UPDATE 2


/**
 * @brief gait_state enumeration
 * 
 * Gait state identified through detect_foot_lift_off(). @see struct_gyro_detection.gait_state.
 */
enum gait_state_enum{
	PARETIC_SUPPORT = 1, 			/**< Paretic support (paretic single support+paretic double support).*/
	NON_PARETIC_SUPPORT = 2			/**< Non-Paretic support (non-paretic single support+non- paretic double support).*/
};

/**
 * @brief weight vector enumeration.
 * 
 * weight vector for weighted average. @see struct_gyro_detection.weight_vector @see struct_gait_param.weight_vector
 * 
 */
enum weight_vector_enum{
	W1 = 1,
	W2 = 2,
	W3 = 3
};

/** @} */

/**
 * @defgroup HIGH_LEVEL_CONTROLLER_INPUT_GROUP 
 * Group of inputs from GUI or handle.
 * @{
 */

/**
 * @brief handle mode enumeration.
 * 
 * Handle mode sourced from handle. @see medexo_controller.handle_mode.
 * 
 */
enum medexo_handle_mode{	
	INACTIVE_MODE = 0,
	PRETENSION_SETTING_MODE = 1,
	ACTIVE_MODE = 2
};

/** @} */

/**
 * @defgroup HIGH_LEVEL_CONTROLLER_STRUCTURE_GROUP
 * Group of structures and their initialization functions used in high-level controller
 * @{
 */ 

/**
 * @brief Gyro Detection structure.
 *
 * Hold all the information and status of the gait dectection algorithm. @see detect_foot_lift_off()
 */
struct struct_gyro_detection {

	s8 stamp_confirmed_npfl;					/**< Non-paretic foot lift confirmation stamp */
	s8 stamp_confirmed_pfl;						/**< Paretic foot lift confirmation stamp  */
	u32 time_at_last_npfl;						/**< Time when detected last non-paretic foot lift  */
	u32 time_at_last_pfl;						/**< Time when detected last non-paretic foot lift */
	u32 time_at_last_pfc; 						/** Time when detected paretic foot contact */
	u32 time_at_last_npfc; 						/** TIme when detected paretic foot contact */	
	u32 time_at_second_last_pfl;				/**< Time when detected second last paretic foot lift */
	u32 time_after_last_pfl;					/**< Time elapsed after last paretic foot lift */
	u32 time_after_last_npfl;					/**< Time elapsed after last non-paretic foot lift */
	u32 time_stop_confirmation_threshold;		/**< Time threshold to confirm wearer stopped */
	u32 time_at_last_stop;						/**< Time when last wearer's stop is confirmed */
	u32 time_at_pfl_candidate;					/**< Time when a candidate for pfl is detected. Internal variable of detect_foot_lift_off() */
	u32 time_at_npfl_candidate;					/**< Time when a candidate for npfl is detectes. Internal variable of detect_foot_lift_off() */	
	u32 time_at_pfc_candidate; 					/** Time when detected a candidate for paretic foot contact. Internal variable of get_time_at_foot_contact() */
	u32 time_at_npfc_candidate; 				/** Time when detected a candidate for non-paretic foot contact. Internal variable of get_time_at_foot_contact() */	

	float gyro_npfl_detection_threshold;		/**< Threshold for peak gyro signal detection at non-paretic foot lift */
	float gyro_pfl_detection_threshold;			/**< Threshold for peak gyro signal detection at paretic foot lift*/	
	float gyro_max_np;							/**< Maximum non-paretic gyro signal. Internal variable of detect_foot_lift_off() */
	float gyros_at_npfl[GYRO_ARRAY_SIZE];		/**< Array to save peak gyro signal at non-paretic foot lift off. Internal variable of detect_foot_lift_off() */
	float gyro_max_p;							/**< Maximum paretic gyro signal. Internal variable of detect_foot_lift_off() */
	float gyros_at_pfl[GYRO_ARRAY_SIZE];		/**< Array to save peak gyro signal at paretic foot lift off. Internal variable of detect_foot_lift_off() */
	float gyro_min_np;							/**< Minimum non-paretic gyro signal. Internal variable of detect_foot_lift_off() */
	float gyro_min_p;							/**< Minimum paretic gyro signal. Internal variable of detect_foot_lift_off() */
	float gyro_pfc_candidate; 					/** Gyroscope signal when a candidate for paretic foot contact was dectected. Internal variable of get_time_at_foot_contact() */
	float gyro_npfc_candidate;					/** Gyroscope signal when a candidate for non-paretic foot contact was dectected. Internal variable of get_time_at_foot_contact() */
	float weight_vector[GYRO_ARRAY_SIZE];		/**< Weight vector to calculate weighted average. Internal variable of detect_foot_lift_off() @see weight_vector_enum */

	s8 flag_is_walking;							/**< Flag if wearer is walking in #ACTIVE_MODE */
	s8 gait_state;								/**< Gait state (#PARETIC_SUPPORT or #NON_PARETIC_SUPPORT)*/
	u32 count_pfl_total;						/**< Total number of detected paretic foot lift after system is on (equivalent to total stride count)*/
	u32 count_pfl_after_last_stop;				/**< number of detected paretic foot lift after wearer stops walking (equivalent to stride count after stop) */
	s8 flag_searching_peak;						/**< Flag if algorithm is looking for gyro peak signal. Internal variable of detect_foot_lift_off() */
	
};

/**
 * @brief Foot to floor angle dectection.
 *
 * Hold all the information and status of the foot to floor detection algorithm.
 */
struct struct_ftf_angle_detection {
	s8 stamp_confirmed_np_mid_swing;			/**< stamp when non-paretic mid swing is confirmed */
	s8 stamp_confirmed_p_mid_swing;				/**< stamp when paretic mid swing is confirmed */
	
	float ftf_ang_min_np;						/**< Minimum non-paretic ftf angle */
	float ftf_ang_min_p;						/**< Minimum paretic ftf angle */
	float ftf_ang_max_np;						/**< Maximum non-paretic ftf angle */
	float ftf_ang_max_p;						/**< Maximum paretic ftf angle */
	
	s8 flag_searching_mid_swing_np;				/**< flag if algorithm is searching for non-paretic mid swing */
	s8 flag_searching_mid_swing_p;				/**< flag if algorithm is searching forparetic mid swing */
};

/**
 * @brief Gait param structure.
 *
 * Hold all gait parameters. @see update_struct_gait_param
 */
struct struct_gait_param {
	
	float stride_times[GAIT_PARAM_ARRAY_SIZE];				/**< Array to save previous stride times. */
	
	float p_support_time;									/**< paretic support time (single support + double support) */
	float np_support_time;									/**< non-paretic support time (single support + double support) */

	float p_double_support_time;							/**< paretic double support time */
	float p_single_support_time;							/**< paretic single support time */
	float np_double_support_time;							/**< non-paretic double support time */
	float np_single_support_time;							/**< non-paretic single support time */
	float p_step_time;										/**< Paretic step time (from paretic foot contact until non-paretic foot contact) */ 
	float np_step_time; 									/**< non-paretic step time (from non-paretic foot contact until paretic foot contact) */ 
};

/** @} */

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @addtogroup HIGH_LEVEL_CONTROLLER_STRUCTURE_GROUP
 * @{
 */

/**
 * @brief Initialize struct_gyro_detection struct.
 *
 * @param my_struct_gyro_detection a pointer to struct_gyro_detection.
 * @return The function doesn't return anything.
 */
void init_struct_gyro_detection(struct struct_gyro_detection* my_struct_gyro_detection);

/**
 * @brief Initialize struct_ftf_angle_detection struct.

 * @param my_struct_ftf_angle_detection a pointer to a struct_ftf_angle_detection.
 * @return The function doesn't return anything.
 */
void init_struct_ftf_angle_detection(struct struct_ftf_angle_detection* my_struct_ftf_angle_detection);

/**
 * @brief Initialize struct_gait_param struct.
 *
 * @param my_struct_gait_param a pointer to a struct_gait_param. 
 * @return The function doesn't return anything.
 */
void init_struct_gait_param(struct struct_gait_param* my_struct_gait_param);

/** @} */

/**
 * @addtogroup GAIT_DETECTION_GROUP
 * @{
 */

/**
 * @brief Dectect foot lift-off (i.e. toe off) from paretic and segment gait cycle
 *  
 - Objectives:
 	-# Detect foot lift-off (i.e. toe-off) based on positive peak signal from gyroscope measurement.
 	-# Identify paretic/non-paretic support phase in walking.
 	-# Identify whether wearer is walking or not.
 	.
 - Inputs:
 	-#  gyro_np_meas_
 	-#  gyro_p_meas_
 	-#  time_
 	.
 - Outputs: all saved in struct_gyro_detection
 	-#  struct_gyro_detection.stamp_confirmed_pfl &  struct_gyro_detection.stamp_confirmed_npfl
 	-#  struct_gyro_detection.time_at_last_pfl &  struct_gyro_detection.time_at_last_npfl
 	-#  struct_gyro_detection.time_after_last_pfl &  struct_gyro_detection.time_after_last_npfl
 	-#  struct_gyro_detection.gait_state
 	-#  struct_gyro_detection.count_pfl_after_last_stop
 	-#  struct_gyro_detection.count_pfl_total
 	-#  struct_gyro_detection.flag_is_walking
 	.
 - Internal variables:  all saved in struct_gyro_detection. Used only in detect_foot_lift_off().
 	-#  struct_gyro_detection.flag_searching_peak
 	-#  struct_gyro_detection.gyro_max_p &  struct_gyro_detection.gyro_max_p
 	-#  struct_gyro_detection.gyro_min_p &  struct_gyro_detection.gyro_min_p
 	-#  struct_gyro_detection.time_at_pfl_candidate &  struct_gyro_detection.time_at_npfl_candidate
 	-#  struct_gyro_detection.gyro_pfl_detection_threshold &  struct_gyro_detection.gyro_npfl_detection_threshold
 	-#  struct_gyro_detection.gyros_at_pfl &  struct_gyro_detection.gyros_at_npfl
 	-#  struct_gyro_detection.weight_vector
 	.
 .
 * @param gyro_np_meas_ Function input. Sagittal plane foot gyroscope measurement from non-paretic side (rad/s)
 * @param gyro_p_meas_ Function input. Sagittal plane foot gyroscope measurement from non-paretic side (rad/s)
 * @param time_ Input. Function input. Current time (msec)
 * @param my_struct_gyro_detection a pointer to struct_gyro_detection which saves all output/internal variables.
 * @return The function doesn't return anything.
 */
void detect_foot_lift_off(float gyro_np_meas_, float gyro_p_meas_, u32 time_, struct struct_gyro_detection* my_struct_gyro_detection);

/**
 * @brief Dectect mid swing based on foot to floor angle from Paretic and Non-paretic side. 
 *
 - Objective: detect mid swing from paretic/non-paretic side
 - Inputs:
 	-#  ftf_ang_np_
 	-#  ftf_ang_np_ 	
	-#	Outputs of detect_foot_lift_off() saved in struct_gyro_detection.
		-# struct_gyro_detection.gait_state
 		.
 	.
 - Outputs: all saved in struct_ftf_angle_detection
 	-#  struct_ftf_angle_detection.stamp_confirmed_np_mid_swing &  struct_ftf_angle_detection.stamp_confirmed_p_mid_swing
 	.
 - Internal variables: all saved in struct_ftf_angle_detection. Used only in detect_mid_swing()
 	-#  struct_ftf_angle_detection.flag_searching_mid_swing_np &  struct_ftf_angle_detection.flag_searching_mid_swing_p
 	-#  struct_ftf_angle_detection.ftf_ang_min_p &  struct_ftf_angle_detection.ftf_ang_min_np 
 	.
 .
 * @param ftf_ang_np_meas_ Function input. foot to floor angle measurement from non-paretic side (deg)
 * @param ftf_ang_p_meas_ Funtion input. foot to floor angle measurement from paretic side (deg)
 * @param my_struct_gyro_detection Function input. a constant pointer to struct_gyro_detection.
 * @param my_struct_ftf_angle_detection a pointer to struct_ftf_angle_detection which saves all output/internal variables.
 * @return The function doesn't return anything.
 */
void detect_mid_swing(s8 handle_mode_, float ftf_ang_np_meas_, float ftf_ang_p_meas_, const struct struct_gyro_detection* my_struct_gyro_detection, struct struct_ftf_angle_detection* my_struct_ftf_angle_detection);


/**
 * @brief Get time stamp at paretic/non-paretic foot contact (i.e heel strike).
 *
 - Objective: Get time when paretic/non-paretic foot contact is detected
 - Inputs:
 	-#  gyro_np_
 	-#  gyro_p_
 	-#  time_
 	-# 	Outputs of detect_foot_lift_off() saved in struct_gyro_detection
 		-# struct_gyro_detection.gait_state
 		-# struct_gyro_detection.stamp_confirmed_pfl & struct_gyro_detection.stamp_confirmed_npfl
 		.
 	.
 - Outputs: all saved in struct_gyro_detection
 	-# struct_gyro_detection.time_at_last_npfc &  struct_gyro_detection.time_at_last_pfc
 	.
 - Internal variables: all saved in struct_gyro_detection. Used only in get_time_at_foot_contact()
 	-#  struct_gyro_detection.gyro_pfc_candidate &  struct_gyro_detection.gyro_pfc_candidate
 	-#  struct_gyro_detection.time_at_pfc_candidate &  struct_ftf_angle_detection.time_at_pfc_candidate 
 	.
 .
 * @param gyro_np_meas_ Function input. Sagittal plane foot gyroscope measurement from non-paretic side (rad/s)
 * @param gyro_p_meas_ Function input. Sagittal plane foot gyroscope measurement from non-paretic side (rad/s)
 * @param time_ Input. Function input. Current time (msec)
 * @param my_struct_gyro_detection a pointer to struct_gyro_detection which saves all output/internal variables.
 * @return The function doesn't return anything.
 */
void get_time_at_foot_contact(float gyro_np_, float gyro_p_, u32 time_, struct struct_gyro_detection* my_struct_gyro_detection);

/**
 * @brief Calculate gait variables such as stride time, paretic/non-paretic support time and gait percentage based on outputs from gait detection algorithm.
 *  
 - Objectives:
 	-# This function update  struct_gait_param.
 	.
 - Inputs:
 	-#  handle_mode_
 	-#  struct_gyro_detection
 	.
 - Outputs: Saved in struct_gyro_detection and struct_gait_param
 	-# In struct_gyro_detection
 		-#  struct_gyro_detection.time_stop_confirmation_threshold
 		.
 	-# In struct_gait_param
 		-#	struct_gait_param.p_double_support_time & struct_gait_param.np_double_support_time
 		-#	struct_gait_param.p_single_support_time & struct_gait_param.np_single_support_time
 		-# struct_gait_param.p_support_time & struct_gait_param.np_support_time
 		-# struct_gait_param.p_step_time & struct_gait_param.np_step_time
 		.
		 
 - Internal variables: All saved in struct_gait_param.
 	-# struct_gait_param.stride_times 	
 	-# struct_gait_param.weight_vector @ref weight_vector_enum
 	.
 .
 * @param handle_mode_ handle mode. See \ref HANDLE_MODES
 * @param my_struct_gyro_detection a constant pointer to a struct_gyro_detection.
 * @param my_struct_gait_param a pointer to a my_struct_gait_param.
 * @return The function doesn't return anything.
 *
 */
void update_struct_gait_param(s8 handle_mode_, struct struct_gyro_detection* my_struct_gyro_detection, struct struct_gait_param* my_struct_gait_param);

/** @} */

/**
 * @defgroup HELPER_GROUP
 * Group of helper functions and constants in high-level controller
 * @{
 */

/**
 * @brief Push data, and remove the oldest data element in the array.
 *
 - Objectives:
 	-# This function is equivalent to push function of queue class.
 	.
 - Inputs:
 	-#  arr
 	-#  d
 	-#  arr_size
 	.
 - Outputs: update  arr_size
 .	
 * @param arr float array containing elems to average
 * @param d float data to be pushed to arr
 * @param arr_size size of array
 * @return The function doesn't return anything.
 */
void push_data_in_array(float arr[], float d, int arr_size);

/**
 * @brief Calculate weighted average based on array data
 *
 - Objectives:
 	-# Calculate weight average
 	.
 - Inputs:
 	-#  arr
 	-#  arr_size
 	.
 - Outputs: average of arr
 .
 *	
 * @param arr float array containing elems to average
 * @param arr_size size of array
 * @return Average of arr
 */
float weighted_average(float arr[], float weight[], int arr_size);

/**
 * @brief Calculate average of elements in an array data
 *
 - Objectives:
 	-# Calculate average value
 	.
 - Inputs:
 	-#  arr
 	-#  weight
 	-#  arr_size
 	.
 - Outputs: weighted average of arr
 .
 *	
 * @param arr float array to update.
 * @param weight weight vector in float
 * @param arr_size size of array
 * @return weighted average of arr
 */
float average(float arr[], int arr_size);

/**
 * @brief switch all elements in the array with its first element
 *
 - Objectives:
 	-# This function switches all elemtns in the array with its first element
 	.
 - Inputs:
 	-#  arr
 	-#  arr_size
 	.
 - Outputs: update  arr_size
 .	
 * @param arr float array to update.
 * @param arr_size size of array
 * @return The function doesn't return anything.
 */
void reset_array_with_first_elem(float arr[], int arr_size);


/** @} */




#ifdef __cplusplus
}
#endif


#endif /*__GYRO_DETECTION_H__*/

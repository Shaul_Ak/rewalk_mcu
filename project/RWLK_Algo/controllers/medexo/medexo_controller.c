#include "medexo_controller.h"


#define DEBUG 1


struct medexo_controller med_ctr;


void init_struct_sensor_input(struct struct_sensor_input* my_struct_sensor_input)
{
	my_struct_sensor_input->lc_pf = 0;
	my_struct_sensor_input->lc_df = 0;
	my_struct_sensor_input->gyro_p = 0;
	my_struct_sensor_input->gyro_np = 0;
	my_struct_sensor_input->ftf_angle_p = 0;
	my_struct_sensor_input->ftf_angle_np = 0;
	my_struct_sensor_input->ftf_angle_p_offset = 0;
	my_struct_sensor_input->ftf_angle_np_offset = 0;
}

s32 medexo_controller_setup(void * controller)
{
	struct medexo_controller *ctr = (struct medexo_controller*)controller;
	
	init_struct_sensor_input(&ctr->my_struct_sensor_input);
	
	/** Initialize high-level controller structures */	
	init_struct_gyro_detection(&ctr->my_struct_gyro_detection);
	init_struct_gait_param(&ctr->my_struct_gait_param);
	init_struct_ftf_angle_detection(&ctr->my_struct_ftf_angle_detection);
	init_struct_position_profile(&ctr->my_struct_position_profile_input);	
	init_struct_pf_controller(&ctr->my_struct_pf_controller);
	init_struct_df_controller(&ctr->my_struct_df_controller);
	init_struct_pretension(&ctr->my_struct_pf_pretension);
	init_struct_pretension(&ctr->my_struct_df_pretension);	
	init_filter_struct(&ctr->lc1_fltr,BUTTERWORTH_FILTER,1000,50);
	init_filter_struct(&ctr->lc2_fltr,BUTTERWORTH_FILTER,1000,50);
	ctr->parietic_side = MEDEXO_CONTROLLER_PARETIC_SIDE_NOT_SPECIFIED;
	ctr->handle_mode = INACTIVE_MODE;

#ifdef __WYSS_FW__
	init_protocol(&ctr->s_protocol,1);
	init_can_protocol_struct(&ctr->can_prot);
	medexo_controller_can_external_setup(ctr);
	medexo_controller_register_tweak_param(ctr);
	fan_set_speed(0);
	register_header(&bt_struct,BT_PACKET_START,BT_PACKET_END,4,&medexo_inc_bluetooth_data_cb);
#endif
	//ctr->motor_pf = MOTOR_ID_2;
	//ctr->motor_df = MOTOR_ID_1;
	ctr->motor_pf = MOTOR_ID_1;
	ctr->motor_df = MOTOR_ID_2;
	return 0;
}

void medexo_controller_do_pf(struct medexo_controller *ctr, u32 t_ms)
{	
		
	/** @todo. get max_position in mm. Write get_motor_max_position_mm function */
	update_struct_pf_pretension(ctr->my_struct_sensor_input.lc_pf, COUNT_2_MM * get_motor_max_position_counts(ctr->motor_pf),
								&ctr->my_struct_gyro_detection, &ctr->my_struct_pf_controller, &ctr->my_struct_pf_pretension);	
	
	float pull_curr = abs(get_motor_demanded_position_mm(ctr->motor_pf) -
						  get_motor_rotation_direction(ctr->motor_pf)*(ctr->my_struct_pf_pretension.p_pret_command
																	   + get_motor_position_offset_mm(ctr->motor_pf)));
	
	update_struct_pf_controller(ctr->my_struct_sensor_input.lc_pf, t_ms, pull_curr,
								COUNT_2_MM * get_motor_max_position_counts(ctr->motor_pf),
								&ctr->my_struct_gyro_detection, &ctr->my_struct_ftf_angle_detection,
								&ctr->my_struct_pf_pretension, &ctr->my_struct_position_profile_input, &ctr->my_struct_pf_controller);	
		
	float vel_curr = ctr->my_struct_pf_controller.v_command;
	float pos_curr =  get_motor_rotation_direction(ctr->motor_pf) * (ctr->my_struct_pf_pretension.p_pret_command
																	 + ctr->my_struct_pf_controller.p_pull_command);				

	motor_move_motor_mm(ctr->motor_pf,pos_curr,vel_curr);
}

void medexo_controller_do_df(struct medexo_controller *ctr, u32 t_ms)
{
	
	update_struct_df_pretension(ctr->my_struct_sensor_input.lc_df, COUNT_2_MM * get_motor_max_position_counts(ctr->motor_df),
								&ctr->my_struct_gyro_detection, &ctr->my_struct_df_pretension);	
								
	update_struct_df_controller(t_ms, COUNT_2_MM * get_motor_max_position_counts(ctr->motor_df),
								&ctr->my_struct_gyro_detection, &ctr->my_struct_df_pretension, &ctr->my_struct_position_profile_input, &ctr->my_struct_df_controller);

	float vel_curr = ctr->my_struct_df_controller.v_command;
	float pos_curr = get_motor_rotation_direction(ctr->motor_df) * (ctr->my_struct_df_pretension.p_pret_command
																	+ ctr->my_struct_df_controller.p_pull_command);		
	
    motor_move_motor_mm(ctr->motor_df,pos_curr,vel_curr);
}



void medexo_controller_set_init_pf_pretension(struct medexo_controller *ctr, u32 t_ms)
{

	float p_curr = abs(get_motor_demanded_position_mm(ctr->motor_pf)
															  - get_motor_rotation_direction(ctr->motor_pf)*get_motor_position_offset_mm(ctr->motor_pf));			
	
	get_static_pretension_position(ctr->my_struct_sensor_input.lc_pf, p_curr, t_ms, &ctr->my_struct_pf_pretension);
		
	float pos_curr;
	float vel_curr;
		
	if(ctr->my_struct_pf_pretension.flag_initial_setting_completed){

		pos_curr = ctr->my_struct_pf_pretension.p_pret_command;
		vel_curr = SLACK_VELOCITY;	
	}
	else{
		pos_curr = COUNT_2_MM * get_motor_rotation_direction(ctr->motor_pf)*get_motor_max_position_counts(ctr->motor_pf);
		vel_curr = INITIAL_PRETENSION_SETTING_VELOCITY;
		
		if (ctr->my_struct_sensor_input.ftf_angle_np_offset == 0) ctr->my_struct_sensor_input.ftf_angle_np_offset =  ftf_angle_np(ctr);
		if (ctr->my_struct_sensor_input.ftf_angle_p_offset == 0) ctr->my_struct_sensor_input.ftf_angle_p_offset =  ftf_angle_p(ctr);		
	}	
	if(ctr->my_struct_pf_pretension.initial_pretension || ctr->my_struct_pf_pretension.flag_initial_setting_completed)
	{
		//pToMoveVarStructsArray[X_Axis]->Pretension = !pToMoveVarStructsArray[X_Axis]->Pretension;
		motor_move_motor_mm(ctr->motor_pf,pos_curr,vel_curr);
		ctr->my_struct_pf_pretension.initial_pretension = false;
	//	pToMoveVarStructsArray[X_Axis]->Pretension = false;
	}
}

void medexo_controller_set_init_df_pretension(struct medexo_controller *ctr, u32 t_ms)
{

	float p_curr = abs(get_motor_demanded_position_mm(ctr->motor_df)
															  - get_motor_rotation_direction(ctr->motor_df)*get_motor_position_offset_mm(ctr->motor_df));			
	
	get_static_pretension_position(ctr->my_struct_sensor_input.lc_df, p_curr, t_ms, &ctr->my_struct_df_pretension);	


	float pos_curr;
	float vel_curr;
	
	if(ctr->my_struct_df_pretension.flag_initial_setting_completed){

		pos_curr = ctr->my_struct_df_pretension.p_pret_command;
		vel_curr = SLACK_VELOCITY;										
	}
	else{
		pos_curr = COUNT_2_MM * get_motor_rotation_direction(ctr->motor_df)*get_motor_max_position_counts(ctr->motor_df);
		vel_curr = INITIAL_PRETENSION_SETTING_VELOCITY;
	}	

	if(ctr->my_struct_df_pretension.initial_pretension || ctr->my_struct_df_pretension.flag_initial_setting_completed)
	{
		//pToMoveVarStructsArray[Y_Axis]->Pretension = !pToMoveVarStructsArray[Y_Axis]->Pretension;
		motor_move_motor_mm(ctr->motor_df,pos_curr,vel_curr);
		ctr->my_struct_df_pretension.initial_pretension = false;
		//pToMoveVarStructsArray[Y_Axis]->Pretension = false;
	}
}


void medexo_controller_update(void * controller, u32 t_ms)
{
	struct medexo_controller *ctr = (struct medexo_controller*)controller;
	
	// Get handle mode	
/*	if(handle_active(ctr))
		ctr->handle_mode = ACTIVE_MODE;					
	else if(handle_ptens(ctr))
		ctr->handle_mode = PRETENSION_SETTING_MODE;			
	else
		ctr->handle_mode = INACTIVE_MODE;*/
	
	// Get sensor inputs
	ctr->my_struct_sensor_input.lc_pf = filter_value(&ctr->lc1_fltr,pf_lc(ctr)/1000);
	ctr->my_struct_sensor_input.lc_df = filter_value(&ctr->lc2_fltr,df_lc(ctr)/1000);
	ctr->my_struct_sensor_input.gyro_np = gyro_np(ctr); 	
	ctr->my_struct_sensor_input.gyro_p = gyro_p(ctr);
	ctr->my_struct_sensor_input.ftf_angle_np = ftf_angle_np(ctr) - ctr->my_struct_sensor_input.ftf_angle_np_offset;
	ctr->my_struct_sensor_input.ftf_angle_p = ftf_angle_p(ctr) - ctr->my_struct_sensor_input.ftf_angle_p_offset; 

#ifdef __WYSS_FW__  
	do_action_protocol(&ctr->s_protocol);
#endif

	if (ctr->parietic_side == MEDEXO_CONTROLLER_PARETIC_SIDE_NOT_SPECIFIED){

#ifdef __WYSS_FW__  
				fan_set_speed(0);
#endif	
				motor_move_motor_mm(ctr->motor_df,0,SLACK_VELOCITY);
				motor_move_motor_mm(ctr->motor_pf,0,SLACK_VELOCITY);	
	}
	else{

		// Run initial pretension routine in pretension setting mode	
		if (ctr->handle_mode == PRETENSION_SETTING_MODE){
#ifdef __WYSS_FW__  
			fan_set_speed(50);
#endif
			medexo_controller_set_init_pf_pretension(ctr, t_ms);		
			medexo_controller_set_init_df_pretension(ctr, t_ms);		
		}
		// If not (either active mode or inactive mode, check handle, and if handle is in active mode, and execute controller accordingly.
		else{		
			detect_foot_lift_off(ctr->my_struct_sensor_input.gyro_np, ctr->my_struct_sensor_input.gyro_p,t_ms, &ctr->my_struct_gyro_detection);	
			get_time_at_foot_contact(ctr->my_struct_sensor_input.gyro_np, ctr->my_struct_sensor_input.gyro_p, t_ms, &ctr->my_struct_gyro_detection);
			update_struct_gait_param(ctr->handle_mode, &ctr->my_struct_gyro_detection, &ctr->my_struct_gait_param);

			// Detect mid swing to determine the onset timing of pf actuation
			detect_mid_swing(ctr->handle_mode, ctr->my_struct_sensor_input.ftf_angle_np, ctr->my_struct_sensor_input.ftf_angle_p, &ctr->my_struct_gyro_detection, &ctr->my_struct_ftf_angle_detection);	
			
			if(ctr->handle_mode == ACTIVE_MODE && ctr->my_struct_df_pretension.flag_initial_setting_completed && ctr->my_struct_pf_pretension.flag_initial_setting_completed){
				
#ifdef __WYSS_FW__  
				fan_set_speed(100);			
#endif
				if(ctr->my_struct_position_profile_input.flag_pf_active)
					medexo_controller_do_pf(ctr, t_ms);
				else
					motor_move_motor_mm(ctr->motor_pf,0,SLACK_VELOCITY);		

				if(ctr->my_struct_position_profile_input.flag_df_active)				
					medexo_controller_do_df(ctr, t_ms);			
				else
					motor_move_motor_mm(ctr->motor_df,0,SLACK_VELOCITY);	
			}
			else{
#ifdef __WYSS_FW__  
				fan_set_speed(0);
#endif	
				reset_internal_vals_in_pf_controller(&ctr->my_struct_pf_controller);
				motor_move_motor_mm(ctr->motor_df,0,SLACK_VELOCITY);
				motor_move_motor_mm(ctr->motor_pf,0,SLACK_VELOCITY);
			}		
		}
	}
}

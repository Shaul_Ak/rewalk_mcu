#ifndef __POSITION_PROFILE__
#define __POSITION_PROFILE__
/**
 * @file position_profile.h
 * @author  Jaehyun Bae, Nicolas Menard
 * @date 22 Mar 2017
 * @brief Contains data structures and method for PF and DF trajectory generation
 *
 * 
 */
#include <controller_config.h>

/**
 * @defgroup TRAJECTORY_GENERATION_ALGORITHM_GROUP
 * functions and constant used for position and velocity trajectory
 * @{
 */

 /** Motor velocity in slack mode [mm/sec] */
#define SLACK_VELOCITY 200
/** Mininum motor velocity [mm/sec] */
#define MIN_VELOCITY 50
/** Maximum motor velocity [mm/sec] */
#define MAX_VELOCITY 540
/** Fast gain to adapt constant pull velocity in PF_CONSTANT_PULL_VELOCITY_MODE. [mm/sec/N]*/
#define CONSTANT_VELOCITY_FAST_ADJUSTMENT_GAIN 0.3
/** Normal gain to adapt constant pull velocity in PF_CONSTANT_PULL_VELOCITY_MODE. [mm/sec/N]*/
#define CONSTANT_VELOCITY_DEFAULT_ADJUSTMENT_GAIN 0.1
/** Rise gain for realtime pull velocity adaptation in PF_ADAPTIVE_PULL_VELOCITY_MODE. [mm/sec/N]*/
#define ADAPTIVE_VELOCITY_RISE_GAIN 0.01
/** Reduction gain for realtime pull velocity adaptation in PF_ADAPTIVE_PULL_VELOCITY_MODE. [mm/sec/N]*/
#define ADAPTIVE_VELOCITY_REDUCTION_GAIN 0.05
/** Force cutoff threshold [msec] */
#define MAXIMUM_HIGH_FORCE_WARNING_DURATION 30
/** Minimum level of peak pf force [N] */
#define MINIMUN_PEAK_FORCE 20
/** Conversion ratio from pull velocity to pushout velocity. v_pushout = conversion_ratio * v_pull */
#define PULL_2_PUSHOUT_VELOCITY 1.3
/** Conversion ratio from pull velocity to pretension velocity. v_pretension = conversion_ratio * v_pull */
#define PULL_2_PRETENSION_VELOCITY 1
/** PF peak force measurement array size */
#define PEAK_FORCE_ARRAY_SIZE 3

/** @} */

/**
 * @addtogroup HELPER_GROUP
 * @{
 */

/** Convert ms to s */
#define MILLISEC_2_SEC 0.001
#ifdef __WYSS_FW__
/** Convert count to mm */
#define COUNT_2_MM 0.0006768472 // change ReWalk to MM_2_COUNT
#else
#define COUNT_2_MM 		0.000248709 // MM per Counts
#define COUNTS_PER_MM   4020.7564  //Count per MM
#define MM_PER_COUNTS   0.000248709 //MM per Counts
#define Pulley_Diameter  57.0
#define NumberOfBinding	 2
#endif

/** @} */

/**
 * @addtogroup TRAJECTORY_GENERATION_ALGORITHM_GROUP
 * @{
 */

/**
 * @brief pf trajectory mode enumeration.
 *
 * Trajectory mode enumeration. See struct_pf_controller.pull_state in @ref TRAJECTORY_GENERATION_ALGORITHM_GROUP
 */
enum pf_trajectory_mode{
	PF_PRETENSION_MODE = 0, 						/**< PF idle mode during active pull in paretic support. */
	PF_CONSTANT_PULL_VELOCITY_MODE = 1, 			/**< PF constant pull velocity mode during active pull in paretic support.*/
	PF_ADAPTIVE_PULL_VELOCITY_MODE = 2, 			/**< PF adaptive pull velocity mode during active pull in paretic support. */
	PF_PUSHOUT_MODE = 3								/**< PF pushout mode mode in paretic support. */
};

/** @} */

/**
 * @addtogroup HIGH_LEVEL_CONTROLLER_INPUT_GROUP
 * @{
 */

/**
 * @brief Controller type enumeration.
 *
 * This used to represent which controller is running. Only using V2 controller now
 */
enum controller_type{
	V2_CONTROLLER = 0,
	V0_CONTROLLER = 1	
};

/** @} */

/**
 * @addtogroup HIGH_LEVEL_CONTROLLER_STRUCTURE_GROUP 
 * @{
 */ 

/**
 * @brief Position profile structure. 
 *
 - This structure contains PF & DF trajectory parameters that are manually adjusted by operator.
 - Used in update_struct_pf_controller() & update_struct_df_controller()  from @ref TRAJECTORY_GENERATION_ALGORITHM_GROUP.
 .
 */
struct struct_position_profile{
	enum controller_type ctype;		/**< controller type (V0 or V2)*/
	s8 flag_controller_changed;		/**< flag if controller is changed */
	s32 f_pf_des;					/**< Desired PF peak force. [N] Sourced from user interface. */
	s32 onset_percentage_pf;		/**< Desired PF onset timing. [% in paretic support] Sourced from user interface. */
	s32 p_df_des;					/**< Desired DF cable pull length. [mm] Sourced from user interface. */	
	s32 v_pull_df;					/**< Desired DF cable pull velocity. [mm/sec] Sourced from user interface. */	
	s32 v_push_out_df;				/**< Desired DF cable push-out velocity. [mm/sec] Sourced from user interface. */	
	s32 flag_df_active;				/**< Flag indicating df actuation is active*/
	s32 flag_pf_active;				/**< Flag indicating pf actuation is active*/
};

/**
 * @brief Data structure for plantar flexion trajectory generation
 *
 - This structure is used when generating PF trajectory.
 - Contains outputs and internal variables used in update_struct_pf_controller() from @ref TRAJECTORY_GENERATION_ALGORITHM_GROUP.
 .
 */
struct struct_pf_controller{	
	float p_pull_command;							/**< PF cable active pull command [mm]*/
	float v_command;								/**< PF cable velocity command [mm/sec]*/
	s8 stamp_start_active_pull;						/**< Stamp at the timing when active pull starts*/
	s8 stamp_start_pushout;							/**< Stamp at the timing when cable push-out starts*/
	enum pf_trajectory_mode pull_state;				/**< pull state. See pf_trajectory_mode */

	float p_max_pull;								/**< PF maximum desired pull length [mm].  Internal val of update_struct_pf_controller() method.*/
	float v_pull_init;								/**< Initial PF cable velocity when start pulling. [mm/sec] used in PF_CONSTANT_PULL_VELOCITY_MODE. Internal val of update_struct_pf_controller() method. */
	float v_pull_prev;								/**< PF cable velocity in previous iteration step [mm/sec].  Internal val of update_struct_pf_controller() method. */
	float v_push_out;								/**< PF cable velocity during push-out. used in PF_PUSHOUT_MODE and NON_PARETIC_SUPPORT.  Internal val of update_struct_pf_controller() method. */
	float v_pretension;								/**< PF cable velocity during pre-tension setup. used in PF_PRETENSION_MODE.  Internal val of update_struct_pf_controller() method.  */	
	
	float peak_forces[PEAK_FORCE_ARRAY_SIZE];		/**< Array to save PF peak force measurements from previous strides [N].  Internal val of update_struct_pf_controller() method. */
	float f_max;									/**< PF Maximum force in current stride [N].  Internal val of update_struct_pf_controller() method. */
	float average_peak_force;						/**< average of previous peak force measurement [N].  Internal val of update_struct_pf_controller() method. */	
	
	s8 flag_reached_hard_stop;						/**< Flag if PF pulley reached hard stop.  Internal val of update_struct_pf_controller() method. */	
	s8 flag_reached_desired_force;					/**< Flag if PF measured force have ever reached desired force after system initialization.  Internal val of update_struct_pf_controller() method. */
	s8 flag_high_force_warning;						/**< Flag if PF measured force is higher than desired force.  Internal val of update_struct_pf_controller() method. */	
	
	u32 t_init_velocity_ramp;						/**< Time when position command changed, and velocity starts to rampup [msec]. Internal val of update_struct_pf_controller() method. */	
	u32 t_init_high_force_warning;					/**< Time when flag_high_force_warning is on. Internal val of update_struct_pf_controller() method. */	
};

/**
 * @brief Data structure for dorsi flexion controller
 *
 - This structure is used when generating DF trajectory
 - Contains outputs and internal variables used in update_struct_df_controller() from @ref TRAJECTORY_GENERATION_ALGORITHM_GROUP.
 .
 */
struct struct_df_controller{
	
	float p_pull_command;						/**< DF cable active pull command [mm]*/
	float v_command;							/**< DF cable active pull command [mm]*/
	u32 t_init_velocity_ramp;					/**< Time when position command changed, and velocity starts to rampup [msec]. Internal val of update_struct_df_controller() method.*/	
};

/** @} */


struct struct_pretension;
struct struct_ftf_angle_detection;
struct struct_gyro_detection;
struct struct_gait_param;


#ifdef __cplusplus
extern "C"{
#endif

/**
 * @addtogroup HIGH_LEVEL_CONTROLLER_STRUCTURE_GROUP
 * @{
 */

/**
 * @brief Initialize struct_position_profile
 * 
 * @param my_struct_position_profile A pointer to @ref struct_position_profile
 * @return The function doesn't return anything.
 *
 */
void init_struct_position_profile(struct struct_position_profile *my_struct_position_profile);

/**
 * @brief Initialize struct_pf_controller 
 * 
 * @param my_struct_pf_controller A pointer to @ref struct_pf_controller
 * @return The function doesn't return anything.
 *
 */
void init_struct_pf_controller(struct struct_pf_controller* my_struct_pf_controller);

/**
 * @brief Initialize struct_df_controller
 * 
 * @param my_struct_df_controller A pointer to a @ref struct_df_controller 
 * @return The function doesn't return anything.
 *
 */
void init_struct_df_controller(struct struct_df_controller* my_struct_df_controller);

/** @} */

/**
 * @addtogroup TRAJECTORY_GENERATION_ALGORITHM_GROUP
 * @{
 */

/**
 * @brief PF position and velocity trajectory generation algorithm
 * 
 - Objectives:
 	-# Generate position and velocity trajectory for PF actuation
 	-# Update struct_pf_controller
 	-# Triggers PF onset based on non-paretic mid swing detected from detect_mid_swing()
 	.
 - Inputs:
 	-# f_meas_
 	-# t_ms_
 	-# pull_curr
 	-# p_lim_
 	-# From struct_gyro_detection
	 	-# struct_gyro_detection.stamp_confirmed_npfl
 		-# struct_gyro_detection.count_pfl_after_last_stop
 		-# struct_gyro_detection.flag_is_walking
 		-# struct_gyro_detection.gait_state
 		.
 	-# From struct_ftf_angle_detection
 		-# struct_ftf_angle_detection.stamp_confirmed_np_mid_swing
 		.
 	-# From struct_pretension (particularly object for PF pretension. i.e. med_ctr.my_struct_pf_pretension)
 		-# struct_pretension.p_pret_command
 		.
 	-# From struct_position_profile
	 	-# struct_position_profile.f_pf_des
 		-# struct_position_profile.onset_percentage_pf
 		.
 	.
 - Outputs: All saved in struct_pf_controller
 	-# struct_pf_controller.p_pull_command
 	-# struct_pf_controller.v_command
 	-# struct_pf_controller.pull_state
 	-# struct_pf_controller.stamp_start_active_pull
 	-# struct_pf_controller.stamp_start_pushout
 	.
 - Internal variables: All saved in struct_pf_controller
 	-# struct_pf_controller.p_max_pull
 	-# struct_pf_controller.v_pull_init
 	-# struct_pf_controller.v_pull_prev
 	-# struct_pf_controller.v_push_out
 	-# struct_pf_controller.v_pretension
 	-# struct_pf_controller.peak_forces
 	-# struct_pf_controller.f_max
 	-# struct_pf_controller.average_peak_force
 	-# struct_pf_controller.flag_reached_hard_stop
 	-# struct_pf_controller.flag_reached_desired_force
 	-# struct_pf_controller.flag_high_force_warning
 	-# struct_pf_controller.t_init_velocity_ramp
 	-# struct_pf_controller.t_init_high_force_warning
 	.
 .
 * @param f_meas_ the force measurement from PF load cell [N]
 * @param t_ms_ time [msec] 
 * @param pull_curr the current active pull length [mm] 
 * @param p_lim_ Maximum PF motor travel [mm]
 * @param my_struct_gyro_detection Constant pointer to struct_gyro_detection 
 * @param my_struct_gait_param Constant pointer to struct_gait_param
 * @param my_struct_pf_pretension  pointer to struct_pretension
 * @param my_struct_position_profile  pointer to struct_position_profile
 * @param my_struct_pf_controller  pointer to struct_pf_controller. Contains internal variables and output of update_struct_pf_controller()
 * @return The function doesn't return anything.
 */
void update_struct_pf_controller(float f_meas_, u32 t_ms_, float pull_curr, float p_lim_, const struct struct_gyro_detection* my_struct_gyro_detection,
									const struct struct_ftf_angle_detection* my_struct_ftf_angle_detection, const struct struct_pretension* my_struct_pf_pretension, 
								const struct struct_position_profile* my_struct_position_profile, struct struct_pf_controller* my_struct_pf_controller);

/**
 * @brief  DF position and velocity trajectory generation algorithm
 * 
 - Objectives:
 	-# Generate position and velocity trajectory for DF actuation
 	-# Update struct_df_controller
 	.
 - Inputs:
 	-# t_ms_
 	-# t_ms_
 	-# p_lim_
 	-# From struct_gyro_detection
	 	-# struct_gyro_detection.stamp_confirmed_npfl
 		-# struct_gyro_detection.stamp_confirmed_pfl 		
 		-# struct_gyro_detection.count_pfl_after_last_stop
 		-# struct_gyro_detection.flag_is_walking
 		-# struct_gyro_detection.gait_state
 		.
 	-# From struct_pretension (particularly object for DF pretension. i.e. med_ctr.my_struct_df_pretension)
 		-# struct_pretension.p_pret_command
 		.
 	-# From struct_position_profile
	 	-# struct_position_profile.p_df_des
 		-# struct_position_profile.v_pull_df
 		-# struct_position_profile.v_push_out_df 		
 		.
 	.
 - Outputs: All saved in struct_df_controller
 	-# struct_df_controller.p_pull_command
 	-# struct_df_controller.v_command
 	.
 - Internal variables: All saved in struct_df_controller
 	-# struct_df_controller.t_init_velocity_ramp
 	.
 .
 * @param t_ms_ time [msec]
 * @param p_lim_ Maximum DF motor travel [mm]
 * @param my_struct_gyro_detection Constant pointer to struct_gyro_detection 
 * @param my_struct_df_pretension  pointer to struct_pretension
 * @param my_struct_position_profile  pointer to struct_position_profile
 * @param struct_df_controller  pointer to struct_df_controller. Contains internal variables and output of update_struct_df_controller()
 * @return The function doesn't return anything.
 */
void update_struct_df_controller(u32 t_ms_, float p_lim_, const struct struct_gyro_detection* my_struct_gyro_detection, const struct struct_pretension* my_struct_df_pretension,
								const struct struct_position_profile* my_struct_position_profile,  struct struct_df_controller* my_struct_df_controller);	


/** @} */

/**
 * @defgroup ETC_GROUP
 * Other functions functions that is not currently used
 * @{
 */

/**
 * @brief Callback for changing controller type  when received associated protocol packet
 * 
 *
 * @param arg1 A pointer to a \ref position_profile structure
 * @param arg2 A pointer to a 4 byte array (message of protocol packet)
 * @param size Size of the protocol packet
 * @return The function doesn't return anything.
 *
 */ 
void change_pf_controller_type(void* arg1, u8* arg2, u8 size);

/** @} */

/**
 * @addtogroup HELPER_GROUP
 * @{
 */

/**
 * @brief Helper function to reset interval valiables of functions; update_struct_pf_controller_v2() or update_struct_pf_controller_v0().
 * 
 *
 * @param my_struct_pf_controller A pointer to struct_pf_controller
 * @return The function doesn't return anything.
 */ 
void reset_internal_vals_in_pf_controller(struct struct_pf_controller* my_struct_pf_controller);

/** @} */



#ifdef __cplusplus
}
#endif
#endif /*__POSITION_PROFILE__ */

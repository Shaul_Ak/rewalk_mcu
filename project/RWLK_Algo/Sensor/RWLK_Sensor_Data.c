/*******************************************************************************
* file name: RWLK_Sensor_Data.c
*
* Description: IMU and Load-Cell Sensors handler
*
* Note: Based on Wyss can_protocol module
*
* Author:  Shaul Akiva
* Date:    April 2017
*
******************************************************************************/

/*******************************************************************************
*  Includes
******************************************************************************/
#include <medexo_controller.h>
#include "RWLK_Common.h"
#include "RWLK_Sensor_Data.h"
#include "F28x_Project.h"     // Device Header file and Examples Include File
#include "inc/hw_types.h"
#include <string.h>
#include <stdio.h>
#include "RWLK_Timer_Driver.h"
#include "RWLK_ExtFlash_application.h"
#include "RWLK_GPIO_Driver.h"
#include "RWLK_CAN_Application.h"
#include "RWLK_CAN_Msg_Handler.h"
#include "RWLK_CAN_Driver.h"

/*******************************************************************************
 *  Definitions
 ******************************************************************************/
#define sensor_msg 				med_ctr.sensor_prot

/*******************************************************************************
*  Data types
******************************************************************************/


/*------------------------------------------------------------------------------
Variables
------------------------------------------------------------------------------*/
#ifdef SENSOR_DEBUG
	static char StrMsg[20];
#endif
static uint8_t DataStatus[NUM_OF_ID];

static uint8_t StatusCounter 	= 0;
int lastUpdate 		= 0;
_Bool Sensor_Data_Is_Available = false;

static SensorCommincationTypedef SensorData_Comm[NUM_OF_SENSORS];
static uint8_t SensorResponseFlag[NUM_OF_SENSORS] = {0};

CAN_SENSOR_MSG SensorResponseBuffer[NUM_OF_SENSORS];
/*******************************************************************************
*  Constants and Macros
******************************************************************************/

/*******************************************************************************
*  Static Data deceleration
******************************************************************************/

// Global variables
extern struct medexo_controller med_ctr;

//struct sensor_protocol_struct sensor_msg;
/*******************************************************************************
*  Static function deceleration
******************************************************************************/
static void  RWLK_SensorData_CopyByte2Word(unsigned char* BytesData, int32_t* wordData, bool reverseWord, bool secondWord);
static void  RWLK_SensorData_CopyWord2Byte(int32_t* wordData, uint8_t* BytesData, bool reverseWord, bool secondWord);

static void RWLK_SensorData_UpdateCounter(uint8_t index);

uint16_t RWLK_SensorData_Ask_SelfTest(uint8_t id);
void RWLK_SensorData_Ask_CalibrationValue(uint8_t id);
void RWLK_SensorData_Set_CalibrationValue(uint8_t id);
void RWLK_SensorData_Ask_Start(uint8_t id);
void RWLK_SensorData_Ask_Stop(uint8_t id);
void RWLK_SensorData_Ask_Status(uint8_t id);

void RWLK_SensorData_Receive_SelfTest(uint8_t id);
void RWLK_SensorData_Receive_CallibrationValue(uint8_t id);
void RWLK_SensorData_Receive_CallibrationtAck(uint8_t id);
void RWLK_SensorData_Receive_StartAck(uint8_t id);
void RWLK_SensorData_Receive_StopAck(uint8_t id);
void RWLK_SensorData_Receive_Status(uint8_t id);
/*******************************************************************************
*  Public Function Bodies
******************************************************************************/

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Shaul 18.4.2017: ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#ifdef SENSOR_MODE
/*******************************************************************************
* Function :  RWLK_SensorData_Parser
*
* Description : Parser Can message to the IMU and Load-cell data type format
* 				data is defined by the ID value gets in CAN
* 				IMU messages should be reverse (little/big endian converter)
*
* Input: SensorByteData : Data type that include 8 byte CAN data
* 		  ID			: CAN message ID  (represent the message type)
* 		  Length		: CAN DLS value
*
******************************************************************************/
void  RWLK_SensorData_Parser(uint32_t ID,unsigned char *SensorByteData) //(CAN_MSG_DATA* SensorByteData, uint32_t ID, uint32_t Length)
{

		switch (ID){
			case CAN_PROTOCOL_IMU0_L:
				RWLK_SensorData_CopyByte2Word(SensorByteData,(int32_t*)&sensor_msg.imu_l[0],false, true);
				RWLK_SensorData_UpdateCounter(IMU0_L_ID);
		#ifdef SENSOR_DEBUG
				sprintf(StrMsg,"151:%x%x,%x%x\r\n",sensor_msg.imu_l[0],sensor_msg.imu_l[1]);
		#endif
				break;
			case CAN_PROTOCOL_IMU1_L:
				RWLK_SensorData_CopyByte2Word(SensorByteData,(int32_t*)&sensor_msg.imu_l[2],false, false);
				RWLK_SensorData_UpdateCounter(IMU1_L_ID);
		#ifdef SENSOR_DEBUG
				 sprintf(StrMsg,"152:%x%x\r\n",sensor_msg.imu_l[2]);
		#endif
				break;
			case CAN_PROTOCOL_IMU2_L:
				RWLK_SensorData_CopyByte2Word(SensorByteData,(int32_t*)&sensor_msg.imu_l[3],false, true);
				RWLK_SensorData_UpdateCounter(IMU2_L_ID );
		#ifdef SENSOR_DEBUG
				 sprintf(StrMsg,"153:%x%x,%x%x\r\n",sensor_msg.imu_l[3],sensor_msg.imu_l[4]);
		#endif
				break;
			case CAN_PROTOCOL_IMU3_L:
				RWLK_SensorData_CopyByte2Word(SensorByteData,(int32_t*)&sensor_msg.imu_l[5],false, false);
				RWLK_SensorData_UpdateCounter(IMU3_L_ID );
		#ifdef SENSOR_DEBUG
				 sprintf(StrMsg,"154:%x%x\r\n",sensor_msg.imu_l[5]);
		#endif
				break;
			case CAN_PROTOCOL_IMU4_L:
				RWLK_SensorData_CopyByte2Word(SensorByteData,(int32_t*)&sensor_msg.imu_l[6],false, true);
				RWLK_SensorData_UpdateCounter(IMU4_L_ID);
		#ifdef SENSOR_DEBUG
				 sprintf(StrMsg,"155:%x%x,%x%x\r\n",sensor_msg.imu_l[6],sensor_msg.imu_l[7]);
		#endif
				break;
			case CAN_PROTOCOL_IMU5_L:
				RWLK_SensorData_CopyByte2Word(SensorByteData,(int32_t*)&sensor_msg.imu_l[8],false, false);
				RWLK_SensorData_UpdateCounter(IMU5_L_ID);
		#ifdef SENSOR_DEBUG
				 sprintf(StrMsg,"156:%x%x\r\n",sensor_msg.imu_l[8]);
		#endif
				break;
			case CAN_PROTOCOL_IMU0_R:
				RWLK_SensorData_CopyByte2Word(SensorByteData,(int32_t*)&sensor_msg.imu_r[0],false, true);
				RWLK_SensorData_UpdateCounter(IMU0_R_ID);
		#ifdef SENSOR_DEBUG
				 sprintf(StrMsg,"141:%x%x,%x%x\r\n",sensor_msg.imu_r[0],sensor_msg.imu_r[1]);
		#endif
				break;
			case CAN_PROTOCOL_IMU1_R:
				RWLK_SensorData_CopyByte2Word(SensorByteData,(int32_t*)&sensor_msg.imu_r[2],false, false);
				RWLK_SensorData_UpdateCounter(IMU1_R_ID);
		#ifdef SENSOR_DEBUG
				 sprintf(StrMsg,"142:%x%x\r\n",sensor_msg.imu_r[2]);
		#endif
				break;
			case CAN_PROTOCOL_IMU2_R:
				RWLK_SensorData_CopyByte2Word(SensorByteData,(int32_t*)&sensor_msg.imu_r[3],false, true);
				RWLK_SensorData_UpdateCounter(IMU2_R_ID);
		#ifdef SENSOR_DEBUG
				 sprintf(StrMsg,"143:%x%x,%x%x\r\n",sensor_msg.imu_r[3],sensor_msg.imu_r[4]);
		#endif
				break;
			case CAN_PROTOCOL_IMU3_R:
				RWLK_SensorData_CopyByte2Word(SensorByteData,(int32_t*)&sensor_msg.imu_r[5],false, false);
				RWLK_SensorData_UpdateCounter(IMU3_R_ID);
		#ifdef SENSOR_DEBUG
				 sprintf(StrMsg,"144:%x%x\r\n",sensor_msg.imu_r[5]);
		#endif
				break;
			case CAN_PROTOCOL_IMU4_R:
				RWLK_SensorData_CopyByte2Word(SensorByteData,(int32_t*)&sensor_msg.imu_r[6],false, true);
				RWLK_SensorData_UpdateCounter(IMU4_R_ID);
		#ifdef SENSOR_DEBUG
				 sprintf(StrMsg,"145:%x%x,%x%x\r\n",sensor_msg.imu_r[6],sensor_msg.imu_r[7]);
		#endif
				break;
			case CAN_PROTOCOL_IMU5_R:
				RWLK_SensorData_CopyByte2Word(SensorByteData,(int32_t*)&sensor_msg.imu_r[8],false, false);
				RWLK_SensorData_UpdateCounter(IMU5_R_ID);
		#ifdef SENSOR_DEBUG
				 sprintf(StrMsg,"146:%x%x\r\n",sensor_msg.imu_r[8]);
		#endif
				break;
			case CAN_PROTOCOL_LC1:
				RWLK_SensorData_CopyByte2Word(SensorByteData,(int32_t*)&sensor_msg.lc1,true,true);
				//can_prot.lc1 = __REV(can_prot->lc1);
				RWLK_SensorData_UpdateCounter(LC1_ID);
		#ifdef SENSOR_DEBUG
				 sprintf(StrMsg,"410:%x%x\r\n",sensor_msg.lc1);
		#endif // SENSOR_DEBUG
				break;
			case CAN_PROTOCOL_LC2:
				RWLK_SensorData_CopyByte2Word(SensorByteData,(int32_t*)&sensor_msg.lc2,true,false);
				//can_prot.lc2 = __REV(can_prot->lc2);
				RWLK_SensorData_UpdateCounter(LC2_ID);
		#ifdef SENSOR_DEBUG
				 sprintf(StrMsg,"710:%x%x\r\n",sensor_msg.lc2);
		#endif // SENSOR_DEBUG
				break;
			default:
				break;
			}
		#ifdef SENSOR_DEBUG
			scic_msg(StrMsg);
		#endif // SENSOR_DEBUG
}

#endif

/*******************************************************************************
* Function : RWLK_SensorData_CopyWord
*
* Description : reverse word data that  get in byte array format and copy it
* 			  to u32 data type
*
* Input:  BytesData   - Input byte array
* 		  wordData    - pointer to int or int array save to 32bit word/s.
* 		  reverseWord - 1 - Reverse the each word and save in little-ending format
* 		 			    0 - not reverse word order
* 		  secondWord  - 1 - there is second word
* 		 			    0 - No second word
*
******************************************************************************/
static void  RWLK_SensorData_CopyByte2Word(unsigned char* BytesData, int32_t* wordData, bool reverseWord, bool secondWord)
{
	if (reverseWord)
	{
		wordData[0] = BytesData[3];
		wordData[0] = (wordData[0] << 8) | BytesData[2];
		wordData[0] = (wordData[0] << 8) | BytesData[1];
		wordData[0] = (wordData[0] << 8) | BytesData[0];
	}else{
		wordData[0] = BytesData[0];
		wordData[0] = (wordData[0] << 8) | BytesData[1];
		wordData[0] = (wordData[0] << 8) | BytesData[2];
		wordData[0] = (wordData[0] << 8) | BytesData[3];
	}

	if (secondWord)
		if (reverseWord)
		{
			wordData[1] = BytesData[7];
			wordData[1] = (wordData[1] << 8) | BytesData[6];
			wordData[1] = (wordData[1] << 8) | BytesData[5];
			wordData[1] = (wordData[1] << 8) | BytesData[4];
		}else
		{
			wordData[1] = BytesData[4];
			wordData[1] = (wordData[1] << 8) | BytesData[5];
			wordData[1] = (wordData[1] << 8) | BytesData[6];
			wordData[1] = (wordData[1] << 8) | BytesData[7];
		}
}

/*******************************************************************************
* Function : 	RWLK_SensorData_CopyWord2Byte
*
* Description : Copy int32 word or tow words to byte array
* 				the copy can be in original order or in reverse order depend on
* 				 the reverse flag.
*
* Input: wordData    - pointer to int or int array.
* 		 BytesData   - Output byte array
* 		 reverseWord - 1 - Reverse the each word and save in little-ending format
* 		 			   0 - not reverse word order
* 		 secondWord  - 1 - there is second word
* 		 			   0 - No second word
*
******************************************************************************/
static void  RWLK_SensorData_CopyWord2Byte(int32_t* wordData, uint8_t* BytesData, bool reverseWord, bool secondWord)
{
	if (reverseWord)
	{
		BytesData[0] = wordData[0] & 0xFF ;;
		BytesData[1] = (wordData[0] >> 8) & 0xFF;
		BytesData[2] = (wordData[0] >> 16) & 0xFF;
		BytesData[3] = (wordData[0] >> 24) & 0xFF;
	}else{
		BytesData[3]= wordData[0] & 0xFF ;
		BytesData[2] = (wordData[0] >> 8) & 0xFF;
		BytesData[1] = (wordData[0] >> 16) & 0xFF;
		BytesData[0] = (wordData[0] >> 24) & 0xFF;
	}

	if (secondWord)
		if (reverseWord)
		{
			BytesData[4] = wordData[1] & 0xFF ;;
			BytesData[5] = (wordData[1] >> 8) & 0xFF;
			BytesData[6] = (wordData[1] >> 16) & 0xFF;
			BytesData[7] = (wordData[1] >> 24) & 0xFF;
		}else{
			BytesData[7]= wordData[1] & 0xFF ;
			BytesData[6] = (wordData[1] >> 8) & 0xFF;
			BytesData[5] = (wordData[1] >> 16) & 0xFF;
			BytesData[4] = (wordData[1] >> 24) & 0xFF;
		}

}
/*******************************************************************************
* Function : RWLK_SensorData_CopyWord
*
* Description : reverse word data that  get in byte array format and copy it
* 			  to u32 data type
*
* Input: BytesData 4 byte array that should be reversed and copied.
*
******************************************************************************/
static void RWLK_SensorData_UpdateCounter(uint8_t index)
{
	uint32_t UpdateTimer = D_Timer_Get_mSec_Counter();

	if ( DataStatus[index] == 0)
	{
		 DataStatus[index]++;
		 StatusCounter++;
	}

	if ( StatusCounter == NUM_OF_ID)
	{
		StatusCounter = 0;
		memset(DataStatus,0,sizeof(DataStatus)*NUM_OF_ID);
		lastUpdate = UpdateTimer - lastUpdate;
		FlagSens_Out_Toggle();

		Sensor_Data_Is_Available = true;

		lastUpdate =  D_Timer_Get_mSec_Counter();
	}
}
/*******************************************************************************
 * 					Command Handler												*
******************************************************************************** */

/*******************************************************************************
* Function : RWLK_SensorData_SetSensorResponseFlage
*
* Description : Set the Response command data flag to new value
*
* Input: SensorCell_ID - ResponseFlage Array index.
* 		 newValue -  0 - No new vale
* 					 1 - New Value
******************************************************************************/
void RWLK_SensorData_SetSensorResponseFlage(uint8_t SensorCell_ID, uint8_t newValue)
{
	SensorResponseFlag[SensorCell_ID] = newValue;
}

/*******************************************************************************
* Function : RWLK_SensorData_GetSensorResponseFlage
*
* Description :  Get the Response command  value
*
* Input: SensorCell_ID - ResponseFlage Array index.
*
* return:   0 - No new vale
* 			1 - New Value
******************************************************************************/
uint8_t RWLK_SensorData_GetSensorResponseFlage(uint8_t SensorCell_ID)
{
	return SensorResponseFlag[SensorCell_ID];
}

/*******************************************************************************
* Function : RWLK_SensorData_CopySensorResponseBuffer
*
* Description : Copy Rx CAN sensor message to Sensor local struct
*
* Input:  SensorCell_ID 		- ResponseFlage Array index.
* 		  CAN_Sensor_Command	- Local struct to Rx message
*
******************************************************************************/
void RWLK_SensorData_CopySensorResponseBuffer(uint8_t SensorCell_ID, CAN_SENSOR_MSG* CAN_Sensor_Command)
{
	memcpy(&SensorResponseBuffer[SensorCell_ID], CAN_Sensor_Command, sizeof(CAN_SENSOR_MSG));
}

/*******************************************************************************
* Function : RWLK_SensorData_CommandInit
*
* Description :	Initial the Sensor board.
* 					1. Sending to all sensor boards Self-test command
* 					2. Update calibration values.
*
* Input:
******************************************************************************/
void RWLK_SensorData_CommandInit(void)
{
	int i;


	for( i = 0 ; i < NUM_OF_SENSORS; ++i)
	{
		RWLK_SensorData_Ask_SelfTest(i);
	}
	SensorData_Comm[0].Calibaration1[0] = -30000.2;
	SensorData_Comm[0].Calibaration1[1] = 90000.5;
	SensorData_Comm[0].Calibaration2[0] = -31000.2;
	SensorData_Comm[0].Calibaration2[1] = 80000.5;
}

/*******************************************************************************
* Function :	RWLK_SensorData_CommandHandler
*
* Description :	This function is called from main every 1mSec and check Sensor response
* 				flag of all sensor that registered.
* 				In case of sensor response rising flag the function calls the
* 				command handler function match the command ID code.
*
* Input:
******************************************************************************/
void RWLK_SensorData_CommandHandler(void)
{
	int i;
	uint8_t SensorCommand;

	for ( i = 0; i < NUM_OF_SENSORS; ++i)
	{
		if (SensorResponseFlag[i] == 1)
		{
			SensorCommand = SensorResponseBuffer[i].ID & 0x0F0;

			switch (SensorCommand)
			{
			case CAN_COMMAND_SELF_TEST:
				RWLK_SensorData_Receive_SelfTest(i);
				break;

			case CAN_COMMAND_GET_CALIBRATION:
				RWLK_SensorData_Receive_CallibrationValue(i);
				break;

			case CAN_COMMAND_SET_CALIBRATION1:
			case CAN_COMMAND_SET_CALIBRATION2:
				RWLK_SensorData_Receive_CallibrationtAck(i);
				break;

			case CAN_COMMAND_STATUS:
				RWLK_SensorData_Receive_Status(i);
				break;
			}
			SensorResponseFlag[i] = 0;
		}
	}
}

/*******************************************************************************
* Function : RWLK_SensorData_RejectHandler
*
* Description : The function handle in receive reject messages get from the sensors
* 				each receive message cause send-back the command that didn't get
* 				correctly in the sensor board.
*
* Input:		id - CAN message ID that contain the command and the sensor ID
*
******************************************************************************/
void RWLK_SensorData_RejectHandler(uint16_t id)
{
	uint8_t SensorID  =  id & 0x0F;

	switch ( id & 0x0F )
	{
	case (CAN_COMMAND_RX_LC1_2 & 0x0F):
		SensorID = LC1_ARRAY_CELL;
		break;

	case (CAN_COMMAND_RX_IMU_R & 0x0F):
		SensorID = IMU_RIGHT_ARRAY_CELL;
		break;

	case (CAN_COMMAND_RX_IMU_L & 0x0F):
		SensorID = IMU_LEFT_ARRAY_CELL;
		break;
	}

	switch ( id & 0xF0 )
	{
	case CAN_COMMAND_SELF_TEST:
		RWLK_SensorData_Ask_SelfTest(SensorID);
		break;

	case CAN_COMMAND_GET_CALIBRATION:
		if ( SensorData_Comm[SensorID].State == SENSOR_RESPONSE_GET_CALIBRATION1 )
			SensorData_Comm[SensorID].State = SENSOR_RESPONSE_SELF_TEST;
		if ( SensorData_Comm[SensorID].State == SENSOR_RESPONSE_GET_CALIBRATION2 )
					SensorData_Comm[SensorID].State = SENSOR_RESPONSE_GET_CALIBRATION1;
		RWLK_SensorData_Ask_CalibrationValue(SensorID);
		break;

	case CAN_COMMAND_SET_CALIBRATION1:
		SensorData_Comm[SensorID].State = SENSOR_RESPONSE_SELF_TEST;
		RWLK_SensorData_Ask_CalibrationValue(SensorID);
		break;

	case CAN_COMMAND_SET_CALIBRATION2:
		SensorData_Comm[SensorID].State = SENSOR_RESPONSE_SET_CALIBRATION1;
		RWLK_SensorData_Set_CalibrationValue(SensorID);
		break;

	case CAN_COMMAND_STATUS:
		RWLK_SensorData_Ask_Status(SensorID);
		break;
	}
}


/*******************************************************************************
* Function :RWLK_SensorData_Ask_SelfTest
*
* Description :  Ask Self-test from the sensor board
*
* Input: id - Local Sensor id number
******************************************************************************/
uint16_t RWLK_SensorData_Ask_SelfTest(uint8_t id)
{
	CAN_SENSOR_MSG TxMsg;
	uint32_t StartTime = D_Timer_Get_uSec_Counter();
	uint32_t CurrTime = D_Timer_Get_uSec_Counter();
	uint16_t ErrCode = ERR_OK;

	SensorData_Comm[id].State = SENSOR_RESPONSE_SELF_TEST;

	TxMsg.ID 	  = CAN_SENSOR_COMMAND_TX | CAN_COMMAND_SELF_TEST;
	TxMsg.Data[0]  = 1;
	TxMsg.Length   = 1;

	ErrCode = RWLK_CAN_Send_Sensor_Command(id, &TxMsg);
	while ( ( ( StartTime + 3) > CurrTime )  && (ErrCode != ERR_OK) )
	{
		ErrCode  = RWLK_CAN_Send_Sensor_Command(id, &TxMsg);
		CurrTime = D_Timer_Get_uSec_Counter();
	}

	return ErrCode;
}

/*******************************************************************************
* Function :	RWLK_SensorData_Ask_CalibrationValue
*
* Description : This function ask calibration params values from the sensor board
* 				the values are received in tow steps one after the other.
* 				The second one is received after sending ack to the first one.
*
* Input:  		id - Local Sensor id number
******************************************************************************/
void RWLK_SensorData_Ask_CalibrationValue(uint8_t id)
{
	CAN_SENSOR_MSG TxMsg;
	if (SensorData_Comm[id].State == SENSOR_RESPONSE_GET_CALIBRATION1)
	{
		SensorData_Comm[id].State = SENSOR_RESPONSE_GET_CALIBRATION2;
		TxMsg.Data[0]   	= 2;
	}else
	{
		SensorData_Comm[id].State = SENSOR_RESPONSE_GET_CALIBRATION1;
		TxMsg.Data[0]   	= 1;
	}

	TxMsg.ID 	   		= CAN_SENSOR_COMMAND_TX |  CAN_COMMAND_GET_CALIBRATION;
	TxMsg.Length   		= 1;
	RWLK_CAN_Send_Sensor_Command(id, &TxMsg);
}

/*******************************************************************************
* Function :	RWLK_SensorData_Set_CalibrationValue
*
* Description : This function send to the sensor board new calibration values
* 				the values is sent in tow steps one after the other.
* 				The second one is sent after get ack response the sensor board.
*
* Input: 		id - Local Sensor id number
******************************************************************************/
void RWLK_SensorData_Set_CalibrationValue(uint8_t id)
{
	CAN_SENSOR_MSG TxMsg;
	if (SensorData_Comm[id].State == SENSOR_RESPONSE_SET_CALIBRATION1)
	{
		SensorData_Comm[id].State = SENSOR_RESPONSE_SET_CALIBRATION2;
		RWLK_SensorData_CopyWord2Byte((int32_t*)SensorData_Comm[id].Calibaration2, TxMsg.Data, false, true);
		TxMsg.ID 	    = CAN_SENSOR_COMMAND_TX | CAN_COMMAND_SET_CALIBRATION2;
	}else
	{
		SensorData_Comm[id].State = SENSOR_RESPONSE_SET_CALIBRATION1;
		RWLK_SensorData_CopyWord2Byte((int32_t*)SensorData_Comm[id].Calibaration1, TxMsg.Data, false, true);
		TxMsg.ID 	    = CAN_SENSOR_COMMAND_TX | CAN_COMMAND_SET_CALIBRATION1;
	}

	TxMsg.Length   	= 8;
	RWLK_CAN_Send_Sensor_Command(id, &TxMsg);
}

/*******************************************************************************
* Function :	RWLK_SensorData_Ask_Start
*
* Description : This function ask from the sensor board to start
*
* Input: 		id - Local Sensor id number
******************************************************************************/
void RWLK_SensorData_Ask_Start(uint8_t id)
{
	CAN_SENSOR_MSG TxMsg;

	SensorData_Comm[id].State = SENSOR_RESPONSE_STATUS;

	TxMsg.ID 	    = CAN_SENSOR_COMMAND_TX | CAN_COMMAND_STATUS;
	TxMsg.Data[0]   = SENSOR_STATUS_START;
	TxMsg.Length   	= 1;
	RWLK_CAN_Send_Sensor_Command(id, &TxMsg);
}

/*******************************************************************************
* Function :	RWLK_SensorData_Ask_Stop
*
* Description : This function ask from the sensor board to stop
*
* Input: 		id - Local Sensor id number
******************************************************************************/
void RWLK_SensorData_Ask_Stop(uint8_t id)
{
	CAN_SENSOR_MSG TxMsg;

	SensorData_Comm[id].State = SENSOR_RESPONSE_STATUS;

	TxMsg.ID 	    = CAN_SENSOR_COMMAND_TX | CAN_COMMAND_STATUS;
	TxMsg.Data[0]   = SENSOR_STATUS_STOP;
	TxMsg.Length   	= 1;
	RWLK_CAN_Send_Sensor_Command(id, &TxMsg);
}


/*******************************************************************************
* Function :	RWLK_SensorData_Ask_Status
*
* Description : This function ask from the sensor board its status
*
* Input: 		id - Local Sensor id number
******************************************************************************/
void RWLK_SensorData_Ask_Status(uint8_t id)
{
	CAN_SENSOR_MSG TxMsg;

	SensorData_Comm[id].State = SENSOR_RESPONSE_STATUS;

	TxMsg.ID 	    = CAN_SENSOR_COMMAND_TX | CAN_COMMAND_STATUS;
	TxMsg.Data[0]   = SENSOR_STATUS_SENDBACK;
	TxMsg.Length   	= 1;
	RWLK_CAN_Send_Sensor_Command(id, &TxMsg);
}

/*******************************************************************************
* Function :	RWLK_SensorData_Receive_SelfTest
*
* Description :	The function is called after received Self-test acknowledge message
* 				the continue option depend on the sensor self-test results:
* 					Self-Test success: 		continue to ask calibration value
* 				 	Self-Test Failure: 			continue to ask self-test again
* 				 	Self-Test No-calibration: 	continue to send new calibration values.
*
* Input:  id - Local Sensor id number
******************************************************************************/
void RWLK_SensorData_Receive_SelfTest(uint8_t id)
{
	if ( SensorData_Comm[id].State != SENSOR_RESPONSE_SELF_TEST)
	{
		RWLK_SensorData_Ask_SelfTest(id);
	}else
	{
		switch (SensorResponseBuffer[id].Data[0])
		{
		case SENSOR_SELF_TEST_SUCCESS:
			RWLK_SensorData_Ask_CalibrationValue(id);
			break;

		case SENSOR_SELF_TEST_FAILURE:
			RWLK_SensorData_Ask_SelfTest(id);
			break;

		case SENSOR_SELF_TEST_NO_CALIBRATION:
			RWLK_SensorData_Set_CalibrationValue(id);
			break;

		}
	}
}

/*******************************************************************************
* Function:		RWLK_SensorData_Receive_CallibrationValue
*
* Description : Function called after receive new calibration value.
* 				The function copy the new calibration params to calibration struct.
* 				continue to ask next calibration params or to ask sensor start.
*
* Input: id - Local Sensor id number
******************************************************************************/
void RWLK_SensorData_Receive_CallibrationValue(uint8_t id)
{
	if ( SensorData_Comm[id].State == SENSOR_RESPONSE_GET_CALIBRATION1 )
	{
		RWLK_SensorData_CopyByte2Word((unsigned char*)SensorResponseBuffer[id].Data, (int32_t*)SensorData_Comm[id].Calibaration1 , false, true);
		//SensorData_Comm[id].State = SENSOR_RESPONSE_GET_CALIBRATION2;
		RWLK_SensorData_Ask_CalibrationValue(id);
	}else if ( SensorData_Comm[id].State == SENSOR_RESPONSE_GET_CALIBRATION2 )
	{
		RWLK_SensorData_CopyByte2Word((unsigned char*)SensorResponseBuffer[id].Data, (int32_t*)SensorData_Comm[id].Calibaration2 , false, true);
		RWLK_SensorData_Ask_Start(id);
	}else
	{
		RWLK_SensorData_Ask_SelfTest(id);
	}
}

/*******************************************************************************
* Function :	RWLK_SensorData_Receive_CallibrationtAck
*
* Description :	Function called after receive Set calibration acknowledge message
* 				continue to next calibration params or to ask sensor start.
*
* Input:  id - Local Sensor id number
******************************************************************************/
void RWLK_SensorData_Receive_CallibrationtAck(uint8_t id)
{
	if ( SensorData_Comm[id].State == SENSOR_RESPONSE_SET_CALIBRATION1 )
	{
		//SensorData_Comm[id].State = SENSOR_RESPONSE_SET_CALIBRATION2;
		RWLK_SensorData_Set_CalibrationValue(id);
	}else if  ( SensorData_Comm[id].State == SENSOR_RESPONSE_SET_CALIBRATION2 )
	{
		RWLK_SensorData_Ask_Start(id);
	}else
	{
		RWLK_SensorData_Ask_SelfTest(id);
	}
}

/*******************************************************************************
* Function :    RWLK_SensorData_Receive_Status
*
* Description :  Handle the sensor receive status message.
* 				 Stop status     - Ask Self-Test again
* 				 Start status    - update status to On-Going
* 				 Set-Calibration - Call Set calibration function
* 				 Get-Calibration - Call Get calibration function
* 				 Failure status  - Ask Self-Test again
*
* Input:  id - Local Sensor id number
******************************************************************************/
void RWLK_SensorData_Receive_Status(uint8_t id)
{
	switch (SensorResponseBuffer[id].Data[0])
	{
	case SENSOR_STATUS_STOP:
		SensorData_Comm[id].Status = SENSOR_STATUS_STOP;
		RWLK_SensorData_Ask_SelfTest(id);
		break;

	case SENSOR_STATUS_START:
		SensorData_Comm[id].Status = SENSOR_STATUS_ON_GOING;
		break;

	case SENSOR_STATUS_SET_CALIBRATION:
		SensorData_Comm[id].Status = SENSOR_STATUS_SET_CALIBRATION;
	//	SensorData_Comm[id].State  = SENSOR_RESPONSE_SET_CALIBRATION1;
		RWLK_SensorData_Ask_CalibrationValue(id);
		break;

	case SENSOR_STATUS_GET_CALIBRATION:
		SensorData_Comm[id].Status = SENSOR_STATUS_GET_CALIBRATION;
	//	SensorData_Comm[id].State  = SENSOR_RESPONSE_GET_CALIBRATION1;
		RWLK_SensorData_Set_CalibrationValue(id);
		break;

	case SENSOR_STATUS_FAILURE:
		SensorData_Comm[id].Status = SENSOR_STATUS_FAILURE;
		RWLK_SensorData_Ask_SelfTest(id);
		break;

	}
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

s32 RWLK_get_sensor_pf_lc(struct sensor_protocol_struct *sensor_prot, enum medexo_controller_pariectic_side side)
{
	if (side == MEDEXO_CONTROLLER_LEFT_PARIETIC)
		return sensor_prot->lc1;
	else if (side == MEDEXO_CONTROLLER_RIGHT_PARIETIC)
		return sensor_prot->lc2;
	return 0;
}

s32 RWLK_get_sensor_df_lc(struct sensor_protocol_struct *sensor_prot, enum medexo_controller_pariectic_side side)
{
	if (side == MEDEXO_CONTROLLER_LEFT_PARIETIC)
		return sensor_prot->lc2;
	else if (side == MEDEXO_CONTROLLER_RIGHT_PARIETIC)
		return sensor_prot->lc1;
	return 0;
}
float RWLK_get_sensor_gyro_p_value(struct sensor_protocol_struct *sensor_prot, enum medexo_controller_pariectic_side side)
{
	if (side == MEDEXO_CONTROLLER_LEFT_PARIETIC)
		return -sensor_prot->imu_l[8];
	else if (side == MEDEXO_CONTROLLER_RIGHT_PARIETIC)
		return sensor_prot->imu_l[8];
	return 0;
}

float RWLK_get_sensor_gyro_np_value(struct sensor_protocol_struct *sensor_prot, enum medexo_controller_pariectic_side side)
{
	if (side == MEDEXO_CONTROLLER_LEFT_PARIETIC)
		return sensor_prot->imu_r[8];
	else if (side == MEDEXO_CONTROLLER_RIGHT_PARIETIC)
		return -sensor_prot->imu_r[8];
	return 0;
}

float RWLK_get_sensor_ftf_angle_p_value(struct sensor_protocol_struct *sensor_prot, enum medexo_controller_pariectic_side side)
{
	if (side == MEDEXO_CONTROLLER_LEFT_PARIETIC)
		return sensor_prot->imu_l[1];
	else if (side == MEDEXO_CONTROLLER_RIGHT_PARIETIC)
		return -sensor_prot->imu_r[1];
	return 0;
}

float RWLK_get_sensor_ftf_angle_np_value(struct sensor_protocol_struct *sensor_prot, enum medexo_controller_pariectic_side side)
{
	if (side == MEDEXO_CONTROLLER_LEFT_PARIETIC)
		return -sensor_prot->imu_r[1];
	else if (side == MEDEXO_CONTROLLER_RIGHT_PARIETIC)
		return sensor_prot->imu_l[1];
	return 0;
}
s32 RWLK_handle_in_active_mode(struct sensor_protocol_struct *sensor_prot)
{
	//return sensor_prot->handle_state.state.button0 && sensor_prot->handle_state.state.button1;
}
s32 RWLK_handle_in_ptens_mode(struct sensor_protocol_struct *sensor_prot)
{
	//return sensor_prot->handle_state.state.button0 ^ sensor_prot->handle_state.state.button1;
}
_Bool _Get_Is_SensorData_Available(void)
{
	return Sensor_Data_Is_Available;
}

void _Reset_Is_SensorData_Available(void)
{
	 Sensor_Data_Is_Available = false;
}

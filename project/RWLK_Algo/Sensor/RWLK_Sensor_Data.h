/*******************************************************************************
* file name: RWLK_Sensor_Data.h
*
* Description: Header file for RWLK_Sensor_Data.c
*
* Author:  Shaul Akiva
* Date:    April 2017
*
******************************************************************************/


#ifndef __RWLK_SENSOR_DATA__
#define __RWLK_SENSOR_DATA__

/*******************************************************************************
*  Includes
******************************************************************************/
#include "types.h"
#include "RWLK_CAN_Msg_Handler.h"
#include "RWLK_SCI_Driver.h"
#include "RWLK_CAN_Application.h"
/*==============================================================================
                               Sensor Defines
==============================================================================*/
//#define SENSOR_DEBUG
#define NUM_OF_ID 14
#define NUM_OF_SENSORS 3

#define LC1_ARRAY_CELL 	0
//#define LC2_ARRAY_CELL 	1
#define IMU_RIGHT_ARRAY_CELL 	1
#define IMU_LEFT_ARRAY_CELL 	2
//#define SENSOR_MODE
/*******************************************************************************
*  							Data types
******************************************************************************/
enum medexo_controller_pariectic_side;

//  Sensor Data message ID
enum Sensor_ID{
	IMU0_L_ID = 0,
	IMU1_L_ID = 1,
	IMU2_L_ID = 2,
	IMU3_L_ID = 3,
	IMU4_L_ID = 4,
	IMU5_L_ID = 5,
    IMU0_R_ID = 6,
	IMU1_R_ID = 7,
	IMU2_R_ID = 8,
	IMU3_R_ID = 9,
	IMU4_R_ID = 10,
	IMU5_R_ID = 11,
	LC1_ID = 	12,
	LC2_ID = 	13
};


// Sensor command options
typedef enum
{
	CAN_COMMAND_STATUS	 	 			= 0x10,
	CAN_COMMAND_SELF_TEST	  			= 0x20,
	CAN_COMMAND_GET_CALIBRATION			= 0x30,
	CAN_COMMAND_SET_CALIBRATION1		= 0x40,
	CAN_COMMAND_SET_CALIBRATION2		= 0x50,
}CanCommandsTypedef;

// Sensor command response handler options
typedef enum
{
	SENSOR_RESPONSE_SELF_TEST			= 0x00,
	SENSOR_RESPONSE_GET_CALIBRATION1,
	SENSOR_RESPONSE_GET_CALIBRATION2,
	SENSOR_RESPONSE_SET_CALIBRATION1,
	SENSOR_RESPONSE_SET_CALIBRATION2,
	SENSOR_RESPONSE_STATUS,
}SensorStateTypedef;

//  Command message - Sensor ID
typedef enum
{
	COMMAND_HUB_ID				  = 0x04,
	COMMAND_LOADCELL1_ID		  = 0x05,
	COMMAND_LOADCELL2_ID 		  = 0x06,
	COMMAND_LOADCELL1_2ID 	 	  = 0x07,

	COMMAND_IMU_RIGHT_ID		  = 0x09,
	COMMAND_IMU_LEFT_ID			  = 0x0A,
	COMMAND_IMU_R_L_ID			  = 0x0B,
}SensorIDTypedef;

// Sensor Command Self-test switch options
typedef enum
{
	SENSOR_SELF_TEST_SUCCESS 			= 0x01,
	SENSOR_SELF_TEST_FAILURE			= 0x02,
	SENSOR_SELF_TEST_NO_CALIBRATION		= 0x03,
}SensorSelfTestTypedef;

// Sensor command status options
typedef enum
{
	SENSOR_STATUS_STOP					= 0x00,
	SENSOR_STATUS_START					= 0x01,
	SENSOR_STATUS_GET_STATUS			= 0x02,
	SENSOR_STATUS_ON_GOING				= 0x02,
	SENSOR_STATUS_SET_CALIBRATION		= 0x03,
	SENSOR_STATUS_GET_CALIBRATION		= 0x04,
	SENSOR_STATUS_FAILURE				= 0x05,
	SENSOR_STATUS_SENDBACK				= 0xFF,
}SensorStatusTypedef;

// Sensor command state, status and calibrations parameters struct.
typedef struct
{
	SensorStateTypedef 		State;
	SensorStatusTypedef	    Status;
	float 		   			Calibaration1[2];
	float		   			Calibaration2[2];
} SensorCommincationTypedef;


struct sensor_protocol_struct
{
	float imu_r[9];
	float imu_l[9];
	s32 lc1;
	s32 lc2;
};



//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Shaul 18.4.2017: ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#ifdef SENSOR_MODE

enum can_protocol_msg_id{
	CAN_PROTOCOL_IMU0_L = 0x151,
	CAN_PROTOCOL_IMU1_L = 0x152,
	CAN_PROTOCOL_IMU2_L = 0x153,
	CAN_PROTOCOL_IMU3_L = 0x154,
	CAN_PROTOCOL_IMU4_L = 0x155,
	CAN_PROTOCOL_IMU5_L = 0x156,
    CAN_PROTOCOL_IMU0_R = 0x141,
	CAN_PROTOCOL_IMU1_R = 0x142,
	CAN_PROTOCOL_IMU2_R = 0x143,
	CAN_PROTOCOL_IMU3_R = 0x144,
	CAN_PROTOCOL_IMU4_R = 0x145,
	CAN_PROTOCOL_IMU5_R = 0x146,
	CAN_PROTOCOL_LC1 = 0x410,
	CAN_PROTOCOL_LC2 = 0x710
};


/*==============================================================================
                                 Sensor Functions Prototypes
==============================================================================*/

void  RWLK_SensorData_Init(void);
//void  RWLK_SensorData_Parser(CAN_MSG_DATA* SensorByteData, uint32_t ID, uint32_t Length);
void  RWLK_SensorData_Parser(uint32_t ID,unsigned char *SensorByteData);
void Send_Sensor_Data_To_Bluetooth(struct sensor_protocol_struct sensor_output_data);
#endif  // SENSOR_MODE
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//Sensor Command
void RWLK_SensorData_SetSensorResponseFlage(uint8_t SensorCell_ID, uint8_t newValue);
uint8_t RWLK_SensorData_GetSensorResponseFlage(uint8_t SensorCell_ID);
void RWLK_SensorData_CopySensorResponseBuffer(uint8_t SensorCell_ID, CAN_SENSOR_MSG* CAN_Sensor_Command);
void RWLK_SensorData_CommandInit(void);
void RWLK_SensorData_CommandHandler(void);
void RWLK_SensorData_RejectHandler(uint16_t id);

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


s32   RWLK_get_sensor_pf_lc(struct sensor_protocol_struct *sensor_prot, enum medexo_controller_pariectic_side side);
s32   RWLK_get_sensor_df_lc(struct sensor_protocol_struct *sensor_prot, enum medexo_controller_pariectic_side side);
float RWLK_get_sensor_gyro_p_value(struct sensor_protocol_struct *sensor_prot, enum medexo_controller_pariectic_side side);
float RWLK_get_sensor_gyro_np_value(struct sensor_protocol_struct *sensor_prot, enum medexo_controller_pariectic_side side);
float RWLK_get_sensor_ftf_angle_p_value(struct sensor_protocol_struct *sensor_prot, enum medexo_controller_pariectic_side side);
float RWLK_get_sensor_ftf_angle_np_value(struct sensor_protocol_struct *sensor_prot, enum medexo_controller_pariectic_side side);
s32   RWLK_handle_in_active_mode(struct sensor_protocol_struct *sensor_prot);
s32   RWLK_handle_in_ptens_mode(struct sensor_protocol_struct *sensor_prot);
_Bool _Get_Is_SensorData_Available(void);
void _Reset_Is_SensorData_Available(void);
#endif /*__RWLK_SENSOR_DATA__*/

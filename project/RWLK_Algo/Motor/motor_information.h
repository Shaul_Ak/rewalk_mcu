#ifndef __MOTOR_INFORMATION_H__
#define __MOTOR_INFORMATION_H__

#include "types.h"

enum direction
{
	NEGATIVE_DIRECTION=-1,
	POSITIVE_DIRECTION=1
};

struct motor_information
{
	s32 actual_position;
	float actual_position_mm;
	s32 demanded_position;
	float demanded_position_mm;
	u32 profile_velocity;
	s32 demanded_velocity;
	float demanded_velocity_mm_sec;
	s32 actual_velocity;
	float actual_velocity_mm_sec;
	float maximum_peak_limit;
	float continous_current_limit;
	float max_peak_duration;
    u32 motor_rated_current;
    s32 max_position;
    enum direction dir;
	/* u32 motor_rated_torque; */
	u32 motor_fault; //TO MOVE
	s32 phase_commu; //TO MOVE
	float voltage;
    u16 status_word;
	s16 actual_current;
	/* s16 target_torque; */
	/* s16 actual_torque; */
	s16 demanded_torque;
	u16 drive_temperature;
	/* u16 max_torque; //TO MOVE */
	/* u16 max_current; //TO MOVE */
	u8 motor_mo;
};
extern struct motor_information motor_info;

#endif


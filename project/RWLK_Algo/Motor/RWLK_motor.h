#ifndef __RWLK_MOTOR_H__
#define __RWLK_MOTOR_H__

#include "motor_information.h"
#include "types.h"

#define RWLK_CAN_MSG_MOTOR_ID 0x431

enum motor_id{
	MOTOR_ID_1 = 0,
	MOTOR_ID_2 = 1,

};

typedef enum {
	MOTOR_MASSAGE_DISABLE = 0,
	MOTOR_MASSAGE_ENABLE  = 1,
	MOTOR_MASSAGE_LENGTH
} MOTOR_MASSAGES_STATUSE_TYPEDEF;


struct motor_information* RWLK_get_motor_info_struct(struct motor_information *motor_info,enum motor_id id);
enum direction            RWLK_get_motor_rotation_direction(enum motor_id m_id);
void                      RWLK_single_motor_move_mm(enum motor_id m_id,int pos,int vel);
float                     RWLK_get_motor_demanded_position_mm(enum motor_id m_id);
float                     RWLK_get_motor_actual_position_mm(enum motor_id m_id);
float                     RWLK_get_motor_actual_velocity_mm_sec(enum motor_id m_id);
float                     RWLK_get_motor_voltage(enum motor_id m_id);
float                     RWLK_get_motor_max_position_counts(enum motor_id m_id);
s32                       RWLK_get_motor_position_offset_mm(enum motor_id m_id);
void 					  RWLK_Set_Motor_Massages_Status(u8 status);

#endif

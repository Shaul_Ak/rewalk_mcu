#include "math.h"
#include <Motor/RWLK_motor.h>
#include <Motor/motor_information.h>
#include "RWLK_CAN_Msg_Handler.h"
#include "position_profile.h"
#include "RWLK_CAN_Msg_Handler.h"
#include "medexo_controller.h"
//#include "RWLK_LogicHandler.h"

static MOTOR_MASSAGES_STATUSE_TYPEDEF Motor_MassageStatus = MOTOR_MASSAGE_DISABLE;

extern  Input_Command_Parameters_Type Requested_Command_Parameters;
extern  Output_Command_Parameters_Type Calculated_Command_Parameters;


//#define COUNTS_PER_MM  1005.0
//#define MM_PER_COUNTS  0.000994

struct motor_information* RWLK_get_motor_info_struct(struct motor_information *motor_info,enum motor_id id)
{
	return;
}

enum direction RWLK_get_motor_rotation_direction(enum motor_id m_id)
{
	if(m_id == X_Axis)
	{
		if( sControlVarStruct.XVelocityFeedback >= 0)
			return  POSITIVE_DIRECTION;
		else
			return  NEGATIVE_DIRECTION;
	}
	else if(m_id == Y_Axis)
	{
		if( sControlVarStruct.YVelocityFeedback >= 0)
			return POSITIVE_DIRECTION;
		else
			return  NEGATIVE_DIRECTION;
	}
}
void	RWLK_single_motor_move_mm(enum motor_id m_id,int pos,int vel)
{

	if (Motor_MassageStatus == MOTOR_MASSAGE_ENABLE)
	{
		 RWLK_CAN_Send_MotorMoveMassage(m_id,pos,vel);
	}
	if((pToMoveVarStructsArray[m_id]->DesiredPosition != (int32_t)((COUNTS_PER_MM*(double)pos)))&&(med_ctr.handle_mode != ACTIVE_MODE))
	{
			pToMoveVarStructsArray[m_id]->ProfileMaxVelocity = (COUNTS_PER_MM*(double)vel);
			pToMoveVarStructsArray[m_id]->ProfileAcceleration = pToMoveVarStructsArray[m_id]->ProfileMaxVelocity * 10;
			pToMoveVarStructsArray[m_id]->ProfileJerk = pToMoveVarStructsArray[m_id]->ProfileAcceleration*10;
			pToMoveVarStructsArray[m_id]->DesiredPosition = (int32_t)((COUNTS_PER_MM*(double)pos)+sMotorControlVariablesStructsArray[m_id]->PreviousIncrementalEncoderData);
			pToMoveVarStructsArray[m_id]->FinalTargetPoint = pToMoveVarStructsArray[m_id]->DesiredPosition;

			if(m_id == X_Axis)
			{
				//AxisCommand = X_Axis;
				pToMoveVarStructsArray[m_id]->StartTargetPoint = sControlVarStruct.XMotorPosition;
				ProfileParsPrepareToMove(pToMoveVarStructsArray[m_id], ProfileVarStructsArray[XCpuToClaProfileVars],CommonCpuProfileParametersStructsArray[m_id]);
			}
			else if(m_id == Y_Axis)
			{
				//AxisCommand = Y_Axis;
				pToMoveVarStructsArray[m_id]->StartTargetPoint = sControlVarStruct.YMotorPosition;
				ProfileParsPrepareToMove(pToMoveVarStructsArray[m_id], ProfileVarStructsArray[YCpuToClaProfileVars],CommonCpuProfileParametersStructsArray[m_id]);

			}
	}
	else if((med_ctr.handle_mode == ACTIVE_MODE))
	{
		pToMoveVarStructsArray[m_id]->PreviousDesiredPosition = pToMoveVarStructsArray[m_id]->DesiredPosition;
		CommonCpuProfileParametersStructsArray[m_id]->Sn = (COUNTS_PER_MM*(double)pos);
		//sCpuToCla1ProfileCommandsStructsArray[m_id]->PositionCommand = (int32_t)((COUNTS_PER_MM*(double)pos));
		//sCpuToCla1ProfileCommandsStructsArray[m_id]->VelocityFeedforwardCommand = (int32_t)((COUNTS_PER_MM*SAMPLING_TIME*0.65*(double)vel));
		CommonCpuProfileParametersStructsArray[m_id]->Vn = (COUNTS_PER_MM*SAMPLING_TIME*0.65*(double)vel); // 0.65 - 65% from velocity;

	}
}
float	RWLK_get_motor_demanded_position_mm(enum motor_id m_id)
{

	if(m_id == X_Axis)
	{
		sMotorControlVariablesStructsArray[X_Axis]->IncrementalEncoderData = sControlVarStruct.XMotorPosition;
		return ((float)sMotorControlVariablesStructsArray[X_Axis]->IncrementalEncoderData*MM_PER_COUNTS);
	}
	else if(m_id == Y_Axis)
	{
		sMotorControlVariablesStructsArray[Y_Axis]->IncrementalEncoderData = sControlVarStruct.YMotorPosition;
		return ((float)sMotorControlVariablesStructsArray[Y_Axis]->IncrementalEncoderData*MM_PER_COUNTS);
	}

}
float	RWLK_get_motor_actual_position_mm(enum motor_id m_id) //NOT USED
{

	if(m_id == X_Axis)
	{
		sMotorControlVariablesStructsArray[X_Axis]->IncrementalEncoderData = sControlVarStruct.XMotorPosition - sMotorControlVariablesStructsArray[X_Axis]->PreviousIncrementalEncoderData;
		return ((float)sMotorControlVariablesStructsArray[X_Axis]->IncrementalEncoderData*MM_PER_COUNTS);
	}
	else if(m_id == Y_Axis)
	{
		sMotorControlVariablesStructsArray[Y_Axis]->IncrementalEncoderData = sControlVarStruct.YMotorPosition - sMotorControlVariablesStructsArray[Y_Axis]->PreviousIncrementalEncoderData;
		return ((float)sMotorControlVariablesStructsArray[Y_Axis]->IncrementalEncoderData*MM_PER_COUNTS);
	}
}
float	RWLK_get_motor_actual_velocity_mm_sec(enum motor_id m_id)
{
	if(m_id == X_Axis)
	{
		return ((float)sControlVarStruct.XVelocityFeedback*MM_PER_COUNTS);
	}
	else if(m_id == Y_Axis)
	{
		return ((float)sControlVarStruct.YVelocityFeedback*MM_PER_COUNTS);
	}
}
float	RWLK_get_motor_voltage(enum motor_id m_id)
{
	return;
}
float	RWLK_get_motor_max_position_counts(enum motor_id m_id)
{
	float Result = 0 ;
	if(m_id == X_Axis)
	{
		Result = Pulley_Diameter * Pi * NumberOfBinding * COUNTS_PER_MM;
	}
	else if(m_id == Y_Axis)
	{
		Result = Pulley_Diameter * Pi * NumberOfBinding * COUNTS_PER_MM;
	}
	return Result;

}
s32	RWLK_get_motor_position_offset_mm(enum motor_id m_id)
{
	float Result = 0 ;
	if(m_id == X_Axis)
	{
		Result = 0;
	}
	else if(m_id == Y_Axis)
	{
		Result = 0;
	}
	return Result ;

}

void 	RWLK_Set_Motor_Massages_Status(u8 status)
{
	if ((MOTOR_MASSAGES_STATUSE_TYPEDEF)status < MOTOR_MASSAGE_LENGTH )
			Motor_MassageStatus = status;

}




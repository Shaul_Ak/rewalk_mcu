/*******************************************************************************
* file name: ExtFlash_Driver.c
*
* Description: .
*
* Note: Developed for TI F28377D.
*
* Author:  Yehuda Bitton
* Date:    
*
******************************************************************************/

/*******************************************************************************
*  Includes
******************************************************************************/
#include "ExtFlash_Driver.h"     // Device Header file and Examples Include File
#include "RWLK_SPI_Driver.h"

/*******************************************************************************
 *  Defines
 ******************************************************************************/
// "Flag Status Register" Bit Defines:
#define		FSR_PEC_BIT						 0x80 // "PROGRAM ERASE CONTROLLER" bit[7]

// "Status Register" Bits defines:
#define 	SR_SRWD_BIT					 	 0x80
#define 	SR_BP3_BIT						 0x40
#define 	SR_TB_BIT						 0x20
#define 	SR_BP2_BIT						 0x10
#define 	SR_BP1_BIT						 0x08
#define 	SR_BP0_BIT						 0x04
#define 	SR_WEL_BIT						 0x02	// "Write enable latch" bit [1]
#define 	SR_WIP_BIT						 0x01	// "Write In Progress" bit [0]

// "Flag Status Register" Bits defines:
#define 	FSR_PROG_ERASE_CTL_BIT			 0x80
#define 	FSR_ERASE_SUSP_BIT				 0x40
#define 	FSR_ERASE_BIT					 0x20
#define 	FSR_PROGRAM_BIT					 0x10
#define 	FSR_VPP_BIT						 0x08
#define 	FSR_PROG_SUSP_BIT				 0x04
#define 	FSR_PROTECT_BIT					 0x02
#define 	FSR_ADDR_MODE_BIT				 0x01



/*******************************************************************************
*  Data types
******************************************************************************/
typedef struct _FLASH_DESCRIPTION
{
	uint32_t		FlashId;
	uint8_t 		FlashType;
//	NMX_uint32 		StartingAddress;	/* Start Address of the Flash Device */
//	NMX_uint32 		FlashAddressMask;
//	NMX_uint32 		FlashSectorCount;
//	NMX_uint32 		FlashSubSectorCount;
//	NMX_uint32		FlashSubSectorSize_bit;
//	NMX_uint32 		FlashPageSize;
//	NMX_uint32		FlashPageCount;
//	NMX_uint32		FlashSectorSize;
//	NMX_uint32		FlashSectorSize_bit;
//	NMX_uint32		FlashSubSectorSize;
//	NMX_uint32 		FlashSize;
//	NMX_uint32		FlashOTPSize;
//	NMX_uint8		FlashDieCount;
//	NMX_uint32		FlashDieSize;
//	NMX_uint32		FlashDieSize_bit;
//	NMX_uint32		Size;				/* The density of flash device in bytes */
//	NMX_uint32		BufferSize;			/* In bytes */
//	NMX_uint8		DataWidth;			/* In bytes */
//	AddressMode		NumAddrByte;		/* Num of bytes used to address memory */

}  FLASH_DESCRIPTION;
/*------------------------------------------------------------------------------
Variables
------------------------------------------------------------------------------*/

/*******************************************************************************
*  Constants and Macros
******************************************************************************/

/*******************************************************************************
*  Static Data deceleration
******************************************************************************/

/******************************************************************************
*  Private Functions Decleration:
******************************************************************************/
ExFlash_ReturnStatus ExtFlash_TimeOut(uint32_t Seconds);
ExFlash_ReturnStatus ExtFlash_Wait_Till_Command_Execution_Complete(uint16_t second);
void Split_Addr_To_Byte_Array(uint32_t Addr, uint8_t *pArray, uint8_t Addr_Mode_Selector );

/*****************************************************************************
*  Public Function Bodies
******************************************************************************/

/*****************************************************************************
 *
 * Function name: ExtFlash_Program_UpToPage
 *
 * Description: Program Flash up to One page (1-256 Byte).
 *
 * Parameters:  Addr - 3 Byte/ 4Byte address.
 *              pDataArr - pointer to 8bit data array to be programmed to the flash.
 *              DataArrLength - Number of bytes in data array.
 *
 * Returns: ExFlash_ReturnStatus
 *
 *ToBeDone - 3/4 address should be selected in the program
 ******************************************************************************/
ExFlash_ReturnStatus ExtFlash_Program_UpToPage (uint32_t Addr, uint8_t *pDataArr, uint32_t DataArrLength )
{
	uint8_t Fsr_Value; // a var to hold "Flag Status Register" content, to validate operation success.
	uint8_t Command_Array[5]; // Command_Array[0] - Flash Command hex, Command_Array[1-4] - 3 byte adress, Command_Array[1-5] - 4 byte adress

	Command_Array[0] = EXFLASH_COM_PAGE_PROGRAM;

	//-------- Step 1: Validate Address:
	//  Address should be within Flash size.
	if ( Addr > EXFLASH_SIZE )
		return Flash_AddressInvalid;
	// Data size should be less or equal to Page size.
	if ( DataArrLength > EXFLASH_PAGE_SIZE )
		return Flash_ExceedPageSize;

	//-------- Step 2: Check whether any previous Write, Program or Erase cycle is on-going
	if ( IsFlashBusy() )
		return Flash_OperationOngoing;

	//-------- Step 3: Disable Write protection
	ExtFlash_WriteEnable ();

	//-------- Step 4: Send Program command (SPI send)
	Split_Addr_To_Byte_Array(Addr,Command_Array, 0 ); // '1' - 4 Byte address.
	SPI_Send(SPI_C_ID, Command_Array, 4, NOT_DISABLE_CS_AFTER_SEND);

	//-------- Step 5: Program data (SPI send)
	SPI_Send(SPI_C_ID, pDataArr, DataArrLength, DISABLE_CS_AFTER_SEND);

	//-------- Step 6: Wait until the operation completes or a timeout occurs.
	ExtFlash_Wait_Till_Command_Execution_Complete(1);

	//-------- Step 7: Check the Program operation success:
    // Read "Flag Status Register" (according to the datasheet it must be follow a Page Program operation).
	ExtFlash_Read_FlagStatusRegister(&Fsr_Value);
    // Clear "Flag Status Register" in case of an error (error bits are sticky).
	ExtFlash_Clear_FlagStatusRegister();

	if(Fsr_Value & FSR_PROTECT_BIT)
		return Flash_SectorProtected;
	else if (Fsr_Value & FSR_PROGRAM_BIT)
		return Flash_OpFailed;
	else
		return Flash_Success;
}

/*****************************************************************************
 * Function name: ExtFlash_Read_Data
 *
 * Description: Read Data.
 * 				- After any READ command is executed, the device will output data from the selected address
 * 				  in the die. After a die boundary is reached, the device will start reading again from
 * 				  the beginning of the same 256Mb die.
 * 				- The operation is terminated by driving S# HIGH at any time during data output.
 * 				- This function default is 3 Byte address mode, for 4 byte address mode "ENABLE 4-BYTE ADDRESS MODE"
 * 				  command should be executed.
 *
 *              Step 1: Validate Address.
 *              Step 2: Send Program command (SPI send).
 *              Step 3:Step 3: Read data (SPI Receive)
 *
 * Parameters: Addr - 3Byte/ 4Byte address.
 *             pDataArr - pointer to 8bit data array to be programmed to the flash.
 *             DataArrLength - Number of bytes in data array.
 *             Select_4Byte_Mode '1' = 4Byte address mode, It works even if the device is in 3Byte mode.
 *             					 '0' = The read command is in 3Byte or 4byte address mode according to device' configuration before calling this function.
 *             					 the device default after reset is 3Byte address mode.
 * Returns:    ReturnStatus.
 *
 ******************************************************************************/
ExFlash_ReturnStatus ExtFlash_Read_Data ( uint32_t Addr, uint8_t *pDataArr, uint32_t DataArrLength, uint8_t Select_4Byte_Mode )
{

	uint8_t Command_Array[5];//4+Select_4Byte_Mode]; // Command_Array[0] - Flash Command hex, Command_Array[1-4] - 3 byte adress, Command_Array[1-5] - 4 byte adress

	if (Select_4Byte_Mode)
		Command_Array[0] = EXFLASH_COM_4BYTE_READ;
	else
		Command_Array[0] = EXFLASH_COM_READ;

	//-------- Step 1: Validate Address:
	//  Address should be within Flash size.
	if ( Addr > EXFLASH_SIZE )
		return Flash_AddressInvalid;

	//-------- Step 2: Send Program command (SPI send)
	Split_Addr_To_Byte_Array(Addr, Command_Array, Select_4Byte_Mode ); //Select_4Byte_Mode: '1' - 4 Byte address.
	SPI_Send(SPI_C_ID, Command_Array, 4+Select_4Byte_Mode, NOT_DISABLE_CS_AFTER_SEND);

	//-------- Step 3: Read data (SPI Receive)
	SPI_Receive(SPI_C_ID, pDataArr, DataArrLength);

	return Flash_Success;
}

/*****************************************************************************
 * Function name: ExtFlash_Read_Device_Id
 *
 * Description: Read 3 byte device ID.
 *              Step 1: Send SPI Read ID command
 *              Step 2: Step 2: Recieve Device ID.
 *              Step 3: Arrange the data in 32bit format
 *
 * Parameters:  pData - a pointer to hold register content.
 * Returns:     ReturnStatus.
 *
 ******************************************************************************/
ExFlash_ReturnStatus ExtFlash_Read_Device_Id (uint32_t *pData)
{
	uint8_t  Id[3] = {0,0,0};

	uint8_t Command_Array[1];
	uint8_t len = 1;

	Command_Array[0] = EXFLASH_COM_READ_DEVICE_ID;

//	// Step 1: Send Flash command.
//	SPI_Send(SPI_C_ID, Command_Array, len, NOT_DISABLE_CS_AFTER_SEND);
//	// Step 2: Recieve Device ID.
//	SPI_Receive(SPI_C_ID, Id, 3);
	SPI_Tranceive(SPI_C_ID, Command_Array, len, Id, 3);


	// Step 3: Arrange the data in 32bit format ( manufaturer id + memory type + memory capacity )
	*pData = Id[0];
	*pData <<= 8;
	*pData |= Id[1];
	*pData <<= 8;
	*pData |= Id[2];

	return Flash_Success;
}


/*****************************************************************************
 * Function name:
 *
 * Description:
 *
 * Parameters:
 * Returns:     ReturnStatus.
 *
 ******************************************************************************/
ExFlash_ReturnStatus  ExtFlash_Erase_SubSector ( uint32_t SubSector_Num, uint8_t Select_4Byte_Mode )
{
	uint32_t SubSector_Addr;
	uint8_t Fsr_Value;
	ExFlash_ReturnStatus ret;

	uint8_t Command_Array[5];
	uint8_t len = 4 + Select_4Byte_Mode;

	// Step 1: Validate the sector number input
	if( SubSector_Num > EXFLASH_NUM_OF_SUBSECTORS )
		return Flash_SectorNrInvalid;

	SubSector_Addr = SubSector_Num << EXFLASH_SUBSECTOR_SIZE_POWER_OF_TWO;

	// Step 2: Check whether any previous Write, Program or Erase cycle is on going
	if(IsFlashBusy())
		return Flash_OperationOngoing;

	// Step 3: Disable Write protection
	ExtFlash_WriteEnable();

	// Step 1: Send SPI command.
	Command_Array[0] = EXFLASH_COM_SUBSECTOR_ERASE;


	//-------- Step 2: Send Program command (SPI send)
	Split_Addr_To_Byte_Array( SubSector_Addr, Command_Array, Select_4Byte_Mode ); //Select_4Byte_Mode: '1' - 4 Byte address.
	SPI_Send(SPI_C_ID, Command_Array, len, DISABLE_CS_AFTER_SEND);


	// Step 6: Wait until the operation completes or a timeout occurs.
	ret = ExtFlash_Wait_Till_Command_Execution_Complete(SSE_TIMEOUT);
    if (ret == Flash_OperationTimeOut)
    	return ret;


    // Read "Flag Status Register" (according to the datasheet it must be follow a Page Program operation).
	ExtFlash_Read_FlagStatusRegister(&Fsr_Value);
    // Clear "Flag Status Register" in case of an error (error bits are sticky).
	ExtFlash_Clear_FlagStatusRegister();

	// Evluate "Flag Status Register" error bits:
	if (Fsr_Value & FSR_PROTECT_BIT)
		return Flash_SectorProtected;
	else if (Fsr_Value & FSR_PROGRAM_BIT)
		return Flash_OpFailed;
	else
		return Flash_Success;
}



/*****************************************************************************
 * Function name: IsFlashBusy
 *
 * Description: Read "Program or erase controller" bit @ "Flag Status Register".
 *              Indicates whether one of the following command cycles is in progress:
 *              WRITE STATUS REGISTER, WRITE NONVOLATILE CONFIGURATION REGISTER, PROGRAM, or ERASE.
 *
 * Parameters:  None.
 * Returns:     true - Ext.Flash is busy.
 * 				false - Ext.Flash is ready.
 *
 ******************************************************************************/
bool IsFlashBusy(void)
{
	uint8_t RecvData;

	// Read Flag Status Register:
	ExtFlash_Read_FlagStatusRegister(&RecvData);

	// Check "Program or erase controller" bit @ Flag Status Register:
	if( !(RecvData & FSR_PEC_BIT))
		return true;
	else
		return false;
}


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//								Register commands
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


/*****************************************************************************
 * Function name: ExtFlash_WriteEnable
 *
 * Description: This function sets ('1') "Write Enable Latch" (WEL) bit in STATUS Register.
 *              This function must be called before WRITE STATUS REGISTER,PROGRAM, or ERASE operations.
 *
 *              Step 1: Send SPI command
 *              Step 2: Verify that write enable command was done.
 *                      (use timeout).
 *
 * Parameters:  None.
 * Returns:     Flash_OperationTimeOut.
 * 				Flash_Success
 *
 * Known issues:
 * Todo:
 *
 ******************************************************************************/
ExFlash_ReturnStatus ExtFlash_WriteEnable (void)
{
	uint8_t Command_Array[1];
	uint8_t len = 1;
	uint8_t RecvData;

	uint8_t i = 0;

   // Step 1: Send SPI command.
	Command_Array[0] = EXFLASH_COM_WRITE_ENABLE;
	SPI_Send(SPI_C_ID, Command_Array, len, DISABLE_CS_AFTER_SEND);

	// Step 2: Verify that write enable command was done.
	do{
		ExtFlash_Read_StatusRegister(&RecvData);
		i++;
		if (i>10000) // Timeout condition.
			return Flash_OperationTimeOut;
	}while( ~RecvData & SR_WEL_BIT ); // Check "Write Enable Latch" bit ('1' means write enabled).

	return Flash_Success;

}

/*****************************************************************************
 * Function name: ExtFlash_WriteDisable
 *
 * Description: This function clears ('0') "Write Enable Latch" (WEL) bit in STATUS Register.
 *              This operation disable WRITE STATUS REGISTER, PROGRAM, or ERASE operations.
 *
 *              Step 1: Send SPI command
 *              Step 2: Verify that write enable command was done.
 *                      (use timeout).
 *
 * Parameters:  None.
 * Returns:     Flash_OperationTimeOut.
 * 				Flash_Success
 *
 ******************************************************************************/
ExFlash_ReturnStatus ExtFlash_WriteDisable( void )
{
	uint8_t Command_Array[1];
	uint8_t len = 1;
	uint8_t RecvData;

	uint8_t i = 0;

	// Step 1: Send SPI command.
	Command_Array[0] = EXFLASH_COM_WRITE_DISABLE;
	SPI_Send(SPI_C_ID, Command_Array, len, DISABLE_CS_AFTER_SEND);


	// Step 2: Verify that write enable command was done.
	do{
		ExtFlash_Read_StatusRegister(&RecvData);
		i++;
		if (i>10000) // Timeout condition.
			return Flash_OperationTimeOut;
	}while( RecvData & SR_WEL_BIT ); // Check "Write Enable Latch" bit ('1' means write enabled).

	return Flash_Success;
}


/*****************************************************************************
 * Function name: ExtFlash_Read_FlagStatusRegister
 *
 * Description: Read "Flag Status Register" .
 *              This function must be called before WRITE STATUS REGISTER,PROGRAM, or ERASE operations.
 *
 * Parameters:  pData - a pointer to hold register content.
 * Returns:     ReturnStatus.
 *
 ******************************************************************************/
ExFlash_ReturnStatus ExtFlash_Read_FlagStatusRegister(uint8_t *pData)
{
	uint8_t Command_Array[1];
	uint8_t len = 1;

	Command_Array[0] = EXFLASH_COM_READ_FLAG_STATUS_REGISTER;

	// Send Flash command:
	SPI_Send(SPI_C_ID, Command_Array, len, NOT_DISABLE_CS_AFTER_SEND);
	// Recieve Flash FlagStatusRegister:
	SPI_Receive(SPI_C_ID, pData, 1);

	return Flash_Success;
}

/*****************************************************************************
 * Function name: ExtFlash_Read_StatusRegister
 *
 * Description: Read "Status Register" .
 *
 * Parameters:  pData - a pointer to hold register content.
 * Returns:     ReturnStatus.
 *
 ******************************************************************************/
ExFlash_ReturnStatus ExtFlash_Read_StatusRegister(uint8_t *pData)
{
	uint8_t Command_Array[1];
	uint8_t len = 1;

	Command_Array[0] = EXFLASH_COM_READ_STATUS_REGISTER;

	// Send Flash command:
	SPI_Send(SPI_C_ID, Command_Array, len, NOT_DISABLE_CS_AFTER_SEND);
	// Recieve Flash FlagStatusRegister:
	SPI_Receive(SPI_C_ID, pData, 1);

	return Flash_Success;
}

/*****************************************************************************
 * Function name: ExtFlash_Com_Clear_FlagStatusRegister
 *
 * Description:  Clear error bits of Flag Status Register.
 * 				- In "Flag Status Register" the error bits are "sticky" They must be cleared
 * 				  through the CLEAR STATUS REGISTER command.
 * 				- In case of write to protected area, "write enable latch" bit on "Status Register"
 * 				  will not be cleared by WRITE DISABLE command, only by this function.
 *
 * Parameters:  none.
 * Returns:     ReturnStatus.
 *
 ******************************************************************************/
ExFlash_ReturnStatus  ExtFlash_Clear_FlagStatusRegister( void )
{
	uint8_t Command_Array[1];
	uint8_t len = 1;

	Command_Array[0] = EXFLASH_COM_CLEAR_FLAG_STATUS_REGISTER;

	// Send Flash command:
	SPI_Send(SPI_C_ID, Command_Array, len, DISABLE_CS_AFTER_SEND);

	return Flash_Success;
}




/*******************************************************************************
Function:     ReturnType FlashEnter4ByteAddressMode(void)
Arguments:

Return Value:
   Flash_Success

Description:  This function set the 4-byte-address mode

Pseudo Code:
   Step 1: Write enable
   Step 2: Initialize the data (i.e. Instruction & value) packet to be sent serially
   Step 3: Send the packet serially
   Step 4: Wait until the operation completes or a timeout occurs.
*******************************************************************************/
ExFlash_ReturnStatus ExtFlash_Enter_4Byte_Address_Mode(void)
{
	uint8_t Fsr_Value;
	uint8_t Command_Array[1];
	uint8_t len = 1;

	Command_Array[0] = EXFLASH_COM_ENTER_4B_ADDR_MODE;

	// Step 3: Disable Write protection
	ExtFlash_WriteEnable();

	// Send Flash command:
	SPI_Send(SPI_C_ID, Command_Array, len, DISABLE_CS_AFTER_SEND);

	// Step 6: Wait until the operation completes or a timeout occurs.
	ExtFlash_Wait_Till_Command_Execution_Complete(SSE_TIMEOUT);

    // Read "Flag Status Register" (according to the datasheet it must be follow a Page Program operation).
	ExtFlash_Read_FlagStatusRegister(&Fsr_Value);

	// Evluate "Flag Status Register" error bits:
	if(Fsr_Value & FSR_ADDR_MODE_BIT)
		return Flash_Success;
	else
		return Flash_OpFailed;
}




/*******************************************************************************
*  Private Functions Prototypes
******************************************************************************/
/*****************************************************************************
 * Function name: Split_Addr_To_Byte_Array
 *
 * Description: Split address to array of bytes.
 *
 * Parameters:  Addr - address.
 * 				pArray - a pointer to array of 5 beytes.
 * 				Addr_Mode_Selector - Selector between 3/4 address byte.
 * 				'1' - 4 Byte address.
 * 				'0' - 3 Byte address.
 * Returns:     None.
 *
 *
 ******************************************************************************/
void Split_Addr_To_Byte_Array(uint32_t Addr, uint8_t *pArray, uint8_t Addr_Mode_Selector )
{
	// Array[0] is the command hex, the rest are the address.

	if(Addr_Mode_Selector)
	{	//4-addr byte mode
		pArray[1] = (uint8_t)((Addr>>24) & 0x000000FF);
		pArray[2] = (uint8_t)((Addr>>16) & 0x000000FF);
		pArray[3] = (uint8_t)((Addr>>8) & 0x000000FF);
		pArray[4] = (uint8_t)(Addr & 0x000000FF);
	}
	else
	{	// 3-addr byte mode
		pArray[1] = (uint8_t)((Addr>>16) & 0x000000FF);
		pArray[2] = (uint8_t)((Addr>>8) & 0x000000FF);
		pArray[3] = (uint8_t)(Addr & 0x000000FF);
	}

	return;
}

/*****************************************************************************
 * Function name:
 *
 * Description: This function wait till instruction execution is complete.
 *
 * Parameters:  None.
 * Returns:     None.
 *
 ******************************************************************************/
ExFlash_ReturnStatus ExtFlash_Wait_Till_Command_Execution_Complete(uint16_t second)
{
	ExtFlash_TimeOut(0); // Reset TimeOut counter

	while(IsFlashBusy())
	{
		if(Flash_OperationTimeOut == ExtFlash_TimeOut(second))
			return  Flash_OperationTimeOut;
	}

	return Flash_Success;
}

/*****************************************************************************
 * Function name:
 *
 * Description: .
 *
 * Parameters:  None.
 * Returns:     None.
 *
 ******************************************************************************/
ExFlash_ReturnStatus ExtFlash_TimeOut(uint32_t Seconds)
{
	static uint32_t Counter = 0;

	// Reset Timeout to 0:
	if (Seconds == 0)
	{
		Counter = 0;
	}
    // Compare Counter to requested Second:
	if (Counter >= (Seconds * COUNT_FOR_A_SECOND))
	{
		Counter = 0;
		return Flash_OperationTimeOut;
	}
	// Increment Counter:
	else
	{
		Counter++;
		return Flash_OperationOngoing;
	}
}


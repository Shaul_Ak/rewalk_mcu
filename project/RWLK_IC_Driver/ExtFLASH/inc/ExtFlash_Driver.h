/*******************************************************************************
 * File name: ExtFlash_Driver.h
 *
 * Description: 
 *              
 *
 * Note: Developed for TI F28377D.
 *
 * Author:	Yehuda Bitton
 * Date:    
 *
 ******************************************************************************/

/*******************************************************************************
  Multiple include protection
 ******************************************************************************/
#ifndef ExtFLASH_DRIVER_H
#define ExtFLASH_DRIVER_H

/*******************************************************************************
 *  Include
 ******************************************************************************/
#include "F28x_Project.h"     // Device Header file and Examples Include File
#include "inc/hw_types.h"

/*******************************************************************************
 *  Defines
 ******************************************************************************/
	//-------- FLASH Structure:
	#define		EXFLASH_SIZE 										0x8000000	// 128 MB
	#define		EXFLASH_DIE_SIZE									0x2000000	// 32  MB
	#define		EXFLASH_SECTOR_SIZE									0x10000 	// 64  KB
	#define		EXFLASH_SUBSECTOR_SIZE	 							0x1000 		// 4   KB
	#define		EXFLASH_PAGE_SIZE									0x100 		// 256 B
	#define		EXFLASH_OTP_SIZE									0x40		// 64  B

	#define		EXFLASH_NUM_OF_DIES									0x4
	#define		EXFLASH_NUM_OF_SECTORS								0x800 		// 2048
	#define		EXFLASH_NUM_OF_SUBSECTORS							0x8000 		// 32768
	#define		EXFLASH_NUM_OF_PAGES								0x80000

	#define		EXFLASH_DIE_SIZE_POWER_OF_TWO						25
	#define		EXFLASH_SECTOR_SIZE_POWER_OF_TWO					16
	#define		EXFLASH_SUBSECTOR_SIZE_POWER_OF_TWO					12

		//fdo->Desc.FlashAddressMask = 0x00FF;


	//------------ Command definitions:

	/* WRITE operations */
	#define		EXFLASH_COM_WRITE_ENABLE        						0x06	/* write enable */
	#define		EXFLASH_COM_WRITE_DISABLE        						0x04	/* write disable */

	/* REGISTER operations */
	#define		EXFLASH_COM_READ_STATUS_REGISTER      					0x05	/* read status register */
	#define 	EXFLASH_COM_READ_FLAG_STATUS_REGISTER					0x70	/* read flag status register */
	#define		EXFLASH_COM_CLEAR_FLAG_STATUS_REGISTER	    			0x50	/* clear flag status register */


//	SPI_FLASH_INS_WRSR      			= 0x01,	/* write status register */
//	SPI_FLASH_INS_RDLR                  = 0xE8, /* read lock register */
//	SPI_FLASH_INS_CMD_WRLR              = 0xE5, /* write lock register */
//	SPI_FLASH_INS_RDNVCR    			= 0xB5,	/* read non volatile configuration register */
//	SPI_FLASH_INS_WRNVCR    			= 0xB1,	/* write non volatile configuration register */
//	SPI_FLASH_INS_RDVCR     			= 0x85,	/* read volatile configuration register */
//	SPI_FLASH_INS_WRVCR     			= 0x81,	/* write volatile configuration register */
//	SPI_FLASH_INS_RDVECR    			= 0x65,	/* read volatile enhanced configuration register */
//	SPI_FLASH_INS_WRVECR    			= 0x61,	/* write volatile enhanced configuration register */
//
//	/* RESET Operations */
//#define		SPI_FLASH_INS_REN				  		 0x66	/* reset enable */
//#define		SPI_FLASH_INS_RMEM		  			     0x99	/* reset memory */
//
//	/* IDENTIFICATION Operations */
	#define 	EXFLASH_COM_READ_DEVICE_ID					        	0x9E	/* read Identification */
//	SPI_FLASH_INS_RDID_ALT    			= 0x9E,	/* read Identification */
//	SPI_FLASH_INS_MULT_IO_RDID   		= 0xAF, /* read multiple i/o read id */
//	SPI_FLASH_INS_DISCOVER_PARAMETER	= 0x5A, /* read serial flash discovery parameter */
//
//	/* READ operations */
	#define 	EXFLASH_COM_READ        								0x03	/* read data bytes */
	#define 	EXFLASH_COM_4BYTE_READ        							0x13	// read data @ 4 byte adress mode
//	SPI_FLASH_INS_FAST_READ   			= 0x0B,	/* read data bytes at higher speed */
//	SPI_FLASH_INS_DOFR       			= 0x3B,	/* dual output Fast Read */
//	SPI_FLASH_INS_DIOFR					= 0x0B, /* dual input output Fast Read */
//	SPI_FLASH_INS_DIOFR_ALT1			= 0x3B, /* dual input output Fast Read */
//	SPI_FLASH_INS_DIOFR_ALT2   			= 0xBB,	/* dual input output Fast Read */
//	SPI_FLASH_INS_QOFR        			= 0x6B,	/* quad output Fast Read */
//	SPI_FLASH_INS_QIOFR					= 0x0B, /* quad output Fast Read */
//	SPI_FLASH_INS_QIOFR_ALT1			= 0x6B, /* quad input/output Fast Read */
//	SPI_FLASH_INS_QIOFR_ALT2   			= 0xEB,	/* quad input output Fast Read */
//
//
//	/* PROGRAM operations */
	#define 	EXFLASH_COM_PAGE_PROGRAM          						0x02	/* PAGE PROGRAM */
//	SPI_FLASH_INS_PP4B					= 0x12, /* 4-BYTE PAGE PROGRAM (N25QxxxA8 only) */
//	SPI_FLASH_INS_DIPP        			= 0xA2,	/* DUAL INPUT FAST PROGRAM */
//	SPI_FLASH_INS_DIEPP					= 0x02, /* EXTENDED DUAL INPUT FAST PROGRAM */
//	SPI_FLASH_INS_DIEPP_ALT1			= 0xA2, /* EXTENDED DUAL INPUT FAST PROGRAM (alt 1) */
//	SPI_FLASH_INS_DIEPP_ALT2   			= 0xD2,	/* EXTENDED DUAL INPUT FAST PROGRAM (alt 2) */
//	SPI_FLASH_INS_QIPP        			= 0x32,	/* QUAD INPUT FAST PROGRAM */
//	SPI_FLASH_INS_QIPP4B				= 0x34, /* 4-BYTE QUAD INPUT FAST PROGRAM (N25QxxxA8 only) */
//	SPI_FLASH_INS_QIEPP					= 0x02, /* EXTENDED QUAD INPUT FAST PROGRAM */
//	SPI_FLASH_INS_QIEPP_ALT1			= 0x32, /* EXTENDED QUAD INPUT FAST PROGRAM (alt 1) */
//	SPI_FLASH_INS_QIEPP_ALT2			= 0x38, /* EXTENDED QUAD INPUT FAST PROGRAM */
//
//	SPI_FLASH_INS_QIEPP_ALT3			= 0x12, /* EXTENDED QUAD INPUT FAST PROGRAM (do not use in N25QxxxA8) */
//
//	/* ERASE operations */

	#define 	EXFLASH_COM_SUBSECTOR_ERASE								0x20
//	SPI_FLASH_INS_SSE         			= 0x20,	/* sub sector erase */
//	SPI_FLASH_INS_SSE4B					= 0x21, /* sub sector erase (N25QxxxA8 only) */
//	SPI_FLASH_INS_SE          			= 0xD8,	/* sector erase */
//	SPI_FLASH_INS_SE4B					= 0xDC, /* sector erase (N25QxxxA8 only) */
//	SPI_FLASH_INS_DE		  			= 0xC4,	/* die erase */
//	SPI_FLASH_INS_BE          			= 0xC7,	/* bulk erase (N25QxxxA8 only) */
//
//	SPI_FLASH_INS_PER         			= 0x7A,	/* program Erase Resume */
//	SPI_FLASH_INS_PES         			= 0x75,	/* program Erase Suspend */
//
//	/* OTP operations */
//	SPI_FLASH_INS_RDOTP					= 0x4B, /* read OTP array */
//	SPI_FLASH_INS_PROTP					= 0x42, /* program OTP array */
//
//	/* 4-BYTE ADDRESS operation  */
	#define 	EXFLASH_COM_ENTER_4B_ADDR_MODE								0xB7  // enter 4-byte address mode

//	SPI_FLASH_4B_MODE_EXIT	  			= 0xE9,	/* exit 4-byte address mode */
//
//	/* DEEP POWER-DOWN operation */
//	SPI_FLASH_INS_ENTERDPD				= 0xB9, /* enter deep power-down */
//	SPI_FLASH_INS_RELEASEDPD			= 0xA8  /* release deep power-down */

/*------------------------------------------------------------------------------
  Configurations
------------------------------------------------------------------------------*/
#define		COUNT_FOR_A_SECOND									200000

#define 	SSE_TIMEOUT 										2	// Sub Sector Erase Timeout in seconds.
#define 	SE_TIMEOUT  										6  	// Sector Erase Timeout in seconds.
#define 	DIE_TIMEOUT 										600	// Die Timeout in seconds.



/*******************************************************************************
 *  Data types
 ******************************************************************************/
typedef enum
{
	Flash_Success,
	Flash_AddressInvalid,
	Flash_ExceedPageSize,
	Flash_MemoryOverflow,
	Flash_PageEraseFailed,
	Flash_PageNrInvalid,
	Flash_SubSectorNrInvalid,
	Flash_SectorNrInvalid,
	Flash_FunctionNotSupported,
	Flash_NoInformationAvailable,
	Flash_OperationOngoing,
	Flash_OperationTimeOut,
	Flash_ProgramFailed,
	Flash_SectorProtected,
	Flash_SectorUnprotected,
	Flash_SectorProtectFailed,
	Flash_SectorUnprotectFailed,
	Flash_SectorLocked,
	Flash_SectorUnlocked,
	Flash_SectorLockDownFailed,
	Flash_WrongType,
	Flash_OpFailed
} ExFlash_ReturnStatus;

/*******************************************************************************
 *  Constants and Macros
 ******************************************************************************/

/*******************************************************************************
 *  Interface function deceleration:
 ******************************************************************************/
ExFlash_ReturnStatus ExtFlash_Program_UpToPage (uint32_t Addr, uint8_t *pDataArr, uint32_t DataArrLength );
bool IsFlashBusy(void);
ExFlash_ReturnStatus ExtFlash_WriteEnable (void);
ExFlash_ReturnStatus ExtFlash_WriteDisable(void);
ExFlash_ReturnStatus ExtFlash_Read_FlagStatusRegister (uint8_t *pData);
ExFlash_ReturnStatus ExtFlash_Read_StatusRegister (uint8_t *pData);
ExFlash_ReturnStatus ExtFlash_Clear_FlagStatusRegister (void);
ExFlash_ReturnStatus ExtFlash_Read_Data ( uint32_t Addr, uint8_t *pDataArr, uint32_t DataArrLength, uint8_t Select_4Byte_Mode );
ExFlash_ReturnStatus ExtFlash_Read_Device_Id (uint32_t *pData);
ExFlash_ReturnStatus ExtFlash_Erase_SubSector ( uint32_t SubSector_Num, uint8_t Select_4Byte_Mode );



/*******************************************************************************
 *  End of file
 ******************************************************************************/
#endif

#include "RWLK_IMU_driver.h"
#include "RWLK_SPI_Driver.h"
#include <math.h>
//#include <F2837xD_usDelay.asm>
#include "RWLK.h"


//**************************************************************************//
// File Description: This module implement the activation and usage of      //
//					 MPU6500 device (MotionInterface device of InvenSense.  //
//                   This is an inertial measurement unit (IMU).            //
//                   MPU communicates with the INF using the SPI protocol.  //																	//
//**************************************************************************//  
//#pragma CODE_SECTION(InitFlash, "ramfuncs");

/* Hold the four full scale values available for the Gyroscope */
static const float VELOCITY_FULL_SCALE_RANGE[4] =
{
	250.0f,
	500.0f,
	1000.0f,
	2000.0f
};


/* static functions */
static void IMU_write_to(uint8_t addr ,uint8_t data);
extern void F28x_usDelay(long LoopCount);
//--------------------------------------------------------------------------//
// Function:	IMU_init_device        										//
//																			//
// Parameters:	None														//
//																			//
// Return:		None														//
//																			//
// Description:	This function initiate MPU6000-IMU					        //  
//--------------------------------------------------------------------------//
void IMU_init_device(void)
{	
	
	////////////////////////////////////////
	// Initialize the SPI communication with the IMU 
	SPI_Init(SPI_A_IMU_ID , BITS_8 , IMU_SPI_BITRATE);

	////////////////////////////////////////
	// HW reset
	// Verify all registers will hold their default values, advice by data sheet for SPI users.
    IMU_write_to(MPUREG_PWR_MGMT_1, BIT_H_RESET);
	DELAY_US(100000L); // delay for 100 msec

	IMU_write_to(MPUREG_SIGNAL_PATH_RESET, BITS_ALL_SIGNALS_RESET);
	DELAY_US(100000);  // delay for 100 msec
    
	////////////////////////////////////////
	// HW configuration
	// Accel scale +/-2g (g=8192)
	IMU_write_to(MPUREG_ACCEL_CONFIG, BITS_FS_2G);           
	DELAY_US(1); // delay of 1 usec

	// Disable I2C bus (recommended on datasheet)
    IMU_write_to(MPUREG_USER_CTRL, BIT_I2C_IF_DIS);
    DELAY_US(1); // delay of 1 usec

	// Disable temperature (power management 1)
	IMU_write_to(MPUREG_PWR_MGMT_1, BIT_DISABLE_TEMPERATURE);
	DELAY_US(1); // delay of 1 usec

    // Enalbe acceleration and disable Gyro (power management 2)
	IMU_write_to(MPUREG_PWR_MGMT_2, BIT_DISABLE_GYRO_ENABLE_ACCELERATION);
	DELAY_US(1); // delay of 1 usec

    IMU_write_to(MPUREG_SMPLRT_DIV, 0x04);     // Sample rate = 200Hz    Fsample= 1Khz/(4+1) = 200Hz
    DELAY_US(1); // delay of 1 usec

}


//--------------------------------------------------------------------------//
// Function:	IMU_write_to        										//
//																			//
// Parameters:	addr - the address to write to								//
//				data - the value to write								    //
//																			//
// Return:		None														//
//																			//
// Description:	This function is writing the data to the address            //
//              (both are given as parameters) in IMU, SPI communication    //     
//--------------------------------------------------------------------------//
static void IMU_write_to(uint8_t addr ,uint8_t data)
{
	
	// Since IMU is using SPI B together with the motion driver, need to reconfigure the SPI
	// before handle it.
	SPI_Init_Registers(SPIB_BASE , BITS_8 , IMU_SPI_BITRATE);
	
	// tell first the address to which we want to write, and then the value to write.
	SPI_Tranceive(SPI_A_IMU_ID, &addr, 1, 0x0, 0);
	SPI_Tranceive(SPI_A_IMU_ID, &data, 1, 0x0, 0);
	
}

//--------------------------------------------------------------------------//
// Function:	IMU_read_data        										//
//																			//
// Parameters:	addr - the addresss to read from							//
//																			//
// Return:		return the value read 										//
//																			//
// Description:	This function read from IMU address given as a parameter    //
//              and returns the read value                                  //
//--------------------------------------------------------------------------//
uint8_t IMU_read_data(uint8_t addr)
{
	uint8_t c = 0u;
	uint8_t readAddr = addr | 0x80;  // according to MPU 6500 interface, address for reading will request 1 in the MSB.
	
	SPI_Init_Registers(SPIB_BASE , BITS_8 , IMU_SPI_BITRATE);

	SPI_Tranceive(SPI_A_IMU_ID, &readAddr, 1, &c, 1);
	DELAY_US(5); // delay of 5 usec

    return  c;

}


//--------------------------------------------------------------------------//
// Function:	IMU_read_raw_measurement        							//
//																			//
// Parameters:	Register address (name from IMU_driver.h file),				//
//				Give as parameter the high value of two registers value		//
//				
//																			//
// Return:		raw value of requested data									//
//																			//
// Description:	Reads mesurement from IMU, of the value its high register   //
//  			address is given as a parameter.						    //
// Example: If parameter gave is MPUREG_ACCEL_YOUT_H then the value returns //
//          is the raw value calculated from the two registers 				//
//			MPUREG_ACCEL_YOUT_H and MPUREG_ACCEL_YOUT_L                     //
//--------------------------------------------------------------------------//
inline uint16_t IMU_read_raw_measurement(uint8_t high_addr)
{
	uint16_t raw_value;
	
	raw_value = (((unsigned short)IMU_read_data(high_addr)) << 8)  | IMU_read_data(high_addr+1) ;

	return raw_value;
}


//--------------------------------------------------------------------------//
// Function:	IMU_read_acceleration        								//
//																			//
// Parameters:	Register address (name from IMU_driver.h file)				//
//																			//
// Return:		acceleration [g]											//
//																			//
// Description:	Return phisical value of acceleration mesurement, which its //
//              higher value register address is given as a parameter.      //
//--------------------------------------------------------------------------//
float IMU_read_acceleration(uint8_t high_addr)
{
	uint16_t raw_value;
	float physical_value, full_sclae_range;
	//uint8_t config_value;
	
	
	/* Calculate the full scale range from the accelerometer config register bits [4:3] */
	full_sclae_range = 2*((IMU_read_data(MPUREG_ACCEL_CONFIG) & 0x18)+1);
	raw_value = IMU_read_raw_measurement(high_addr);
	physical_value = (float)raw_value/32768.0f * full_sclae_range;

	return physical_value;
}


//--------------------------------------------------------------------------//
// Function:	IMU_read_angle_velocity        								//
//																			//
// Parameters:	Register address (name from IMU_driver.h file)				//
//																			//
// Return:		angular velocity											//
//																			//
// Description:	Return phisical value of velocity mesurement, which its		//
//              higher value register address is given as a parameter.      //
//--------------------------------------------------------------------------//
float IMU_read_angle_velocity(uint8_t high_addr)
{
	uint16_t raw_value;
	float physical_value, full_sclae_range;
	// config_value;
	
	
	/* Calculate the full scale range from the accelerometer config register bits [4:3] */
	full_sclae_range = VELOCITY_FULL_SCALE_RANGE[(IMU_read_data(MPUREG_GYRO_CONFIG) & 0x18)];
	raw_value = IMU_read_raw_measurement(high_addr);
	physical_value = (float)raw_value/32768.0f * full_sclae_range;

	return physical_value;
}	
	

//--------------------------------------------------------------------------//
// Function:	IMU_get_tilt_angle        									//
//																			//
// Parameters:	None														//
//																			//
// Return:		angle between Y and Z acclerations	    					//
//																			//
// Description:	Reads raw Y and Z acceleration angles from the IMU and      //
//              calculate the angle between them						    //
//--------------------------------------------------------------------------//
float IMU_get_tilt_angle(void)
{
 	short accel_y_raw, accel_z_raw;
	float calculated_angle = 0;
 	
	// read raw values of y and z acceleration, for each axis read high and low register and built the value
	accel_y_raw = ((((short)IMU_read_data(MPUREG_ACCEL_YOUT_H)) << 8)  | IMU_read_data(MPUREG_ACCEL_YOUT_L)) ;
	accel_z_raw = ((((short)IMU_read_data(MPUREG_ACCEL_ZOUT_H)) << 8)  | IMU_read_data(MPUREG_ACCEL_ZOUT_L)) ;

	/* avoid devide by zero by using atan2 */
	/* atan2 calculates the arctangent of y/x (if x equals 0, atan2 returns p/2 if y is positive, */
	/* -p/2 if y is negative, or 0 if y is 0.) */	
	calculated_angle = (atan2(accel_z_raw,accel_y_raw))*57.29578; // rad to degree
	return calculated_angle; 	
	
}

//--------------------------------------------------------------------------//
// Function:	IMU_get_roll_angle        									//
//																			//
// Parameters:	None														//
//																			//
// Return:		angle between Y and Z acclerations	    					//
//																			//
// Description:	Reads raw Y and Z acceleration angles from the IMU and      //
//              calculate the angle between them						    //
//--------------------------------------------------------------------------//
float IMU_get_roll_angle(void)
{
 	short accel_y_raw, accel_x_raw;
	float calculated_angle = 0;
 	
	accel_y_raw = ((((short)IMU_read_data(MPUREG_ACCEL_YOUT_H)) << 8)  | IMU_read_data(MPUREG_ACCEL_YOUT_L)) ;
	accel_x_raw = ((((short)IMU_read_data(MPUREG_ACCEL_XOUT_H)) << 8)  | IMU_read_data(MPUREG_ACCEL_XOUT_L)) ;

	/* avoid devide by zero */
	/*atan2 calculates the arctangent of y/x (if x equals 0, atan2 returns p/2 if y is positive, -p/2 if y is negative, or 0 if y is 0.) */
	calculated_angle = (atan2(-accel_x_raw,accel_y_raw))*57.29578; // rad to degree
	
	return calculated_angle; 	
	
}

//--------------------------------------------------------------------------//
// Function:	IMU_Sbit	      										    //
//																			//
// Parameters:	None														//
//																			//
// Return:		true for pass/false for fail								//
//																			//
// Description:	Verify device is working, communicating and alive			//  
//--------------------------------------------------------------------------//
bool IMU_Sbit(void)
{
	uint8_t device_Id;
	uint16_t accel_x_without_test, accel_y_without_test, accel_z_without_test;
	uint16_t accel_x_with_test, accel_y_with_test, accel_z_with_test;
	
	//////////////////////////////////
	/* Verify who_am_I register return the right value, mean communication with the device is OK */
	device_Id = IMU_read_data(MPUREG_WHOAMI);
	if (!((device_Id == MPU6000) || (device_Id == MPU6500)))
	{
		/* TODO: orit */
		//PrepareLogMessage(LOG_MESSAGE_IMU,LOG_MESSAGE_SBIT,LOG_MESSAGE_COMMUNICATION_ERROR,0,0,0,0,0,0,0);
		//WriteToLog(LOG_INF_Card_Num,IMU_Module_Num,LOG_ERROR,3,Log_Message);
		return false;
	}
	//////////////////////////////////
	/* In order to verify liveness of the sensor, read accel_Y and accel_Z and verify before and after bit values are different */
	accel_y_without_test = IMU_read_raw_measurement(MPUREG_ACCEL_YOUT_H);
	accel_z_without_test = IMU_read_raw_measurement(MPUREG_ACCEL_ZOUT_H);
	accel_x_without_test = IMU_read_raw_measurement(MPUREG_ACCEL_XOUT_H);	 
	/* Activate bit */
	IMU_write_to(MPUREG_ACCEL_CONFIG,BITS_ACTIVATE_ACCELERATION_SELF_TEST);
	DELAY_US(1000); // delay for 1 msec

	accel_y_with_test = IMU_read_raw_measurement(MPUREG_ACCEL_YOUT_H);
	accel_z_with_test = IMU_read_raw_measurement(MPUREG_ACCEL_ZOUT_H);
	accel_x_with_test = IMU_read_raw_measurement(MPUREG_ACCEL_XOUT_H);	 	
	/* Disable self test */
	IMU_write_to(MPUREG_ACCEL_CONFIG, BITS_FS_2G); 
	DELAY_US(1); // delay of 1 usec
	/* Compare values before with and without the self test, must be different */
	if ((accel_x_without_test != accel_x_with_test) && (accel_y_without_test != accel_y_with_test) && 
		(accel_z_without_test != accel_z_with_test))
		return true;
	else
	{
		/* TODO: orit */
		//PrepareLogMessage(LOG_MESSAGE_IMU,LOG_MESSAGE_SBIT,LOG_MESSAGE_FREEZE_DATA,0,0,0,0,0,0,0);
		//WriteToLog(LOG_INF_Card_Num,IMU_Module_Num,LOG_ERROR,3,Log_Message);
		return false;
	}
} 


//--------------------------------------------------------------------------//
// Function:	IMU_Cbit	      										    //
//																			//
// Parameters:	None														//
//																			//
// Return:		1 for pass/ 0 for fail										//
//																			//
// Description:	Activate MPU 6000 periodic bit. part of the error handling  //  
//--------------------------------------------------------------------------//
bool IMU_Cbit(void)
{	
	bool result = true;
	static bool prev_result = true;
	uint8_t device_Id;
	
	//////////////////////////////////
	/* Verify who_am_I register return the right value, means communication with the device is OK */
	device_Id = IMU_read_data(MPUREG_WHOAMI);
	if (!((device_Id == MPU6000) || (device_Id == MPU6500)))
	{
		result = false;
		if (prev_result != result)
		{
			/* TODO: orit */
			//PrepareLogMessage(LOG_MESSAGE_IMU, LOG_MESSAGE_COMMUNICATION_ERROR,0,0,0,0,0,0,0,0);
			//WriteToLog(LOG_INF_Card_Num,IMU_Module_Num,LOG_ERROR,3,Log_Message);
		}
		////////////////
		// Try to recover IMU, for next CBIT activation
		IMU_init_device();				
	}
	else
	{
		// report recover only upon change
		if (prev_result != result)
		{
			/* TODO: orit */
			//PrepareLogMessage(LOG_MESSAGE_IMU, LOG_MESSAGE_COMMUNICATION_RECOVER,0,0,0,0,0,0,0,0);
			//WriteToLog(LOG_INF_Card_Num,IMU_Module_Num,LOG_ERROR,3,Log_Message);
		}
	}
	
	// keep result for next cbit activation 
	prev_result = result;
	return result;
} 


/*
 * RWLK_DRV8031_Applications.h
 *
 *  Created on: Jul 21, 2016
 *      Author: arie
 */

#ifndef RWLK_APPLICATION_INC_RWLK_DRV8031_APPLICATIONS_H_
#define RWLK_APPLICATION_INC_RWLK_DRV8031_APPLICATIONS_H_


#include "RWLK.h"
/*#include "F2837xD_device.h"
#include "F2837xD_GlobalPrototypes.h"
#include "F2837xD_Gpio_defines.h"
#include "F2837xD_Gpio.h"
#include <stdint.h>
#include "F2837xD_Examples.h"
#include "RWLK_Common.h"
#include "RWLK_DRV8031_SPI_Driver.h"
#include "RWLK_SPI_Driver.h"
*/


/*==============================================================================
                               drv8031 IO Defines
==============================================================================*/

#if BOOSTXL == 0
	#define EN_GATE			118 // DRV8031 Enable/Disable gate driver and current shunt amplifier
	#define DC_CAL			117 // Device shorts inputs of shunt amplifier + disconnets load
	#define FAULT			62 // Fault report generator
	#define OCTW			67 // Owercurrent and Overtemperature warning


	#define XEN_GATE_SET		GpioDataRegs.GPDSET.bit.GPIO118
	#define XEN_GATE_CLEAR		GpioDataRegs.GPDCLEAR.bit.GPIO118

	#define YEN_GATE_SET		GpioDataRegs.GPDSET.bit.GPIO114
	#define YEN_GATE_CLEAR		GpioDataRegs.GPDCLEAR.bit.GPIO114

	#define DC_CAL_SET			GpioDataRegs.GPDSET.bit.GPIO117
	#define DC_CAL_CLEAR		GpioDataRegs.GPDCLEAR.bit.GPIO117

	#define YDC_CAL_SET			GpioDataRegs.GPDSET.bit.GPIO113
	#define YDC_CAL_CLEAR		GpioDataRegs.GPDCLEAR.bit.GPIO113

	#define nSPI_A_CS_X_AXIS	106 //
	#define nSPI_A_CS_Y_AXIS	66 //
	#define nSPI_B_CS_IMU	28  //
#else
	#define EN_GATE			88 // DRV8031 Enable/Disable gate driver and current shunt amplifier
	#define DC_CAL			89 // Device shorts inputs of shunt amplifier + disconnets load
	#define nSPI_A_CS_MOT	91 //MOtor driver CS

	#define EN_GATE_SET			GpioDataRegs.GPCSET.bit.GPIO88
	#define XEN_GATE_CLEAR		GpioDataRegs.GPCCLEAR.bit.GPIO88

	#define DC_CAL_SET			GpioDataRegs.GPCSET.bit.GPIO89
	#define DC_CAL_CLEAR		GpioDataRegs.GPCCLEAR.bit.GPIO89

#endif
	#define GPIO_FUNCTION	0  // BASIC GPIO Function enable





 typedef struct _DRV8031_DATA_READ
 		{
 		uint8_t Reg1_DataLow;
 		uint8_t Reg1_DataHigh;
 		uint8_t Reg2_DataLow;
 		uint8_t Reg2_DataHigh;
 		}sDrv8031DataRead;

extern sDrv8031DataRead sDrv8031DataReadSend;








//uint16_t DRV8031_Command;

/*==============================================================================
                               drv8031 Functions Prototypes
==============================================================================*/

extern void DRV8031_Gpio_Init(void);
extern void DRV8031_Init(uint32_t SPI_Base , uint32_t SPI_Length , uint32_t SPI_Bit_Rate );
extern void DRV8031_Init_Registers(uint32_t SPI_Base , uint32_t SPI_Length , uint32_t SPI_Bit_Rate);
extern uint16_t DRV8031_SPI_TxRx(uint16_t *Address1Result, uint16_t *Address2Result, uint16_t Axis);
extern void DRV8031_SPI_nCS_Set(uint16_t AxisDriverNumber);
extern void DRV8031_SPI_nCS_Reset(void);
extern void DRV8031_SPI_CS_Toggle(void);
extern void DRV8031_ReadDataParser( uint16_t *Register1Data, uint16_t *Register2Data, struct _DRV8031_DATA_READ *TxsDrv8031Data);

#endif /* RWLK_APPLICATION_INC_RWLK_DRV8031_APPLICATIONS_H_ */

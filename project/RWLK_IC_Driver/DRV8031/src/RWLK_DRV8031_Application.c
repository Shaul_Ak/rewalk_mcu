/*
 * RWLK_DRV8031_Application.c
 *
 *  Created on: Jul 21, 2016
 *      Author: arie
 */
#include "RWLK.h"
#include "F2837xD_device.h"
#include "RWLK_Motor_Control.h"
#include "RWLK_DRV8031_Motor_Drv.h"
#include "RWLK_DRV8031_Applications.h"
#include "RWLK_SPI_Driver.h"





#define ERR_OK                  0x00




sDrv8031DataRead sDrv8031DataReadSend;
/*==============================================================================
Function : DRV8031_Init

Description : This function initialize 8031 driver

SPI_Base 		: SPI base address - Options (A channel , B channel)
SPI_Length 	: SPI Length (8bit or 16bit)
SPI_Bit_Rate 	: SPI Frequency (CAN_BUS_SPEED_1MHZ , CAN_BUS_SPEED_500KHZ)
==============================================================================*/
void DRV8031_Init(uint32_t SPI_Base , uint32_t SPI_Length , uint32_t SPI_Bit_Rate )
{
	DRV8031_Gpio_Init();
	SPI_Gpio_Init(SPI_Base);
	//DRV8031_SPI_nCS_Set();
	DELAY_US(50000);
#if  BOOSTXL == 0
	XEN_GATE_CLEAR = 1;
#else
	EN_GATE_SET = 1;
#endif
#if  BOOSTXL == 0
	DC_CAL_SET = 1;
#else
	DC_CAL_CLEAR = 1;
#endif

	DELAY_US(50000);		//delay to allow DRV830x supplies to ramp up

#if  BOOSTXL == 0
	XEN_GATE_SET = 1;
#else
	XEN_GATE_CLEAR = 1;
#endif

#if  BOOSTXL == 0
	DC_CAL_CLEAR = 1;
#else
	DC_CAL_SET = 1;
#endif

	DELAY_US(50000);		//delay to allow DRV830x supplies to ramp up
	//SPI_Init(SPI_Base , SPI_Length , SPI_Bit_Rate );
if(SPI_Base == SPIB_BASE)
{
	DRV8301_SPI_Init(&SpibRegs);
}
else if(SPI_Base == SPIA_BASE)
{
	DRV8301_SPI_Init(&SpiaRegs);
}
	DELAY_US(50000);

	//write to DRV8301 control register 1, returns status register 1
	if(SPI_Base == SPIB_BASE)
	{
		XDrv8031CntrlReg1_Prepare();
		Drv8031DataReg1 = DRV8301_SPI_Write(&SpibRegs,CNTRL_REG_1_ADDR,DRV8301_cntrl_reg1.all, X_Axis);
		DRV8031_SPI_nCS_Reset();
		YDrv8031CntrlReg1_Prepare();
		Drv8031DataReg1 = DRV8301_SPI_Write(&SpibRegs,CNTRL_REG_1_ADDR,DRV8301_cntrl_reg1.all, Y_Axis);
		DRV8031_SPI_nCS_Reset();
	}
	else if (SPI_Base == SPIA_BASE)
	{
		XDrv8031CntrlReg1_Prepare();
		Drv8031DataReg1 = DRV8301_SPI_Write(&SpiaRegs,CNTRL_REG_1_ADDR,DRV8301_cntrl_reg1.all, X_Axis);
		DRV8031_SPI_nCS_Reset();
		YDrv8031CntrlReg1_Prepare();
		Drv8031DataReg1 = DRV8301_SPI_Write(&SpiaRegs,CNTRL_REG_1_ADDR,DRV8301_cntrl_reg1.all, Y_Axis);
		DRV8031_SPI_nCS_Reset();
	}
	if(Drv8031DataReg1 != DRV8301_cntrl_reg1.all)
	{
		//ESTOP0;
	}
	//write to DRV8301 control register 2, returns status register 1
	if(SPI_Base == SPIB_BASE)
	{
		XDrv8031CntrlReg2_Prepare();
		Drv8031DataReg2 = DRV8301_SPI_Write(&SpibRegs,CNTRL_REG_2_ADDR,DRV8301_cntrl_reg2.all, X_Axis);
		DRV8031_SPI_nCS_Reset();
		YDrv8031CntrlReg2_Prepare();
		Drv8031DataReg2 = DRV8301_SPI_Write(&SpibRegs,CNTRL_REG_2_ADDR,DRV8301_cntrl_reg2.all, Y_Axis);
		DRV8031_SPI_nCS_Reset();
	}
	else if (SPI_Base == SPIA_BASE)
	{
		XDrv8031CntrlReg2_Prepare();
		Drv8031DataReg2 = DRV8301_SPI_Write(&SpiaRegs,CNTRL_REG_2_ADDR,DRV8301_cntrl_reg2.all, X_Axis );
		DRV8031_SPI_nCS_Reset();
		YDrv8031CntrlReg2_Prepare();
		Drv8031DataReg2 = DRV8301_SPI_Write(&SpiaRegs,CNTRL_REG_2_ADDR,DRV8301_cntrl_reg2.all, Y_Axis);
		DRV8031_SPI_nCS_Reset();
	}
	if(Drv8031DataReg2 != DRV8301_cntrl_reg2.all)
	{
		//ESTOP0;
	}
	DRV8031_SPI_nCS_Reset();
	DELAY_US(50000);
	//DRV8031_SPI_nCS_Reset();
}



/*==============================================================================
Function : DRV8031_Gpio_Init

Description : This function initialize 8031 driver GPIO

==============================================================================*/
void DRV8031_Gpio_Init(void)
{
	EALLOW;
	GPIO_SetupPinMux(EN_GATE, GPIO_MUX_CPU1, GPIO_FUNCTION); // Enable DRV8031 Enable/Disable gate driver and current shunt amplifier
	GPIO_SetupPinOptions(EN_GATE, GPIO_OUTPUT, GPIO_ASYNC);//define GPIO As output

	GPIO_SetupPinMux(DC_CAL, GPIO_MUX_CPU1, GPIO_FUNCTION); // Enable DC_CAL GPIO(description in header)
	GPIO_SetupPinOptions(DC_CAL, GPIO_OUTPUT, GPIO_ASYNC);//define GPIO As output

	GPIO_SetupPinMux(nSPI_A_CS_X_AXIS, GPIO_MUX_CPU1, GPIO_FUNCTION); // CS for motor driver, CPU1
	GPIO_SetupPinOptions(nSPI_A_CS_X_AXIS, GPIO_OUTPUT, GPIO_ASYNC);  // define GPIO As output

	GPIO_SetupPinMux(nSPI_A_CS_Y_AXIS, GPIO_MUX_CPU1, GPIO_FUNCTION); // CS for motor driver, CPU1
	GPIO_SetupPinOptions(nSPI_A_CS_Y_AXIS, GPIO_OUTPUT, GPIO_ASYNC);  // define GPIO As output

#if BOOSTXL == 1
	GPIO_SetupPinMux(FAULT, GPIO_MUX_CPU1, GPIO_FUNCTION); // Enable Fault report generator GPIO
	GPIO_SetupPinOptions(FAULT, GPIO_INPUT, GPIO_ASYNC);//define FAULT GPIO As input


	GPIO_SetupPinMux(OCTW, GPIO_MUX_CPU1, GPIO_FUNCTION); // Enable Owercurrent and Overtemperature warning GPIO
	GPIO_SetupPinOptions(OCTW, GPIO_INPUT, GPIO_ASYNC);//define OCTW GPIO As input

	GPIO_SetupPinMux(nSPI_A_CS_X_AXIS, GPIO_MUX_CPU1, GPIO_FUNCTION); // CS for motor driver, CPU1
	GPIO_SetupPinOptions(nSPI_A_CS_X_AXIS, GPIO_OUTPUT, GPIO_ASYNC);  // define GPIO As output
#endif

	EDIS;
}


/*==============================================================================
Function : DRV8031_SPI_Transmit

Description : This function initialize 8031 driver

SPI_Base 		: SPI base address - Options (A channel , B channel)
SPI_Length 	: SPI Length (8bit or 16bit)
SPI_Bit_Rate 	: SPI Frequency (CAN_BUS_SPEED_1MHZ , CAN_BUS_SPEED_500KHZ)
==============================================================================*/
uint16_t DRV8031_SPI_TxRx(uint16_t *Address1Result, uint16_t *Address2Result, uint16_t Axis)
{

	uint16_t  ErrCode = ERR_OK;
	//SPI_Transmit(*TxData);
	//ErrCode = Spi_GetData( Result );
	//DRV8301_stat_reg1.all = DRV8301_SPI_Read(&SpiaRegs,STAT_REG_1_ADDR);
	//DRV8301_stat_reg2.all = DRV8301_SPI_Read(&SpiaRegs,STAT_REG_2_ADDR);
	if(Axis == X_Axis)
	{
		*Address1Result = DRV8301_SPI_Read(&SpiaRegs,CNTRL_REG_1_ADDR,X_Axis);
		*Address2Result = DRV8301_SPI_Read(&SpiaRegs,CNTRL_REG_2_ADDR,X_Axis);
	}
	else
	{
		*Address1Result = DRV8301_SPI_Read(&SpiaRegs,CNTRL_REG_1_ADDR,Y_Axis);
		*Address2Result = DRV8301_SPI_Read(&SpiaRegs,CNTRL_REG_2_ADDR,Y_Axis);
	}

	return ErrCode;

}


/*==============================================================================
Function : DRV8031_CS_Set

Description : This function set driver CS

==============================================================================*/
void DRV8031_SPI_nCS_Set(uint16_t AxisDriverNumber)
{
	switch(AxisDriverNumber)
{
	case X_Axis:
		//GpioDataRegs.GPDCLEAR.bit.GPIO106 = 1;
		XDRV8031_CS_Set = 1;
		break;

	case Y_Axis:
		//GpioDataRegs.GPCCLEAR.bit.GPIO66 = 1;
		YDRV8031_CS_Set = 1;
		break;


	}



	//GpioDataRegs.GPCCLEAR.bit.GPIO91 = 1;
}

/*==============================================================================
Function : DRV8031_CS_Reset

Description : This function reset driver CS

==============================================================================*/
void DRV8031_SPI_nCS_Reset(void)
{
	//GpioDataRegs.GPDSET.bit.GPIO106 = 1;
	//GpioDataRegs.GPCSET.bit.GPIO66  = 1;
	XDRV8031_CS_Reset = 1;
	YDRV8031_CS_Reset = 1;
	//GpioDataRegs.GPCSET.bit.GPIO91 = 1;
}


/*==============================================================================
Function : DRV8031_SPI_CS_Toggle

Description : This function reset driver CS

==============================================================================*/
void DRV8031_SPI_CS_Toggle(void)
{
	GpioDataRegs.GPDTOGGLE.bit.GPIO106 = 1;
}



/*==============================================================================
Function : DRV8031_ReadDataParser

Description : This function reads received packet

==============================================================================*/
void DRV8031_ReadDataParser( uint16_t *Register1Data, uint16_t *Register2Data, struct _DRV8031_DATA_READ *TxsDrv8031Data)
{

	uint16_t TimePar;

	TimePar = *Register1Data;
	TxsDrv8031Data ->Reg1_DataLow = (uint8_t)(TimePar & 0x00ff);
	TimePar >>= 8;
	TxsDrv8031Data ->Reg1_DataHigh = (uint8_t)(TimePar & 0x00ff);

	TimePar = *Register2Data;
	TxsDrv8031Data ->Reg2_DataLow = (uint8_t)(TimePar & 0x00ff);
	TimePar >>= 8;
	TxsDrv8031Data ->Reg2_DataHigh = (uint8_t)(TimePar & 0x00ff);
}


void XDrv8031CntrlReg1_Prepare(void)
{
	DRV8301_cntrl_reg1.bit.GATE_CURRENT = 0;		// full current 1.7A
//			DRV8301_cntrl_reg1.bit.GATE_CURRENT = 1;		// med current 0.7A
//			DRV8301_cntrl_reg1.bit.GATE_CURRENT = 2;		// min current 0.25A
	DRV8301_cntrl_reg1.bit.GATE_RESET = 0;			// Normal Mode
	DRV8301_cntrl_reg1.bit.PWM_MODE = 0;			// six independant PWMs
//			DRV8301_cntrl_reg1.bit.OC_MODE = 0;				// current limiting when OC detected
	//DRV8301_cntrl_reg1.bit.OC_MODE = 1;				// latched OC shutdown
			DRV8301_cntrl_reg1.bit.OC_MODE = 2;				// Report on OCTWn pin and SPI reg only, no shut-down
//			DRV8301_cntrl_reg1.bit.OC_MODE = 3;				// OC protection disabled
//			DRV8301_cntrl_reg1.bit.OC_ADJ_SET = 0;			// OC @ Vds=0.060V
//			DRV8301_cntrl_reg1.bit.OC_ADJ_SET = 4;			// OC @ Vds=0.097V
//			DRV8301_cntrl_reg1.bit.OC_ADJ_SET = 6;			// OC @ Vds=0.123V
//			DRV8301_cntrl_reg1.bit.OC_ADJ_SET = 9;			// OC @ Vds=0.175V
	DRV8301_cntrl_reg1.bit.OC_ADJ_SET = 15;					// OC @ Vds=0.358V
//			DRV8301_cntrl_reg1.bit.OC_ADJ_SET = 16;			// OC @ Vds=0.403V
//			DRV8301_cntrl_reg1.bit.OC_ADJ_SET = 17;			// OC @ Vds=0.454V
//			DRV8301_cntrl_reg1.bit.OC_ADJ_SET = 18;			// OC @ Vds=0.511V
	DRV8301_cntrl_reg1.bit.Reserved = 0;
}



void YDrv8031CntrlReg1_Prepare(void)
{
	DRV8301_cntrl_reg1.bit.GATE_CURRENT = 0;		// full current 1.7A
//			DRV8301_cntrl_reg1.bit.GATE_CURRENT = 1;		// med current 0.7A
//			DRV8301_cntrl_reg1.bit.GATE_CURRENT = 2;		// min current 0.25A
	DRV8301_cntrl_reg1.bit.GATE_RESET = 0;			// Normal Mode
	DRV8301_cntrl_reg1.bit.PWM_MODE = 0;			// six independant PWMs
//			DRV8301_cntrl_reg1.bit.OC_MODE = 0;				// current limiting when OC detected
	//DRV8301_cntrl_reg1.bit.OC_MODE = 1;				// latched OC shutdown
			DRV8301_cntrl_reg1.bit.OC_MODE = 2;				// Report on OCTWn pin and SPI reg only, no shut-down
//			DRV8301_cntrl_reg1.bit.OC_MODE = 3;				// OC protection disabled
//			DRV8301_cntrl_reg1.bit.OC_ADJ_SET = 0;			// OC @ Vds=0.060V
//			DRV8301_cntrl_reg1.bit.OC_ADJ_SET = 4;			// OC @ Vds=0.097V
//			DRV8301_cntrl_reg1.bit.OC_ADJ_SET = 6;			// OC @ Vds=0.123V
//			DRV8301_cntrl_reg1.bit.OC_ADJ_SET = 9;			// OC @ Vds=0.175V
	DRV8301_cntrl_reg1.bit.OC_ADJ_SET = 15;					// OC @ Vds=0.358V
//			DRV8301_cntrl_reg1.bit.OC_ADJ_SET = 16;			// OC @ Vds=0.403V
//			DRV8301_cntrl_reg1.bit.OC_ADJ_SET = 17;			// OC @ Vds=0.454V
//			DRV8301_cntrl_reg1.bit.OC_ADJ_SET = 18;			// OC @ Vds=0.511V
	DRV8301_cntrl_reg1.bit.Reserved = 0;
}



void XDrv8031CntrlReg2_Prepare(void)
{
	//			DRV8301_cntrl_reg2.bit.OCTW_SET = 0;			// report OT and OC
		DRV8301_cntrl_reg2.bit.OCTW_SET = 1;			// report OT only

		DRV8301_cntrl_reg2.bit.GAIN = 3;				// CS amplifier gain = 80
	    //GpioDataRegs.GPACLEAR.bit.GPIO30 = 1; // Lower GPIO30, trigger XINT1

		DRV8301_cntrl_reg2.bit.DC_CAL_CH1 = 0;			// not in CS calibrate mode
		DRV8301_cntrl_reg2.bit.DC_CAL_CH2 = 0;			// not in CS calibrate mode
		DRV8301_cntrl_reg2.bit.OC_TOFF = 0;				// normal mode
		DRV8301_cntrl_reg2.bit.Reserved = 0;
}



void YDrv8031CntrlReg2_Prepare(void)
{
	//			DRV8301_cntrl_reg2.bit.OCTW_SET = 0;			// report OT and OC
		DRV8301_cntrl_reg2.bit.OCTW_SET = 1;			// report OT only

		DRV8301_cntrl_reg2.bit.GAIN = 1;				// CS amplifier gain = 20
	    //GpioDataRegs.GPACLEAR.bit.GPIO30 = 1; // Lower GPIO30, trigger XINT1

		DRV8301_cntrl_reg2.bit.DC_CAL_CH1 = 0;			// not in CS calibrate mode
		DRV8301_cntrl_reg2.bit.DC_CAL_CH2 = 0;			// not in CS calibrate mode
		DRV8301_cntrl_reg2.bit.OC_TOFF = 0;				// normal mode
		DRV8301_cntrl_reg2.bit.Reserved = 0;
}






//No More

#include "RWLK_MELODY.h"

const char MelodyBLU_Commands[MELODY_SUPPORT_COMMANDS_NUMBER][MELODY_SUPPORT_COMMAND_STRING_LENGTH] = {
    "reset\r",            // Reset Bluetooth Module
    "inquiry 30\r",
    "list\r",
    "status\r",          //Blue status command
    "status",            //ask about link status
    "config\r",         // read blue module configuration
    "advertising",     //advertising on/off  for BLE devices
    "discoverable",  //discoverable on/off
    "close 15",           // Close link / all
    "name",
    "open",
    "pair",
    "unpair\r",//unpair all devices
    "unpair", //unpair BT address
    "version\r",
    "write\r",
    "$$$$", //Exit_Data_Mode command
    "restore\r",
    "enter_data_mode 15\r", //Enter data mode (link)
    "dfu\r",
    "power",
    "send 15",
    "send_raw",
    "passkey",
    "scan"

};

// Melody Notifications

const char MelodyBLU_Notifications[MELODY_SUPPORT_NOTIFICATIONS_NUMBER][MELODY_SUPPORT_NOTIFICATION_STRING_LENGTH] = {
	"OK",
    "ERROR",           //Error then error number
    "OPEN_OK",
    "OPEN_OK 14",      //OPEN  BLE profile
    "OPEN_OK 15",      //OPEN  SPP
    "OPEN_OK 24",      //OPEN  BLE profile
    "OPEN_OK 25",      //OPEN  SPP
    "OPEN_ERROR",
    "PAIR_OK",      //OPEN SPP or  BLE profile
    "PAIR_ERROR",
    "Ready",          //Ready after reset command
    "RECV",         //Recv data from remote
    "NAME",        //Get BT device name
    "CLOSE_OK",
    "CLOSE_OK 14",
    "CLOSE_OK 15",
    "CLOSE_OK 24",
    "CLOSE_OK 25",
    "SCAN",
    "ENRCU",
    "NONE"
};



const char MelodyBLU_ErrorCodes[MELODY_SUPPORT_ERROR_CODES_NUMBER][MELODY_SUPPORT_ERROR_CODES_STRING_LENGTH] = {

	"ERROR 0x0003",
    "ERROR 0x0011",
    "ERROR 0x0012",
    "ERROR 0x0013",
    "ERROR 0x0014",
    "ERROR 0x0015",
    "ERROR 0x0016",
    "ERROR 0x0017",
    "ERROR 0x0018"

};



// Melody Configuration

const char MelodyBLU_Configuration[MELODY_SUPPORT_CONFIGURATIONS_NUMBER][MELODY_SUPPORT_CONFIGURATIONS_STRING_LENGTH] = {
    "autoconn",            // Reset Bluetooth Module
    "bc_smart_uuids",
    "ble_config",
    "ble_conn_params",
    "cmd_to",   // read blue module configuration
    "cod",
    "conn_to", //advertising on for BLE devices
    "deep_sleep", //advertising off
    "device_id",
    "discoverable",
    "enable_capsense",
    "enable_led",
    "enable_spp_sniff",
    "local_addr",
    "name",
    "name_short",
    "pin",
    "profiles",
    "remote_addr",
    "spp_high_speed",
    "spp_uuids",
    "ssp_caps",
    "uart_config",
    "vreg_role"

};

MELODY_STATUS  Get_OK_or_ERROR_Notify = MELODY_IDLE ;


//functions
void Set_Blue_Melody_Status(MELODY_STATUS notify)
{
	Get_OK_or_ERROR_Notify = notify;
}

MELODY_STATUS  Get_Blue_Melody_Status(void)
{
	 return (Get_OK_or_ERROR_Notify);
}



MELODY_STATUS Send_Melody_Command(MELODY_Commands command)
{
	uint32_t timeout=0 ,timeout_diff=0;
	const char *blue_recv;

	blue_recv = Init_Blue_Recv_Data_Memory();

	switch(command)
	{
	case ENTER_DATA_MODE_GUI:
		  memset((void *)blue_recv,0,MAX_SCI_RECV_BYTES_COUNT);
		  Reset_SCIA_Recv_Byte_Index();
		  scia_msg_melody_com_config(MelodyBLU_Commands[command] );
		  DELAY_US(10000);
	      timeout = D_Timer_Get_mSec_Counter();
	      while(timeout_diff<BLUE_COMMAND_NOTIFY_TIMEOUT )
	      {
		     if(NULL != strstr(blue_recv , MelodyBLU_Notifications[OK_BLU])) //OPEN SPP link by GUI
				  {
				   memset((void *)blue_recv,0,MAX_SCI_RECV_BYTES_COUNT);
				   Reset_SCIA_Recv_Byte_Index();
				   return MELODY_OK;
				  }
	    	 timeout_diff =  D_Timer_Get_mSec_Counter() - timeout;
	      }

	         return MELODY_ERROR;


	default:
		 return MELODY_IDLE;


	}
}



MELODY_STATUS Send_GUI_Blue_Data(char *data ,MELODY_MESSAGE_TYPE type )
{
	uint32_t timeout=0 ,timeout_diff=0;


       if(type == MELODY_MESSAGE_TYPE_DATA )
       {
    	 scia_msg_melody_data(data);
    	 return MELODY_IDLE;
       }
       else if(type == MELODY_MESSAGE_TYPE_COMMAND_CONFIG )
       {
	     Get_OK_or_ERROR_Notify = MELODY_WAIT;
	     timeout = D_Timer_Get_mSec_Counter();
    	 scia_msg_melody_com_config(data);
	     while(Get_OK_or_ERROR_Notify == MELODY_WAIT && timeout_diff<BLUE_COMMAND_NOTIFY_TIMEOUT )
	     {
	   	  timeout_diff =  D_Timer_Get_mSec_Counter() - timeout;
	     }
         return Get_OK_or_ERROR_Notify;
       }
}




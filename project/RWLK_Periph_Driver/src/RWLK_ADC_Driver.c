/*
 * RWLK_ADC_Driver.c
 *
 *  Created on: Feb 21, 2016
 *
 */
#include "RWLK.h"
#include "RWLK_Motor_Control.h"
#include "../../RWLK_Periph_Driver/inc/RWLK_ADC_Driver.h"




/*==============================================================================
                               Local definitions
==============================================================================*/
//#define ADC_MAX_CONVERSION_TIME    50

int16_t CalibratedChannelZero;
#pragma DATA_SECTION(sMotorPhasesCurrentsSend,"ramdata")
struct _MOTOR_PHASES_DATA sMotorPhasesCurrentsSend;

#pragma DATA_SECTION(XsDigitalToAnalogCPUSensorStruct,"ramdata")
struct _DIGITAL_TO_ANALOG_SENSOR XsDigitalToAnalogCPUSensorStruct;

#pragma DATA_SECTION(YsDigitalToAnalogCPUSensorStruct,"ramdata")
struct _DIGITAL_TO_ANALOG_SENSOR YsDigitalToAnalogCPUSensorStruct;

#pragma DATA_SECTION(sDigitalToAnalogSensorStruct,"CpuToCla1MsgRAM")
struct _DIGITAL_TO_ANALOG_SENSOR sDigitalToAnalogSensorStruct;

#pragma DATA_SECTION(XsDigitalToAnalogSensorStruct,"CLADataLS0")
struct _DIGITAL_TO_ANALOG_SENSOR XsDigitalToAnalogSensorStruct;

#pragma DATA_SECTION(YsDigitalToAnalogSensorStruct,"CLADataLS0")
struct _DIGITAL_TO_ANALOG_SENSOR YsDigitalToAnalogSensorStruct;

/*==============================================================================
                               Functions definitions
==============================================================================*/
/*
** ===================================================================
**     Function   :  ADC_read
**
**     Description :
**                                ADC conversion for requested channel
**     Parameters  :
**                                u16 chNo - channel number. Possible values:
**                                     - ADC_CH_MOTOR_CURRENT,
**                                     - ADC_CH_FS1_V,
**                                     - ADC_CH_FS2_V,
**                                     - ADC_CH_FS3_V,
**                                     - ADC_CH_3_3V_SAMPLE,
**                                     - ADC_CH_12V_SAMPLE,
**                                     - ADC_CH_TEMPEARTURE,
**                                     - ADC_CH_VREF,
**                                     - ADC_CH_5V_SAMPLE,
**                                     - ADC_CH_MUX_3_3V,
**                                     - ADC_CH_1_8V_SAMPLE,
**                                     - ADC_CH_28V;
**								  u16 *Result - the conversion result.
**     Returns     :
**                                error code. Possible values:
**                                     - ERR_OK - conversion successfully finished;
**                                     - ERR_ADC_NO_CHANNEL_DOES_NOT_MATCH - illegal the chNo parameter value;
**                                     - ERR_ADC_ACQ_PULSE_NOT_MATCH - illegal the acquisitionPulseTime parameter value;
**                                     - ERR_ADC_CONVERSION_FAILS - conversion timeout detected.
**
**     Notes       :
**
** ===================================================================
*/
/*#pragma CODE_SECTION(ADC_Read, ".drivers")
uint16_t ADC_Read ( uint16_t ADC_Block, uint8_t ChNo, uint16_t *Result);
{
   u8  ErrorCode = ERR_OK;
   u16 ConversionTime = ADC_MAX_CONVERSION_TIME;

   if( ChNo > MAX_ADC_CHANNEL )
   {
   	  *Result = DUMMY_ADC_RESULT;
      ErrorCode = ERR_ADC_NO_CHANNEL_DOES_NOT_MATCH;
   }
   else
   {
	   // Set ADC conversion mode
	   AdcRegs.ADCTRL1.bit.ACQ_PS = ADC_SHCLK_160_NS_CONVERSION; // Sequential mode: Sample rate   = 1/[(2+ACQ_PS)*ADC clock in ns]
	   AdcRegs.ADCTRL2.bit.RST_SEQ1 = 1;
	   AdcRegs.ADCTRL3.bit.ADCCLKPS = ADC_CKPS;
	   AdcRegs.ADCCHSELSEQ1.all = 0x0;         // Initialize all ADC channel selects to A0
	   AdcRegs.ADCCHSELSEQ2.all = 0x0;         // Initialize all ADC channel selects to A1
	   AdcRegs.ADCCHSELSEQ3.all = 0x0;         // Initialize all ADC channel selects to A2
	   AdcRegs.ADCCHSELSEQ4.all = 0x0;         // Initialize all ADC channel selects to A3
	   AdcRegs.ADCCHSELSEQ1.bit.CONV00 = ChNo; //
	   AdcRegs.ADCMAXCONV.all = 0;				// 1 single conversion
	   AdcRegs.ADCREFSEL.bit.REF_SEL = ADC_REF_2048MV;

	    // Start SEQ1
	   AdcRegs.ADCTRL2.all = 0x2000;

	   // Wait for interrupt flag
	   while ( AdcRegs.ADCST.bit.INT_SEQ1 == 0 && --ConversionTime != 0 )continue;
	   // Clear interrupt flag
	   AdcRegs.ADCST.bit.INT_SEQ1_CLR = 1;

	   if ( ConversionTime == 0 )
	   {
	   	  *Result = DUMMY_ADC_RESULT;
	      ErrorCode = ERR_ADC_CONVERSION_FAILS;
	   }
	   else
	   {
	   	  // Obtain ADC data
	   	  *Result = AdcRegs.ADCRESULT0 >> 4;
	   }
   }
   return ErrorCode;
}

*/


void MotorPhasesCurrentParser(volatile cControlVar *MotorPhasesCurrent , struct _MOTOR_PHASES_DATA *TxPhasesCurrents, uint16_t AxisNumber)
{
	int16_t TimePar;

	switch(AxisNumber)
	{
	case(X_Axis):
		TimePar = MotorPhasesCurrent->XAxis_APhaseCurrentFeedback;

	TxPhasesCurrents->PhaseA_DataLow = (uint8_t)(TimePar & 0x00ff);
	TimePar >>= 8;
	TxPhasesCurrents->PhaseA_DataHigh = (uint8_t)(TimePar & 0x00ff);

		TimePar = 0;
		TimePar = MotorPhasesCurrent->XAxis_BPhaseCurrentFeedback;
	TxPhasesCurrents->PhaseB_DataLow = (uint8_t)(TimePar & 0x00ff);
	TimePar >>= 8;
	TxPhasesCurrents->PhaseB_DataHigh = (uint8_t)(TimePar & 0x00ff);

		break;

	case(Y_Axis):

		TimePar = MotorPhasesCurrent->YAxis_APhaseCurrentFeedback;
		TxPhasesCurrents->PhaseA_DataLow = (uint8_t)(TimePar & 0x00ff);
		TimePar >>= 8;
		TxPhasesCurrents->PhaseA_DataHigh = (uint8_t)(TimePar & 0x00ff);

		TimePar = 0;
		TimePar = MotorPhasesCurrent->YAxis_BPhaseCurrentFeedback;
		TxPhasesCurrents->PhaseB_DataLow = (uint8_t)(TimePar & 0x00ff);
	TimePar >>= 8;
		TxPhasesCurrents->PhaseB_DataHigh = (uint8_t)(TimePar & 0x00ff);


		break;

	}
}

//Write ADC configurations and power up the ADC for both ADC A and ADC B
void ConfigureADC(void)
{
	EALLOW;

	//write configurations
	AdcaRegs.ADCCTL2.bit.PRESCALE = 6; //set ADCCLK divider to /4
	AdcbRegs.ADCCTL2.bit.PRESCALE = 6; //set ADCCLK divider to /4
	AdccRegs.ADCCTL2.bit.PRESCALE = 6; //set ADCCLK divider to /4
	AdcdRegs.ADCCTL2.bit.PRESCALE = 6; //set ADCCLK divider to /4
    AdcSetMode(ADC_ADCA, ADC_RESOLUTION_12BIT, ADC_SIGNALMODE_SINGLE);
    AdcSetMode(ADC_ADCB, ADC_RESOLUTION_12BIT, ADC_SIGNALMODE_SINGLE);
    AdcSetMode(ADC_ADCC, ADC_RESOLUTION_12BIT, ADC_SIGNALMODE_SINGLE);
    AdcSetMode(ADC_ADCD, ADC_RESOLUTION_16BIT, ADC_SIGNALMODE_DIFFERENTIAL);

	//Set pulse positions to late
	AdcaRegs.ADCCTL1.bit.INTPULSEPOS = 1;
	AdcbRegs.ADCCTL1.bit.INTPULSEPOS = 1;
	AdccRegs.ADCCTL1.bit.INTPULSEPOS = 1;
	AdcdRegs.ADCCTL1.bit.INTPULSEPOS = 1;

	//power up the ADCs
	AdcaRegs.ADCCTL1.bit.ADCPWDNZ = 1;
	AdcbRegs.ADCCTL1.bit.ADCPWDNZ = 1;
	AdccRegs.ADCCTL1.bit.ADCPWDNZ = 1;
	AdccRegs.ADCCTL1.bit.ADCPWDNZ = 1;

	//delay for 1ms to allow ADC time to power up
	DELAY_US(1000);

	EDIS;
}

void SetupADCSoftware(uint16_t InterruptSorce)
{
	Uint16 acqps;

	//determine minimum acquisition window (in SYSCLKS) based on resolution
	if(ADC_RESOLUTION_12BIT == AdcaRegs.ADCCTL2.bit.RESOLUTION){
		acqps = 14; //75ns
	}
	else { //resolution is 16-bit
		acqps = 63; //320ns
	}
	EALLOW;
	AdcaRegs.ADCSOC0CTL.bit.ACQPS = acqps; //sample window is acqps + 1 SYSCLK cycles
	AdcaRegs.ADCSOC1CTL.bit.ACQPS = acqps; //sample window is acqps + 1 SYSCLK cycles
	EDIS;

	//determine minimum acquisition window (in SYSCLKS) based on resolution
	if(ADC_RESOLUTION_12BIT == AdcbRegs.ADCCTL2.bit.RESOLUTION){
		acqps = 14; //75ns
	}
	else { //resolution is 16-bit
		acqps = 63; //320ns
	}
	EALLOW;
	AdcbRegs.ADCSOC0CTL.bit.ACQPS = acqps; //sample window is acqps + 1 SYSCLK cycles
	AdcbRegs.ADCSOC1CTL.bit.ACQPS = acqps; //sample window is acqps + 1 SYSCLK cycles
	EDIS;

	//determine minimum acquisition window (in SYSCLKS) based on resolution
	if(ADC_RESOLUTION_12BIT == AdccRegs.ADCCTL2.bit.RESOLUTION){
		acqps = 14; //75ns
	}
	else { //resolution is 16-bit
		acqps = 63; //320ns
	}
	EALLOW;
	AdccRegs.ADCSOC0CTL.bit.ACQPS = acqps; //sample window is acqps + 1 SYSCLK cycles
	EDIS;


	if(ADC_RESOLUTION_12BIT == AdcdRegs.ADCCTL2.bit.RESOLUTION){
		acqps = 14; //75ns
	}
	else { //resolution is 16-bit
		acqps = 63; //320ns
	}
	EALLOW;
	AdcdRegs.ADCSOC0CTL.bit.ACQPS = acqps; //sample window is acqps + 1 SYSCLK cycles
	AdcdRegs.ADCSOC1CTL.bit.ACQPS = acqps; //sample window is acqps + 1 SYSCLK cycles
	EDIS;

//Select the channels to convert and end of conversion flag
    //ADCA
    EALLOW;

   //Motor A_PHASE feedback configurations for X  axes
    AdcaRegs.ADCSOC0CTL.bit.CHSEL = 5;  //SOC0 will convert pin A5 - Hall effect sensor
    //AdcaRegs.ADCSOC0CTL.bit.CHSEL = 4;  //SOC0 will convert pin A4	- X axis Motor A Phase
    //AdcaRegs.ADCSOC0CTL.bit.ACQPS = acqps; //sample window is acqps + 1 SYSCLK cycles
    AdcaRegs.ADCSOC0CTL.bit.TRIGSEL = InterruptSorce; 		//SOC0 will begin conversion on ePWM1 SOCA
    AdcaRegs.ADCINTSEL1N2.bit.INT1SEL = 0; //end of SOC0 will set INT1 flag
    AdcaRegs.ADCINTSEL1N2.bit.INT1E = 1;   //enable INT1 flag
    AdcaRegs.ADCINTFLGCLR.bit.ADCINT1 = 1; //make sure INT1 flag is cleared


    //Motor A_PHASE feedback configurations for Y axes

     AdcaRegs.ADCSOC1CTL.bit.CHSEL = 3;  //SOC1 will convert pin A3	- Y axis Motor A Phase
     //AdcaRegs.ADCSOC0CTL.bit.ACQPS = acqps; //sample window is acqps + 1 SYSCLK cycles
     AdcaRegs.ADCSOC1CTL.bit.TRIGSEL = InterruptSorce; 	//SOC1 will begin conversion on ePWM1 SOCA 5
     AdcaRegs.ADCINTSEL1N2.bit.INT2SEL = 1; //end of SOC1 will set INT2 flag
     AdcaRegs.ADCINTSEL1N2.bit.INT2E = 1;   //enable INT2 flag
     AdcaRegs.ADCINTFLGCLR.bit.ADCINT2 = 1; //make sure INT2 flag is cleared



     //Motor B_PHASE feedback configurations for X and Y axes

     //ADCB
  	// Shunt Motor Currents (SW) @ B4
  	// ********************************
     	 AdcbRegs.ADCSOC0CTL.bit.CHSEL = 5; //SOC0 will convert pin A5 - Hall effect sensor
    //  AdcbRegs.ADCSOC0CTL.bit.CHSEL = 4;  //SOC0 will convert pin B4 Shunt motor current - sampling resistor
    //AdcbRegs.ADCSOC0CTL.bit.ACQPS = acqps; //sample window is acqps + 1 SYSCLK cycles
    AdcbRegs.ADCSOC0CTL.bit.TRIGSEL = InterruptSorce; 		//SOC0 will begin conversion on ePWM1 SOCA
    AdcbRegs.ADCINTSEL1N2.bit.INT1SEL = 0; //end of SOC0 will set INT1 flag
    AdcbRegs.ADCINTSEL1N2.bit.INT1E = 1;   //enable INT1 flag
    AdcbRegs.ADCINTFLGCLR.bit.ADCINT1 = 1; //make sure INT1 flag is cleared


    AdcbRegs.ADCSOC1CTL.bit.CHSEL = 1;  		//SOC1 will convert pin B1
    AdcbRegs.ADCSOC1CTL.bit.TRIGSEL = InterruptSorce; 		//SOC1 will begin conversion on ePWM1 SOCA
    AdcbRegs.ADCINTSEL1N2.bit.INT2SEL = 1; //end of SOC1 will set INT1 flag
    AdcbRegs.ADCINTSEL1N2.bit.INT2E = 1;   //enable INT2 flag
    AdcbRegs.ADCINTFLGCLR.bit.ADCINT2 = 1; //make sure INT2 flag is cleared

    //ADCC
    AdccRegs.ADCSOC0CTL.bit.CHSEL = 5;  //SOC0 will convert pin C5
    //AdcbRegs.ADCSOC0CTL.bit.ACQPS = acqps; //sample window is acqps + 1 SYSCLK cycles
    AdccRegs.ADCSOC0CTL.bit.TRIGSEL = InterruptSorce; 		//SOC0 will begin conversion on ePWM1 SOCA
    AdccRegs.ADCINTSEL1N2.bit.INT1SEL = 0; //end of SOC0 will set INT1 flag
    AdccRegs.ADCINTSEL1N2.bit.INT1E = 1;   //enable INT1 flag
    AdccRegs.ADCINTFLGCLR.bit.ADCINT1 = 1; //make sure INT1 flag is cleared


    //ADCC
    // Differential Voltage
    AdcdRegs.ADCSOC0CTL.bit.CHSEL = 0;  //SOC0 will convert pin D0
    //AdcbRegs.ADCSOC0CTL.bit.ACQPS = acqps; //sample window is acqps + 1 SYSCLK cycles
    AdcdRegs.ADCSOC0CTL.bit.TRIGSEL = 5; 		//SOC0 will begin conversion on ePWM1 SOCA
    AdcdRegs.ADCINTSEL1N2.bit.INT1SEL = 0; //end of SOC0 will set INT1 flag
    AdcdRegs.ADCINTSEL1N2.bit.INT1E = 1;   //enable INT1 flag
    AdcdRegs.ADCINTFLGCLR.bit.ADCINT1 = 1; //make sure INT1 flag is cleared

    AdcdRegs.ADCSOC1CTL.bit.CHSEL = 2;  //SOC1 will convert pin D0
    AdcdRegs.ADCSOC1CTL.bit.TRIGSEL = 5; 		//SOC1 will begin conversion on ePWM1 SOCA



	// SETUP DACS
	DacaRegs.DACCTL.bit.DACREFSEL = 1; //REFERENCE_VREF;
	DacaRegs.DACCTL.bit.LOADMODE  = 0;      // enable value change only on sync signal
	//Enable DAC output
	DacaRegs.DACOUTEN.bit.DACOUTEN = 1;
	DacaRegs.DACCTL.bit.SYNCSEL    = 0;     // sync sel 0 meanse sync from pwm 1
	DacaRegs.DACVALS.bit.DACVALS   = 2048;

	DacbRegs.DACCTL.bit.DACREFSEL  = 1; //REFERENCE_VREF;
	DacbRegs.DACCTL.bit.LOADMODE  = 0;      // enable value change only on sync signal
	//Enable DAC output
	DacbRegs.DACOUTEN.bit.DACOUTEN = 1;
	DacbRegs.DACCTL.bit.SYNCSEL    = 0;     // sync sel 0 meanse sync from pwm 1
	DacbRegs.DACVALS.bit.DACVALS   = 2048;

/*#if BOOSTXL == 0
	DaccRegs.DACCTL.bit.DACREFSEL  = 1; //REFERENCE_VREF;
	DaccRegs.DACCTL.bit.LOADMODE  = 0;      // enable value change only on sync signal
	//Enable DAC output
	DaccRegs.DACOUTEN.bit.DACOUTEN = 1;
	DaccRegs.DACCTL.bit.SYNCSEL    = 0;     // sync sel 0 meanse sync from pwm 1
	DaccRegs.DACVALS.bit.DACVALS   = 2048;
#endif
*/
    EDIS;

    //Setup parameters for dacs

    sDigitalToAnalogSensorStruct.CurrentSensorResolution = (float)HALF_ADC_RESOLUTION/CURRENT_SENSOR_MEASUREMENT_RATE;
    sDigitalToAnalogSensorStruct.DigitalToAnalogOutputResolution = (float)HALF_DAC_RESOLUTION/(float)MAXIMUM_CURRENT_MEASUREMENT;
    sDigitalToAnalogSensorStruct.RelativeResolutionsCoefficient = sDigitalToAnalogSensorStruct.DigitalToAnalogOutputResolution/sDigitalToAnalogSensorStruct.CurrentSensorResolution;
    sDigitalToAnalogSensorStruct.HalfOfDigitalToAnalogResolution = (int32_t)HALF_DAC_RESOLUTION;
}

int16_t CalibrateADCSoftware(int16_t Channel)
{
	int16_t CalibrationCounter = 0;
	int32_t	CalibratedZero = 0;
	int32_t ChannelConversionTime = 150;
	int32_t ADCInterrupt = 0;
	while(CalibrationCounter != 32)
	{
		switch(Channel)
		{
		case 0:
			AdcaRegs.ADCSOCFRC1.all = 0x0001;
	    	do{
	    		ADCInterrupt = (int32_t)AdcaRegs.ADCINTFLG.bit.ADCINT1;
	    		ChannelConversionTime--;
	    	}while(ADCInterrupt == 0 && ChannelConversionTime != 0 );
	    	AdcaRegs.ADCINTFLGCLR.bit.ADCINT1 = 1;
	    	CalibratedZero = CalibratedZero + (int32_t)AdcaResultRegs.ADCRESULT0;
	    	CalibrationCounter ++;
			break;
		case 1:
			AdcbRegs.ADCSOCFRC1.all = 0x0001;
		    do{
		    	ADCInterrupt = AdcbRegs.ADCINTFLG.bit.ADCINT1;
		    	ChannelConversionTime--;
		    }while(ADCInterrupt == 0 && ChannelConversionTime != 0 );
		    AdcbRegs.ADCINTFLGCLR.bit.ADCINT1 = 1;
	    	CalibratedZero = CalibratedZero + (int32_t)AdcbResultRegs.ADCRESULT0;
	    	CalibrationCounter ++;
			break;

		case 2:
			AdccRegs.ADCSOCFRC1.all = 0x0001;
	    	do{
	    		ADCInterrupt = AdccRegs.ADCINTFLG.bit.ADCINT1;
	    		ChannelConversionTime--;
	    	}while(ADCInterrupt == 0 && ChannelConversionTime != 0 );
	    	AdccRegs.ADCINTFLGCLR.bit.ADCINT1 = 1;
	    	CalibratedZero = CalibratedZero + (int32_t)AdccResultRegs.ADCRESULT0;
	    	CalibrationCounter ++;
			break;

		case 3:
			AdcaRegs.ADCSOCFRC1.all = 0x0002;
	    	do{
	    		ADCInterrupt = AdcaRegs.ADCINTFLG.bit.ADCINT2;
	    		ChannelConversionTime--;
	    	}while(ADCInterrupt == 0 && ChannelConversionTime != 0 );
	    	AdcaRegs.ADCINTFLGCLR.bit.ADCINT2 = 1;
	    	CalibratedZero = CalibratedZero + (int32_t)AdcaResultRegs.ADCRESULT1;
	    	CalibrationCounter ++;
			break;

		case 4:
			AdcbRegs.ADCSOCFRC1.all = 0x0002;
	    	do{
	    		ADCInterrupt = AdcbRegs.ADCINTFLG.bit.ADCINT2;
	    		ChannelConversionTime--;
	    	}while(ADCInterrupt == 0 && ChannelConversionTime != 0 );
	    	AdcbRegs.ADCINTFLGCLR.bit.ADCINT2 = 1;
	    	CalibratedZero = CalibratedZero + (int32_t)AdcbResultRegs.ADCRESULT1;
	    	CalibrationCounter ++;
			break;
		}
	}

    	//wait for ADCB to complete, then acknowledge flag
    	//ConversionTime = ADC_MAX_CONVERSION_TIME;

	CalibratedZero >>= 5;
	CalibratedChannelZero = CalibratedZero;
	return CalibratedChannelZero;

}


//===========================================================================
// No more.
//===========================================================================


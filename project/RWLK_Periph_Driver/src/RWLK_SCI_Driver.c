//###########################################################################
// FILE:   RWLK_SCI_Driver.c
// TITLE:  SCI A and C configuration using FIFO  with Interrupts.


// Author:  Rasem Fadela
// Date:    5/02/2017
//
// SCIA communicate with the Bluetooth module
// SCIC communicate with the PC Terminal  via MAX converter
//###########################################################################
#include "RWLK_SCI_Driver.h"


// Global variables
char recv_dataA[MAX_SCI_RECV_BYTES_COUNT];    // Received data for SCI-A
char recv_dataC[MAX_SCI_RECV_BYTES_COUNT];    // Received data for SCI-C

char GUI_recveived_data[MAX_SCI_RECV_BYTES_COUNT];

int scia_recv_byte_index =0;

Uint16 Sci_A_RX_FIFO_Depth= FIFO_DEPTH_1;
Uint16 Sci_C_RX_FIFO_Depth= FIFO_DEPTH_1;
Uint16 Sci_A_TX_FIFO_Depth= FIFO_DEPTH_1;
Uint16 Sci_C_TX_FIFO_Depth= FIFO_DEPTH_1;



void Reset_SCIA_Recv_Byte_Index(void)
{
	scia_recv_byte_index = 0;
}


const char *Init_Blue_Recv_Data_Memory(void)
{
     return recv_dataA;
}


interrupt void sciaRxFifoIsr(void)
{
 Uint16 i;

 for(i=0;i<Sci_A_RX_FIFO_Depth;i++)
    {

	  recv_dataA[scia_recv_byte_index]=SciaRegs.SCIRXBUF.all;  // Read data
	  ScicRegs.SCITXBUF.all =  recv_dataA[scia_recv_byte_index]; //Write SCIA received data to SCIC for debug purpose
      scia_recv_byte_index++;
    }

  if(scia_recv_byte_index == MAX_SCI_RECV_BYTES_COUNT)
	  scia_recv_byte_index=0;



  SciaRegs.SCIFFRX.bit.RXFFOVRCLR=1;   // Clear Overflow flag
  SciaRegs.SCIFFRX.bit.RXFFINTCLR=1;   // Clear Interrupt flag
  PieCtrlRegs.PIEACK.all|=0x100;       // Issue PIE ack
}

interrupt void scicRxFifoIsr(void)
{
 Uint16 i;
 char *r;
 static int scic_recv_byte_index=0;
 static _Bool Terminal_Command;

 for(i=0;i<Sci_C_RX_FIFO_Depth;i++)
    {
	  recv_dataC[scic_recv_byte_index] = ScicRegs.SCIRXBUF.all;// Read data
	  if(recv_dataC[scic_recv_byte_index]=='#' || Terminal_Command)
	  {
		  Terminal_Command = true;
	  }
	  else
	  {
	  SciaRegs.SCITXBUF.all= recv_dataC[scic_recv_byte_index];  //every  data sent by SCIC will be send to SCIA - bluetooth module- by SCIC -terminal
	  }
	  scic_recv_byte_index++;
    }

  if(recv_dataC[scic_recv_byte_index-1] == 0x0d) //accepting global command from wired terminal - after typing end-of-line or '\r'
  {
	  scic_recv_byte_index = 0;
	  Terminal_Command = false;
	  //this will be transfered to handler in main if needed
		  r = strstr((const char *)recv_dataC , "#gled"); //OPEN SPP link by GUI
		  if(r  != NULL)
			  {
			   GPIO_WritePin(GREEN_LED, LED_ON);
			   memset(recv_dataC,0,sizeof(recv_dataC));
			   ScicRegs.SCITXBUF.all = 'G';
			   ScicRegs.SCITXBUF.all = '\n';

			  }

		  r = strstr((const char *)recv_dataC , "#rled"); //OPEN SPP link by GUI
		  if(r  != NULL)
			  {
			   GPIO_WritePin(RED_LED, LED_ON);
			   memset(recv_dataC,0,sizeof(recv_dataC));
			   ScicRegs.SCITXBUF.all = 'R';
			   ScicRegs.SCITXBUF.all = '\n';
			  }
		  r = strstr((const char *)recv_dataC , "#oled"); //OPEN SPP link by GUI
		  if(r  != NULL)
			  {
			   GPIO_WritePin(ORANGE_LED, LED_ON);
			   memset(recv_dataC,0,sizeof(recv_dataC));
			   ScicRegs.SCITXBUF.all = 'O';
			   ScicRegs.SCITXBUF.all = '\n';

			  }
		  r = strstr((const char *)recv_dataC , "#offled"); //OPEN SPP link by GUI
		  if(r  != NULL)
			  {
			   GPIO_WritePin(RED_LED, LED_OFF);
			   GPIO_WritePin(GREEN_LED, LED_OFF);
			   GPIO_WritePin(ORANGE_LED, LED_OFF);
			   memset(recv_dataC,0,sizeof(recv_dataC));
			   ScicRegs.SCITXBUF.all = 'O';ScicRegs.SCITXBUF.all = 'F';ScicRegs.SCITXBUF.all = 'F';
			   ScicRegs.SCITXBUF.all = '\n';

			  }
		  r = strstr((const char *)recv_dataC , "#onled"); //OPEN SPP link by GUI
		   if(r  != NULL)
			  {
			   GPIO_WritePin(RED_LED, LED_ON);
			   GPIO_WritePin(GREEN_LED, LED_ON);
			   GPIO_WritePin(ORANGE_LED, LED_ON);
			   memset(recv_dataC,0,sizeof(recv_dataC));
			   ScicRegs.SCITXBUF.all = 'O';ScicRegs.SCITXBUF.all = 'N';
			   ScicRegs.SCITXBUF.all = '\n';

			  }
  }

  if( scic_recv_byte_index == MAX_SCI_RECV_BYTES_COUNT)
 	  scic_recv_byte_index=0;

  ScicRegs.SCIFFRX.bit.RXFFOVRCLR=1;   // Clear Overflow flag
  ScicRegs.SCIFFRX.bit.RXFFINTCLR=1;   // Clear Interrupt flag
  PieCtrlRegs.PIEACK.all|=0x080;       // Issue PIE ack
}



//Toggle GPIO pin or led
void GPIO_TogglePin(Uint16 pin)

{
	volatile Uint32 *gpioDataReg;
	Uint32 pinMask;

	gpioDataReg = (volatile Uint32 *)&GpioDataRegs + (pin/32)*GPY_DATA_OFFSET;
	pinMask = 1UL << (pin % 32);


	gpioDataReg[GPYTOGGLE] = pinMask;

}

void GPIO_LEDs_Config(void)
{
	// Configure all LEDs-GPIO:
	GPIO_SetupPinMux(RED_LED, GPIO_MUX_CPU1, 0);
	GPIO_SetupPinOptions(RED_LED, GPIO_OUTPUT, GPIO_PUSHPULL);
	GPIO_SetupPinMux(GREEN_LED, GPIO_MUX_CPU1, 0);
	GPIO_SetupPinOptions(GREEN_LED, GPIO_OUTPUT, GPIO_PUSHPULL);
	GPIO_SetupPinMux(ORANGE_LED, GPIO_MUX_CPU1, 0);
	GPIO_SetupPinOptions(ORANGE_LED, GPIO_OUTPUT, GPIO_PUSHPULL);
}

//Temporarily define should be removed
void GPIO_Test_TimeScope_Config(void)
{
	// Configure GPIO103 as GPOIO for test
	GPIO_SetupPinMux(TIME_TEST_PIN_GPIO103, GPIO_MUX_CPU1, 0);
	GPIO_SetupPinOptions(TIME_TEST_PIN_GPIO103, GPIO_OUTPUT, GPIO_PUSHPULL);
}

void LEDs_ON(void)
{
	// Turn all  LEDS ON
	GPIO_WritePin(RED_LED, LED_ON);
	GPIO_WritePin(GREEN_LED, LED_ON);
	GPIO_WritePin(ORANGE_LED, LED_ON);

}

void LEDs_OFF(void)
{
	// Turn LEDS OFF
	GPIO_WritePin(RED_LED, LED_OFF);
	GPIO_WritePin(GREEN_LED, LED_OFF);
	GPIO_WritePin(ORANGE_LED, LED_OFF);

}

void Toggle_LEDs(void)
{
	// Toggle all LEDS
	GPIO_TogglePin(RED_LED);
	GPIO_TogglePin(GREEN_LED);
	GPIO_TogglePin(ORANGE_LED);

}

void GPIO_SCI_A_Config(void)
{
	 /*MCU  SCI A  - pins GPIO64 is  SCIARXD and pin GPIO65 is SCIATXD */
	 GPIO_SetupPinMux(RWLK_SCIARXD_GPIO, GPIO_MUX_CPU1, 6); //index should be 6 to make SCIA function
	 GPIO_SetupPinOptions(RWLK_SCIARXD_GPIO, GPIO_INPUT, GPIO_PUSHPULL);
	 GPIO_SetupPinMux(RWLK_SCIATXD_GPIO, GPIO_MUX_CPU1,6); //index should be 6 to make SCIA function
	 GPIO_SetupPinOptions(RWLK_SCIATXD_GPIO, GPIO_OUTPUT, GPIO_ASYNC);
}

void GPIO_SCI_C_Config(void)
{
	 /*MCU  SCI A  - pins GPIO64 is  SCIARXD and pin GPIO65 is SCIATXD */
	 GPIO_SetupPinMux(RWLK_SCICRXD_GPIO, GPIO_MUX_CPU1, 6); //index should be 6 to make SCIC function
	 GPIO_SetupPinOptions(RWLK_SCICRXD_GPIO, GPIO_INPUT, GPIO_PUSHPULL);
	 GPIO_SetupPinMux(RWLK_SCICTXD_GPIO, GPIO_MUX_CPU1,6); //index should be 6 to make SCIC function
	 GPIO_SetupPinOptions(RWLK_SCICTXD_GPIO, GPIO_OUTPUT, GPIO_ASYNC);
}

void SCI_A_Interrupt_Install(void)
{
	// Interrupts that are used in this example are re-mapped to
	// ISR functions found within this file.
	 EALLOW;  // This is needed to write to EALLOW protected registers
	 PieVectTable.SCIA_RX_INT = &sciaRxFifoIsr;
	 PieVectTable.SCIA_TX_INT = &sciaTxFifoIsr;
	 EDIS;   // This is needed to disable write to EALLOW protected registers

}

void SCI_C_Interrupt_Install(void)
{
	// Interrupts that are used in this example are re-mapped to
	// ISR functions found within this file.
	 EALLOW;  // This is needed to write to EALLOW protected registers
	 PieVectTable.SCIC_RX_INT = &scicRxFifoIsr;
	 PieVectTable.SCIC_TX_INT = &scicTxFifoIsr;
	 EDIS;   // This is needed to disable write to EALLOW protected registers
}

void SCI_A_Interrupt_Enable(void)
{
	// Enable interrupts of SCIA
	PieCtrlRegs.PIECTRL.bit.ENPIE = 1;   // Enable the PIE block
	PieCtrlRegs.PIEIER9.bit.INTx1=1;     // PIE Group 9, INT1 RX
	PieCtrlRegs.PIEIER9.bit.INTx2=0;     // PIE Group 9, INT2 TX
	IER|= 0x100; // Enable CPU INT
}

void SCI_C_Interrupt_Enable(void)
{
	 // Enable interrupts required for SCI_C
	 PieCtrlRegs.PIECTRL.bit.ENPIE = 1;   // Enable the PIE block
	 PieCtrlRegs.PIEIER8.bit.INTx5=1;     // PIE Group 8, INT5  RX
	 PieCtrlRegs.PIEIER8.bit.INTx6=0;     // PIE Group 8, INT6 TX
	 IER|= 0x080; // Enable CPU INT
}



interrupt void sciaTxFifoIsr(void)
{

    SciaRegs.SCIFFTX.bit.TXFFINTCLR=1;  // Clear SCI Interrupt flag
    PieCtrlRegs.PIEACK.all|=0x100;      // Issue PIE ACK
}

interrupt void scicTxFifoIsr(void)
{

    ScicRegs.SCIFFTX.bit.TXFFINTCLR=1;  // Clear SCI Interrupt flag
    PieCtrlRegs.PIEACK.all|=0x080;      // Issue PIE ACK
}





void scia_fifo_init(Uint16 TX_fifo_depth , Uint16 RX_fifo_depth , Uint32 sci_baud_rate)
{
   SciaRegs.SCICCR.all =0x0007;   // 1 stop bit,  No loopback // No parity,8 char bits // async mode, idle-line protocol
   SciaRegs.SCICTL1.all =0x0003;  // enable TX, RX, internal SCICLK, // Disable RX ERR, SLEEP, TXWAKE
   SciaRegs.SCICTL2.bit.TXINTENA =1;
   SciaRegs.SCICTL2.bit.RXBKINTENA =1;

   // SCIA at 9600 baud
   // @LSPCLK = 50 MHz (200 MHz SYSCLK) HBAUD = 0x02 and LBAUD = 0x8B.
   Uint32 SCI_PRD  =     (LSPCLK_FREQ/(sci_baud_rate*8))-1;  //calculated SCI_PRD field

   SciaRegs.SCILBAUD.all =  (Uint16)SCI_PRD;
   SciaRegs.SCIHBAUD.all =  (Uint16)(SCI_PRD>>8);
   SciaRegs.SCICCR.bit.LOOPBKENA =0; // Disable loop back
   SciaRegs.SCIFFTX.all=0xC000;
   SciaRegs.SCIFFRX.all=0x0020|RX_fifo_depth; //Read 1 or 16 (fifo_depth)  word from fifo
   SciaRegs.SCIFFCT.all=0x00;
   SciaRegs.SCICTL1.all =0x0023;     // Relinquish SCI from Reset
   SciaRegs.SCIFFTX.bit.TXFIFOXRESET=1;
   SciaRegs.SCIFFRX.bit.RXFIFORESET=1;
   Sci_A_RX_FIFO_Depth = RX_fifo_depth;
   Sci_A_TX_FIFO_Depth = TX_fifo_depth;
}


void scic_fifo_init(Uint16 TX_fifo_depth , Uint16 RX_fifo_depth , Uint32 sci_baud_rate)
{
   ScicRegs.SCICCR.all =0x0007;   // 1 stop bit,  No loopback  // No parity,8 char bits,// async mode, idle-line protocol
   ScicRegs.SCICTL1.all =0x0003;  // enable TX, RX, internal SCICLK, // Disable RX ERR, SLEEP, TXWAKE
   ScicRegs.SCICTL2.bit.TXINTENA =1;
   ScicRegs.SCICTL2.bit.RXBKINTENA =1;

   Uint32 SCI_PRD  =     (LSPCLK_FREQ/(sci_baud_rate*8))-1;  //calculated SCI_PRD field
   // SCIC at 9600 baud
   // @LSPCLK = 50 MHz (200 MHz SYSCLK) HBAUD = 0x02 and LBAUD = 0x8B

   ScicRegs.SCILBAUD.all =  (Uint16)SCI_PRD;
   ScicRegs.SCIHBAUD.all =  (Uint16)(SCI_PRD>>8);

   ScicRegs.SCICCR.bit.LOOPBKENA =0; // Disable loop back
   ScicRegs.SCIFFTX.all=0xC000;
   ScicRegs.SCIFFRX.all=0x0020|RX_fifo_depth; //Read 1 or 16 (fifo_depth) word from fifo
   ScicRegs.SCIFFCT.all=0x00;
   ScicRegs.SCICTL1.all =0x0023;     // Relinquish SCI from Reset
   ScicRegs.SCIFFTX.bit.TXFIFOXRESET=1;
   ScicRegs.SCIFFRX.bit.RXFIFORESET=1;
   Sci_C_RX_FIFO_Depth = RX_fifo_depth;
   Sci_C_TX_FIFO_Depth = TX_fifo_depth;
}

// Transmit a character from the SCI_A
void scia_xmit(int a)
{
    while (SciaRegs.SCIFFTX.bit.TXFFST != 0) {}
    SciaRegs.SCITXBUF.all =a;

}


void scia_msg_melody_com_config(const char * msg)
{

     while(*msg!= '\0')
      {
		while (SciaRegs.SCIFFTX.bit.TXFFST != 0) {}
		SciaRegs.SCITXBUF.all = *msg;
	    msg++;
	  }
}


void scia_msg_melody_data(const char *msg)
{
int cnt_16_bytes=0;
_Bool send = true;

	          while (SciaRegs.SCIFFTX.bit.TXFFST != 0) {}

	          while(send)
	          {
    		    while(*msg != '\r' && cnt_16_bytes < 16)
        		  {
    			    //check here if need this while --   while (SciaRegs.SCIFFTX.bit.TXFFST != 0) {}
       			    SciaRegs.SCITXBUF.all = *msg;
                    msg++;
                    cnt_16_bytes++;
        		  }
				  if(cnt_16_bytes == 16)
				  {
					  cnt_16_bytes = 0;
					  while (SciaRegs.SCIFFTX.bit.TXFFST != 0) {}

				  }
				  else
				  {
					  send = false;
				  }
	          }
    		  SciaRegs.SCITXBUF.all = '\r';

}
// Transmit a character from the SCI_C
void scic_xmit(int a)
{
    while (ScicRegs.SCIFFTX.bit.TXFFST != 0) {}
    ScicRegs.SCITXBUF.all =a;
}

void scic_msg(char * msg)
{
    int i;
    i = 0;

   while(msg[i] != '\0')
    {
        scic_xmit(msg[i]);
        i++;
    }
}


/////////////////////////////////Main functions to be called from main loop or  Initialization

void sci_a_init(Uint16 TX_fifo_depth , Uint16 RX_fifo_depth , Uint32 scia_baud_rate)
{
  GPIO_SCI_A_Config();//GPIO64 and GPIO65
  SCI_A_Interrupt_Install();
  scia_fifo_init(TX_fifo_depth,RX_fifo_depth,scia_baud_rate);
  SCI_A_Interrupt_Enable();
}

void sci_c_init(Uint16 TX_fifo_depth , Uint16 RX_fifo_depth , Uint32 scic_baud_rate)
{
  char *msg;
  GPIO_SCI_C_Config();//GPIO64 and GPIO65
  SCI_C_Interrupt_Install();
  scic_fifo_init(TX_fifo_depth,RX_fifo_depth,scic_baud_rate);
  SCI_C_Interrupt_Enable();
  DELAY_US(1000);
  //send debugger welcome message
  msg ="******** Welcom to DSP and Bluetooth Debugger by SCI_C ************\nEnter command starts with '_' or Enter Bluetooth command:\n";
  scic_msg(msg);
}

void sci_a_change_fifo_depth(Uint16 TX_fifo_depth ,Uint16 RX_fifo_depth)
{
	   SciaRegs.SCICTL1.all =0x0000;     // Relinquish SCI from Reset
	   SciaRegs.SCIFFTX.all=0xC020|TX_fifo_depth;  //Write 1 or 16 (fifo_depth)  words to fifo
	   SciaRegs.SCIFFRX.all=0x0020|RX_fifo_depth; //Read 1 or 16 (fifo_depth) word from fifo
	   SciaRegs.SCICTL2.bit.TXINTENA =1;
       SciaRegs.SCICTL2.bit.RXBKINTENA =1;
       SciaRegs.SCIFFCT.all=0x00;
       SciaRegs.SCICTL1.all =0x0023;     // Relinquish SCI from Reset
       SciaRegs.SCIFFTX.bit.TXFIFOXRESET=1;
       SciaRegs.SCIFFRX.bit.RXFIFORESET=1;
       Sci_A_RX_FIFO_Depth = RX_fifo_depth;
       Sci_A_TX_FIFO_Depth = TX_fifo_depth;
}

void sci_c_change_fifo_depth(Uint16 TX_fifo_depth ,Uint16 RX_fifo_depth)
{
	   ScicRegs.SCICTL1.all =0x0000;     // Relinquish SCI from Reset
	   ScicRegs.SCIFFTX.all=0xC020|TX_fifo_depth;  //Write 1 or 16 (fifo_depth)  words to fifo
	   ScicRegs.SCIFFRX.all=0x0020|RX_fifo_depth; //Read 1 or 16 (fifo_depth) word from fifo
	   ScicRegs.SCICTL2.bit.TXINTENA =1;
       ScicRegs.SCICTL2.bit.RXBKINTENA =1;
       ScicRegs.SCIFFCT.all=0x00;
       ScicRegs.SCICTL1.all =0x0023;     // Relinquish SCI from Reset
       ScicRegs.SCIFFTX.bit.TXFIFOXRESET=1;
       ScicRegs.SCIFFRX.bit.RXFIFORESET=1;
       Sci_C_RX_FIFO_Depth = RX_fifo_depth;
       Sci_C_TX_FIFO_Depth = TX_fifo_depth;
}
//===========================================================================
// No more.
//===========================================================================


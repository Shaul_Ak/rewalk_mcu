
/*******************************************************************************
* file name: RWLK_SPI_Driver.c
*
* Description: SPI_A, SPI_C drivers.
*
* Note: Developed for TI F28377D.
*
* Author:  Yehuda Bitton
* Date:    July 2016
*
******************************************************************************/

/*******************************************************************************
*  Includes
******************************************************************************/


#include "RWLK_SPI_Driver.h"
#include "RWLK_Common.h"
#include "RWLK_GPIO_Driver.h"
#include "RWLK.h"

/*******************************************************************************
*  Data types
******************************************************************************/



typedef struct
{
	uint32_t SPI_Base;
	volatile struct SPI_REGS *SPI_Reg_Adrs;
	uint32_t STE_Pin;  // This is the chip select pin
} SPI_Addresses_Type;

/*------------------------------------------------------------------------------
Variables
------------------------------------------------------------------------------*/

/*******************************************************************************
*  Constants and Macros
******************************************************************************/
const SPI_Addresses_Type SPI_ADDRESSES_TABLE[SPI_NUM] =
{
		{ SPIA_BASE, &SpiaRegs, SPI_STE_IMU_A},
		{ SPIA_BASE, &SpiaRegs, SPI_STE_MOTOR_A},
		{ SPIB_BASE, &SpibRegs, SPI_STE_B},
		{ SPIC_BASE, &SpicRegs, SPI_STE_C}
};

/*******************************************************************************
*  Static Data deceleration
******************************************************************************/

/*******************************************************************************
*  Private Functions Decleration
******************************************************************************/
static uint8_t SPI_TranceiveByte (uint32_t SpiId, uint8_t TxDataByte, uint8_t *RxDataByte);


/*******************************************************************************
*  Public Function Bodies
******************************************************************************/


/*==============================================================================
Function : SPI_Init

Description : This function initialize SPI driver

SPI_Base 		: SPI Channel
					SPIA_BASE
					SPIB_BASE
					SPIC_BASE

SPI_Length		: Number of Bits
					BITS_8
					BITS_16

SPI_Bit_Rate 	:
					SPI_BAUDRATE_128 - 7Fh (R/W) = SPI Baud Rate = LSPCLK/128
 Author: Saman

==============================================================================*/

void SPI_Init(uint32_t SpiId, uint32_t SPI_Length , uint32_t SPI_Bit_Rate )
{
	uint32_t CurrStePin;
	uint32_t CurrSPIBase;

	CurrSPIBase = SPI_ADDRESSES_TABLE[SpiId].SPI_Base;
	CurrStePin = SPI_ADDRESSES_TABLE[SpiId].STE_Pin;

	SPI_Gpio_Init(CurrSPIBase);

	// Disable chip select before initializing the registers.
    GPIO_WritePin(CurrStePin, 1); // Disable CS
    // End Copy from C - debug the SPI B
	if (SPI_A_IMU_ID == SpiId)
		SPI_Init_Registers(CurrSPIBase, SPI_Length , IMU_SPI_BITRATE );
	else
		SPI_Init_Registers(CurrSPIBase, SPI_Length , SPI_BITRATE_128 );

}

/*==============================================================================
Function : SPI_Gpio_Init

Description : This function initialize GPIO to SPI

SPI_Base 		: SPI Channel
					SPIA_BASE
					SPIB_BASE
					SPIC_BASE
==============================================================================*/

void SPI_Gpio_Init(uint32_t SPI_Base)
{


	switch(SPI_Base)
	{
		case SPIA_BASE:
			   //InitSpiaGpio();
			GPIO_SetupPinMux(SPI_SIMO_A, GPIO_MUX_CPU1, SPIA_ENABLE); // Setup GPIO to SPI function , CPU1
			GPIO_SetupPinOptions(SPI_SIMO_A, GPIO_OUTPUT, GPIO_ASYNC); // MO-SI as output , Enable pull-up.

			GPIO_SetupPinMux(SPI_SOMI_A, GPIO_MUX_CPU1, SPIA_ENABLE); // Setup GPIO to SPI
			GPIO_SetupPinOptions(SPI_SOMI_A, GPIO_INPUT, GPIO_ASYNC); //MI-SO as input , Asynch input.

			GPIO_SetupPinMux(SPI_CLK_A, GPIO_MUX_CPU1, SPIA_ENABLE); // Enable GPIO to SPI
			GPIO_SetupPinOptions(SPI_CLK_A, GPIO_OUTPUT, GPIO_ASYNC); // CLK as output  , Enable pull-up.

			//GPIO_SetupPinMux(SPI_STE_A, GPIO_MUX_CPU1, SPIA_ENABLE); // Enable GPIO to SPI
			//GPIO_SetupPinOptions(SPI_STE_A, GPIO_OUTPUT, GPIO_PUSHPULL); // CLK as output  , Enable pull-up.



			//GPIO_SetupPinMux(SPI_STE_IMU_A, GPIO_MUX_CPU1, SPIA_ENABLE); // Enable GPIO to SPI
			//GPIO_SetupPinOptions(SPI_STE_IMU_A, GPIO_OUTPUT, GPIO_PUSHPULL); // CS as output , Enable pull-up.
		break;

		case SPIB_BASE: /* will be used for the IMU */

			GPIO_SetupPinMux(SPI_SIMO_B, GPIO_MUX_CPU1, SPIB_ENABLE); // Setup GPIO to SPI function , CPU1
			GPIO_SetupPinOptions(SPI_SIMO_B, GPIO_OUTPUT, GPIO_PUSHPULL); // MO-SI as output , Enable pull-up.

			GPIO_SetupPinMux(SPI_SOMI_B, GPIO_MUX_CPU1, SPIB_ENABLE); // Setup GPIO to SPI
			GPIO_SetupPinOptions(SPI_SOMI_B, GPIO_INPUT, GPIO_ASYNC); //MI-SO as input , Asynch input.

			GPIO_SetupPinMux(SPI_CLK_B, GPIO_MUX_CPU1, SPIB_ENABLE); // Enable GPIO to SPI
			GPIO_SetupPinOptions(SPI_CLK_B, GPIO_OUTPUT, GPIO_PUSHPULL); // CLK as output  , Enable pull-up.

#if BOOSTXL == 0
//			GPIO_SetupPinMux(SPI_STE_B, GPIO_MUX_CPU1, 0); // Enable GPIO to SPI
//			GPIO_SetupPinOptions(SPI_STE_B, GPIO_OUTPUT, GPIO_PUSHPULL);
#else
			GPIO_SetupPinMux(SPI_STE_B, GPIO_MUX_CPU1, SPIB_ENABLE); // Enable GPIO to SPI
			GPIO_SetupPinOptions(SPI_STE_B, GPIO_OUTPUT, GPIO_PUSHPULL); // CLK as output  , Enable pull-up.
#endif
		break;

		case SPIC_BASE:
#if IDDK_MODULE == 0 && BOOSTXL == 0
			EALLOW; //GPIO registers are CPU write protected
			//------------------ GPIO Enable/ Disable Pull Up of SPI_C ----------------------//
		    GpioCtrlRegs.GPDPUD.bit.GPIO122 = 0;   // Enable pull-up  (SPISIMOC)
		    GpioCtrlRegs.GPDPUD.bit.GPIO123 = 0;   // Enable pull-up  (SPISOMIC)
		    GpioCtrlRegs.GPDPUD.bit.GPIO124 = 0;   // Enable pull-up  (SPICLKC)
		    GpioCtrlRegs.GPDPUD.bit.GPIO125 = 0;   // Enable pull-up  (SPISTEC)
		    GpioCtrlRegs.GPDPUD.bit.GPIO126 = 0;   // Enable pull-up  (Wnot)
		    GpioCtrlRegs.GPDPUD.bit.GPIO127 = 0;   // Enable pull-up  (HOLDnot)


            //----------------------- GPIO qualification of SPI_C ---------------------------//
		    GpioCtrlRegs.GPDQSEL2.bit.GPIO122 = 3; // Asynch input  (SPISIMOC)
		    GpioCtrlRegs.GPDQSEL2.bit.GPIO123 = 3; // Asynch input  (SPISOMIC)
		    GpioCtrlRegs.GPDQSEL2.bit.GPIO124 = 3; // Asynch input  (SPICLKC)
		    GpioCtrlRegs.GPDQSEL2.bit.GPIO125 = 0; // synch input   (SPISTEC)
		    GpioCtrlRegs.GPDQSEL2.bit.GPIO126 = 0; // synch input   (Wnot)
		    GpioCtrlRegs.GPDQSEL2.bit.GPIO127 = 0; // synch input   (HOLDnot)


		    //---------------------- Configure MUX and GMUX for SPI_C: ---------------------//
		    GpioCtrlRegs.GPDMUX2.bit.GPIO122 = 2; // Configure SPISIMOC
		    GpioCtrlRegs.GPDMUX2.bit.GPIO123 = 2; // Configure SPISOMIC
		    GpioCtrlRegs.GPDMUX2.bit.GPIO124 = 2; // Configure SPICLKC
		    GpioCtrlRegs.GPDMUX2.bit.GPIO125 = 0; // Configure SPISTEC
		    GpioCtrlRegs.GPDMUX2.bit.GPIO126 = 0; // Configure (Wnot)
		    GpioCtrlRegs.GPDMUX2.bit.GPIO127 = 0; // Configure (HOLDnot)

		    GpioCtrlRegs.GPDGMUX2.bit.GPIO122 = 1; // Configure SPISIMOC
		    GpioCtrlRegs.GPDGMUX2.bit.GPIO123 = 1; // Configure SPISOMIC
		    GpioCtrlRegs.GPDGMUX2.bit.GPIO124 = 1; // Configure SPICLKC
		    GpioCtrlRegs.GPDGMUX2.bit.GPIO125 = 0; // Configure SPISTEC
		    GpioCtrlRegs.GPDGMUX2.bit.GPIO126 = 0; // Configure (Wnot)
		    GpioCtrlRegs.GPDGMUX2.bit.GPIO127 = 0; // Configure (HOLDnot)

		    //---------------- Configure GPIO direction for SPI_C CS and ExtFlash: ---------------//
		    GpioCtrlRegs.GPDDIR.bit.GPIO122 = 1; // Configure SPISTEC direction
		    GpioCtrlRegs.GPDDIR.bit.GPIO123 = 0; // Configure (Wnot) direction
		    GpioCtrlRegs.GPDDIR.bit.GPIO124 = 1; // Configure (HOLDnot) direction

		    EDIS;//restore registers write protection
#endif
		break;
	}

}
/*==============================================================================
Function : SPI_Init_Registers

Description : This function initialize SPI Registers

SPI_Base 		: SPI Channel
					SPIA_BASE
					SPIB_BASE
					SPIC_BASE

SPI_Length		: Number of Bits
					BITS_8
					BITS_16

SPI_Bit_Rate 	:
					SPI_BITRATE_128 - 7Fh (R/W) = SPI Baud Rate = LSPCLK/128
 Author: Saman
==============================================================================*/
void SPI_Init_Registers(uint32_t SPI_Base , uint32_t SPI_Length , uint32_t SPI_Bit_Rate)
{

	switch(SPI_Base)
	{
		case SPIA_BASE:
			SpiaRegs.SPICCR.bit.SPICHAR =	SPI_Length - 1;	 // Reset on, rising edge, 16-bit char bits

			SpiaRegs.SPICTL.bit.TALK =	true;    		     // Enable master mode, normal phase,
			SpiaRegs.SPICTL.bit.MASTER_SLAVE = true;
			// YehudaB: Configure SPI Clocking Schemes to be Rising Edge (Polarity zero) Without Delay (Phase Zero).
			SpiaRegs.SPICTL.bit.CLK_PHASE = 0;
			SpiaRegs.SPICCR.bit.CLKPOLARITY = 0;

			// enable talk, and SPI int disabled.

			SpiaRegs.SPIBRR.bit.SPI_BIT_RATE = SPI_Bit_Rate;  // Bit rate transfer
			SpiaRegs.SPICCR.bit.SPISWRESET = true;		      // Relinquish SPI from Reset
			SpiaRegs.SPIPRI.bit.FREE = true;                  // Set so breakpoints don't disturb xmission
			break;

		case SPIB_BASE:
			SpibRegs.SPICCR.bit.SPICHAR =	SPI_Length - 1;	 // Reset on, rising edge, 16-bit char bits

			SpibRegs.SPICTL.bit.TALK =	true;    		     // Enable master mode, normal phase,
			SpibRegs.SPICTL.bit.MASTER_SLAVE = true;
			// Configure SPI Clocking Schemes to be Falling Edge (Polarity 1) Without Delay (Phase Zero).
			SpibRegs.SPICTL.bit.CLK_PHASE = 0;
			SpibRegs.SPICCR.bit.CLKPOLARITY = 1;

			// enable talk, and SPI int disabled.

			SpibRegs.SPIBRR.bit.SPI_BIT_RATE = SPI_Bit_Rate;  // Bit rate transfer
			SpibRegs.SPICCR.bit.SPISWRESET = true;		      // Relinquish SPI from Reset
			SpibRegs.SPIPRI.bit.FREE = true;                  // Set so breakpoints don't disturb xmission
		break;

		case SPIC_BASE:
			SpicRegs.SPICCR.bit.SPICHAR =	SPI_Length - 1;	 // Reset on, rising edge, 16-bit char bits

			SpicRegs.SPICTL.bit.TALK =	true;    		     // Enable master mode, normal phase,
			SpicRegs.SPICTL.bit.MASTER_SLAVE = true;
			// YehudaB: Configure SPI Clocking Schemes to be Rising Edge (Polarity zero) Without Delay (Phase Zero).
			SpicRegs.SPICTL.bit.CLK_PHASE = 0;
			SpicRegs.SPICCR.bit.CLKPOLARITY = 0;

			// enable talk, and SPI int disabled.

			SpicRegs.SPIBRR.bit.SPI_BIT_RATE = SPI_Bit_Rate;  // Bit rate transfer
			SpicRegs.SPICCR.bit.SPISWRESET = true;		      // Relinquish SPI from Reset
			SpicRegs.SPIPRI.bit.FREE = true;                  // Set so breakpoints don't disturb xmission
			break;
	}


}



/*****************************************************************************
 * Function name: SPI_Send
 *
 * Description: SPI_C Send.
 *
 * Parameters:  *pData - a pointer to an array holding the data to be sent.
 *              Length - Number of element to send (array size).
 *              DisableCS_AfterSend: [1] - Disable Chip Select when transmittion ends.
 *                                   [0] - Do not disable Chip Select when transmittion ends (Leave CS active).
 *
 * Returns: ErrCode: ERR_OK or ERR_SPI_EXCHANGING_FAILS.
 *
 * Author: Yehuda Bitton
 ******************************************************************************/
uint8_t SPI_Send(uint32_t SpiId, uint8_t *pData, uint8_t Length, uint8_t DisableCS_AfterSend)
{

	uint8_t  ErrCode = ERR_OK;
	uint16_t LeftJustified_Byte;
	uint8_t DummyByte = 0;
	uint8_t  i;
	uint32_t CurrStePin;

	CurrStePin = SPI_ADDRESSES_TABLE[SpiId].STE_Pin;

	GPIO_WritePin(CurrStePin, 0); // Active CS

	// Transmit loop
	for(i=0; i < Length; i++)
	{
		LeftJustified_Byte = ((uint16_t)(pData[i]))<<8; // Data should be left justified in TXBUFF (In RXBUFF it is right justified).
		ErrCode |= SPI_TranceiveByte (SpiId, LeftJustified_Byte, &DummyByte); // ErrCode is ORed, because it can only be '0' or ERR_SPI_EXCHANGING_FAILS.
	}//end of loop

	if(DisableCS_AfterSend)
		GPIO_WritePin(CurrStePin, 1); // Disable CS

	return ErrCode;

}

/*****************************************************************************
 * Function name: SPI_Receive
 *
 * Description: SPI C Receive.
 *
 * Parameters:  *pData - a pointer to an array which will hold the received data.
 *              Length - Number of element to be recieved (array size).
 *
 * Returns: ErrCode: ERR_OK or ERR_SPI_EXCHANGING_FAILS.
 *
 * Author: Yehuda Bitton
 ******************************************************************************/
uint8_t SPI_Receive(uint32_t SpiId, uint8_t *pData, uint8_t Length)
{
	uint8_t  ErrCode = ERR_OK;
	uint8_t  i = 0;
	uint32_t CurrStePin;

	CurrStePin = SPI_ADDRESSES_TABLE[SpiId].STE_Pin;

	GPIO_WritePin(CurrStePin, 0); // Active CS


	for(i=0; i < Length; i++)
	{
		ErrCode |= SPI_TranceiveByte (SpiId, 0x0, (pData + i)); // errCode is ORed, because it can only be '0' or ERR_SPI_EXCHANGING_FAILS.

	}//end For loop

	GPIO_WritePin(CurrStePin, 1); // Disable CS

	return ErrCode;
}

/*****************************************************************************
 * Function name: SPI C Tranceive
 *
 * Description: Transmit a buffer of data and afterwards Receive data to a buffer .
 *              CS is enabled before transmit starts and disabled after receive ends.
 *
 * Parameters:  *pOutData - a pointer to an array which hold data to be sent.
 *              OutLength - Number of element to be sent (array size).
 *				*pInData  - a pointer to an array which will hold the received data.
 *              InLength  - Number of element to be recieved (array size).*
 *
 * Returns: ErrCode: ERR_OK or ERR_SPI_EXCHANGING_FAILS.
 *
 * Author: Yehuda Bitton
 ******************************************************************************/
uint8_t SPI_Tranceive(uint32_t SpiId, uint8_t *pOutData, uint8_t OutLength, uint8_t *pInData, uint8_t InLength)
{
	uint8_t  ErrCode = ERR_OK;
	uint8_t LeftJustified_Byte;
	uint8_t DummyByte = 0;
	uint8_t  i = 0;
	uint32_t CurrStePin;

	CurrStePin = SPI_ADDRESSES_TABLE[SpiId].STE_Pin;

	GPIO_WritePin(CurrStePin, 1); // Disable CS
	GPIO_WritePin(CurrStePin, 0); // Active CS

	// Transmit loop
	for(i=0; i < OutLength; i++)
	{
		LeftJustified_Byte = ((uint16_t)(pOutData[i]))<<8; // Data should be left justified in TXBUFF (In RXBUFF it is right justified).
		ErrCode |= SPI_TranceiveByte (SpiId, LeftJustified_Byte, &DummyByte);
	}//end of loop

	// Receive loop
	for(i=0; i < InLength; i++)
	{
		ErrCode |= SPI_TranceiveByte (SpiId, 0x0, (pInData + i)); // errCode is ORed, because it can only be '0' or ERR_SPI_EXCHANGING_FAILS.
	}//end of loop

	GPIO_WritePin(CurrStePin, 1); // Disable CS

	return ErrCode;
}

/*****************************************************************************
 * Function name: SPI_TranceiveByte
 *
 * Description: Single operation of both transmit and recieve (on TI C2000, it is done simultaneously).
 * 				Please note that TxDataByte should be already left justified.
 *
 * Parameters:  TxDataByte - Data byte to be sent, (can be 0x0 when just received byte is required).
 *              *RxDataByte - a pointer to hold the receive byte (can be dummy byte pointer when you just transmit).
 *
 * Returns: ErrCode: ERR_OK or ERR_SPI_EXCHANGING_FAILS.
 *
 * Author: Yehuda Bitton
 ******************************************************************************/
uint8_t SPI_TranceiveByte (uint32_t SpiId, uint8_t TxDataByte, uint8_t *RxDataByte)
{
	uint8_t  ErrCode = ERR_OK;
	uint16_t Iteration = 2000;
	volatile struct SPI_REGS *CurrSpiReg;

	CurrSpiReg = SPI_ADDRESSES_TABLE[SpiId].SPI_Reg_Adrs;


    // Transmit data
	CurrSpiReg->SPITXBUF= TxDataByte;

    // Wait until data is received
    while( CurrSpiReg->SPISTS.bit.INT_FLAG != 1 && Iteration > 0 ) {Iteration--;}

    if( Iteration == 0 )
    {
     	ErrCode = ERR_SPI_EXCHANGING_FAILS;
     	return ErrCode;
    }
    else
    {// Read recieve data:
    	*RxDataByte = CurrSpiReg->SPIRXBUF; // It also clears INT_FLAG which indicates that the SPI has completed sending or receiving.
    }

    return ErrCode;
}

/*
 * RWLK_GPIO_Driver.c
 *
 *  Created on: 30 ���� 2015
 *      Author: Saman
 */

#include "RWLK.h"
#include "RWLK_Common.h"
#include "RWLK_Motor_Control.h"
#include "../../RWLK_Periph_Driver/inc/RWLK_GPIO_Driver.h"


void DefineGC_GPIO(void)
{

				GPIO_SetupPinMux(GC1_OUT, GPIO_MUX_CPU1, 0); // Enable QEep GPIO
				GPIO_SetupPinOptions(GC1_OUT, GPIO_OUTPUT, GPIO_ASYNC);//define GPIO As input

				GPIO_SetupPinMux(GC2_OUT, GPIO_MUX_CPU1, 0); // Enable QEep GPIO
				GPIO_SetupPinOptions(GC2_OUT, GPIO_OUTPUT, GPIO_ASYNC);//define GPIO As input

				GPIO_SetupPinMux(GC3_OUT, GPIO_MUX_CPU1, 0); // Enable QEep GPIO
				GPIO_SetupPinOptions(GC3_OUT, GPIO_OUTPUT, GPIO_ASYNC);//define GPIO As input

}


void DefineCurrenSensEnable_GPIO(void)
{
	GPIO_SetupPinMux(EN_CUR_SNS, GPIO_MUX_CPU1, 0);
	GPIO_SetupPinOptions(EN_CUR_SNS, GPIO_OUTPUT, GPIO_PULLUP);
}


void  DefineDataFlagSensEnable_GPIO(void)
{
	GPIO_SetupPinMux(EN_DATA_FLAG_SNS, GPIO_MUX_CPU1, 0);
	GPIO_SetupPinOptions(EN_DATA_FLAG_SNS, GPIO_OUTPUT, GPIO_PULLUP);
}

void FlagSens_Out_Toggle(void)
{
	DATA_FLAG_SNS_TOGGLE = 1;
}

void GC1_Out_Reset(void)
{
	GC1_OUT_CLEAR = 1;
}
void GC2_Out_Reset(void)
{
	GC2_OUT_CLEAR = 1;
}
void GC3_Out_Reset(void)
{
	GC3_OUT_CLEAR = 1;
}

void GC1_Out_Set(void)
{
	GC1_OUT_SET = 1;
}
void GC2_Out_Set(void)
{
	GC2_OUT_SET = 1;

}
void GC3_Out_Set(void)
{
	GC3_OUT_SET = 1;
}

void GC_Init(void)
{
	DefineGC_GPIO();
	GC1_Out_Reset();
	GC2_Out_Reset();
	GC3_Out_Reset();
}


void CurrenSensEnable_Init(void)
{
	DefineCurrenSensEnable_GPIO();
	DELAY_US(20);
	CurrenSensEnable_Reset();
	DELAY_US(20);
}


void CurrenSensEnable_Set(void)
{
	EN_CUR_SNS_SET = 1;
}

void CurrenSensEnable_Reset(void)
{
	EN_CUR_SNS_CLEAR = 1;
}


void SafetyGPIO_Init(void)
{

	GPIO_SetupPinMux(ANALOG_UNDER_VOLTAGE, GPIO_MUX_CPU1, 0); // Enable QEep GPIO
	GPIO_SetupPinOptions(ANALOG_UNDER_VOLTAGE, GPIO_INPUT, GPIO_PULLUP);//define GPIO As input

	GPIO_SetupPinMux(ANALOG_OVER_VOLTAGE, GPIO_MUX_CPU1, 0); // Enable QEep GPIO
	GPIO_SetupPinOptions(ANALOG_OVER_VOLTAGE, GPIO_INPUT, GPIO_PULLUP);//define GPIO As input

	GPIO_SetupPinMux(BUS_VOLTAGE_PRESENT, GPIO_MUX_CPU1, 0); // Enable QEep GPIO
	GPIO_SetupPinOptions(BUS_VOLTAGE_PRESENT, GPIO_INPUT, GPIO_PULLUP);//define GPIO As input

	GPIO_SetupPinMux(DIGITAL_UNGER_VOLTAGE, GPIO_MUX_CPU1, 0); // Enable QEep GPIO
	GPIO_SetupPinOptions(DIGITAL_UNGER_VOLTAGE, GPIO_INPUT, GPIO_PULLUP);//define GPIO As input

	GPIO_SetupPinMux(DIGITAL_OVER_VOLTAGE, GPIO_MUX_CPU1, 0); // Enable QEep GPIO
	GPIO_SetupPinOptions(DIGITAL_OVER_VOLTAGE, GPIO_INPUT, GPIO_PULLUP);//define GPIO As input

	GPIO_SetupPinMux(DIGITAL_VOLTAGE_PRESENT, GPIO_MUX_CPU1, 0); // Enable QEep GPIO
	GPIO_SetupPinOptions(DIGITAL_VOLTAGE_PRESENT, GPIO_INPUT, GPIO_PULLUP);//define GPIO As input


	//Its SCIARXD for bluetooth pin
//	GPIO_SetupPinMux(64, GPIO_MUX_CPU1CLA, 0); // Enable QEep GPIO
//	GPIO_SetupPinOptions(64, GPIO_OUTPUT, GPIO_PULLUP);//define GPIO As input

}


void HallEffectMotorSensors_Init(void)
{
	GPIO_SetupPinMux(A_HALL_SENSOR, GPIO_MUX_CPU1, 0); // Enable QEep GPIO
	GPIO_SetupPinOptions(A_HALL_SENSOR, GPIO_INPUT, GPIO_PULLUP);//define GPIO As input

	GPIO_SetupPinMux(B_HALL_SENSOR, GPIO_MUX_CPU1, 0); // Enable QEep GPIO
	GPIO_SetupPinOptions(B_HALL_SENSOR, GPIO_INPUT, GPIO_PULLUP);//define GPIO As input

	GPIO_SetupPinMux(C_HALL_SENSOR, GPIO_MUX_CPU1, 0); // Enable QEep GPIO
	GPIO_SetupPinOptions(C_HALL_SENSOR, GPIO_INPUT, GPIO_PULLUP);//define GPIO As input
}

//No More

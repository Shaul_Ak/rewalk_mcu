/*
 * RWLK_EQep_Driver.c
 *
 *  Created on: Jan 13, 2016
 *      Author: arie
 */


#include "RWLK.h"
#include "RWLK_Common.h"
#include "RWLK_Motor_Control.h"
#include "../../RWLK_Periph_Driver/inc/RWLK_GPIO_Driver.h"
#include "../../RWLK_Periph_Driver/inc/RWLK_EQep_Driver.h"



/*==============================================================================
Function : CAN_Init

Description : This function initialize CANBUS driver

CAN_Base 		: CANBUS base address - Options (A channel , B channel)
CPU_Frequency 	: CANBUS Frequency (CPU_FREQ_200MHZ)
CAN_Frequency 	: CANBUS Frequency (CAN_BUS_SPEED_1MHZ , CAN_BUS_SPEED_500KHZ)
==============================================================================*/

uint16_t EQep_Init(uint32_t EQepBlockNumber, uint32_t EncMaxValue, uint32_t EncInitValue)
{
	uint16_t  ErrCode = ERR_OK;

	if(EQepBlockNumber > MAX_EQepBLOCK_NUM)
	{
		ErrCode = ERR_QE_BLOCK_NUMBER;
	}
	else
	{
		switch (EQepBlockNumber)
		{
			case QEep1BlockNum:
				GPIO_SetupPinMux(EQEP1A_IO, GPIO_MUX_CPU1, QEep1BlockEnable); // Enable QEep GPIO
				GPIO_SetupPinOptions(EQEP1A_IO, GPIO_INPUT, GPIO_SYNC);//define GPIO As input
				//QEepBlockEnable
				GPIO_SetupPinMux(EQEP1B_IO, GPIO_MUX_CPU1, QEep1BlockEnable); // Enable QEep GPIO
				GPIO_SetupPinOptions(EQEP1B_IO, GPIO_INPUT, GPIO_SYNC);//define GPIO As input

				GPIO_SetupPinMux(EQEP1S_IO, GPIO_MUX_CPU1, QEep1BlockEnable); // Enable QEep GPIO
				GPIO_SetupPinOptions(EQEP1S_IO, GPIO_INPUT, GPIO_SYNC);//define GPIO As input


				GPIO_SetupPinMux(EQEP1I_IO, GPIO_MUX_CPU1, QEep1BlockEnable); // Enable QEep GPIO
				GPIO_SetupPinOptions(EQEP1I_IO, GPIO_INPUT, GPIO_SYNC);//define GPIO As input

				break;


			case (QEep2BlockNum):
				GPIO_SetupPinMux(EQEP2A_IO, GPIO_MUX_CPU1, QEep2BlockEnable); // Enable QEep GPIO
				GPIO_SetupPinOptions(EQEP2A_IO, GPIO_INPUT, GPIO_SYNC);//define GPIO As input

				GPIO_SetupPinMux(EQEP2B_IO, GPIO_MUX_CPU1, QEep2BlockEnable); // Enable QEep GPIO
				GPIO_SetupPinOptions(EQEP2B_IO, GPIO_INPUT, GPIO_SYNC);//define GPIO As input

				GPIO_SetupPinMux(EQEP2S_IO, GPIO_MUX_CPU1, QEep2BlockEnable); // Enable QEep GPIO
				GPIO_SetupPinOptions(EQEP2S_IO, GPIO_INPUT, GPIO_SYNC);//define GPIO As input


				GPIO_SetupPinMux(EQEP2I_IO, GPIO_MUX_CPU1, QEep2BlockEnable); // Enable QEep GPIO
				GPIO_SetupPinOptions(EQEP2I_IO, GPIO_INPUT, GPIO_SYNC);//define GPIO As input

				break;

			case QEep3BlockNum:
				GPIO_SetupPinMux(EQEP3A_IO,GPIO_MUX_CPU1 , QEep3BlockEnable); //QEep3BlockEnable// Enable QEep GPIOGPIO_MUX_CPU1CLA
				GPIO_SetupPinOptions(EQEP3A_IO, GPIO_INPUT, GPIO_SYNC);//define GPIO As input
				//QEepBlockEnable
				GPIO_SetupPinMux(EQEP3B_IO, GPIO_MUX_CPU1, QEep3BlockEnable); // Enable QEep GPIO
				GPIO_SetupPinOptions(EQEP3B_IO, GPIO_INPUT, GPIO_SYNC);//define GPIO As input

				GPIO_SetupPinMux(EQEP3S_IO, GPIO_MUX_CPU1, QEep3BlockEnable); // Enable QEep GPIO
				GPIO_SetupPinOptions(EQEP3S_IO, GPIO_INPUT, GPIO_SYNC);//define GPIO As input


				GPIO_SetupPinMux(EQEP3I_IO, GPIO_MUX_CPU1, QEep3BlockEnable); // Enable QEep GPIO
				GPIO_SetupPinOptions(EQEP3I_IO, GPIO_INPUT, GPIO_SYNC);//define GPIO As input

				break;


		}
	}

	//ErrCode = ErrCode | IncEncoder_Init( EncMaxValue, EncInitValue );
	ErrCode = IncEncoder_Init( EncMaxValue, EncInitValue );

	    return ErrCode;

}

/*==============================================================================
                               Functions definition
==============================================================================*/
/*
** ===================================================================
**     Function   :  IncEncoder_Init
**
**     Description :
**					The incremental encoder initialization
**                  for the simple quadrature counter mode
**     Parameters  :
**					u32 MaxValue -  the maximun encoder counter value.
**                                  Shall be not euals to 0
**					u32 InitValue - the initial encoder counter value.
**                                  Shall not be grater then MaxValue
**     Returns     :
**					Error code. Possible values:
**                      - ERR_OK,
**                      - ERR_RANGE_INCR_ENCODER_MAX_VAL,
**                      - ERR_INCR_ENCODER_INIT_VAL. (InitValue>MaxValue)
**     Notes       :
**
** ===================================================================
*/
//#pragma CODE_SECTION(IncEncoder_Init, ".drivers")
uint8_t IncEncoder_Init( uint32_t MaxValue, uint32_t InitValue )
{
	uint8_t ErrCode = ERR_OK;

	if( MaxValue != 0)
	{
		if( InitValue > MaxValue )
		{
			ErrCode = ERR_XINCR_ENCODER_INIT_VAL;
		}
		else
		{

			// The simple quadrature counter mode
			EQep1Regs.QDECCTL.all = 0;
			EQep1Regs.QDECCTL.bit.SWAP = 0;

			EQep1Regs.QEPCTL.bit.PCRM = 1;		// QPOSCNT reset on the maximum position
			EQep1Regs.QEPCTL.bit.QPEN = 1; 		// QEP enable

			// QPOSCNT reset on the maximum position
			EQep1Regs.QPOSMAX = MaxValue;

	 		// QEP Capture Enable
			EQep1Regs.QCAPCTL.bit.CEN = 1;

			// Start value definition
			EQep1Regs.QPOSINIT = 0;
			EQep1Regs.QPOSCNT = InitValue;


			// The simple quadrature counter mode
			EQep2Regs.QDECCTL.all = 0;

			EQep2Regs.QEPCTL.bit.PCRM = 1;		// QPOSCNT reset on the maximum position
			EQep2Regs.QEPCTL.bit.QPEN = 1; 		// QEP enable

			// QPOSCNT reset on the maximum position
			EQep2Regs.QPOSMAX = MaxValue;

	 		// QEP Capture Enable
			EQep2Regs.QCAPCTL.bit.CEN = 1;

			// Start value definition
			EQep2Regs.QPOSINIT = 0;
			EQep2Regs.QPOSCNT = InitValue;


			// The simple quadrature counter mode
			EQep3Regs.QDECCTL.all = 0;

			EQep3Regs.QEPCTL.bit.PCRM = 1;		// QPOSCNT reset on the maximum position
			EQep3Regs.QEPCTL.bit.QPEN = 1; 		// QEP enable

			// QPOSCNT reset on the maximum position
			EQep3Regs.QPOSMAX = MaxValue;

	 		// QEP Capture Enable
			EQep3Regs.QCAPCTL.bit.CEN = 1;

			// Start value definition
			EQep3Regs.QPOSINIT = 0;
			EQep3Regs.QPOSCNT = InitValue;
		}
	}
	else
	{
		ErrCode = ERR_RANGE_YINCR_ENCODER_MAX_VAL;
	}

	return ErrCode;
}

/*
** ===================================================================
**     Function   :  IncEncoder_GetPosition
**
**     Description :
**					Obtain the incremental encoder current pozition
**     Parameters  :
**					u32 *Position - pointer to the current position
**     Returns     :
**					Error code. Possible values:
**                      - ERR_OK,
**                      - ERR_INCR_ENCODER_POS_COUNTER.
**     Notes       :
**
** ===================================================================
*/
#pragma CODE_SECTION(IncEncoder_GetPosition, "ramfuncs")
uint8_t IncEncoder_GetPosition( int32_t *Position )
{
	int32_t  ErrCode = ERR_OK;

	*Position = EQep1Regs.QPOSCNT;
	if( EQep1Regs.QEPSTS.bit.PCEF == 1 || *Position > EQep1Regs.QPOSMAX )
	{
		ErrCode = ERR_XINCR_ENCODER_POS_COUNTER;
		*Position = 0;
	}

	return ErrCode;
}


/*
** ===================================================================
**     Function   :  IncEncoder_DirectionRead
**
**     Description :
**					Obtain the incremental encoder current direction
**     Parameters  :
**					u8 *Direction - pointer to the current direction.
**                      Direction can be one of the following:
**                      - INC_ENCODER_ANTICLOCKWISE_ROTATION \ INC_ENCODER_REVERSE_MOVEMENT,
**                      - INC_ENCODER_CLOCKWISE_ROTATION \ INC_ENCODER_FORWARD_MOVEMENT,
**                      - INC_ENCODER_DIRECTION_FALSE.
**     Returns     :
**					Error code. Possible values:
**                      - ERR_OK,
**                      - ERR_INCR_ENCODER_POS_COUNTER.
**     Notes       :
**
** ===================================================================
*/

//#pragma CODE_SECTION(IncEncoder_ClrPosition, "Cla1Prog")
//#pragma CODE_SECTION(IncEncoder_DirectionRead, "ramfuncs")
//#pragma CODE_SECTION(IncEncoder_ClrPosition, "claram")
uint16_t IncEncoder_DirectionRead( uint16_t *Direction )
{
	uint8_t  ErrCode = ERR_OK;

	if( EQep1Regs.QEPSTS.bit.PCEF == 1 || EQep1Regs.QPOSCNT > EQep1Regs.QPOSMAX )
	{
		ErrCode = ERR_XINCR_ENCODER_POS_COUNTER;
		*Direction = INC_ENCODER_DIRECTION_FALSE;
	}
	else
	{
		*Direction = ( EQep1Regs.QEPSTS.bit.QDF == 0 ) ? INC_ENCODER_ANTICLOCKWISE_ROTATION : INC_ENCODER_CLOCKWISE_ROTATION;
	}

	return ErrCode;
}

/*
** ===================================================================
**     Function   :  IncEncoder_ClrPosition
**
**     Description :
**					Set the incremental encoder pozition value equals zero
**     Parameters  :
**					none
**     Returns     :
**					Error code. Possible values:
**                      - ERR_OK,
**                      - ERR_INCR_ENCODER_POS_COUNTER.
**     Notes       :
**
** ===================================================================
*/

//#pragma CODE_SECTION(IncEncoder_ClrPosition, "ramfuncs")
//#pragma CODE_SECTION(IncEncoder_ClrPosition, "claram")
uint8_t IncEncoder_ClrPosition( void )
{
	uint8_t  ErrCode = ERR_OK;

	if( EQep1Regs.QEPSTS.bit.PCEF == 1 )
	{
		ErrCode = ERR_XINCR_ENCODER_POS_COUNTER;
	}
	EQep1Regs.QPOSCNT = 0;

	return ErrCode;
}

// No More

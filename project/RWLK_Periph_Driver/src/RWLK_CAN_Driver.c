
/*==============================================================================
                               RWLK_CAN_Driver.c
==============================================================================*/
#include "../../RWLK_Periph_Driver/inc/RWLK_CAN_Driver.h"

#include "RWLK.h"
#include "driverlib/can.h"
#include "RWLK_Common.h"
#include "RWLK_CardConfig.h"

#include "../../RWLK_Periph_Driver/inc/RWLK_GPIO_Driver.h"
#include <stdlib.h>

/*==============================================================================
                               	   CANBUS Variables
==============================================================================*/
tCANMsgObject sCANMsgObjectPool[CAN_MAX_MAILBOX_NUM];	// Receive message object
#pragma DATA_SECTION(sCANMessageType,"ramdata")
tMsgObjType	  sCANMessageType;	// Message type

#pragma DATA_SECTION(ucTXMsgData,"ramdata")
unsigned char ucTXMsgData[8] = {0,0,0,0,0,0,0,0};  // Transmit data array (8 byte)
#pragma DATA_SECTION(ucRXMsgData,"ramdata")
unsigned char ucRXMsgData[8] = {0,0,0,0,0,0,0,0};	// Receive data array (8 byte)


/*==============================================================================
Function : CAN_Init

Description : This function initialize CANBUS driver

CAN_Base 		: CANBUS base address - Options (A channel , B channel)
CPU_Frequency 	: CANBUS Frequency (CPU_FREQ_200MHZ)
CAN_Frequency 	: CANBUS Frequency (CAN_BUS_SPEED_1MHZ , CAN_BUS_SPEED_500KHZ)
==============================================================================*/

uint16_t CAN_Init(uint32_t CAN_Base,uint32_t CPU_Frequency,uint32_t CAN_Frequency)
{
	uint16_t  ErrCode = ERR_OK;

	if(CAN_Base != CANA_BASE && CAN_Base != CANB_BASE)
	{
		ErrCode = ERR_CAN_BASE_ADDRESS;
		return ErrCode;
	}

	switch (CAN_Base)
	{
		case CANA_BASE:

			GPIO_SetupPinMux(CANBUS_CHA_Rx, GPIO_MUX_CPU1, CANBUS_Enable); // Enable CANBUS Rx GPIO
			GPIO_SetupPinOptions(CANBUS_CHA_Rx, GPIO_INPUT, GPIO_ASYNC);//define GPIO As input

			GPIO_SetupPinMux(CANBUS_CHA_Tx, GPIO_MUX_CPU1, CANBUS_Enable); // Enable CANBUS Tx GPIO
			GPIO_SetupPinOptions(CANBUS_CHA_Tx, GPIO_OUTPUT, GPIO_PUSHPULL);// define GPIO As output

			break;
		case CANB_BASE:		//Required to define the CANBUSB GPIO
			break;
	}

	// Initialize the CAN controller
		CANInit(CAN_Base);

		CANClkSourceSelect(CAN_Base, 0);

		CANBitRateSet(CAN_Base, CPU_Frequency, CAN_Frequency);

	 	CANIntEnable(CAN_Base, CAN_INT_IE0);

	 	InitMailBoxRegs(CAN_Base, CARD_MAILBOX_RX, CardConfigGetMessageID(), 0 ,0,  CAN_OBJECT_Rx_TYPE);
	 	InitMailBoxRegs(CAN_Base, CARD_MAILBOX_TX, CardConfigGetMessageID(), 0 ,0,  CAN_OBJECT_Tx_TYPE);
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Shaul 18.4.2017: ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		#ifdef SENSOR_MODE
		InitMailBoxRegs(CAN_Base, CARD_MAILBOX_IMUR_0_RX, CAN_IMUR_0_MSG_ID, 0, 0,  CAN_OBJECT_Rx_TYPE);
		InitMailBoxRegs(CAN_Base, CARD_MAILBOX_IMUR_1_RX, CAN_IMUR_1_MSG_ID, 0, 0,  CAN_OBJECT_Rx_TYPE);
		InitMailBoxRegs(CAN_Base, CARD_MAILBOX_IMUR_2_RX, CAN_IMUR_2_MSG_ID, 0, 0,  CAN_OBJECT_Rx_TYPE);
		InitMailBoxRegs(CAN_Base, CARD_MAILBOX_IMUR_3_RX, CAN_IMUR_3_MSG_ID, 0, 0,  CAN_OBJECT_Rx_TYPE);
		InitMailBoxRegs(CAN_Base, CARD_MAILBOX_IMUR_4_RX, CAN_IMUR_4_MSG_ID, 0, 0,  CAN_OBJECT_Rx_TYPE);
		InitMailBoxRegs(CAN_Base, CARD_MAILBOX_IMUR_5_RX, CAN_IMUR_5_MSG_ID, 0, 0,  CAN_OBJECT_Rx_TYPE);

		InitMailBoxRegs(CAN_Base, CARD_MAILBOX_IMUL_0_RX, CAN_IMUL_0_MSG_ID, 0, 0,  CAN_OBJECT_Rx_TYPE);
		InitMailBoxRegs(CAN_Base, CARD_MAILBOX_IMUL_1_RX, CAN_IMUL_1_MSG_ID, 0, 0,  CAN_OBJECT_Rx_TYPE);
		InitMailBoxRegs(CAN_Base, CARD_MAILBOX_IMUL_2_RX, CAN_IMUL_2_MSG_ID, 0, 0,  CAN_OBJECT_Rx_TYPE);
		InitMailBoxRegs(CAN_Base, CARD_MAILBOX_IMUL_3_RX, CAN_IMUL_3_MSG_ID, 0, 0,  CAN_OBJECT_Rx_TYPE);
		InitMailBoxRegs(CAN_Base, CARD_MAILBOX_IMUL_4_RX, CAN_IMUL_4_MSG_ID, 0, 0,  CAN_OBJECT_Rx_TYPE);
		InitMailBoxRegs(CAN_Base, CARD_MAILBOX_IMUL_5_RX, CAN_IMUL_5_MSG_ID, 0, 0,  CAN_OBJECT_Rx_TYPE);

		InitMailBoxRegs(CAN_Base,CARD_MAILBOX_LC1_RX, CAN_LC1_MSG_ID, 0, 0,  CAN_OBJECT_Rx_TYPE);
		InitMailBoxRegs(CAN_Base,CARD_MAILBOX_LC2_RX, CAN_LC2_MSG_ID, 0, 0,  CAN_OBJECT_Rx_TYPE);

		InitMailBoxRegs(CAN_Base,CARD_MAILBOX_COMMAND_RX_LC, CAN_COMMAND_RX_LC1_2,
						CAN_COMMAND_FILTER_LC, MSG_OBJ_USE_ID_FILTER,  CAN_OBJECT_Rx_TYPE);
		InitMailBoxRegs(CAN_Base,CARD_MAILBOX_COMMAND_RX_IMU_R, CAN_COMMAND_RX_IMU_R,
						CAN_COMMAND_FILTER_IMU, MSG_OBJ_USE_ID_FILTER,  CAN_OBJECT_Rx_TYPE);
		InitMailBoxRegs(CAN_Base,CARD_MAILBOX_COMMAND_RX_IMU_L, CAN_COMMAND_RX_IMU_L,
						CAN_COMMAND_FILTER_IMU, MSG_OBJ_USE_ID_FILTER,  CAN_OBJECT_Rx_TYPE);
		InitMailBoxRegs(CAN_Base,CARD_MAILBOX_COMMAND_REJECT, CAN_SENSOR_COMMAND_TX,
						CAN_COMMAND_FILTER_REJ, MSG_OBJ_USE_ID_FILTER,  CAN_OBJECT_Rx_TYPE);
		#endif  // SENSOR_MODE
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		InitMailBoxRegs(CAN_Base, CARD_MAILBOX_BROADCAST_RX,CAN_BROADCAST_MSG_ID , 0 ,0,  CAN_OBJECT_Rx_TYPE);
		InitMailBoxRegs(CAN_Base, CARD_MAILBOX_BROADCAST_TX,CAN_BROADCAST_MSG_ID , 0 ,0,  CAN_OBJECT_Tx_TYPE);

	    CANEnable(CAN_Base);

	    return ErrCode;

}


/** ===================================================================
**     Function   :  InitMailBoxRegs
**
**     Description :
**					Set the defined mail box registers for the CAN_InitMailBox functions
**     Parameters  :
**					u8   MailBox      - Mail box number. Possible values 0...31;
**					BOOL IDmasked     - ID mask flag. If TRUE, Mask parameter defineds
**                                      received messages mask; if FALSE Mask parameter is ignored;
**					u16  Mask         - LSB 11 bits of the Mask parameter defines the received
**                                      message mask. Accept a 0 or a 1 (don't care) for the
**                                      corresponding bit of the received identifier. 5 MSB bits
**                                      of the Mask are ignored. If IDmasked parameter equals to FALSE
**                                      the Mask parameter is ignored and only messages have ID exactly
**                                      equal to MessageID parameter value are accepted;
**                  MailBoxFunction
**                  					0	-	MSG_OBJ_TYPE_TX
**                  					1	-	MSG_OBJ_TYPE_RX
**                  					2	-	MSG_OBJ_TYPE_TX_REMOTE
**                  					3	-	MSG_OBJ_TYPE_RX_REMOTE
**                  					4	-	MSG_OBJ_TYPE_RXTX_REMOTE
**     Returns     :
**					none
**     Notes       :
**                  This function is only a driver function and can not be used by user
** ===================================================================
*/
//#pragma CODE_SECTION(InitMailBoxRegs, ".drivers")

void InitMailBoxRegs( uint32_t CAN_Base, uint16_t MailBox, uint32_t Msg_ID, uint32_t Mask, uint32_t Flags, uint32_t MailBoxFunction)
{

    switch(MailBoxFunction)
    {
    	case CAN_OBJECT_Tx_TYPE:
    		ClearMessageObject(CAN_OBJECT_Tx_TYPE);
    		InitMsgObj(Msg_ID, Mask, Flags, sizeof(ucTXMsgData),CAN_OBJECT_Tx_TYPE,MailBox);
    		CANMessageSet(CAN_Base, MailBox, &sCANMsgObjectPool[MailBox], MSG_OBJ_TYPE_TX);
    		break;

    	case CAN_OBJECT_Rx_TYPE:
    		ClearMessageObject(CAN_OBJECT_Rx_TYPE);
    		InitMsgObj(Msg_ID, Mask, Flags, sizeof(ucRXMsgData),CAN_OBJECT_Rx_TYPE,MailBox);
    		CANMessageSet(CAN_Base, MailBox, &sCANMsgObjectPool[MailBox], MSG_OBJ_TYPE_RX);
    		break;

    	case CAN_OBJECT_REMOTE_Tx_TYPE:
    		ClearMessageObject(CAN_OBJECT_Tx_TYPE);
    		InitMsgObj(Msg_ID, Mask, Flags, sizeof(ucTXMsgData), CAN_OBJECT_REMOTE_Tx_TYPE,MailBox);
    		CANMessageSet(CAN_Base, MailBox, &sCANMsgObjectPool[MailBox], MSG_OBJ_TYPE_TX_REMOTE);
    		break;

    	case CAN_OBJECT_REMOTE_Rx_TYPE:
    		ClearMessageObject(CAN_OBJECT_Rx_TYPE);
    		InitMsgObj(Msg_ID, Mask, Flags, sizeof(ucRXMsgData), CAN_OBJECT_REMOTE_Rx_TYPE,MailBox);
    		CANMessageSet(CAN_Base, MailBox, &sCANMsgObjectPool[MailBox], MSG_OBJ_TYPE_RX_REMOTE);
    		break;

    	case CAN_OBJECT_REMOTE_TxRx_TYPE:
    		ClearMessageObject(CAN_OBJECT_Tx_TYPE);
    		ClearMessageObject(CAN_OBJECT_Rx_TYPE);
    		InitMsgObj(Msg_ID, Mask, Flags, sizeof(ucTXMsgData),  CAN_OBJECT_REMOTE_TxRx_TYPE,MailBox);
    		CANMessageSet(CAN_Base, MailBox, &sCANMsgObjectPool[MailBox],  MSG_OBJ_TYPE_RXTX_REMOTE);
    		break;

    }


}

/*==============================================================================
Function : InitMsgObj

Description : This function initialize CANBUS Message Object

MsgID 		: Message ID
MsgIDMask 	: 0 The corresponding bit in the identifier of the message object is not
				used for acceptance filtering (don't care).
			  1 The corresponding bit in the identifier of the message object is
				used for acceptance filtering.

SetMsgFlags : Message control register Flags :  CAN_IF2MCTL Register
MsgLengs	: Message number of bytes

==============================================================================*/

void InitMsgObj(uint32_t MsgID, uint32_t MsgIDMask, uint32_t SetMsgFlags, uint32_t MsgLengs, uint32_t TxRx,uint16_t MailBox)
{
	sCANMsgObjectPool[MailBox].ui32MsgID      = MsgID;             // CAN message ID - use 320 for receive
	sCANMsgObjectPool[MailBox].ui32MsgIDMask  = MsgIDMask;         // no mask needed for Rx
	sCANMsgObjectPool[MailBox].ui32Flags      = SetMsgFlags; 	   // MSG_OBJ_RX_INT_ENABLE; //(MSG_OBJ_USE_DIR_FILTER | MSG_OBJ_RX_INT_ENABLE | MSG_OBJ_USE_ID_FILTER);
	sCANMsgObjectPool[MailBox].ui32MsgLen     = MsgLengs;          // sizeof(ucRXMsgData);   // size of message is 4

	if(TxRx == CAN_OBJECT_Rx_TYPE)
	{
		sCANMsgObjectPool[MailBox].pucMsgData = ucRXMsgData;       // ptr to RX message content
	}
	else
	{
		sCANMsgObjectPool[MailBox].pucMsgData = ucTXMsgData;	   // ptr to RX message content
	}
}

/*==============================================================================
Function : ClearMessageObject

Description 		: This function Clears Message objetcs

MailBoxFunction 	: Based on MailBox function

==============================================================================*/

void ClearMessageObject(uint32_t MailBoxFunction)
{

	switch(MailBoxFunction)
	{
	case CAN_OBJECT_Tx_TYPE :
		*(unsigned long *)ucTXMsgData = 0;
		InitMsgObj(0, 0, 0, 0, CAN_OBJECT_Tx_TYPE,0);
		break;
	case CAN_OBJECT_Rx_TYPE:
		*(unsigned long *)ucRXMsgData = 0;
		InitMsgObj(0, 0, 0, 0, CAN_OBJECT_Rx_TYPE,0);
		break;
	}

}


void RWLK_CanTransmitDriver(uint32_t CAN_Base, uint16_t MailBox, tCANMsgObject *pMsgObject)
{

	CANMessageSet(CAN_Base, MailBox, pMsgObject, MSG_OBJ_TYPE_TX);
}

bool RWLK_IsCanTransmit(uint32_t CAN_Base)
{
	return (bool)CANStatusGet(CAN_Base, CAN_STS_TXREQUEST );
}

void RWLK_CanReceiveDriver(uint32_t CAN_Base, uint16_t MailBox, tCANMsgObject *pMsgObject, bool bClrPendingInt)
{
	CANMessageGet(CAN_Base, MailBox, pMsgObject, bClrPendingInt);
}

//No More

/*******************************************************************************
* file name: Timer_Driver.c
*
* Description: Timer Driver to CPU-Timers.
*              It enables to set start and stop timers.
*              Enable/disable interrupts and read/clear overflow flag.
*
*              Timer0 is used as mainloop triger with interrupt each mili second.
*              Timer1 is a cyclic counter, which ticks every 1 milisecond.
*              Timer2 is a cyclic counter, which ticks evey 1 microsecond.
*
* Note: Developed for TI F28377D.
*
* Author:  Yehuda Bitton
* Date:    21/01/2016
*
******************************************************************************/

/*******************************************************************************
*  Includes
******************************************************************************/
#include "../../RWLK_Periph_Driver/inc/RWLK_Timer_Driver.h"     // Device Header file and Examples Include File

#include "RWLK.h"
#include "RWLK_Common.h"

/*******************************************************************************
*  Data types
******************************************************************************/

/*------------------------------------------------------------------------------
Variables structures
------------------------------------------------------------------------------*/

/*******************************************************************************
*  Constants and Macros
******************************************************************************/

/*******************************************************************************
*  Static Data deceleration
******************************************************************************/

/*******************************************************************************
*  Private Functions Prototypes
******************************************************************************/

/*******************************************************************************
*  Public Function Bodies
******************************************************************************/

/*****************************************************************************
 * Function name: D_InitCpuTimers
 *
 * Description: Initialize CPU timers.
 *
 * Parameters:  None.
 * Returns:     None.
 *
 * Known issues:
 * Todo:
 *
 ******************************************************************************/
void D_InitCpuTimers(void)
{
	InitCpuTimers();   // This function can be found in F2837xD_CpuTimers.c
}

/*****************************************************************************
* Function   :  D_Timer_Start
*
* Description : Start Timer.
*
* Parameters  :TimerNo - Timer to be started. Possible values:
*				- TIMER_0,
*				- TIMER_1,
*				- TIMER_2;
*              Us - number of microseconds.
*
* Returns: Error code. Possible values:
*           - ERR_OK,
*           - ERR_RANGE_TIMER_PERIOD,
*
* Known issues: Currently this function is avilable only for Timer0,
* 		since Timer1 and Timer2 are used as mili and micro secod counter.
* Todo:
******************************************************************************/

uint8_t D_Timer_Start ( uint8_t TimerNo, uint32_t Us )
{
	uint8_t ErrCode = ERR_OK;

	if( Us == 0 )
	{
		ErrCode = ERR_RANGE_TIMER_PERIOD;
	}
	else
	{
		switch( TimerNo )
		{
	  		case TIMER_0:
			{
				ConfigCpuTimer ( &CpuTimer0, 200, Us );
				// To ensure precise timing, use write-only instructions to write to the entire register. Therefore, if any
				// of the configuration bits are changed in ConfigCpuTimer and InitCpuTimers (in F2837xD_cputimervars.h), the
				// below settings must also be updated.
				CpuTimer0Regs.TCR.all = 0x4000; // Use write-only instruction to set TSS bit = 0
			}break;
	  		case TIMER_1:
			{
				ConfigCpuTimer ( &CpuTimer1, 200, Us );
				// To ensure precise timing, use write-only instructions to write to the entire register. Therefore, if any
				// of the configuration bits are changed in ConfigCpuTimer and InitCpuTimers (in F2837xD_cputimervars.h), the
				// below settings must also be updated.
				CpuTimer1Regs.TCR.all = 0x4000; // Use write-only instruction to set TSS bit = 0
			}break;
	  		case TIMER_2:
			{
				ConfigCpuTimer ( &CpuTimer2, 200, Us );
				// To ensure precise timing, use write-only instructions to write to the entire register. Therefore, if any
				// of the configuration bits are changed in ConfigCpuTimer and InitCpuTimers (in F2837xD_cputimervars.h), the
				// below settings must also be updated.
				CpuTimer2Regs.TCR.all = 0x4000; // Use write-only instruction to set TSS bit = 0
			}break;

	  		default:
	  			ErrCode = ERR_RANGE_TIMER_NUMBER;
		}
	}

  return ErrCode;
}


/*****************************************************************************
* Function: D_Timer_Stop
*
* Description: Stop cpu timer.
*
* Parameters: TimerNo - Timer to be stopped. Possible values:
*   		   - TIMER_0,
*			   - TIMER_1,
* 			   - TIMER_2.
*
* Returns: Error code. Possible values:
*           - ERR_OK,
*           - ERR_RANGE_TIMER_NUMBER,
*
* Known issues:
* Todo:
******************************************************************************/
uint8_t D_Timer_Stop ( uint8_t TimerNo )
{

	uint8_t ErrCode = ERR_OK;

	switch( TimerNo )
	{
		case TIMER_0:
		{
			CpuTimer0Regs.TCR.bit.TSS = 1;
		}break;

		case TIMER_1:
		{
			CpuTimer1Regs.TCR.bit.TSS = 1;
		}break;

		case TIMER_2:
		{
			CpuTimer2Regs.TCR.bit.TSS = 1;
		}break;

		default:
			ErrCode = ERR_RANGE_TIMER_NUMBER;
	}

	return ErrCode;
}

/*****************************************************************************
* Function:  D_Timer_EnableInterrupt
*
* Description: Enable Cpu timer interrupt.
*              timer 0 -> PIE -> INT1
*              timer 1 -> INT13
*              timer 2 -> INT14
*
* Parameters: TimerNo - Timer to be started. Possible values:
*   		   - TIMER_0.
*			   - TIMER_1.
*			   - TIMER_2.
*
* Returns: Error code. Possible values:
*           - ERR_OK [0],
*           - ERR_RANGE_TIMER_NUMBER [2].
*
* Notes: To obtain the timer interrupt user must enable global interrupts using
*	     the Int_EnableGlobalInterrupts function.
 ******************************************************************************/
uint8_t D_Timer_EnableInterrupt ( uint8_t TimerNo )
{

	uint8_t ErrCode = ERR_OK;

	switch( TimerNo )
	{
  		case TIMER_0:
		{
			// Enable interrupt bit of TIMER0 TINT0 @ PIE Interrupt Enable Register:
			PieCtrlRegs.PIEIER1.bit.INTx7 = 1;
			// Enable CPU INT1:
			IER |= M_INT1;
		} break;

	  	case TIMER_1:
		{
			// Enable CPU INT13 (dedicated to TIMER1 INT1):
			IER |= M_INT13;
			CpuTimer1Regs.TCR.bit.TIE = 1;
		}break;

  		case TIMER_2:
		{
			// Enable CPU INT14 (dedicated to TIMER2 INT2):
			IER |= M_INT14;
			CpuTimer2Regs.TCR.bit.TIE = 1;
		}break;

  		default:
  			ErrCode = ERR_RANGE_TIMER_NUMBER;
	}

	return ErrCode;
}

/*****************************************************************************
* Function:  D_Timer_DisableInterrupt.
*
* Description: Disable CPU timer interrupt.
*              timer 0 -> PIE -> INT1
*              timer 1 -> INT13
*              timer 2 -> INT14
*
* Parameters: TimerNo - Timer to be started. Possible values:
*   		   - TIMER_0,
*			   - TIMER_1,
*			   - TIMER_2;
*             Us - number of microseconds.
*
* Returns:Error code. Possible values:
*          - ERR_OK,
*          - ERR_RANGE_TIMER_NUMBER.
*
* Known issues:
* Todo:
 ******************************************************************************/

uint8_t D_Timer_DisableInterrupt ( uint8_t TimerNo )
{
	uint8_t ErrCode = ERR_OK;

	switch( TimerNo )
	{
  		case TIMER_0:
		{
			DINT; // Disabel global interrupt
			// Disable interrupt bit of TIMER0 TINT0 @ PIE Interrupt Enable Register:
			PieCtrlRegs.PIEIER1.bit.INTx7 = 0;
			// Wait 5 cycles for possibele propogating interrupt.
			asm (" RPT #4 || NOP");
			// Clear CPU INT1 Flag:
			IFR &= ~M_INT1;
			// Acknowledge this interrupt to receive more interrupts from group 1:
			PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
			EINT; //Enabel Global interrupt
		} break;

	  	case TIMER_1:
		{
			CpuTimer1Regs.TCR.bit.TIE = 0;
			//asm("  AND IER, #0xEFFF  ");
			IER &= ~M_INT13;
		}break;

  		case TIMER_2:
		{
			CpuTimer2Regs.TCR.bit.TIE = 0;
			//AND IFR,#~M_INT14;
			IER &= ~M_INT14;
		}break;

  		default:
  			ErrCode = ERR_RANGE_TIMER_NUMBER;
	}

	return ErrCode;
}

/*****************************************************************************
* Function: D_Timer_Get_Overflow_Flag
*
* Description: Get over flow flag (which occures when timer's counter reach zero).
*
* Parameters: TimerNo - Timer number. Possible values:
*			   - TIMER_0,
*			   - TIMER_1,
*			   - TIMER_2.
*             EventFlag - pointer to EventFlag:
*              [0] - overflow did not occured.
*              [1] - overflow did occured.
*
* Returns: Error code. Possible values:
*           - ERR_OK,
*           - ERR_RANGE_TIMER_NUMBER.
*
* Known issues:
* Todo:
******************************************************************************/
uint8_t D_Timer_Get_Overflow_Flag ( uint8_t TimerNo, uint8_t *EventFlag )
{
	uint8_t ErrCode = ERR_OK;

	switch( TimerNo )
	{
  		case TIMER_0:
		{
			*EventFlag = CpuTimer0Regs.TCR.bit.TIF;
		}break;

  		case TIMER_1:
		{
			*EventFlag = CpuTimer1Regs.TCR.bit.TIF;
		}break;

  		case TIMER_2:
		{
			*EventFlag = CpuTimer2Regs.TCR.bit.TIF;
		}break;

  		default:
  			ErrCode = ERR_RANGE_TIMER_NUMBER;
	}

	return ErrCode;
}


/*****************************************************************************
* Function: D_Timer_Clear_Overflow_Flag
*
* Description: Clear timer interrupt overflow flag.
*
* Parameters: TimerNo - Timer number. Possible values:
*			  - TIMER_0,
*			  - TIMER_1,
*			  - TIMER_2.
*
* Returns: Error code. Possible values:
*          - ERR_OK,
*          - ERR_RANGE_TIMER_NUMBER.
*
* Notes: Use this function when timer runs and timer or global interrupt disable.
******************************************************************************/

uint8_t D_Timer_Clear_Overflow_Flag ( uint8_t TimerNo )
{
	uint8_t ErrCode = ERR_OK;

	switch( TimerNo )
	{
  		case TIMER_0:
  		{
			CpuTimer0Regs.TCR.bit.TIF = 1;
  		}break;

  		case TIMER_1:
  		{
			CpuTimer1Regs.TCR.bit.TIF = 1;
  		}break;

  		case TIMER_2:
  		{
			CpuTimer2Regs.TCR.bit.TIF = 1;
  		}break;

  		default:
  			ErrCode = ERR_RANGE_TIMER_NUMBER;
	}

	return ErrCode;
}

/*****************************************************************************
* Function: D_Timer_Is_Stop
*
* Description: Is Timer is started or soped.
*
* Parameters: TimerNo - Timer number. Possible values:
*			   - TIMER_0,
*			   - TIMER_1,
*			   - TIMER_2.
*
* Returns: stop_bit. Possible values:
*           [0] - did not stoped.
*           [1] - stopped.
*           [2] - ERR_RANGE_TIMER_NUMBER
*
* Known issues:
* Todo:
******************************************************************************/
uint8_t D_Timer_Is_Stop ( uint8_t TimerNo )
{

	switch( TimerNo )
	{
  		case TIMER_0:
		{
			return CpuTimer0Regs.TCR.bit.TSS;
		}

  		case TIMER_1:
		{
			return CpuTimer1Regs.TCR.bit.TSS;
		}

  		case TIMER_2:
		{
			return CpuTimer2Regs.TCR.bit.TSS;
		}

  		default:
  			return ERR_RANGE_TIMER_NUMBER;
	}

}

/*****************************************************************************
* Function: D_Timer_Get_uSec_Counter
*
* Description: return micro seconds since Timer2 start.
*              Please note that approximatly every 70 minutes the counter will be zeored.
*
* Parameters: None.
* Returns: CPU Timer Counter - 32bit.
*
******************************************************************************/
uint32_t D_Timer_Get_uSec_Counter ( void )
{
	//Timer2 is 32 bit count down timer
	return 0xFFFFFFFF - CpuTimer2Regs.TIM.all;
}

/*****************************************************************************
* Function: D_Timer_Get_mSec_Counter
*
* Description: return micro seconds since Timer2 start.
*              Please note that approximatly every 70 minutes the counter will be zeored.
*
* Parameters: None.
* Returns: CPU Timer Counter - 32bit.
*
******************************************************************************/
uint32_t D_Timer_Get_mSec_Counter ( void )
{
	//Timer1 is 32 bit count down timer
	return ((0xFFFFFFFF - CpuTimer1Regs.TIM.all)>>2);
}

/*****************************************************************************
* Function: D_Timer_Config_mSec_Timer
*
* Description: Configure Timer1 to tick every 0.25 msec. It is a cyclic timer counter of 124.2 days.
*
* 			   Yehuda Bitton:
* 			   Timer operation Descripiton:
* 			   Timer input clock is CPU1.sysclk (currently configured to 200MHz - see 'InitSysPll' function).
* 			   TIM - Timer counter register (32bit), is loaded by PRD (Period Reg) value and counts down to zero.
* 			   TDDR - Timer Divide Down Register is a 16 bit register placed in TPR[0:7] (Low byte), TPRH[0:7] (High byte).
* 			   Every (TDDRH:TDDR + 1) timer clock source cycles, the timer counter register (TIM) decrements by one.
*
* 			   In order to produce 1 milisecond Timer TDDR is used:
* 			   TDDRH:TDDR = 49999
* 			   TimerTick = (TDDRH:TDDR + 1)/CPU1.sysclk = 50000/200MHz = 0.25msec.
*
* 			   PRD (Period Reg) is set to max value in order to generate the longest Timer counter.
*
* Parameters: None.
* Returns:
*
******************************************************************************/
// after configuration.
void D_Timer_Config_mSec_Timer(void)
{ //mili-second counter
    struct CPUTIMER_VARS *Timer1;
    Timer1 = &CpuTimer1;

    // Initialize timer period:
    Timer1->CPUFreqInMHz = 200;

    Timer1->RegsAddr->PRD.all = 0xFFFFFFFF; //Maximum value In period register.

    // Set pre-scale counter to divide by 1 (SYSCLKOUT):
    Timer1->RegsAddr->TPR.all  = 0x004F; //0.25msec
    Timer1->RegsAddr->TPRH.all  =0x00C3;

    // Initialize timer control register:
    Timer1->RegsAddr->TCR.bit.TSS = 1;      // 1 = Stop timer, 0 = Start/Restart
                                           // Timer
    Timer1->RegsAddr->TCR.bit.TRB = 1;      // 1 = reload timer
    Timer1->RegsAddr->TCR.bit.SOFT = 0;
    Timer1->RegsAddr->TCR.bit.FREE = 0;     // Timer Free Run Disabled
    Timer1->RegsAddr->TCR.bit.TIE = 0;      // 0 = Disable/ 1 = Enable Timer
                                           // Interrupt

    // Reset interrupt counter:
    Timer1->InterruptCount = 0;

    // start timer:
    Timer1->RegsAddr->TCR.bit.TSS = 0;      // 1 = Stop timer, 0 = Start/Restart

}

/*****************************************************************************
* Function: D_Timer_Config_uSec_Timer
*
* Description: Configure Timer2 to tick every 1usec. It is a cyclic timer counter of 71.5 minutes.
*
* 			   Yehuda Bitton:
* 			   Timer operation Descripiton:
* 			   Timer input clock is CPU1.sysclk (currently configured to 200MHz - see 'InitSysPll' function).
* 			   TIM - Timer counter register (32bit), is loaded by PRD (Period Reg) value and counts down to zero.
* 			   TDDR - Timer Divide Down Register is a 16 bit register placed in TPR[0:7] (Low byte), TPRH[0:7] (High byte).
* 			   Every (TDDRH:TDDR + 1) timer clock source cycles, the timer counter register (TIM) decrements by one.
*
* 			   In order to produce 1 milisecond Timer TDDR is used:
* 			   TDDRH:TDDR = 199
* 			   TimerTick = (TDDRH:TDDR + 1)/CPU1.sysclk = 200/200MHz = 1usec.
*
* 			   PRD (Period Reg) is set to max value in order to generate the longest Timer counter.
*
* Parameters: None.
* Returns:
*
******************************************************************************/

void D_Timer_Config_uSec_Timer(void)
{// micro-second counter
    struct CPUTIMER_VARS *Timer2;
    Timer2 = &CpuTimer2;

    // Initialize timer period:
    Timer2->CPUFreqInMHz = 200;
    Timer2->RegsAddr->PRD.all = 0xFFFFFFFF; //Maximum value In period register.

    // Set pre-scale counter to divide by 1 (SYSCLKOUT):
    Timer2->RegsAddr->TPR.all  = 0x00C7; // C7 = 199(dec)
    Timer2->RegsAddr->TPRH.all  = 0x0000;

    // Initialize timer control register:
    Timer2->RegsAddr->TCR.bit.TSS = 1;      // 1 = Stop timer, 0 = Start/Restart
                                           // Timer
    Timer2->RegsAddr->TCR.bit.TRB = 1;      // 1 = reload timer
    Timer2->RegsAddr->TCR.bit.SOFT = 0;
    Timer2->RegsAddr->TCR.bit.FREE = 0;     // Timer Free Run Disabled
    Timer2->RegsAddr->TCR.bit.TIE = 0;      // 0 = Disable/ 1 = Enable Timer
                                           // Interrupt

    // Reset interrupt counter:
    Timer2->InterruptCount = 0;

    // start timer:
    Timer2->RegsAddr->TCR.bit.TSS = 0;      // 1 = Stop timer, 0 = Start/Restart
}



/*
 * RWLK_ePWM_Driver.c
 *
 *  Created on: Jan 27, 2016
 *      Author: arie
 */



#include "RWLK.h"
#include "RWLK_Common.h"
#include "IQmathLib.h"
#include "RWLK_Motor_Control.h"

#include "../../RWLK_Periph_Driver/inc/RWLK_CAN_Driver.h"
#include "../../RWLK_Periph_Driver/inc/RWLK_GPIO_Driver.h"
#include "../../RWLK_Periph_Driver/inc/RWLK_ePWM_Driver.h"


//#define PWM_DC	INV_PWM_HALF_TBPRD

/*==============================================================================
Function : EPWM_Init

Description : This function initialize EPWM BLOCKS

ePWM_Block 		: EPWM number - Options (block#1 to block#12)
============================================================================*/
#pragma CODE_SECTION(EPWM_IO_Init,"ramfuncs")
uint16_t EPWM_IO_Init(uint32_t ePWM_Block)
{
	uint16_t  ErrCode = ERR_OK;

	if(ePWM_Block > MAX_ePWM_NUM || ePWM_Block < MIN_ePWM_NUM)
	{
		ErrCode = ERR_CAN_BASE_ADDRESS;
	}
	else
	{
		switch (ePWM_Block)
		{
			case ePWM1BlockNum:

				GPIO_SetupPinMux(EPWM1A_IO, GPIO_MUX_CPU1, ePWM_Enable); // Enable CANBUS Rx GPIO
				GPIO_SetupPinOptions(EPWM1A_IO, GPIO_OUTPUT, GPIO_PULLUP);//define GPIO As input

				GPIO_SetupPinMux(EPWM1B_IO, GPIO_MUX_CPU1, ePWM_Enable); // Enable CANBUS Tx GPIO
				GPIO_SetupPinOptions(EPWM1B_IO, GPIO_OUTPUT, GPIO_PULLUP);// define GPIO As output

				//GPIO_SetupPinMux(EPWM1A_IO, GPIO_MUX_CPU1, 0); // Enable CANBUS Rx GPIO
				//GPIO_SetupPinOptions(EPWM1A_IO, GPIO_OUTPUT, GPIO_PULLUP);//define GPIO As input

				//GPIO_SetupPinMux(EPWM1B_IO, GPIO_MUX_CPU1, 0); // Enable CANBUS Tx GPIO
				//GPIO_SetupPinOptions(EPWM1B_IO, GPIO_OUTPUT, GPIO_PULLUP);// define GPIO As output

				break;

			case ePWM2BlockNum:		//Required to define the CANBUSB GPIO

				GPIO_SetupPinMux(EPWM2A_IO, GPIO_MUX_CPU1, ePWM_Enable); // Enable CANBUS Rx GPIO
				GPIO_SetupPinOptions(EPWM2A_IO, GPIO_OUTPUT, GPIO_PULLUP);//define GPIO As input

				GPIO_SetupPinMux(EPWM2B_IO, GPIO_MUX_CPU1, ePWM_Enable); // Enable CANBUS Tx GPIO
				GPIO_SetupPinOptions(EPWM2B_IO, GPIO_OUTPUT, GPIO_PULLUP);// define GPIO As output

				break;


			case ePWM3BlockNum:		//Required to define the CANBUSB GPIO

				GPIO_SetupPinMux(EPWM3A_IO, GPIO_MUX_CPU1, ePWM_Enable); // Enable CANBUS Rx GPIO
				GPIO_SetupPinOptions(EPWM3A_IO, GPIO_OUTPUT, GPIO_PULLUP);//define GPIO As input

				GPIO_SetupPinMux(EPWM3B_IO, GPIO_MUX_CPU1, ePWM_Enable); // Enable CANBUS Tx GPIO
				GPIO_SetupPinOptions(EPWM3B_IO, GPIO_OUTPUT, GPIO_PULLUP);// define GPIO As output

				break;

			case ePWM10BlockNum:		//Required to define the CANBUSB GPIO

				GPIO_SetupPinMux(EPWM10A_IO, GPIO_MUX_CPU1, ePWM_Enable); // Enable CANBUS Rx GPIO
				GPIO_SetupPinOptions(EPWM10A_IO, GPIO_OUTPUT, GPIO_PULLUP);//define GPIO As input

				GPIO_SetupPinMux(EPWM10B_IO, GPIO_MUX_CPU1, ePWM_Enable); // Enable CANBUS Tx GPIO
				GPIO_SetupPinOptions(EPWM10B_IO, GPIO_OUTPUT, GPIO_PULLUP);// define GPIO As output

				break;

			case ePWM11BlockNum:		//Required to define the CANBUSB GPIO

				GPIO_SetupPinMux(EPWM11A_IO, GPIO_MUX_CPU1, ePWM_Enable); // Enable CANBUS Rx GPIO
				GPIO_SetupPinOptions(EPWM11A_IO, GPIO_OUTPUT, GPIO_PULLUP);//define GPIO As input

				GPIO_SetupPinMux(EPWM11B_IO, GPIO_MUX_CPU1, ePWM_Enable); // Enable CANBUS Tx GPIO
				GPIO_SetupPinOptions(EPWM11B_IO, GPIO_OUTPUT, GPIO_PULLUP);// define GPIO As output

				break;

			case ePWM12BlockNum:		//Required to define the CANBUSB GPIO

				GPIO_SetupPinMux(EPWM12A_IO, GPIO_MUX_CPU1, ePWM_Enable); // Enable CANBUS Rx GPIO
				GPIO_SetupPinOptions(EPWM12A_IO, GPIO_OUTPUT, GPIO_PULLUP);//define GPIO As input

				GPIO_SetupPinMux(EPWM12B_IO, GPIO_MUX_CPU1, ePWM_Enable); // Enable CANBUS Tx GPIO
				GPIO_SetupPinOptions(EPWM12B_IO, GPIO_OUTPUT, GPIO_PULLUP);// define GPIO As output

				break;

		}

	}
	    return ErrCode;

}

/*
** ===================================================================
**     Function   :  PWM_Start
**
**     Description :
**					Start ePWM1 and ePWM2 modules pulses using TB and DB submodules.
**                  DB operates in the AHC mode
**     Parameters  :
**					u16 Periode   - the PWM period(us/100 units);
**                  u16 DutyCycle - the Duty (Low level)cycle time (us/100 units);
**					u16 RED_FED   - the RED(FED) time (us/100 units);
**     Returns     :
**					Error code. Possible values:
**                      - ERR_OK,
**                      - ERR_PWM_PERIOD_RANGE, (Period=0),
**						- ERR_PWM_DUTY_RANGE, (DutyCycle = 0 or DutyCycle not less then Period)
**                      - ERR_PWM_RED_FED_RANGE, (RED_FED < 1 or RED_FED > 1023)
**						- ERR_PWM_RED_GT_DUTY, (RED_FED not less then 2*DutyCycle)
**						- ERR_PWM_FED_GT_DUTY, (RED_FED not less then 2*(Period - Duty cycle)
**     Notes       :
**					1. The document 'Argo PWM waveform.doc' represents the PWM wafeform
**                  2. RED alwas equals to FED in our case
**
** ===================================================================
*/

#pragma CODE_SECTION(PwmInitDriver,"ramfuncs")
uint16_t PwmInitDriver( uint16_t Period, uint16_t DutyCycle, uint16_t RED_FED )
{
	uint16_t  ErrCode = ERR_OK;
	uint16_t Period2 = Period;
	uint16_t RED_FED2 = RED_FED;
	uint16_t DutyCycle2 = Period2 - DutyCycle;

	if( Period == 0 )
	{
		ErrCode = ERR_PWM_PERIOD_RANGE;
	}
	else
	{
		if( DutyCycle == 0 || DutyCycle >= Period )
		{
			ErrCode = ERR_PWM_DUTY_RANGE;
		}
		else
		{
			if( RED_FED < PWM_RED_MIN_VAL || RED_FED > PWM_RED_MAX_VAL )
			{
				ErrCode = ERR_PWM_RED_FED_RANGE;
			}
			else
			{
				if( RED_FED >= 2 * DutyCycle )
				{
					ErrCode = ERR_PWM_RED_GT_DUTY;
				}
				else
				{
					if( RED_FED >= 2 * ( Period - DutyCycle ) )
					{
						ErrCode = ERR_PWM_FED_GT_DUTY;
					}
				}
			}
		}
	}
			EALLOW;

		EPWM_IO_Init(ePWM1BlockNum);
		EPWM_IO_Init(ePWM2BlockNum);
		EPWM_IO_Init(ePWM3BlockNum);
	if( ErrCode == ERR_OK )
	{
		// EPWM Module 1 config
		EPwm1Regs.TBPRD = Period; // PWM Period = 2*Period TBCLK counts
		EPwm2Regs.TBPHS.bit.TBPHS = 0; // Set Phase register to zero
		EPwm1Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;// Symmetrical mode
		EPwm1Regs.TBCTL.bit.PHSEN = TB_DISABLE; // Master module
		EPwm1Regs.TBCTL.bit.PRDLD = TB_SHADOW;
		EPwm1Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1; // TBCLK = SYSCLK
		EPwm1Regs.TBCTL.bit.CLKDIV = TB_DIV1;
		EPwm1Regs.TBCTL.bit.SYNCOSEL = TB_CTR_ZERO; // Sync down-stream module
		EPwm1Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
		EPwm1Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
		EPwm1Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO; // load on CTR=Zero
		EPwm1Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO; // load on CTR=Zero
#if IDDK_MODULE == 0 && BOOSTXL == 1
		EPwm1Regs.AQCTLA.bit.CAU = AQ_CLEAR;//AQ_SET; // set actions for EPWM1A
		EPwm1Regs.AQCTLA.bit.CAD = AQ_SET;//AQ_CLEAR;
#else
		EPwm1Regs.AQCTLA.bit.CAU = AQ_SET; // set actions for EPWM1A
		EPwm1Regs.AQCTLA.bit.CAD = AQ_CLEAR;
#endif
		EPwm1Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE; // enable Dead-band module
		EPwm1Regs.DBCTL.bit.POLSEL = DB_ACTV_HIC; // Active Hi complementary
		EPwm1Regs.DBFED = RED_FED; // TBCLK counts
		EPwm1Regs.DBRED = RED_FED; // TBCLK counts


		EPwm1Regs.ETSEL.bit.SOCAEN	= 1;	        // Disable SOC on A group
		EPwm1Regs.ETSEL.bit.SOCASEL	= 1;	        // Select SOC on TBCTR = 0
		EPwm1Regs.ETPS.bit.SOCAPRD = 1;		        // Generate pulse on 1st event
		EPwm1Regs.ETSEL.bit.INTEN = 1;     			// Enable ePWM Interrupt
#if IDDK_MODULE == 0 && BOOSTXL == 0
		EPwm1Regs.ETSEL.bit.INTSEL = 1;				// Select ePWM Interrupt on TBCTR = 0
#else
		EPwm1Regs.ETSEL.bit.INTSEL = 2;				// Select ePWM Interrupt on TBCTR = TBPRD
#endif

		EPwm1Regs.ETPS.bit.INTPRD=1;
		EPwm1Regs.ETFLG.all=0;
		EPwm1Regs.ETCLR.all=0;
		EPwm1Regs.ETFRC.all=0;
		EPwm1Regs.CMPA.bit.CMPA = DutyCycle; // PWM duty = 2*DutyCycle  TBCLK counts

		// EPWM Module 2 config
		EPwm2Regs.TBPRD = Period2; // PWM Period = 2*Period TBCLK counts
		EPwm2Regs.TBPHS.bit.TBPHS = 0; // Set Phase register to zero
		EPwm2Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;// Symmetrical mode
		EPwm2Regs.TBCTL.bit.PHSEN = TB_ENABLE; // Slave module
		EPwm2Regs.TBCTL.bit.PRDLD = TB_SHADOW;
		EPwm2Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1; // TBCLK = SYSCLK
		EPwm2Regs.TBCTL.bit.CLKDIV = TB_DIV1;
		EPwm2Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_IN; // sync flow-through
		EPwm2Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
		EPwm2Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
		EPwm2Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO; // load on CTR=Zero
		EPwm2Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO; // load on CTR=Zero
#if IDDK_MODULE == 0 && BOOSTXL == 1
		EPwm2Regs.AQCTLA.bit.CAU = AQ_CLEAR;//AQ_SET; // set actions for EPWM1A
		EPwm2Regs.AQCTLA.bit.CAD = AQ_SET;//AQ_CLEAR;
#else
		EPwm2Regs.AQCTLA.bit.CAU = AQ_SET; // set actions for EPWM1A
		EPwm2Regs.AQCTLA.bit.CAD = AQ_CLEAR;
#endif
		EPwm2Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE; // enable Dead-band module
		EPwm2Regs.DBCTL.bit.POLSEL = DB_ACTV_HIC; // Active Hi complementary
		EPwm2Regs.DBFED = RED_FED2;  // TBCLK counts
		EPwm2Regs.DBRED = RED_FED2;  // TBCLK counts
		EPwm2Regs.CMPA.bit.CMPA = DutyCycle2; // PWM duty = 2*DutyCycle  TBCLK counts

		// EPWM Module 3 config
		EPwm3Regs.TBPRD = Period; // PWM Period = 2*Period TBCLK counts
		EPwm3Regs.TBPHS.bit.TBPHS = 0; // Set Phase register to zero
		EPwm3Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;// Symmetrical mode
		EPwm3Regs.TBCTL.bit.PHSEN = TB_ENABLE; // Slave module
		EPwm3Regs.TBCTL.bit.PRDLD = TB_SHADOW;
		EPwm3Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1; // TBCLK = SYSCLK
		EPwm3Regs.TBCTL.bit.CLKDIV = TB_DIV1;
		EPwm3Regs.TBCTL.bit.SYNCOSEL = TB_CTR_ZERO; // Sync down-stream module
		EPwm3Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
		EPwm3Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
		EPwm3Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO; // load on CTR=Zero
		EPwm3Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO; // load on CTR=Zero
#if IDDK_MODULE == 0 && BOOSTXL == 1
		EPwm3Regs.AQCTLA.bit.CAU = AQ_CLEAR;//AQ_SET; // set actions for EPWM1A
		EPwm3Regs.AQCTLA.bit.CAD = AQ_SET;//AQ_CLEAR;
#else
		EPwm3Regs.AQCTLA.bit.CAU = AQ_SET; // set actions for EPWM1A
		EPwm3Regs.AQCTLA.bit.CAD = AQ_CLEAR;
#endif
		EPwm3Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE; // enable Dead-band module
		EPwm3Regs.DBCTL.bit.POLSEL = DB_ACTV_HIC; // Active Hi complementary
		EPwm3Regs.DBFED = RED_FED; // TBCLK counts
		EPwm3Regs.DBRED = RED_FED; // TBCLK counts
		EPwm3Regs.CMPA.bit.CMPA = DutyCycle2; // PWM duty = 2*DutyCycle  TBCLK counts*/


		//EPWM Module 10 config



		EPwm10Regs.TBPRD = Period2; // PWM Period = 2*Period TBCLK counts
		EPwm10Regs.TBPHS.bit.TBPHS = 0; // Set Phase register to zero
		EPwm10Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;// Symmetrical mode
		EPwm10Regs.TBCTL.bit.PHSEN = TB_DISABLE; // Master module
		EPwm10Regs.TBCTL.bit.PRDLD = TB_SHADOW;
		EPwm10Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1; // TBCLK = SYSCLK
		EPwm10Regs.TBCTL.bit.CLKDIV = TB_DIV1;
		EPwm10Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_IN; // sync flow-through
		EPwm10Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
		EPwm10Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
		EPwm10Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO; // load on CTR=Zero
		EPwm10Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO; // load on CTR=Zero
#if IDDK_MODULE == 0 && BOOSTXL == 1
		EPwm10Regs.AQCTLA.bit.CAU = AQ_CLEAR;//AQ_SET; // set actions for EPWM1A
		EPwm10Regs.AQCTLA.bit.CAD = AQ_SET;//AQ_CLEAR;
#else
		EPwm10Regs.AQCTLA.bit.CAU = AQ_SET; // set actions for EPWM1A
		EPwm10Regs.AQCTLA.bit.CAD = AQ_CLEAR;
#endif
		EPwm10Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE; // enable Dead-band module
		EPwm10Regs.DBCTL.bit.POLSEL = DB_ACTV_HIC; // Active Hi complementary
		EPwm10Regs.DBFED = RED_FED2;  // TBCLK counts
		EPwm10Regs.DBRED = RED_FED2;  // TBCLK counts

		EPwm10Regs.ETSEL.bit.SOCAEN	= 1;	        // Disable SOC on A group
		EPwm10Regs.ETSEL.bit.SOCASEL	= 1;	        // Select SOC on TBCTR = 0
		EPwm10Regs.ETPS.bit.SOCAPRD = 1;		        // Generate pulse on 1st event
		EPwm10Regs.ETSEL.bit.INTEN = 1;     			// Enable ePWM Interrupt
#if IDDK_MODULE == 0 && BOOSTXL == 0
		EPwm10Regs.ETSEL.bit.INTSEL = 1;				// Select ePWM Interrupt on TBCTR = 0
#else
		EPwm10Regs.ETSEL.bit.INTSEL = 2;				// Select ePWM Interrupt on TBCTR = TBPRD
#endif

		EPwm10Regs.ETPS.bit.INTPRD=1;
		EPwm10Regs.ETFLG.all=0;
		EPwm10Regs.ETCLR.all=0;
		EPwm10Regs.ETFRC.all=0;




		EPwm10Regs.CMPA.bit.CMPA = DutyCycle2; // PWM duty = 2*DutyCycle  TBCLK counts


		//EPWM Module 11 config


		EPwm11Regs.TBPRD = Period2; // PWM Period = 2*Period TBCLK counts
		EPwm11Regs.TBPHS.bit.TBPHS = 0; // Set Phase register to zero
		EPwm11Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;// Symmetrical mode
		EPwm11Regs.TBCTL.bit.PHSEN = TB_ENABLE; // Slave module
		EPwm11Regs.TBCTL.bit.PRDLD = TB_SHADOW;
		EPwm11Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1; // TBCLK = SYSCLK
		EPwm11Regs.TBCTL.bit.CLKDIV = TB_DIV1;
		EPwm11Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_IN; // sync flow-through
		EPwm11Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
		EPwm11Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
		EPwm11Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO; // load on CTR=Zero
		EPwm11Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO; // load on CTR=Zero
#if IDDK_MODULE == 0 && BOOSTXL == 1
		EPwm11Regs.AQCTLA.bit.CAU = AQ_CLEAR;//AQ_SET; // set actions for EPWM1A
		EPwm11Regs.AQCTLA.bit.CAD = AQ_SET;//AQ_CLEAR;
#else
		EPwm11Regs.AQCTLA.bit.CAU = AQ_SET; // set actions for EPWM1A
		EPwm11Regs.AQCTLA.bit.CAD = AQ_CLEAR;
#endif
		EPwm11Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE; // enable Dead-band module
		EPwm11Regs.DBCTL.bit.POLSEL = DB_ACTV_HIC; // Active Hi complementary
		EPwm11Regs.DBFED = RED_FED2;  // TBCLK counts
		EPwm11Regs.DBRED = RED_FED2;  // TBCLK counts
		EPwm11Regs.CMPA.bit.CMPA = DutyCycle2; // PWM duty = 2*DutyCycle  TBCLK counts


		//EPWM Module 12 config



		EPwm12Regs.TBPRD = Period2; // PWM Period = 2*Period TBCLK counts
		EPwm12Regs.TBPHS.bit.TBPHS = 0; // Set Phase register to zero
		EPwm12Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;// Symmetrical mode
		EPwm12Regs.TBCTL.bit.PHSEN = TB_ENABLE; // Slave module
		EPwm12Regs.TBCTL.bit.PRDLD = TB_SHADOW;
		EPwm12Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1; // TBCLK = SYSCLK
		EPwm12Regs.TBCTL.bit.CLKDIV = TB_DIV1;
		EPwm12Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_IN; // sync flow-through
		EPwm12Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
		EPwm12Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
		EPwm12Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO; // load on CTR=Zero
		EPwm12Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO; // load on CTR=Zero
#if IDDK_MODULE == 0 && BOOSTXL == 1
		EPwm12Regs.AQCTLA.bit.CAU = AQ_CLEAR;//AQ_SET; // set actions for EPWM1A
		EPwm12Regs.AQCTLA.bit.CAD = AQ_SET;//AQ_CLEAR;
#else
		EPwm12Regs.AQCTLA.bit.CAU = AQ_SET; // set actions for EPWM1A
		EPwm12Regs.AQCTLA.bit.CAD = AQ_CLEAR;
#endif
		EPwm12Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE; // enable Dead-band module
		EPwm12Regs.DBCTL.bit.POLSEL = DB_ACTV_HIC; // Active Hi complementary
		EPwm12Regs.DBFED = RED_FED2;  // TBCLK counts
		EPwm12Regs.DBRED = RED_FED2;  // TBCLK counts
		EPwm12Regs.CMPA.bit.CMPA = DutyCycle2; // PWM duty = 2*DutyCycle  TBCLK counts
	}


	return ErrCode;
}

/*
** ===================================================================
**     Function   :  PWM_Stop
**
**     Description :
**					Stop ePWM1 and ePWM2 modules pulses.
**
**     Parameters  :
**					none
**     Returns     :
**					none
**     Notes       :
**
**
** ===================================================================
*/
#pragma CODE_SECTION(PwmStopDriver, "ramfuncs")
void PwmStopDriver( void )
{
		EPwm1Regs.TBPRD = 0;
		EPwm2Regs.TBPRD = 0;
		EPwm3Regs.TBPRD = 0;
}


/*
** ===================================================================
**     Function   :  PwmSetDriver
**
**     Description :
**					Stop ePWM1  ePWM2 and ePWM3 modules pulses.
**
**     Parameters  :
**					none
**     Returns     :
**					none
**     Notes       :
**
**
** ===================================================================
*/
#pragma CODE_SECTION(PwmSetDriver, "ramfuncs")
void PwmSetDriver( int16_t *AphaseCommand, int16_t *BphaseCommand, int16_t *CphaseCommand, uint16_t Axis )
{
	//SET_PWM1A = PWM_DC + AphaseCommand;
	//SET_PWM2A = PWM_DC + BphaseCommand;
	//SET_PWM3A = PWM_DC + CphaseCommand;
	//BRLS_PWMA = AphaseCommand;
	//BRLS_PWMB = BphaseCommand;
	//BRLS_PWMC = CphaseCommand;
	if(Axis == X_Axis)
	{
		EPwm1Regs.CMPA.bit.CMPA = PWM_DC + *AphaseCommand;
		EPwm2Regs.CMPA.bit.CMPA = PWM_DC + *BphaseCommand;
		EPwm3Regs.CMPA.bit.CMPA = PWM_DC + *CphaseCommand;
	}
	if(Axis == Y_Axis)
{
		EPwm10Regs.CMPA.bit.CMPA = PWM_DC + *AphaseCommand;
		EPwm11Regs.CMPA.bit.CMPA = PWM_DC + *BphaseCommand;
		EPwm12Regs.CMPA.bit.CMPA = PWM_DC + *CphaseCommand;
}


}


/*
** ===================================================================
**     Function   :  SetOpenLoopPwm
**
**     Description :
**					Set open loop PWM
**
**     Parameters  :
**					none
**     Returns     :
**					none
**     Notes       :
**
**
** ===================================================================
*/
void SetOpenLoopPwm(int16_t OpenLoopPwm, int16_t *Pwm)
{
		*Pwm = OpenLoopPwm;
}

//===========================================================================
// End of file.
//==========================

/*******************************************************************************
 * File name: RWLK_Timer_Driver.h
 *
 * Description: CPU Timer dirvers header file.
 *              Developed for TI F28377D.
 *
 * Note: It includes self test which demonstrate Timers operation and preform self test.
 *
 * Author:	Yehuda Bitton
 * Date:    21/01/2016
 *
 ******************************************************************************/

/*******************************************************************************
  Multiple include protection
 ******************************************************************************/
#ifndef RWLK_TIMER_DRIVER_H
#define RWLK_TIMER_DRIVER_H

/*******************************************************************************
 *  Include
 ******************************************************************************/
#include "RWLK.h"
//#include "F28x_Project.h"     // Device Header file and Examples Include File
//#include "inc/hw_types.h"
/*******************************************************************************
 *  Defines
 ******************************************************************************/
#define  TIMER_0 										0
#define  TIMER_1										1
#define  TIMER_2										2

//#define ERR_OK 											0
#define ERR_RANGE_TIMER_NUMBER 							2
#define ERR_RANGE_TIMER_PERIOD 							3

/*------------------------------------------------------------------------------
  Configurations
------------------------------------------------------------------------------*/

/*******************************************************************************
 *  Data types
 ******************************************************************************/


/*******************************************************************************
 *  Constants and Macros
 ******************************************************************************/

/*******************************************************************************
 *  Interface function deceleration:
 ******************************************************************************/
void D_InitCpuTimers(void);
uint8_t D_Timer_Start ( uint8_t TimerNo, uint32_t Us );
uint8_t D_Timer_Stop ( uint8_t TimerNo );
uint8_t D_Timer_EnableInterrupt ( uint8_t TimerNo );
uint8_t D_Timer_DisableInterrupt ( uint8_t TimerNo );
uint8_t D_Timer_Get_Overflow_Flag ( uint8_t TimerNo, uint8_t *EventFlag );
uint8_t D_Timer_Clear_Overflow_Flag ( uint8_t TimerNo );
uint8_t D_Timer_Is_Stop ( uint8_t TimerNo );

void D_Timer_Config_mSec_Timer(void);
void D_Timer_Config_uSec_Timer(void);
uint32_t D_Timer_Get_mSec_Counter ( void );
uint32_t D_Timer_Get_uSec_Counter ( void );


/*******************************************************************************
 *  End of file
 ******************************************************************************/
#endif

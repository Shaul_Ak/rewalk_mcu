/************************************************************/

/**************************************************************/
//defines
#include <string.h>
#include <stdbool.h>
#include "F28x_Project.h"     // Device Headerfile and Examples Include File
#include "RWLK_Timer_Driver.h"
#include "gyro_detection.h"

//SCI baud rates
#define BAUD_RATE_9600    9600   //Default low baud rate
#define BAUD_RATE_115200  115200 //High baud rate
#define BAUD_RATE_460800  460800 //Highest baud rate
#define BAUD_RATE_230400  230400
#define BAUD_RATE_345600  345600
//calculate desired  sci baud rate on the given CPU clock
#define CPU_FREQ    200E6          //200MHZ
#define LSPCLK_FREQ CPU_FREQ/4     //LSPCLK is fourth of CPU clock as default
#define MAX_SCI_RECV_BYTES_COUNT 150

//SCI A and C RWALK GPIOs
#define RWLK_SCIARXD_GPIO     64
#define RWLK_SCIATXD_GPIO     65
#define RWLK_SCICRXD_GPIO     139
#define RWLK_SCICTXD_GPIO     140
#define TIME_TEST_PIN_GPIO103  103 //temporarily define should be removed

//Service LEDS
#define  RED_LED     152
#define  GREEN_LED   151
#define  ORANGE_LED  153
#define  LED_ON   0
#define  LED_OFF  1
// Prototype statements for functions found within this file.
//SCI-A
interrupt void sciaTxFifoIsr(void);
interrupt void sciaRxFifoIsr(void);
interrupt void scicTxFifoIsr(void);
interrupt void scicRxFifoIsr(void);


//SCI FIFO depth
#define FIFO_DEPTH_1           1   //Minimum depth of fifo to enable correct communication with Bluetooth module while commands or configuration
#define FIFO_DEPTH_16         16  //Max depth of fifo
#define MAX_FIFO_DEPTH_16     16  //Max depth of fifo



//service function to toggle board LEDS
void GPIO_TogglePin(Uint16 pin);
void GPIO_LEDs_Config(void);
void LEDs_ON(void);
void LEDs_OFF(void);
void Toggle_LEDs(void);
void GPIO_Test_TimeScope_Config(void);
void sci_a_init(Uint16 TX_fifo_depth , Uint16 RX_fifo_depth , Uint32 sci_baud_rate);
void sci_c_init(Uint16 TX_fifo_depth , Uint16 RX_fifo_depth , Uint32 sci_baud_rate);
void sci_a_change_fifo_depth(Uint16 TX_fifo_depth ,Uint16 RX_fifo_depth);
void sci_c_change_fifo_depth(Uint16 TX_fifo_depth ,Uint16 RX_fifo_depth);
void scia_msg_melody_com_config(const char * msg);
void scia_msg_melody_data(const char * msg);
void scic_msg(char * msg);
void Set_GUI_Blue_New_Data_Recv_Flag(_Bool  flag);
_Bool   Get_GUI_Blue_New_Data_Recv_Flag(void);
const char *Init_Blue_Recv_Data_Memory(void);
void Reset_SCIA_Recv_Byte_Index(void);




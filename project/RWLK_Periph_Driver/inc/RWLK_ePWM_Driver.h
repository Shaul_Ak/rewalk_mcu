/*
 * RWLK_ePWM_Driver.h
 *
 *  Created on: Jan 27, 2016
 *      Author: arie
 */

#ifndef RWLK_EPWM_DRIVER_H_
#define RWLK_EPWM_DRIVER_H_

#include "RWLK.h"



#define ePWM_Enable			 1
#define MAX_ePWM_NUM		12
#define MIN_ePWM_NUM		 1
#define ERR_ePWM_NUM		 1

#define ePWM1BlockNum        1
#define ePWM2BlockNum        2
#define ePWM3BlockNum        3
#define ePWM4BlockNum        4
#define ePWM5BlockNum      	 5
#define ePWM6BlockNum      	 6
#define ePWM7BlockNum      	 7
#define ePWM8BlockNum     	 8
#define ePWM9BlockNum        9
#define ePWM10BlockNum      10
#define ePWM11BlockNum      11
#define ePWM12BlockNum      12

/*==============================================================================
                               Functions declaration
==============================================================================*/
/*==============================================================================
Function : EPWM_IO_Init

Description : This function initialize EPWM BLOCKS

ePWM_Block 		: EPWM number - Options (block#1 to block#12)
============================================================================*/
uint16_t EPWM_IO_Init(uint32_t ePWM_Block);

/*
** ===================================================================
**     Function   :  PwmInitDriver
**
**     Description :
**					Start ePWM1 and ePWM2 modules pulses using TB and DB submodules.
**                  DB operates in the AHC mode
**     Parameters  :
**					u16 Periode   - the PWM period(us/100 units);
**                  u16 DutyCycle - the Duty (Low level)cycle time (us/100 units);
**					u16 RED_FED   - the RED(FED) time (us/100 units);
**     Returns     :
**					Error code. Possible values:
**                      - ERR_OK,
**                      - ERR_PWM_PERIOD_RANGE, (Period=0),
**						- ERR_PWM_DUTY_RANGE, (DutyCycle = 0 or DutyCycle not less then Period)
**                      - ERR_PWM_RED_FED_RANGE, (RED_FED < 1 or RED_FED > 1023)
**						- ERR_PWM_RED_GT_DUTY, (RED_FED not less then 2*DutyCycle)
**						- ERR_PWM_FED_GT_DUTY, (RED_FED not less then 2*(Period - Duty cycle)
**     Notes       :
**					1. The document 'Argo PWM waveform.doc' represents the PWM wafeform
**                  2. RED alwas equals to FED in our case
**
** ===================================================================
*/
uint16_t   PwmInitDriver( uint16_t Period, uint16_t DutyCycle, uint16_t RED_FED );

/*
** ===================================================================
**     Function   :  PwmStopDriver
**
**     Description :
**					Stop ePWM1  ePWM2 and ePWM3 modules pulses.
**
**     Parameters  :
**					none
**     Returns     :
**					none
**     Notes       :
**
**
** ===================================================================
*/
void PwmStopDriver( void );

/*
** ===================================================================
**     Function   :  PwmSetDriver
**
**     Description :
**					Stop ePWM1  ePWM2 and ePWM3 modules pulses.
**
**     Parameters  :
**					none
**     Returns     :
**					none
**     Notes       :
**
**
** ===================================================================
*/
void PwmSetDriver( int16_t *AphaseCommand, int16_t *BphaseCommand, int16_t *CphaseCommand, uint16_t Axis);


/*
** ===================================================================
**     Function   :  SetOpenLoopPwm
**
**     Description :
**					Set pwm duty cycle for open loop.
**
**     Parameters  :
**					OpenLoopPwm - Duty Cycle for open loop
**     Returns     :
**					none
**     Notes       :
**
**
** ===================================================================
** */
void SetOpenLoopPwm(int16_t OpenLoopPwm, int16_t *Pwm);

#endif /* RWLK_EPWM_DRIVER_H_ */

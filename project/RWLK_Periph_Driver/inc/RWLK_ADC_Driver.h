/*
 * RWLK_ADC_Driver.h
 *
 *  Created on: Feb 21, 2016
 *      Author: arie
 */

#ifndef RWLK_ADC_DRIVER_H_
#define RWLK_ADC_DRIVER_H_
//#include "RWLK.h"
//#include "inc/hw_types.h"
#include "RWLK_Motor_Control.h"

/*==============================================================================
                               Macro definitions
==============================================================================*/

// ADC value constants for available values check (ADC_x_MIN <= ADCval <= ADC_x_MAX)
#define ADC_3_3V_MIN			2816  // 3.05V
#define ADC_3_3V_MAX			3332  // 3.55V
#define ADC_12V_MIN				2487  // 10.1V
#define ADC_12V_MAX				3446  // 14V
#define ADC_5V_MIN				3072  // 4.5V
#define ADC_5V_MAX				3754  // 5.5V
#define ADC_1_8V_MIN			2322  // 1.71V
#define ADC_1_8V_MAX			2594  // 1.9V
#define ADC_28V_MIN				1536  // 18V
#define ADC_VREF_MIN			2663  // 1.95V
#define ADC_VREF_MAX			2935  // 2.15V



// Coefficients for conversion from row data into matric units (volts or �C)
#define ADC_V_3_3V_COEFF        (3.0*32.0/22.0/4096.0)  // Formula: Vvolt = ADC_V_3_3V_COEFF*(float)ADCval
#define ADC_V_12V_COEFF         (3.0*12.2/2.2/4096.0)   // Formula: Vvolt = ADC_V_12V_COEFF*(float)ADCval
#define ADC_V_5V_COEFF          (3.0*20.0/10.0/4096.0)  // Formula: Vvolt = ADC_V_5V_COEFF*(float)ADCval
#define ADC_V_1_8V_COEFF        (3.0/4096.0)            // Formula: Vvolt = ADC_V_1_8V_COEFF*(float)ADCval
#define ADC_V_REF_COEFF         (3.0/4096.0)            // Formula: Vvolt = ADC_V_REF_COEFF*(float)ADCval
#define ADC_V_28V_COEFF         (3.0*16.0/1.0/4096.0)   // Formula: Vvolt = ADC_V_28V_COEFF*(float)ADCval
#define ADC_FSR_COEFF           (3.0/4096.0)            // Formula: Vvolt = ADC_FSR_COEFF*(float)ADCval
#define ADC_TEMPER_MULT_COEFF	(300.0/4096.0)          // Formula:
#define ADC_TEMPER_ADD_COEFF	(-50)                  //  Temperature_�C = ADC_TEMPER_MULT_COEFF*(float)ADCval+ADC_TEMPER_ADD_COEFF



extern int16_t CalibratedChannelZero;

//***********Last Added 11/10/2016******************
 typedef struct _DIGITAL_TO_ANALOG_SENSOR
 {
	float CurrentSensorResolution;
	float DigitalToAnalogOutputResolution;
	float RelativeResolutionsCoefficient;
	float PositionResolutionCoefficient;
	float PositionErrorResolutionCoefficient;
	int32_t HalfOfDigitalToAnalogResolution;
	int32_t OneAmpCommandResolution;
 }sDigitalToAnalogStruct;
//**************************************************
 typedef struct _MOTOR_PHASES_DATA
 		{
 		uint8_t PhaseA_DataLow;
 		uint8_t PhaseA_DataHigh;
 		uint8_t PhaseB_DataLow;
 		uint8_t PhaseB_DataHigh;
 		uint8_t PhaseC_DataLow;
 		uint8_t PhaseC_DataHigh;
 		}sMotorPhasesCurrents;

extern struct _MOTOR_PHASES_DATA sMotorPhasesCurrentsSend;

extern struct _DIGITAL_TO_ANALOG_SENSOR XsDigitalToAnalogCPUSensorStruct;
extern struct _DIGITAL_TO_ANALOG_SENSOR YsDigitalToAnalogCPUSensorStruct;
extern struct _DIGITAL_TO_ANALOG_SENSOR sDigitalToAnalogSensorStruct;
extern struct _DIGITAL_TO_ANALOG_SENSOR XsDigitalToAnalogSensorStruct;
extern struct _DIGITAL_TO_ANALOG_SENSOR YsDigitalToAnalogSensorStruct;


/*==============================================================================
                               Functions declaration
==============================================================================*/
uint16_t ADC_Read ( uint16_t ADC_Block, uint8_t ChNo, uint16_t *Result);
/*
** ===================================================================
**     Function   :  ADC_read
**
**     Description :
**                                ADC conversion for requested channel
**     Parameters  :
**                                u16 chNo - channel number. Possible values:
**                                     - ADC_CH_MOTOR_CURRENT,
**                                     - ADC_CH_FS1_V,
**                                     - ADC_CH_FS2_V,
**                                     - ADC_CH_FS3_V,
**                                     - ADC_CH_3_3V_SAMPLE,
**                                     - ADC_CH_12V_SAMPLE,
**                                     - ADC_CH_TEMPEARTURE,
**                                     - ADC_CH_VREF,
**                                     - ADC_CH_5V_SAMPLE,
**                                     - ADC_CH_MUX_3_3V,
**                                     - ADC_CH_1_8V_SAMPLE,
**                                     - ADC_CH_28V;
**								  u16 *Result - the conversion result.
**     Returns     :
**                                error code. Possible values:
**                                     - ERR_OK - conversion successfully finished;
**                                     - ERR_ADC_NO_CHANNEL_DOES_NOT_MATCH - illegal the chNo parameter value;
**                                     - ERR_ADC_ACQ_PULSE_NOT_MATCH - illegal the acquisitionPulseTime parameter value;
**                                     - ERR_ADC_CONVERSION_FAILS - conversion timeout detected.
**
**     Notes       :
**
** ===================================================================
*/

void MotorPhasesCurrentParser(volatile cControlVar *MotorPhasesCurrent, struct _MOTOR_PHASES_DATA *TxPhasesCurrents, uint16_t AxisNumber);
void ConfigureADC(void);
void SetupADCSoftware(uint16_t InterruptSorce);
int16_t CalibrateADCSoftware(int16_t Channel);
//===========================================================================
// No more.
//===========================================================================












#endif /* RWLK_ADC_DRIVER_H_ */

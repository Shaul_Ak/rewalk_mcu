
/*==============================================================================
                               RWLK_CAN_Driver.h
==============================================================================*/

#ifndef RWLK_CAN_DRIVER_H_
#define RWLK_CAN_DRIVER_H_

//#include "inc/hw_types.h"
#include "RWLK.h"
#include "../driverlib/can.h"
//#include "RWLK_CAN_Application.h"
/*==============================================================================
                               CANBUS Error ID Defines
==============================================================================*/

#define ERR_CAN_TSEQ1_RANGE			           	0x60 // TSEQ1=0 or TSEQ1>16
#define ERR_CAN_TSEQ2_RANGE         		  	0x61 // TSEQ2=0 or TSEQ2>8
#define ERR_CAN_TSEQ2_DOES_NOT_MATCH_BPR	  	0x62 // TSEQ2 < 3/BPR
#define ERR_CAN_FREQ_RANGE              	  	0x63 // Frequency == 0
#define ERR_CAN_CANBUS_CORRUPTED        	  	0x64 // Can bus operation does not finished or received data not allowed
#define ERR_CAN_RANGE_MAIL_BOX_NO        	  	0x65
#define ERR_CAN_RANGE_MESS_ID	        	  	0x66
#define ERR_CAN_RANGE_MESS_LENGTH               0x67
#define ERR_CAN_MESSAGE_NOT_PENDING        	  	0x68
#define ERR_CAN_MESSAGE_PENDING         	  	0x69
#define ERR_CAN_RANGE_MASK_ID                   0x6A
#define ERR_CAN_TIMEOUT_INVOKED                 0x6B
#define ERR_CAN_BASE_ADDRESS					0x6C


/*==============================================================================
                               CANBUS CLK Defines
==============================================================================*/

#define CAN_BUS_SPEED_1MHZ      1000000
#define CAN_BUS_SPEED_500KHZ    500000

/*==============================================================================
                               CAN OBJECT TYPE
==============================================================================*/

#define	CAN_OBJECT_Tx_TYPE				0
#define	CAN_OBJECT_Rx_TYPE				1
#define	CAN_OBJECT_REMOTE_Tx_TYPE		2
#define	CAN_OBJECT_REMOTE_Rx_TYPE		3
#define	CAN_OBJECT_REMOTE_TxRx_TYPE		4
/*==============================================================================
                               Local constants
==============================================================================*/


//#define CANBUS_Enable	1
#if IDDK_MODULE == 0 && BOOSTXL == 0
#define CANBUS_Enable	6
#endif

#if IDDK_MODULE == 1 && BOOSTXL == 0
#define CANBUS_Enable	1
#endif

#if IDDK_MODULE == 0 && BOOSTXL == 1
#define CANBUS_Enable	1
#endif


#define TSEQ1_MIN    	1
#define TSEQ1_MAX    	16
#define TSEQ2_MIN    	1
#define TSEQ2_MAX    	8
#define CANBUS_TIMEOUT_MSEC 1000    // ms

#define CAN_MAX_MAILBOX_NUMBER  31
#define CAN_MAX_STANDARD_ID     ((1L<<11)-1)
#define CAN_MAX_DATA_LENDTH     8
/*==============================================================================
                               	   MailBox Defines
==============================================================================*/
#define CARD_MAILBOX_TX	1
#define CARD_MAILBOX_RX	2
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Shaul 18.4.2017: ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#ifdef SENSOR_MODE
#define CARD_MAILBOX_IMUR_0_RX	3
#define CARD_MAILBOX_IMUR_1_RX	4
#define CARD_MAILBOX_IMUR_2_RX	5
#define CARD_MAILBOX_IMUR_3_RX	6
#define CARD_MAILBOX_IMUR_4_RX	7
#define CARD_MAILBOX_IMUR_5_RX	8

#define CARD_MAILBOX_IMUL_0_RX	9
#define CARD_MAILBOX_IMUL_1_RX	10
#define CARD_MAILBOX_IMUL_2_RX	11
#define CARD_MAILBOX_IMUL_3_RX	12
#define CARD_MAILBOX_IMUL_4_RX	13
#define CARD_MAILBOX_IMUL_5_RX	14



#define CARD_MAILBOX_LC1_RX		15
#define CARD_MAILBOX_LC2_RX		16

#define CARD_MAILBOX_COMMAND_RX_LC		17
#define CARD_MAILBOX_COMMAND_RX_IMU_R	18
#define CARD_MAILBOX_COMMAND_RX_IMU_L	19
#define CARD_MAILBOX_COMMAND_REJECT		20
#endif  // SENSOR_MODE
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#define CARD_MAILBOX_BROADCAST_TX	21
#define CARD_MAILBOX_BROADCAST_RX	22


#define CAN_MAX_MAILBOX_NUM 33  // 32 from 1-32
/*==============================================================================
                               	   MSG ID Defines
==============================================================================*/
#define CAN_LEFT_HIP_MSG_ID		0x330
#define CAN_LEFT_KNEE_MSG_ID	0x331
#define CAN_RIGHT_HIP_MSG_ID	0x332
#define CAN_RIGHT_KNEE_MSG_ID	0x333
#define CAN_BROADCAST_MSG_ID	0x339

#ifdef SENSOR_MODE
#define CAN_IMUR_0_MSG_ID		0x141   //	IMU-R: 0x141 - 0x146
#define CAN_IMUR_1_MSG_ID		0x142
#define CAN_IMUR_2_MSG_ID		0x143
#define CAN_IMUR_3_MSG_ID		0x144
#define CAN_IMUR_4_MSG_ID		0x145
#define CAN_IMUR_5_MSG_ID		0x146

#define CAN_IMUL_0_MSG_ID		0x151   //	IMU-L: 0x151 - 0x156
#define CAN_IMUL_1_MSG_ID		0x152
#define CAN_IMUL_2_MSG_ID		0x153
#define CAN_IMUL_3_MSG_ID		0x154
#define CAN_IMUL_4_MSG_ID		0x155
#define CAN_IMUL_5_MSG_ID		0x156

#define CAN_SENSOR_COMMAND_TX	0x500	//	Sensor command 4-MSB of the ID : 0x5
#define CAN_COMMAND_RX_LC1_2 	0x604	//	Response to Sensor command 4-MSB of the ID : 0x6
#define CAN_COMMAND_RX_IMU_R 	0x609
#define CAN_COMMAND_RX_IMU_L	0x60A
#define CAN_COMMAND_FILTER_LC   0x70C
#define CAN_COMMAND_FILTER_IMU  0x70F
#define CAN_COMMAND_FILTER_REJ  0x700


#define CAN_LC1_MSG_ID			0x410	//	LC1: 0x410
#define CAN_LC2_MSG_ID			0x710	//	LC2: 0x710
#endif  // SENSOR_MODE
/*==============================================================================
                               	   CANBUS Variables
==============================================================================*/

/*tCANMsgObject sTXCANMessage;	// Transmit message object
tCANMsgObject sRXCANMessage;	// Receive message object
tMsgObjType	  sCANMessageType;	// Message type

unsigned char ucTXMsgData[8] = {0,0,0,0,0,0,0,0};  // Transmit data array (8 byte)
unsigned char ucRXMsgData[8] = {0,0,0,0,0,0,0,0};	// Receive data array (8 byte)
*/
/*==============================================================================
                               CANBUS Functions Prototypes
==============================================================================*/

extern uint16_t CAN_Init(uint32_t CAN_Base,uint32_t CPU_Frequency,uint32_t CAN_Frequency);
extern void InitMailBoxRegs( uint32_t CAN_Base, uint16_t MailBox, uint32_t Msg_ID, uint32_t Mask, uint32_t Flags, uint32_t MailBoxFunction);
extern void InitMsgObj(uint32_t MsgID, uint32_t MsgIDMask, uint32_t SetMsgFlags, uint32_t MsgLengs, uint32_t TxRx,uint16_t MailBox);
extern void ClearMessageObject(uint32_t MailBoxFunction);
extern void RWLK_CanTransmitDriver(uint32_t CAN_Base, uint16_t MailBox, tCANMsgObject *pMsgObject);
extern void RWLK_CanReceiveDriver(uint32_t CAN_Base, uint16_t MailBox, tCANMsgObject *pMsgObject, bool bClrPendingInt);
extern bool RWLK_IsCanTransmit(uint32_t CAN_Base);
#endif /* RWLK_CAN_DRIVER_H_ */

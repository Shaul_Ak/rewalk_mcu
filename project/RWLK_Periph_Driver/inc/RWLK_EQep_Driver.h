/*
 * RWLK_EQep_Driver.h
 *
 *  Created on: Jan 13, 2016
 *
 */

#ifndef RWLK_EQEP_DRIVER_H_
#define RWLK_EQEP_DRIVER_H_

#include "RWLK.h"

#define QEep1BlockEnable		1
#define QEep2BlockEnable	5
#define QEep3BlockEnable	5

#define ERR_QE_BLOCK_NUMBER  0x6C

#define MAX_EQepBLOCK_NUM  12


#define QEep1BlockNum        1
#define QEep2BlockNum        2
#define QEep3BlockNum        3


 /*==============================================================================
                                	   CANBUS Structers
 ==============================================================================*/


uint16_t EQep_Init(uint32_t EQepBlockNumber, uint32_t EncMaxValue, uint32_t EncInitValue);
/*==============================================================================
                               Functions declaration
==============================================================================*/

uint8_t IncEncoder_Init( uint32_t MaxValue, uint32_t InitValue );
/*
** ===================================================================
**     Function   :  IncEncoder_Init
**
**     Description :
**					The incremental encoder initialization
**                  for the simple quadrature counter mode
**     Parameters  :
**					u32 MaxValue -  the maximun encoder counter value.
**                                  Shall be not euals to 0
**					u32 InitValue - the initial encoder counter value.
**                                  Shall not be grater then MaxValue
**     Returns     :
**					Error code. Possible values:
**                      - ERR_OK,
**                      - ERR_RANGE_INCR_ENCODER_MAX_VAL,
**                      - ERR_INCR_ENCODER_INIT_VAL. (InitValue>MaxValue)
**     Notes       :
**
** ===================================================================
*/

uint8_t IncEncoder_GetPosition( int32_t *Position );
/*
** ===================================================================
**     Function   :  IncEncoder_GetPosition
**
**     Description :
**					Obtain the incremental encoder current pozition
**     Parameters  :
**					u32 *Position - pointer to the current position
**     Returns     :
**					Error code. Possible values:
**                      - ERR_OK,
**                      - ERR_INCR_ENCODER_POS_COUNTER.
**     Notes       :
**
** ===================================================================
*/





uint8_t IncEncoder_ClrPosition( void );
/*
** ===================================================================
**     Function   :  IncEncoder_ClrPosition
**
**     Description :
**					Set the incremental encoder pozition value equals zero
**     Parameters  :
**					none
**     Returns     :
**					Error code. Possible values:
**                      - ERR_OK,
**                      - ERR_INCR_ENCODER_POS_COUNTER.
**     Notes       :
**
** ===================================================================
*/

uint16_t IncEncoder_DirectionRead( uint16_t *Direction );
/*
** ===================================================================
**     Function   :  IncEncoder_DirectionRead
**
**     Description :
**					Obtain the incremental encoder current direction
**     Parameters  :
**					u8 *Direction - pointer to the current direction.
**                      Direction can be one of the following:
**                      - INC_ENCODER_ANTICLOCKWISE_ROTATION \ INC_ENCODER_REVERSE_MOVEMENT,
**                      - INC_ENCODER_CLOCKWISE_ROTATION \ INC_ENCODER_FORWARD_MOVEMENT,
**                      - INC_ENCODER_DIRECTION_FALSE.
**     Returns     :
**					Error code. Possible values:
**                      - ERR_OK,
**                      - ERR_INCR_ENCODER_POS_COUNTER.
**     Notes       :
**
** ===================================================================
*/



#endif /* RWLK_EQEP_DRIVER_H_ */

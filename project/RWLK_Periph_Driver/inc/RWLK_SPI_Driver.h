/*==============================================================================
                               RWLK_SPI_Driver.h
==============================================================================*/

#ifndef RWLK_SPI_DRIVER_H_
#define RWLK_SPI_DRIVER_H_

#include "RWLK.h"


/*==============================================================================
                               SPI Defines
==============================================================================*/
#define SPIA_ENABLE 			1
#if BOOSTXL == 0
#define SPIB_ENABLE 			6
#else
#define SPIB_ENABLE 			15
#endif
#define SPI_BITRATE_128		0x7F 			//SPI Baud Rate = LSPCLK/128 Default SYSCLK/4 -> 50MHz(LOSPCP Register) --> Buad rate =  50MHz/128 400Khz
#define IMU_SPI_BITRATE		0x32 			//SPI Baud Rate = LSPCLK/50 Default SYSCLK/4 -> 50MHz(LOSPCP Register) --> Buad rate =  50MHz/50 1Mhz
#define BITS_16				16
#define BITS_8				8

#define DISABLE_CS_AFTER_SEND									1
#define NOT_DISABLE_CS_AFTER_SEND								0


/*******************************************************************************
*  Data types
******************************************************************************/
typedef enum
{

	SPI_A_IMU_ID,
	SPI_A_MOTOR_ID,
	SPI_B_ID,
	SPI_C_ID,
	SPI_NUM
} SPI_ID_Enum_Type;

/*==============================================================================
                               SPI Functions Prototypes
==============================================================================*/

extern void SPI_Gpio_Init(uint32_t SPI_Base);
extern void SPI_Init(uint32_t SPI_Base , uint32_t SPI_Length , uint32_t SPI_Bit_Rate );
extern void SPI_Init_Registers(uint32_t SPI_Base , uint32_t SPI_Length , uint32_t SPI_Bit_Rate);
extern void SPI_Transmit(uint16_t TxData);

//YehudaB
uint8_t SPI_Receive(uint32_t SpiId, uint8_t *pData, uint8_t Length);
uint8_t SPI_Send(uint32_t SpiId, uint8_t *pData, uint8_t Length, uint8_t DisableCS_AfterSend);
uint8_t SPI_Tranceive(uint32_t SpiId, uint8_t *pOutData, uint8_t OutLength, uint8_t *pInData, uint8_t InLength);


#endif /* RWLK_SPI_DRIVER_H_ */

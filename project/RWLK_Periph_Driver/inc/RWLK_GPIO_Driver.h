/*==============================================================================
                              RWLK_GPIO_Driver.h
==============================================================================*/

#ifndef RWLK_PERIPH_DRIVER_INC_RWLK_GPIO_DRIVER_H_
#define RWLK_PERIPH_DRIVER_INC_RWLK_GPIO_DRIVER_H_

/*==============================================================================
                               CANBUS GPIO Defines
==============================================================================*/

#if IDDK_MODULE == 0 && BOOSTXL == 0
#define CANBUS_CHA_Rx	36
#define CANBUS_CHA_Tx	37
#endif

#if IDDK_MODULE == 1 && BOOSTXL == 0
#define CANBUS_CHA_Rx	30
#define CANBUS_CHA_Tx	31
#endif

#if IDDK_MODULE == 0 && BOOSTXL == 1
#define CANBUS_CHA_Rx	30
#define CANBUS_CHA_Tx	31
#endif
/*==============================================================================
                               SPI GPIO Defines
==============================================================================*/
	#define SPI_SIMO_A		16 // PIN 16 or 5
	#define SPI_SOMI_A		17 // PIN 17 or 3
	#define SPI_CLK_A		18 // PIN 18 Only
	#define SPI_STE_A		19 // PIN 19 Only // Generate Interupt

#if IDDK_MODULE == 0 && BOOSTXL == 0
	#define SPI_SIMO_B		24
	#define SPI_SOMI_B		25
	#define SPI_CLK_B		26
#else
	#define SPI_SIMO_B		63 //16 // PIN 16 or 5
	#define SPI_SOMI_B		64//17 // PIN 17 or 3 (SDO)
	#define SPI_CLK_B		65//18 // PIN 18 Only
#endif
	#define SPI_STE_B		66//19 // PIN 19 Only // Generate Interupt
	#define SPI_STE_IMU_A	28    // cs
	#define SPI_STE_MOTOR_A	106   // cs


	#define SPI_SIMO_C		122
	#define SPI_SOMI_C		123
	#define SPI_CLK_C		124
	#define SPI_STE_C		125




/*==============================================================================
                               EQep GPIO Defines
==============================================================================*/
//#if BOOSTXL == 0
#define EQEP1A_IO	20
#define EQEP1B_IO	21
#define EQEP1S_IO	22
#define EQEP1I_IO	23
//#else
//#define EQEP1A_IO	96
//#define EQEP1B_IO	97
//#define EQEP1S_IO	98
//#define EQEP1I_IO	99
//#endif

#if IDDK_MODULE == 0
#define EQEP2A_IO	100
#define EQEP2B_IO	101
#define EQEP2S_IO	102
#define EQEP2I_IO	103
#endif

#define EQEP3A_IO	104
#define EQEP3B_IO	105
#define EQEP3S_IO	106
#define EQEP3I_IO	107

/*==============================================================================
                               ePWM GPIO Defines
==============================================================================*/
#define EPWM1A_IO	0
#define EPWM1B_IO	1
#define EPWM2A_IO	2
#define EPWM2B_IO	3
#define EPWM3A_IO	4
#define EPWM3B_IO	5
#define EPWM10A_IO	163
#define EPWM10B_IO	164
#define EPWM11A_IO	165
#define EPWM11B_IO	166
#define EPWM12A_IO	167
#define EPWM12B_IO	168


#define GC1_OUT		156
#define GC2_OUT		157
#define GC3_OUT		158

#define GC1_OUT_SET					GpioDataRegs.GPESET.bit.GPIO156
#define GC1_OUT_CLEAR						GpioDataRegs.GPECLEAR.bit.GPIO156

#define GC2_OUT_SET					GpioDataRegs.GPESET.bit.GPIO157
#define GC2_OUT_CLEAR						GpioDataRegs.GPECLEAR.bit.GPIO157

#define GC3_OUT_SET					GpioDataRegs.GPESET.bit.GPIO158
#define GC3_OUT_CLEAR						GpioDataRegs.GPECLEAR.bit.GPIO158


#define EN_CUR_SNS	107
#define EN_CUR_SNS_SET				GpioDataRegs.GPDSET.bit.GPIO107
#define EN_CUR_SNS_CLEAR			GpioDataRegs.GPDCLEAR.bit.GPIO107


#define EN_DATA_FLAG_SNS	66
#define EN_DATA_FLAG_SNS_SET			GpioDataRegs.GPCSET.bit.GPIO66
#define EN_DATA_FLAG_SNS_CLEAR			GpioDataRegs.GPCCLEAR.bit.GPIO66

#define DATA_FLAG_SNS_TOGGLE			GpioDataRegs.GPCTOGGLE.bit.GPIO66


#define ANALOG_UNDER_VOLTAGE		128
#define ANALOG_OVER_VOLTAGE			129
#define BUS_VOLTAGE_PRESENT			130
#define DIGITAL_UNGER_VOLTAGE		131
#define DIGITAL_OVER_VOLTAGE		132
#define DIGITAL_VOLTAGE_PRESENT		133


#define	A_HALL_SENSOR	143
#define B_HALL_SENSOR	144
#define C_HALL_SENSOR	145

/*==============================================================================
                               extern functions
==============================================================================*/
extern void GC_Init(void);
extern void DefineGC_GPIO(void);
extern void GC1_Out_Reset(void);
extern void GC2_Out_Reset(void);
extern void GC3_Out_Reset(void);
extern void GC1_Out_Set(void);
extern void GC2_Out_Set(void);
extern void GC3_Out_Set(void);

extern void CurrenSensEnable_Init(void);
extern void DefineCurrenSensEnable_GPIO(void);
extern void CurrenSensEnable_Set(void);
extern void CurrenSensEnable_Reset(void);
extern void SafetyGPIO_Init(void);
extern void HallEffectMotorSensors_Init(void);
extern void DefineDataFlagSensEnable_GPIO(void);
extern void FlagSens_Out_Toggle(void);


/*==============================================================================
                               	   	   END
==============================================================================*/
#endif /* RWLK_PERIPH_DRIVER_INC_RWLK_GPIO_DRIVER_H_ */

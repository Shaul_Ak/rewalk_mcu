/*******************************************************************************
* file name: RWLK_GPIO_Test.c
*
* Description: .
*
* Note: Developed for TI F28377D.
*
* Author:  Yehuda Bitton
* Date:
*
******************************************************************************/

/*******************************************************************************
*  Includes
******************************************************************************/
#include "RWLK_Tests.h"
#include "RWLK_Timer_Driver.h"

/*******************************************************************************
 *  Defines
 ******************************************************************************/



/*******************************************************************************
*  Data types
******************************************************************************/

/*------------------------------------------------------------------------------
Variables
------------------------------------------------------------------------------*/

/*******************************************************************************
*  Constants and Macros
******************************************************************************/

/*******************************************************************************
*  Static Data deceleration
******************************************************************************/

/******************************************************************************
*  Private Functions Decleration:
******************************************************************************/

/*****************************************************************************
*  Public Function Bodies
******************************************************************************/

/*****************************************************************************
 *
 * Function name: RWLK_Leds_Test
 *
 * Description: Toggele desired led inside endless loop
 *
 * Parameters:
 * Returns:
 ******************************************************************************/
void RWLK_Leds_Test (void)
{
	uint32_t i;

	uint16_t Red_Led = 152;
	uint16_t Green_Led = 151;
	uint16_t Orange_Led = 153;


	// Configure all Leds-GPIO:
	GPIO_SetupPinMux(Red_Led, GPIO_MUX_CPU1, 0);
	GPIO_SetupPinOptions(Red_Led, GPIO_OUTPUT, GPIO_PUSHPULL);
	GPIO_SetupPinMux(Green_Led, GPIO_MUX_CPU1, 0);
	GPIO_SetupPinOptions(Green_Led, GPIO_OUTPUT, GPIO_PUSHPULL);
	GPIO_SetupPinMux(Orange_Led, GPIO_MUX_CPU1, 0);
	GPIO_SetupPinOptions(Orange_Led, GPIO_OUTPUT, GPIO_PUSHPULL);
	// Turn off leds
	GPIO_WritePin(Red_Led, 1);
	GPIO_WritePin(Green_Led, 1);
	GPIO_WritePin(Orange_Led, 1);


	while(1)
	{
		// Turn on LED
		GPIO_WritePin(Red_Led, 0);
		// Delay for a bit
		for(i=0; i<2500000; i++);
		// Turn off LED
		GPIO_WritePin(Red_Led, 1);

		// Turn on LED
		GPIO_WritePin(Green_Led, 0);
		// Delay for a bit
		for(i=0; i<2500000; i++);
		// Turn off LED
		GPIO_WritePin(Green_Led, 1);

		// Turn on LED
		GPIO_WritePin(Orange_Led, 0);
		// Delay for a bit
		for(i=0; i<2500000; i++);
		// Turn off LED
		GPIO_WritePin(Orange_Led, 1);
	 }

}

/*****************************************************************************
 *
 * Function name: RWLK_DIPswitch_Test
 *
 * Description: Toggle manualy DIP Switch and view DIPx_GPIO value using stop
 * 				button and DIP_State[0/1].
 *
 * Parameters:
 * Returns:
 ******************************************************************************/
void RWLK_DIPswitch_Test (void)
{
	uint32_t i;

	uint16_t DIP1_GPIO = 149;
	uint16_t DIP2_GPIO = 150;
	uint16_t DIP3_GPIO = 158;
	uint16_t DIP4_GPIO = 159;

	volatile uint16_t DIP_State[4] = {0,0,0,0};


	// Configure all Dip Switches:
	GPIO_SetupPinMux(DIP1_GPIO, GPIO_MUX_CPU1, 0);
	GPIO_SetupPinOptions(DIP1_GPIO, GPIO_INPUT, GPIO_PUSHPULL);
	GPIO_SetupPinMux(DIP2_GPIO, GPIO_MUX_CPU1, 0);
	GPIO_SetupPinOptions(DIP2_GPIO, GPIO_INPUT, GPIO_PUSHPULL);
	GPIO_SetupPinMux(DIP3_GPIO, GPIO_MUX_CPU1, 0);
	GPIO_SetupPinOptions(DIP3_GPIO, GPIO_INPUT, GPIO_PUSHPULL);
	GPIO_SetupPinMux(DIP4_GPIO, GPIO_MUX_CPU1, 0);
	GPIO_SetupPinOptions(DIP4_GPIO, GPIO_INPUT, GPIO_PUSHPULL);


	while(1)
	{
		// TRead DIP SWITCH GPIO:
		DIP_State[0] = GPIO_ReadPin(DIP1_GPIO);
		DIP_State[1] = GPIO_ReadPin(DIP2_GPIO);
		DIP_State[2] = GPIO_ReadPin(DIP3_GPIO);
		DIP_State[3] = GPIO_ReadPin(DIP4_GPIO);

		// Delay for a bit
		for(i=0; i<250000; i++);
	 }

}


/*****************************************************************************
 *
 * Function name: RWLK_UserButton_Test
 *
 * Description: Toggle manualy DIP Switch and view DIPx_GPIO value using stop
 * 				button and DIP_State[0/1].
 *
 * Parameters:
 * Returns:
 ******************************************************************************/
void RWLK_UserButton_Test (void)
{
	uint32_t i;

	uint16_t UserButton1_GPIO = 108;
	uint16_t UserButton2_GPIO = 109;

	volatile uint16_t UserButton[2] = {0,0};


	// Configure all Dip Switches:
	GPIO_SetupPinMux(UserButton1_GPIO, GPIO_MUX_CPU1, 0);
	GPIO_SetupPinOptions(UserButton1_GPIO, GPIO_INPUT, GPIO_PUSHPULL);
	GPIO_SetupPinMux(UserButton2_GPIO, GPIO_MUX_CPU1, 0);
	GPIO_SetupPinOptions(UserButton2_GPIO, GPIO_INPUT, GPIO_PUSHPULL);


	while(1)
	{
		// TRead DIP SWITCH GPIO:
		UserButton[0] = GPIO_ReadPin(UserButton1_GPIO);
		UserButton[1] = GPIO_ReadPin(UserButton2_GPIO);

		// Delay for a bit
		for(i=0; i<250000; i++);
	 }

}

/*****************************************************************************
 *
 * Function name: RWLK_GPIO_Toggele_by_Timer_Test
 *
 * Description: This function Toggele GPIO pin, according to Timer counter readings.
 *              Timer1 and Timer2 ticks every 1 mili/micro second respectivly.
 *              The Toggele is done every 10th Timer's tick.
 *              The toggeled pin creates a square wave which can be seen by a scope,
 *              This way the timer accuracy can be validated.
 *
 * Parameters:
 * Returns:
 ******************************************************************************/
void RWLK_GPIO_Toggele_by_Timer_Test (void)
{
	uint32_t TempCounter = 0;
	uint32_t LastRecord = 0;

	uint16_t GPIO_Num = 64;
	uint16_t PinState = 0;

	// Configure timers 1 and 2 to be milisec and microsec counters:
	D_Timer_Config_mSec_Timer();
	D_Timer_Config_uSec_Timer();

	// Configure all Leds-GPIO:
	GPIO_SetupPinMux(GPIO_Num, GPIO_MUX_CPU1, 0);
	GPIO_SetupPinOptions(GPIO_Num, GPIO_OUTPUT, GPIO_PUSHPULL);


	while(1)
	{
		// read Timer counter:
		TempCounter = D_Timer_Get_mSec_Counter();
		//TempCounter = D_Timer_Get_uSec_Counter();

		// Toggele pin every 10 Ticks:
		if (TempCounter % 10 == 0 && TempCounter != LastRecord)
		{
			PinState = PinState^1;
			GPIO_WritePin(GPIO_Num, PinState);
			LastRecord = TempCounter;
		}
	 }

}

/*******************************************************************************
* file name: RWLK_Tests.c
*
* Description: .
*
* Note: Developed for TI F28377D.
*
* Author:  Yehuda Bitton
* Date:
*
******************************************************************************/

/*******************************************************************************
*  Includes
******************************************************************************/
#include "RWLK_SPI_Driver.h"
#include "ExtFlash_Driver.h"
#include "RWLK_Tests.h"

/*******************************************************************************
 *  Defines
 ******************************************************************************/



/*******************************************************************************
*  Data types
******************************************************************************/

/*------------------------------------------------------------------------------
Variables
------------------------------------------------------------------------------*/

/*******************************************************************************
*  Constants and Macros
******************************************************************************/

/*******************************************************************************
*  Static Data deceleration
******************************************************************************/

/******************************************************************************
*  Private Functions Decleration:
******************************************************************************/
uint8_t  Test_Compare_Array(uint8_t *array1, uint8_t *array2, uint32_t length);

/*****************************************************************************
*  Public Function Bodies
******************************************************************************/

/*****************************************************************************
 *
 * Function name: Test_ExtFLASH
 *
 * Description: This functin preform Read, Write and erase operations.
 * 				It first reads the Flash product ID.
 * 				Then it wirte data to a certain page, read it back and finaly
 * 				erase this sub block.
 * 				Function verify these operation success.
 *
 * Parameters: none.
 *
 * Returns: TEST_PASS or TEST_FAIL.
 *
 ******************************************************************************/
uint8_t Test_ExtFLASH (void)
{
    uint32_t pID = 0;
	ExFlash_ReturnStatus ret = Flash_Success;

	SPI_Gpio_Init(SPIC_BASE);

	//GPIO_Setup functions call inside to EALLOW EDIS macros;
	// Configure CS: (19 in controlCARD)
    GPIO_SetupPinMux(125, GPIO_MUX_CPU1, 0);
    GPIO_SetupPinOptions(125, GPIO_OUTPUT, GPIO_PUSHPULL);
	GPIO_WritePin(125, 1); // Disable CS

    // Configure Wprotect: (41 in controlCARD)
    GPIO_SetupPinMux(126, GPIO_MUX_CPU1, 0);
    GPIO_SetupPinOptions(126, GPIO_OUTPUT, GPIO_PUSHPULL);
	GPIO_WritePin(126, 1); // Disable Write Protect

	// Configure Hold: (40 in controlCARD)
    GPIO_SetupPinMux(127, GPIO_MUX_CPU1, 0);
    GPIO_SetupPinOptions(127, GPIO_OUTPUT, GPIO_PUSHPULL);
	GPIO_WritePin(127, 1); // Disable Hold

	SPI_Init_Registers(SPIC_BASE , BITS_8 , SPI_BITRATE_128);


	ExtFlash_Read_Device_Id (&pID);


	uint32_t SubSectorNumber = 0x0;
	uint32_t My_Addr = SubSectorNumber<<12;
	const uint32_t Length = 2;

	My_Addr = My_Addr + 0x8;

	uint8_t WriteData[2] = {0x20,0xAA};
	uint8_t ReadData1[2] = {0x0,0x0};
	uint8_t ReadData2[2] = {0x0,0x0};
	uint8_t ErasedArr[2] = {0xFF,0xFF};

	ret |= ExtFlash_Program_UpToPage (My_Addr ,WriteData, Length);
	ret |= ExtFlash_Read_Data (My_Addr , ReadData1, Length, 0);
	ret |= ExtFlash_Erase_SubSector ( SubSectorNumber, 0);
	ret |= ExtFlash_Read_Data (My_Addr, ReadData2, Length, 0);

	if ( ret != Flash_Success)
		return TEST_FAIL;
	else if (Test_Compare_Array(WriteData, ReadData1, Length) == TEST_FAIL)
		return TEST_FAIL;
	else if (Test_Compare_Array(ErasedArr, ReadData2, Length) == TEST_FAIL)
		return TEST_FAIL;
	else
		return TEST_PASS;

}

/*****************************************************************************
 * Function name: Test_Compare_Array
 *
 * Description: Caompare each value of 2 arrays, return FAIl if one value is different.
 *
 * Parameters:
 * 		array1 - pointer to array 1 base address.
 * 		array2 - pointer to array 2 base address.
 * 		length - number of element to compare.
 *
 * Returns: TEST_PASS or TEST_FAIL
 *
 ******************************************************************************/
uint8_t  Test_Compare_Array(uint8_t *array1, uint8_t *array2, uint32_t length)
{
	int32_t i = 0;

	for (i=0; i<length; i++)
	{
		if( *(array1 + i) != *(array2 + i) )
		{
			return TEST_FAIL;
		}
	}

	return TEST_PASS;
}

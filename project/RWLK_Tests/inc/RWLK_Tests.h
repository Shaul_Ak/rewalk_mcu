/*******************************************************************************
 * File name: RWLK_Tests.h
 *
 * Description:
 *
 *
 * Note: Developed for TI F28377D.
 *
 * Author:	Yehuda Bitton
 * Date:
 *
 ******************************************************************************/

/*******************************************************************************
  Multiple include protection
 ******************************************************************************/
#ifndef RWLK_TESTS_H
#define RWLK_TESTS_H

/*******************************************************************************
 *  Include
 ******************************************************************************/
#include "F28x_Project.h"     // Device Header file and Examples Include File
#include "inc/hw_types.h"

#include <stdio.h>

/*******************************************************************************
 *  Defines
 ******************************************************************************/
#define 	TEST_PASS											1
#define     TEST_FAIL											0
/*------------------------------------------------------------------------------
  Configurations
------------------------------------------------------------------------------*/
//_______________________________IMPORTANT______________________________________
// DEBUG_ON is defined in Debug Configuration mode:
// Build -> C2000 Compiler -> Advanced Options -> Predefined Symbols.
//______________________________________________________________________________
// User can uncomment requested tests.
	#define		DEBUG_EXTFLASH								1
	//#define		DEBUG_LEDS									1
	//#define		DEBUG_DIPSWITCH								1
	//#define 	DEBUG_USERBUTTON						 	    1
	//#define		DEBUG_GPIO_TIMER								1
	//#define 	DEBUG_IMU							            1
	//#define 	DEBUG_UART							            1


/*******************************************************************************
 *  Data types
 ******************************************************************************/


/*******************************************************************************
 *  Constants and Macros
 ******************************************************************************/

/*******************************************************************************
 *  Interface function deceleration:
 ******************************************************************************/
void RWLK_Debug_Tests(void);
uint8_t Test_ExtFLASH (void);
void RWLK_Leds_Test (void);
void RWLK_DIPswitch_Test (void);
void RWLK_UserButton_Test (void);
void RWLK_GPIO_Toggele_by_Timer_Test (void);
void RWLK_Test_IMU (void);
void RWLK_Test_UART(void);



/*******************************************************************************
 *  End of file
 ******************************************************************************/
#endif

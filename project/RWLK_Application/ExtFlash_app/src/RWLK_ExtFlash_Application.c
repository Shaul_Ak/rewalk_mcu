/*
 * RWLK_ExtFlash_application.c
 *
 *  Created on: Jan 2, 2017
 *      Author: arie
 */

#include "RWLK.h"
#include "RWLK_ExtFlash_Application.h"
#include "RWLK_SPI_Driver.h"
#include "ExtFlash_Driver.h"


/*
** ===================================================================
**     Function   :  ExtrFlash_Init
**
**     Description :
**					Absolute Encoder initialization
**     Parameters  :
**					u16 Baud Rate - SPI baud rate in kHz. May be from 196 to ENCODER_MAX_RATE.
**     Returns     :
**					Error code. Possible values:
**                      - ERR_OK,
**                      - ERR_SPI_TRANSFER_RATE_DOES_NOT_MATCH.
**     Notes       :
**
** ===================================================================
*/
#pragma CODE_SECTION(ExtrFlash_Init, "ramfuncs")
uint16_t ExtrFlash_Init(void)
{
	uint8_t ErrCode = ERR_OK;

	SPI_Gpio_Init(SPIC_BASE);

	//GPIO_Setup functions call inside to EALLOW EDIS macros;
	// Configure CS: (19 in controlCARD)
    GPIO_SetupPinMux(125, GPIO_MUX_CPU1, 0);
    GPIO_SetupPinOptions(125, GPIO_OUTPUT, GPIO_PUSHPULL);
	GPIO_WritePin(125, 1); // Disable CS

    // Configure Wprotect: (41 in controlCARD)
    GPIO_SetupPinMux(126, GPIO_MUX_CPU1, 0);
    GPIO_SetupPinOptions(126, GPIO_OUTPUT, GPIO_PUSHPULL);
	GPIO_WritePin(126, 1); // Disable Write Protect

	// Configure Hold: (40 in controlCARD)
    GPIO_SetupPinMux(127, GPIO_MUX_CPU1, 0);
    GPIO_SetupPinOptions(127, GPIO_OUTPUT, GPIO_PUSHPULL);
	GPIO_WritePin(127, 1); // Disable Hold

	SPI_Init_Registers(SPIC_BASE , BITS_8 , SPI_BITRATE_128);

	return ErrCode;

}




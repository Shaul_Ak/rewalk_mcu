/*
 * RWLK_ExtFlash_application.h
 *
 *  Created on: Dec 25, 2016
 *      Author: arie
 */

#ifndef RWLK_APPLICATION_EXTFLASH_APP_INC_RWLK_EXTFLASH_APPLICATION_H_
#define RWLK_APPLICATION_EXTFLASH_APP_INC_RWLK_EXTFLASH_APPLICATION_H_


extern uint16_t ExtrFlash_Init(void);
//extern uint16_t ExtrFlash_Read();


#endif /* RWLK_APPLICATION_EXTFLASH_APP_INC_RWLK_EXTFLASH_APPLICATION_H_ */

/*******************************************************************************
 * File name: RWLK_CAN_Msg_Handler.h
 *
 * Description:
 *
 *
 * Note: Developed for TI F28377D.
 *
 * Author:	Yehuda Bitton
 * Date: MAy 2016
 *
 ******************************************************************************/

/*******************************************************************************
 *  Include
 ******************************************************************************/
#include "RWLK.h"
#include "IQmathLib.h"
#include "RWLK_Cla_Control.h"
#include "RWLK_Motor_Control.h"
#include "RWLK_CAN_Application.h"
#include "RWLK_CAN_Driver.h"
#include "RWLK_CAN_Msg_Handler.h"
#include "RWLK_DRV8031_Applications.h"
#include "RWLK_CardConfig.h"
#include "RWLK_Common_Par.h"
#include "RWLK_ADC_Driver.h"
#include "RWLK_ServoManager.h"
#include "RWLK_BLUE_GUI_API.h"

//#include "medexo_controller.h"

#include "RWLK_EQep_Driver.h"
#include "../../RWLK_Periph_Driver/inc/RWLK_ePWM_Driver.h"
#include "../../RWLK_Periph_Driver/inc/RWLK_ADC_Driver.h"
#include "../../RWLK_Periph_Driver/inc/RWLK_SPI_Driver.h"
#include "../../RWLK_Periph_Driver/inc/RWLK_DRV8031_MOTOR_DRV.h"
#include "../../RWLK_Periph_Driver/inc/RWLK_CAN_Driver.h"

#include <stdio.h>



/*******************************************************************************
 *  Data types
 ******************************************************************************/

/*******************************************************************************
 *  Constants and Macros
 ******************************************************************************/
#define LOW_BYTE_MASK 0x00ff
#define DONT_CARE	0

#define BOOT_VERSION_ADDRESS        0x9803   // TODO: move this to the right place

/*==============================================================================
                               	   CANBUS Type Definitions
==============================================================================*/
 const uint8_t COMMAND_RECEIVED_MOVE = 0;
 const uint8_t COMMAND_RECEIVED_RETURN_STATUS = 1;
 const uint8_t COMMAND_RECEIVED_MPB_IS_ALIVE = 2;
 const uint8_t COMMAND_RECEIVED_PERFORM_BIT = 3;
 const uint8_t COMMAND_RECEIVED_LOAD_PROGRAM = 4;
 const uint8_t COMMAND_RECEIVED_SWITCH_MODE = 5;
 const uint8_t COMMAND_RECEIVED_LOAD_PARAMETERS = 6;
 const uint8_t COMMAND_RECEIVED_RESET = 7;
 const uint8_t COMMAND_RECEIVED_ENCODER_RESET = 8;
 const uint8_t COMMAND_RECEIVED_STOP_MOTOR = 9;
 const uint8_t COMMAND_SEND_ACTION_COMPLETED = 20;
 const uint8_t COMMAND_SEND_ACTION_NOT_COMPLETED = 21;
 const uint8_t COMMAND_SEND_SYSTEM_STATUS = 22;
 const uint8_t COMMAND_SEND_STATUS = 23;
 const uint8_t COMMAND_SEND_BIT_COMPLETED = 24;
 const uint8_t COMMAND_SEND_WARNING = 25;
 const uint8_t COMMAND_SEND_ACK = 26;
 const uint8_t COMMAND_EMERGENCY = 27;
 const uint8_t COMMAND_MOTOR_CURRENT_STATUS = 28;
 const uint8_t COMMAND_SEND_STATUS_FSR = 29;
 const uint8_t RETURN_ENCODER_CALIBRATION	= 30;
 const uint8_t COMMAND_SEND_SWITCH_MODE = 5;
 const uint8_t COMMAND_SEND_STAND_MODE = 0;
 const uint8_t COMMAND_SEND_WALK_MODE = 1;
 const uint8_t COMMAND_SEND_ASCEND_MODE = 2;
 const uint8_t COMMAND_SEND_DESC_MODE = 3;
 const uint8_t COMMAND_SEND_SIT_MODE = 4;
 const uint8_t COMMAND_SEND_MANUAL_MODE = 5;

 const uint8_t COMMAND_SEND_INC_ENCODER_POSITION = 0x0d;
 const uint8_t COMMAND_SEND_MOTOR_PHASES_CURRENTS = 44;


 /*******************************************************************************
 *  Global Variables
 ******************************************************************************/
// designated initializer, Other members are initialized as zero
#pragma DATA_SECTION(Requested_Command_Parameters,"ramdata")
Input_Command_Parameters_Type Requested_Command_Parameters = { .ReceivedCommand = NO_REQUEST, .OperationalMode = _MANUAL };

#pragma DATA_SECTION(Calculated_Command_Parameters,"ramdata")
Output_Command_Parameters_Type Calculated_Command_Parameters;

// Hold the card message Id, will be initialized during sw init.
static uint16_t RWLK_CAN_CardMessageID;
static uint16_t RWLK_CAN_CardNumber;

 //static CAN_MSG_DATA Out_Can_Message_Data;  // used for building the message to be sent

sEncData  sEncDataSend;
sCellData sCellDataSend;

/*******************************************************************************
*  External Variables
******************************************************************************/
extern bool  ByPassEnable ;
//extern bool  TestSystemFlag ;
extern bool  AllMotorsFlag ;
extern uint32_t CurrentOperationMode ;  // Mode parameter in old MCU program
extern int32_t IncEncFeedbackPosition;
/*************************************************************************
 *	Static Functions
 *************************************************************************/


/*****************************************************************************
 function bodies
 *****************************************************************************/

/*****************************************************************************
 * Function name: RWLK_CAN_MSG_Handler_Init
 *
 * Description: Initialize module variables
 * Parameters:  None
 * Returns:     None.
 ******************************************************************************/
void RWLK_CAN_MSG_Handler_Init(void)
{

	RWLK_CAN_CardMessageID = CardConfigGetMessageID();
	RWLK_CAN_CardNumber = CardConfigGetCardNumber();

}


/*****************************************************************************
 * Function name: RWLK_CAN_Decode_EncoderTest_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  Msg_Data - struct of 8 bytes of data.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_EncoderTest_Msg, "ramfuncs")
void RWLK_CAN_Decode_EncoderTest_Msg(const CAN_MSG_DATA* Msg_Data)
{

}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_ReturnStatus_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  Msg_Data - struct of 8 bytes of data.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_ReturnStatus_Msg, "ramfuncs")
void RWLK_CAN_Decode_ReturnStatus_Msg(const CAN_MSG_DATA* Msg_Data)
{
	Requested_Command_Parameters.ReceivedCommand = RETURN_STATUS;
	Requested_Command_Parameters.AbsEncoderReadFlag = (false == Msg_Data->Byte1);
	Requested_Command_Parameters.MotorCurrentReadFlag = (true == Msg_Data->Byte1);

}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_ReadLimits_Msg
 *
 * Description: Process Income CAN BUS message. Read the calibration boundaries command
 * Parameters:  Msg_Data - struct of 8 bytes of data.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_ReadLimits_Msg, "ramfuncs")
void RWLK_CAN_Decode_ReadLimits_Msg(void)
{
	Requested_Command_Parameters.ReceivedCommand = READ_LIMITS;
}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_ReadyForMovement_Msg
 *
 * Description: Process Income CAN BUS message. Check if motor is ready for movement
 * Parameters:  Msg_Data - struct of 8 bytes of data.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_ReadyForMovement_Msg, "ramfuncs")
void RWLK_CAN_Decode_ReadyForMovement_Msg(void)
{
	Requested_Command_Parameters.ReceivedCommand = READY_FOR_MOVEMENT_TEST;
}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_ReadyForMovement_Msg
 *
 * Description: Process Income CAN BUS message. Check if motor is ready for movement
 * Parameters:  Msg_Data - struct of 8 bytes of data.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_AbsoluteEncoderProblem_Msg, "ramfuncs")
void RWLK_CAN_Decode_AbsoluteEncoderProblem_Msg(void)
{
	Requested_Command_Parameters.ReceivedCommand = ABS_ENCODER_PROBLEM;
}


/*****************************************************************************
 * Function name: RWLK_CAN_Decode_XStop_Motor_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  Msg_Data - struct of 8 bytes of data.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_XStop_Motor_Msg, "ramfuncs")
void RWLK_CAN_Decode_XStop_Motor_Msg(void)
{
	Requested_Command_Parameters.ReceivedCommand = X_STOP_MOTOR;
}



/*****************************************************************************
 * Function name: RWLK_CAN_Decode_YStop_Motor_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  Msg_Data - struct of 8 bytes of data.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_YStop_Motor_Msg, "ramfuncs")
void RWLK_CAN_Decode_YStop_Motor_Msg(void)
{
	Requested_Command_Parameters.ReceivedCommand = Y_STOP_MOTOR;
}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_ReturnSwitchMode_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  Msg_Data - struct of 8 bytes of data.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_SwitchMode_Msg, "ramfuncs")
void RWLK_CAN_Decode_SwitchMode_Msg(const CAN_MSG_DATA* Msg_Data)
{
	Requested_Command_Parameters.ReceivedCommand = SWITCH_MODE;
	Requested_Command_Parameters.OperationalMode = (OPERATION_MODE)Msg_Data->Byte1;
	Requested_Command_Parameters.ManualVelocity = Msg_Data->Byte2;
}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_XAxisMoveOpenLoop_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  Msg_Data - struct of 8 bytes of data.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_XAxisMoveOpenLoop_Msg, "ramfuncs")
void RWLK_CAN_Decode_XAxisMoveOpenLoop_Msg(const CAN_MSG_DATA *Msg_Data)
{
	Requested_Command_Parameters.ReceivedCommand = X_MOVE_OPEN_LOOP;
	Requested_Command_Parameters.PwmTimePar = Msg_Data->Byte1;
	Requested_Command_Parameters.PwmTimePar <<= 8;
	Requested_Command_Parameters.PwmTimePar = Requested_Command_Parameters.PwmTimePar + Msg_Data->Byte2;
	Requested_Command_Parameters.OpenLoopEnable = Msg_Data->Byte3;
}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_YAxisMoveOpenLoop_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  Msg_Data - struct of 8 bytes of data.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_YAxisMoveOpenLoop_Msg, "ramfuncs")
void RWLK_CAN_Decode_YAxisMoveOpenLoop_Msg(const CAN_MSG_DATA *Msg_Data)
{
	Requested_Command_Parameters.ReceivedCommand = Y_MOVE_OPEN_LOOP;
	Requested_Command_Parameters.PwmTimePar = Msg_Data->Byte1;
	Requested_Command_Parameters.PwmTimePar <<= 8;
	Requested_Command_Parameters.PwmTimePar = Requested_Command_Parameters.PwmTimePar + Msg_Data->Byte2;
	Requested_Command_Parameters.OpenLoopEnable = Msg_Data->Byte3;
	Requested_Command_Parameters.CommandA = Msg_Data->Byte4;
	Requested_Command_Parameters.CommandA <<= 8;
	Requested_Command_Parameters.CommandA = Requested_Command_Parameters.CommandA + Msg_Data->Byte5;
}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_CurrentTrsh_Msg
 *
 * Description: Process Income CAN BUS message. and save the parameters for
 * 			    Updating motors current threshold
 * Parameters:  Msg_Data - struct of 8 bytes of data.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_CurrentTrsh_Msg, "ramfuncs")
void RWLK_CAN_Decode_CurrentTrsh_Msg(const CAN_MSG_DATA* Msg_Data)
{
	Requested_Command_Parameters.ReceivedCommand = CURRENT_TRSH;
	Requested_Command_Parameters.CurrentHandleAlgo = Msg_Data->Byte3;
	Requested_Command_Parameters.Walk_Current_Threshold = (uint16_t)(Msg_Data->Byte4 << 8) + Msg_Data->Byte5;
	Requested_Command_Parameters.Stairs_Current_Threshold = (uint16_t)(Msg_Data->Byte6 << 8) + Msg_Data->Byte7;
}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_IamAlive_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  Msg_Data - struct of 8 bytes of data.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_IamAlive_Msg, "ramfuncs")
void RWLK_CAN_Decode_IamAlive_Msg(const CAN_MSG_DATA* Msg_Data)
{
	Requested_Command_Parameters.ReceivedCommand = IAM_ALIVE;
	Requested_Command_Parameters.IamAliveActivateFlag=  Msg_Data->Byte1;
}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_LogMode_Msg
 *
 * Description: Decode Income CAN BUS message.
 * Parameters:  Msg_Data - struct of 8 bytes of data.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_LogMode_Msg, "ramfuncs")
void RWLK_CAN_Decode_LogMode_Msg(const CAN_MSG_DATA* Msg_Data)
{
	Requested_Command_Parameters.ReceivedCommand = LogMode;
	Requested_Command_Parameters.LogLevel = Msg_Data->Byte1;
}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_SetPid_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  Msg_Data - struct of 8 bytes of data.
 * Returns:     None.
 ******************************************************************************/
/*#pragma CODE_SECTION(RWLK_CAN_Decode_SetPid_Msg, "ramfuncs")
void RWLK_CAN_Decode_SetPid_Msg(const CAN_MSG_DATA* Msg_Data)
{
	Requested_Command_Parameters.ReceivedCommand = SET_PID;
}
*/


/*****************************************************************************
 * Function name: RWLK_CAN_Decode_AccFactor_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  Msg_Data - struct of 8 bytes of data.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_AccFactor_Msg, "ramfuncs")
void RWLK_CAN_Decode_AccFactor_Msg(const CAN_MSG_DATA* Msg_Data)
{
	Requested_Command_Parameters.ReceivedCommand = ACC_FACTOR;
}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_WriteSamplingTime_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  Msg_Data - struct of 8 bytes of data.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_WriteSamplingTime_Msg, "ramfuncs")
void RWLK_CAN_Decode_WriteSamplingTime_Msg(const CAN_MSG_DATA* Msg_Data)
{

}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_ReadSamplingTime_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  Msg_Data - struct of 8 bytes of data.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_ReadSamplingTime_Msg, "ramfuncs")
void RWLK_CAN_Decode_ReadSamplingTime_Msg(const CAN_MSG_DATA* Msg_Data)
{
	Requested_Command_Parameters.ReceivedCommand = READ_SAMPLING_TIME;
}


/*****************************************************************************
 * Function name: RWLK_CAN_Decode_FlashStore_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  Msg_Data - struct of 8 bytes of data.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_FlashStore_Msg, "ramfuncs")
void RWLK_CAN_Decode_FlashStore_Msg(const CAN_MSG_DATA* Msg_Data)
{
	Requested_Command_Parameters.ReceivedCommand = FLASH_STORE;
}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_SetHWLimits_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  Msg_Data - struct of 8 bytes of data.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_SetHWLimits_Msg, "ramfuncs")
void RWLK_CAN_Decode_SetHWLimits_Msg(const CAN_MSG_DATA* Msg_Data)
{
	Requested_Command_Parameters.ReceivedCommand = SET_HWLIMITS;
//	Can_Message_Data.Byte1 = sMailBoxDataPtr->RXMsgDataByte1;
//	Can_Message_Data.Byte2 = sMailBoxDataPtr->RXMsgDataByte2;
//	Can_Message_Data.Byte3 = sMailBoxDataPtr->RXMsgDataByte3;
//	Can_Message_Data.Byte4 = sMailBoxDataPtr->RXMsgDataByte4;
}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_AutoCalibration_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  Msg_Data - struct of 8 bytes of data.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_AutoCalibration_Msg, "ramfuncs")
void RWLK_CAN_Decode_AutoCalibration_Msg(const CAN_MSG_DATA* Msg_Data)
{
	Requested_Command_Parameters.ReceivedCommand = AUTO_CALIBRATION;
	Requested_Command_Parameters.OpenLoopPWM = Msg_Data->Byte2;
}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_ChangeXPwmCommand_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_ChangeXPwmCommand_Msg, "ramfuncs")
void RWLK_CAN_Decode_ChangeXPwmCommand_Msg(const CAN_MSG_DATA* Msg_Data)
{
	int16_t tempCommand;

	tempCommand = Msg_Data->Byte1;
	tempCommand <<= 8;
	Requested_Command_Parameters.CommandA = tempCommand + Msg_Data->Byte2;

	tempCommand = Msg_Data->Byte3;
	tempCommand <<= 8;
	Requested_Command_Parameters.CommandB = tempCommand + Msg_Data->Byte4;

	tempCommand = Msg_Data->Byte5;
	tempCommand <<= 8;
	Requested_Command_Parameters.CommandC = tempCommand + Msg_Data->Byte6;

	Requested_Command_Parameters.ReceivedCommand = CHANGE_XPWM;

}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_ChangeYPwmCommand_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_ChangeYPwmCommand_Msg, "ramfuncs")
void RWLK_CAN_Decode_ChangeYPwmCommand_Msg(const CAN_MSG_DATA* Msg_Data)
{
	int16_t tempCommand;

	tempCommand = Msg_Data->Byte1;
	tempCommand <<= 8;
	Requested_Command_Parameters.CommandA = tempCommand + Msg_Data->Byte2;

	tempCommand = Msg_Data->Byte3;
	tempCommand <<= 8;
	Requested_Command_Parameters.CommandB = tempCommand + Msg_Data->Byte4;

	tempCommand = Msg_Data->Byte5;
	tempCommand <<= 8;
	Requested_Command_Parameters.CommandC = tempCommand + Msg_Data->Byte6;

	Requested_Command_Parameters.ReceivedCommand = CHANGE_YPWM;

}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_XCommutationSetRequest_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_XCommutationSetRequest_Msg, "ramfuncs")
void RWLK_CAN_Decode_XCommutationSetRequest_Msg(const CAN_MSG_DATA* Msg_Data)
{
	Requested_Command_Parameters.ReceivedCommand = X_COMMUT_REQUEST;
	Requested_Command_Parameters.BRLS_PWM_value = (Msg_Data->Byte1 <<8) + Msg_Data->Byte2;
}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_YCommutationSetRequest_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_YCommutationSetRequest_Msg, "ramfuncs")
void RWLK_CAN_Decode_YCommutationSetRequest_Msg(const CAN_MSG_DATA* Msg_Data)
{
	Requested_Command_Parameters.ReceivedCommand = Y_COMMUT_REQUEST;
	Requested_Command_Parameters.BRLS_PWM_value = (Msg_Data->Byte1 <<8) + Msg_Data->Byte2;
}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_SetProfileParameterRequest_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_SetProfileParameterRequest_Msg, "ramfuncs")
void RWLK_CAN_Decode_SetProfileParameterRequest_Msg(const CAN_MSG_DATA* Msg_Data)
{
	int32_t TempPar = 0;

	Requested_Command_Parameters.ReceivedCommand = (COMMAND_PROTOCOL)Msg_Data->Byte0;
	// Calculate the value to be set
	TempPar = Msg_Data->Byte1;
				TempPar <<= 8;
				TempPar = TempPar + Msg_Data->Byte2;
				TempPar <<= 8;
				TempPar = TempPar + Msg_Data->Byte3;
				TempPar <<= 8;
				TempPar = TempPar + Msg_Data->Byte4;
	// Save the value to set
	Requested_Command_Parameters.ProfileNewValue = TempPar;
	Requested_Command_Parameters.TheParameterToSet = (POSITION_ADJUSTER_PARAMS)Msg_Data->Byte5;

}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_ServoCurrentLoop_Msg
 *
 * Description: Process Income CAN BUS message, of type Servo Current Loop (SCL-XX)
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_ServoCurrentLoop_Msg, "ramfuncs")
void RWLK_CAN_Decode_ServoCurrentLoop_Msg(const CAN_MSG_DATA* Msg_Data)
{
	Requested_Command_Parameters.ReceivedCommand = (COMMAND_PROTOCOL)Msg_Data->Byte0;
	Requested_Command_Parameters.PIFilterMember = (PI_FILTER_MEMBERS)Msg_Data->Byte3;
	Requested_Command_Parameters.FilterValue = 	(Msg_Data->Byte1 << 8) + Msg_Data->Byte2;
}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_ServoVelocityLoop_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_ServoVelocityLoop_Msg, "ramfuncs")
void RWLK_CAN_Decode_ServoVelocityLoop_Msg(const CAN_MSG_DATA* Msg_Data)
{
	Requested_Command_Parameters.ReceivedCommand = (COMMAND_PROTOCOL)Msg_Data->Byte0;
	Requested_Command_Parameters.PIFilterMember = (PI_FILTER_MEMBERS)Msg_Data->Byte3;
	Requested_Command_Parameters.FilterValue = 	(Msg_Data->Byte1 << 8) + Msg_Data->Byte2;
	//SetPiGains(F_VPI, pif, Msg_Data);
}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_ServoPositionLoop_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_ServoPositionLoop_Msg, "ramfuncs")
void RWLK_CAN_Decode_ServoPositionLoop_Msg(const CAN_MSG_DATA* Msg_Data)
{
	Requested_Command_Parameters.ReceivedCommand = (COMMAND_PROTOCOL)Msg_Data->Byte0;
	Requested_Command_Parameters.PIFilterMember = (PI_FILTER_MEMBERS)Msg_Data->Byte3;
	Requested_Command_Parameters.FilterValue = 	(Msg_Data->Byte1 << 8) + Msg_Data->Byte2;
}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_ServoPositionLoop_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_SvlSofBw_Msg, "ramfuncs")
void RWLK_CAN_Decode_SvlSofBw_Msg(const CAN_MSG_DATA* Msg_Data)
{

	Requested_Command_Parameters.ReceivedCommand = (COMMAND_PROTOCOL)Msg_Data->Byte0;
	Requested_Command_Parameters.SOFilterMember = (SOF_FILTER_MEMBERS)Msg_Data->Byte3;
	Requested_Command_Parameters.FilterValue = 	(Msg_Data->Byte1 << 8) + Msg_Data->Byte2;
}


/*****************************************************************************
 * Function name: RWLK_CAN_Decode_SetSofPars_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_SetSofPars_Msg, "ramfuncs")
void RWLK_CAN_Decode_SetSofPars_Msg(const CAN_MSG_DATA* Msg_Data)
{
	Requested_Command_Parameters.ReceivedCommand = (COMMAND_PROTOCOL)Msg_Data->Byte0;
}



/*****************************************************************************
 * Function name: RWLK_CAN_Decode_CalcSofPars_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_CalcSofPars_Msg, "ramfuncs")
void RWLK_CAN_Decode_CalcSofPars_Msg(const CAN_MSG_DATA* Msg_Data)
{
	Requested_Command_Parameters.ReceivedCommand = (COMMAND_PROTOCOL)Msg_Data->Byte0;
}


/*****************************************************************************
 * Function name: RWLK_CAN_Decode_SvlNfSetPar_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_SvlNfSetPar_Msg, "ramfuncs")
void RWLK_CAN_Decode_SvlNfSetPar_Msg(const CAN_MSG_DATA* Msg_Data)
{
	Requested_Command_Parameters.ReceivedCommand = (COMMAND_PROTOCOL)Msg_Data->Byte0;
}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_CalcNfPars_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_CalcNfPars_Msg, "ramfuncs")
void RWLK_CAN_Decode_CalcNfPars_Msg(const CAN_MSG_DATA* Msg_Data)
{
	Requested_Command_Parameters.ReceivedCommand = (COMMAND_PROTOCOL)Msg_Data->Byte0;
}


/*****************************************************************************
 * Function name: RWLK_CAN_Decode_SendToClaNfPars_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_SendToClaNfPars_Msg, "ramfuncs")
void RWLK_CAN_Decode_SendToClaNfPars_Msg(const CAN_MSG_DATA* Msg_Data)
{
	Requested_Command_Parameters.ReceivedCommand = (COMMAND_PROTOCOL)Msg_Data->Byte0;
}
/*****************************************************************************
 * Function name: RWLK_CAN_Decode_GCSet_Msg
 *
 * Description: Process setting gracefull collapse request
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_GCSet_Msg, "ramfuncs")
void RWLK_CAN_Decode_GCSet_Msg(const CAN_MSG_DATA* Msg_Data)
{
	Requested_Command_Parameters.ReceivedCommand = GC_SET;
}
/*****************************************************************************
 * Function name: RWLK_CAN_Decode_Xcalibration_Msg
 *
 * Description: Process setting gracefull collapse request
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_Xcalibration_Msg, "ramfuncs")
void RWLK_CAN_Decode_Xcalibration_Msg(const CAN_MSG_DATA* Msg_Data)
{
	Requested_Command_Parameters.ReceivedCommand = XCALIBRATION;
}
/*****************************************************************************
 * Function name: RWLK_CAN_Decode_Handle_Modes_Setup
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_Handle_Modes_Setup, "ramfuncs")
void RWLK_CAN_Decode_Handle_Modes_Setup(const CAN_MSG_DATA* Msg_Data)
{
	Requested_Command_Parameters.ReceivedCommand = (COMMAND_PROTOCOL)Msg_Data->Byte0;
	Requested_Command_Parameters.HandleStatus = (COMMAND_PROTOCOL)Msg_Data->Byte1;

}
/*****************************************************************************
 * Function name: RWLK_CAN_Decodes_Leg_Side
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decodes_Leg_Side, "ramfuncs")
void RWLK_CAN_Decodes_Leg_Side(const CAN_MSG_DATA* Msg_Data)
{
	Requested_Command_Parameters.ReceivedCommand = (COMMAND_PROTOCOL)Msg_Data->Byte0;
	Requested_Command_Parameters.LegSideSelection = (COMMAND_PROTOCOL)Msg_Data->Byte1;
}
/*****************************************************************************
 * Function name: RWLK_CAN_Decode_Ycalibration_Msg
 *
 * Description: Process setting gracefull collapse request
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_Ycalibration_Msg, "ramfuncs")
void RWLK_CAN_Decode_Ycalibration_Msg(const CAN_MSG_DATA* Msg_Data)
{
	Requested_Command_Parameters.ReceivedCommand = YCALIBRATION;
	Requested_Command_Parameters.PwmTimePar = Msg_Data->Byte1;
	Requested_Command_Parameters.PwmTimePar <<= 8;
	Requested_Command_Parameters.PwmTimePar = Requested_Command_Parameters.PwmTimePar + Msg_Data->Byte2;
	YpAdjusterParametersStruct.AdjusterProfileVelocity = (double)Requested_Command_Parameters.PwmTimePar;
}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_StopCalibration_Msg
 *
 * Description: Process setting gracefull collapse request
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_StopCalibration_Msg, "ramfuncs")
void RWLK_CAN_Decode_StopCalibration_Msg(const CAN_MSG_DATA* Msg_Data)
{
	Requested_Command_Parameters.ReceivedCommand = STOP_CALIBRATION;
}


/*****************************************************************************
 * Function name: RWLK_CAN_Decode_AdjusterRequest_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_AdjusterRequest_Msg, "ramfuncs")
void RWLK_CAN_Decode_AdjusterRequest_Msg(const CAN_MSG_DATA* Msg_Data)
{
	Requested_Command_Parameters.ReceivedCommand = (COMMAND_PROTOCOL)Msg_Data->Byte0;
	Requested_Command_Parameters.AdjusterRequest = (ADJUSTER_REQUEST_PARAMETERS)Msg_Data->Byte1;

}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_XAxisMove_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_XAxisMove_Msg, "ramfuncs")
void RWLK_CAN_Decode_XAxisMove_Msg(const CAN_MSG_DATA* Msg_Data)
{
		Requested_Command_Parameters.ReceivedCommand = X_MOVE;
		Requested_Command_Parameters.DesiredPosition = 0;
		Requested_Command_Parameters.ProfileMaxVelocity = 0;
	/* TODO: orit  - falling detection is sent by INMP but is not used */
	//Requested_Command_Parameters.PositionAckDeg = Msg_Data->Byte2;
	//Requested_Command_Parameters.DesiredPosition = (uint16_t)(Msg_Data->Byte3 << 8) + Msg_Data->Byte4;
	//Requested_Command_Parameters.ProfileMaxVelocity = (uint16_t)(Msg_Data->Byte5 << 8) + Msg_Data->Byte6;
		Requested_Command_Parameters.DesiredPosition = (Msg_Data->Byte1 << 8) + Msg_Data->Byte2;
		Requested_Command_Parameters.DesiredPosition = (Requested_Command_Parameters.DesiredPosition << 8) + Msg_Data->Byte3;
		Requested_Command_Parameters.DesiredPosition = (Requested_Command_Parameters.DesiredPosition << 8) + Msg_Data->Byte4;

		Requested_Command_Parameters.ProfileMaxVelocity = (Msg_Data->Byte5 << 8) + Msg_Data->Byte6;
		Requested_Command_Parameters.ProfileMaxVelocity = (Requested_Command_Parameters.ProfileMaxVelocity << 8) + Msg_Data->Byte7;

}




/*****************************************************************************
 * Function name: RWLK_CAN_Decode_YAxisMove_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_YAxisMove_Msg, "ramfuncs")
void RWLK_CAN_Decode_YAxisMove_Msg(const CAN_MSG_DATA* Msg_Data)
{
		Requested_Command_Parameters.ReceivedCommand = Y_MOVE;
		Requested_Command_Parameters.DesiredPosition = 0;
		Requested_Command_Parameters.ProfileMaxVelocity = 0;
	/* TODO: orit  - falling detection is sent by INMP but is not used */
	//Requested_Command_Parameters.PositionAckDeg = Msg_Data->Byte2;
	//Requested_Command_Parameters.DesiredPosition = (uint16_t)(Msg_Data->Byte3 << 8) + Msg_Data->Byte4;
	//Requested_Command_Parameters.ProfileMaxVelocity = (uint16_t)(Msg_Data->Byte5 << 8) + Msg_Data->Byte6;
		Requested_Command_Parameters.DesiredPosition = (Msg_Data->Byte1 << 8) + Msg_Data->Byte2;
		Requested_Command_Parameters.DesiredPosition = (Requested_Command_Parameters.DesiredPosition << 8) + Msg_Data->Byte3;
		Requested_Command_Parameters.DesiredPosition = (Requested_Command_Parameters.DesiredPosition << 8) + Msg_Data->Byte4;

		Requested_Command_Parameters.ProfileMaxVelocity = (Msg_Data->Byte5 << 8) + Msg_Data->Byte6;
		Requested_Command_Parameters.ProfileMaxVelocity = (Requested_Command_Parameters.ProfileMaxVelocity << 8) + Msg_Data->Byte7;

}




/*****************************************************************************
 * Function name: RWLK_CAN_Process_XXX_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Process_Drv8031InitRequest_Msg, "ramfuncs")
void RWLK_CAN_Process_Drv8031InitRequest_Msg(void)
{
	DRV8031_Init(SPIA_BASE , BITS_16 , SPI_BITRATE_128 );
}


/*****************************************************************************
 * Function name: RWLK_CAN_Decode_JumpToBoot_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_JumpToBoot_Msg, "ramfuncs")
void RWLK_CAN_Decode_JumpToBoot_Msg(void)
{
	Requested_Command_Parameters.ReceivedCommand = JUMP_TO_BOOT;
}

/*****************************************************************************
 * Function name: RWLK_CAN_Process_XXX_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
/*#pragma CODE_SECTION(RWLK_CAN_Decode_ByPassMode_Msg, "ramfuncs")
void RWLK_CAN_Decode_ByPassMode_Msg(const CAN_MSG_DATA* Msg_Data)
{
	Requested_Command_Parameters.ReceivedCommand = ByPassMode;
	if (Msg_Data->Byte1 == 0x04)
		Requested_Command_Parameters.ByPassEnable = !Requested_Command_Parameters.ByPassEnable;
}*/

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_JointState_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_JointState_Msg, "ramfuncs")
void RWLK_CAN_Decode_JointState_Msg(void)
{
	Requested_Command_Parameters.ReceivedCommand = JOINT_STATE;
}


/*****************************************************************************
 * Function name: RWLK_CAN_Decode_GetXIncEncoderValue_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_GetXIncEncoderValue_Msg, "ramfuncs")
void RWLK_CAN_Decode_GetXIncEncoderValue_Msg(void)
{
	Requested_Command_Parameters.ReceivedCommand = GET_XINC_ENCODER_VALUE;
}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_GetYIncEncoderValue_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_GetXIncEncoderValue_Msg, "ramfuncs")
void RWLK_CAN_Decode_GetYIncEncoderValue_Msg(void)
{
	Requested_Command_Parameters.ReceivedCommand = GET_YINC_ENCODER_VALUE;
}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_GetAbsEncoderAngle_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_GetAbsEncoderAngle_Msg, "ramfuncs")
void RWLK_CAN_Decode_GetAbsEncoderAngle_Msg(void)
{
	Requested_Command_Parameters.ReceivedCommand = GET_XABS_ENCODER_ANG;
}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_XMotorEnable_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_XMotorEnable_Msg, "ramfuncs")
void RWLK_CAN_Decode_XMotorEnable_Msg(void)
{
	Requested_Command_Parameters.ReceivedCommand = X_MOTOR_ENABLE;
}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_YMotorEnable_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_YMotorEnable_Msg, "ramfuncs")
void RWLK_CAN_Decode_YMotorEnable_Msg(void)
{
	Requested_Command_Parameters.ReceivedCommand = Y_MOTOR_ENABLE;
}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_XMotorDisable_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_XMotorDisable_Msg, "ramfuncs")
void RWLK_CAN_Decode_XMotorDisable_Msg(void)
{
	Requested_Command_Parameters.ReceivedCommand = X_MOTOR_DISABLE;
}


/*****************************************************************************
 * Function name: RWLK_CAN_Decode_YMotorDisable_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_YMotorDisable_Msg, "ramfuncs")
void RWLK_CAN_Decode_YMotorDisable_Msg(void)
{
	Requested_Command_Parameters.ReceivedCommand = Y_MOTOR_DISABLE;
}


/*****************************************************************************
 * Function name: RWLK_CAN_Decode_ReleaseXMotor_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_ReleaseXMotor_Msg, "ramfuncs")
void RWLK_CAN_Decode_ReleaseXMotor_Msg(void)
{
	Requested_Command_Parameters.ReceivedCommand = X_RELEASE_MOTOR;
}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_ReleaseYMotor_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_ReleaseYMotor_Msg, "ramfuncs")
void RWLK_CAN_Decode_ReleaseYMotor_Msg(void)
{
	Requested_Command_Parameters.ReceivedCommand = Y_RELEASE_MOTOR;
}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_ActionCompleted_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_ActionCompleted_Msg, "ramfuncs")
void RWLK_CAN_Decode_ActionCompleted_Msg(void)
{
	Requested_Command_Parameters.ReceivedCommand = ACTION_COMPLETED;
}



/*****************************************************************************
 * Function name: RWLK_CAN_Process_XXX_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_UnreleaseXMotor_Msg, "ramfuncs")
void RWLK_CAN_Decode_UnreleaseXMotor_Msg(void)
{
	Requested_Command_Parameters.ReceivedCommand = X_UNRELEASE_MOTOR;
}

/*****************************************************************************
 * Function name: RWLK_CAN_Process_XXX_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_UnreleaseYMotor_Msg, "ramfuncs")
void RWLK_CAN_Decode_UnreleaseYMotor_Msg(void)
{
	Requested_Command_Parameters.ReceivedCommand = Y_UNRELEASE_MOTOR;
}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_FilterParams_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_FilterParams_Msg, "ramfuncs")
void RWLK_CAN_Decode_FilterParams_Msg(const CAN_MSG_DATA* Msg_Data)
{
	Requested_Command_Parameters.ReceivedCommand = FILTER_PAR;
}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_PerformSystemTesting_Msg
 *
 * Description: Process Income CAN BUS message, and keep the relevant parameters for later performing the request
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_PerformSystemTesting_Msg, "ramfuncs")
void RWLK_CAN_Decode_PerformSystemTesting_Msg(void)
{
	// Save the request for performing the system testing
	Requested_Command_Parameters.ReceivedCommand = PERFORM_SYSTEM_TESTING;

}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_RunTestFlag_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_RunTestFlag_Msg, "ramfuncs")
void RWLK_CAN_Decode_RunTestFlag_Msg(void)
{
	Requested_Command_Parameters.ReceivedCommand = RUNTESTFLAG;
}

/*---------------------------------------------------------------------------------------------*/

/*****************************************************************************
 * Function name: RWLK_CAN_Send_XXX_Msg
 *
 * Description: Send specific CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Send_ActionComplete_Msg, "ramfuncs")
void RWLK_CAN_Send_ActionComplete_Msg(void)
{

	int16_t byte1;
//	int16_t byte4, byte5, byte6, byte7;

//	u32 TempReceivedPosition ;
//	u32 TempActualPosition;
	//asm( " estop0");

	byte1 = Calculated_Command_Parameters.RequiredMode&0x00ff;

//	if (MoveFlag)
//	{
//		MoveFlag = FALSE;
//		TempActualPosition 				= FeedbackPosition;
//		TempReceivedPosition			= DesiredPosition ;
//		if(CARD_NUMBER==CardConfig_e_LEFT_HIP_Card || CARD_NUMBER==CardConfig_e_RIGHT_HIP_Card)
//		{
//			TempActualPosition   =  	TempActualPosition - LowSoftwareLimit;
//			TempReceivedPosition =  	TempReceivedPosition - LowSoftwareLimit;
//		}
//		else if(CARD_NUMBER==CardConfig_e_LEFT_KNEE_Card || CARD_NUMBER==CardConfig_e_RIGHT_KNEE_Card)
//		{
//			TempActualPosition 	 =  	TempActualPosition - HighSoftwareLimit;
//			TempReceivedPosition =  	TempReceivedPosition - HighSoftwareLimit;
//		}
//   		ECanaMboxes.MBOX25.MDH.byte.BYTE5 = TempReceivedPosition & 0x00ff;
//   		ECanaMboxes.MBOX25.MDH.byte.BYTE4 = ((TempReceivedPosition >>= 8) & 0x00ff);
//   		ECanaMboxes.MBOX25.MDH.byte.BYTE7 = TempActualPosition & 0x00ff;
//		ECanaMboxes.MBOX25.MDH.byte.BYTE6 = ((TempActualPosition >>= 8) & 0x00ff);
//	}
//	else
//	{
//		ECanaMboxes.MBOX25.MDH.all = 0 ;
//	}
	CAN_MessageTransmit(ACTION_COMPLETED,
				byte1,
				0,
				0,
				0,
				0,
				0,
				0,
				CARD_MAILBOX_TX, RWLK_CAN_CardMessageID, 8);
}

/*****************************************************************************
 * Function name: RWLK_CAN_Send_XXX_Msg
 *
 * Description: Send specific CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Send_ActionNotComplete_Msg, "ramfuncs")
void RWLK_CAN_Send_ActionNotComplete_Msg(void)
{
	int16_t byte4, byte5, byte6, byte7;
	uint16_t StuckCurrentUnsigned, StuckVoltageUnsigned;

	StuckCurrentUnsigned = (uint16_t)Calculated_Command_Parameters.StuckCurrent;
	StuckVoltageUnsigned = Calculated_Command_Parameters.StuckVoltage;

	byte5 = StuckVoltageUnsigned & LOW_BYTE_MASK;
	byte4 = (uint8_t)((StuckVoltageUnsigned >>= 8) & LOW_BYTE_MASK);
	byte7 = (uint8_t)(StuckCurrentUnsigned & LOW_BYTE_MASK);
	byte6 = (uint8_t)((StuckCurrentUnsigned >>= 8) & LOW_BYTE_MASK);

	CAN_MessageTransmit(ACTION_NOT_COMPLETED,
				Calculated_Command_Parameters.stopalgomethod, //byte 1
				Calculated_Command_Parameters.ActionError,
				Calculated_Command_Parameters.StuckSection,
				byte4,
				byte5,
				byte6,
				byte7,
				CARD_MAILBOX_TX, RWLK_CAN_CardMessageID, 8);


}

/*****************************************************************************
 * Function name: RWLK_CAN_Send_XXX_Msg
 *
 * Description: Send specific CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Send_EncoderCurrentTest_Msg, "ramfuncs")
void RWLK_CAN_Send_EncoderCurrentTest_Msg(void)
{
//	u32 EncoderCurrentPositionUsigned = 0;
//	if(RWLK_CAN_CardNumber==0 || RWLK_CAN_CardNumber==2)
//	{
//	EncoderCurrentPosition = EncoderCurrentPosition - LowSoftwareLimit;
//	}
//	else if(RWLK_CAN_CardNumber==1 || RWLK_CAN_CardNumber==3)
//	{
//	EncoderCurrentPosition = EncoderCurrentPosition - HighSoftwareLimit;
//	}
//	EncoderCurrentPositionUsigned = (u32)EncoderCurrentPosition;
//  	ECanaMboxes.MBOX25.MDL.byte.BYTE0 = 0x6A;
//	ECanaMboxes.MBOX25.MDL.byte.BYTE2 = 0x00;
//	ECanaMboxes.MBOX25.MDL.byte.BYTE1 = 0x01;
//	ECanaMboxes.MBOX25.MDH.byte.BYTE4 = EncoderCurrentPositionUsigned & 0x00ff; // FeedbackPosition
//	ECanaMboxes.MBOX25.MDL.byte.BYTE3 = ((EncoderCurrentPositionUsigned >> 8) & 0x00ff);
//	ECanaMboxes.MBOX25.MDH.all = ECanaMboxes.MBOX25.MDH.all & 0xff000000;
//	ECanaMboxes.MBOX25.MDH.byte.BYTE5 = 0;
//	ECanaMboxes.MBOX25.MDH.byte.BYTE7 = CurrentSample;
//	ECanaMboxes.MBOX25.MDH.byte.BYTE6 = CurrentSample >> 8;
//
//	ECanaShadow.CANTRS.all = 0;
//		ECanaShadow.CANTRS.bit.TRS25 = 1;     // Set TRS for mailbox under test
//		ECanaRegs.CANTRS.all = ECanaShadow.CANTRS.all;
//	CAN_MessageTransmit(WRITE_MESSAGE_TO_LOG,
//				Calculated_Command_Parameters.stopalgomethod, //byte 1
//				Calculated_Command_Parameters.ActionError,
//				Calculated_Command_Parameters.StuckSection,
//				byte4,
//				byte5,
//				byte6,
//				byte7,
//				CARD_MAILBOX_TX, RWLK_CAN_CardMessageID, 8);
}

/*****************************************************************************
 * Function name: RWLK_CAN_Send_XXX_Msg
 *
 * Description: Send specific CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Send_EncoderError_Msg, "ramfuncs")
void RWLK_CAN_Send_EncoderError_Msg(void)
{

}

/*****************************************************************************
 * Function name: RWLK_CAN_Send_XXX_Msg
 *
 * Description: Send specific CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Send_HighCurrent_Msg, "ramfuncs")
void RWLK_CAN_Send_HighCurrent_Msg(void)
{

}

/*****************************************************************************
 * Function name: RWLK_CAN_Send_JointAngle_Msg
 *
 * Description: Send specific CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Send_JointAngle_Msg, "ramfuncs")
void RWLK_CAN_Send_JointAngle_Msg(void)
{
	int16_t byte3, byte4;

	byte4 = LOW_BYTE_MASK & Calculated_Command_Parameters.absenc_angSend;
	byte3 = (0x00ff & (Calculated_Command_Parameters.absenc_angSend >>= 8));

	CAN_MessageTransmit(RETURN_ABS_ENCODER_ANG,
						0,
						0,
						byte3,
						byte4,
						0,
						0,
						0,
						CARD_MAILBOX_TX, RWLK_CAN_CardMessageID, 8);

}

/*****************************************************************************
 * Function name: RWLK_CAN_Send_XXX_Msg
 *
 * Description: Send specific CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Send_MagnetLoss_Msg, "ramfuncs")
void RWLK_CAN_Send_MagnetLoss_Msg(void)
{

}

/*****************************************************************************
 * Function name: RWLK_CAN_Send_XXX_Msg
 *
 * Description: Send specific CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Send_PositionAck_Msg, "ramfuncs")
void RWLK_CAN_Send_PositionAck_Msg(void)
{
//	int16_t byte1, byte2, byte3, byte4, byte5, byte6, byte7;
//	u32 TempSendAckRequiredPosition = 0 ;
//	TempSendAckRequiredPosition = PositionAck ;
//
//	if(CARD_NUMBER==CardConfig_e_LEFT_HIP_Card || CARD_NUMBER==CardConfig_e_RIGHT_HIP_Card)
//	{
//		TempSendAckRequiredPosition   =  	TempSendAckRequiredPosition - LowSoftwareLimit;
//
//	}
//	else if(CARD_NUMBER==CardConfig_e_LEFT_KNEE_Card || CARD_NUMBER==CardConfig_e_RIGHT_KNEE_Card)
//	{
//		TempSendAckRequiredPosition 	 =  	TempSendAckRequiredPosition - HighSoftwareLimit;
//
//	}
//
//
//	ECanaMboxes.MBOX25.MDL.byte.BYTE3 =  TempSendAckRequiredPosition & 0x00ff;
//	ECanaMboxes.MBOX25.MDL.byte.BYTE2 = ((TempSendAckRequiredPosition >>= 8) & 0x00ff);;
//	ECanaMboxes.MBOX25.MDH.all = 0 ;
//	ECanaShadow.CANTRS.all = 0;
//  	ECanaShadow.CANTRS.bit.TRS25= 1;     // Set TRS for mailbox under test
//  	ECanaRegs.CANTRS.all = ECanaShadow.CANTRS.all;
//
//  	CAN_MessageTransmit(REQ_POSITION_ACK, 0, byte2, byte3, byte4, byte5, byte6, byte7, MAIL_BOX_1, RWLK_CAN_CardMessageID, 8);
}

/*****************************************************************************
 * Function name: RWLK_CAN_Send_ReadCalibrationLimits_Msg
 *
 * Description: Send specific CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Send_ReadCalibrationLimits_Msg, "ramfuncs")
void RWLK_CAN_Send_ReadCalibrationLimits_Msg(/*sMailBoxData *txData ?????????????? output can message*/)
{
	int16_t byte1, byte2, byte3, byte4, byte5, byte6, byte7;
	int16_t temp;

	// LSB
	byte2 = Calculated_Command_Parameters.CalibrationHighLimits & LOW_BYTE_MASK;
	// MSB
	byte1 = Calculated_Command_Parameters.CalibrationHighLimits >>= 8;
	// LSB
	byte4 = Calculated_Command_Parameters.CalibrationLowLimits & LOW_BYTE_MASK;
	// MSB
	byte3 = Calculated_Command_Parameters.CalibrationLowLimits >>= 8;
	byte5 = Calculated_Command_Parameters.FeedbackConversionFlag;

	temp = Calculated_Command_Parameters.HighSoftwareLimit - Calculated_Command_Parameters.LowSoftwareLimit;
	byte7 = temp & LOW_BYTE_MASK;
	temp >>= 8;
	byte6 = temp & LOW_BYTE_MASK;

	CAN_MessageTransmit(READ_LIMITS, byte1, byte2, byte3, byte4, byte5, byte6, byte7, CARD_MAILBOX_TX, RWLK_CAN_CardMessageID, 8);

}

/*****************************************************************************
 * Function name: RWLK_CAN_Send_XXX_Msg
 *
 * Description: Send specific CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Send_ReadCurrentAverage_Msg, "ramfuncs")
void RWLK_CAN_Send_ReadCurrentAverage_Msg(void)
{

}

/*****************************************************************************
 * Function name: RWLK_CAN_Send_RepeatAction_Msg
 *
 * Description: Send specific CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Send_RepeatAction_Msg, "ramfuncs")
void RWLK_CAN_Send_RepeatAction_Msg(void)
{
	CAN_MessageTransmit(REPEAT_LAST_ACTION, 0, 0, 0, 0, 0, 0, 0, CARD_MAILBOX_TX, RWLK_CAN_CardMessageID, 8);
}

/*****************************************************************************
 * Function name: RWLK_CAN_Send_SendAck_Msg
 *
 * Description: Send specific CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Send_SendAck_Msg, "ramfuncs")
void RWLK_CAN_Send_SendAck_Msg(void)
{
	int16_t byte1, byte2, byte3, byte4, byte5, byte6, byte7;

	/* Note: take the values for the acknowledge message from the Requested_Command_Parameters structure, since for move the acknowledge sent before the move is being process
	 * and the Calculated_Command_Parameters relevant values are not yet set.
	 */
	byte4 = Requested_Command_Parameters.DesiredPosition & LOW_BYTE_MASK;
	byte3 = (Requested_Command_Parameters.DesiredPosition >>= 8) & LOW_BYTE_MASK;
	byte2 = (Requested_Command_Parameters.DesiredPosition >>= 8) & LOW_BYTE_MASK;
	byte1 = (Requested_Command_Parameters.DesiredPosition >>= 8) & LOW_BYTE_MASK;
	byte7 = Requested_Command_Parameters.ProfileMaxVelocity  & LOW_BYTE_MASK;
	byte6 = (Requested_Command_Parameters.ProfileMaxVelocity >>= 8) & LOW_BYTE_MASK;
	byte5 = (Requested_Command_Parameters.ProfileMaxVelocity >>= 8) & LOW_BYTE_MASK;
	//byte5 = Calculated_Command_Parameters.RequiredMode;

	CAN_MessageTransmit(MOTOR_SEND_ACK, byte1, byte2, byte3, byte4, byte5, byte6, byte7, CARD_MAILBOX_TX, RWLK_CAN_CardMessageID, 8);
}

/*****************************************************************************
 * Function name: RWLK_CAN_Send_XIncrementalEncoderFeedback_Msg
 *
 * Description: Send specific CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Send_XIncrementalEncoderFeedback_Msg, "ramfuncs")
void RWLK_CAN_Send_XIncrementalEncoderFeedback_Msg(void)
{
	uint8_t ErrCode = 0;

	//ErrCode = IncEncoder_GetPosition(&IncEncFeedbackPosition);
	IncEncPos = sControlVarStruct.XMotorPosition;
	EncoderPositionParser( &IncEncPos, &sEncDataSend);
	LoadCellDataParser(med_ctr.my_struct_sensor_input.lc_pf , &sCellDataSend);
	CAN_MessageTransmit(GET_XINC_ENCODER_VALUE, ErrCode, sCellDataSend.TXCellData1, sCellDataSend.TXCellData0, sEncDataSend.TXEncoderData3, sEncDataSend.TXEncoderData2, sEncDataSend.TXEncoderData1, sEncDataSend.TXEncoderData0, 1, RWLK_CAN_CardMessageID, sizeof(ucTXMsgData));

}

/*****************************************************************************
 * Function name: RWLK_CAN_Send_YIncrementalEncoderFeedback_Msg
 *
 * Description: Send specific CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Send_YIncrementalEncoderFeedback_Msg, "ramfuncs")
void RWLK_CAN_Send_YIncrementalEncoderFeedback_Msg(void)
{
	uint8_t ErrCode = 0;

	//ErrCode = IncEncoder_GetPosition(&IncEncFeedbackPosition);
	IncEncPos = sControlVarStruct.YMotorPosition;
	EncoderPositionParser( &IncEncPos, &sEncDataSend);
	LoadCellDataParser(med_ctr.my_struct_sensor_input.lc_df , &sCellDataSend);
	//Send_GUI_Blue_Message_float_Par(READ_FEEDBACK_Y ,  (float)56.7);
	//Send_GUI_Blue_Message_float_Par(READ_FEEDBACK_Y ,  (float)32.89);

	CAN_MessageTransmit(GET_YINC_ENCODER_VALUE, ErrCode, sCellDataSend.TXCellData1, sCellDataSend.TXCellData0, sEncDataSend.TXEncoderData3, sEncDataSend.TXEncoderData2, sEncDataSend.TXEncoderData1, sEncDataSend.TXEncoderData0, 1, RWLK_CAN_CardMessageID, sizeof(ucTXMsgData));

}

/*****************************************************************************
 * Function name: RWLK_CAN_Send_AbsoluteEncoderFeedback_Msg
 *
 * Description: Send specific CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Send_AbsoluteEncoderFeedback_Msg, "ramfuncs")
void RWLK_CAN_Send_AbsoluteEncoderFeedback_Msg(void)
{
	uint8_t ErrCode = 0;

	//IncEncPos = sControlVarStruct.MotorPosition;
	EncoderPositionParser( &XAbsEncPos, &sEncDataSend);

	CAN_MessageTransmit(GET_XINC_ENCODER_VALUE, ErrCode, 0, 0, sEncDataSend.TXEncoderData3, sEncDataSend.TXEncoderData2, sEncDataSend.TXEncoderData1, sEncDataSend.TXEncoderData0, 1, RWLK_CAN_CardMessageID, sizeof(ucTXMsgData));

}

#pragma CODE_SECTION(RWLK_CAN_Send_XServoAnalyzerMeasurementPoints_Msg, "ramfuncs")
void RWLK_CAN_Send_XServoAnalyzerMeasurementPoints_Msg(void)
{
	int16_t byte1, byte2, byte3, byte4,byte5,byte6;
	int32_t Temp;
	Temp = X_sCpuToCla1ProfileCommandsStruct.PositionCommand - pToMoveVarStructsArray[X_Axis]->StartServo;
	byte3 = (uint8_t)(Temp & LOW_BYTE_MASK);
	byte2 = (uint8_t)((Temp>>=8) & LOW_BYTE_MASK);
	byte1 = (uint8_t)((Temp>>=8) & LOW_BYTE_MASK);
	Temp = sControlVarStruct.XMotorPosition - pToMoveVarStructsArray[X_Axis]->StartServo;
	byte6 = (uint8_t)(Temp & LOW_BYTE_MASK);
	byte5 = (uint8_t)((Temp >>=8) & LOW_BYTE_MASK);
	byte4 = (uint8_t)((Temp >>=8) & LOW_BYTE_MASK);
	CAN_MessageTransmit(X_SERVO_ANALYZER, byte1, byte2, byte3, byte4, byte5, byte6, 0, CARD_MAILBOX_TX, RWLK_CAN_CardMessageID, 8);

}


#pragma CODE_SECTION(RWLK_CAN_Send_YServoAnalyzerMeasurementPoints_Msg, "ramfuncs")
void RWLK_CAN_Send_YServoAnalyzerMeasurementPoints_Msg(void)
{
	int16_t byte1, byte2, byte3, byte4,byte5,byte6;
	int32_t Temp;
	Temp = Y_sCpuToCla1ProfileCommandsStruct.PositionCommand - pToMoveVarStructsArray[Y_Axis]->StartServo;
	byte3 = (uint8_t)(Temp & LOW_BYTE_MASK);
	byte2 = (uint8_t)((Temp>>=8) & LOW_BYTE_MASK);
	byte1 = (uint8_t)((Temp>>=8) & LOW_BYTE_MASK);
	Temp = sControlVarStruct.YMotorPosition - pToMoveVarStructsArray[Y_Axis]->StartServo;
	byte6 = (uint8_t)(Temp & LOW_BYTE_MASK);
	byte5 = (uint8_t)((Temp >>=8) & LOW_BYTE_MASK);
	byte4 = (uint8_t)((Temp >>=8) & LOW_BYTE_MASK);
	CAN_MessageTransmit(Y_SERVO_ANALYZER, byte1, byte2, byte3, byte4, byte5, byte6, 0, CARD_MAILBOX_TX, RWLK_CAN_CardMessageID, 8);
}

/*****************************************************************************
 * Function name: RWLK_CAN_Send_XXX_Msg
 *
 * Description: Send specific CAN BUS message. Test readiness
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Send_XServoOnFlag_Msg, "ramfuncs")
void RWLK_CAN_Send_XServoOnFlag_Msg(void)
{
	int16_t byte1;

	byte1 = Calculated_Command_Parameters.MotorOn;

	CAN_MessageTransmit(READY_FOR_MOVEMENT_TEST, byte1, 0, 0, 0, 0, 0, 0, CARD_MAILBOX_TX, RWLK_CAN_CardMessageID, 8);
}

/*****************************************************************************
 * Function name: RWLK_CAN_Send_XXX_Msg
 *
 * Description: Send specific CAN BUS message. Send the command TEST_COMPLETETD with test results
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Send_SystemTest_Msg, "ramfuncs")
void RWLK_CAN_Send_SystemTest_Msg(void)
{
	int16_t byte1, byte2, byte3, byte4, byte5, byte6, byte7;
	//uint16_t ProgramVersion;

	unsigned long int *Boot_Version_Ver = (unsigned long int *)BOOT_VERSION_ADDRESS;
	//ProgramVersion = PROG_VERSION;
  	//asm(" ESTOP0");

	// test result
	byte1 = (Calculated_Command_Parameters.TestResultByte & LOW_BYTE_MASK);
	// system ID
	byte2 = 0xC1; //changed to C1 New Hardware INCLUDING Graceful collapse, TODO: check what to do for this ? if to hold a variable, or hardware...
	byte3 = RWLK_CAN_CardNumber;
	// software version
	byte5 = (uint8_t)(Calculated_Command_Parameters.ProgramVersion & LOW_BYTE_MASK);
	byte4 = (uint8_t)((Calculated_Command_Parameters.ProgramVersion >>8) & LOW_BYTE_MASK);
	byte6 = (uint8_t)0x7;    // TODO: PROCESSOR;
	byte7 = (uint8_t)*Boot_Version_Ver;

  	CAN_MessageTransmit(TEST_COMPLETED, byte1, byte2, byte3, byte4 ,byte5 ,byte6 ,byte7, CARD_MAILBOX_TX, RWLK_CAN_CardMessageID, 8);
}

/*****************************************************************************
 * Function name: RWLK_CAN_CheckIfTransmitionInProcess
 *
 * Description: Send specific CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_CheckIfTransmitionInProcess, "ramfuncs")
bool RWLK_CAN_CheckIfTransmitionInProcess(void)
{
	// TODO: complete the logic before deciding tghe message was successfully sent.
	return false;
}


/*****************************************************************************
 * Function name: RWLK_CAN_Send_XXX_Msg
 *
 * Description: Send specific CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Send_GetMotorPhasesCurrents_Msg, "ramfuncs")
void RWLK_CAN_Send_GetMotorPhasesCurrents_Msg(void)
{

	MotorPhasesCurrentParser(&sControlVarStruct, &sMotorPhasesCurrentsSend, X_Axis);
	CAN_MessageTransmit(COMMAND_SEND_MOTOR_PHASES_CURRENTS, sMotorPhasesCurrentsSend.PhaseA_DataHigh, sMotorPhasesCurrentsSend.PhaseA_DataLow, sMotorPhasesCurrentsSend.PhaseB_DataHigh,sMotorPhasesCurrentsSend.PhaseB_DataLow ,sMotorPhasesCurrentsSend.PhaseC_DataHigh ,sMotorPhasesCurrentsSend.PhaseC_DataLow ,0, 1, CAN_LEFT_HIP_MSG_ID, sizeof(ucTXMsgData));
	//MotorPhasesCurrentParser(&sControlVarStruct, &sMotorPhasesCurrentsSend, Y_Axis);
	//CAN_MessageTransmit(COMMAND_SEND_MOTOR_PHASES_CURRENTS, sMotorPhasesCurrentsSend.PhaseA_DataHigh, sMotorPhasesCurrentsSend.PhaseA_DataLow, sMotorPhasesCurrentsSend.PhaseB_DataHigh,sMotorPhasesCurrentsSend.PhaseB_DataLow ,sMotorPhasesCurrentsSend.PhaseC_DataHigh ,sMotorPhasesCurrentsSend.PhaseC_DataLow ,0, 1, CAN_LEFT_HIP_MSG_ID, sizeof(ucTXMsgData));
}




/*****************************************************************************
 * Function name: RWLK_CAN_Send_XXX_Msg
 *
 * Description: Send specific CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Send_GetXDrv8031Registers_Msg, "ramfuncs")
void RWLK_CAN_Send_GetXDrv8031Registers_Msg(void)
{

	DRV8031_SPI_TxRx(&Drv8031DataReg1, &Drv8031DataReg2, X_Axis);
	DRV8031_ReadDataParser( &Drv8031DataReg1, &Drv8031DataReg2, &sDrv8031DataReadSend);
	CAN_MessageTransmit(XDRV_READ , sDrv8031DataReadSend.Reg1_DataHigh, sDrv8031DataReadSend.Reg1_DataLow, sDrv8031DataReadSend.Reg2_DataHigh,sDrv8031DataReadSend.Reg2_DataLow ,0 ,0 ,0, 1, RWLK_CAN_CardMessageID, sizeof(ucTXMsgData));

}




/*****************************************************************************
 * Function name: RWLK_CAN_Send_XXX_Msg
 *
 * Description: Send specific CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Send_GetYDrv8031Registers_Msg, "ramfuncs")
void RWLK_CAN_Send_GetYDrv8031Registers_Msg(void)
{

	DRV8031_SPI_TxRx(&Drv8031DataReg1, &Drv8031DataReg2, Y_Axis);
	DRV8031_ReadDataParser( &Drv8031DataReg1, &Drv8031DataReg2, &sDrv8031DataReadSend);
	CAN_MessageTransmit(YDRV_READ , sDrv8031DataReadSend.Reg1_DataHigh, sDrv8031DataReadSend.Reg1_DataLow, sDrv8031DataReadSend.Reg2_DataHigh,sDrv8031DataReadSend.Reg2_DataLow ,0 ,0 ,0, 1, RWLK_CAN_CardMessageID, sizeof(ucTXMsgData));

}

/*****************************************************************************
 * Function name: RWLK_CAN_Send_McuShuttingDown_Msg
 *
 * Description: Send specific CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Send_McuShuttingDown_Msg, "ramfuncs")
void RWLK_CAN_Send_McuShuttingDown_Msg (void)
{
	int16_t byte3, byte4;

	byte4 = Calculated_Command_Parameters.PowerStatus & LOW_BYTE_MASK;
	byte3 = Calculated_Command_Parameters.PowerStatus >> 8;

	CAN_MessageTransmit(MCU_SHUTTING_DOWN, 0, 0, byte3, byte4, 0, 0, 0, CARD_MAILBOX_TX, RWLK_CAN_CardMessageID, 8);
}

/*****************************************************************************
 * Function name: RWLK_CAN_Send_PowerRecovery
 *
 * Description: Send specific CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Send_PowerRecovery, "ramfuncs")
void RWLK_CAN_Send_PowerRecovery(void)
{
	int16_t byte3, byte4;

	byte4 = Calculated_Command_Parameters.PowerStatus & LOW_BYTE_MASK;
	byte3 = Calculated_Command_Parameters.PowerStatus >> 8;

	CAN_MessageTransmit(MCU_POWER_RECOVERY, 0, 0, byte3, byte4, 0, 0, 0, CARD_MAILBOX_TX, RWLK_CAN_CardMessageID, 8);

}

/*****************************************************************************
 * Function name: RWLK_CAN_Send_JointStatus_Msg
 *
 * Description: Send specific CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Send_JointStatus_Msg, "ramfuncs")
void RWLK_CAN_Send_JointStatus_Msg(void)
{

	uint32_t Temp;
	int16_t absenc_ang;
	uint16_t absenc_angSend;
	int16_t byte1, byte2, byte3, byte4, byte5, byte6, byte7;

	// Prepare the message to be sent, the calcaulated bytes only.

	// convert Time stamp to bytes 1-3
	Temp = Calculated_Command_Parameters.WorkTimer;
	byte3 = Temp & 0x000000ff;
	byte2 = (Temp >>= 8) & 0x000000ff;
	byte1 = (Temp >>= 8) & 0x000000ff;


	// Prepare Encoder value
	absenc_ang = Calculated_Command_Parameters.FeedbackPosition;
	if(RWLK_CAN_CardNumber==CardConfig_e_LEFT_HIP_Card || RWLK_CAN_CardNumber==CardConfig_e_RIGHT_HIP_Card)
	{
		absenc_ang = absenc_ang - Calculated_Command_Parameters.LowSoftwareLimit;
	}
	else if(RWLK_CAN_CardNumber==CardConfig_e_LEFT_KNEE_Card || RWLK_CAN_CardNumber==CardConfig_e_RIGHT_KNEE_Card)
	{
		absenc_ang = absenc_ang - Calculated_Command_Parameters.HighSoftwareLimit;
	}
	absenc_angSend = (uint16_t)absenc_ang ;
	byte5 =  absenc_angSend & LOW_BYTE_MASK;
	byte4 =  ((absenc_angSend >>= 8) & LOW_BYTE_MASK);

	// Prepare Current value

	byte7 = Calculated_Command_Parameters.MotorCurrent & LOW_BYTE_MASK;      // reach here
	byte6 = ((Calculated_Command_Parameters.MotorCurrent >> 8)	& LOW_BYTE_MASK);

	CAN_MessageTransmit(JOINT_STATE, byte1, byte2, byte3, byte4, byte5, byte6, byte7, CARD_MAILBOX_TX, RWLK_CAN_CardMessageID, 8);
}

/*****************************************************************************
 * Function name: RWLK_CAN_SendEncoderError_Msg
 *
 * Description: Send specific CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_SendEncoderError_Msg, "ramfuncs")
void RWLK_CAN_SendEncoderError_Msg(uint16_t ReasonOfFail)
{
	int16_t byte1, byte2;

	byte2 = ReasonOfFail&LOW_BYTE_MASK;
	byte1 = ((ReasonOfFail>>8)&LOW_BYTE_MASK);

  	CAN_MessageTransmit(ENCODER_ERROR, byte1, byte2, 0, 0, 0, 0, 0, CARD_MAILBOX_TX, RWLK_CAN_CardMessageID, 8);
}

/*****************************************************************************
 * Function name: RWLK_CAN_GetInOutCommandsParameters
 *
 * Description: Supply pointers to the input and output commands parameters structure
 * Parameters:  pointers to Requested_Command_Parameters and Calculated_Command_Parameters
 * Returns:     None.
 ******************************************************************************/
//void RWLK_CAN_GetInOutCommandsParameters(uint32_t *InParamsAddr, uint32_t *OutParamsAddr)
//{
//	InParamsAddr = (uint32_t *)&Requested_Command_Parameters;
//	OutParamsAddr = (uint32_t *)&Calculated_Command_Parameters;
//}


/*****************************************************************************
 * Function name: RWLK_CAN_Send_SwitchModeComplete_Msg
 *
 * Description: Send Acknowledge CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 * Note: This message will be activate for SWITCH MODE command only, after performing the switch mode.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Send_SwitchModeComplete_Msg, "ramfuncs")
void RWLK_CAN_Send_SwitchModeComplete_Msg(void)
{
	CAN_MessageTransmit(SWITCH_MODE, Calculated_Command_Parameters.ReceivedOperationalMode, 0, 0, 0, 0, 0, 0, CARD_MAILBOX_TX, RWLK_CAN_CardMessageID, 8);
	//GpioDataRegs.GPDTOGGLE.bit.GPIO104 = 1;
	//GpioDataRegs.GPDTOGGLE.bit.GPIO105 = 1;
}

/*****************************************************************************
 * Function name: RWLK_CAN_Send_MotorMoveMassage
 *
 * Description: Send Information about the Motor Move
 *
 * Parameters:  move - Motor move parameter
 *
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Send_MotorMoveMassage, "ramfuncs")
void RWLK_CAN_Send_MotorMoveMassage(int m_id, int pos, int move)
{
	CAN_MessageTransmit(m_id >> 8, m_id, pos >> 8, pos,	move >> 8, move,
						0, 0, CARD_MAILBOX_TX, RWLK_CAN_MSG_MOTOR_ID,6);
}


/*****************************************************************************
 * Function name: RWLK_CAN_Send_Sensor_Command
 *
 * Description: Send Sensor Command message
 * 				The sensor ID is determined by the Sensor module internal ID value
 *
 * Parameters:  SensorTx - struct of Tx message parmeters : ID, Length and 8 bytes of data.
 *
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Send_Sensor_Command, "ramfuncs")
uint16_t RWLK_CAN_Send_Sensor_Command(uint8_t Sensor_ID, CAN_SENSOR_MSG* SensorTx)
{
	uint16_t ErrCode = ERR_OK;
	if (Sensor_ID == LC1_ARRAY_CELL )
		Sensor_ID = COMMAND_LOADCELL1_ID;

//	if (Sensor_ID == LC2_ARRAY_CELL )
//		Sensor_ID = COMMAND_LOADCELL2_ID;

	if (Sensor_ID == IMU_RIGHT_ARRAY_CELL )
			Sensor_ID = COMMAND_IMU_RIGHT_ID;

	if (Sensor_ID == IMU_LEFT_ARRAY_CELL )
			Sensor_ID = COMMAND_IMU_LEFT_ID	;

	ErrCode = CAN_MessageTransmit( SensorTx->Data[0], SensorTx->Data[1], SensorTx->Data[2],
						 SensorTx->Data[3], SensorTx->Data[4], SensorTx->Data[5],
						 SensorTx->Data[6], SensorTx->Data[7],
						 CARD_MAILBOX_TX, SensorTx->ID | Sensor_ID, SensorTx->Length);
	return ErrCode;
}

#ifdef DEBUG_SYSTEM_STABILITY
/*****************************************************************************
 * Function name: SendSystemStabilityMessage
 *
 * Description: Send Acknowledge CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(SendSystemStabilityMessage, "ramfuncs")
void SendSystemStabilityMessage(void)
{
	static uint8_t stabilityCounter = 0;

	stabilityCounter++;
	if (stabilityCounter >= 250)
	{
		stabilityCounter = 0;
	}

	CAN_MessageTransmit(0x99, stabilityCounter, 2, 3, 4, 5, 6, 7, CARD_MAILBOX_TX, RWLK_CAN_CardMessageID, 8);
}
#endif

//#ifdef DEBUG_DEV_PHASE
/*****************************************************************************
 * Function name: RWLK_CAN_Decode_MotorHeatingThreshold_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  Msg_Data - struct of 8 bytes of data.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_MotorHeatingThreshold_Msg, "ramfuncs")
void RWLK_CAN_Decode_MotorHeatingThreshold_Msg(const CAN_MSG_DATA* Msg_Data)
{
	int16_t tempInt16;
	uint32_t tempUint32;

	Requested_Command_Parameters.ReceivedCommand = MOTOR_HEATING_THRESHOLD;
	// Read HeatingCurrentThreshold
	tempInt16 = Msg_Data->Byte1;
	tempInt16 <<= 8;
	Requested_Command_Parameters.HeatingCurrentThreshold = tempInt16 + Msg_Data->Byte2;
	// Read HeatCounterTHrshld
	tempUint32 = Msg_Data->Byte3;
	tempUint32 <<= 8;
	Requested_Command_Parameters.HeatCounterTHrshld = tempUint32 + Msg_Data->Byte4;

}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_SetIncEncoderValue_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  Msg_Data - struct of 8 bytes of data.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_SetIncEncoderValue_Msg, "ramfuncs")
void RWLK_CAN_Decode_SetIncEncoderValue_Msg(const CAN_MSG_DATA* Msg_Data)
{
	// A debug command, received through can value
	// TODO: Arie - what parameters are needed here ?
}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_SetAbsoluteEncoderAngle_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  Msg_Data - struct of 8 bytes of data.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_SetAbsoluteEncoderAngle_Msg, "ramfuncs")
void RWLK_CAN_Decode_SetAbsoluteEncoderAngle_Msg(const CAN_MSG_DATA* Msg_Data)
{
	// A debug command, received through can value
	// TODO: Arie - what parameters are needed here ?
}

/*****************************************************************************
 * Function name: RRWLK_CAN_Decode_Sensor_LC_Response
 *
 * Description: Process Income CAN BUS response command message from load-cell
 * 				1. Rising the income message flag
 * 				2. copy Rx buffer to Sensor income local struct
 *
 * 	Note: 		In case of income flag has already raised - Send the same Tx message back to the sensor.
 *
 * Parameters: CAN_Sensor_Command - struct of Rx message parmeters : ID, Length and 8 bytes of data.
 *
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_Sensor_LC_Response, "ramfuncs")
void RWLK_CAN_Decode_Sensor_LC_Response(CAN_SENSOR_MSG* CAN_Sensor_Command)
{
	if ( ( CAN_Sensor_Command->ID & 0x001 ) == 1 )
	{
		if ( RWLK_SensorData_GetSensorResponseFlage(LC1_ARRAY_CELL ) == 0 )
		{
			RWLK_SensorData_SetSensorResponseFlage(LC1_ARRAY_CELL , 1);
			RWLK_SensorData_CopySensorResponseBuffer(LC1_ARRAY_CELL , CAN_Sensor_Command);
		}else
		{
			CAN_Sensor_Command->ID = CAN_SENSOR_COMMAND_TX | (CAN_Sensor_Command->ID & 0x0F0);
			RWLK_CAN_Send_Sensor_Command(LC1_ARRAY_CELL, CAN_Sensor_Command);
		}
	}
//	if ( ( CAN_Sensor_Command->ID & 0x002 ) == 2 )
//	{
//		if ( RWLK_SensorData_GetSensorResponseFlage(LC2_ARRAY_CELL) == 0 )
//		{
//			RWLK_SensorData_SetSensorResponseFlage(LC2_ARRAY_CELL, 1);
//			RWLK_SensorData_CopySensorResponseBuffer(LC2_ARRAY_CELL, CAN_Sensor_Command);
//		}else
//		{
//			RWLK_CAN_Send_Sensor_Command(CAN_Sensor_Command->ID & 0x002, CAN_Sensor_Command);
//		}
//	}
}

/*****************************************************************************
 * Function name: RRWLK_CAN_Decode_Sensor_LC_Response
 *
 * Description: Process Income CAN BUS response command message from load-cell
 * 				1. Rising the income message flag
 * 				2. copy Rx buffer to Sensor income local struct
 *
 * 	Note: 		In case of income flag has already raised - Send the same Tx message back to the sensor.
 *
 * Parameters: CAN_Sensor_Command - struct of Rx message parmeters : ID, Length and 8 bytes of data.
 *
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_Sensor_IMU_Response, "ramfuncs")
void RWLK_CAN_Decode_Sensor_IMU_Response(CAN_SENSOR_MSG* CAN_Sensor_Command)
{
	if ( ( CAN_Sensor_Command->ID & 0x001 ) == 1 )
	{
		if ( RWLK_SensorData_GetSensorResponseFlage(IMU_RIGHT_ARRAY_CELL) == 0 )
		{
			RWLK_SensorData_SetSensorResponseFlage(IMU_RIGHT_ARRAY_CELL, 1);
			RWLK_SensorData_CopySensorResponseBuffer(IMU_RIGHT_ARRAY_CELL, CAN_Sensor_Command);
		}else
		{
			CAN_Sensor_Command->ID = CAN_SENSOR_COMMAND_TX | (CAN_Sensor_Command->ID & 0x0F0);
			RWLK_CAN_Send_Sensor_Command(IMU_RIGHT_ARRAY_CELL, CAN_Sensor_Command);
		}
	}
	if ( ( CAN_Sensor_Command->ID & 0x002 ) == 2 )
	{
		if ( RWLK_SensorData_GetSensorResponseFlage(IMU_LEFT_ARRAY_CELL) == 0 )
		{
			RWLK_SensorData_SetSensorResponseFlage(IMU_LEFT_ARRAY_CELL, 1);
			RWLK_SensorData_CopySensorResponseBuffer(IMU_LEFT_ARRAY_CELL, CAN_Sensor_Command);
		}else
		{
			CAN_Sensor_Command->ID = CAN_SENSOR_COMMAND_TX | (CAN_Sensor_Command->ID & 0x0F0);
			RWLK_CAN_Send_Sensor_Command(IMU_LEFT_ARRAY_CELL, CAN_Sensor_Command);
		}
	}
}

/*****************************************************************************
 * Function name: RWLK_CAN_Decode_Sensor_Reject
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  Msg_Data - struct of 8 bytes of data.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_CAN_Decode_Sensor_Reject, "ramfuncs")
void RWLK_CAN_Decode_Sensor_Reject(CAN_SENSOR_MSG* CAN_Sensor_Command)
{
	RWLK_SensorData_RejectHandler(CAN_Sensor_Command->ID);
}

/*****************************************************************************
 * Function name:RWLK_CAN_Set_MotorMove_MassageStatus
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  Msg_Data - struct of 8 bytes of data.
 * Returns:     None.
 ******************************************************************************/
void RWLK_CAN_Set_MotorMove_MassageStatus(const CAN_MSG_DATA* Msg_Data)
{
	RWLK_Set_Motor_Massages_Status(Msg_Data->Byte2);
}


//#endif

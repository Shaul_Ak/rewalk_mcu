/*
 * RWLK_CAN_Application.c
 *
 *  Created on: Jan 10, 2016
 *  Last Update: 18.4.2017  by Shaul : Add sensor receive handler
 *  Author: arie
 */
#include "DCL.h"
#include "RWLK.h"
#include "RWLK_Sensor_Data.h"
#include "RWLK_CLA_Control.h"
#include "RWLK_Motor_Control.h"
#include "RWLK_CAN_Application.h"
#include "RWLK_CAN_Msg_Handler.h"
#include "RWLK_Common.h"
#include "RWLK_Common_Par.h"
#include "../../RWLK_Periph_Driver/inc/RWLK_CAN_Driver.h"
#include "../../RWLK_Periph_Driver/inc/RWLK_EQep_Driver.h"
#include "../../RWLK_Periph_Driver/inc/RWLK_GPIO_Driver.h"
#include "../driverlib/can.h"
#include "RWLK_CardConfig.h"
#include "RWLK_ADC_Driver.h"
#include <stdio.h>
#include "RWLK_Timer_Driver.h"
#include "RWLK_GPIO_Driver.h"

#define ENCODER_NOT_CONNECTED	0xFFFF
#define MAGNET_LOSS_PROBLEM		0x00FF

uint16_t OutCAN_Msg_Flag = 0; // Need to set apropriate init value
uint16_t RWLK_CAN_CardConfigMsgId;
uint16_t RWLK_CAN_CardConfig;

//bool  ByPassEnable = false ;
//bool  TestSystemFlag ;
bool  FirstMoveafterBoot ;
bool  AllMotorsFlag ;
uint32_t CurrentOperationMode ;  // Mode parameter in old MCU program
int32_t IncEncFeedbackPosition = 0;


#pragma DATA_SECTION(RxMailBoxData,"ramdata")
sMailBoxData RxMailBoxData;
#pragma DATA_SECTION(TxMailBoxData,"ramdata")
sMailBoxData TxMailBoxData;
#pragma DATA_SECTION(TxArbitrator,"ramdata")
sTransmitFlags TxArbitrator = {0}; // TxArbitrator

//--------------------------- Messages Identifier -----------------------------//
#pragma DATA_SECTION(Can_Message_Data,"ramdata")
static CAN_MSG_DATA Can_Message_Data;
static CAN_SENSOR_MSG CAN_Sensor_Command;
//#ifdef DEBUG_CAN_TX
//extern Output_Command_Parameters_Type Calculated_Command_Parameters;
//#endif

/*==============================================================================
Function : RWLK_CAN_Application_Init

Description 		: This Fucntion initialize the needed module variables

inputs 				: None
outputs				: None
==============================================================================*/
void RWLK_CAN_Application_Init(void)
{
	RxMailBoxData.MODE = NO_REQUEST;
	RxMailBoxData.REQUIRED_MODE = NO_REQUEST;

	RWLK_CAN_CardConfigMsgId = CardConfigGetMessageID();
	RWLK_CAN_CardConfig = CardConfigGetCardNumber();

#ifdef DEBUG_SYSTEM_STABILITY
  TxArbitrator.TransmitSystemStabilityMessage = true;
#endif

}


/*==============================================================================
 *
Function : Read CANBUS Message

Description 		: This Function will allow read CANBUS Message

inputs 				: uint16_t MailBox, which mailbox required to be read

==============================================================================*/
#pragma CODE_SECTION(CAN_ReadMessage, "ramfuncs")
bool CAN_ReadMessage(uint16_t MailBox)
{
	uint16_t i =0;
	
	RWLK_CanReceiveDriver(CANA_BASE, MailBox, &sCANMsgObjectPool[MailBox], true);

	if((sCANMsgObjectPool[MailBox].ui32Flags & MSG_OBJ_NEW_DATA) == MSG_OBJ_NEW_DATA)
	{
		
		for(i=0;i<=7;i++)
		{
			ucTXMsgData[i] = ucRXMsgData[i];
		}
		return true;
	}
	return false;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Shaul 18.4.2017: ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

/*==============================================================================
*
* Function : CAN_SensorReadMessage
*
* Description 		: This Function reads CANBUS Sensor data mailboxes,
*					   if Message if exist the function call SensorData parser function.
*
* Inputs 				: None.
*
==============================================================================*/
#pragma CODE_SECTION(CAN_MessageReceive, "ramfuncs")
bool CAN_SensorReadMessage(void)
{
	bool MESSAGE_EXIST;
	uint32_t i;

//	FlagSens_Out_Toggle();
	for ( i = CARD_MAILBOX_IMUR_0_RX; i <= CARD_MAILBOX_LC2_RX; i++)
	{
		MESSAGE_EXIST = CAN_ReadMessage(i);
		if(MESSAGE_EXIST)
		{
			//CAN_SensorsMessageReceive(CARD_MAILBOX_IMU_RX, &ucRXMsgData[0]);
			RWLK_SensorData_Parser(sCANMsgObjectPool[i].ui32MsgID, &ucRXMsgData[0]);
			AllMotorsFlag = false;
		}
	}
//	FlagSens_Out_Toggle();
	return MESSAGE_EXIST;
}

/*==============================================================================
*
* Function : CAN_SensorReadResponse
*
* Description 		: This Function reads response to sensor command mailboxes,
*					   if response to exist the function call SensorsMessageReceive function.
*
* Inputs 				: None.
*
==============================================================================*/
#pragma CODE_SECTION(CAN_SensorReadResponse, "ramfuncs")
bool CAN_SensorReadResponse(void)
{
	bool MESSAGE_EXIST;
	uint32_t i;

	for ( i = CARD_MAILBOX_COMMAND_RX_LC; i <= CARD_MAILBOX_COMMAND_REJECT; i++)
	{
		MESSAGE_EXIST = CAN_ReadMessage(i);
		if(MESSAGE_EXIST)
		{
			CAN_SensorsMessageReceive(i);
			AllMotorsFlag = false;
		}
	}

	return MESSAGE_EXIST;
}


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

/*==============================================================================
 *
Function :  CAN_Protocol execution

Description 		: This Function will  read CANBUS Message if exist and will
					  parser received message

inputs 				: no inputs
outputs				: no outputs

==============================================================================*/
#pragma CODE_SECTION(CAN_Input, "ramfuncs")
void CAN_Input(void)
{

	bool MESSAGE_EXIST = false;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Shaul 18.4.2017: ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	MESSAGE_EXIST = CAN_SensorReadMessage();
	MESSAGE_EXIST = CAN_SensorReadResponse();
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	if (!MESSAGE_EXIST)
	{
		MESSAGE_EXIST = CAN_ReadMessage(CARD_MAILBOX_RX);

		if(MESSAGE_EXIST)
		{

			CAN_MessageReceive(CARD_MAILBOX_RX, &ucRXMsgData[0]);
			AllMotorsFlag = false;
		}
		else
		{
			MESSAGE_EXIST = CAN_ReadMessage(CARD_MAILBOX_BROADCAST_RX);

			if(MESSAGE_EXIST)
			{
				CAN_MessageReceive(CARD_MAILBOX_BROADCAST_RX, &ucRXMsgData[0]);
				AllMotorsFlag = false;
			}
		}
	}
	// Note: Leave this line outside the "if(MESSAGE_EXIST)", to allow the cases of sending ack first and than the relvevant respond.	
	ProtocolParser(&AllMotorsFlag, &RxMailBoxData, &TxArbitrator);


#ifdef DEBUG_CAN_TX
	TransmitFlags.EncoderErrorFlag = true;
#endif


	// move to main
//	RWLK_CAN_TxMsg_Handler(&TransmitFlags);

}



/*==============================================================================
 *
Function : CAN_MessageReceive

Description 		: This Fucntion will  read CANBUS Message if exist and will parser received message

inputs 				: uint16_t MailBox, which mailbox required to be read
					: unsigned char *sRxMessageData, which is pointer to data array

==============================================================================*/
#pragma CODE_SECTION(CAN_MessageReceive, "ramfuncs")
void CAN_MessageReceive(uint16_t MailBox, unsigned char *sRxMessageData)
{
	RxMailBoxData.MODE = CHECK_REQUIRED_ACK;
	RxMailBoxData.REQUIRED_MODE =  sRxMessageData[0];
	RxMailBoxData.RXMsgDataByte1 = sRxMessageData[1];
	RxMailBoxData.RXMsgDataByte2 = sRxMessageData[2];
	RxMailBoxData.RXMsgDataByte3 = sRxMessageData[3];
	RxMailBoxData.RXMsgDataByte4 = sRxMessageData[4];
	RxMailBoxData.RXMsgDataByte5 = sRxMessageData[5];
	RxMailBoxData.RXMsgDataByte6 = sRxMessageData[6];
	RxMailBoxData.RXMsgDataByte7 = sRxMessageData[7];

}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Shaul 18.4.2017: ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#ifdef SENSOR_MODE

/*==============================================================================
 *
Function : CAN_SensorsMessageReceive

Description 		: This Function parses the received message
					the mode is determined from the message ID value

inputs 				: uint16_t MailBox: which mailbox required to be read

==============================================================================*/
#pragma CODE_SECTION(CAN_MessageReceive, "ramfuncs")
void CAN_SensorsMessageReceive(uint16_t MailBox)
{
	CAN_Sensor_Command.ID  	    = sCANMsgObjectPool[MailBox].ui32MsgID;
	CAN_Sensor_Command.Length   = sCANMsgObjectPool[MailBox].ui32MsgLen;
	memcpy(CAN_Sensor_Command.Data, ucRXMsgData, sizeof(uint8_t) * CAN_Sensor_Command.Length );

	RxMailBoxData.MODE = CAN_Sensor_Command.ID & 0xF0C;
	ProtocolParser(&AllMotorsFlag, &RxMailBoxData, &TxArbitrator);
}

#endif  // SENSOR_MODE
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


/*==============================================================================
 *
Function : SendXIncrementalEncoderPosition

Description 		: This Fucntion will  allow incremental encoder sending

inputs 				: *TxArbitratorFlags pointer to struct _Transmit_Flags , which point to transmit flags structure


==============================================================================*/
#pragma CODE_SECTION(SendXIncrementalEncoderPosition, "ramfuncs")
void SendXIncrementalEncoderPosition(struct _Transmit_Flags *TxArbitratorFlags)
{
	TxArbitratorFlags -> SendXIncrementalEncoderFeedbackFlag = true ;
}

/*==============================================================================
 *
Function : SendYIncrementalEncoderPosition

Description 		: This Fucntion will  allow incremental encoder sending

inputs 				: *TxArbitratorFlags pointer to struct _Transmit_Flags , which point to transmit flags structure


==============================================================================*/
#pragma CODE_SECTION(SendYIncrementalEncoderPosition, "ramfuncs")
void SendYIncrementalEncoderPosition(struct _Transmit_Flags *TxArbitratorFlags)
{
	TxArbitratorFlags -> SendYIncrementalEncoderFeedbackFlag = true ;
}

/*==============================================================================
 *
Function : SendAbsoluteEncoderPosition

Description 		: This Fucntion will  allow absolute encoder sending

inputs 				: *TxArbitratorFlags pointer to struct _Transmit_Flags , which point to transmit flags structure


==============================================================================*/
#pragma CODE_SECTION(SendAbsoluteEncoderPosition, "ramfuncs")
void SendAbsoluteEncoderPosition(struct _Transmit_Flags *TxArbitratorFlags)
{
	TxArbitratorFlags -> SendAbsoluteEncoderFeedbackFlag = true ;
}
/*==============================================================================
 *
Function : SendMotorPhasesCurrents

Description 		: This Fucntion will  allow motor phases currents sending

inputs 				: *TxArbitratorFlags pointer to struct _Transmit_Flags , which point to transmit flags structure


==============================================================================*/
#pragma CODE_SECTION(SendMotorPhasesCurrents, "ramfuncs")
void SendMotorPhasesCurrents(struct _Transmit_Flags *TxArbitratorFlags)
{
	TxArbitratorFlags -> SendMotorPhasesCurrentsFlag = true ;
}


//u8 CAN_TransmitMessage( u8 MailBox, u16 MessageID, u8 MessageDataLength, u8 MessageData[] ); //Removed BY Saman Space
/*
** ===================================================================
**     Function   :  CAN_MessageTransmit
**
**     Description :
**					Transmit message from the defined mail box
**     Parameters  :
**					u8  MailBox           - Mail box number. Possible values 0...31;
**                  u16 MessageID         - the Message ID;
**					u8 MessageDataLength  - the message data lenth. Possible values 0...8;
**					u8 MessageData        - 8-bytes array. First MessageDataLength bytes
**                                           of this array contain the message data to be trancmitted
**     Returns     :
**					Error code. Possible values:
**                      - ERR_OK,
**                      - ERR_CAN_RANGE_MAIL_BOX_NO,
**                      - ERR_CAN_RANGE_MESS_LENGTH,
**                      - ERR_CAN_MESSAGE_PENDING,  (Mail box contains a message was not read)
**                      - ERR_CAN_CANBUS_CORRUPTED. (Message was not acknowledged)
**     Notes       :
**
** ===================================================================
*/
#pragma CODE_SECTION(CAN_MessageTransmit, "ramfuncs")
uint16_t CAN_MessageTransmit(uint16_t Action, uint16_t MessageByte1, int16_t MessageByte2, int16_t MessageByte3, int16_t MessageByte4, int16_t MessageByte5, int16_t MessageByte6, int16_t MessageByte7, uint16_t MailBox, uint32_t MessageID, uint32_t MessageDataLength)
{

	uint16_t ErrCode = ERR_OK;
	if ( RWLK_IsCanTransmit(CANA_BASE) )
	return ERR_TX_BUSY;

	ucTXMsgData[0] = Action;
	ucTXMsgData[1] = MessageByte1;
	ucTXMsgData[2] = MessageByte2;
	ucTXMsgData[3] = MessageByte3;
	ucTXMsgData[4] = MessageByte4;
	ucTXMsgData[5] = MessageByte5;
	ucTXMsgData[6] = MessageByte6;
	ucTXMsgData[7] = MessageByte7;

	InitMsgObj(MessageID, 0, 0, MessageDataLength, CAN_OBJECT_Tx_TYPE,MailBox);
	RWLK_CanTransmitDriver(CANA_BASE, MailBox, &sCANMsgObjectPool[MailBox]);
	ClearMessageObject(CAN_OBJECT_Tx_TYPE);

	return ErrCode;

}

/*==============================================================================
 *
Function : ProtocolParser

Description 		: This Fucntion will parse received message

inputs 				: struct _MAILBOX_DATA *sMailBoxDataPtr, which is pointer to mailbox  received message structure
==============================================================================*/
#pragma CODE_SECTION(ProtocolParser, "ramfuncs")
void ProtocolParser(bool *MOTOR_FLAGS, struct _MAILBOX_DATA *sMailBoxDataPtr, struct _Transmit_Flags *TxArbitratorFlags)
{


	switch(sMailBoxDataPtr->MODE)
	{
		case (CHECK_REQUIRED_ACK):
			// Read from the mail box into the Can_Message_Data
			Can_Message_Data.Byte0 = sMailBoxDataPtr->REQUIRED_MODE;
			Can_Message_Data.Byte1 = sMailBoxDataPtr->RXMsgDataByte1;
			Can_Message_Data.Byte2 = sMailBoxDataPtr->RXMsgDataByte2;
			Can_Message_Data.Byte3 = sMailBoxDataPtr->RXMsgDataByte3;
			Can_Message_Data.Byte4 = sMailBoxDataPtr->RXMsgDataByte4;
			Can_Message_Data.Byte5 = sMailBoxDataPtr->RXMsgDataByte5;
			Can_Message_Data.Byte6 = sMailBoxDataPtr->RXMsgDataByte6;
			Can_Message_Data.Byte7 = sMailBoxDataPtr->RXMsgDataByte7;
			if((sMailBoxDataPtr -> REQUIRED_MODE == X_MOVE)&&(!*MOTOR_FLAGS))
			{
				// Call this function here, so we can send the acknowledge message already with the needed parameters.
				RWLK_CAN_Decode_XAxisMove_Msg(&Can_Message_Data);
				TxArbitratorFlags -> SendAckFlag = true ;
			}
			else if((sMailBoxDataPtr -> REQUIRED_MODE == Y_MOVE)&&(!*MOTOR_FLAGS))
			{
				RWLK_CAN_Decode_YAxisMove_Msg(&Can_Message_Data);
				TxArbitratorFlags -> SendAckFlag = true ;
			}

			sMailBoxDataPtr -> MODE = sMailBoxDataPtr -> REQUIRED_MODE;
			break;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Shaul 18.4.2017: ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	#ifdef SENSOR_MODE
		case (CHECK_SENSOR_REQUIRED_ACK):
			// Read from the mail box into the Can_Message_Data
			Can_Message_Data.Byte0 = sMailBoxDataPtr->RXMsgDataByte0;
			Can_Message_Data.Byte1 = sMailBoxDataPtr->RXMsgDataByte1;
			Can_Message_Data.Byte2 = sMailBoxDataPtr->RXMsgDataByte2;
			Can_Message_Data.Byte3 = sMailBoxDataPtr->RXMsgDataByte3;
			Can_Message_Data.Byte4 = sMailBoxDataPtr->RXMsgDataByte4;
			Can_Message_Data.Byte5 = sMailBoxDataPtr->RXMsgDataByte5;
			Can_Message_Data.Byte6 = sMailBoxDataPtr->RXMsgDataByte6;
			Can_Message_Data.Byte7 = sMailBoxDataPtr->RXMsgDataByte7;

			sMailBoxDataPtr -> MODE =  CAN_PROTOCOL_IMU;
			break;
	#endif  // SENSOR_MODE
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ YehudaB 20.4.2016: ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	//	case (CHECK_REQUIRED_ACK):
	//		if((sMailBoxDataPtr -> REQUIRED_MODE == MOVE)&&(!*MOTOR_FLAGS))
	//		{
		//		Can_Message_Data.Byte1 = sMailBoxDataPtr->RXMsgDataByte1;
	//			Can_Message_Data.Byte2 = sMailBoxDataPtr->RXMsgDataByte2;
	//			Can_Message_Data.Byte3 = sMailBoxDataPtr->RXMsgDataByte3;
	//			Can_Message_Data.Byte4 = sMailBoxDataPtr->RXMsgDataByte4;
	//			Can_Message_Data.Byte5 = sMailBoxDataPtr->RXMsgDataByte5;
	//			Can_Message_Data.Byte6 = sMailBoxDataPtr->RXMsgDataByte6;
	//			Can_Message_Data.Byte7 = sMailBoxDataPtr->RXMsgDataByte7;
	//			RWLK_CAN_Process_CheckRequiredAck_Msg(Can_Message_Data);
	//		}
	//		sMailBoxDataPtr -> MODE = sMailBoxDataPtr -> REQUIRED_MODE;
	//		break;

		case(JUMP_TO_BOOT):
		// jump_to_boot_app(UPGRADE_REQUEST);
			RWLK_CAN_Decode_JumpToBoot_Msg();
			sMailBoxDataPtr -> MODE = NO_REQUEST;
			break;

	/*	case (ByPassMode):
			RWLK_CAN_Decode_ByPassMode_Msg(&Can_Message_Data);
		    sMailBoxDataPtr->MODE = NO_REQUEST;
		    break;*/

		case(JOINT_STATE):
		    RWLK_CAN_Decode_JointState_Msg();
			sMailBoxDataPtr -> MODE = NO_REQUEST;
			break;

		case(X_MOVE):
			// Note: the decode move already been called in the CHECK_REQUIRED_ACK case, therefor no need to activate it here.
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;

		case(Y_MOVE):
			// Note: the decode move already been called in the CHECK_REQUIRED_ACK case, therefor no need to activate it here.
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;

		case(GET_XINC_ENCODER_VALUE):
			RWLK_CAN_Decode_GetXIncEncoderValue_Msg();
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;

		case(GET_YINC_ENCODER_VALUE):
			RWLK_CAN_Decode_GetYIncEncoderValue_Msg();
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;

		case(GET_XABS_ENCODER_ANG):
			RWLK_CAN_Decode_GetAbsEncoderAngle_Msg();
			sMailBoxDataPtr->MODE = NO_REQUEST;


			//SendAbsoluteEncoderPosition(TxArbitratorFlags);
			//sMailBoxDataPtr->MODE = NO_REQUEST;
			break;

	/*	case(EncoderTest):
//			if (sMailBoxDataPtr->MODE != _MANUAL && FirstMoveafterBoot == 1)
//			{
//				Can_Message_Data.Byte1 = sMailBoxDataPtr->RXMsgDataByte1;
//				Can_Message_Data.Byte2 = sMailBoxDataPtr->RXMsgDataByte2; // Falling detection bit
//				Can_Message_Data.Byte3 = sMailBoxDataPtr->RXMsgDataByte3; // Angle in degrees
//				Can_Message_Data.Byte4 = sMailBoxDataPtr->RXMsgDataByte4; // Angle High
//				Can_Message_Data.Byte5 = sMailBoxDataPtr->RXMsgDataByte5; // Angle Low
//				Can_Message_Data.Byte6 = sMailBoxDataPtr->RXMsgDataByte6; // Velocity High
//				Can_Message_Data.Byte7 = sMailBoxDataPtr->RXMsgDataByte7; // Velocity Low
//				RWLK_CAN_Process_EncoderTest_Msg(Can_Message_Data);
//			}
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;*/

		case(ACTION_COMPLETED):
			RWLK_CAN_Decode_ActionCompleted_Msg();
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;

		case(ReadCurrentAverage):
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;

		case(READY_FOR_MOVEMENT_TEST):
			RWLK_CAN_Decode_ReadyForMovement_Msg();
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;

		case(ABS_ENCODER_PROBLEM):
			RWLK_CAN_Decode_AbsoluteEncoderProblem_Msg();
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;

		case(ENCODER_PROBLEM_END):
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;

		case(X_STOP_MOTOR):
		    RWLK_CAN_Decode_XStop_Motor_Msg();
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;

		case(Y_STOP_MOTOR):
		    RWLK_CAN_Decode_YStop_Motor_Msg();
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;

	/*	case(SET_PID ):
			RWLK_CAN_Decode_SetPid_Msg(&Can_Message_Data);
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;
	 */
		case(HIGH_FRICTION):
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;

		case(CHNG_PE_THRSHLD):
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;

		case(RETURN_STATUS ):
			RWLK_CAN_Decode_ReturnStatus_Msg(&Can_Message_Data);
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;

		case(READ_LIMITS ):
			RWLK_CAN_Decode_ReadLimits_Msg();
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;

		case(SWITCH_MODE ):
		    RWLK_CAN_Decode_SwitchMode_Msg(&Can_Message_Data);
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;


		case(CURRENT_TRSH ): // leave the new command 0x63 and remove the old command 0x27 from the IDD

			RWLK_CAN_Decode_CurrentTrsh_Msg(&Can_Message_Data);
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;


		case(IAM_ALIVE):
			RWLK_CAN_Decode_IamAlive_Msg(&Can_Message_Data);
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;

		case(LogMode):
			RWLK_CAN_Decode_LogMode_Msg(&Can_Message_Data);
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;

		case(ACC_FACTOR ):
			RWLK_CAN_Decode_AccFactor_Msg(&Can_Message_Data);
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;

		case(WRITE_SAMPLING_TIME ):
		    RWLK_CAN_Decode_WriteSamplingTime_Msg(&Can_Message_Data);
		    sMailBoxDataPtr->MODE = NO_REQUEST;
		    break;

		case(READ_SAMPLING_TIME ):
		    RWLK_CAN_Decode_ReadSamplingTime_Msg(&Can_Message_Data);
		    sMailBoxDataPtr->MODE = NO_REQUEST;
		    break;

		case(X_MOTOR_ENABLE ):
		    RWLK_CAN_Decode_XMotorEnable_Msg();
		    sMailBoxDataPtr->MODE = NO_REQUEST;
		    break;

		case(X_MOTOR_DISABLE ):
		    RWLK_CAN_Decode_XMotorDisable_Msg();
		    sMailBoxDataPtr->MODE = NO_REQUEST;
		    break;

		case(X_RELEASE_MOTOR ):
		    RWLK_CAN_Decode_ReleaseXMotor_Msg();
		    sMailBoxDataPtr->MODE = NO_REQUEST;
		    break;

		case(X_UNRELEASE_MOTOR ):
		    RWLK_CAN_Decode_UnreleaseXMotor_Msg();
		    sMailBoxDataPtr->MODE = NO_REQUEST;
		    break;

		case(X_MOVE_OPEN_LOOP ):
			RWLK_CAN_Decode_XAxisMoveOpenLoop_Msg(&Can_Message_Data);
		    sMailBoxDataPtr->MODE = NO_REQUEST;
		    break;

		case(Y_MOTOR_ENABLE ):
		    RWLK_CAN_Decode_YMotorEnable_Msg();
		    sMailBoxDataPtr->MODE = NO_REQUEST;
		    break;

		case(Y_MOTOR_DISABLE ):
		    RWLK_CAN_Decode_YMotorDisable_Msg();
		    sMailBoxDataPtr->MODE = NO_REQUEST;
		    break;

		case(Y_RELEASE_MOTOR ):
		    RWLK_CAN_Decode_ReleaseYMotor_Msg();
		    sMailBoxDataPtr->MODE = NO_REQUEST;
		    break;

		case(Y_UNRELEASE_MOTOR ):
		    RWLK_CAN_Decode_UnreleaseYMotor_Msg();
		    sMailBoxDataPtr->MODE = NO_REQUEST;
		    break;

		case(Y_MOVE_OPEN_LOOP ):
			RWLK_CAN_Decode_YAxisMoveOpenLoop_Msg(&Can_Message_Data);
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;

		case(FILTER_PAR ):
		    RWLK_CAN_Decode_FilterParams_Msg(&Can_Message_Data);
		    sMailBoxDataPtr->MODE = NO_REQUEST;
		    break;

		case(FLASH_STORE ):
			RWLK_CAN_Decode_FlashStore_Msg(&Can_Message_Data);
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;

		case(SET_HWLIMITS ):
			RWLK_CAN_Decode_SetHWLimits_Msg(&Can_Message_Data);
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;

		case(AUTO_CALIBRATION ):
			RWLK_CAN_Decode_AutoCalibration_Msg(&Can_Message_Data);
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;

		case(PERFORM_SYSTEM_TESTING ):
			RWLK_CAN_Decode_PerformSystemTesting_Msg();
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;

		case(RUNTESTFLAG ):
			RWLK_CAN_Decode_RunTestFlag_Msg();
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;

		case(CHANGE_XPWM):
			RWLK_CAN_Decode_ChangeXPwmCommand_Msg(&Can_Message_Data);
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;

		case(CHANGE_YPWM):
			RWLK_CAN_Decode_ChangeYPwmCommand_Msg(&Can_Message_Data);
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;

		case(X_COMMUT_REQUEST):
			RWLK_CAN_Decode_XCommutationSetRequest_Msg(&Can_Message_Data);
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;

		case(Y_COMMUT_REQUEST):
			RWLK_CAN_Decode_YCommutationSetRequest_Msg(&Can_Message_Data);
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;

		case(X_POSITION_ADJUSTER):
				// Set ADJUST member in byte 1
				Can_Message_Data.Byte1 = POS_ADJUSTER_REQUEST;
				RWLK_CAN_Decode_AdjusterRequest_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(X_VELOCITY_ADJUSTER):
				// Set ADJUST member in byte 1
				Can_Message_Data.Byte1 = VEL_ADJUSTER_REQUEST;
				RWLK_CAN_Decode_AdjusterRequest_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(X_CURRENT_ADJUSTER):
				//RWLK_CAN_Process_CurrentAdjusterRequest_Msg(&AdjustingPeriod,&AdjustingCurrent, sMailBoxDataPtr);
				// Set ADJUST member in byte 1
				Can_Message_Data.Byte1 = CUR_ADJUSTER_REQUEST;
				RWLK_CAN_Decode_AdjusterRequest_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(X_STOP_ADJUSTER):
				//RWLK_CAN_Process_CurrentAdjusterRequest_Msg(&AdjustingPeriod,&AdjustingCurrent, sMailBoxDataPtr);
				// Set ADJUST member in byte 1
				Can_Message_Data.Byte1 = STOP_ADJUSTER_REQUEST;
				RWLK_CAN_Decode_AdjusterRequest_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(Y_POSITION_ADJUSTER):
				// Set ADJUST member in byte 1
				Can_Message_Data.Byte1 = POS_ADJUSTER_REQUEST;
				RWLK_CAN_Decode_AdjusterRequest_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(Y_VELOCITY_ADJUSTER):
				// Set ADJUST member in byte 1
				Can_Message_Data.Byte1 = VEL_ADJUSTER_REQUEST;
				RWLK_CAN_Decode_AdjusterRequest_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(Y_CURRENT_ADJUSTER):
				//RWLK_CAN_Process_CurrentAdjusterRequest_Msg(&AdjustingPeriod,&AdjustingCurrent, sMailBoxDataPtr);
				// Set ADJUST member in byte 1
				Can_Message_Data.Byte1 = CUR_ADJUSTER_REQUEST;
				RWLK_CAN_Decode_AdjusterRequest_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(Y_STOP_ADJUSTER):
				//RWLK_CAN_Process_CurrentAdjusterRequest_Msg(&AdjustingPeriod,&AdjustingCurrent, sMailBoxDataPtr);
				// Set ADJUST member in byte 1
				Can_Message_Data.Byte1 = STOP_ADJUSTER_REQUEST;
				RWLK_CAN_Decode_AdjusterRequest_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

// TODO: orit - split according to new design	20/12
		case(Y_SERVO_ANALYZER):
				Can_Message_Data.Byte1 = SERVO_ANALIZER_REQUEST;
				RWLK_CAN_Decode_AdjusterRequest_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;
	// TODO: orit - split according to new design	20/12
		case(X_SERVO_ANALYZER):
				//RWLK_CAN_Process_CurrentAdjusterRequest_Msg(&AdjustingPeriod,&AdjustingCurrent, sMailBoxDataPtr);
				// Set ADJUST member in byte 1
				Can_Message_Data.Byte1 = SERVO_ANALIZER_REQUEST;
				RWLK_CAN_Decode_AdjusterRequest_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;
//				Can_Message_Data.Byte1 = sMailBoxDataPtr->RXMsgDataByte1;
//				Can_Message_Data.Byte2 = sMailBoxDataPtr->RXMsgDataByte2;
//				Can_Message_Data.Byte3 = sMailBoxDataPtr->RXMsgDataByte3;
//				Can_Message_Data.Byte4 = sMailBoxDataPtr->RXMsgDataByte4;
//				Can_Message_Data.Byte5 = sMailBoxDataPtr->RXMsgDataByte5;
//				RWLK_CAN_Process_ServoAnalizerStart_Msg(Can_Message_Data);
//				break;

		case(XSCLKP):
				// Set filter member in byte 3
				Can_Message_Data.Byte3 = KP;
				RWLK_CAN_Decode_ServoCurrentLoop_Msg(&Can_Message_Data);
				//RWLK_CAN_Process_SetCurrentPiGains_Msg(Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;


		case(XSCLI10):
				// Set filter member in byte 3
				Can_Message_Data.Byte3 = I10;
				RWLK_CAN_Decode_ServoCurrentLoop_Msg(&Can_Message_Data);
//				Can_Message_Data.Byte1 = sMailBoxDataPtr->RXMsgDataByte1;
//				Can_Message_Data.Byte2 = sMailBoxDataPtr->RXMsgDataByte2;
//				Can_Message_Data.Byte3 = I6;
//				RWLK_CAN_Process_SetCurrentPiGains_Msg(Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(XSCLKI):
				// Set filter member in byte 3
				Can_Message_Data.Byte3 = KI;
				RWLK_CAN_Decode_ServoCurrentLoop_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				//RWLK_CAN_Process_SetCurrentPiGains_Msg(Can_Message_Data);
				break;

		case(XSCLLIM):
				// Set filter member in byte 3
				Can_Message_Data.Byte3 = UMAX;
				RWLK_CAN_Decode_ServoCurrentLoop_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(XSVLKP):
				// Set filter member in byte 3
				//Can_Message_Data.Byte1 = sMailBoxDataPtr->RXMsgDataByte1;
				//Can_Message_Data.Byte2 = sMailBoxDataPtr->RXMsgDataByte2;
				Can_Message_Data.Byte3 = KP;
				RWLK_CAN_Decode_ServoVelocityLoop_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(XSVLI10):
				Can_Message_Data.Byte3 = I10;
				RWLK_CAN_Decode_ServoVelocityLoop_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(XSVLKI):
				Can_Message_Data.Byte3 = KI;
				RWLK_CAN_Decode_ServoVelocityLoop_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(XSVLLIM):
				Can_Message_Data.Byte3 = UMAX;
				RWLK_CAN_Decode_ServoVelocityLoop_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(XSPLKP):
				Can_Message_Data.Byte3 = KP;
				RWLK_CAN_Decode_ServoPositionLoop_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(XSPLKI):
				Can_Message_Data.Byte3 = KI;
				RWLK_CAN_Decode_ServoPositionLoop_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(XSPLLIM):
				Can_Message_Data.Byte3 = UMAX;
				RWLK_CAN_Decode_ServoPositionLoop_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(XSPLI10):
				Can_Message_Data.Byte3 = I10;
				RWLK_CAN_Decode_ServoPositionLoop_Msg(&Can_Message_Data);
				//RWLK_CAN_Process_SetPositionPiGains_Msg(Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;


		case(YSCLKP):
				// Set filter member in byte 3
				Can_Message_Data.Byte3 = KP;
				RWLK_CAN_Decode_ServoCurrentLoop_Msg(&Can_Message_Data);
				//RWLK_CAN_Process_SetCurrentPiGains_Msg(Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;


		case(YSCLI10):
				// Set filter member in byte 3
				Can_Message_Data.Byte3 = I10;
				RWLK_CAN_Decode_ServoCurrentLoop_Msg(&Can_Message_Data);
//				Can_Message_Data.Byte1 = sMailBoxDataPtr->RXMsgDataByte1;
//				Can_Message_Data.Byte2 = sMailBoxDataPtr->RXMsgDataByte2;
//				Can_Message_Data.Byte3 = I6;
//				RWLK_CAN_Process_SetCurrentPiGains_Msg(Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(YSCLKI):
				// Set filter member in byte 3
				Can_Message_Data.Byte3 = KI;
				RWLK_CAN_Decode_ServoCurrentLoop_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				//RWLK_CAN_Process_SetCurrentPiGains_Msg(Can_Message_Data);
				break;

		case(YSCLLIM):
				// Set filter member in byte 3
				Can_Message_Data.Byte3 = UMAX;
				RWLK_CAN_Decode_ServoCurrentLoop_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(YSPLKP):
				Can_Message_Data.Byte3 = KP;
				RWLK_CAN_Decode_ServoPositionLoop_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(YSPLKI):
				Can_Message_Data.Byte3 = KI;
				RWLK_CAN_Decode_ServoPositionLoop_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(YSPLLIM):
				Can_Message_Data.Byte3 = UMAX;
				RWLK_CAN_Decode_ServoPositionLoop_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(YSPLI10):
				Can_Message_Data.Byte3 = I10;
				RWLK_CAN_Decode_ServoPositionLoop_Msg(&Can_Message_Data);
				//RWLK_CAN_Process_SetPositionPiGains_Msg(Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(YSVLKP):
				// Set filter member in byte 3
				//Can_Message_Data.Byte1 = sMailBoxDataPtr->RXMsgDataByte1;
				//Can_Message_Data.Byte2 = sMailBoxDataPtr->RXMsgDataByte2;
				Can_Message_Data.Byte3 = KP;
				RWLK_CAN_Decode_ServoVelocityLoop_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(YSVLI10):
				Can_Message_Data.Byte3 = I10;
				RWLK_CAN_Decode_ServoVelocityLoop_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(YSVLKI):
				Can_Message_Data.Byte3 = KI;
				RWLK_CAN_Decode_ServoVelocityLoop_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(YSVLLIM):
				Can_Message_Data.Byte3 = UMAX;
				RWLK_CAN_Decode_ServoVelocityLoop_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;



/*
		case(YSVLKP):
				// Set filter member in byte 3
				//Can_Message_Data.Byte1 = sMailBoxDataPtr->RXMsgDataByte1;
				//Can_Message_Data.Byte2 = sMailBoxDataPtr->RXMsgDataByte2;
				Can_Message_Data.Byte3 = Kp;
				RWLK_CAN_Decode_ServoVelocityLoop_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(YSVLKI):
				Can_Message_Data.Byte3 = Ki;
				RWLK_CAN_Decode_ServoVelocityLoop_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(YSVLKD):
				Can_Message_Data.Byte3 = Kd;
				RWLK_CAN_Decode_ServoVelocityLoop_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(YSVLI10):
				Can_Message_Data.Byte3 = i10;
				RWLK_CAN_Decode_ServoVelocityLoop_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;


		case(YSVLLIM):
				Can_Message_Data.Byte3 = Umax;
				RWLK_CAN_Decode_ServoVelocityLoop_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;
				*/

		case(SLVSOFBW):
				Can_Message_Data.Byte3 = VLSOFBW;
				RWLK_CAN_Decode_SvlSofBw_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;
		case(SLVSOFDR):
				Can_Message_Data.Byte3 = VLSOFDR;
				RWLK_CAN_Decode_SvlSofBw_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(CALC_SLVSOF_PAR):
				RWLK_CAN_Decode_CalcSofPars_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(XSLVSOF_PAR_SET):
				RWLK_CAN_Decode_SetSofPars_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(YSLVSOF_PAR_SET):
				RWLK_CAN_Decode_SetSofPars_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(SLVNATT):
				Can_Message_Data.Byte3 = VLNATT;
				RWLK_CAN_Decode_SvlNfSetPar_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(SLVNFREQ):
				Can_Message_Data.Byte3 =  VLNFREQ;
				RWLK_CAN_Decode_SvlNfSetPar_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(SLVNWID):
				Can_Message_Data.Byte3 = VLNWID;
				RWLK_CAN_Decode_SvlNfSetPar_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(CALC_SLVNF_PAR):
				RWLK_CAN_Decode_CalcNfPars_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(XSLVN_PAR_SET):
				RWLK_CAN_Decode_SendToClaNfPars_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;
		case(Handle_Modes_Setup):
				RWLK_CAN_Decode_Handle_Modes_Setup(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;
		case(Leg_Side):
				RWLK_CAN_Decodes_Leg_Side(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;
		case(XDWELL_TIME):
				// Set the byte 5 with the parameter need to be adjusted. therefor use the same decoding for all the set profile commands
				Can_Message_Data.Byte5 = A_DT;
				RWLK_CAN_Decode_SetProfileParameterRequest_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(XPROFILE_TARGET):
				// Set the byte 5 with the parameter need to be adjusted. therefor use the same decoding for all the set profile commands
				Can_Message_Data.Byte5 = A_TARGET;
				RWLK_CAN_Decode_SetProfileParameterRequest_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(XPROFILE_VEL):
				// Set the byte 5 with the parameter need to be adjusted. therefor use the same decoding for all the set profile commands
				Can_Message_Data.Byte5 = A_VEL;
				RWLK_CAN_Decode_SetProfileParameterRequest_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(XPROFILE_ACC):
				// Set the byte 5 with the parameter need to be adjusted. therefor use the same decoding for all the set profile commands
				Can_Message_Data.Byte5 = A_ACC;
				RWLK_CAN_Decode_SetProfileParameterRequest_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(XPROFILE_JERK):
				// Set the byte 5 with the parameter need to be adjusted. therefor use the same decoding for all the set profile commands
				Can_Message_Data.Byte5 = A_JERK;
				RWLK_CAN_Decode_SetProfileParameterRequest_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(XCURRENT_ADJUSTER_VALUE):
				// Set the byte 5 with the parameter need to be adjusted. therefor use the same decoding for all the set profile commands
				Can_Message_Data.Byte5 = A_CURRENT;
				RWLK_CAN_Decode_SetProfileParameterRequest_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(YDWELL_TIME):
				// Set the byte 5 with the parameter need to be adjusted. therefor use the same decoding for all the set profile commands
				Can_Message_Data.Byte5 = A_DT;
				RWLK_CAN_Decode_SetProfileParameterRequest_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(YPROFILE_TARGET):
				// Set the byte 5 with the parameter need to be adjusted. therefor use the same decoding for all the set profile commands
				Can_Message_Data.Byte5 = A_TARGET;
				RWLK_CAN_Decode_SetProfileParameterRequest_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(YPROFILE_VEL):
				// Set the byte 5 with the parameter need to be adjusted. therefor use the same decoding for all the set profile commands
				Can_Message_Data.Byte5 = A_VEL;
				RWLK_CAN_Decode_SetProfileParameterRequest_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(YPROFILE_ACC):
				// Set the byte 5 with the parameter need to be adjusted. therefor use the same decoding for all the set profile commands
				Can_Message_Data.Byte5 = A_ACC;
				RWLK_CAN_Decode_SetProfileParameterRequest_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(YPROFILE_JERK):
				// Set the byte 5 with the parameter need to be adjusted. therefor use the same decoding for all the set profile commands
				Can_Message_Data.Byte5 = A_JERK;
				RWLK_CAN_Decode_SetProfileParameterRequest_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(YCURRENT_ADJUSTER_VALUE):
				// Set the byte 5 with the parameter need to be adjusted. therefor use the same decoding for all the set profile commands
				Can_Message_Data.Byte5 = A_CURRENT;
				RWLK_CAN_Decode_SetProfileParameterRequest_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(XCALIBRATION):
				RWLK_CAN_Decode_Xcalibration_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(YCALIBRATION):
				RWLK_CAN_Decode_Ycalibration_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(STOP_CALIBRATION):
				RWLK_CAN_Decode_StopCalibration_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(GC_SET):
				RWLK_CAN_Decode_GCSet_Msg(&Can_Message_Data);
				sMailBoxDataPtr->MODE = NO_REQUEST;
				break;

		case(READ_CURRENT):
					SendMotorPhasesCurrents(TxArbitratorFlags);
					sMailBoxDataPtr->MODE = NO_REQUEST;
					break;

		case(ENABLE_CUR_SNS):
					CurrenSensEnable_Set();
					sMailBoxDataPtr->MODE = NO_REQUEST;
					break;

		case(DISABLE_CUR_SNS):
					CurrenSensEnable_Reset();
					sMailBoxDataPtr->MODE = NO_REQUEST;
					break;

		case(XDRV_INIT):
					RWLK_CAN_Process_Drv8031InitRequest_Msg();
					sMailBoxDataPtr->MODE = NO_REQUEST;
				break;
		case(XDRV_WRITE): // todo
					sMailBoxDataPtr->MODE = NO_REQUEST;
					break;

		case(XDRV_READ):
					RWLK_CAN_Send_GetXDrv8031Registers_Msg();
					sMailBoxDataPtr->MODE = NO_REQUEST;
					break;

		case(YDRV_INIT):
					RWLK_CAN_Process_Drv8031InitRequest_Msg();
					sMailBoxDataPtr->MODE = NO_REQUEST;
					break;

		case(YDRV_WRITE):	// TODO
					sMailBoxDataPtr->MODE = NO_REQUEST;
					break;

		case(YDRV_READ):
					RWLK_CAN_Send_GetYDrv8031Registers_Msg();
					sMailBoxDataPtr->MODE = NO_REQUEST;
					break;
		case(TRANSIMT_MOTOR_MOVE):
					RWLK_CAN_Set_MotorMove_MassageStatus(&Can_Message_Data);
					sMailBoxDataPtr->MODE = NO_REQUEST;
					break;

// Commands which are activated through the CAN VALUE only, not part of the normal operation of the system.
//#ifdef DEBUG_DEV_PHASE
		case (MOTOR_HEATING_THRESHOLD):
			// Calibrate the motor, activated through the ValueCAN
		    RWLK_CAN_Decode_MotorHeatingThreshold_Msg(&Can_Message_Data);
		    sMailBoxDataPtr->MODE = NO_REQUEST;
		    break;

		case (SET_INC_ENCODER_VALUE):
			// Activated through the ValueCAN
			RWLK_CAN_Decode_SetIncEncoderValue_Msg(&Can_Message_Data);
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;

		case (SET_ABS_ENCODER_VALUE):
			// Activated through the ValueCAN
			RWLK_CAN_Decode_SetAbsoluteEncoderAngle_Msg(&Can_Message_Data);
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;

		case (CAN_ID_SENSOR_RESPONSE_LC):
			RWLK_CAN_Decode_Sensor_LC_Response(&CAN_Sensor_Command);
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;

		case (CAN_ID_SENSOR_RESPONSE_IMU):
			RWLK_CAN_Decode_Sensor_IMU_Response(&CAN_Sensor_Command);
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;

		case (CAN_ID_SENSOR_CAOMMAND_LC):
		case (CAN_ID_SENSOR_CAOMMAND_IMU):
			RWLK_CAN_Decode_Sensor_Reject(&CAN_Sensor_Command);
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;


//#endif


		case(NO_REQUEST):
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;

		default:
			sMailBoxDataPtr->MODE = NO_REQUEST;
			break;
	}
}


/*=============================================================================*/
#pragma CODE_SECTION(RWLK_CAN_TxMsg_Handler, "ramfuncs")
void RWLK_CAN_TxMsg_Handler(void)
{
#ifdef DEBUG_CAN_TX
	Calculated_Command_Parameters.CalibrationHighLimits = 0x345;
	Calculated_Command_Parameters.CalibrationLowLimits = 0x765;
	Calculated_Command_Parameters.FeedbackConversionFlag = 0xFF;
	Calculated_Command_Parameters.HighSoftwareLimit = 0x2323;
	Calculated_Command_Parameters.LowSoftwareLimit = 0x1111;
	Calculated_Command_Parameters.DesiredPosition = 0x1234;
	Calculated_Command_Parameters.ProfileMaxVelocity = 0x9876;
	Calculated_Command_Parameters.RequiredMode = 0x66;
	// ACTION_NOT_COMPLETED
	Calculated_Command_Parameters.stopalgomethod = 1;
	Calculated_Command_Parameters.ActionError = 0xB;
	Calculated_Command_Parameters.StuckSection = 3;
	Calculated_Command_Parameters.StuckVoltage = 0x7531;
	Calculated_Command_Parameters.StuckCurrent = 0x3579;
	// parameters for MCU_SHUTTING_DOWN and MCU_RECOVERY
	Calculated_Command_Parameters.PowerStatus = 0x4722;
	// parameters for
	Calculated_Command_Parameters.MotorOn = true;
	// parameters for RETURN_WRITTEN_ABS_ENCODER_ANG
	Calculated_Command_Parameters.absenc_angSend = 359;
	// parameters for JOINT_STATE
	Calculated_Command_Parameters.WorkTimer = 0xAA55AA;
	Calculated_Command_Parameters.FeedbackPosition = -2435;
	Calculated_Command_Parameters.MotorCurrent = 0x531;
	// parameters for TEST_COMPLETED
	Calculated_Command_Parameters.TestResultByte = 0x0C;
	Calculated_Command_Parameters.ProgramVersion = 0x6633;


#endif

	// Note: the if for TxArbitrator.TransmitionInProcess, must be the first to be tested,
	// So before sending a message, verify not in transmitting process.
	TxArbitrator.TransmitionInProcess = RWLK_CAN_CheckIfTransmitionInProcess();
	if (TxArbitrator.TransmitionInProcess)
	{
		printf("Oops, transmitting\n");
	}
	else if (TxArbitrator.LowPowerDetectioFlag)
	{
		RWLK_CAN_Send_McuShuttingDown_Msg();
		TxArbitrator.LowPowerDetectioFlag = false;
	}
	else if (TxArbitrator.PowerRecoveryFlag)
	{
		RWLK_CAN_Send_PowerRecovery();
	}
	else if (TxArbitrator.XActionCompleteFlag)
	{
		RWLK_CAN_Send_ActionComplete_Msg();
		TxArbitrator.XActionCompleteFlag = false;
	}
	else if (TxArbitrator.YActionCompleteFlag)
	{
		RWLK_CAN_Send_ActionComplete_Msg();
		TxArbitrator.YActionCompleteFlag = false;
	}
	else if (TxArbitrator.ActionNotCompleteFlag)
	{
		printf("tx-actionnotcomplete\n");
		RWLK_CAN_Send_ActionNotComplete_Msg();
		TxArbitrator.ActionNotCompleteFlag = false;
	}
	else if (TxArbitrator.EncoderCurrentTestFlag)
	{
		RWLK_CAN_Send_EncoderCurrentTest_Msg();
		TxArbitrator.EncoderCurrentTestFlag = false;
	}
	else if (TxArbitrator.EncoderErrorFlag)
	{
		RWLK_CAN_SendEncoderError_Msg(ENCODER_NOT_CONNECTED);
		TxArbitrator.EncoderErrorFlag = false;
	}
	else if (TxArbitrator.HighCurrentFlag)
	{
		RWLK_CAN_Send_HighCurrent_Msg();
		TxArbitrator.HighCurrentFlag = false;
	}
	else if (TxArbitrator.JointStatusFlag)
	{
		RWLK_CAN_Send_JointStatus_Msg();
		TxArbitrator.JointStatusFlag = false;
	}
	else if (TxArbitrator.MagnetLossFlag)
	{
		RWLK_CAN_SendEncoderError_Msg(MAGNET_LOSS_PROBLEM);
		TxArbitrator.MagnetLossFlag = false;
	}
	else if (TxArbitrator.PositionAckFlag)
	{
		RWLK_CAN_Send_PositionAck_Msg();
		TxArbitrator.PositionAckFlag = false;
	}
	else if (TxArbitrator.ReadCalibrationLimitsFlag)
	{


		RWLK_CAN_Send_ReadCalibrationLimits_Msg();
		TxArbitrator.ReadCalibrationLimitsFlag = false;
	}
	else if (TxArbitrator.ReadCurrentAverageFlag)
	{
		RWLK_CAN_Send_ReadCurrentAverage_Msg();
		TxArbitrator.ReadCurrentAverageFlag = false;
	}
	else if (TxArbitrator.RepeatActionFlag)
	{
		RWLK_CAN_Send_RepeatAction_Msg();
		TxArbitrator.RepeatActionFlag = false;
	}
	else if (TxArbitrator.SwitchModeCompleteFlag)
	{
		RWLK_CAN_Send_SwitchModeComplete_Msg();
		TxArbitrator.SwitchModeCompleteFlag = false;
	}
	else if (TxArbitrator.ReturnJointAngleFlag)
	{
		RWLK_CAN_Send_JointAngle_Msg();
		TxArbitrator.ReturnJointAngleFlag = false;
	}
	else if (TxArbitrator.SendAckFlag)
	{
		RWLK_CAN_Send_SendAck_Msg();
		TxArbitrator.SendAckFlag = false;
	}
	else if (TxArbitrator.SendXIncrementalEncoderFeedbackFlag)
	{
		RWLK_CAN_Send_XIncrementalEncoderFeedback_Msg();
		TxArbitrator.SendXIncrementalEncoderFeedbackFlag = false;
	}
	else if (TxArbitrator.SendYIncrementalEncoderFeedbackFlag)
	{
		RWLK_CAN_Send_YIncrementalEncoderFeedback_Msg();
		TxArbitrator.SendYIncrementalEncoderFeedbackFlag = false;
	}
	else if (TxArbitrator.SendAbsoluteEncoderFeedbackFlag)
	{
		RWLK_CAN_Send_AbsoluteEncoderFeedback_Msg();
		TxArbitrator.SendAbsoluteEncoderFeedbackFlag = false;
	}
	else if(TxArbitrator.SendMotorPhasesCurrentsFlag)
	{
		RWLK_CAN_Send_GetMotorPhasesCurrents_Msg();
		TxArbitrator.SendMotorPhasesCurrentsFlag = false;
	}
	else if (TxArbitrator.XServoOnFlag)
	{
		RWLK_CAN_Send_XServoOnFlag_Msg();
		TxArbitrator.XServoOnFlag = false;
	}
	else if(TxArbitrator.SendXServoAnalyzerMeasurementsFlag)
	{
		RWLK_CAN_Send_XServoAnalyzerMeasurementPoints_Msg();
		TxArbitrator.SendXServoAnalyzerMeasurementsFlag = false;
	}
	else if(TxArbitrator.SendYServoAnalyzerMeasurementsFlag)
	{
		RWLK_CAN_Send_YServoAnalyzerMeasurementPoints_Msg();
		TxArbitrator.SendYServoAnalyzerMeasurementsFlag = false;
	}
	else if (TxArbitrator.SystemTestMsgFlag)
	{
		RWLK_CAN_Send_SystemTest_Msg();
		TxArbitrator.SystemTestMsgFlag = false;
	}
#ifdef DEBUG_SYSTEM_STABILITY
	else if (TxArbitrator.TransmitSystemStabilityMessage)
	{
			SendSystemStabilityMessage();
			TxArbitrator.TransmitionInProcess 				= true;
			TxArbitrator.TransmitSystemStabilityMessage	= false;
	}
#endif


}

/*==============================================================================
 *
Function : CommunicationTrasmitArbitrator

Description 		: This Fucntion will responsible on  message transmit and will prevent message lost

inputs 				: struct _Transmit_Flags *TxArbitratorFlags, which is pointer to  required message structure
outputs				: No outputs
==============================================================================*/
//#pragma CODE_SECTION(CommunicationTrasmitArbitrator, "ramfuncs")
//void CommunicationTrasmitArbitrator(struct _Transmit_Flags *TxArbitratorFlags)
//{
//	uint8_t ErrCode = ERR_OK;
//
//
//	if ((TxArbitratorFlags -> ActionCompleteFlag  == true )&&(TxArbitratorFlags -> TransmitionInProcess == false))
//	{
//		CAN_MessageTransmit(ACTION_COMPLETED, TxMailBoxData.MODE, 0, 0, 0, 0, 0, 0, 1, RWLK_CAN_CardConfig, sizeof(ucTXMsgData));
//		//TxArbitratorFlags -> TransmitionInProcess = true;
//		TxArbitratorFlags -> ActionCompleteFlag	  = false;
//	}
//
//	else if((TxArbitratorFlags -> SendIncrementalEncoderFeedbackFlag  == true )&&(TxArbitratorFlags -> TransmitionInProcess == false))
//	{
//		ErrCode = IncEncoder_GetPosition(&IncEncFeedbackPosition);
//		IncEncoderPositionParser( &IncEncFeedbackPosition, &sIncEncDataSend);
//		CAN_MessageTransmit(GET_INC_ENCODER_VALUE, ErrCode, 0, 0, sIncEncDataSend.TXEncoderData3, sIncEncDataSend.TXEncoderData2, sIncEncDataSend.TXEncoderData1, sIncEncDataSend.TXEncoderData0, 1, RWLK_CAN_CardConfigMsgId, sizeof(ucTXMsgData));
//		//TxArbitratorFlags -> TransmitionInProcess = true;
//		TxArbitratorFlags -> SendIncrementalEncoderFeedbackFlag	  = false;
//	}
//
//	else if ((TxArbitratorFlags -> SendAckFlag  == true )&&(TxArbitratorFlags -> TransmitionInProcess == false))
//	{
//		CAN_MessageTransmit(MOTOR_SEND_ACK, 0, 0, 0, 0, 0, 0, 0, 0, RWLK_CAN_CardConfig, sizeof(ucTXMsgData));
//		//TxArbitratorFlags -> TransmitionInProcess = true;
//		TxArbitratorFlags -> SendAckFlag	  = false;
//	}
//
//}

/*==============================================================================
 *
Function : SystemTestEnable

Description 		: This Fucntion will allowed system test enable

inputs 				: no inputs
outputs				: no outputs
==============================================================================*/
#pragma CODE_SECTION(SystemTestEnable, "ramfuncs")
void SystemTestEnable(bool *FirstMoveFlag, bool *TestFlag, uint16_t *TestCounter)
{
	*FirstMoveFlag = true;
	*TestFlag = true;
	*TestCounter = 0;
}


/*==============================================================================
 *
Function : ByPassModeEnDis

Description 		: This Fucntion will allowed system test enable

inputs 				: no inputs
outputs				: no outputs
==============================================================================*/
#pragma CODE_SECTION(ByPassModeEnDis, "ramfuncs")
void ByPassModeEnDis(struct _MAILBOX_DATA *sMailBoxDataPtr, struct _Transmit_Flags *TxArbitratorFlags, bool *ByPassSwitch)
{

	if (sMailBoxDataPtr -> RXMsgDataByte1 == 0x04)
	{
		*ByPassSwitch = !*ByPassSwitch;
 		TxArbitratorFlags -> XActionCompleteFlag = true ;
	}

}

/*==============================================================================
 *
Function : CheckJointStatusFlagSet

Description 		: This Fucntion will set joint status flag

inputs 				: struct _Transmit_Flags *TxArbitratorFlags is pointer to Arbitration flags structure
outputs				: no outputs
==============================================================================*/
#pragma CODE_SECTION(CheckJointStatusFlagSet, "ramfuncs")
void CheckJointStatusFlagSet(struct _Transmit_Flags *TxArbitratorFlags)
{
	TxArbitratorFlags -> JointStatusFlag = true ;
}













/*==============================================================================
 *
Function : CurrentAdjusterRequest

Description 		: This Fucntion will set joint status flag

inputs 				: struct _Transmit_Flags *TxArbitratorFlags is pointer to Arbitration flags structure
outputs				: no outputs
==============================================================================*/
#pragma CODE_SECTION(PositionAdjusterRequest, "ramfuncs")
void PositionAdjusterRequest(_Bool *PositionAdjusterFlag,volatile struct ADJUSTER_PROFILE_PAR *ParametersStruct,volatile struct _CONTROL_VAR *ControlVarStruct,volatile struct _P2MOVE_VAR *PrepareToMoveVariablesStruct, CAN_MSG_DATA Msg_Data)
{






}

/*==============================================================================
 *
Function : CurrentAdjusterRequest

Description 		: This Fucntion will set joint status flag

inputs 				: struct _Transmit_Flags *TxArbitratorFlags is pointer to Arbitration flags structure
outputs				: no outputs
==============================================================================*/
#pragma CODE_SECTION(VelocityAdjusterRequest, "ramfuncs")
void VelocityAdjusterRequest(uint16_t *Period, uint16_t *VelocityAmplitude,CAN_MSG_DATA Msg_Data)
{

	uint32_t TimePar = 0;



	if(Msg_Data.Byte5 == 0)
	{
		TimePar = Msg_Data.Byte1;
		TimePar <<= 8;
		TimePar = TimePar + Msg_Data.Byte2;
		*Period = (int32_t)TimePar;

		TimePar = 0;
		TimePar = Msg_Data.Byte3;
		TimePar <<= 8;
		TimePar = TimePar + Msg_Data.Byte4;
		*VelocityAmplitude = TimePar;
		//CpuClaCntrlRegs.X_MFLAGS.bit.VELOCITY_ADJUSTER = 1;
		MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.VELOCITY_ADJUSTER = 1;
	}
	else
	{
		MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.VELOCITY_ADJUSTER = 0;
		EPwm1Regs.CMPA.bit.CMPA = 1250;
		EPwm2Regs.CMPA.bit.CMPA = 1250;
		EPwm3Regs.CMPA.bit.CMPA = 1250;
	}
}


/*==============================================================================
 *
Function : CurrentAdjusterRequest

Description 		: This Fucntion will set joint status flag

inputs 				: struct _Transmit_Flags *TxArbitratorFlags is pointer to Arbitration flags structure
outputs				: no outputs
==============================================================================*/
#pragma CODE_SECTION(CurrentAdjusterRequest, "ramfuncs")
void CurrentAdjusterRequest(int32_t *Period, uint16_t *CurrentAmplitude,CAN_MSG_DATA Msg_Data)
{

	uint32_t TimePar = 0;
			if(Msg_Data.Byte5 == 0)
			{
				TimePar = Msg_Data.Byte1;
				TimePar <<= 8;
				TimePar = TimePar + Msg_Data.Byte2;
				*Period = (int32_t)TimePar;

				TimePar = 0;
				TimePar = Msg_Data.Byte3;
				TimePar <<= 8;
				TimePar = TimePar + Msg_Data.Byte4;
				*CurrentAmplitude = (uint16_t)TimePar;
				MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.CURRENT_ADJUSTER = 1;
			}
			else
			{
				MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.CURRENT_ADJUSTER = 0;
				EPwm1Regs.CMPA.bit.CMPA = 1250;
				EPwm2Regs.CMPA.bit.CMPA = 1250;
				EPwm3Regs.CMPA.bit.CMPA = 1250;
			}
}


//No More

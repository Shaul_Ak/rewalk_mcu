/*******************************************************************************
 * File name: RWLK_CAN_Msg_Handler.h
 *
 * Description:
 *
 *
 * Note: Developed for TI F28377D.
 *
 * Author:	Yehuda Bitton
 * Date: May 2017
 *
 ******************************************************************************/

/*******************************************************************************
  Multiple include protection
 ******************************************************************************/
#ifndef _RWLK_CAN_MSG_HANDLER_H_
#define _RWLK_CAN_MSG_HANDLER_H_


/*******************************************************************************
 *  Include
 ******************************************************************************/
#include "RWLK_Common.h"
#include "RWLK_Common_Par.h"
#include "RWLK_Motor_Control.h"

/*******************************************************************************
 *  Defines
 ******************************************************************************/
#define  INC_ENCODER_FEEDBACK_FLAG				1

/*------------------------------------------------------------------------------
  Configurations
------------------------------------------------------------------------------*/


/*******************************************************************************
 *  Data types
 ******************************************************************************/
typedef struct _CAN_MSG_DATA
{
 uint8_t Byte0;
 uint8_t Byte1;
 uint8_t Byte2;
 uint8_t Byte3;
 uint8_t Byte4;
 uint8_t Byte5;
 uint8_t Byte6;
 uint8_t Byte7;
}CAN_MSG_DATA;


typedef struct
{
	uint16_t ID;
	uint8_t  Length;
	uint8_t Data[8];
}CAN_SENSOR_MSG;


//------------------------- Struct holding the requests parameters -------------//
typedef struct
{
	COMMAND_PROTOCOL ReceivedCommand;
	// parameter for LogMode
	uint8_t LogLevel;
	// parameter for CURRENT_TRSH
	uint8_t CurrentHandleAlgo;  // CAN_MESSAGE[3]
	uint16_t Walk_Current_Threshold;  // can be negative value ???????
	uint16_t	Stairs_Current_Threshold;
	// parameter for MOVE
	/* TODO: falling detection byte 1 is not used in the P6 */
	int16_t PositionAckDeg; // [Degrees]
	int32_t DesiredPosition; // [Encoder counts]
	int32_t ProfileMaxVelocity;
	// parameter for RETURN_STATUS
	bool AbsEncoderReadFlag;
	bool MotorCurrentReadFlag;
	// parameters for SWITCH_MODE
	OPERATION_MODE OperationalMode;
	uint8_t ManualVelocity;
	// parameters for IAM_ALIVE
	uint8_t IamAliveActivateFlag;
	// parameters for AUTO_CALIBRATION
	uint16_t OpenLoopPWM;
	// parameters for BYPASS_MODE
	uint8_t ByPassEnable;
	// parameters for SCL (Servo Current Loop)
	uint32_t FilterValue;
	PI_FILTER_MEMBERS PIFilterMember;
	PID_FILTER_MEMBERS PIDFilterMember;
	SOF_FILTER_MEMBERS SOFilterMember;
	NOTCH_FILTER_MEMBERS NFilterMember;
	// parameter for adjust request
	ADJUSTER_REQUEST_PARAMETERS AdjusterRequest;
	// parameters for MOVE�_OPEN_�LOOP
	int16_t PwmTimePar;
	bool OpenLoopEnable;
	// parameters for CHANGE_PWM
	int16_t CommandA;
	int16_t CommandB;
	int16_t CommandC;
	// parameters for profiling setting (commands like: DWELL_TIME)
	int32_t ProfileNewValue;
	POSITION_ADJUSTER_PARAMS TheParameterToSet;
//#ifdef DEBUG_DEV_PHASE
	// parameters for MOTOR_HEATING_THRESHOLD
	int16_t HeatingCurrentThreshold;
	uint32_t HeatCounterTHrshld;
	// parameter for COMMUT_REQUEST
	int16_t BRLS_PWM_value;
	// parameters for SET filter parameters
	VL_FILTER_NAME VL_FilterName;
	uint16_t HandleStatus;
	uint16_t LegSideSelection;


//#endif
} Input_Command_Parameters_Type;

/* TODO: orit - do not forget to initailziae the structure */

//------------------------- Struct holding the requests parameters -------------//
typedef struct
{
	// parameters for READ_LIMITS
	int16_t CalibrationHighLimits;
	int16_t CalibrationLowLimits;
	int8_t FeedbackConversionFlag; // = 0;
	int16_t HighSoftwareLimit; // used also for JOINT_STATE
	int16_t LowSoftwareLimit;  // used also for JOINT_STATE
	// parameters for MOTOR_SEND_ACK
	int32_t DesiredPosition;
	uint32_t ProfileMaxVelocity;
	uint16_t RequiredMode;
	// parameters for ACTION_NOT_COMPLETED
	uint8_t stopalgomethod;
	uint16_t ActionError;
	uint16_t StuckSection;
	uint16_t StuckVoltage;
	int16_t StuckCurrent;
	// parameters for MCU_SHUTTING_DOWN and MCU_RECOVERY
	uint16_t PowerStatus;
	// parameters for READY_FOR_MOVEMENT_TEST
	uint8_t MotorOn;
	// parameters for RETURN_WRITTEN_ABS_ENCODER_ANG
	uint16_t absenc_angSend;
	// parameters for JOINT_STATE
	uint32_t WorkTimer;
	int16_t FeedbackPosition;
	uint16_t MotorCurrent;
	// parameters for TEST_COMPLETED
	uint16_t  TestResultByte;
	uint16_t ProgramVersion;
	// parameters for REQ_POSITION_ACK
	// parameters for SWITCH_MODE
	OPERATION_MODE ReceivedOperationalMode;
	// Encoder Test
	uint32_t EncoderCurrentPositionUsigned;



} Output_Command_Parameters_Type;

/*******************************************************************************
 *  Constants and Macros
 ******************************************************************************/

/*******************************************************************************
 *  Interface function deceleration:
 ******************************************************************************/
void RWLK_CAN_MSG_Handler_Init(void);

//////////////////////////////////////////////
// Income CAN BUS message - process function:
void RWLK_CAN_Decode_JumpToBoot_Msg(void);
void RWLK_CAN_Decode_JointState_Msg(void);
void RWLK_CAN_Decode_PerformSystemTesting_Msg(void);
void RWLK_CAN_Decode_ReadLimits_Msg(void);
void RWLK_CAN_Decode_ReadyForMovement_Msg(void);
void RWLK_CAN_Decode_AbsoluteEncoderProblem_Msg(void);
void RWLK_CAN_Decode_XStop_Motor_Msg(void);
void RWLK_CAN_Decode_YStop_Motor_Msg(void);
void RWLK_CAN_Decode_GetXIncEncoderValue_Msg(void);
void RWLK_CAN_Decode_GetYIncEncoderValue_Msg(void);
void RWLK_CAN_Decode_GetAbsEncoderAngle_Msg(void);
void RWLK_CAN_Decode_XMotorEnable_Msg(void);
void RWLK_CAN_Decode_YMotorEnable_Msg(void);
void RWLK_CAN_Decode_ActionCompleted_Msg(void);
void RWLK_CAN_Decode_XMotorDisable_Msg(void);
void RWLK_CAN_Decode_YMotorDisable_Msg(void);
void RWLK_CAN_Decode_ReleaseXMotor_Msg(void);
void RWLK_CAN_Decode_ReleaseYMotor_Msg(void);
void RWLK_CAN_Decode_UnreleaseXMotor_Msg(void);
void RWLK_CAN_Decode_UnreleaseYMotor_Msg(void);
void RWLK_CAN_Decode_RunTestFlag_Msg(void);

void RWLK_CAN_Decode_ReturnStatus_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_CurrentTrsh_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_IamAlive_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_LogMode_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_AutoCalibration_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_XAxisMove_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_YAxisMove_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_SwitchMode_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_ByPassMode_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_EncoderTest_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_XAxisMoveOpenLoop_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_YAxisMoveOpenLoop_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_CalcSofPars_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_SvlNfSetPar_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_CalcNfPars_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_SendToClaNfPars_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_Handle_Modes_Setup(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decodes_Leg_Side(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_SetPid_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_AccFactor_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_WriteSamplingTime_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_ReadSamplingTime_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_FlashStore_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_SetHWLimits_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_ChangeXPwmCommand_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_ChangeYPwmCommand_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_XCommutationSetRequest_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_YCommutationSetRequest_Msg(const CAN_MSG_DATA* Msg_Data);
// A common decoding for the adjusting requests
void RWLK_CAN_Decode_AdjusterRequest_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_CurrentAdjusterRequest_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Process_ServoAnalizerStart_Msg(CAN_MSG_DATA Msg_Data);
// This message decodes request for setting profile parameter new value (a common decoding for all the parameters)
void RWLK_CAN_Decode_SetProfileParameterRequest_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_ServoCurrentLoop_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Process_SetCurrentPiGains_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_ServoPositionLoop_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_SvlSofBw_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_SetSofPars_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_GCSet_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_Xcalibration_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_Ycalibration_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_StopCalibration_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_ServoVelocityLoop_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_FilterParams_Msg(const CAN_MSG_DATA* Msg_Data);
//#ifdef DEBUG_DEV_PHASE
void RWLK_CAN_Decode_MotorHeatingThreshold_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_SetIncEncoderValue_Msg(const CAN_MSG_DATA* Msg_Data);
void RWLK_CAN_Decode_SetAbsoluteEncoderAngle_Msg(const CAN_MSG_DATA* Msg_Data);
//#endif

void RWLK_CAN_Decode_Sensor_LC_Response(CAN_SENSOR_MSG* CAN_Sensor_Command);
void RWLK_CAN_Decode_Sensor_IMU_Response(CAN_SENSOR_MSG* CAN_Sensor_Command);
void RWLK_CAN_Decode_Sensor_Reject(CAN_SENSOR_MSG* CAN_Sensor_Command);


void RWLK_CAN_Process_Drv8031InitRequest_Msg(void);



//////////////////////////////////////////////
// Out CAN BUS message - send function:
void RWLK_CAN_Send_ReadCalibrationLimits_Msg(void);

/////////////////////////////////////////////////////



void RWLK_CAN_Send_ActionComplete_Msg(void);
void RWLK_CAN_Send_ActionNotComplete_Msg(void);
void RWLK_CAN_Send_EncoderCurrentTest_Msg(void);
void RWLK_CAN_Send_HighCurrent_Msg(void);
void RWLK_CAN_Send_JointStatus_Msg(void);
void RWLK_CAN_Send_MagnetLoss_Msg(void);
void RWLK_CAN_Send_PositionAck_Msg(void);
void RWLK_CAN_SendEncoderError_Msg(uint16_t ReasonOfFail);
void RWLK_CAN_Send_SwitchModeComplete_Msg(void);
#ifdef DEBUG_SYSTEM_STABILITY
void SendSystemStabilityMessage(void);
#endif


void RWLK_CAN_Send_ReadCurrentAverage_Msg(void);
void RWLK_CAN_Send_RepeatAction_Msg(void);
void RWLK_CAN_Send_JointAngle_Msg(void);
void RWLK_CAN_Send_SendAck_Msg(void);
void RWLK_CAN_Send_XIncrementalEncoderFeedback_Msg(void);
void RWLK_CAN_Send_YIncrementalEncoderFeedback_Msg(void);
void RWLK_CAN_Send_AbsoluteEncoderFeedback_Msg(void);
void RWLK_CAN_Send_XServoAnalyzerMeasurementPoints_Msg(void);
void RWLK_CAN_Send_YServoAnalyzerMeasurementPoints_Msg(void);
void RWLK_CAN_Send_XServoOnFlag_Msg(void);
void RWLK_CAN_Send_YServoOnFlag_Msg(void);
void RWLK_CAN_Send_SystemTest_Msg(void);
bool RWLK_CAN_CheckIfTransmitionInProcess(void);
void RWLK_CAN_Send_GetMotorPhasesCurrents_Msg(void);
void RWLK_CAN_Send_GetXDrv8031Registers_Msg(void);
void RWLK_CAN_Send_GetYDrv8031Registers_Msg(void);
void RWLK_CAN_Send_McuShuttingDown_Msg(void);
void RWLK_CAN_Send_PowerRecovery(void);
void RWLK_CAN_Send_MotorMoveMassage(int id, int pos, int move);

//Sensor Operation
uint16_t RWLK_CAN_Send_Sensor_Command(uint8_t Sensor_ID, CAN_SENSOR_MSG* SensorTx);

//void RWLK_CAN_GetInOutCommandsParameters(uint32_t *InParamsAddr, uint32_t *OutParamsAddr);

uint16_t CAN_MessageTransmit(uint16_t Action, uint16_t MessageByte1, int16_t MessageByte2, int16_t MessageByte3, int16_t MessageByte4, int16_t MessageByte5, int16_t MessageByte6, int16_t MessageByte7, uint16_t MailBox, uint32_t MessageID, uint32_t MessageDataLength);
extern void PositionAdjusterRequest(_Bool *PositionAdjusterFlag,volatile struct ADJUSTER_PROFILE_PAR *ParametersStruct,volatile struct _CONTROL_VAR *ControlVarStruct,volatile struct _P2MOVE_VAR *PrepareToMoveVariablesStruct, CAN_MSG_DATA Msg_Data);
extern void VelocityAdjusterRequest(uint16_t *Period, uint16_t *VelocityAmplitude,CAN_MSG_DATA Msg_Data);
extern void CurrentAdjusterRequest(int32_t *Period, uint16_t *CurrentAmplitude,CAN_MSG_DATA Msg_Data);
extern void SetSofParameters(VL_FILTER_NAME fName,DF22 *filter[], CAN_MSG_DATA Msg_Data);
//extern void SetPiGains(FILTER_NAME fName, PI *filter[], PI_FILTER_MEMBERS filterMember, uint32_t filterValue);

extern void CalcSofParametrs(void);
extern void CalcNotchFilterParameters(void);

#endif /* _RWLK_CAN_MSG_HANDLER_H_ */

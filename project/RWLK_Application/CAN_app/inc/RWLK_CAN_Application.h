/*
 * RWLK_CAN_Application.h
 *
 *  Created on: Jan 10, 2016
 *      Author: arie
 */

#ifndef RWLK_CAN_APPLICATION_H_
#define RWLK_CAN_APPLICATION_H_
#include "../driverlib/can.h"
#include "RWLK_CAN_Driver.h"
#include "RWLK_Common_Par.h"


/*==============================================================================
                               	   CANBUS Type Definitions
==============================================================================*/

#define ERR_TX_BUSY 0x01
 /*==============================================================================
                                	   CANBUS Structers
 ==============================================================================*/
 typedef struct _MAILBOX_DATA
 		{
 		uint8_t REQUIRED_MODE;
 		uint8_t MODE;
 //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Shaul 18.4.2017: ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		#ifdef SENSOR_MODE
 		uint8_t ID;
 		uint8_t Length;
		uint8_t RXMsgDataByte0;
		#endif  // SENSOR_MODE
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 		uint8_t RXMsgDataByte1;
 		uint8_t RXMsgDataByte2;
 		uint8_t RXMsgDataByte3;
 		uint8_t RXMsgDataByte4;
 		uint8_t RXMsgDataByte5;
 		uint8_t RXMsgDataByte6;
 		uint8_t RXMsgDataByte7;
 		}sMailBoxData;

 //typedef struct sMailBoxData *sMailBoxDataPtr;

 struct MBOXES_DATA {
 			struct _MAILBOX_DATA MBOX1_DATA;
 			struct _MAILBOX_DATA MBOX2_DATA;
 			struct _MAILBOX_DATA MBOX3_DATA;
 			struct _MAILBOX_DATA MBOX4_DATA;
 			struct _MAILBOX_DATA MBOX5_DATA;
 			struct _MAILBOX_DATA MBOX6_DATA;
 			struct _MAILBOX_DATA MBOX7_DATA;
 			struct _MAILBOX_DATA MBOX8_DATA;
 			struct _MAILBOX_DATA MBOX9_DATA;
 			struct _MAILBOX_DATA MBOX10_DATA;
 			struct _MAILBOX_DATA MBOX11_DATA;
 			struct _MAILBOX_DATA MBOX12_DATA;
 			struct _MAILBOX_DATA MBOX13_DATA;
 			struct _MAILBOX_DATA MBOX14_DATA;
 			struct _MAILBOX_DATA MBOX15_DATA;
 			struct _MAILBOX_DATA MBOX16_DATA;
 			struct _MAILBOX_DATA MBOX17_DATA;
 			struct _MAILBOX_DATA MBOX18_DATA;
 			struct _MAILBOX_DATA MBOX19_DATA;
 			struct _MAILBOX_DATA MBOX20_DATA;
 			struct _MAILBOX_DATA MBOX21_DATA;
 			struct _MAILBOX_DATA MBOX22_DATA;
 			struct _MAILBOX_DATA MBOX23_DATA;
 			struct _MAILBOX_DATA MBOX24_DATA;
 			struct _MAILBOX_DATA MBOX25_DATA;
 			struct _MAILBOX_DATA MBOX26_DATA;
 			struct _MAILBOX_DATA MBOX27_DATA;
 			struct _MAILBOX_DATA MBOX28_DATA;
 			struct _MAILBOX_DATA MBOX29_DATA;
 			struct _MAILBOX_DATA MBOX30_DATA;
 			struct _MAILBOX_DATA MBOX31_DATA;
 			struct _MAILBOX_DATA MBOX32_DATA;
 		};
/*
 sMailBoxData RxMailBoxData;
 sMailBoxData TxMailBoxData;
*/

 //typedef struct sMailBoxData *sMailBoxDataPtr;



//sMailBoxData    RxMailBoxData;
extern volatile struct MBOXES_DATA CanaMboxesData;

extern tCANMsgObject 	sCANMsgObjectPool[CAN_MAX_MAILBOX_NUM];	       // Receive message object
extern tMsgObjType	 	sCANMessageType;       // Message type
extern unsigned char    ucTXMsgData[8];        // Transmit data array (8 byte)
extern unsigned char    ucRXMsgData[8];	       // Receive data array (8 byte)

void RWLK_CAN_Application_Init(void);
void CAN_Input(void);

void RWLK_CAN_TxMsg_Handler(void);
uint16_t CAN_MessageTransmit(uint16_t Action, uint16_t MessageByte1, int16_t MessageByte2, int16_t MessageByte3, int16_t MessageByte4, int16_t MessageByte5, int16_t MessageByte6, int16_t MessageByte7, uint16_t MailBox, uint32_t MessageID, uint32_t MessageDataLength);
//void RWLK_CAN_GetTransmitFlagsPtr(sTransmitFlags *transmitFlagsPtr);

extern bool CAN_ReadMessage(uint16_t MailBox);
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Shaul 18.4.2017: ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#ifdef SENSOR_MODE
extern bool CAN_SensorReadMessage(void);
extern bool CAN_SensorReadResponse(void);
extern void CAN_SensorsMessageReceive(uint16_t MailBox);
void CAN_SensorsRejectHandler(void);
#endif  // SENSOR_MODE
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
extern void CAN_MessageReceive(uint16_t MailBox, unsigned char *sRxMessageData);
extern void ProtocolParser(bool *MOTOR_FLAGS, struct _MAILBOX_DATA *sMailBoxDataPtr, struct _Transmit_Flags *TxArbitratorFlags);
extern void CommunicationTrasmitArbitrator(struct _Transmit_Flags *TxArbitratorFlags);
extern void SystemTestEnable(bool *FirstMoveFlag, bool *TestFlag, uint16_t *TestCounter);
extern void ByPassModeEnDis(struct _MAILBOX_DATA *sMailBoxDataPtr, struct _Transmit_Flags *TxArbitratorFlags, bool *ByPassSwitch);
extern void CheckJointStatusFlagSet(struct _Transmit_Flags *TxArbitratorFlags);
extern void SendXIncrementalEncoderPosition(struct _Transmit_Flags *TxArbitratorFlags);
extern void SendYIncrementalEncoderPosition(struct _Transmit_Flags *TxArbitratorFlags);
extern void SendMotorPhasesCurrents(struct _Transmit_Flags *TxArbitratorFlags);



#endif /* RWLK_CAN_APPLICATION_H_ */

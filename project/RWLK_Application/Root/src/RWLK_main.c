/*******************************************************************************
 * Description: main of MPU7 project.
 *
 * Note: Developed for TI F28377D.
 * Ver 1.0.0
 *
 ******************************************************************************/

/*==============================================================================
                           Global Defines
==============================================================================*/
#define BUFFER_SIZE						5

// CLA defines
#define WAITSTEP 	asm(" RPT #255 || NOP")

// Define for IQmathLib.h:
#define   MATH_TYPE      				1

/*==============================================================================
                               Includes
==============================================================================*/
#include "RWLK_Common.h"
#include "RWLK_LogicHandler.h"
#include "RWLK_CAN_Msg_Handler.h"
#include "RWLK_CAN_Application.h"
#include "RWLK_StrokeHandler.h"
#include "RWLK_Sensor_Data.h"

#include "IQmathLib.h"

#include "RWLK_SPI_Driver.h"
#include "RWLK_IMU_driver.h"
#include "ExtFlash_Driver.h"
#include "RWLK_Motor_Control.h"
#include "RWLK_DRV8031_Applications.h"
#include "RWLK_BLUE_GUI_API.h"


#ifdef DEBUG_ON // Only in DEBUG configuration (for details see RWLK_Test.h)
	#include "RWLK_Tests.h"
#endif

#include "F2837xD_dcsm.h"


/*==============================================================================
						Globals Variables
==============================================================================*/
//Task 1 (C) Variables
float y1[BUFFER_SIZE+1];
float y2[BUFFER_SIZE+1];

uint32_t	Counter = 0;
uint32_t 	Max_Count = 20;


#ifdef __cplusplus
#pragma DATA_SECTION("CpuToCla1MsgRAM")
float fVal;
#pragma DATA_SECTION("Cla1ToCpuMsgRAM")
float fResult;
#else
#pragma DATA_SECTION(fVal,"CpuToCla1MsgRAM")
float fVal;
#pragma DATA_SECTION(fResult,"Cla1ToCpuMsgRAM")
float fResult;
#endif // __cplusplus

_iq PhaseSinArgument;

/*==============================================================================
                          External Variables
==============================================================================*/
//extern volatile struct DCSM_Z1_OTP DcsmZ1Otp;

/*==============================================================================
                    Private functions declartion
==============================================================================*/
//extern long _IQsinPU(long A);
void CLA_runTest(void);
static void RWLK_SW_Init(void);

/*==============================================================================
                           External functions
==============================================================================*/
extern void  CLAfIRSTpROG(void);

/*==============================================================================
                           Boot pins remap
==============================================================================*/
//#pragma DATA_SECTION (Z1_BOOTCTRL_value, "Z1_BOOTCTRL_section");
//volatile const long Z1_BOOTCTRL_value = 0x97960B5A;


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MAIN ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

uint32_t  Msec_counter;

int main(void)
 {
	uint8_t ErrCode = ERR_OK;


	ErrCode = RWLK_Init();
	// Initialize software modules
	RWLK_SW_Init();

	if (ErrCode != ERR_OK)
	{
		//Do something
	}

   //For Debug - Algo - Wyss

//	med_ctr.parietic_side = MEDEXO_CONTROLLER_LEFT_PARIETIC;
  //  med_ctr.handle_mode = INACTIVE_MODE;
 //   med_ctr.my_struct_pf_pretension.flag_initial_setting_completed = 1 ;
 //   med_ctr.my_struct_df_pretension.flag_initial_setting_completed = 1;
/*    med_ctr.my_struct_position_profile_input.flag_pf_active = 1 ;
    med_ctr.my_struct_position_profile_input.flag_df_active = 1 ;*/
    /****************************************************************/

    // Only in DEBUG configuration (for details see RWLK_Test.h)
#ifdef DEBUG_ON
	RWLK_Debug_Tests();
#endif
	LEDs_OFF();
   /*  for measuring */
	GPIO_Test_TimeScope_Config();
    GPIO_WritePin(TIME_TEST_PIN_GPIO103, 0);

 	for(;;) //main loop
	{

 	   while(!StartTick)continue;
	   GPIO_WritePin(TIME_TEST_PIN_GPIO103, 1);

 	   StartTick = 0;
 	   GUI_Blue_Handler(Msec_counter);
 	   Msec_counter++;


 	   if (1) //Get_Restore_Mode() == ACTIVE_MODE)
 	   {

			CAN_Input();
			// Translate the command recieved to perform by the MCU into profile parameters preparation.
			RWLK_LogicHandler();
	       // TODO: orit - CAN_Output();
			RWLK_CAN_TxMsg_Handler(/*&TransmitFlags*/);
			RWLK_SensorData_CommandHandler();
			//ControlWordCheck(); // move inside the logic handler
			//IncEncPos = sControlVarStruct.MotorPosition;
 	   } //if ACTIVE mode

 	   else
 	   {
 		   //not active mode
 	   }
 	  GPIO_WritePin(TIME_TEST_PIN_GPIO103, 0);
	}  //end for

}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ end MAIN ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//


static void RWLK_SW_Init(void)
{
	// Activate sw modules init functions
	RWLK_CAN_Application_Init();
	RWLK_CAN_MSG_Handler_Init();
	RWLK_LogicHandlerInit();
}


//*****************************************************************************
// Private function definitions
//*****************************************************************************
#pragma CODE_SECTION(CLA_runTest, "ramfuncs")
void CLA_runTest(void)
{
	//int16_t i;
	//float fError[BUFFER_SIZE+1], fError1[BUFFER_SIZE+1];
	//asm(" ESTOP0");

	Cla1ForceTask1andWait();
	WAITSTEP;
	//asm(" ESTOP0");
}

/*****************************************************************************
								CLA ISR
*****************************************************************************/
__interrupt void cla1Isr1 ()
{
	fVal = fResult;
	// Acknowledge the end-of-task interrupt for task 1
	PieCtrlRegs.PIEACK.all = M_INT11;
	//asm(" ESTOP0");
}

__interrupt void cla1Isr2 ()
{
	//int16_t ErrCode = 0;
	PieCtrlRegs.PIEACK.all = M_INT11;
	//CPU_RotorPosition = EQep1Regs.QPOSCNT;
	//asm(" ESTOP0");
}

__interrupt void cla1Isr3 ()
{
	//int16_t ErrCode = 0;
	XBRLS_PWM = 0;
	PieCtrlRegs.PIEACK.all = M_INT11;
	//CPU_RotorPosition = EQep1Regs.QPOSCNT;
	//asm(" ESTOP0");
}

__interrupt void cla1Isr4 ()
{
	//int16_t ErrCode = 0;
	YBRLS_PWM = 0;
	PieCtrlRegs.PIEACK.all = M_INT11;
	//CPU_RotorPosition = EQep1Regs.QPOSCNT;
	//asm(" ESTOP0");
}

__interrupt void cla1Isr5 ()
{
	asm(" ESTOP0");
}

__interrupt void cla1Isr6 ()
{
	asm(" ESTOP0");
}

__interrupt void cla1Isr7 ()
{
	asm(" ESTOP0");

}

__interrupt void cla1Isr8 ()
{
	// Acknowledge the end-of-task interrupt for task 8
	PieCtrlRegs.PIEACK.all = M_INT11;
	asm(" ESTOP0");
}




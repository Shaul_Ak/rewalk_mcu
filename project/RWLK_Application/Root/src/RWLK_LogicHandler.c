/*******************************************************************************
 * File name: RWLK_LogicHandler.c
 *
 * Description:
 *
 *
 * Note:
 *
 * Author:	Yehuda Bitton
 * Date: MAy 2016
 *
 ******************************************************************************/

/*******************************************************************************
 *  Include
 ******************************************************************************/
#include "RWLK_LogicHandler.h"
#include "RWLK_CAN_Msg_Handler.h"
#include "RWLK_CAN_Application.h"
#include "RWLK_CardConfig.h"
#include "stdio.h"
#include "RWLK_Cla_Control.h"
#include "RWLK_ADC_Driver.h"
#include "RWLK_ePWM_Driver.h"
#include "RWLK_ServoManager.h"
#include "RWLK_Abs_Encoder_Application.h"
#include "RWLK_Manual.h"
#include "medexo_controller.h"

/*******************************************************************************
 *  Definitions
 ******************************************************************************/
#define	CALBIRATION_FIRST_BYTE 0x3E8800    // Flash address taken is the address of CALBIRATION_2808_FIRST_ADDRESS
#define MAX_CURRENT	1456

#define MANUAL_DEFAULT_SPEED 0x22

// This struct is the working surface of this module
typedef struct
{
	uint16_t MyCardId;  // Hold the card id of the MCU running the application
	OPERATION_MODE Mode;
	OPERATION_MODE LastMode;
	bool ManualModeFlag;
	int16_t MaxPeakCurrentThreshold;
	// IAM_ALIVE
	bool ButtonPushedFlag;
	bool IamAliveActivateFlag;
	uint32_t IamAliveCounter;
	bool  ByPassEnable;
	bool FirstMoveafterBoot;
	//static uint16_t AdcErr = 0;
	uint16_t TestSystemCounter;
	bool TestSystemFlag; // this flag is to mark the test shall be perfomed, it is not the flag for the message to be sent back to the MPB
	bool DataLogFlag; // current Implementation treating only
	bool OpenLoopModeFlag;  // TODO: shall be connected to some logic.
	// STOP_MOTOR
	bool MovementFlag;
	// Filter (commands of set servo position/velocity/current)
	PI_FILTER_MEMBERS FilterMember;
	uint32_t TempTimeCounter;
	bool AnotherAxisEncoderDisc;

} RWLK_Logic_Variables_Type;


/*******************************************************************************
 *  Global variables
 ******************************************************************************/
#pragma DATA_SECTION(Input_Command_Parameter_Pointer,"ramdata")
volatile Input_Command_Parameters_Type *Input_Command_Parameter_Pointer=NULL;
#pragma DATA_SECTION(Output_Command_Parameter_Pointer,"ramdata")
volatile Output_Command_Parameters_Type *Output_Command_Parameter_Pointer=NULL;
#pragma DATA_SECTION(transmitFlagsPtr,"ramdata")
static sTransmitFlags *transmitFlagsPtr = NULL;

extern Input_Command_Parameters_Type Requested_Command_Parameters;

extern Output_Command_Parameters_Type Calculated_Command_Parameters;
extern sTransmitFlags TxArbitrator;

//extern volatile struct _P2MOVE_VAR X_pToMoveVariablesStruct;


RWLK_Logic_Variables_Type RWLK_Logic_Vars;

/*******************************************************************************
 *  static functions
 ******************************************************************************/
static void ProcessReceivedCommand(void);
static void SystemTestFunction(void);
static void RWLK_Logic_Process_PerformSystemTesting(void);
static void RWLK_Logic_Process_Move(void);
static void RWLK_Logic_Process_LogMode(void);
static void RWLK_Logic_Process_CurrentTresholdUpdating(void);
static void RWLK_Logic_ReadTheCalibrationBoundaries(void);
static void RWLK_Logic_JointStatus(void);
static void RWLK_Logic_ReturnStatus(void);
static void RWLK_Logic_Process_SwitchMode(void);
static void RWLK_Logic_Process_IamAliveRequest(void);
static void RWLK_Logic_Process_XStopMotor(void);
static void RWLK_Logic_Process_GetXIncEncoderValue(void);
static void RWLK_Logic_Process_GetYIncEncoderValue(void);
static void RWLK_Logic_Process_GetAbsEncoderAngle(void);
static void RWLK_Logic_Process_SetAbsoluteEncoderAngle(void);
static void RWLK_Logic_Process_ActionCompleted(void);
static void RWLK_Logic_Process_AbsoluteEncoderProblem(void);
static void RWLK_Logic_Process_XAxisServoCurrentLoop(void);
static void RWLK_Logic_Process_YAxisServoCurrentLoop(void);
static void RWLK_Logic_Process_XAxisServoVelocityLoop(void);
static void RWLK_Logic_Process_YAxisServoVelocityLoop(void);
static void RWLK_Logic_Process_XAxisServoPositionLoop(void);
static void RWLK_Logic_Process_YAxisServoPositionLoop(void);
static void RWLK_Logic_Process_EncoderTest(void);
static void RWLK_Logic_Process_XAdjusterRequest(void);
static void RWLK_Logic_Process_YAdjusterRequest(void);
static void RWLK_Logic_Process_XReleaseMotor(void);
static void RWLK_Logic_Process_YReleaseMotor(void);
static void RWLK_Logic_Process_XAxisMoveOpenLoop(void);
static void RWLK_Logic_Process_YAxisMoveOpenLoop(void);
static void RWLK_Logic_Process_SetSvlSofPar(void);
static void RWLK_Logic_Process_SetSvlNfPar(void);
static void RWLK_Logic_Process_CalcSofPars(void);
static void RWLK_Logic_Process_XAxisSetSofPars_Msg(void);
static void RWLK_Logic_Process_YAxisSetSofPars_Msg(void);
static void RWLK_Logic_Process_CalcNfPars(void);
static void RWLK_Logic_Process_SendToClaXNfPars_Msg(void);
static void RWLK_Logic_Process_SendToClaYNfPars_Msg(void);
static void RWLK_Logic_Process_Handle_Modes_Setup();
static void RWLK_Logic_Process_Leg_Side();
static void RWLK_Logic_Process_SetPid(void);
static void RWLK_Logic_Process_RunTestFlag_Msg(void);
static void RWLK_Logic_Process_AccFactor(void);
static void RWLK_Logic_Process_WriteSamplingTime(void);
static void RWLK_Logic_Process_ReadSamplingTime(void);
static void RWLK_Logic_Process_FlashStore(void);
static void RWLK_Logic_Process_SetHWLimits(void);
static void RWLK_Logic_Process_UnreleaseXMotor(void);
static void RWLK_Logic_Process_UnreleaseYMotor(void);
static void RWLK_Logic_Process_ChangeXPwmCommand(void);
static void RWLK_Logic_Process_ChangeYPwmCommand(void);
static void RWLK_Logic_Process_GCSet(void);
static void RWLK_Logic_Process_Xcalibration(void);
static void RWLK_Logic_Process_Ycalibration(void);
static void RWLK_Logic_Process_StopCalibration(void);
static void RWLK_Logic_Process_XCommutationSetRequest(void);
static void RWLK_Logic_Process_YCommutationSetRequest(void);
static void RWLK_Logic_Process_XMotorEnable(void);
static void RWLK_Logic_Process_YMotorEnable(void);
static void RWLK_Logic_Process_XMotorDisable(void);
static void RWLK_Logic_Process_YMotorDisable(void);
static void RWLK_Logic_Process_XSetProfileParameterRequest(void);
static void RWLK_Logic_Process_YSetProfileParameterRequest(void);

static void RWLK_IamAliveHandle();

//#ifdef DEBUG_DEV_PHASE
static void RWLK_Logic_Process_MotorHeatingThreshold(void);
static void RWLK_Logic_Process_SetIncEncoderValue(void);
//#endif

/*****************************************************************************
 * Function name: RWLK_LogicHandlerInit
 *
 * Description: Initialize global structures and variables of the LogicHandler module.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
void RWLK_LogicHandlerInit(void)
{
	//uint32_t inputAddr, outputAddr;

	// Get my card Id.
	RWLK_Logic_Vars.MyCardId = CardConfigGetCardNumber();
	// Get the pointers to the input structure (data received from the CAN messages).
	//RWLK_CAN_GetInOutCommandsParameters(&inputAddr,&outputAddr);
	//Input_Command_Parameter_Pointer = (Input_Command_Parameters_Type *)inputAddr;
	//Output_Command_Parameter_Pointer = (Output_Command_Parameters_Type *)outputAddr;
	Input_Command_Parameter_Pointer = &Requested_Command_Parameters;
	Output_Command_Parameter_Pointer = &Calculated_Command_Parameters;
	transmitFlagsPtr = &TxArbitrator;
	RWLK_Logic_Vars.ManualModeFlag = false;
	RWLK_Logic_Vars.MaxPeakCurrentThreshold = MAX_CURRENT;
	RWLK_Logic_Vars.ButtonPushedFlag = false;
	RWLK_Logic_Vars.IamAliveActivateFlag = false;
	RWLK_Logic_Vars.IamAliveCounter = 0;
	RWLK_Logic_Vars.ByPassEnable = false;
	RWLK_Logic_Vars.FirstMoveafterBoot = false;
	RWLK_Logic_Vars.TestSystemCounter = 0;
	RWLK_Logic_Vars.TestSystemFlag = false; // this flag is to mark the test shall be perfomed, it is not the flag for the message to be sent back to the MPB
	RWLK_Logic_Vars.DataLogFlag = false; // current Implementation treating only
	RWLK_Logic_Vars.OpenLoopModeFlag = true;  // TODO: shall be connected to some logic.

	RWLK_MotorControlInit();
	//RWLK_CAN_GetTransmitFlagsPtr(transmitFlagsPtr);
	/* Initialize the manual handling module init here, since it is being activated by the logic handler only */
	RWLK_Manual_Buttons_HandlingInit();




}

/*****************************************************************************
 * Function name: RWLK_LogicHandler
 *
 * Description: Main procedure of the logic handler module.
 * 				Handle the process from taking the received command and its relevant parameters,
 * 				and after managing necessary logic, set needed data into CPU to CLA shared memory.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_LogicHandler, "ramfuncs")
void RWLK_LogicHandler(void)
{
#ifdef DEBUG_SYSTEM_STABILITY
	static uint32_t transmitFilter = 0;
#endif
	ProcessReceivedCommand();
	SystemTestFunction();
	/*========================
	 * I am alive handle	**
	 *=======================*/
	RWLK_IamAliveHandle();

if(med_ctr.parietic_side != MEDEXO_CONTROLLER_PARETIC_SIDE_NOT_SPECIFIED)
{
	RWLK_Stroke_Handler();
}

	ControlWordCheck();
#ifdef DEBUG_SYSTEM_STABILITY
	transmitFilter++;
	if (0 == (transmitFilter % 2000))
	{
		transmitFlagsPtr->TransmitSystemStabilityMessage = true;
		transmitFilter = 0;
	}

#endif


}

/*****************************************************************************
 * Function name: RWLK_LogicHandler
 *
 * Description: Main procedure of the logic handler module.
 * 				Handle the process from taking the received command and its relevant parameters,
 * 				and after managing necessary logic, set needed data into CPU to CLA shared memory.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(ProcessReceivedCommand, "ramfuncs")
static void ProcessReceivedCommand(void)
{
	if (Input_Command_Parameter_Pointer->ReceivedCommand != NO_REQUEST)
		Calculated_Command_Parameters.RequiredMode = Input_Command_Parameter_Pointer->ReceivedCommand;

	switch (Input_Command_Parameter_Pointer->ReceivedCommand)
	{
		case X_MOVE:
		case Y_MOVE:
			RWLK_Logic_Process_Move();
			break;
		case IAM_ALIVE:
			RWLK_Logic_Process_IamAliveRequest();
			break;
		case RETURN_STATUS:
			// Return status of either Joint angle (absoulute encoder) or motor current according to byte 1 of the command
			RWLK_Logic_ReturnStatus();
			break;
		case PERFORM_SYSTEM_TESTING:
			RWLK_Logic_Process_PerformSystemTesting();
			break;
		case SWITCH_MODE:
			printf("process SWITCH_MODE\n");
		//	RWLK_Logic_Process_SwitchMode();
			break;
		case X_STOP_MOTOR:
			RWLK_Logic_Process_XStopMotor();
			break;
		case Y_STOP_MOTOR:
			RWLK_Logic_Process_XStopMotor();
			break;
		case READ_LIMITS:   // 0x2E
			RWLK_Logic_ReadTheCalibrationBoundaries();
			break;
	/*	case EncoderTest:
			RWLK_Logic_Process_EncoderTest();
			break;*/
		case CURRENT_TRSH:	// 0x63
			RWLK_Logic_Process_CurrentTresholdUpdating();
			break;
		case LogMode:	// 0x6B
			RWLK_Logic_Process_LogMode();
			break;
		case JOINT_STATE:	// 0xFE
			RWLK_Logic_JointStatus();
			break;
		case GET_XINC_ENCODER_VALUE:
			RWLK_Logic_Process_GetXIncEncoderValue();
			break;
		case GET_YINC_ENCODER_VALUE:
			RWLK_Logic_Process_GetYIncEncoderValue();
			break;
		case GET_XABS_ENCODER_ANG:
			RWLK_Logic_Process_GetAbsEncoderAngle();
			break;
		case ACTION_COMPLETED:
			RWLK_Logic_Process_ActionCompleted();
			break;
		case ABS_ENCODER_PROBLEM:
			RWLK_Logic_Process_AbsoluteEncoderProblem();
			break;
		case XSCLKP:
		case XSCLKI:
		case XSCLLIM:
		case XSCLI10:
			RWLK_Logic_Process_XAxisServoCurrentLoop();
			break;
		case XSVLKP:
		case XSVLKI:
		case XSVLLIM:
		case XSVLI10:
			RWLK_Logic_Process_XAxisServoVelocityLoop();
			break;
		case XSPLKP:
		case XSPLKI:
		case XSPLLIM:
		case XSPLI10:
			RWLK_Logic_Process_XAxisServoPositionLoop();
			break;
		case X_VELOCITY_ADJUSTER:
		case X_POSITION_ADJUSTER:
		case X_CURRENT_ADJUSTER:
		case X_STOP_ADJUSTER:
		case X_SERVO_ANALYZER:
			RWLK_Logic_Process_XAdjusterRequest();
			break;
		case YSPLKP:
		case YSPLKI:
		case YSPLLIM:
		case YSPLI10:
			RWLK_Logic_Process_YAxisServoPositionLoop();
			break;
		case YSVLKP:
		case YSVLKI:
		case YSVLKD:
		case YSVLLIM:
		case YSVLI10:
			RWLK_Logic_Process_YAxisServoVelocityLoop();
			break;
		case YSCLKP:
		case YSCLKI:
		case YSCLLIM:
		case YSCLI10:
			RWLK_Logic_Process_YAxisServoCurrentLoop();
			break;
		case Y_VELOCITY_ADJUSTER:
		case Y_POSITION_ADJUSTER:
		case Y_CURRENT_ADJUSTER:
		case Y_STOP_ADJUSTER:
		case Y_SERVO_ANALYZER:
			RWLK_Logic_Process_YAdjusterRequest();
			break;
	/*	case SET_PID:
			RWLK_Logic_Process_SetPid();
			break;
			*/
		case SLVSOFBW:
		case SLVSOFDR:
			RWLK_Logic_Process_SetSvlSofPar();
			break;
		case(CALC_SLVSOF_PAR):
			RWLK_Logic_Process_CalcSofPars();
			break;

		case(XSLVSOF_PAR_SET):
			RWLK_Logic_Process_XAxisSetSofPars_Msg();
			break;

		case(YSLVSOF_PAR_SET):
			RWLK_Logic_Process_YAxisSetSofPars_Msg();
			break;

		case(SLVNATT):
		case(SLVNFREQ):
		case(SLVNWID):
				RWLK_Logic_Process_SetSvlNfPar();
				break;

		case(CALC_SLVNF_PAR):
				RWLK_Logic_Process_CalcNfPars();
				break;

		case(XSLVN_PAR_SET):
				RWLK_Logic_Process_SendToClaXNfPars_Msg();
				break;

		case(YSLVN_PAR_SET):
				RWLK_Logic_Process_SendToClaYNfPars_Msg();
				break;
		case(Handle_Modes_Setup):
				RWLK_Logic_Process_Handle_Modes_Setup();
				break;
		case(Leg_Side):
				RWLK_Logic_Process_Leg_Side();
				break;
		case ACC_FACTOR:
			RWLK_Logic_Process_AccFactor();
			break;
		case WRITE_SAMPLING_TIME:
			RWLK_Logic_Process_WriteSamplingTime();
			break;
		case READ_SAMPLING_TIME:
			RWLK_Logic_Process_ReadSamplingTime();
			break;
		case FLASH_STORE:
			RWLK_Logic_Process_FlashStore();
			break;
		case SET_HWLIMITS:
			RWLK_Logic_Process_SetHWLimits();
			break;
		case CHANGE_XPWM:
			RWLK_Logic_Process_ChangeXPwmCommand();
			break;
		case CHANGE_YPWM:
			RWLK_Logic_Process_ChangeYPwmCommand();
			break;
		case X_COMMUT_REQUEST:
			RWLK_Logic_Process_XCommutationSetRequest();
			break;
		case Y_COMMUT_REQUEST:
			RWLK_Logic_Process_YCommutationSetRequest();
			break;
		case XDWELL_TIME:
		case XPROFILE_TARGET:
		case XPROFILE_VEL:
		case XPROFILE_ACC:
		case XPROFILE_JERK:
		case XCURRENT_ADJUSTER_VALUE:
			// A common procedure is handling the profile parameter set requests commands
			RWLK_Logic_Process_XSetProfileParameterRequest();
			break;

		case YDWELL_TIME:
		case YPROFILE_TARGET:
		case YPROFILE_VEL:
		case YPROFILE_ACC:
		case YPROFILE_JERK:
		case YCURRENT_ADJUSTER_VALUE:
			// A common procedure is handling the profile parameter set requests commands
			RWLK_Logic_Process_YSetProfileParameterRequest();
			break;
		case GC_SET:
			RWLK_Logic_Process_GCSet();
			break;
		case XCALIBRATION:
			RWLK_Logic_Process_Xcalibration();
			break;
		case YCALIBRATION:
			RWLK_Logic_Process_Ycalibration();
			break;
		case STOP_CALIBRATION:
			RWLK_Logic_Process_StopCalibration();
			break;
		case RUNTESTFLAG:
			RWLK_Logic_Process_RunTestFlag_Msg();
			break;
		case X_MOTOR_ENABLE:
			RWLK_Logic_Process_XMotorEnable();
			break;
		case Y_MOTOR_ENABLE:
			RWLK_Logic_Process_YMotorEnable();
			break;
		case X_MOTOR_DISABLE:
		    RWLK_Logic_Process_XMotorDisable();
		    break;
		case Y_MOTOR_DISABLE:
		    RWLK_Logic_Process_YMotorDisable();
		    break;
		case X_RELEASE_MOTOR:
			RWLK_Logic_Process_XReleaseMotor();
			break;
		case Y_RELEASE_MOTOR:
			RWLK_Logic_Process_YReleaseMotor();
			break;
		case X_UNRELEASE_MOTOR:
			RWLK_Logic_Process_UnreleaseXMotor();
			break;
		case Y_UNRELEASE_MOTOR:
			RWLK_Logic_Process_UnreleaseYMotor();
			break;
		case X_MOVE_OPEN_LOOP:
			RWLK_Logic_Process_XAxisMoveOpenLoop();
		    break;
		case Y_MOVE_OPEN_LOOP:
			RWLK_Logic_Process_YAxisMoveOpenLoop();
			break;

//#ifdef DEBUG_DEV_PHASE
		case MOTOR_HEATING_THRESHOLD:
			RWLK_Logic_Process_MotorHeatingThreshold();
			break;
		case SET_INC_ENCODER_VALUE:
			RWLK_Logic_Process_SetIncEncoderValue();
			break;
		case SET_ABS_ENCODER_VALUE:
			RWLK_Logic_Process_SetAbsoluteEncoderAngle();
			break;
//#endif



		default:
			break;
	}
	// Mark ReceivedCommand with NO_REQUEST, in order not to repeat handling this request.
	Input_Command_Parameter_Pointer->ReceivedCommand = NO_REQUEST;
}


/*****************************************************************************
 * Function name: SystemTestFunction
 *
 * Description: System testing of the relevant test required
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(SystemTestFunction, "ramfuncs")
static void SystemTestFunction(void)
{
	uint16_t CalibrationTravelCheck;
	//uint8_t try = 0;//(uint8_t)(TestSystemFlag);

	if(RWLK_Logic_Vars.TestSystemFlag)
	{
		switch(RWLK_Logic_Vars.TestSystemCounter)
		{
			case(0):
				Output_Command_Parameter_Pointer->TestResultByte = 0 ;
				RWLK_Logic_Vars.TestSystemCounter = 8;
			//todo: THIS IS THE LINE THAT SHALL STAY INSTEAD OF THE UPPER ONE TestSystemCounter++;
			 	 break;

//			case(1): //CASE 1 : Test ADC & 12 Volt
////				//TestVariable = Sensor_ReadFSR( ADC_CH_12V_SAMPLE );  //ADC_CH_FS1_V
////				AdcErr 		 = ADC_Read ( ADC_CH_12V_SAMPLE , &TestVariable );
////				AdcRegs.ADCCHSELSEQ1.bit.CONV00 = ADC_CH_MOTOR_CURRENT;
////				if( AdcErr == ERR_OK ) //if ADC Converstion PASS
////				{
////					if(TestVariable < 2000) //check ADC Value if it's below 9 volts -
////					{
////						TestResultByte |= V12_FAILURE;
////						TestSystemCounter = 8;
////					}
////					else
////						TestSystemCounter++;
////
////				}
////				else
////				{
////					TestResultByte |=	ADC_FAILURE; //ADC Fail
//					TestSystemCounter = 8;
////				}
//
//
//
//				break;
//
//			case(2): //Case 2 check if 1.8 Volts & ADC
////				AdcErr 		 = ADC_Read ( ADC_CH_1_8V_SAMPLE , &TestVariable );
////				AdcRegs.ADCCHSELSEQ1.bit.CONV00 = ADC_CH_MOTOR_CURRENT;
////				if( AdcErr == ERR_OK ) //if ADC Converstion PASS
////				{
////					if(TestVariable < 1000) // Check if Voltage below 1.18 Volts
////					{
////						TestResultByte |= V1_8_FAILURE ;
//						TestSystemCounter = 8;
////					}
////					else
////						TestSystemCounter++;
////
////				}
////				else
////				{
////					TestResultByte |=	ADC_FAILURE; //ADC Fail
////					TestSystemCounter = 8;
////				}
//
//
//
//				break;
//
//			case(3): //Check Encoder Connection - If Encoder Connected
//
////					if (EncoderNCFlag && !LowPowerDetectioFlag)
////					{
////						TestResultByte |=	ABS_ENCODER_NC	; //Encoder Not Connected;
//							TestSystemCounter = 8;
////					//	EncoderNCFlag = FALSE;					//Should Be removed
////					}
////					else
////						TestSystemCounter++;
//
//
//				break;
//
//			case(4):	 //Check If there is Calbiration
////					if(!LimitsInitDone)
////					{
////						TestResultByte |= NO_CALBIRATION ;
////						TestSystemCounter = 8;
////					}
////					else
////						TestSystemCounter++;
//
//
//
//				break;
//
//	 		case(5): //check if Encoder Reading into Limits
////	 			  	if((FeedbackPosition >= HighSoftwareLimit + TargetRadius) || (FeedbackPosition <= LowSoftwareLimit - TargetRadius))
////	 				{
////	 					if(!LowPowerDetectioFlag)
////	 					{
////		 					X_AxisServoOff(); //stop motors
////			 				TestResultByte |= ENCODER_ERROR_POS ;
////			 				TestSystemCounter = 8 ;
////	 					}
////					}
////					else
////						TestSystemCounter++ ;
//
//
//	 			break;
//	 		case(6):
////				Encoder_Init();
////				CalibrationTravelCheck = HighSoftwareLimit - LowSoftwareLimit ;
////				if(((RWLK_Logic_Vars.MyCardId==CardConfig_e_LEFT_HIP_Card || RWLK_Logic_Vars.MyCardId==CardConfig_e_RIGHT_HIP_Card) & ((CalibrationTravelCheck) > HipTravelDistance)) || ((RWLK_Logic_Vars.MyCardId==CardConfig_e_LEFT_KNEE_Card || RWLK_Logic_Vars.MyCardId==CardConfig_e_RIGHT_KNEE_Card) & ((CalibrationTravelCheck) > KneeTravelDistance)))
////					{
////		 				TestSystemCounter++;
////					}
////	 			else
////	 				{
////	 					X_AxisServoOff(); //stop motors
////	 					TestResultByte |= CALIBRATION_LIMITS_ERROR ;
////						TestSystemCounter = 8;  //FeedbackPosition , LowSoftwareLimit
////	 				}
//	 		 		break;
//
//			case(7): //Based on low volatge test (should be more than 4 in the case )
//					//Low voltage counter is 4 Loops
//
////				if(!MotorOn)
////				{
////					TestResultByte =	MOTOR_ON_FAILURE;
////					TestSystemCounter = 8 ;
////					X_AxisServoOff();
////				}
////				else
////					TestSystemCounter++;
//			break;
//
			case(8):
					RWLK_Logic_Vars.TestSystemFlag = false;
					RWLK_Logic_Vars.TestSystemCounter = 0 ;
					transmitFlagsPtr->SystemTestMsgFlag = true;

					//SystemTest();   // TODO: orit this is the original function in ARGO that sends back the TEST_COMPLETED
				break;
		}
	 }

}



/*****************************************************************************
 * Function name: RWLK_Logic_Process_PerformSystemTesting
 *
 * Description: Process Income CAN BUS message, and keep the relevant parameters for later performing the request
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_PerformSystemTesting, "ramfuncs")
static void RWLK_Logic_Process_PerformSystemTesting(void)
{
	printf("in ProcessReceivedCommand1\n");
	// No paramters for this command
	RWLK_Logic_Vars.FirstMoveafterBoot = true;
	RWLK_Logic_Vars.TestSystemFlag = true;
	RWLK_Logic_Vars.TestSystemCounter = 0;
}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_Move
 *
 * Description: Handle the MOVE command, until sending the command to the motor shared memory
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_Move, "ramfuncs")
static void RWLK_Logic_Process_Move(void)
{
	uint16_t AxisCommand;

	// Prepare acknowledge parameters
	Calculated_Command_Parameters.DesiredPosition = Requested_Command_Parameters.DesiredPosition;
	Calculated_Command_Parameters.ProfileMaxVelocity = Requested_Command_Parameters.ProfileMaxVelocity;
	
	if(Requested_Command_Parameters.ReceivedCommand == X_MOVE)
	{
		AxisCommand = X_Axis;
		pToMoveVarStructsArray[AxisCommand]->StartTargetPoint = sControlVarStruct.XMotorPosition;
	}
	else if(Requested_Command_Parameters.ReceivedCommand == Y_MOVE)
	{
		AxisCommand = Y_Axis;
		pToMoveVarStructsArray[AxisCommand]->StartTargetPoint = sControlVarStruct.YMotorPosition;
	}
	pToMoveVarStructsArray[AxisCommand]->ProfileMaxVelocity = (float)Calculated_Command_Parameters.ProfileMaxVelocity;
	pToMoveVarStructsArray[AxisCommand]->ProfileAcceleration = pToMoveVarStructsArray[AxisCommand]->ProfileMaxVelocity * 10;
	pToMoveVarStructsArray[AxisCommand]->ProfileJerk = pToMoveVarStructsArray[AxisCommand]->ProfileAcceleration*10;
	pToMoveVarStructsArray[AxisCommand]->DesiredPosition = (int32_t)Calculated_Command_Parameters.DesiredPosition;
	pToMoveVarStructsArray[AxisCommand]->FinalTargetPoint = pToMoveVarStructsArray[AxisCommand]->DesiredPosition;


	if(AxisCommand == X_Axis)
	{
		ProfileParsPrepareToMove(pToMoveVarStructsArray[X_Axis], ProfileVarStructsArray[XCpuToClaProfileVars],CommonCpuProfileParametersStructsArray[X_Axis]);
	}
	else if(AxisCommand == Y_Axis)
	{
		ProfileParsPrepareToMove(pToMoveVarStructsArray[Y_Axis], ProfileVarStructsArray[YCpuToClaProfileVars],CommonCpuProfileParametersStructsArray[Y_Axis]);
	}

	//pToMoveVarStructsArray[AxisCommand]->ProfileAcceleration = pAdjusterParametersStruct.AdjusterProfileAcc;
	//pToMoveVarStructsArray[AxisCommand]->ProfileJerk = pAdjusterParametersStruct.AdjusterProfileJerk;
	
#if INCREMENTAL_ENCODER
	X_pToMoveVariablesStruct.ProfileMaxVelocity = pAdjusterParametersStruct.AdjusterProfileVelocity;
	X_pToMoveVariablesStruct.ProfileAcceleration = pAdjusterParametersStruct.AdjusterProfileAcc;
	X_pToMoveVariablesStruct.ProfileJerk = pAdjusterParametersStruct.AdjusterProfileJerk;
	X_pToMoveVariablesStruct.FinalTargetPoint = pAdjusterParametersStruct.AdjusterProfileTarget;
	X_pToMoveVariablesStruct.StartTargetPoint = sControlVarStruct.MotorPosition;
#endif
	//X_pToMoveVariablesStruct.FinalTargetPoint = Requested_Command_Parameters.DesiredPosition;
	//X_pToMoveVariablesStruct.DesiredPosition = Requested_Command_Parameters.DesiredPosition;
/*	X_pToMoveVariablesStruct.ProfileMaxVelocity = (float)Calculated_Command_Parameters.ProfileMaxVelocity;
	X_pToMoveVariablesStruct.ProfileAcceleration = X_pToMoveVariablesStruct.ProfileMaxVelocity*10.0;
	X_pToMoveVariablesStruct.ProfileJerk = X_pToMoveVariablesStruct.ProfileAcceleration*10.0;
	X_pToMoveVariablesStruct.DesiredPosition  = (int32_t)Calculated_Command_Parameters.DesiredPosition + sAbsoluteEncoderStruct.LowSoftwareLimit;
	if((X_pToMoveVariablesStruct.DesiredPosition > sAbsoluteEncoderStruct.HighSoftwareLimit)&&(X_pToMoveVariablesStruct.DesiredPosition < sAbsoluteEncoderStruct.LowSoftwareLimit))
	{
		//TODO Fault
	}
	else
	{
		X_pToMoveVariablesStruct.FinalTargetPoint = X_pToMoveVariablesStruct.DesiredPosition;
		X_pToMoveVariablesStruct.StartTargetPoint = X_sCpuToCla1ProfileCommandsStruct.AbsoluteEncoderFeedback;
		X_sCommonCpuProfileParametersStruct.TargetRadius = 10;
		X_sCommonCpuProfileParametersStruct.CheckSettling = 1;
		ProfileParsPrepareToMove(&X_pToMoveVariablesStruct, ProfileVarStructsArray,CommonCpuProfileParametersStructsArray, XCpuToClaProfileVars,X_Axis);
	}*/
	//ProfileParsPrepareToMove(volatile struct _P2MOVE_VAR *pToMoveStruct, volatile struct CPU_PROFILE_VAR *CpuProfilesArray[], PROFILE_STRUCT_MEMBERS ParrayMember);
	Input_Command_Parameter_Pointer->ReceivedCommand = NO_REQUEST;
}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_LogMode
 *
 * Description: Handle the log level configuration for the MCU messages
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_LogMode, "ramfuncs")
static void RWLK_Logic_Process_LogMode(void)
{
	if (Requested_Command_Parameters.LogLevel & 0x04)  // todo: define constatnt for the 0x4
	{
		RWLK_Logic_Vars.DataLogFlag = true; // Send Data MSG to LOG ;
	}
	else
	{
		RWLK_Logic_Vars.DataLogFlag = false; // DO NOT SEND DATA TO LOG ;
	}
	transmitFlagsPtr->XActionCompleteFlag = true;
}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_CurrentTresholdUpdating
 *
 * Description: Updating motors current threshold
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_CurrentTresholdUpdating, "ramfuncs")
static void RWLK_Logic_Process_CurrentTresholdUpdating(void)
{

	// These are the collected data for this command
//	Requested_Command_Parameters.CurrentHandleAlgo
//	Requested_Command_Parameters.Walk_Current_Threshold
//	Requested_Command_Parameters.Stairs_Current_Threshold

	transmitFlagsPtr->XActionCompleteFlag = true;
}

/*****************************************************************************
 * Function name: RWLK_Logic_ReadTheCalibrationBoundaries
 *
 * Description: Updating motors current threshold
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_ReadTheCalibrationBoundaries, "ramfuncs")
static void RWLK_Logic_ReadTheCalibrationBoundaries(void)
{
	uint32_t CurrFlashAddress;

	CurrFlashAddress = CALBIRATION_FIRST_BYTE;
	Calculated_Command_Parameters.CalibrationHighLimits = *(Uint16 *)CurrFlashAddress;
	CurrFlashAddress += 1;
	Calculated_Command_Parameters.CalibrationLowLimits = *(Uint16 *)CurrFlashAddress;

	/* TODO: Ask Arie about his value ????????? */
	Calculated_Command_Parameters.FeedbackConversionFlag = 2; /*FeedbackConversionFlag;*/
	CurrFlashAddress += 1;
	Calculated_Command_Parameters.HighSoftwareLimit = 33;   // TODO: ask Arie what to do here
	Calculated_Command_Parameters.LowSoftwareLimit = 23;

	transmitFlagsPtr->ReadCalibrationLimitsFlag = true;
	Input_Command_Parameter_Pointer->ReceivedCommand = NO_REQUEST;
}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_XAxisMoveOpenLoop
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  Msg_Data - struct of 8 bytes of data.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_XAxisMoveOpenLoop, "ramfuncs")
static void RWLK_Logic_Process_XAxisMoveOpenLoop(void)
{
	if(Requested_Command_Parameters.OpenLoopEnable)
	{
		MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.OPEN_LOOP = 1;
		sDigitalToAnalogSensorStruct.CurrentSensorResolution = (float)HALF_ADC_RESOLUTION/CURRENT_SENSOR_MEASUREMENT_RATE;
		sDigitalToAnalogSensorStruct.DigitalToAnalogOutputResolution = (float)HALF_DAC_RESOLUTION/(float)MAXIMUM_CURRENT_MEASUREMENT;
		sDigitalToAnalogSensorStruct.RelativeResolutionsCoefficient = sDigitalToAnalogSensorStruct.DigitalToAnalogOutputResolution/sDigitalToAnalogSensorStruct.CurrentSensorResolution;
		sDigitalToAnalogSensorStruct.HalfOfDigitalToAnalogResolution = (int32_t)HALF_DAC_RESOLUTION;
		SetOpenLoopPwm(Requested_Command_Parameters.PwmTimePar, &XBRLS_PWM);
	}
	else
	{
		MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.OPEN_LOOP = 0;
		SetOpenLoopPwm(0, &XBRLS_PWM);
		//PwmSetDriver( 0, 0, 0 );
	}
}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_YAxisMoveOpenLoop
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  Msg_Data - struct of 8 bytes of data.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_YAxisMoveOpenLoop, "ramfuncs")
static void RWLK_Logic_Process_YAxisMoveOpenLoop(void)
{
	uint16_t status=0;
	if(Requested_Command_Parameters.OpenLoopEnable)
	{
		MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.OPEN_LOOP = 1;
		sDigitalToAnalogSensorStruct.CurrentSensorResolution = (float)HALF_ADC_RESOLUTION/CURRENT_SENSOR_MEASUREMENT_RATE;
		sDigitalToAnalogSensorStruct.DigitalToAnalogOutputResolution = (float)HALF_DAC_RESOLUTION/(float)MAXIMUM_CURRENT_MEASUREMENT;
		sDigitalToAnalogSensorStruct.RelativeResolutionsCoefficient = sDigitalToAnalogSensorStruct.DigitalToAnalogOutputResolution/sDigitalToAnalogSensorStruct.CurrentSensorResolution;
		sDigitalToAnalogSensorStruct.HalfOfDigitalToAnalogResolution = (int32_t)HALF_DAC_RESOLUTION;
		SetOpenLoopPwm(Requested_Command_Parameters.PwmTimePar, &YBRLS_PWM);
		CPU_PHASE_ADVANCE = Requested_Command_Parameters.CommandA;
		status = Y_AxisServoOff();
		if(status == ERR_OK)
		{
			// TODO TxArbitrator.XActionCompleteFlag = true;
			CPU_PHASE_ADVANCE = Requested_Command_Parameters.CommandA;
	}
	else
	{
			// TODO TxArbitrator.ActionNotCompleteFlag = true;
	}
}
	else
	{
		MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.OPEN_LOOP = 0;
		SetOpenLoopPwm(0, &YBRLS_PWM);
	}
}

/*****************************************************************************
 * Function name: RWLK_Logic_JointStatus
 *
 * Description: Supply MCU joint status
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_JointStatus, "ramfuncs")
static void RWLK_Logic_JointStatus(void)
{

//	Calculated_Command_Parameters.CalibrationHighLimits = *(Uint16 *)CurrFlashAddress;
	// these three we jabe tp give
//	uint32_t WorkTimer;
//		int16_t FeedbackPosition;
//		uint16_t MotorCurrent;

	transmitFlagsPtr->JointStatusFlag = true;
	Input_Command_Parameter_Pointer->ReceivedCommand = NO_REQUEST;
}

/*****************************************************************************
 * Function name: RWLK_Logic_JointStatus
 *
 * Description: Supply either MCU joint angle or motor current according to the command parameters received.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_ReturnStatus, "ramfuncs")
static void RWLK_Logic_ReturnStatus(void)
{
	// Return status of either Joint angle (absoulute encoder) or motor current according to byte 1 of the command
		/*==============================
		 * Read Absolute encoder value *
		 *=============================*/
	// TODO: Check with Arie what shall be here exactly (how many options 0 1 2 3 or 0 1 or...)
		if (Input_Command_Parameter_Pointer->AbsEncoderReadFlag == true)
		{

//			if((OpenLoopModeFlag)||(read_notransformed_flag==1)||(!LimitsInitDone))
//			{
//				absenc_ang = EncoderPosition;
//			}
//			else if(read_notransformed_flag==0)
//			{
//				absenc_ang = FeedbackPosition;
//				if(CARD_NUMBER==0 || CARD_NUMBER==2)
//				{
//					absenc_angSend = (u16)(absenc_ang - LowSoftwareLimit);
//				}
//				else if(CARD_NUMBER==1 || CARD_NUMBER==3)
//				{
//					absenc_angSend = (u16)(absenc_ang - HighSoftwareLimit);
//				}
//			}
//			if(read_notransformed_flag==2)
//			{
//				absenc_angSend = (u16)EncoderData;
//			}

				transmitFlagsPtr->ReturnJointAngleFlag	= true;
				Input_Command_Parameter_Pointer->AbsEncoderReadFlag = false;

		}

}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_SwitchMode
 *
 * Description: Supply either MCU joint angle or motor current according to the command parameters received.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_SwitchMode, "ramfuncs")
static void RWLK_Logic_Process_SwitchMode(void)
{
/*
	//Calculated_Command_Parameters.RequiredMode = SWITCH_MODE;
	Calculated_Command_Parameters.ReceivedOperationalMode = Requested_Command_Parameters.OperationalMode;

//	MaxPeakCurrentThreshold = 1456 ;
//						MaxAverageCurrentStruct[0] =1456;
//						MaxAverageCurrentStruct[1] =1456;
//						MaxAverageCurrentStruct[2] =1456;
//						MaxAverageCurrentStruct[3] =1456;
//						Last_Mode = Mode;
//						Mode = CanBuslow.BYTE1;
//
//						if(OverLoadFlag)
//						{
//							MinPWM = RedFed_Time;
//							MaxPWM = PWM_Period - MinPWM;
//							X_AxisServoOff();
//							ServoOn();
//						}
//						OverLoadFlag	= FALSE;
//						MSGFlagReset	= TRUE;
//						PosErrorFlag	= TRUE;
//						if(Mode == _MANUAL)
//						{
//							 //asm( " ESTOP0");
//							 ManualModeFlag = TRUE;
//							 Last_Mode = _MANUAL;
//							 //OperationMode = _MANUAL;
//								if((CanBuslow.BYTE2>0)&&(CanBuslow.BYTE2<=0x44))
//									{
								//Vn=(float)(CanBuslow.BYTE2);
								//Vn=Vn*22.7527;
								//Vn=Vn*0.00025;
								//Vn=Vn*0.33333;
//									}
//								else
//									{
								//Vn = DiffultManualVelocity;
								//Vn=Vn*22.7527;
//								Vn=Vn*0.00025;
//								Vn=Vn*0.33333;
//									}
//
//									Vn_1 = 0;
//									Sn_1 = (float)FeedbackPosition;
//									Sn = Sn_1;
//									Profiler = 8;
//
//
//					   	}
//					    else
//					    {
//					    		PosErrorFlag	= FALSE;
//								if(Mode == _WALK)
//								{
//
//									POS_CURRENT_TRSH = WMPOS_CURRENT_TRSH;
//									NEG_CURRENT_TRSH = WMNEG_CURRENT_TRSH;
//								}
//								else
//								{
//									POS_CURRENT_TRSH = NWMPOS_CURRENT_TRSH;
//									NEG_CURRENT_TRSH = NWMNEG_CURRENT_TRSH;
//								}
//
//								if(Mode == _SIT)
//								{
//									X_AxisServoOff();
//									ServoOn();
//									SeatFlag 		= TRUE;
//									TempTimeCounter = 0; //Timer Counter Should be reset when switched mode to start timer again
//								}
//								else
//								{
//									SeatFlag = FALSE;
//								}
//								if(Mode == _SIT_TO_STAND)
//								{
//									X_AxisServoOff();
//									ServoOn();
//									PositionErrorThrshldLow = DesiredPosition - PositionErrorThresholdRange;
//									PositionErrorThrshldHigh = DesiredPosition + PositionErrorThresholdRange;
//
//								}
//								if(Mode == _STAND)
//								{
//									PositionErrorThrshldLow = DesiredPosition - PositionErrorThresholdRange;
//									PositionErrorThrshldHigh = DesiredPosition + PositionErrorThresholdRange;
//								}
//
//									//OperationMode = Mode;
//									ManualModeFlag = FALSE;
//									//Profiler = 0;
						 }

						Calculated_Command_Parameters.ReceivedOperationalMode = RWLK_Logic_Vars.Mode;
						Input_Command_Parameter_Pointer->ReceivedCommand = NO_REQUEST;
						// TODO: orit - verify the arbitrator is set on only after the change mode was actually completed.
						transmitFlagsPtr->SwitchModeCompleteFlag = true;
*/
}


/*****************************************************************************
 * Function name: RWLK_Logic_Process_IamAliveRequest
 *
 * Description: Update I am alive configuration according to request
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_IamAliveRequest, "ramfuncs")
static void RWLK_Logic_Process_IamAliveRequest(void)
{
	if(RWLK_Logic_Vars.MyCardId == CardConfig_e_LEFT_HIP_Card)
	{
		RWLK_Logic_Vars.ButtonPushedFlag = false;
		RWLK_Logic_Vars.IamAliveActivateFlag = Input_Command_Parameter_Pointer->IamAliveActivateFlag;
		RWLK_Logic_Vars.IamAliveCounter = 0;
		transmitFlagsPtr->XActionCompleteFlag = true;
	}
	else
	{
		RWLK_Logic_Vars.ButtonPushedFlag = false;
	}

}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_XStopMotor
 *
 * Description: Update I am alive configuration according to request
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_XStopMotor, "ramfuncs")
static void RWLK_Logic_Process_XStopMotor(void)
{
	RWLK_Logic_Vars.MovementFlag = false;
	RWLK_Logic_Vars.TestSystemFlag = false;
	RWLK_Logic_Vars.TestSystemCounter = 0;
	X_sCommonCpuProfileParametersStruct.ProfilerSwitch = 0;
	X_sCommonCpuProfileParametersStruct.TargetRadiusCounter = 0;
	// TODO: motorstop(TRUE);

}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_YStopMotor
 *
 * Description: Update I am alive configuration according to request
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_YStopMotor, "ramfuncs")
static void RWLK_Logic_Process_YStopMotor(void)
{
	RWLK_Logic_Vars.MovementFlag = false;
	RWLK_Logic_Vars.TestSystemFlag = false;
	RWLK_Logic_Vars.TestSystemCounter = 0;
	Y_sCommonCpuProfileParametersStruct.ProfilerSwitch = 0;
	Y_sCommonCpuProfileParametersStruct.TargetRadiusCounter = 0;

	// TODO: motorstop(TRUE);
}




/*****************************************************************************
 * Function name: RWLK_Logic_Process_GetXIncEncoderValue
 *
 * Description: Process Income command.
 * Parameters:  None
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_GetXIncEncoderValue, "ramfuncs")
static void RWLK_Logic_Process_GetXIncEncoderValue(void)
{
	// TODO: Arie, call the low level procedure
	transmitFlagsPtr->SendXIncrementalEncoderFeedbackFlag = true;
}


/*****************************************************************************
 * Function name: RWLK_Logic_Process_GetYIncEncoderValue
 *
 * Description: Process Income command.
 * Parameters:  None
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_GetYIncEncoderValue, "ramfuncs")
static void RWLK_Logic_Process_GetYIncEncoderValue(void)
{
	// TODO: Arie, call the low level procedure
	transmitFlagsPtr->SendYIncrementalEncoderFeedbackFlag = true;
}


/*****************************************************************************
 * Function name: RWLK_Logic_Process_GetAbsEncoderAngle
 *
 * Description: Process Income command.
 * Parameters:  None
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_GetAbsEncoderAngle, "ramfuncs")
static void RWLK_Logic_Process_GetAbsEncoderAngle(void)
{
	// TODO: Arie, call the low level procedure
	transmitFlagsPtr->SendAbsoluteEncoderFeedbackFlag = true;
}



/*****************************************************************************
 * Function name: RWLK_Logic_Process_ActionCompleted
 *
 * Description: Process Income command.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_ActionCompleted, "ramfuncs")
static void RWLK_Logic_Process_ActionCompleted(void)
{
	// TODO: ask Arie about this led
	// SET_LED2_VAL = 1;
	RWLK_Logic_Vars.ButtonPushedFlag = false;
}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_AbsoluteEncoderProblem
 *
 * Description: Process Income command.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_AbsoluteEncoderProblem, "ramfuncs")
static void RWLK_Logic_Process_AbsoluteEncoderProblem(void)
{
	// TODO: Arie
	//motorStop(FALSE);
	RWLK_Logic_Vars.AnotherAxisEncoderDisc = true;
}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_EncoderTest
 *
 * Description: Process Income command.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_EncoderTest, "ramfuncs")
static void RWLK_Logic_Process_EncoderTest(void)
{

}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_ReadSamplingTime
 *
 * Description: Process Income message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_ReadSamplingTime, "ramfuncs")
static void RWLK_Logic_Process_ReadSamplingTime(void)
{

}



/*****************************************************************************
 * Function name: RWLK_Logic_Process_XReleaseMotor
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_XReleaseMotor, "ramfuncs")
static void RWLK_Logic_Process_XReleaseMotor(void)
{
	MotorReleaseRequest(X_Axis);
}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_YReleaseMotor
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_YReleaseMotor, "ramfuncs")
static void RWLK_Logic_Process_YReleaseMotor(void)
{
	MotorReleaseRequest(Y_Axis);
}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_XAxisServoCurrentLoop
 *
 * Description: Handle the logic for ...
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_XAxisServoCurrentLoop, "ramfuncs")
static void RWLK_Logic_Process_XAxisServoCurrentLoop(void)
{
	//SetPiGains(F_DPI, pif, Input_Command_Parameter_Pointer->FilterMember, Input_Command_Parameter_Pointer->FilterValue);
	//SetPiGains(F_QPI, pif, Input_Command_Parameter_Pointer->FilterMember, Input_Command_Parameter_Pointer->FilterValue);
	SetClaPiGains(CLAF_DPI, Input_Command_Parameter_Pointer->PIFilterMember, Input_Command_Parameter_Pointer->FilterValue,X_Axis);
	//SetClaPiGains(CLAF_QPI, Input_Command_Parameter_Pointer->PIFilterMember, Input_Command_Parameter_Pointer->FilterValue,X_Axis);
}


/*****************************************************************************
 * Function name: RWLK_Logic_Process_YAxisServoCurrentLoop
 *
 * Description: Handle the logic for ...
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_YAxisServoCurrentLoop, "ramfuncs")
static void RWLK_Logic_Process_YAxisServoCurrentLoop(void)
{
	//SetPiGains(F_DPI, pif, Input_Command_Parameter_Pointer->FilterMember, Input_Command_Parameter_Pointer->FilterValue);
	//SetPiGains(F_QPI, pif, Input_Command_Parameter_Pointer->FilterMember, Input_Command_Parameter_Pointer->FilterValue);
	SetClaPiGains(CLAF_DPI, Input_Command_Parameter_Pointer->PIFilterMember, Input_Command_Parameter_Pointer->FilterValue,Y_Axis);
	//SetClaPiGains(CLAF_QPI, Input_Command_Parameter_Pointer->PIFilterMember, Input_Command_Parameter_Pointer->FilterValue,Y_Axis);
}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_XAxisServoVelocityLoop
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_XAxisServoVelocityLoop, "ramfuncs")
static void RWLK_Logic_Process_XAxisServoVelocityLoop(void)
{
	//SetPiGains(F_VPI, pif, Input_Command_Parameter_Pointer->FilterMember, Input_Command_Parameter_Pointer->FilterValue);
	SetClaPiGains(CLAF_VPI, Input_Command_Parameter_Pointer->PIFilterMember, Input_Command_Parameter_Pointer->FilterValue,X_Axis);
}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_YAxisServoVelocityLoop
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_YAxisServoVelocityLoop, "ramfuncs")
static void RWLK_Logic_Process_YAxisServoVelocityLoop(void)
{
	//SetPiGains(F_VPI, pif, Input_Command_Parameter_Pointer->FilterMember, Input_Command_Parameter_Pointer->FilterValue);
	SetClaPiGains(CLAF_VPI, Input_Command_Parameter_Pointer->PIFilterMember, Input_Command_Parameter_Pointer->FilterValue,Y_Axis);
	//SetClaPidGains(CLAF_VPI, Input_Command_Parameter_Pointer->PIFilterMember, Input_Command_Parameter_Pointer->FilterValue,Y_Axis);
}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_XAxisServoPositionLoop
 *
 * Description:
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_XAxisServoPositionLoop, "ramfuncs")
static void RWLK_Logic_Process_XAxisServoPositionLoop(void)
{
	//SetPiGains(F_PPI, pif, Input_Command_Parameter_Pointer->FilterMember, Input_Command_Parameter_Pointer->FilterValue);
	SetClaPiGains(CLAF_PPI, Input_Command_Parameter_Pointer->PIFilterMember, Input_Command_Parameter_Pointer->FilterValue,X_Axis);

}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_YAxisServoPositionLoop
 *
 * Description:
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_YAxisServoPositionLoop, "ramfuncs")
static void RWLK_Logic_Process_YAxisServoPositionLoop(void)
{
	//SetPiGains(F_PPI, pif, Input_Command_Parameter_Pointer->FilterMember, Input_Command_Parameter_Pointer->FilterValue);
	SetClaPiGains(CLAF_PPI, Input_Command_Parameter_Pointer->PIFilterMember, Input_Command_Parameter_Pointer->FilterValue,Y_Axis);
}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_XAdjusterRequest
 *
 * Description:
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_XAdjusterRequest, "ramfuncs")
static void RWLK_Logic_Process_XAdjusterRequest(void)
{
	SetAdjusterRequest(Input_Command_Parameter_Pointer->AdjusterRequest,MotorFalgsRegsArray[X_Axis],pAdjusterParametersStructsPointer[X_Axis], X_Axis);
}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_YAdjusterRequest
 *
 * Description:
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_YAdjusterRequest, "ramfuncs")
static void RWLK_Logic_Process_YAdjusterRequest(void)
{
	SetAdjusterRequest(Input_Command_Parameter_Pointer->AdjusterRequest,MotorFalgsRegsArray[Y_Axis],pAdjusterParametersStructsPointer[Y_Axis], Y_Axis);
}



/*****************************************************************************
 * Function name:RWLK_Logic_Process_SetSvlSofPar
 *
 * Description:
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_SetSvlSofPar, "ramfuncs")
static void RWLK_Logic_Process_SetSvlSofPar(void)
{

	//SetClaPiGains(CLAF_PPI, Input_Command_Parameter_Pointer->FilterMember, Input_Command_Parameter_Pointer->FilterValue,Y_Axis);
	SetClaSofPars(Input_Command_Parameter_Pointer->SOFilterMember, Input_Command_Parameter_Pointer->FilterValue);
}

/*****************************************************************************
 * Function name:RWLK_Logic_Process_SetSvlNfPar
 *
 * Description:
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_SetSvlSofPar, "ramfuncs")
static void RWLK_Logic_Process_SetSvlNfPar(void)
{
	SetClaNfPars(Input_Command_Parameter_Pointer->NFilterMember, Input_Command_Parameter_Pointer->FilterValue);
}


/*****************************************************************************
 * Function name:RWLK_Logic_Process_CalcSofPars
 *
 * Description:
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_CalcSofPars, "ramfuncs")
static void RWLK_Logic_Process_CalcSofPars(void)
{
	CalcSofParametrs();
}

/*****************************************************************************
 * Function name:RWLK_Logic_Process_XAxisSetSofPars_Msg
 *
 * Description:
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_XAxisSetSofPars_Msg, "ramfuncs")
static void RWLK_Logic_Process_XAxisSetSofPars_Msg(void)
{
	SetSofParametrs(VLSOF, X_Axis);
}

/*****************************************************************************
 * Function name:RWLK_Logic_Process_YAxisSetSofPars_Msg
 *
 * Description:
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_YAxisSetSofPars_Msg, "ramfuncs")
static void RWLK_Logic_Process_YAxisSetSofPars_Msg(void)
{
	SetSofParametrs(VLSOF,Y_Axis);
}



/*****************************************************************************
 * Function name:RWLK_Logic_Process_CalcNfPars
 *
 * Description:
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_CalcNfPars, "ramfuncs")
static void RWLK_Logic_Process_CalcNfPars(void)
{
	CalcNotchFilterParameters();
}


/*****************************************************************************
 * Function name:RWLK_Logic_Process_SendToClaXNfPars_Msg
 *
 * Description:
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_SendToClaXNfPars_Msg, "ramfuncs")
static void RWLK_Logic_Process_SendToClaXNfPars_Msg(void)
{
	SetSofParametrs(VLNF,X_Axis);
}


/*****************************************************************************
 * Function name:RWLK_Logic_Process_SendToClaYNfPars_Msg
 *
 * Description:
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_SendToClaYNfPars_Msg, "ramfuncs")
static void RWLK_Logic_Process_SendToClaYNfPars_Msg(void)
{
	SetSofParametrs(VLNF,Y_Axis);
}
/*****************************************************************************
 * Function name: RWLK_Logic_Process_Handle_Modes_Setup
 *
 * Description:
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_Handle_Modes_Setup, "ramfuncs")
static void RWLK_Logic_Process_Handle_Modes_Setup()
{
	switch(Requested_Command_Parameters.HandleStatus)
	{
	case 0x11:
		med_ctr.handle_mode = ACTIVE_MODE ;
		break;
	case 0x10:
		med_ctr.handle_mode = PRETENSION_SETTING_MODE ;
		med_ctr.my_struct_pf_pretension.initial_pretension = true;
		med_ctr.my_struct_df_pretension.initial_pretension = true;
	//	pToMoveVarStructsArray[X_Axis]->Pretension = true;
	//	pToMoveVarStructsArray[Y_Axis]->Pretension = true;
		break;
	case 0x01:
		med_ctr.handle_mode = PRETENSION_SETTING_MODE ;
		med_ctr.my_struct_pf_pretension.initial_pretension = true;
		med_ctr.my_struct_df_pretension.initial_pretension = true;
		break;
	case 0x00:
		med_ctr.handle_mode = INACTIVE_MODE ;
		break;

	}
	//RWLK_Handle_Update_Status(Requested_Command_Parameters.HandleStatus);

}
/*****************************************************************************
 * Function name: RWLK_Logic_Process_Leg_Side
 *
 * Description:
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_Leg_Side, "ramfuncs")
static void RWLK_Logic_Process_Leg_Side()
{

	switch(Requested_Command_Parameters.LegSideSelection)
	{
	case 0x10:
		med_ctr.parietic_side = MEDEXO_CONTROLLER_LEFT_PARIETIC;
		break;
	case 0x01:
		med_ctr.parietic_side = MEDEXO_CONTROLLER_RIGHT_PARIETIC;
		break;
	case 0x00:
		med_ctr.parietic_side = MEDEXO_CONTROLLER_PARETIC_SIDE_NOT_SPECIFIED;
		break;

	}
}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_SetPid
 *
 * Description: Process Income message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_SetPid, "ramfuncs")
static void RWLK_Logic_Process_SetPid(void)
{

}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_RunTestFlag_Msg
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_RunTestFlag_Msg, "ramfuncs")
static void RWLK_Logic_Process_RunTestFlag_Msg(void)
{

}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_WriteSamplingTime
 *
 * Description: Process Income message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_WriteSamplingTime, "ramfuncs")
static void RWLK_Logic_Process_WriteSamplingTime(void)
{

}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_FlashStore
 *
 * Description: Process Income message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_FlashStore, "ramfuncs")
static void RWLK_Logic_Process_FlashStore(void)
{

}

/*****************************************************************************
 * Function name: RWLK_CAN_Process_SetIncEncoderValue
 *
 * Description: Process Income command.
 * Parameters:  None
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_SetIncEncoderValue, "ramfuncs")
static void RWLK_Logic_Process_SetIncEncoderValue(void)
{
	// TODO: Arie, call the low level procedure
	// No reply shall be send.
}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_SetAbsoluteEncoderAngle
 *
 * Description: Process Income command.
 * Parameters:  None
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_SetAbsoluteEncoderAngle, "ramfuncs")
static void RWLK_Logic_Process_SetAbsoluteEncoderAngle(void)
{
	// TODO: Arie, call the low level procedure
	// No reply shall be send.

}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_AccFactor
 *
 * Description: Process Income command.
 * Parameters:  None
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_AccFactor, "ramfuncs")
static void RWLK_Logic_Process_AccFactor(void)
{

}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_SetHWLimits
 *
 * Description: Process Income command.
 * Parameters:  None
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_SetHWLimits, "ramfuncs")
static void RWLK_Logic_Process_SetHWLimits(void)
{

}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_UnreleaseXMotor
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_UnreleaseXMotor, "ramfuncs")
static void RWLK_Logic_Process_UnreleaseXMotor(void)
{
	MotorUnReleaseRequest(X_Axis);
}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_UnreleaseYMotor
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_UnreleaseYMotor, "ramfuncs")
static void RWLK_Logic_Process_UnreleaseYMotor(void)
{
	MotorUnReleaseRequest(Y_Axis);
}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_ChangeXPwmCommand
 *
 * Description: Process Income command.
 * Parameters:  None
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_ChangeXPwmCommand, "ramfuncs")
static void RWLK_Logic_Process_ChangeXPwmCommand(void)
{
	//PwmSetDriver(Input_Command_Parameter_Pointer->CommandA, Input_Command_Parameter_Pointer->CommandB, Input_Command_Parameter_Pointer->CommandC );
	SetOpenLoopPwm(Input_Command_Parameter_Pointer->CommandA, &XBRLS_PWM);
}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_ChangeYPwmCommand
 *
 * Description: Process Income command.
 * Parameters:  None
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_ChangeYPwmCommand, "ramfuncs")
static void RWLK_Logic_Process_ChangeYPwmCommand(void)
{
	//PwmSetDriver(Input_Command_Parameter_Pointer->CommandA, Input_Command_Parameter_Pointer->CommandB, Input_Command_Parameter_Pointer->CommandC );
	SetOpenLoopPwm(Input_Command_Parameter_Pointer->CommandA, &YBRLS_PWM);
}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_MotorHeatingThreshold
 *
 * Description: Process Income command.
 * Parameters:  None
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_MotorHeatingThreshold, "ramfuncs")
static void RWLK_Logic_Process_MotorHeatingThreshold(void)
{
	// TODO: process
}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_GCSet
 *
 * Description: Process Income command.
 * Parameters:  None
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_GCSet, "ramfuncs")
static void RWLK_Logic_Process_GCSet(void)
{

}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_Xcalibration
 *
 * Description: Process Income command.
 * Parameters:  None
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_Xcalibration, "ramfuncs")
static void RWLK_Logic_Process_Xcalibration(void)
{
	MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.CALIBRATION_REQUEST = 1;
	CommonCpuProfileParametersStructsArray[X_Axis] ->CalibrationSwitch = 1;
	CommonCpuProfileParametersStructsArray[X_Axis]->CalibrationCounter = 0;
}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_Ycalibration
 *
 * Description: Process Income command.
 * Parameters:  None
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_Ycalibration, "ramfuncs")
static void RWLK_Logic_Process_Ycalibration(void)
{
	MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.CALIBRATION_REQUEST = 1;
	CommonCpuProfileParametersStructsArray[Y_Axis] ->CalibrationSwitch = 1;
	CommonCpuProfileParametersStructsArray[Y_Axis]->CalibrationCounter = 0;
}


/*****************************************************************************
 * Function name: RWLK_Logic_Process_Ycalibration
 *
 * Description: Process Income command.
 * Parameters:  None
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_StopCalibration, "ramfuncs")
static void RWLK_Logic_Process_StopCalibration(void)
{
	MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.CALIBRATION_REQUEST = 0;
	MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.CALIBRATION_REQUEST = 0;
	CommonCpuProfileParametersStructsArray[Y_Axis] ->CalibrationSwitch = 0;
	CommonCpuProfileParametersStructsArray[Y_Axis]->CalibrationCounter = 0;

}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_XCommutationSetRequest
 *
 * Description: Process Income command.
 * Parameters:  None
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_XCommutationSetRequest, "ramfuncs")
static void RWLK_Logic_Process_XCommutationSetRequest(void)
{
	XBRLS_PWM = Input_Command_Parameter_Pointer->BRLS_PWM_value;
	MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.COMMUTATION = 1;
	XCommutState = 1;
}



/*****************************************************************************
 * Function name: RWLK_Logic_Process_YCommutationSetRequest
 *
 * Description: Process Income command.
 * Parameters:  None
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_YCommutationSetRequest, "ramfuncs")
static void RWLK_Logic_Process_YCommutationSetRequest(void)
{
	YBRLS_PWM = Input_Command_Parameter_Pointer->BRLS_PWM_value;
	MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.COMMUTATION = 1;
	YCommutState = 1;
}


/*****************************************************************************
 * Function name: RWLK_Logic_Process_XMotorEnable
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_XMotorEnable, "ramfuncs")
static void RWLK_Logic_Process_XMotorEnable(void)
{
	Calculated_Command_Parameters.ActionError = MotorEnableRequest(X_Axis);
}


/*****************************************************************************
 * Function name: RWLK_Logic_Process_YMotorEnable
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_YMotorEnable, "ramfuncs")
static void RWLK_Logic_Process_YMotorEnable(void)
{
	Calculated_Command_Parameters.ActionError = MotorEnableRequest(Y_Axis);
}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_XMotorDisable
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_XMotorDisable, "ramfuncs")
static void RWLK_Logic_Process_XMotorDisable(void)
{
	Calculated_Command_Parameters.ActionError = MotorDisableRequest(X_Axis);
}


/*****************************************************************************
 * Function name: RWLK_Logic_Process_YMotorDisable
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_YMotorDisable, "ramfuncs")
static void RWLK_Logic_Process_YMotorDisable(void)
{
	Calculated_Command_Parameters.ActionError = MotorDisableRequest(Y_Axis);
}

/*****************************************************************************
 * Function name: RWLK_Logic_Process_XSetProfileParameterRequest
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_XSetProfileParameterRequest, "ramfuncs")
static void RWLK_Logic_Process_XSetProfileParameterRequest(void)
{
	SetProfileParameters(Input_Command_Parameter_Pointer->TheParameterToSet,pAdjusterParametersStructsPointer[X_Axis],Input_Command_Parameter_Pointer->ProfileNewValue);
}



/*****************************************************************************
 * Function name: RWLK_Logic_Process_XXSetProfileParameterRequest
 *
 * Description: Process Income CAN BUS message.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Logic_Process_YSetProfileParameterRequest, "ramfuncs")
static void RWLK_Logic_Process_YSetProfileParameterRequest(void)
{
	SetProfileParameters(Input_Command_Parameter_Pointer->TheParameterToSet,pAdjusterParametersStructsPointer[Y_Axis],Input_Command_Parameter_Pointer->ProfileNewValue);
}


//#endif


/*****************************************************************************
 * Function name: RWLK_IamAliveHandle
 *
 * Description: Hanlde the I am alive mechanism
 * Parameters:  None.
 * Returns:     None.
 * Note: the I am alive handshake is working between MPB and Left Hip
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_IamAliveHandle, "ramfuncs")
static void RWLK_IamAliveHandle()
{
	if((CardConfig_e_LEFT_HIP_Card == RWLK_Logic_Vars.MyCardId)&&(RWLK_Logic_Vars.IamAliveActivateFlag))
	{
		RWLK_Logic_Vars.IamAliveCounter++;
		if(RWLK_Logic_Vars.IamAliveCounter>=I_AM_ALIVE_TIME)
		{
				RWLK_Logic_Vars.IamAliveActivateFlag = false;
//				Vn= MANUAL_DEFAULT_SPEED ;
//				Vn=Vn*22.7527;
//				Vn=Vn*0.00025;
//				Vn=Vn*0.33333;
//				Vn_1 = 0;
//				Sn_1 = (float)FeedbackPosition;
//				Sn = Sn_1;
//				Profiler = 8;
				RWLK_Logic_Vars.IamAliveCounter = 0;
//				Last_Mode = Mode;
//				Mode = _MANUAL;
				RWLK_Logic_Vars.ManualModeFlag = true;
//				Last_Mode = _MANUAL;
				//SendCommandtoanother(SWITCH_MODE,_MANUAL,MANUAL_DEFAULT_SPEED,true/*MCUinternalFlag*/);
				CAN_MessageTransmit(SWITCH_MODE, _MANUAL, MANUAL_DEFAULT_SPEED, 0, 0, 0, 0, true, CARD_MAILBOX_BROADCAST_TX, CAN_BROADCAST_MSG_ID, 8);
		}
	}

}



// No more

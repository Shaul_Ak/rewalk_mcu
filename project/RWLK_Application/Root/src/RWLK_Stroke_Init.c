#include "RWLK_Stroke_Init.h"
#include "RWLK_Common.h"
//#include "RWLK_Stroke_Common.h"
#include "RWLK_Timer_Driver.h"
#include "medexo_controller.h"
/*****************************************************************************
*
*: Function Name: RWLK_Stroke_Init
*: Abstract:
*
*: Preconditions:
*    None.
*: Postconditions:
*
*: Logic:
*
*****************************************************************************/
void RWLK_Stroke_Init()
{
	medexo_controller_setup(&med_ctr);
}

#include "RWLK_CardConfig.h"
#include "RWLK_CAN_Driver.h"

CardConfigObject_t sCardConfig;

/*****************************************************************************
*                                CONSTANTS
*****************************************************************************/
#define CardConfig_c_LEFT_LEG_FORWARD_DIR    false

#define CardConfig_c_LEFT_LEG_BACKWARD_DIR   true

#define CardConfig_c_RIGHT_LEG_FORWARD_DIR   true

#define CardConfig_c_RIGHT_LEG_BACKWARD_DIR  false

#ifdef JOINT_NEW_PINS
static uint16_t CardConfigDip1_GPIO = 158;    //Dip Switch one

static uint16_t CardConfigDip2_GPIO = 159;    //Dip Switch two

#else
static uint16_t CardConfigDip1_GPIO = 149;    //Dip Switch one

static uint16_t CardConfigDip2_GPIO = 150;    //Dip Switch two
#endif


/*****************************************************************************
*
*: Function Name: CardConfigInit
*: Abstract:
*
*: Preconditions:
*    None.
*: Postconditions:
*
*: Logic:
*
*****************************************************************************/
void CardConfigInit()
{
	GPIO_SetupPinMux(CardConfigDip1_GPIO, GPIO_MUX_CPU1, 0);
	GPIO_SetupPinOptions(CardConfigDip1_GPIO, GPIO_INPUT, GPIO_PUSHPULL);
	GPIO_SetupPinMux(CardConfigDip2_GPIO, GPIO_MUX_CPU1, 0);
	GPIO_SetupPinOptions(CardConfigDip2_GPIO, GPIO_INPUT, GPIO_PUSHPULL);

	sCardConfig.ConfigSwitch1 = GPIO_ReadPin(CardConfigDip1_GPIO);
	sCardConfig.ConfigSwitch2 = GPIO_ReadPin(CardConfigDip2_GPIO);

	if(!sCardConfig.ConfigSwitch1 )
	{
		if(!sCardConfig.ConfigSwitch2)
		{
			// Left hip and Emergency card
			sCardConfig.CardNumber = CardConfig_e_LEFT_HIP_Card;
			sCardConfig.moveDirection[CardConfig_e_move_forward]  = CardConfig_c_LEFT_LEG_FORWARD_DIR;
			sCardConfig.moveDirection[CardConfig_e_move_backward] = CardConfig_c_LEFT_LEG_BACKWARD_DIR;
			sCardConfig.MessageID = CAN_LEFT_HIP_MSG_ID;
	  	}
	  	else
	  	{
			// Right hip card
			sCardConfig.CardNumber = CardConfig_e_RIGHT_HIP_Card;
			sCardConfig.moveDirection[CardConfig_e_move_forward]  = CardConfig_c_RIGHT_LEG_FORWARD_DIR;
			sCardConfig.moveDirection[CardConfig_e_move_backward] = CardConfig_c_RIGHT_LEG_BACKWARD_DIR;
			sCardConfig.MessageID = CAN_RIGHT_HIP_MSG_ID;
	  	}
	}
	else
	{
		if(!sCardConfig.ConfigSwitch2)
		{
			// Left knee card
			sCardConfig.CardNumber = CardConfig_e_LEFT_KNEE_Card;
			sCardConfig.moveDirection[CardConfig_e_move_forward]  = CardConfig_c_LEFT_LEG_FORWARD_DIR;
			sCardConfig.moveDirection[CardConfig_e_move_backward] = CardConfig_c_LEFT_LEG_BACKWARD_DIR;
			sCardConfig.MessageID = CAN_LEFT_KNEE_MSG_ID;
		}
		else
		{
			// Right knee card
			sCardConfig.CardNumber = CardConfig_e_RIGHT_KNEE_Card;
			sCardConfig.moveDirection[CardConfig_e_move_forward]  = CardConfig_c_RIGHT_LEG_FORWARD_DIR;
			sCardConfig.moveDirection[CardConfig_e_move_backward] = CardConfig_c_RIGHT_LEG_BACKWARD_DIR;
			sCardConfig.MessageID = CAN_RIGHT_KNEE_MSG_ID;
		}
	}
	return;
}
/*****************************************************************************
*
*: Function Name: CardConfigGetMessageID
*: Abstract:
*
*: Preconditions:
*    None.
*: Postconditions:
*
*: Logic:
*
*****************************************************************************/
uint16_t CardConfigGetMessageID()
{
#if IDDK_MODULE == 0 && BOOSTXL == 0
	return sCardConfig.MessageID;
#else
	return CAN_LEFT_HIP_MSG_ID;
#endif
}
/*****************************************************************************
*
*: Function Name: CardConfigGetCardNumber
*: Abstract:
*
*: Preconditions:
*    None.
*: Postconditions:
*
*: Logic:
*
*****************************************************************************/
uint16_t CardConfigGetCardNumber()
{
	return sCardConfig.CardNumber;
}
/*****************************************************************************
*
*: Function Name: CardConfigGetMoveDirection
*: Abstract:
*
*: Preconditions:
*    None.
*: Postconditions:
*
*: Logic:
*
*****************************************************************************/
bool CardConfigGetMoveDirection(CardConfig_move_direction_t xi_direction)
{
	return sCardConfig.moveDirection[xi_direction];
}

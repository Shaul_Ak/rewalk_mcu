#include "RWLK_BLUE_GUI_API.h"


sBlue_Restore_GUI_Parameters    Blue_GUI_Parameters_Exchange;
_Bool GUI_Blue_New_Data_Recv_Flag = false;
long int Restore_System_Step_Counter = 0;
sBlue_MCU_GUI_Data mcu_gui_data;

char hex_value[9];

const char Bluetooth_GUI_Data_TAGS[LAST_BLUETOOTH_GUI_TAG][BLUETOOTH_GUI_TAGS_STRING_LENGTH+1] = {

	"MODE",
	"LEDS",
	"MOVX",
	"RFPX",
	"RFPY",
	"TIME",
	"MCSP",
	"MCGP",
	"PRF1",
	"PRF2",
	"PRF3",
	"DRF1",
	"DRF2",
	"DRF3",
	"AUMA",
	"KSEN#",
	"RESD#",
	"PARS#",
	"BATT#",
	"MCHW#",
	"MCSW#",
	"FATE#",
	"WARN#",
	"STCO#",
	"WAKS#",


     //Add command tags

};
//Sending hex chars
uint16_t _Convert_Float_ToHexString(char *x , float a )
{
	uint16_t  temp_h,temp_l, MSB_long,LSB_long;
	uint32_t  unsigned_long_par = 0;
    char hex_value_[8];
	     memset(hex_value , '\0' , 9);
	     memset(hex_value_ , '\0' , 8);
	     memcpy(&unsigned_long_par ,&a , 8); //float 8 chars because char = 2 bytes (2 bytes x 4 chars )
	     temp_h = (uint16_t) ( (unsigned_long_par>>16) & 0xFFFF);
	     MSB_long = sprintf(hex_value, "%x", temp_h);
	     temp_l = (uint16_t)(unsigned_long_par & 0xFFFF);
	     if(temp_l != 0)
	     {
	       LSB_long = sprintf(hex_value_, "%x", temp_l);
	       if(LSB_long<4){
	         strncat(hex_value ,"0000" , 4-LSB_long);
	         strcat(hex_value , hex_value_  );
	       }
	       else if(LSB_long == 4)
	       {
	    	   LSB_long = sprintf(hex_value+MSB_long, "%x", temp_l);
	       }

	       LSB_long = 4;
	     }
	     else
	     {
	    	 strncpy(hex_value+MSB_long,"0000",4);
	    	 LSB_long = 4;
	     }

	     return (MSB_long + LSB_long);

}
//Sending hex chars
uint16_t _Convert_S32_ToHexString(char *x , uint32_t a )
{
	uint16_t  temp_h,temp_l, MSB_long,LSB_long;
	uint32_t  unsigned_long_par = 0;
    char hex_value_[8];


	     memset(hex_value , '\0' , 9);
	     memset(hex_value_ , '\0' , 8);
	     memcpy(&unsigned_long_par ,&a , 8); //float 8 chars because char = 2 bytes (2 bytes x 4 chars )
	     temp_h = (uint16_t) ( (unsigned_long_par>>16) & 0xFFFF);
	     MSB_long = sprintf(hex_value, "%x", temp_h);
	     temp_l = (uint16_t)(unsigned_long_par & 0xFFFF);
	     if(temp_l != 0)
	     {
	       LSB_long = sprintf(hex_value_, "%x", temp_l);
	       if(LSB_long<4){
	         strncat(hex_value ,"0000" , 4-LSB_long);
	         strcat(hex_value , hex_value_  );
	       }
	       else if(LSB_long == 4)
	       {
	    	   LSB_long = sprintf(hex_value+MSB_long, "%x", temp_l);
	       }

	       LSB_long = 4;
	     }
	     else
	     {
	    	 strncpy(hex_value+MSB_long,"0000",4);
	    	 LSB_long = 4;
	     }

	     return (MSB_long + LSB_long);

}

/*  Sending binary values
uint16_t _Convert_Float_ToHexString(char *x , float a )
{

	uint16_t  temp_h,temp_l;//, MSB_long,LSB_long;
	uint32_t  unsigned_long_par = 0;

	     memset(hex_value , '\0' , 9);
	     memcpy(&unsigned_long_par ,&a , 8); //float 8 chars because char = 2 bytes (2 bytes x 4 chars )
	     temp_h = (uint16_t) ( (unsigned_long_par>>16) & 0xFFFF);

	    // MSB_long = sprintf(hex_value, "%x", temp_h);

	     hex_value[0] = (uint8_t) ( (temp_h>>8) & 0xFF);
	     hex_value[1] = (uint8_t) ( (temp_h) & 0xFF);

	     temp_l = (uint16_t)(unsigned_long_par & 0xFFFF);

	     hex_value[2] = (uint8_t) ( (temp_l>>8) & 0xFF);
	     hex_value[3] = (uint8_t) ( (temp_l) & 0xFF);


	     return (4);

}

*/


uint16_t _Convert_ShortInt_ToHexString(char *x , uint16_t a )
{
	uint16_t long_ , uint16_t_par;

	     memset(hex_value , '\0' , 9);
	     memcpy(&uint16_t_par ,&a , 4);//short 4 chars
	     long_ = sprintf(hex_value, "%x", uint16_t_par);

	     return (long_);

}
void Set_Restore_Mode(enum medexo_handle_mode mode )
{
	 med_ctr.handle_mode = mode;
	 Blue_GUI_Parameters_Exchange.Restore_Device_Status = mode;
}

enum medexo_handle_mode Get_Restore_Mode(void)
{
	return  med_ctr.handle_mode ;
	//return Blue_GUI_Parameters_Exchange.Restore_Device_Status;
}

GUI_BLUETOOTH_STATUS GUI_Blue_Status;




void Set_GUI_Blue_Conn_Status(GUI_BLUETOOTH_STATUS status)
{
	GUI_Blue_Status = status;
}

GUI_BLUETOOTH_STATUS  Get_GUI_Blue_Conn_Status(void)
{
	 return (GUI_Blue_Status);


}
void Enter_GUI_Blue_Data_Mode(void)
{

	if(Send_Melody_Command(ENTER_DATA_MODE_GUI) == MELODY_OK)
       GUI_Blue_Status = GUI_BLUE_DATA_MODE_ENTRY;
	else
		GUI_Blue_Status = GUI_BLUE_COMMAND_MODE;
}


void Update_Melody_Received_Data(void)
{

const char *blue_recv;

 blue_recv = Init_Blue_Recv_Data_Memory();
//check if valid message was received


	  if(NULL !=  strstr(blue_recv , MelodyBLU_Notifications[OPEN_OK_15]))  //OPEN SPP link by GUI
		  {
		   GUI_Blue_Status = GUI_BLUE_CONNECTED;
		   memset((void *)blue_recv,0,MAX_SCI_RECV_BYTES_COUNT);
		   Reset_SCIA_Recv_Byte_Index();
		  }
	  else if (NULL != strstr(blue_recv , MelodyBLU_Notifications[CLOSE_OK_15])) //Close SPP link by GUI
		  {
		   GUI_Blue_Status = GUI_BLUE_DISCONNECTED;
		   memset((void *)blue_recv,0,MAX_SCI_RECV_BYTES_COUNT);
		   Reset_SCIA_Recv_Byte_Index();
		  }

 }


void Recv_GUI_Restore_Blue_Data(void)
{
	 const char *blue_recv;
	 const char *gui_char_start , *gui_char_end;
	 blue_recv = Init_Blue_Recv_Data_Memory();

	  gui_char_end = strstr(blue_recv , "GUE");

	  if(NULL !=  gui_char_end)
	  {
	   gui_char_start =  strstr(blue_recv , "GP");
	   Update_GUI_Restore_Blue_Parameters_Exchange(gui_char_start + 2 );
	   memset((void *)blue_recv,0,MAX_SCI_RECV_BYTES_COUNT);
	   Reset_SCIA_Recv_Byte_Index();
	  }


}


void Send_GUI_EchoData_to_Print(char *message_to_print )
{
char blue_data_string[BLUETOOTH_MESSAGE_MAX_CHARS_LENGTH];

    memset(blue_data_string , '\0' , BLUETOOTH_MESSAGE_MAX_CHARS_LENGTH);
    strcpy(blue_data_string , message_to_print );
	strcat(blue_data_string , "$" ) ;
	strcat(blue_data_string , "\n\r");

	Send_GUI_Blue_Data(blue_data_string , MELODY_MESSAGE_TYPE_DATA);
}


void Send_GUI_Blue_Message_Long_Hex_Par(const char *tag , float  a )
{
	char blue_data_string [BLUETOOTH_MESSAGE_MAX_CHARS_LENGTH];




	   memset(blue_data_string , '\0' , BLUETOOTH_MESSAGE_MAX_CHARS_LENGTH);
	   strcpy(blue_data_string , tag);


	   _Convert_Float_ToHexString(hex_value , a);
	   strcat(blue_data_string ,hex_value);
	   strcat(blue_data_string ,"#");


	   strcat(blue_data_string , "%" ) ;
	   strcat(blue_data_string , "\n\r");

	   Send_GUI_Blue_Data(blue_data_string , MELODY_MESSAGE_TYPE_DATA);
}


void Send_GUI_Blue_Message_Long_Hex_Buf(const char *tag , float  *a ,uint32_t len)
{
	char blue_data_string [len*4];
    int i;


	   memset(blue_data_string , '\0' , len*4);
	   strcpy(blue_data_string , tag);

       for(i=0; i<len; i++)
       {
    	   _Convert_Float_ToHexString(hex_value , a[i]);
		   strcat(blue_data_string ,hex_value);
		   strcat(blue_data_string ,"#");
       }


	   strcat(blue_data_string , "$" ) ;
	   strcat(blue_data_string , "\n\r");

	   Send_GUI_Blue_Data(blue_data_string , MELODY_MESSAGE_TYPE_DATA);
}

void Init_MCU_GUI_Data(void)
{
	mcu_gui_data.Battery_Status = '7';
	mcu_gui_data.Fatal_Error_Status= 0;
	mcu_gui_data.Warning_Status = 0;
	mcu_gui_data.MCU_HW_Version = 0;
	strncpy(mcu_gui_data.MCU_SW_Version,"01.02",5);
	mcu_gui_data.Walking_Speed =  0;
	mcu_gui_data.Step_Total_Counter = 1234567;
}

MELODY_STATUS Send_RestoreDevice_Data_To_Bluetooth(void)
{
	char blue_data_string [sizeof(sBlue_MCU_GUI_Data)*2+1];




	   memset(blue_data_string , '\0' , sizeof(sBlue_MCU_GUI_Data)*2);
	   strncpy(blue_data_string , Bluetooth_GUI_Data_TAGS[RESTORE_DEVICE_DATA],5);

       //only for the show
	   if(mcu_gui_data.Battery_Status == '7')
	   {
		   mcu_gui_data.Battery_Status = '8';
	       strncat(blue_data_string ,"8", 1);
	   }
	   else
	   {
		   if(mcu_gui_data.Battery_Status == '8')
		   {
		     mcu_gui_data.Battery_Status = '7';
		     strncat(blue_data_string ,"7", 1);
		   }
	   }



	   strcat(blue_data_string ,"#");

	   _Convert_Float_ToHexString(hex_value , (float)mcu_gui_data.Step_Total_Counter);
	   strcat(blue_data_string ,hex_value);
	   strcat(blue_data_string ,"#");

	   strncat(blue_data_string ,mcu_gui_data.MCU_SW_Version,5);
	   strcat(blue_data_string ,"#");


	   strcat(blue_data_string , "%" ) ;
	   strcat(blue_data_string , "\n\r");

	   return (Send_GUI_Blue_Data(blue_data_string , MELODY_MESSAGE_TYPE_DATA));

}





void Init_Restore_System_Default_GUI_Data(void)
{
	Blue_GUI_Parameters_Exchange.Pf1 =  (uint16_t)med_ctr.handle_mode;
	Blue_GUI_Parameters_Exchange.Pf2 =  (uint16_t)med_ctr.my_struct_position_profile_input.f_pf_des;
	Blue_GUI_Parameters_Exchange.Pf3 = 0;
	Blue_GUI_Parameters_Exchange.Df1 =  (uint16_t)med_ctr.parietic_side;
	Blue_GUI_Parameters_Exchange.Df2 =  (uint16_t)med_ctr.my_struct_position_profile_input.p_df_des;
	Blue_GUI_Parameters_Exchange.Df3 = 0;
	Blue_GUI_Parameters_Exchange.OperationMode = 1;

}


_Bool  Send_RestoreDevice_Parameters_To_Bluetooth(void)
{
	char blue_data_string [sizeof(sBlue_Restore_GUI_Parameters)*4+1];
    static uint16_t cntr_para=0;
	   memset(blue_data_string , '\0' , sizeof(sBlue_Restore_GUI_Parameters)*2);
	   strncpy(blue_data_string ,Bluetooth_GUI_Data_TAGS[GUI_RESTORE_PARAMETERS],5);


	   _Convert_ShortInt_ToHexString(hex_value ,  Blue_GUI_Parameters_Exchange.Pf1);
	   strcat(blue_data_string ,hex_value);
	   strcat(blue_data_string ,"#");

	   _Convert_ShortInt_ToHexString(hex_value , Blue_GUI_Parameters_Exchange.Pf2);
	   strcat(blue_data_string ,hex_value);
	   strcat(blue_data_string ,"#");

	   _Convert_ShortInt_ToHexString(hex_value , Blue_GUI_Parameters_Exchange.Pf3);
	   strcat(blue_data_string ,hex_value);
	   strcat(blue_data_string ,"#");

	   _Convert_ShortInt_ToHexString(hex_value , Blue_GUI_Parameters_Exchange.Df1);
	   strcat(blue_data_string ,hex_value);
	   strcat(blue_data_string ,"#");

	   _Convert_ShortInt_ToHexString(hex_value, Blue_GUI_Parameters_Exchange.Df2);
	   strcat(blue_data_string ,hex_value);
	   strcat(blue_data_string ,"#");

	   _Convert_ShortInt_ToHexString(hex_value , Blue_GUI_Parameters_Exchange.Df3);
	   strcat(blue_data_string ,hex_value);
	   strcat(blue_data_string ,"#");

	   _Convert_ShortInt_ToHexString(hex_value , Blue_GUI_Parameters_Exchange.OperationMode);
	   strcat(blue_data_string ,hex_value);
	   strcat(blue_data_string ,"#");



	   strcat(blue_data_string , "%" ) ;
	   strcat(blue_data_string , "\n\r");

	   Send_GUI_Blue_Data(blue_data_string , MELODY_MESSAGE_TYPE_DATA);



	return true;

}




void Send_Sensor_Data_To_Bluetooth(struct sensor_protocol_struct sensor_output_data)
{
char blue_data_string [sizeof(struct sensor_protocol_struct)*8+1];//float and s32 here considered 2 bytes so multiple by 2 and 1 to end string '\0'
int i=0;



		   memset(blue_data_string , '\0' , sizeof(struct sensor_protocol_struct)*2);


		   strncpy(blue_data_string , Bluetooth_GUI_Data_TAGS[KST_SENSOR_DATA],5);

		   _Convert_S32_ToHexString( hex_value ,  (sensor_output_data.lc1 / 1000));
		   strcat(blue_data_string ,hex_value);
		   strcat(blue_data_string ,"#");
		   _Convert_S32_ToHexString( hex_value ,  (sensor_output_data.lc2 / 1000));
		   strcat(blue_data_string ,hex_value);
		   strcat(blue_data_string ,"#");

	       _Convert_Float_ToHexString( hex_value , sensor_output_data.imu_l[8]);
		   strcat(blue_data_string ,hex_value);
		   strcat(blue_data_string ,"#");
		   _Convert_Float_ToHexString( hex_value , sensor_output_data.imu_r[8]);
		   strcat(blue_data_string ,hex_value);
		   strcat(blue_data_string ,"#");
	       _Convert_Float_ToHexString( hex_value , 1);//pos1
		   strcat(blue_data_string ,hex_value);
		   strcat(blue_data_string ,"#");
		   _Convert_Float_ToHexString( hex_value ,2);//pos2
		   strcat(blue_data_string ,hex_value);
		   strcat(blue_data_string ,"#");
	       _Convert_Float_ToHexString( hex_value , 3);//Bat1V
		   strcat(blue_data_string ,hex_value);
		   strcat(blue_data_string ,"#");
		   _Convert_Float_ToHexString( hex_value ,4);//Bat1Cur
		   strcat(blue_data_string ,hex_value);
		   strcat(blue_data_string ,"#");
	       _Convert_Float_ToHexString( hex_value , 5);//Bat2V
		   strcat(blue_data_string ,hex_value);
		   strcat(blue_data_string ,"#");
		   _Convert_Float_ToHexString( hex_value ,6);//Bat2Cur
		   strcat(blue_data_string ,hex_value);
		   strcat(blue_data_string ,"#");
	       _Convert_Float_ToHexString( hex_value , 7);//Bat1Te
		   strcat(blue_data_string ,hex_value);
		   strcat(blue_data_string ,"#");
		   _Convert_Float_ToHexString( hex_value ,8);//Bat2Te
		   strcat(blue_data_string ,hex_value);
		   strcat(blue_data_string ,"#");
	       _Convert_Float_ToHexString( hex_value , 9);//Bat1RC
		   strcat(blue_data_string ,hex_value);
		   strcat(blue_data_string ,"#");
		   _Convert_Float_ToHexString( hex_value ,10);//Bat2RC
		   strcat(blue_data_string ,hex_value);
		   strcat(blue_data_string ,"#");
	       _Convert_Float_ToHexString( hex_value , 11);//Bat1RTE
		   strcat(blue_data_string ,hex_value);
		   strcat(blue_data_string ,"#");
		   _Convert_Float_ToHexString( hex_value ,12);//Bat2RTE
		   strcat(blue_data_string ,hex_value);
		   strcat(blue_data_string ,"#");






	   strcat(blue_data_string , "%" ) ;
	   strcat(blue_data_string , "\n\r");


       Send_GUI_Blue_Data(blue_data_string , MELODY_MESSAGE_TYPE_DATA);

}
void Send_GUI_Restore_Blue_Data(uint32_t msec_cnt)
{

    if(_Get_Is_SensorData_Available()) // check sensor data every cycle
	{

		  Send_Sensor_Data_To_Bluetooth(med_ctr.sensor_prot);

		  _Reset_Is_SensorData_Available();

	}

	else if(msec_cnt % 4999 == 0 ) // otherwise send sensor data every 5 second
	{

		  Send_Sensor_Data_To_Bluetooth(med_ctr.sensor_prot);

		  _Reset_Is_SensorData_Available();

	}

    else if(msec_cnt % 10188 == 0) //update mcu data (battery , steps , version every ~ 10 sec )
	{
		Send_RestoreDevice_Data_To_Bluetooth();
	}


	//else if(msec_cnt % 15800 == 0) //update mcu-alog parameters  every 15 seconds - if needed
    //{
	//  Send_RestoreDevice_Parameters_To_Bluetooth();
   // }
}


void GUI_Blue_Handler(uint32_t msec_cnt)
{
	static int  enter_data_mode_try = 0;
	static short int msec_cnt_temp = 0;
	Update_Melody_Received_Data();

	switch(Get_GUI_Blue_Conn_Status())
	{
	case GUI_BLUE_CONNECTED:
		Enter_GUI_Blue_Data_Mode();
		break;
	case GUI_BLUE_COMMAND_MODE:
		enter_data_mode_try++;
		if(enter_data_mode_try < 3)
			Set_GUI_Blue_Conn_Status(GUI_BLUE_CONNECTED);
		else
			enter_data_mode_try = 0;
		break;
	case GUI_BLUE_DATA_MODE_ENTRY:
		enter_data_mode_try = 0;
		Init_MCU_GUI_Data();
	//	Init_Restore_System_Default_GUI_Data();
        if(msec_cnt - msec_cnt_temp > 3 ||  msec_cnt_temp == 0  ) {
        	msec_cnt_temp = msec_cnt;
        	Init_Restore_System_Default_GUI_Data();
        	Send_RestoreDevice_Parameters_To_Bluetooth();


        	Set_GUI_Blue_Conn_Status( GUI_BLUE_DATA_MODE);
        	msec_cnt_temp = 0;

        }
        else
        {
        	msec_cnt_temp = msec_cnt;
        }
		break;
	case GUI_BLUE_DATA_MODE:
		Recv_GUI_Restore_Blue_Data();
		Send_GUI_Restore_Blue_Data(msec_cnt);
		med_ctr.handle_mode = (enum medexo_handle_mode)Blue_GUI_Parameters_Exchange.Pf1;

		if(med_ctr.handle_mode == PRETENSION_SETTING_MODE && !med_ctr.my_struct_pf_pretension.initial_pretension && !med_ctr.my_struct_df_pretension.initial_pretension)
		{
			med_ctr.my_struct_pf_pretension.initial_pretension = true;
			med_ctr.my_struct_df_pretension.initial_pretension = true;
		}
	    med_ctr.parietic_side = (enum medexo_controller_pariectic_side)Blue_GUI_Parameters_Exchange.Df1;

	    med_ctr.my_struct_position_profile_input.f_pf_des = (s32) Blue_GUI_Parameters_Exchange.Pf2;//150; //PF Force
        med_ctr.my_struct_position_profile_input.p_df_des = (s32) Blue_GUI_Parameters_Exchange.Df2;//15;//DF mm

		//Update_Restore_Statics_General_Data(msec_cnt);

		break;


	default:
		break;
	}
}








void Update_GUI_Restore_Blue_Parameters_Exchange(const char *blue_GUI_data_buffer)
{
	char *gui_data;
    char temp[5] ={0};
    char *p;

	    gui_data =  strstr(blue_GUI_data_buffer , Bluetooth_GUI_Data_TAGS[SYSTEM_MODE]);

	    if(gui_data != NULL)
	      {
	    	if(gui_data[BLUETOOTH_GUI_TAGS_STRING_LENGTH] == '#')
	    	{
	    		 switch(gui_data[BLUETOOTH_GUI_TAGS_STRING_LENGTH+1])
	    		    	  {
	    		    	  case '0':
	    		    		  Blue_GUI_Parameters_Exchange.Restore_Device_Status = INACTIVE_MODE;
	    		    		  GUI_Blue_New_Data_Recv_Flag = false;
	    		    		  break;
	    		    	  case '1':
	    		    		  Blue_GUI_Parameters_Exchange.Restore_Device_Status = ACTIVE_MODE;
	    		    		  GUI_Blue_New_Data_Recv_Flag = false ;
	    		    		  break;
	    		    	  default:
	    		    	      break;
	    		    	  }

	          }

	       }

        /*****************************************************************/

	    gui_data =  strstr(blue_GUI_data_buffer , Bluetooth_GUI_Data_TAGS[GUI_RESTORE_PARAMETERS_GET]);

	    if(gui_data != NULL)
	      {
	    	Send_RestoreDevice_Parameters_To_Bluetooth();
	      }
       /****************************************************************************************************/

	    gui_data =  strstr(blue_GUI_data_buffer , Bluetooth_GUI_Data_TAGS[LEDS]);

	    if(gui_data != NULL)
	      {
	    	if(gui_data[BLUETOOTH_GUI_TAGS_STRING_LENGTH] == '#')
	    	{
	    		 switch(gui_data[BLUETOOTH_GUI_TAGS_STRING_LENGTH+1])
	    		    	  {
	    		    	  case '0':
	    		    		  LEDs_OFF();
	    		    		  GUI_Blue_New_Data_Recv_Flag = false;
	    		    		  break;
	    		    	  case '1':
	    		    		  LEDs_ON();
	    		    		  GUI_Blue_New_Data_Recv_Flag = false ;
	    		    		  break;
	    		    	  default:
	    		    	      break;
	    		    	  }

	          }

	       }

       /****************************************************************************************************/
	    gui_data =  strstr(blue_GUI_data_buffer , Bluetooth_GUI_Data_TAGS[GUI_RESTORE_PARAMETERS_SET]);

	    if(gui_data != NULL)
	      {
	    	if(gui_data[BLUETOOTH_GUI_TAGS_STRING_LENGTH] == '#')
	    	{

	    	   strncpy(temp ,  (const char *)(gui_data + BLUETOOTH_GUI_TAGS_STRING_LENGTH +1)  , 5 );
	    	   Blue_GUI_Parameters_Exchange.Pf1 = (short int) strtoul(temp, &p, 10);
	    	   memset(temp ,4 ,0);
	    	   strncpy(temp , (const char *) (gui_data+BLUETOOTH_GUI_TAGS_STRING_LENGTH+1+5+1) ,5 );
	    	   Blue_GUI_Parameters_Exchange.Pf2 = (short int) strtoul(temp, &p, 10);
	    	   memset(temp ,4 ,0);
	    	   strncpy(temp , (const char *)(gui_data+BLUETOOTH_GUI_TAGS_STRING_LENGTH+1+5+1+5+1) ,5 );
	    	   Blue_GUI_Parameters_Exchange.Pf3 = (short int) strtoul(temp, &p, 10);
	    	   memset(temp ,4 ,0);
	    	   strncpy(temp , (const char *)(gui_data+BLUETOOTH_GUI_TAGS_STRING_LENGTH+1+5+1+5+1+5+1) , 5 );
	    	   Blue_GUI_Parameters_Exchange.Df1 = (short int) strtoul(temp, &p, 10);
	    	   memset(temp ,4 ,0);
	    	   strncpy(temp , (const char *)(gui_data+BLUETOOTH_GUI_TAGS_STRING_LENGTH+1+5+1+5+1+5+1+5+1) , 5 );
	    	   Blue_GUI_Parameters_Exchange.Df2 = (short int) strtoul(temp, &p, 10);
	    	   memset(temp ,4 ,0);
	    	   strncpy(temp , (const char *)(gui_data+BLUETOOTH_GUI_TAGS_STRING_LENGTH+1+5+1+5+1+5+1+5+1+5+1) , 5 );
	    	   Blue_GUI_Parameters_Exchange.Df3 = (short int) strtoul(temp, &p, 10);
	    	   memset(temp ,4 ,0);
	    	   strncpy(temp , (const char *)(gui_data+BLUETOOTH_GUI_TAGS_STRING_LENGTH+1+5+1+5+1+5+1+5+1+5+1+5+1) , 5 );
	    	   Blue_GUI_Parameters_Exchange.OperationMode = (short int) strtoul(temp, &p, 10);


	    	   if(Blue_GUI_Parameters_Exchange.OperationMode == 0)
	    	   {
	    		   LEDs_OFF();

	    	   }
	    	   else if(Blue_GUI_Parameters_Exchange.OperationMode == 1)
	    	   {
	    		   LEDs_ON();
	    	   }

	          }
	      }
	     /**********************************************************************************************************/

	   		    gui_data =  strstr(blue_GUI_data_buffer ,  Bluetooth_GUI_Data_TAGS[MOVE_MOTOR_X]);

	   		    if(gui_data != NULL)
	   		      {
	   		    	if(gui_data[BLUETOOTH_GUI_TAGS_STRING_LENGTH] == '#')
	   		    	{
	   		    		 switch(gui_data[BLUETOOTH_GUI_TAGS_STRING_LENGTH+1])
	   		    		    	  {
	   		    		    	  case '0':
	   		    		    		  LEDs_OFF(); //move x  forward
	   		    		    		  GUI_Blue_New_Data_Recv_Flag = false;
	   		    		    		  break;
	   		    		    	  case '1':
	   		    		    		  LEDs_ON();//move x backward
	   		    		    		  GUI_Blue_New_Data_Recv_Flag = false ;
	   		    		    		  break;
	   		    		    	  default:
	   		    		    	      break;
	   		    		    	  }

	   		          }

	   		       }

/************************************************************************************************************************************/
	   		    gui_data =  strstr(blue_GUI_data_buffer ,  Bluetooth_GUI_Data_TAGS[READ_FEEDBACK_Y]);

		   		    if(gui_data != NULL)
		   		      {
		   		    	//arie encoder
		   		    	//float encoder = 345 ;//sControlVarStruct.YMotorPosition;
		   		    	IncEncPos = sControlVarStruct.YMotorPosition;
		   		    	Send_GUI_Blue_Message_Long_Hex_Par("ENCY#", IncEncPos);
		   		      }

		   		    gui_data =  strstr(blue_GUI_data_buffer ,  Bluetooth_GUI_Data_TAGS[READ_FEEDBACK_X]);

					if(gui_data != NULL)
					  {
						//arie encoder
						//float encoder = 345 ;//sControlVarStruct.YMotorPosition;
						IncEncPos = sControlVarStruct.XMotorPosition;
						Send_GUI_Blue_Message_Long_Hex_Par("ENCX#", IncEncPos);
					  }




}



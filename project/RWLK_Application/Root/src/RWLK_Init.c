/*
 * RWLK_Init.c
 *
 *  Created on: 5 ���� 2016
 *      Author: Saman
 */

#include "RWLK.h"
#include "RWLK_Common.h"
#include "RWLK_Abs_Encoder_Application.h"
#include "RWLK_Cla_Control.h"
#include "RWLK_Motor_Control.h"
#include "RWLK_DRV8031_Applications.h"
#include "RWLK_CardConfig.h"
#include "RWLK_ExtFlash_application.h"


#include "cla_shared.h"
#include "RWLK_Abs_Encoder_Driver.h"
#include "RWLK_CAN_Driver.h"
#include "RWLK_EQep_Driver.h"
#include "RWLK_ePWM_Driver.h"
#include "RWLK_Timer_Driver.h"
#include "RWLK_SPI_Driver.h"
#include "RWLK_GPIO_Driver.h"
#include "RWLK_ADC_Driver.h"
#include "RWLK_IMU_Driver.h"
#include "RWLK_SCI_Driver.h"
#include "RWLK_Stroke_Init.h"
#include "RWLK_Sensor_Data.h"



//extern unsigned int Cla1funcsRunStart;
//extern unsigned int Cla1funcsLoadStart;
//extern unsigned int Cla1funcsLoadSize;
//*****************************************************************************
// function prototypes
//*****************************************************************************
void CLA_runTest(void);
void CPU_configRamMemory(void);
void CLA_configClaMemory(void);
void CLA_initCpu1Cla1(void);
extern void CalcSofParametrs(void);
//void EPWM_initEpwm(void);
//void ADC_initAdcA(void);

__interrupt void cla1Isr1();
__interrupt void cla1Isr2();
__interrupt void cla1Isr3();
__interrupt void cla1Isr4();
__interrupt void cla1Isr5();
__interrupt void cla1Isr6();
__interrupt void cla1Isr7();
__interrupt void cla1Isr8();

__interrupt void cpu_timer0_isr(void);

uint16_t RWLK_Init(void)
{

	uint16_t  ErrCode = ERR_OK;
	float Temp;



// Step 1. Initialize System Control:
// PLL, WatchDog, enable Peripheral Clocks
	InitSysCtrl();
	//asm( " ESTOP0");
	//CLA_configClaMemory();
	//asm( " ESTOP0");
	//*****************************************************************************
	// function definitions
	//*****************************************************************************
	//memcpy(&Cla1funcsRunStart, &Cla1funcsLoadStart, (size_t)&Cla1funcsLoadSize);
	// Step 2. Initialize GPIO:
    // This example function is found in the F2837xD_Gpio.c file and
    // illustrates how to set the GPIO to its default state.
	InitGpio();

    // Step 3. Clear all interrupts and initialize PIE vector table:
    // Disable CPU interrupts
	DINT;

    // Initialize the PIE control registers to their default state.
    // The default state is all PIE interrupts disabled and flags
    // are cleared.
    // This function is found in the F2837xD_PieCtrl.c file.
	InitPieCtrl();

    // Disable CPU interrupts and clear all CPU interrupt flags:
	IER = 0x0000;
	IFR = 0x0000;

    // Initialize the PIE vector table with pointers to the shell Interrupt
    // Service Routines (ISR).
    // This will populate the entire table, even if the interrupt
    // is not used in this example.  This is useful for debug purposes.
    // The shell ISR routines are found in F2837xD_DefaultIsr.c.
    // This function is found in F2837xD_PieVect.c.
    InitPieVectTable();

    ClaCpuStatusRegs.X_MFLAGS_CLASTATUS.all = 0;
    MotorFalgsRegsArray[X_Axis]->MFLAGS.all = 0;
    MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.BRUSHLESS = 1; //BRUSHLESS_MOTOR;
    ClaCpuStatusRegs.Y_MFLAGS_CLASTATUS.all = 0;
    MotorFalgsRegsArray[Y_Axis]->MFLAGS.all = 0;
    MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.BRUSHLESS = 1; //BRUSHLESS_MOTOR;
    CPU_configRamMemory();
    TablePointsNumber = LENGTH_OF_SIN_TABLE;
    SinTableCalc();

    XPhaseNumberOfPointsPerCount = ((float)LENGTH_OF_SIN_TABLE/(XMOTOR_REV_PER_ELECTRICAL_PERIOD*XMOTOR_ENCODER_RESOLUTION*4));
    YPhaseNumberOfPointsPerCount = ((float)LENGTH_OF_SIN_TABLE/(YMOTOR_REV_PER_ELECTRICAL_PERIOD*YMOTOR_ENCODER_RESOLUTION*4));
    //YPhaseNumberOfPointsPerCount = ((float)LENGTH_OF_SIN_TABLE/(YMOTOR_REV_PER_ELECTRICAL_PERIOD*YMOTOR_ENCODER_RESOLUTION*4));
    //YPhaseNumberOfPointsPerCount = 0;

    X_sCommonCpuProfileParametersStruct.TargetRadiusCounter = 0;
    Y_sCommonCpuProfileParametersStruct.TargetRadiusCounter = 0;
    X_sCommonCpuProfileParametersStruct.TargetRadius = 20;
    Y_sCommonCpuProfileParametersStruct.TargetRadius = 20;


// Init Position Loop filter
    xppi_cla.Kp = 0.00064;
    xppi_cla.Ki = 0.0;
    xppi_cla.Umax = 75.0;
    xppi_cla.Umin = -75.0;
    xppi_cla.i6 = 0;
    xppi_cla.i10 = 0;

    yppi_cla.Kp = 0.00128; //0.00256;
    yppi_cla.Ki = 0.0; //
    yppi_cla.Umax = 75.0;
    yppi_cla.Umin = -75.0;
    yppi_cla.i6 = 0;
    yppi_cla.i10 = 0;
    // Init Velocity Loop filters
    // Init Second order filter
    SamplingTime = SAMPLING_TIME;
    SamplingTimeQuad = SAMPLING_TIME*SAMPLING_TIME;
    InversSamplingTimeQuad = 1/SamplingTimeQuad;
    sVelLoopSecondOrderFilterStruct.SLVSOFBW = 100.0; //Set second order filter bandwidth
    sVelLoopSecondOrderFilterStruct.SLVSOFDR = 0.707; // Set second order filter damping ratio
    CalcSofParametrs();

	xvlpf_cla.a1 = sVelLoopSecondOrderFilterStruct.a1; //-1.692;
	xvlpf_cla.a2 = sVelLoopSecondOrderFilterStruct.a2; //0.7337;
	xvlpf_cla.b0 = sVelLoopSecondOrderFilterStruct.b0; //281.7;
	xvlpf_cla.b1 = sVelLoopSecondOrderFilterStruct.b1; //0.006213;
	xvlpf_cla.b2 = sVelLoopSecondOrderFilterStruct.b2; //281.7;

    sVelLoopSecondOrderFilterStruct.SLVSOFBW = 700.0; //Set second order filter bandwidth
    sVelLoopSecondOrderFilterStruct.SLVSOFDR = 0.707; // Set second order filter damping ratio
    CalcSofParametrs();

	yvlpf_cla.a1 = sVelLoopSecondOrderFilterStruct.a1; //-1.692;
	yvlpf_cla.a2 = sVelLoopSecondOrderFilterStruct.a2; //0.7337;
	yvlpf_cla.b0 = sVelLoopSecondOrderFilterStruct.b0; //281.7;
	yvlpf_cla.b1 = sVelLoopSecondOrderFilterStruct.b1; //0.006213;
	yvlpf_cla.b2 = sVelLoopSecondOrderFilterStruct.b2; //281.7;

	yvlpf_cla.x1 = 0;
	yvlpf_cla.x2 = 0;



	  sVelLoopSecondOrderFilterStruct.SLVSOFBW = 300.0; //Set second order filter bandwidth
	  sVelLoopSecondOrderFilterStruct.SLVSOFDR = 0.707; // Set second order filter damping ratio
	  CalcSofParametrs();

	  yrmslpf_cla.a1 = sVelLoopSecondOrderFilterStruct.a1; //-1.692;
	  yrmslpf_cla.a2 = sVelLoopSecondOrderFilterStruct.a2; //0.7337;
	  yrmslpf_cla.b0 = sVelLoopSecondOrderFilterStruct.b0; //281.7;
	  yrmslpf_cla.b1 = sVelLoopSecondOrderFilterStruct.b1; //0.006213;
	  yrmslpf_cla.b2 = sVelLoopSecondOrderFilterStruct.b2; //281.7;

	  yrmslpf_cla.x1 = 0;
	  yrmslpf_cla.x2 = 0;

	  sVelLoopSecondOrderFilterStruct.SLVSOFBW = 300.0; //Set second order filter bandwidth
	  sVelLoopSecondOrderFilterStruct.SLVSOFDR = 0.707; // Set second order filter damping ratio
	  CalcSofParametrs();
	  yvellpf_cla.a1 = sVelLoopSecondOrderFilterStruct.a1; //-1.692;
	  yvellpf_cla.a2 = sVelLoopSecondOrderFilterStruct.a2; //0.7337;
	  yvellpf_cla.b0 = sVelLoopSecondOrderFilterStruct.b0; //281.7;
	  yvellpf_cla.b1 = sVelLoopSecondOrderFilterStruct.b1; //0.006213;
	  yvellpf_cla.b2 = sVelLoopSecondOrderFilterStruct.b2; //281.7;

	  yvellpf_cla.x1 = 0;
	  yvellpf_cla.x2 = 0;




	// Init Velocirt PI filter
	xvpi1_cla.Kp = 51.2;
	xvpi1_cla.Ki = 0.0016;
	xvpi1_cla.Umax = 2048.0;
	xvpi1_cla.Umin = -2048.0;
	xvpi1_cla.i6 = 0;
	xvpi1_cla.i10 = 0;

	// 7 POLE MOTOR

	// Init Velocirt PI filter
	//yvpi1_cla.Kp = 25.6; //409.6;	//819.2;
	//yvpi1_cla.Ki = 0.0512; //0.0032;	//0.0064;
	//yvpi1_cla.Umax = 1240.0; //2040.0; //1800.0;
	//yvpi1_cla.Umin = -1240.0; //-2040.0;
	//yvpi1_cla.i6 = 0;
	//yvpi1_cla.i10 = 0;

	// 2 POLE MOTOR
	yvpi1_cla.Kp = 25.8; //409.6;	//819.2;
	yvpi1_cla.Ki = 0.0384; //0.0032;	//0.0064;
	yvpi1_cla.Umax = 1240.0; //1240.0; //2040.0; //1800.0;
	yvpi1_cla.Umin = -1240.0; //-1240.0;
	yvpi1_cla.i6 = 0;
	yvpi1_cla.i10 = 0;


	yvpid1_cla.c1 = 2*0.000159155;
	yvpid1_cla.c2 = SAMPLING_TIME - yvpid1_cla.c1;
	yvpid1_cla.c1 = SAMPLING_TIME + yvpid1_cla.c1;
	yvpid1_cla.c2 = yvpid1_cla.c2/yvpid1_cla.c1;
	yvpid1_cla.c1 = 2/yvpid1_cla.c1;

	yvpid1_cla.Kp = 64.0;
	yvpid1_cla.Ki = 0.016;
	yvpid1_cla.Kd = 0;
	yvpid1_cla.Umax = 1240.0; //2040.0; //1800.0;
	yvpid1_cla.Umin = -1240.0; //-2040.0;
	yvpid1_cla.i10 = 0;
	yvpid1_cla.i14 = 0;


    // Init Current loop PI filters
    xdpi1_cla.Kp = 0.8;
    xdpi1_cla.Ki = 0.238;
    xdpi1_cla.Umax = 1240.0;
    xdpi1_cla.Umin = -1240.0;
    xdpi1_cla.i6 = 0;
    xdpi1_cla.i10 = 0;

    xqpi1_cla.Kp = 0.8;
    xqpi1_cla.Ki = 0.238;
    xqpi1_cla.Umax = 1240.0;
    xqpi1_cla.Umin = -1240.0;
    xqpi1_cla.i6 = 0;
    xqpi1_cla.i10 = 0;


    ydpi1_cla.Kp = 0.32; //2.048;
    ydpi1_cla.Ki = 0.2048; //0.0064;
    ydpi1_cla.Umax = 1240.0;
    ydpi1_cla.Umin = -1240.0;
    ydpi1_cla.i6 = 0.0;
    ydpi1_cla.i10 = 0.0;

    yqpi1_cla.Kp = 0.32; //2.048;
    yqpi1_cla.Ki = 0.2048; //0.0064;
    yqpi1_cla.Umax = 1240.0;
    yqpi1_cla.Umin = -1240.0;
    yqpi1_cla.i6 = 0.0;
    yqpi1_cla.i10 = 0.0;

// RMS Motor Current zeroing
    claMotorCurrentsData.RMSTimer = 0;
    claMotorCurrentsData.XAxisRMSCurrent = 0;
    claMotorCurrentsData.XAxisTempCurrent = 0;
    claMotorCurrentsData.YAxisRMSCurrent = 0;
    claMotorCurrentsData.YAxisTempCurrent = 0;



    CPU_PHASE_ADVANCE = 0;

  // INIT Arrays
  /*  MotorFalgsRegsArray[0] = &xMotorFlagsReg;
    MotorFalgsRegsArray[1] = &yMotorFlagsReg;
    ProfileVarStructsArray[0]=&XcCpuToClaProfileVarStruct;
    ProfileVarStructsArray[1]=&XFirstTargetPointProfileVarStruct;
    ProfileVarStructsArray[2]=&XSecondTargetPointProfileVarStruct;
    ProfileVarStructsArray[3]=&YcCpuToClaProfileVarStruct;
    ProfileVarStructsArray[4]=&YFirstTargetPointProfileVarStruct;
    ProfileVarStructsArray[5]=&YSecondTargetPointProfileVarStruct;
    pToMoveVarStructsArray[0]=&X_pToMoveVariablesStruct;
    pToMoveVarStructsArray[1]=&Y_pToMoveVariablesStruct;
*/

    CLA_configClaMemory();
	CLA_initCpu1Cla1();

    xMotorFlagsReg.MFLAGS.bit.CURRENT_LOOP = 1;
    yMotorFlagsReg.MFLAGS.bit.CURRENT_LOOP = 0;

// Step 4. Timer initialization
    D_InitCpuTimers();
    EALLOW;  // This is needed to write to EALLOW protected registers
    PieVectTable.TIMER0_INT = &cpu_timer0_isr;
    EDIS;    // This is needed to disable write to EALLOW protected registers
    D_Timer_EnableInterrupt ( TIMER_0 );

	IER |= M_INT1;
	IER |= M_INT2;


	//**************Rasem: add before enabling global interrupts EINT of CPU ********************
	//**********************scia and scic Bluetooth and Debugger init****************************
	GPIO_LEDs_Config(); //Config board's LEDS
	           //TX  FIFO                RX FIFO
	sci_a_init(FIFO_DEPTH_1 , FIFO_DEPTH_1 , 460800);//Bluetooth Module
	sci_c_init(FIFO_DEPTH_1 , FIFO_DEPTH_1 , 115200);//PC terminal Debugger

    //*********************************************************************************************/

// Step 5. Enable global Interrupts and higher priority real-time debug events:
	EINT;  // Enable Global interrupt INTM
	ERTM;  // Enable Global realtime interrupt DBGM

	//bissc_init();
	CardConfigInit();

	//*******************CAN bus Init********************************
   // ErrCode = CAN_Init(CANA_BASE,CPU_FREQ_200MHZ, CAN_BUS_SPEED_500KHZ);
    ErrCode = CAN_Init(CANA_BASE,CPU_FREQ_200MHZ,CAN_BUS_SPEED_1MHZ);
    //*******************Incremental Encoder Init********************
    //ErrCode = EQep_Init(QEep1BlockNum,1000000000,0);	// INIT INCREMENTAL ENCODER BLOCK NUMBER 1, INIT MAX_ENCODER_VALUE AND START_ENCODER_VALUE

    XPolePairsNumber = XMOTOR_POLES_PAIRS;
    YPolePairsNumber = YMOTOR_POLES_PAIRS;
    TablePointsNumber = LENGTH_OF_SIN_TABLE;
    Temp = (float)TablePointsNumber;
    Temp = Temp/3;
    PiDiv3TablePointsNumber = (uint16_t)Temp;

    Temp = (float)TablePointsNumber;
    Temp = Temp/4;
    PiDiv2TablePointsNumber = (uint16_t)Temp;


    XEncoderResolution = XMOTOR_ENCODER_RESOLUTION;
   	XEncoderResolution = XEncoderResolution*4;
    YEncoderResolution = YMOTOR_ENCODER_RESOLUTION;
   	YEncoderResolution = YEncoderResolution*4;
    // Commutation Resolution Calculation
    XCommutationFactor = (float)XPolePairsNumber/(float)(XEncoderResolution);
    YCommutationFactor = (float)YPolePairsNumber/(float)(YEncoderResolution);

    //YEncoderResolution = (YEncoderResolution<<=2);
    ErrCode = EQep_Init(QEep1BlockNum,(XEncoderResolution - 1),0);	// INIT INCREMENTAL ENCODER BLOCK NUMBER 1, INIT MAX_ENCODER_VALUE AND START_ENCODER_VALUE
    ErrCode = EQep_Init(QEep3BlockNum,(YEncoderResolution - 1),0);	// INIT INCREMENTAL ENCODER BLOCK NUMBER 1, INIT MAX_ENCODER_VALUE AND START_ENCODER_VALUE
    //YEncoderResolution = (YEncoderResolution>>=2);

    //******************PWM Blocks Init******************************
    ErrCode = EPWM_IO_Init(ePWM1BlockNum);
    ErrCode = EPWM_IO_Init(ePWM2BlockNum);
    ErrCode = EPWM_IO_Init(ePWM3BlockNum);
    ErrCode = EPWM_IO_Init(ePWM10BlockNum);
    ErrCode = EPWM_IO_Init(ePWM11BlockNum);
    ErrCode = EPWM_IO_Init(ePWM12BlockNum);

    //******************GC Block Init********************************
#if IDDK_MODULE == 0 && BOOSTXL == 0
    	GC_Init();

    //*********** Safety GPIO Init function ***************************
    	SafetyGPIO_Init();

    // ************ Motor Hall effect sensors inputs init *************
    	HallEffectMotorSensors_Init();


    //******************Current Sensors Power Enable******************
    	CurrenSensEnable_Init();
#endif

        //***************Motor Driver Init******************************
//    #if IDDK_MODULE == 0 && BOOSTXL == 1
//        DRV8031_Init(SPIB_BASE , BITS_16 , SPI_BITRATE_128 );
//        DELAY_US(1000);
//    #endif

#if IDDK_MODULE == 0 && BOOSTXL == 0
        ErrCode = ExtrFlash_Init();
        ErrCode = AbsEncoder_Init();
    #endif

#if IDDK_MODULE == 0
        //CHECK:
        DRV8031_Init(SPIA_BASE , BITS_16 , SPI_BITRATE_128 );
        DELAY_US(10000);
       // goto CHECK;
 #endif
    //*************ADC Block Configuration*****************
    sDigitalToAnalogSensorStruct.PositionResolutionCoefficient =  (float)HALF_DAC_RESOLUTION/MAX_RELATIVE_POSITION_MEASUREMENTS;
    sDigitalToAnalogSensorStruct.PositionErrorResolutionCoefficient = (float)HALF_DAC_RESOLUTION/MAX_POSITION_ERROR_MEASUREMENT;
    ConfigureADC();
    DELAY_US(20);
    SetupADCSoftware(0);
    DELAY_US(20);
    XZeroCalibratedAchannel = CalibrateADCSoftware(0);
    XZeroCalibratedBchannel = CalibrateADCSoftware(1);
    XZeroCalibratedCchannel = CalibrateADCSoftware(2);
    YZeroCalibratedAchannel = CalibrateADCSoftware(3);
    YZeroCalibratedBchannel = CalibrateADCSoftware(4);


    //SetupADCSoftware(5);
    SetupADCSoftware(23);
    DELAY_US(20);
   // PieVectTable.ADCA1_INT = &ADCA1_ISR;

    ErrCode = PwmInitDriver( 0x9c4, 1250, 0x0064);
    ErrCode = D_Timer_Start(TIMER_0,1000);

    // Configure timers 1 and 2 to be milisec and microsec counters used for WYSS Algo
    D_Timer_Config_mSec_Timer();
    D_Timer_Config_uSec_Timer();

    //************************* IMU *********************************//
#if IDDK_MODULE == 0 && BOOSTXL == 0
    // TODO: orit - move SPI to A and then open this line on the new board
	//IMU_init_device();
#endif

 
   // INIT Arrays
      MotorFalgsRegsArray[0] = &xMotorFlagsReg;
      MotorFalgsRegsArray[1] = &yMotorFlagsReg;
      ProfileVarStructsArray[0]=&XcCpuToClaProfileVarStruct;
      ProfileVarStructsArray[1]=&XFirstTargetPointProfileVarStruct;
      ProfileVarStructsArray[2]=&XSecondTargetPointProfileVarStruct;
      ProfileVarStructsArray[3]=&YcCpuToClaProfileVarStruct;
      ProfileVarStructsArray[4]=&YFirstTargetPointProfileVarStruct;
      ProfileVarStructsArray[5]=&YSecondTargetPointProfileVarStruct;
      pToMoveVarStructsArray[0]=&X_pToMoveVariablesStruct;
      pToMoveVarStructsArray[1]=&Y_pToMoveVariablesStruct;
      sMotorControlVariablesStructsArray[0]=&X_sMotorControlVariablesStruct;
      sMotorControlVariablesStructsArray[1]=&Y_sMotorControlVariablesStruct;




      	  YsDigitalToAnalogCPUSensorStruct.CurrentSensorResolution = (float)(2*HALF_ADC_RESOLUTION)/Y_AXIS_CURRENT_SENSOR_MEASUREMENT_RATE;
      	  YsDigitalToAnalogCPUSensorStruct.DigitalToAnalogOutputResolution = (float)(2*HALF_DAC_RESOLUTION)/(float)Y_AXIS_MAXIMUM_CURRENT_MEASUREMENT;
      	  YsDigitalToAnalogCPUSensorStruct.RelativeResolutionsCoefficient = YsDigitalToAnalogCPUSensorStruct.DigitalToAnalogOutputResolution/YsDigitalToAnalogCPUSensorStruct.CurrentSensorResolution;
      	  YsDigitalToAnalogCPUSensorStruct.HalfOfDigitalToAnalogResolution = (int32_t)HALF_DAC_RESOLUTION;

      	Y_sCommonCpuProfileParametersStruct.YAxisMotorMaxPermitCurrent = Y_AXIS_BITS_PER_AMPER*Y_AXIS_MAXIMUM_PERMITTED_RMS_CURRENT;
      	Y_sCommonCpuProfileParametersStruct.OvercurrentFaultFlag = false;



    	  XsDigitalToAnalogCPUSensorStruct.CurrentSensorResolution = (float)(HALF_ADC_RESOLUTION)/X_AXIS_CURRENT_SENSOR_MEASUREMENT_RATE;
    	  XsDigitalToAnalogCPUSensorStruct.DigitalToAnalogOutputResolution = (float)(HALF_DAC_RESOLUTION)/(float)X_AXIS_MAXIMUM_CURRENT_MEASUREMENT;
    	  XsDigitalToAnalogCPUSensorStruct.RelativeResolutionsCoefficient = XsDigitalToAnalogCPUSensorStruct.DigitalToAnalogOutputResolution/XsDigitalToAnalogCPUSensorStruct.CurrentSensorResolution;
    	  XsDigitalToAnalogCPUSensorStruct.HalfOfDigitalToAnalogResolution = (int32_t)HALF_DAC_RESOLUTION;

    	X_sCommonCpuProfileParametersStruct.XAxisMotorMaxPermitCurrent = X_AXIS_BITS_PER_AMPER*X_AXIS_MAXIMUM_PERMITTED_RMS_CURRENT;
    	X_sCommonCpuProfileParametersStruct.OvercurrentFaultFlag = false;


    CalcCommutCommands();
   RWLK_Stroke_Init();

// Shaul Debug May 2017
    DefineDataFlagSensEnable_GPIO();
	RWLK_SensorData_CommandInit();
//*************************************************************************

    MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.COMMUTATION = 1;
    XCommutState = 1;
    XBRLS_PWM = 0x100;
    MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.COMMUTATION = 1;
    YCommutState = 1;
    YBRLS_PWM = 0x100;

    return ErrCode;

}


void CPU_configRamMemory(void)
{
//	extern uint32_t RamfuncsRunStart, RamfuncsLoadStart, RamfuncsLoadSize;

	EALLOW;

#ifdef _FLASH
	// Copy over code from FLASH to RAM
	memcpy((uint32_t *)&RamfuncsRunStart, (uint32_t *)&RamfuncsLoadStart,
				(uint32_t)&RamfuncsLoadSize);
#endif //_FLASH

	EDIS;

}

void CLA_configClaMemory(void)
{
	extern uint32_t Cla1funcsRunStart, Cla1funcsLoadStart, Cla1funcsLoadSize;
	//extern uint32_t Cla2funcsRunStart, Cla2funcsLoadStart, Cla2funcsLoadSize;
	//extern uint32_t Cla3funcsRunStart, Cla3funcsLoadStart, Cla3funcsLoadSize;
	extern uint32_t Cla1ConstRunStart, Cla1ConstLoadStart, Cla1ConstLoadSize;

	EALLOW;

#ifdef _FLASH
	// Copy over code from FLASH to RAM
	memcpy((uint32_t *)&Cla1funcsRunStart, (uint32_t *)&Cla1funcsLoadStart,
			(uint32_t)&Cla1funcsLoadSize);

	//memcpy((uint32_t *)&Cla2funcsRunStart, (uint32_t *)&Cla2funcsLoadStart,
			//(uint32_t)&Cla2funcsLoadSize);

	//memcpy((uint32_t *)&Cla3funcsRunStart, (uint32_t *)&Cla3funcsLoadStart,
			//(uint32_t)&Cla3funcsLoadSize);

	memcpy((uint32_t *)&Cla1ConstRunStart, (uint32_t *)&Cla1ConstLoadStart,
			(uint32_t)&Cla1ConstLoadSize);

#endif //_FLASH
	// Initialize and wait for CLA1ToCPUMsgRAM
	MemCfgRegs.MSGxINIT.bit.INIT_CLA1TOCPU = 1;
	while(MemCfgRegs.MSGxINITDONE.bit.INITDONE_CLA1TOCPU != 1){};

	// Initialize and wait for CPUToCLA1MsgRAM
	MemCfgRegs.MSGxINIT.bit.INIT_CPUTOCLA1 = 1;
	while(MemCfgRegs.MSGxINITDONE.bit.INITDONE_CPUTOCLA1 != 1){};

	// Select LS1RAM to be the programming space for the CLA
	// First configure the CLA to be the master for LS5 and then
	// set the space to be a program block
	MemCfgRegs.LSxMSEL.bit.MSEL_LS1 = 1;
	MemCfgRegs.LSxCLAPGM.bit.CLAPGM_LS1 = 1;

	// Select LS2RAM to be the programming space for the CLA
	// First configure the CLA to be the master for LS5 and then
	// set the space to be a program block
	MemCfgRegs.LSxMSEL.bit.MSEL_LS2 = 1;
	MemCfgRegs.LSxCLAPGM.bit.CLAPGM_LS2 = 1;


	// Select LS4RAM to be the programming space for the CLA
	// First configure the CLA to be the master for LS5 and then
	// set the space to be a program block
	MemCfgRegs.LSxMSEL.bit.MSEL_LS4 = 1;
	MemCfgRegs.LSxCLAPGM.bit.CLAPGM_LS4 = 1;

	// Select LS5RAM to be the programming space for the CLA
	// First configure the CLA to be the master for LS5 and then
	// set the space to be a program block
	MemCfgRegs.LSxMSEL.bit.MSEL_LS5 = 1;
	MemCfgRegs.LSxCLAPGM.bit.CLAPGM_LS5 = 1;

	// Select LS3RAM to be the programming space for the CLA
	// First configure the CLA to be the master for LS5 and then
	// set the space to be a program block
	MemCfgRegs.LSxMSEL.bit.MSEL_LS3 = 1;
	MemCfgRegs.LSxCLAPGM.bit.CLAPGM_LS3 = 1;

	// Select LS0RAM to be the data space for the CLA
	// First configure the CLA to be the master for LS0 and then
	// set the space to be a data block
	MemCfgRegs.LSxMSEL.bit.MSEL_LS0 = 1;
	MemCfgRegs.LSxCLAPGM.bit.CLAPGM_LS0 = 0;

	EDIS;
}



void CLA_initCpu1Cla1(void)
{
	// Compute all CLA task vectors
	// On Type-1 CLAs the MVECT registers accept full 16-bit task addresses as
	// opposed to offsets used on older Type-0 CLAs
	EALLOW;
	Cla1Regs.MVECT1 = (uint16_t)(&Cla1Task1);
	Cla1Regs.MVECT2 = (uint16_t)(&Cla1Task2);
	Cla1Regs.MVECT3 = (uint16_t)(&Cla1Task3);
	Cla1Regs.MVECT4 = (uint16_t)(&Cla1Task4);
	Cla1Regs.MVECT5 = (uint16_t)(&Cla1Task5);
	Cla1Regs.MVECT6 = (uint16_t)(&Cla1Task6);
	Cla1Regs.MVECT7 = (uint16_t)(&Cla1Task7);
	Cla1Regs.MVECT8 = (uint16_t)(&Cla1Task8);


	// Set the EPWM1INT as the trigger for task 1
	//DmaClaSrcSelRegs.CLA1TASKSRCSEL1.bit.TASK1 = 36;
	DmaClaSrcSelRegs.CLA1TASKSRCSEL1.bit.TASK1 = 45;
	//DmaClaSrcSelRegs.CLA1TASKSRCSEL1.bit.TASK1 = 0;

	// Set the EPWM1INT as the trigger for task 2
	//DmaClaSrcSelRegs.CLA1TASKSRCSEL1.bit.TASK2 = 36;
	DmaClaSrcSelRegs.CLA1TASKSRCSEL1.bit.TASK2 = 45;

	// Set the soft command as the trigger for task 3
	DmaClaSrcSelRegs.CLA1TASKSRCSEL1.bit.TASK3 = 0;

	// Set the soft command as the trigger for task 4
	DmaClaSrcSelRegs.CLA1TASKSRCSEL1.bit.TASK4 = 0;

	// Enable the IACK instruction to start a task on CLA in software
	// for all  8 CLA tasks. Also, globally enable all 8 tasks (or a
	// subset of tasks) by writing to their respective bits in the
	// MIER register
	Cla1Regs.MCTL.bit.IACKE = 1;
    Cla1Regs.MIER.all 	= (M_INT1 | M_INT2 | M_INT3 | M_INT4); //| M_INT8 | M_INT7);

    // Configure the vectors for the end-of-task interrupt for all
    // 8 tasks
	PieVectTable.CLA1_1_INT   = &cla1Isr1;
	PieVectTable.CLA1_2_INT   = &cla1Isr2;
	PieVectTable.CLA1_3_INT   = &cla1Isr3;
	PieVectTable.CLA1_4_INT   = &cla1Isr4;
	PieVectTable.CLA1_5_INT   = &cla1Isr5;
	PieVectTable.CLA1_6_INT   = &cla1Isr6;
	PieVectTable.CLA1_7_INT   = &cla1Isr7;
	PieVectTable.CLA1_8_INT   = &cla1Isr8;

	// Set the adca.1 as the trigger for task 7
	//DmaClaSrcSelRegs.CLA1TASKSRCSEL2.bit.TASK7 = 1;

	// Enable CLA interrupts at the group and subgroup levels
	PieCtrlRegs.PIEIER11.all  = 0xFFFF;
	IER |= (M_INT11 | M_INT5);
	EDIS;
}





// No More

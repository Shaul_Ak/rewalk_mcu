/*
 * RWLK_Manual.c
 *
 *  Created on: 6 ����� 2016
 *      Author: orit.benhorin
 *  This module manages the manual buttons pressing and activate the relevant logic
 */

/*******************************************************************************
 *  Include
 ******************************************************************************/
#include "RWLK_LogicHandler.h"
#include "F2837xD_Gpio_defines.h"
#include "RWLK_Common.h"
#include "RWLK_Manual.h"



/*******************************************************************************
 *  Constants and definitions
 ******************************************************************************/
static const uint16_t UserButton1_GPIO = 108;
static const uint16_t UserButton2_GPIO = 109;

static RWLK_Manual_buttons_Info_Type RWLK_Manual_buttons_Info;

//RWLK_Logic_Variables_Type *RWLK_Logic_Vars_Ptr = 0;

/*****************************************************************************
 * Function name: RWLK_Manual_Buttons_HandlingInit
 *
 * Description: Initialize the module and relevant variables.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
void RWLK_Manual_Buttons_HandlingInit(void/*RWLK_Logic_Variables_Type *ptr*/)
{
//	if (ptr != 0)
//		RWLK_Logic_Vars_Ptr = ptr;
//	else
//		printf("error null pointr");

	// Configure the user buttons GPIOs.
	GPIO_SetupPinMux(UserButton1_GPIO, GPIO_MUX_CPU1, 0);
	GPIO_SetupPinOptions(UserButton1_GPIO, GPIO_INPUT, GPIO_PUSHPULL);
	GPIO_SetupPinMux(UserButton2_GPIO, GPIO_MUX_CPU1, 0);
	GPIO_SetupPinOptions(UserButton2_GPIO, GPIO_INPUT, GPIO_PUSHPULL);
}

/*****************************************************************************
 * Function name: RWLK_Manual_Buttons_Handling
 *
 * Description: Read and analyze the user buttons pressing (long press, short press, release)
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Manual_Buttons_Handling, "ramfuncs")
void RWLK_Manual_Buttons_Handling(OPERATION_MODE Mode)
{
	// Read buttons status from relevant GPIOs
	RWLK_Manual_buttons_Info.ManualSwitch1 = (0 == GPIO_ReadPin(UserButton1_GPIO));
	RWLK_Manual_buttons_Info.ManualSwitch2 = (0 == GPIO_ReadPin(UserButton2_GPIO));


//if(!ManualSwitch1 && !ManualSwitch2 && ((Mode == _SIT)||(Mode == _STAND)||(Mode == _MANUAL) )) //Check if both switches pressed
//{
//	BypassModeCounter++; //count 1 at each loop , where each loop is 250usec
//
//	if (BypassModeCounter == 8000) // 250usec * 8000 = 2 seconds should hold switches
//	{
//		// ByPassEnable = !ByPassEnable;
//		ByPassCommand(ByPassActiviation); //if buttons has been holded for 2 seconds send can msg.
//	}
//}
//else if(!ManualSwitch1 && ManualSwitch2 && ByPassEnable ) //Check if both switches pressed
//{
//	BypassModeCounter++; //count 1 at each loop , where each loop is 250usec
//
//	if (BypassModeCounter == 8000) // 250usec * 8000 = 3 seconds should hold switches
//	{
//		ByPassCommand(UpperButtonMode); //if buttons has been holded for 2 seconds send can msg.
//	}
//}
//else if(ManualSwitch1 && !ManualSwitch2 && ByPassEnable) //Check if both switches pressed
//{
//	BypassModeCounter++; //count 1 at each loop , where each loop is 250usec
//
//	if (BypassModeCounter == 8000) // 250usec * 8000 = 3 seconds should hold switches
//	{
//		ByPassCommand(LowerButtonMode); //if buttons has been holded for 2 seconds send can msg.
//	}
//}
//else
//{
//	BypassModeCounter = 0 ;
//}


///*================END BYPASS Module===================*/
//
//
////Servo Loop
// //if( IncEncoder_GetPosition( &MotorEncoderPosition ) == ERR_OK  && IncEncoder_DirectionRead(  &MotorDirection ) == ERR_OK )
////{
//	//FeedbackPosition = IncrEncoderPosition;
//	//asm(" ESTOP0");  // Stop here
//
//	/*=====================================
//	* 		REPEAT ACTION REQUEST		***
//	*====================================*/
//
//	//Mode = _DESC ; //for debug Issue
//
//	if(((Mode == _ASCEND) || (Mode == _DESC))&&(CARD_NUMBER==0 || CARD_NUMBER==2) && (!ByPassEnable))
//		{
//			if(!ManualSwitch1 && !ManualSwitch2)
//			{
//			TwoButtonPressed = true;
//			DebounceCounter = 0 ;
//			}
//			if (ManualSwitch1 && ManualSwitch2 && TwoButtonPressed)
//			{
//			TwoButtonPressed = false;
//			DebounceCounter = 0 ;
//			}
//				ButtonXorStatus 	= !ManualSwitch1 ^ !ManualSwitch2 ; //Xor Bwt Two buttons to detect change
//				ButtonAndStatus	   =	!ManualSwitch1 && !ManualSwitch2 ;
//
//
//				if ((DebounceCounter > 1200) && (!ButtonXorStatus) && (!ButtonAndStatus)&& (!TwoButtonPressed))
//				{
//					DebounceCounter = 0 ;  //Reset Counter
//					TransmitFlags.RepeatActionFlag = true ;
//					//SendRepeatRequest();
//					CLR_LED2_VAL = 1;
//					ButtonPushedFlag = true;
//				}
//
//				if (ButtonXorStatus && !TwoButtonPressed)
//				{
//					DebounceCounter++;
//				}
//
//
//		}
} /* end function RWLK_Manual_Handling */

/*****************************************************************************
 * Function name: RWLK_Manual_GetButtonsStatePtr
 *
 * Description: Read and analyze the user buttons pressing (long press, short press, release)
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(RWLK_Manual_GetButtonsStatePtr, "ramfuncs")
void RWLK_Manual_GetButtonsStatePtr(const RWLK_Manual_buttons_Info_Type* buttonsInfo)
{
	// Read buttons status from relevant GPIOs
	buttonsInfo = &RWLK_Manual_buttons_Info;
}

/*
 * RWLK_LogicHandler.h
 *
 *  Created on: 17 ����� 2016
 *      Author: orit.benhorin
 */

#ifndef RWLK_LOGICHANDLER_H_
#define RWLK_LOGICHANDLER_H_

#include <stdint.h>
#include <stdbool.h>


void RWLK_LogicHandlerInit(void);
void RWLK_LogicHandler(void);
//void RWLK_Logic_GetLogicVarsPtr(RWLK_Logic_Variables_Type *ptr);
extern uint16_t MotorEnableRequest(uint16_t Axis);
//extern static void RWLK_Logic_Process_Move(void);


#endif /* RWLK_LOGICHANDLER_H_ */

/*==============================================================================
                              RWLK.h
==============================================================================*/

#ifndef RWLK_H_
#define RWLK_H_

/*==============================================================================
                              Defines
==============================================================================*/

/*==============================================================================
                           //Global Defines
==============================================================================*/
//#define   MATH_TYPE      1
//#include "IQmathLib.h"


#include <stdint.h>
#include <stdbool.h>


#include "F28x_Project.h"
#include "F2837xD_device.h"
#include "F2837xD_GlobalPrototypes.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"


#include "F2837xD_Gpio_defines.h"
#include "F2837xD_EPwm_defines.h"

//#define SENSOR_MODE

typedef enum{X_Axis=0, Y_Axis} MOTOR_DRIVER_AXES_NUMBERS;

// If want to use IDDK_MODULE, define a compilation flag
#define IDDK_MODULE 0
#define BOOSTXL		0

// parameters for current measurements to DAC
#if IDDK_MODULE == 1 && BOOSTXL == 0
	#define CURRENT_SENSOR_MEASUREMENT_RATE	8.8 		// +-31 Ampers	CurrentSensorMeasurementRate
#else
#if BOOSTXL == 1 && IDDK_MODULE == 0
	#define CURRENT_SENSOR_MEASUREMENT_RATE	16.5 		// +-31 Ampers
	#define  INVERSE_OF_CURRENT_SENSOR_RESOLUTION		0.0606
#else
	#define CURRENT_SENSOR_MEASUREMENT_RATE	16.5 //7.8125 //6.25 		// +-31 Ampers
	#define X_AXIS_CURRENT_SENSOR_MEASUREMENT_RATE	9.33
	#define Y_AXIS_CURRENT_SENSOR_MEASUREMENT_RATE	16.5
	#define INVERSE_X_AXIS_CURRENT_SENSOR_MEASUREMENT_RATE 1/X_AXIS_CURRENT_SENSOR_MEASUREMENT_RATE
	#define INVERSE_Y_AXIS_CURRENT_SENSOR_MEASUREMENT_RATE 1/Y_AXIS_CURRENT_SENSOR_MEASUREMENT_RATE
#endif
#endif

#define HALF_ADC_RESOLUTION	2048 	// 11 bit HalfAnalogToDigitalResolution
#define HALF_DAC_RESOLUTION   2048 	// 11 bit HalfDigitalToAnalogResolution
#define INV_HALF_DAC_RESOLUTION 	0.00048828125
#define MAXIMUM_CURRENT_MEASUREMENT	3 		// Ampers MaximumCarrentMeasurement
#define X_AXIS_MAXIMUM_CURRENT_MEASUREMENT	3 		// Ampers MaximumCarrentMeasurement
#define X_AXIS_BITS_PER_AMPER				INVERSE_X_AXIS_CURRENT_SENSOR_MEASUREMENT_RATE*HALF_ADC_RESOLUTION
#define X_AXIS_AMPERS_PER_BIT				X_AXIS_CURRENT_SENSOR_MEASUREMENT_RATE*INV_HALF_DAC_RESOLUTION
#define X_AXIS_MAXIMUM_PERMITTED_RMS_CURRENT	5
#define Y_AXIS_MAXIMUM_CURRENT_MEASUREMENT	8 		// Ampers MaximumCarrentMeasurement
#define Y_AXIS_BITS_PER_AMPER				INVERSE_Y_AXIS_CURRENT_SENSOR_MEASUREMENT_RATE*HALF_ADC_RESOLUTION
#define Y_AXIS_AMPERS_PER_BIT				Y_AXIS_CURRENT_SENSOR_MEASUREMENT_RATE*INV_HALF_DAC_RESOLUTION
#define Y_AXIS_MAXIMUM_PERMITTED_RMS_CURRENT	3



// defines for position measurements to DAC
#define MAX_RELATIVE_POSITION_MEASUREMENTS	327680.0
#define MAX_POSITION_ERROR_MEASUREMENT	300.0

// defines for velocity measurements to DAC
#define MAX_MOTOR_VEL_MEASUREMENT		5000	// RPM
#define SAMPLING_FREQUENCY				20000	// Hz
#define ONE_MINUTE_IN_SEC				60      // sec

#define Pi								3.141592653589
#define SAMPLING_TIME					0.00005		// 50us sampling
#define ERR_OK                  0x00
/*==============================================================================
                           //ReWalk Application Prototype functions
==============================================================================*/
uint16_t RWLK_Init(void);
void CAN_Input(void);


#endif /* RWLK_H_ */

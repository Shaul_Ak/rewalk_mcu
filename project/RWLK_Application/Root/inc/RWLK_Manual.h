/*
 * RWLK_Manual.h
 *
 *  Created on: 6 ����� 2016
 *      Author: orit.benhorin
 */

#ifndef RWLK_MANUAL_H
#define RWLK_MANUAL_H

typedef struct
{
	bool ManualSwitch1;
	bool ManualSwitch2;

} RWLK_Manual_buttons_Info_Type;

void RWLK_Manual_Buttons_HandlingInit(void);
void RWLK_Manual_Buttons_Handling(OPERATION_MODE Mode);
void RWLK_Manual_GetButtonsStatePtr(const RWLK_Manual_buttons_Info_Type* buttonsInfo);

#endif /* RWLK_MANUAL_H */

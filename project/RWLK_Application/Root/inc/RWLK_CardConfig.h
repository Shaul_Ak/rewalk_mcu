#ifndef RWLK_CARD_CONFIG_H_
#define RWLK_CARD_CONFIG_H_
/*****************************************************************************
*                                 IMPORTS
*****************************************************************************/
#include "RWLK.h"
#include "RWLK_Common.h"

/*****************************************************************************
*                      EXPORTED TYPES, CONSTANTS AND MACROS
*****************************************************************************/

typedef enum {
    CardConfig_e_move_forward      = 0,
    CardConfig_e_move_backward     = 1,
	CardConfig_e_total_direction   = 2,
}CardConfig_move_direction_t;

typedef enum {
	CardConfig_e_LEFT_HIP_Card     = 0,
	CardConfig_e_LEFT_KNEE_Card    = 1,
	CardConfig_e_RIGHT_HIP_Card    = 2,
	CardConfig_e_RIGHT_KNEE_Card   = 3,
}CardConfig_CardNumber_t;

typedef struct
{
    uint16_t ConfigSwitch1;

	uint16_t ConfigSwitch2;

	uint16_t MessageID;

	uint16_t CardNumber;

	bool     moveDirection[CardConfig_e_total_direction];

}CardConfigObject_t,*tCardConfigObject_ptr_t;

/*****************************************************************************
*
*: Function Name: CardConfigInit
*: Abstract:
*
*: Preconditions:
*    None.
*: Postconditions:
*
*: Logic: Call Card Dip Switch and initalize Card Data structure
*
*****************************************************************************/
extern void CardConfigInit();
/*****************************************************************************
*
*: Function Name: CardConfigGetMessageID
*: Abstract:
*
*: Preconditions:
*    None.
*: Postconditions:
*
*: Logic:
*
*****************************************************************************/
extern uint16_t CardConfigGetMessageID();
/*****************************************************************************
*
*: Function Name: CardConfigGetCardNumber
*: Abstract:
*
*: Preconditions:
*    None.
*: Postconditions:
*
*: Logic:
*
*****************************************************************************/
extern uint16_t CardConfigGetCardNumber();
/*****************************************************************************
*
*: Function Name: CardConfigGetMoveDirection
*: Abstract:
*
*: Preconditions:
*    None.
*: Postconditions:
*
*: Logic:
*
*****************************************************************************/
extern bool CardConfigGetMoveDirection(CardConfig_move_direction_t xi_direction);

#endif

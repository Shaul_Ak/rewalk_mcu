#include "RWLK_MELODY.h"
#include "RWLK_Sensor_Data.h"
#include <medexo_controller.h>

#define BLUETOOTH_MESSAGE_MAX_CHARS_LENGTH  50
#define BLUETOOTH_GUI_STRINGS_QUEUE_MAX_ELEMENTS     4
#define BLUETOOTH_GUI_STRINGS_QUEUE_ELEMENT_LENGTH   16
#define BLUETOOTH_GUI_TAGS_STRING_LENGTH             4
#define GUI_BLUETOOTH_STREAM_BUFFER_LENGTH           160

typedef enum
{
	GUI_BLUE_IDLE ,
	GUI_BLUE_CONNECTED ,
	GUI_BLUE_DATA_MODE_ENTRY,
	GUI_BLUE_DATA_MODE ,
	GUI_BLUE_COMMAND_MODE,
	GUI_BLUE_DISCONNECTED,
	GUI_BLUE_Debug
}GUI_BLUETOOTH_STATUS;

typedef enum
{
	Short_Integer,
	Integer ,
	Float ,
	String ,
	Restore_System_Data,
	Unsigned_Long_Hex,
	Signed_Long_Hex,
	DUMMY,

}GUI_BLUETOOTH_PARMETER_TYPE;


typedef enum
{
	SYSTEM_MODE ,
    LEDS,
	MOVE_MOTOR_X,
	READ_FEEDBACK_X,
	READ_FEEDBACK_Y,
	Elapsed_Time,
	GUI_RESTORE_PARAMETERS_SET,
	GUI_RESTORE_PARAMETERS_GET,
	GUI_RESTORE_PARAMETER_PF1,
	GUI_RESTORE_PARAMETER_PF2,
	GUI_RESTORE_PARAMETER_PF3,
	GUI_RESTORE_PARAMETER_DF1,
	GUI_RESTORE_PARAMETER_DF2,
	GUI_RESTORE_PARAMETER_DF3,
	GUI_RESTORE_PARAMETER_AUTO_MAN_MODE,
	KST_SENSOR_DATA,
	RESTORE_DEVICE_DATA,
	GUI_RESTORE_PARAMETERS,
	BATTRIES_STATUS,
	MCU_HW_VERSION,
	MCU_SW_VERSION,
	SYSTEM_FATAL_ERROR,
	SYSTEM_WARNING,
	STSTEM_TOTAL_STEP_COUNTER,
	SYSTEM_WALK_SPEED,

	//Add your commands




	LAST_BLUETOOTH_GUI_TAG

}BLUETOOTH_GUI_DATA_COMMANDS_TAGS;

typedef struct  _Bluetooth_Restore_GUI_Parameters
{
	enum medexo_handle_mode  Restore_Device_Status;
	uint16_t Pf1;
	uint16_t Pf2;
	uint16_t Pf3;
	uint16_t Df1;
	uint16_t Df2;
	uint16_t Df3;
	uint16_t OperationMode;

}
sBlue_Restore_GUI_Parameters , *pBlue_Restore_GUI_Parameters;

typedef struct  _Bluetooth_MCU_GUI_Data
{
	char  Battery_Status;
	uint32_t Warning_Status;
	uint32_t Fatal_Error_Status;
	uint32_t MCU_HW_Version;
	char MCU_SW_Version[5];
    uint32_t  Step_Total_Counter;
    float  Walking_Speed;

}
sBlue_MCU_GUI_Data , *pBlue_MCU_GUI_Data;



extern char General_System_Error;
extern char System_Warning;
extern char Fatal_System_Error;
extern char Batteries_Status;
extern sBlue_MCU_GUI_Data mcu_gui_data;
extern long int Restore_System_Step_Counter;
void Set_Restore_Mode(enum medexo_handle_mode mode );
void Update_GUI_Restore_Blue_Parameters_Exchange(const char *blue_GUI_data_buffer);
enum medexo_handle_mode Get_Restore_Mode(void);
void Recv_GUI_Restore_Blue_Data(void);
void Send_GUI_Blue_Data_to_Print(char *message_to_print );
_Bool  Send_Init_MCU_GUI_Restore_Blue_Data(void);
void Send_GUI_Blue_Message_Long_Hex_Par(const char *tag , float  a );
void Send_Sensor_Data_To_Bluetooth(struct sensor_protocol_struct sensor_output_data);
void Send_GUI_Restore_Blue_Data(uint32_t msec_cnt);
void Enter_GUI_Blue_Data_Mode(void);
void Update_Melody_Received_Data(void);
void Set_Restore_Mode();
GUI_BLUETOOTH_STATUS  Get_GUI_Blue_Conn_Status(void);
void Set_GUI_Blue_Conn_Status(GUI_BLUETOOTH_STATUS status);
enum medexo_handle_mode Get_Restore_Mode(void);
void Set_GUI_Blue_Conn_Status(GUI_BLUETOOTH_STATUS status);
void GUI_Blue_Handler(uint32_t msec_cnt);





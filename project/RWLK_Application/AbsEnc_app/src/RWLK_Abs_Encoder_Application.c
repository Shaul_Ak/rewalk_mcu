/*
 * RWLK_Abs_Encoder_Application.c
 *
 *  Created on: Nov 19, 2016
 *      Author: Arie
 */




#include "RWLK.h"
#include "RWLK_Abs_Encoder_Application.h"
#include "RWLK_Motor_Control.h"
#include "../../RWLK_Periph_Driver/inc/RWLK_Abs_Encoder_Driver.h"
#include "ExtFlash_Driver.h"

#define ERR_OK                  0x00

#pragma DATA_SECTION(sAbsoluteEncoderStruct,"ramdata")
struct	 _ABS_ENCODER_STRUCT sAbsoluteEncoderStruct;

/*==============================================================================
                               Functions definition
==============================================================================*/
/*
** ===================================================================
**     Function   :  AbsEncoder_Init
**
**     Description :
**					Absolute Encoder initialization
**     Parameters  :
**					u16 Baud Rate - SPI baud rate in kHz. May be from 196 to ENCODER_MAX_RATE.
**     Returns     :
**					Error code. Possible values:
**                      - ERR_OK,
**                      - ERR_SPI_TRANSFER_RATE_DOES_NOT_MATCH.
**     Notes       :
**
** ===================================================================
*/
#pragma CODE_SECTION(AbsEncoder_Init, "ramfuncs")
uint16_t AbsEncoder_Init(void)
{
	uint8_t ErrCode = ERR_OK;
	const uint32_t Length = 5;
	uint32_t SubSectorNumber = 0x0;
	uint32_t My_Addr = SubSectorNumber<<12;
	ExFlash_ReturnStatus ret = Flash_Success;
	uint8_t WriteData[5] = {0x20,0xAA,0xBB,0xCC,0x01};
	uint8_t ReadData[5] = {0x0,0x0,0x0,0x0,0x0};
	//uint8_t ErasedArr[2] = {0xFF,0xFF};

	/*if( BaudRate > ENCODER_MAX_RATE )
	{
		ErrCode = ERR_SPI_TRANSFER_RATE_DOES_NOT_MATCH;
	}
	else
	{
		ErrCode = Spi_Init( BaudRate );
	}
*/

	bissc_init();
	//ret |= ExtFlash_Program_UpToPage (My_Addr ,WriteData, Length);
	//System Limits
	ret |= ExtFlash_Read_Data (My_Addr , sServoParametersStruct.ReadData, Length, 0);
	sAbsoluteEncoderStruct.CalibrationDoneFlag  = sServoParametersStruct.ReadData[4];
	if(sAbsoluteEncoderStruct.CalibrationDoneFlag != 1)
	{
		ErrCode |= 1; //No Calibration
	}
	sAbsoluteEncoderStruct.HighSoftwareLimit = 0;
	sAbsoluteEncoderStruct.LowSoftwareLimit = 0;
	//if Knee  ->
	sAbsoluteEncoderStruct.MaxPermittedRange = MAXIMUM_HIP_PERMITTED_RANGE;
	sAbsoluteEncoderStruct.HighSoftwareLimit = (uint32_t)sServoParametersStruct.ReadData[0];
	sAbsoluteEncoderStruct.HighSoftwareLimit = (sAbsoluteEncoderStruct.HighSoftwareLimit << 8) + (uint32_t)sServoParametersStruct.ReadData[1];
	sAbsoluteEncoderStruct.LowSoftwareLimit = (uint32_t)sServoParametersStruct.ReadData[2];
	sAbsoluteEncoderStruct.LowSoftwareLimit = (sAbsoluteEncoderStruct.LowSoftwareLimit << 8) + (uint32_t)sServoParametersStruct.ReadData[3];
	sAbsoluteEncoderStruct.HighSoftwareLimit = sAbsoluteEncoderStruct.HighSoftwareLimit << 16;
	sAbsoluteEncoderStruct.HighSoftwareLimit = sAbsoluteEncoderStruct.HighSoftwareLimit >> 16;
	sAbsoluteEncoderStruct.LowSoftwareLimit = sAbsoluteEncoderStruct.LowSoftwareLimit << 16;
	sAbsoluteEncoderStruct.LowSoftwareLimit = sAbsoluteEncoderStruct.LowSoftwareLimit >> 16;
	sAbsoluteEncoderStruct.SoftwareLimitsDifference = sAbsoluteEncoderStruct.HighSoftwareLimit - sAbsoluteEncoderStruct.LowSoftwareLimit;
	if(abs(sAbsoluteEncoderStruct.SoftwareLimitsDifference) <=  sAbsoluteEncoderStruct.MaxPermittedRange)
	{
		if(sAbsoluteEncoderStruct.SoftwareLimitsDifference > 0)
		{
			sAbsoluteEncoderStruct.FeedbackConversionFlag = 1;
		}
		else
		{
			sAbsoluteEncoderStruct.FeedbackConversionFlag = 2;
			sAbsoluteEncoderStruct.HighSoftwareLimit = (~sAbsoluteEncoderStruct.HighSoftwareLimit + 1);
			sAbsoluteEncoderStruct.LowSoftwareLimit = (~sAbsoluteEncoderStruct.LowSoftwareLimit + 1);
		}

	}

	else
	{

		if(sAbsoluteEncoderStruct.SoftwareLimitsDifference > 0)
		{
			sAbsoluteEncoderStruct.FeedbackConversionFlag = 3;
			// 1's complement of 15LSB and set sign bits to zero
			sAbsoluteEncoderStruct.HighSoftwareLimit = sAbsoluteEncoderStruct.HighSoftwareLimit << 1;
			sAbsoluteEncoderStruct.HighSoftwareLimit = ~sAbsoluteEncoderStruct.HighSoftwareLimit;
			sAbsoluteEncoderStruct.HighSoftwareLimit = sAbsoluteEncoderStruct.HighSoftwareLimit >> 1;
			sAbsoluteEncoderStruct.HighSoftwareLimit &= 0x00007fff;

			// 1's complement  and set sign bits to one
			sAbsoluteEncoderStruct.LowSoftwareLimit = ~sAbsoluteEncoderStruct.LowSoftwareLimit;
			sAbsoluteEncoderStruct.LowSoftwareLimit |= 0xffff8000;
		}
		else
		{
			sAbsoluteEncoderStruct.FeedbackConversionFlag = 4;

			// 1's complement  and set sign bits to one
			sAbsoluteEncoderStruct.HighSoftwareLimit = ~sAbsoluteEncoderStruct.HighSoftwareLimit;
			sAbsoluteEncoderStruct.HighSoftwareLimit |= 0xffff8000;
			// 2's complement
			sAbsoluteEncoderStruct.HighSoftwareLimit = (~sAbsoluteEncoderStruct.HighSoftwareLimit + 1);

			// 1's complement of 15LSB and set sign bits to zero
			sAbsoluteEncoderStruct.LowSoftwareLimit = sAbsoluteEncoderStruct.LowSoftwareLimit << 1;
			sAbsoluteEncoderStruct.LowSoftwareLimit = ~sAbsoluteEncoderStruct.LowSoftwareLimit;
			sAbsoluteEncoderStruct.LowSoftwareLimit = sAbsoluteEncoderStruct.LowSoftwareLimit >> 1;
			sAbsoluteEncoderStruct.LowSoftwareLimit &= 0x00007fff;
			// 2's complement
			sAbsoluteEncoderStruct.LowSoftwareLimit = (~sAbsoluteEncoderStruct.LowSoftwareLimit + 1);
		}


	}
	//ret |= ExtFlash_Erase_SubSector ( SubSectorNumber, 0);
	//ret |= ExtFlash_Read_Data (My_Addr, ReadData2, Length, 0);

	return ErrCode;
}

/*
** ===================================================================
**     Function   :  AbsEncoder_GetData
**
**     Description :
**					TODO:
**     Parameters  :
**					
**     Returns     :
**					Error code. Possible values:
**                      - ERR_OK,
**                      - ERR_SPI_TRANSFER_RATE_DOES_NOT_MATCH.
**     Notes       :
**
** ===================================================================
*/
#pragma CODE_SECTION(AbsEncoder_GetData, "ramfuncs")
uint16_t AbsEncoder_GetData( int32_t *Result )
{

	uint16_t Err_Code = ERR_OK;
PM_bissc_setupNewSCDTransfer(BISS_DATA_CLOCKS, SPI_FIFO_WIDTH);

#if BISS_ENCODER_HAS_CD_INTERFACE
if(bissc_doCDTasks() == 0)
{
	ESTOP0; //most likely the BiSS CD CRC did not match
}
#endif

PM_bissc_startOperation();

while (bissc_data_struct.dataReady != 1) {}

// fill and receive SCD data (including position) and putting it into the BiSS data structure
if(bissc_receivePosition(BISS_POSITION_BITS, BISS_POSITION_CRC_BITS) == 0)
{
	//ESTOP0; //BiSS SCD CRC did not match
	Err_Code = 1;
}
//*Result = bissc_data_struct.position ;
*Result = (int32_t)(bissc_data_struct.scd_raw >> 2);

return Err_Code;
}
//No more

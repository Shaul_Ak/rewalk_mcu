/*
 * RWKL_Abs_Encoder_Application.h
 *
 *  Created on: Nov 19, 2016
 *      Author: Arie
 */

#ifndef RWLK_APPLICATION_ABSENC_APP_INC_RWKL_ABS_ENCODER_APPLICATION_H_
#define RWLK_APPLICATION_ABSENC_APP_INC_RWKL_ABS_ENCODER_APPLICATION_H_

//#include "../../RWLK_Periph_Driver/inc/RWLK_Abs_Encoder_Driver.h"


typedef struct _ABS_ENCODER_STRUCT
{
	int32_t HighSoftwareLimit;
	int32_t LowSoftwareLimit;
	int32_t SoftwareLimitsDifference;
	int32_t MaxPermittedRange;
	int32_t FeedbackConversionFlag;
	int32_t CalibrationDoneFlag;
}sAbsoluteEncoderParameters;


extern struct _ABS_ENCODER_STRUCT sAbsoluteEncoderStruct;


/*==============================================================================
                               Functions declaration
==============================================================================*/

extern uint16_t AbsEncoder_Init(void);
/*
** ===================================================================
**     Function   :  AbsEncoder_Init
**
**     Description :
**					Absolute Encoder initialization
**     Parameters  :
**					u16 Baud Rate - SPI baud rate in kHz. May be from 196 to ENCODER_MAX_RATE.
**     Returns     :
**					Error code. Possible values:
**                      - ERR_OK,
**                      - ERR_SPI_TRANSFER_RATE_DOES_NOT_MATCH.
**     Notes       :
**
** ===================================================================
*/

extern uint16_t AbsEncoder_GetData( int32_t *Result );
/*
** ===================================================================
**     Function   :  AbsEncoder_GetData
**
**     Description :
**					Read encoder value
**     Parameters  :
**					u16 *Result - received data
**     Returns     :
**					Error code. Possible values:
**                      - ERR_OK,
**                      - ERR_SPI_EXCHANGING_FAILS.
**     Notes       :
**
** ===================================================================
*/




#endif /* RWLK_APPLICATION_ABSENC_APP_INC_RWKL_ABS_ENCODER_APPLICATION_H_ */

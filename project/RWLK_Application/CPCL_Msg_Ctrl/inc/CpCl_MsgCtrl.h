/*******************************************************************************
 * File name: CpCl_MsgCtrl.h
 *
 * Description:
 *
 *
 * Note: Developed for TI F28377D.
 *
 * Author:	Yehuda Bitton
 * Date:
 *
 ******************************************************************************/

/*******************************************************************************
  Multiple include protection
 ******************************************************************************/
#ifndef CPCL_MSGCTRL_H
#define CPCL_MSGCTRL_H

/*******************************************************************************
 *  Include
 ******************************************************************************/

/*******************************************************************************
 *  Defines
 ******************************************************************************/
#define WRITE_CLA_MSG						0 //Not allowed within CPU
#define READ_CLA_MSG						1
#define WRITE_CPU_MSG						2 //Not allowed within CLA
#define READ_CPU_MSG						3

#define READ_REQUEST_INDEX					0 // First place in data message array between cpu and cla
#define READ_DATA_INDEX_0					1
#define READ_DATA_INDEX_1					2
#define READ_DATA_INDEX_2					3

#define ERROR_WRONG_CPCL_MSG_OPTYPE 		11
#define ERROR_WRONG_CPCL_MSG_INDEX 			12

/*------------------------------------------------------------------------------
  Configurations
------------------------------------------------------------------------------*/
#define CPU_MSG_ARR_SIZE					16
#define CLA_MSG_ARR_SIZE					CPU_MSG_ARR_SIZE

/*******************************************************************************
 *  Data types
 ******************************************************************************/
typedef struct CPU_MsgStruct{
	uint16_t Data[CPU_MSG_ARR_SIZE];
}_CPU_MsgStruct;

typedef struct CLA_MsgStruct{
	uint16_t Data[CLA_MSG_ARR_SIZE];
}_CLA_MsgStruct;


/*******************************************************************************
 *  Constants and Macros
 ******************************************************************************/

/*******************************************************************************
 *  Interface function deceleration:
 ******************************************************************************/
uint16_t CpCl_Init_CpuMsg_Struct(void);
uint16_t CpCl_Init_ClaMsg_Struct(void);
uint16_t CpCl_CLA_Msg_Ctrl(uint16_t OpType, uint16_t Index, uint16_t *pData );
uint16_t CpCl_CPU_Msg_Ctrl(uint16_t OpType, uint16_t Index, uint16_t *pData );

/*******************************************************************************
 *  End of file
 ******************************************************************************/
#endif /* CPU_CLA_MSGCTRL_H */

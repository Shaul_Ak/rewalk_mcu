/*******************************************************************************
* file name: Cpu_MsgCtrl.c
*
* Description: Control message structs within MessageRAMs.
* 			   - Init CPU message ram in CPUtoCLAmsgRAM.
* 			   - R/W CPU message ram in CPUtoCLAmsgRAM.
* 			   - R CLA message ram in CLAtoCPUmsgRAM.
*
*
* Note: Developed for TI F28377D.
*
* Author: Yehuda Bitton
* Date:   2/2016
*
******************************************************************************/

/*******************************************************************************
*  Includes
******************************************************************************/
#include "F28x_Project.h"     // Device Headerfile and Examples Include File
#include "CpCl_MsgCtrl.h"


/*******************************************************************************
*  Data types
******************************************************************************/

/*------------------------------------------------------------------------------
Variables
------------------------------------------------------------------------------*/
#ifdef __cplusplus
#pragma DATA_SECTION("CpuToCla1MsgRAM")
_CPU_MsgStruct CPU_Msg;
#pragma DATA_SECTION("Cla1ToCpuMsgRAM")
_CLA_MsgStruct CLA_Msg;
#pragma DATA_SECTION(CLA_MsgStrct, "CLADataLS0")
_CLA_MsgStruct CLA_MsgStrct;
#else
#pragma DATA_SECTION(CPU_Msg,"CpuToCla1MsgRAM");
_CPU_MsgStruct CPU_Msg;
#pragma DATA_SECTION(CLA_Msg,"Cla1ToCpuMsgRAM")
_CLA_MsgStruct CLA_Msg;
#pragma DATA_SECTION(CLA_MsgStrct, "CLADataLS0")
_CLA_MsgStruct CLA_MsgStrct;
#endif //__cplusplus

/*******************************************************************************
*  Constants and Macros
******************************************************************************/

/*******************************************************************************
*  Static Data deceleration
******************************************************************************/

/*******************************************************************************
*  Private Functions Prototypes
******************************************************************************/

/*******************************************************************************
*  Public Function Bodies
******************************************************************************/
/*****************************************************************************
 * Function name: CpCl_Init_CpuMsg_Struct
 *
 * Description: Initialize CPU message struct in CPUtoCLA message RAM.
 *
 * Parameters:  None.
 * Returns:     [0] - init successfully.
 * 				[1] - init failed.
 *
 * Known issues:
 * Todo:
 *
 ******************************************************************************/
uint16_t CpCl_Init_CpuMsg_Struct(void)
{
	uint16_t i = 0;

	for (i = 0; i < CPU_MSG_ARR_SIZE; i++)
		CPU_Msg.Data[i] = 0;
	// Verify successful write:
	for (i = 0; i < CPU_MSG_ARR_SIZE; i++)
	{
		if (CPU_Msg.Data[i] == 0)
			return 1;
	}

	return 0;
}

/*****************************************************************************
 * Function name: CpCl_CPU_Msg_Ctrl
 *
 * Description: CPU interface to message structs in CLAtoCPUmsgRAM and CPUtoCLAmsgRAM:
 * 				- Read/ Write CPU message struct.
 * 				- Read CLA message struct.
 * 				This function should be called by the CPU only.
 *
 * Parameters:  OpType - < READ_CLA_MSG, WRITE_CPU_MSG, READ_CPU_MSG>.
 * 				Index - the element in the array struct.
 * 				pData - pointer to the data where to be written.
 * 				        can be Null in read operation.
 * Returns:     [0] - pass.
 * 				[11] - ERROR_WRONG_CPCL_MSG_OPTYPE.
 * 				[12] - ERROR_WRONG_CPCL_MSG_INDEX
 *
 * Known issues:
 * Todo:
 *
 ******************************************************************************/
#pragma CODE_SECTION(CpCl_CPU_Msg_Ctrl, "ramfuncs")
uint16_t CpCl_CPU_Msg_Ctrl (uint16_t OpType, uint16_t Index, uint16_t *pData )
{
	uint16_t Err_Code = 0;

	// Check index value:
	if (OpType == READ_CLA_MSG && Index > CLA_MSG_ARR_SIZE)
			Err_Code = ERROR_WRONG_CPCL_MSG_INDEX;
	else if (Index > CPU_MSG_ARR_SIZE)
		Err_Code = ERROR_WRONG_CPCL_MSG_INDEX;
		switch (OpType)
		{
			case (WRITE_CPU_MSG):
			{
				CPU_Msg.Data[Index] = *pData;
			}
			break;

			case (READ_CPU_MSG):
			{
				*pData = CPU_Msg.Data[Index];
			}
			break;

			case (READ_CLA_MSG):
			{
				*pData = CLA_Msg.Data[Index];
			}
			break;

			default:
			{
				Err_Code = ERROR_WRONG_CPCL_MSG_OPTYPE;
			}
		} // end switch

	return Err_Code;
}

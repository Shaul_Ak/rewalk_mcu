/*
 * RWLK_Common_Par.h
 *
 *  Created on: Jan 11, 2016
 *      Author: arie
 */

#ifndef RWLK_COMMON_PAR_H_
#define RWLK_COMMON_PAR_H_


//#define  SENSOR_MODE
/*
bool  ByPassEnable ;
bool  TestSystemFlag ;
bool  FirstMoveafterBoot ;
bool  AllMotorsFlag ;
uint16_t TestSystemCounter ;
uint32_t CurrentOperationMode ;  // Mode parameter in old MCU program
uint32_t JointMessageID  = 0x330 ;
int32_t IncEncFeedbackPosition = 0;
*/
/*==============================================================================
                               	   CANBUS Type Definitions
==============================================================================*/

// extern const uint8_t COMMAND_SEND_STAND_MODE;
// extern const uint8_t COMMAND_SEND_WALK_MODE;
// extern const uint8_t COMMAND_SEND_ASCEND_MODE;
// extern const uint8_t COMMAND_SEND_DESC_MODE;
// extern const uint8_t COMMAND_SEND_SIT_MODE;
// extern const uint8_t COMMAND_SEND_MANUAL_MODE;
// extern const uint8_t COMMAND_SEND_INC_ENCODER_POSITION;

 typedef struct _Transmit_Flags
 {
  bool LowPowerDetectioFlag;   // In ARGO, was called as part of the function VoltageDetectionFunction(). This logic was activated before TransmitArbitrator() in ARGO.
  bool PowerRecoveryFlag;

  // flags from protocol
  bool HighCurrentFlag;
  bool JointStatusFlag;
  bool SendAckFlag;
  bool EncoderErrorFlag;
  bool MagnetLossFlag;
  bool ActionNotCompleteFlag;
  bool XActionCompleteFlag;
  bool YActionCompleteFlag;
  bool RepeatActionFlag;
  bool SwitchModeCompleteFlag;
  bool ReturnJointAngleFlag;
  bool PositionAckFlag;
  bool ReadCurrentAverageFlag;
  bool XServoOnFlag;
  bool EncoderCurrentTestFlag;
  bool SystemTestMsgFlag;
  bool ReadCalibrationLimitsFlag;
  bool SendXIncrementalEncoderFeedbackFlag;
  bool SendYIncrementalEncoderFeedbackFlag;
  bool SendAbsoluteEncoderFeedbackFlag;
  bool SendMotorPhasesCurrentsFlag;
  bool SendXServoAnalyzerMeasurementsFlag;
  bool SendYServoAnalyzerMeasurementsFlag;
  bool TransmitionInProcess;
#ifdef DEBUG_SYSTEM_STABILITY
  bool TransmitSystemStabilityMessage;
#endif

 }sTransmitFlags;

 typedef enum{KP = 0,KI, I10, UMAX ,UMIN , I6} PI_FILTER_MEMBERS;
 typedef enum{Kp = 0,Ki, Kd, i10, Umax ,Umin , i14} PID_FILTER_MEMBERS;
 typedef enum{VLSOFBW, VLSOFDR} SOF_FILTER_MEMBERS;
 typedef enum{VLNATT, VLNFREQ, VLNWID} NOTCH_FILTER_MEMBERS;
 typedef enum{POS_ADJUSTER_REQUEST,VEL_ADJUSTER_REQUEST,CUR_ADJUSTER_REQUEST,SERVO_ANALIZER_REQUEST,PID_LOOP_REQUEST,STOP_ADJUSTER_REQUEST}ADJUSTER_REQUEST_PARAMETERS;

#endif /* RWLK_COMMON_PAR_H_ */

/*
 * RWLK_Common.h
 *
 *  Created on: 31 ���� 2015
 *      Author: Saman
 */


// Project 16.05.17

#ifndef RWLK_COMMON_INC_H
#define RWLK_COMMON_INC_H


#include "RWLK.h"




#define ERR_OK                  0x00
#define forever					for(;;)


#define CPU_FREQ_200MHZ			200000000
#define CAN_BUS_SPEED_500KHZ    500000


typedef enum
{
    X_MOVE                              	 = 0x0,
	RETURN_STATUS							 = 0x1,
    IAM_ALIVE                         		 = 0x2,
    PERFORM_SYSTEM_TESTING           	     = 0x3,
    SWITCH_MODE                     		 = 0x5,
    X_STOP_MOTOR                       		 = 0x9,
//#ifdef DEBUG_DEV_PHASE
	MOTOR_HEATING_THRESHOLD					 = 0xB,
	SET_INC_ENCODER_VALUE                 	 = 0xC,
	SET_ABS_ENCODER_VALUE               	 = 0xF,
//#endif

    CONFIG_UPPERLIMIT_ABS_ENCODER     		 = 0x10,
    CONFIG_LOWERLIMIT_ABS_ENCODER     		 = 0x11,
    CONFIG_MAXANGLELIMIT              		 = 0x12,
    ACTION_COMPLETED                  		 = 0x14,   // used in both directions
    ACTION_NOT_COMPLETED        	  		 = 0x15,
    /*SYSTEM_STATUS*/ REPEAT_LAST_ACTION     = 0x16,   // from MCU to MPB
	ENCODER_ERROR 							 = 0x17,
    TEST_COMPLETED                    		 = 0x18,
    WARNING                           		 = 0x19,   // MCU_JUMP_BOOT_ACK 0x19
    ACKNOWLEDGE                       		 = 0x1A,
    ACC_FACTOR		                       	 = 0x1B,
	NO_COMMNAND_RECEIVED          	  		 = 0x1E,
    INVALID_COMMAND                   		 = 0x1F,
	Y_STOP_MOTOR							 = 0x20,
	//SET_PID                          		 = 0x20,
	X_MOTOR_ENABLE    				  		 = 0x21,
	X_MOTOR_DISABLE                     	 = 0x22,
	X_RELEASE_MOTOR    				  		 = 0x23,
	X_UNRELEASE_MOTOR 				  		 = 0x24,
	DISABLE_LIMIT					  		 = 0x25,
	FILTER_PAR				  		 		 = 0x26,
	X_STOP_ADJUSTER							 = 0x28,  // PART OF THE ADJUST FAMILITY
	X_CURRENT_ADJUSTER                  	 = 0x29,
	OUT_ON                            		 = 0x2A,
	OUT_OFF                           		 = 0x2B,
	READ_CURRENT                       		 = 0x2C,
	//SVLCP									 = 0x2D,
	READ_LIMITS								 = 0x2E,
	GET_MOTOR_VELOCITY						 = 0x2F,
	CHECK_REQUIRED_ACK						 = 0x31,
	#ifdef SENSOR_MODE
	CHECK_SENSOR_REQUIRED_ACK				 = 0x32,
	#endif  // SENSOR_MODE
	SWITCH_TWO_APDATE                 		 = 0x40,
    PWM_RAMP_SWICH                    		 = 0x41,
	MOVE_TO_ZERRO                     		 = 0x42,
	JUMP_TO_BOOT							 = 0X44,
    RETURN_ABS_ENCODER_ANG            		 = 0x50,
    RETURN_CONFIG_UPPERLIMIT_ABS_ENCODER     = 0x51,
    RETURN_CONFIG_LOWERLIMIT_ABS_ENCODER     = 0x52,
	X_MOVE_OPEN_LOOP                    	 = 0x53,
	FLASH_STORE                       		 = 0x55,
	CHANGE_XPWM								 = 0x56,
	VEL_COMMAND								 = 0x57,
	VEL_INTGAIN								 = 0x58,
	VEL_PGAIN								 = 0x59,
	READ_MEM								 = 0x60,
	SET_HWLIMITS							 = 0x61,
	SET_VELADJUSTER							 = 0x62,
	CURRENT_TRSH							 = 0x63,
	HIGH_FRICTION							 = 0x64,
	ABS_ENCODER_PROBLEM						 = 0x65,
	ENCODER_PROBLEM_END						 = 0x66,
	//FAULT_RESET							 = 0x67,
	//ASK_FAULT								 = 0x68,
	AUTO_CALIBRATION						 = 0x69,
	WRITE_MESSAGE_TO_LOG					 = 0x6A,
	MCU_SHUTTING_DOWN				    	 = 0x70,
	MCU_POWER_RECOVERY						 = 0x71,
	READY_FOR_MOVEMENT_TEST					 = 0x72, /* TODO: orit - replace SERVO_ON_FLAG */
	X_COMMUT_REQUEST						 = 0x73,
	// Commands for tuning Servo Current Loop (SCL)
	XSCLKP									 = 0x74,
	XSCLKI									 = 0x75,
	XSCLLIM									 = 0x76,
	XSCLI10									 = 0x77,   // through can bus
	X_VELOCITY_ADJUSTER						 = 0x78,
	// Commands for tuning Servo Velocity Loop (SVL)
	XSVLKP									 = 0x79,
	XSVLKI									 = 0x7A,
	XSVLLIM									 = 0x7B,
	XSVLI10									 = 0x7C,
	X_POSITION_ADJUSTER						 = 0x7D,
	XCURRENT_ADJUSTER_VALUE					 = 0x7E,
	XDWELL_TIME								 = 0x7F,

	MOTOR_SEND_ACK							 = 0x88,
	
	YDWELL_TIME								 = 0x89,
	YPROFILE_TARGET							 = 0x8A,
	YPROFILE_VEL							 = 0x8B,
	YPROFILE_ACC							 = 0x8C,
	YPROFILE_JERK							 = 0x8D,
	YCURRENT_ADJUSTER_VALUE					 = 0x8E,

	XPROFILE_VEL							 = 0x91,
	XPROFILE_ACC							 = 0x92,
	XPROFILE_JERK							 = 0x93,
	XPROFILE_TARGET							 = 0x94,
	GC_SET									 = 0x95,
	GC_RESET								 = 0x96,
	ENABLE_CUR_SNS							 = 0x97,
	DISABLE_CUR_SNS							 = 0x98,
	XDRV_WRITE								 = 0x99,
	XDRV_READ							 	 = 0x9A,
	XDRV_INIT								 = 0x9B,
	YDRV_WRITE								 = 0x9C,
	YDRV_READ							 	 = 0x9D,
	YDRV_INIT								 = 0x9E,
	Y_COMMUT_REQUEST						 = 0x9F,
	XSPLKP									 = 0xA8,
	XSPLKI									 = 0xA9,
	XSPLLIM									 = 0xAA,
	XSPLI10									 = 0xAB,
	X_SERVO_ANALYZER						 = 0xAC, // part of the adjuster family /* TODO orit: verify handling of this message, from can bus to MCU */
	/* TODO - orit verify with Arie what is the response for set and what for get */
	GET_XABS_ENCODER_ANG               		 = 0xB0,	// A command for reading data from the encoder, Not influencing the system functionality.
	GET_XINC_ENCODER_VALUE		      		 = 0xB1,	// A command for reading data from the X axis encoder, Not influencing the system functionality.
	GET_YINC_ENCODER_VALUE		      		 = 0xB2,	// A command for reading data from the Y axis encoder, Not influencing the system functionality.
	Y_VELOCITY_ADJUSTER						 = 0xB3,
	Y_POSITION_ADJUSTER						 = 0xB4,
	Y_CURRENT_ADJUSTER						 = 0xB5,
	Y_STOP_ADJUSTER							 = 0xB6,
	Y_SERVO_ANALYZER						 = 0xB7,
	Y_MOTOR_ENABLE							 = 0xB8,
	Y_MOTOR_DISABLE                     	 = 0xB9,
	Y_RELEASE_MOTOR							 = 0xBA,
	Y_UNRELEASE_MOTOR						 = 0xBB,
	Y_MOVE_OPEN_LOOP						 = 0xBC,
	CHANGE_YPWM								 = 0xBD,
	// Y Axis Servo Loop Parameters Setting
	// Commands for tuning Y Axis Servo Position Loop (SPL)
	YSPLKP									 = 0xBE,
	YSPLKI									 = 0xBF,
	YSPLLIM									 = 0xC0,
	YSPLI10									 = 0xC1,
	// Velosity Loop Parameters
	// Commands for tuning Y Axis Servo Velocity Loop (SVL)
	YSVLKP									 = 0xC2,
	YSVLKI									 = 0xC3,
	YSVLLIM									 = 0xC4,
	YSVLI10									 = 0xC5,
	// Commands for tuning Y Axis Servo Current Loop (SCL)
	YSCLKP									 = 0xC6,
	YSCLKI									 = 0xC7,
	YSCLLIM									 = 0xC8,
	YSCLI10									 = 0xC9,   // through can bus
	Y_MOVE									 = 0xCA,

	// Commands for tuning  Servo Velocity Loop Second Order Filter parameters (SVL)
	SLVSOFBW								 = 0xCB,
	SLVSOFDR								 = 0xCC,
	CALC_SLVSOF_PAR							 = 0xCD,
	XSLVSOF_PAR_SET							 = 0xCE,
	YSLVSOF_PAR_SET							 = 0xCF,

	// Commands for tuning  Servo Velocity Loop Notch Filter parameters (SVL)
	SLVNATT 								 = 0xD0,
	SLVNFREQ								 = 0xD1,
	SLVNWID 								 = 0xD2,
	CALC_SLVNF_PAR							 = 0xD3,
	XSLVN_PAR_SET							 = 0xD4,
	YSLVN_PAR_SET							 = 0xD5,
	XCALIBRATION							 = 0xD6,
	YCALIBRATION							 = 0xD7,
	STOP_CALIBRATION						 = 0xD8,

	//System Setup Modes
	Handle_Modes_Setup						= 0xD9,
	Leg_Side								= 0xDA,

	WRITE_SAMPLING_TIME						 = 0xE1,
	READ_SAMPLING_TIME						 = 0xE2,
	YSVLKD									 = 0xE3,
	TEST_FLASH                               = 0xF3,
	TRANSIMT_MOTOR_MOVE                      = 0xF4,
	CHNG_PE_THRSHLD							 = 0xFD,
	JOINT_STATE								 = 0xFE,
	NO_REQUEST								 = 0xFF,

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Shaul 18.4.2017: ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	/*============SensorsCommandsConstant======================*/
	#ifdef SENSOR_MODE
	CAN_PROTOCOL_IMU	 					 = 0x100,

	CAN_ID_SENSOR_RESPONSE_LC		 		 = 0x604,
	CAN_ID_SENSOR_RESPONSE_IMU		 		 = 0x608,

	CAN_ID_SENSOR_CAOMMAND_LC 				 = 0x504, // IMU boards 4LSB: 0b0100,
	CAN_ID_SENSOR_CAOMMAND_IMU 				 = 0x508, // IMU boards 4LSB: 0b1000,
	#endif  // SENSOR_MODE
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	/*============ByPassCommandsConstant======================*/
 	ByPassActiviation						=	0x01,
 	UpperButtonMode							=	0x03,
	LowerButtonMode					    	=	0x02,
	LogMode									= 	0x6B, 
	//ByPassMode							    =   0x90,
	//EncoderTest								=   0x30,
	RUNTESTFLAG								=   0x80, 
	ReadCurrentAverage						=   0x81


} COMMAND_PROTOCOL;


typedef enum {


	_STAND   		 = 0,
    _WALK    		 = 0x01,
    _ASCEND  		 = 0x02,
    _DESC    		 = 0x03,
    _SIT     		 = 0x04,
    _MANUAL  		 = 0x05,
    _SIT_TO_STAND	 = 0x06,
    _STAND_TO_SIT	 = 0x07,
    _RESV1   		 = 0x10,
    _RESV2   		 = 0x11,
    _RESV3   		 = 0x12,
	_IDLE		     = 0x16,
	_TEST_MOTORS     = 0x53
} OPERATION_MODE;


//extern OPERATION_MODE	OperationModeArray;
// test byte values

#define TEST_SUCCESS          		0x00
#define ABS_ENCODER_NC		  		0x80
#define	TEST_IN_PROCESS  			0x40
#define NO_CALBIRATION        		0x20
#define ENCODER_ERROR_POS			0x10
#define CALIBRATION_LIMITS_ERROR	0x11
#define MOTOR_ON_FAILURE			0x09
#define V12_FAILURE          		0x08
#define ADC_FAILURE          		0x04
#define V1_8_FAILURE          		0x02
#define MEMORY_FAILURE        		0x01
#define DefaultEncoderSamplingTime	300    //wait 300 msec
#define HipTravelDistance 			3004   // 3004/22.75555 = 133 degrees
#define KneeTravelDistance 			2480   // 2480/22.75555 = 109 degress
#define I_AM_ALIVE_TIME				120000

#define ADC_TIMEOUT		4 			//4 us

// Increment encoder errors
#define ERR_RANGE_XINCR_ENCODER_MAX_VAL          0x30
#define ERR_XINCR_ENCODER_INIT_VAL	            0x31
#define ERR_XINCR_ENCODER_POS_COUNTER            0x32
#define ERR_RANGE_YINCR_ENCODER_MAX_VAL          0x33
#define ERR_YINCR_ENCODER_INIT_VAL	            0x34
#define ERR_YINCR_ENCODER_POS_COUNTER            0x35

// Incremental encoder driver constants definition
#define INC_ENCODER_DIRECTION_FALSE 		255
#define INC_ENCODER_CLOCKWISE_ROTATION		1
#define INC_ENCODER_ANTICLOCKWISE_ROTATION	0
#define INC_ENCODER_FORWARD_MOVEMENT        1
#define INC_ENCODER_REVERSE_MOVEMENT        0

// PWM errors
#define ERR_PWM_PERIOD_RANGE		           	0x70 // Period = 0
#define ERR_PWM_DUTY_RANGE         		  	    0x71 // Duty Cycle = 0 or Duty Cycle >= Period
#define ERR_PWM_RED_FED_RANGE              	  	0x72 // RED_FED < PWM_RED_MIN_VAL or RED_FED > PWM_RED_MAX_VAL
#define ERR_PWM_RED_GT_DUTY                	  	0x74 // RED_FED >= Duty cycle
#define ERR_PWM_FED_GT_DUTY                	  	0x75 // RED_FED >= (Period - Duty cycle)

// PWM driver constants definition
#define PWM_RED_MIN_VAL  1
#define PWM_RED_MAX_VAL  1023
#define PWM_FED_MIN_VAL  1
#define PWM_FED_MAX_VAL  1023


#define SET_PWM1A				 EPwm1Regs.CMPA.bit.CMPA
#define SET_HRPWM1A				 EPwm1Regs.CMPA.bit.CMPAHR
#define SET_FPWM1A				 EPwm1Regs.CMPA.all
#define SET_PWM2A				 EPwm2Regs.CMPA.bit.CMPA
#define SET_HRPWM2A				 EPwm2Regs.CMPA.bit.CMPAHR
#define SET_FPWM2A				 EPwm2Regs.CMPA.all
#define SET_PWM3A				 EPwm3Regs.CMPA.bit.CMPA
#define SET_HRPWM3A				 EPwm3Regs.CMPA.bit.CMPAHR
#define SET_FPWM3A				 EPwm3Regs.CMPA.all
//#define SET_PWM3B				 EPwm3Regs.CMPB.bit.CMPB

// SPI errors
//#define ERR_SPI_EXCHANGING_FAILS				0xAA
#define ERR_SPI_TRANSFER_RATE_DOES_NOT_MATCH	0x20
#define ERR_SPI_EXCHANGING_FAILS            	0x21



// ADC driver constants definition
#define ADC_MODCLK 0x4 //  HSPCLK = SYSCLKOUT/8


// ADC MODULE CHANNELS
#define ADCIN0          0
#define ADCIN1          1
#define ADCIN2          2
#define ADCIN3          3
#define ADCIN4          4
#define ADCIN5          5
#define ADCIN6          6
#define ADCIN7          7
#define ADCIN8          8
#define ADCIN9        	9
#define ADCIN10        10
#define ADCIN11        11
#define ADCIN12        12
#define ADCIN13        13
#define ADCIN14        14
#define ADCIN15        15


#define NO_ADC_CHANNEL 255

//#define ADC_CH_MOTOR_CURRENT     ADCINA0
//#define ADC_CH_FS1_V             ADCINA2
//#define ADC_CH_FS2_V             ADCINA3
//#define ADC_CH_3_3V_SAMPLE       ADCINA4
//#define ADC_CH_12V_SAMPLE        ADCINA5
//#define ADC_CH_TEMPEARTURE       ADCINB0
//#define ADC_CH_FS3_V             ADCINB1
//#define ADC_CH_VREF              ADCINB2
//#define ADC_CH_5V_SAMPLE         ADCINB3
//#define ADC_CH_1_8V_SAMPLE       ADCINB4
//#define ADC_CH_28V_SAMPLE        ADCINB5

#define A_PHASE_CH_MOTOR_CURRENT     ADCIN4
#define B_PHASE_CH_MOTOR_CURRENT     ADCIN4


#define MAX_ADC_CHANNEL          ADC_CH_28V_SAMPLE

#define DUMMY_ADC_RESULT 0xFFFF

#define ADC_CKPS   0x02   // ADC module clock = HSPCLK/30*ADC_CKPS   = 12.5MHz/(30*1) = 417KHz

#define ADC_REF_INTERNAL_REF       0
#define ADC_REF_2048MV             1
#define ADC_REF_1500MV             2
#define ADC_REF_1024MV             3

#define ADC_SHCLK_160_NS_CONVERSION  0   // S/H width in ADC module periods = 2 ADC clocks
#define ADC_SHCLK_240_NS_CONVERSION  1   // S/H width in ADC module periods = 3 ADC clocks
#define ADC_SHCLK_320_NS_CONVERSION  2   // S/H width in ADC module periods = 4 ADC clocks
#define ADC_SHCLK_400_NS_CONVERSION  3   // S/H width in ADC module periods = 5 ADC clocks
#define ADC_SHCLK_480_NS_CONVERSION  4   // S/H width in ADC module periods = 6 ADC clocks
#define ADC_SHCLK_560_NS_CONVERSION  5   // S/H width in ADC module periods = 7 ADC clocks
#define ADC_SHCLK_640_NS_CONVERSION  6   // S/H width in ADC module periods = 8 ADC clocks
#define ADC_SHCLK_360_NS_CONVERSION  7   // S/H width in ADC module periods = 9 ADC clocks
#define ADC_SHCLK_800_NS_CONVERSION  8   // S/H width in ADC module periods = 10 ADC clocks
#define ADC_SHCLK_880_NS_CONVERSION  9   // S/H width in ADC module periods = 11 ADC clocks
#define ADC_SHCLK_860_NS_CONVERSION  10  // S/H width in ADC module periods = 12 ADC clocks
#define ADC_SHCLK_1040_NS_CONVERSION 11  // S/H width in ADC module periods = 13 ADC clocks
#define ADC_SHCLK_1120_NS_CONVERSION 12  // S/H width in ADC module periods = 14 ADC clocks
#define ADC_SHCLK_1200_NS_CONVERSION 13  // S/H width in ADC module periods = 15 ADC clocks
#define ADC_SHCLK_1280_NS_CONVERSION 14  // S/H width in ADC module periods = 16 ADC clocks
#define ADC_SHCLK_1360_NS_CONVERSION 15  // S/H width in ADC module periods = 17 ADC clocks

// ADC errors
#define ERR_ADC_NO_CHANNEL_DOES_NOT_MATCH   	0x10
#define ERR_ADC_CONVERSION_FAILS            	0x11


#define ADC_MAX_CONVERSION_TIME    150



#endif /* RWLK_COMMON_INC_H */

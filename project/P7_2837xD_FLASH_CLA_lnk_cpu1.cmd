// The user must define CLA_C in the project linker settings if using the
// CLA C compiler
// Project Properties -> C2000 Linker -> Advanced Options -> Command File 
// Preprocessing -> --define
#ifdef CLA_C
// Define a size for the CLA scratchpad area that will be used
// by the CLA compiler for local symbols and temps
// Also force references to the special symbols that mark the
// scratchpad are. 
CLA_SCRATCHPAD_SIZE = 0x100;
--undef_sym=__cla_scratchpad_end
--undef_sym=__cla_scratchpad_start
#endif //CLA_C

MEMORY
{
PAGE 0 :
   /* BEGIN is used for the "boot to SARAM" bootloader mode   */

   BEGIN           	: origin = 0x080000, length = 0x000002
   RAMM0           	: origin = 0x000122, length = 0x0002DE
   RAMD0           	: origin = 0x00B000, length = 0x000800
   RAMLS12345		: origin = 0x008800, length = 0x002800
   //RAMLS3      		: origin = 0x009800, length = 0x000800  // RAMLS3 were moved from PAGE1 to PAGE0.
   //RAMLS45			: origin = 0x00A000, length = 0x001000  // RAMLS4 & RAMLS5 were combained into one memory unit.
   RAMGS12131415    : origin = 0x018000, length = 0x004000  // RAMGS14 & RAMGS15 were combained into one memory unit.
   RESET           	: origin = 0x3FFFC0, length = 0x000002

   /* Flash sectors */
   FLASHA           : origin = 0x080002, length = 0x001FFE	/* on-chip Flash */
   FLASHB           : origin = 0x082000, length = 0x002000	/* on-chip Flash */
   FLASHCD           : origin = 0x084000, length = 0x004000	/* on-chip Flash */
   //FLASHD           : origin = 0x086000, length = 0x002000	/* on-chip Flash */
   FLASHE           : origin = 0x088000, length = 0x008000	/* on-chip Flash */
   FLASHF           : origin = 0x090000, length = 0x008000	/* on-chip Flash */
   FLASHG           : origin = 0x098000, length = 0x008000	/* on-chip Flash */
   FLASHH           : origin = 0x0A0000, length = 0x008000	/* on-chip Flash */
   FLASHI           : origin = 0x0A8000, length = 0x008000	/* on-chip Flash */
   FLASHJ           : origin = 0x0B0000, length = 0x008000	/* on-chip Flash */
   FLASHK           : origin = 0x0B8000, length = 0x002000	/* on-chip Flash */
   FLASHL           : origin = 0x0BA000, length = 0x002000	/* on-chip Flash */
   FLASHM           : origin = 0x0BC000, length = 0x002000	/* on-chip Flash */
   FLASHN           : origin = 0x0BE000, length = 0x002000	/* on-chip Flash */
   FPUTABLES		: origin = 0x3febdc, length = 0x0006a0

   IQTABLES (R)		: origin = 0x3FE000, length = 0x000b50
   IQTABLES2 (R)	: origin = 0x3FEB50, length = 0x00008C

PAGE 1 :

   BOOT_RSVD        : origin = 0x000002, length = 0x000120     /* Part of M0, BOOT rom will use this for stack */
   RAMM1            : origin = 0x000400, length = 0x000400     /* on-chip RAM block M1 */

   COUNTER			: origin = 0x008000, length = 0x000002
   IENCODER			: origin = 0x008002, length = 0x000004
   SINTABLEADD		: origin = 0x008006, length = 0x000002
   CURRENTTABLEADD	: origin = 0x008008, length = 0x000002
   SINTABLE			: origin = 0x00800A, length = 0x000400
   RAMLS0          	: origin = 0x00840A, length = 0x0003F6
  // RAMLS1          	: origin = 0x008800, length = 0x000800
  // RAMLS2      		: origin = 0x009000, length = 0x000800
   
   
   RAMGS0           : origin = 0x00C000, length = 0x001000
   RAMGS1           : origin = 0x00D000, length = 0x001000
   RAMGS2           : origin = 0x00E000, length = 0x001000
   RAMGS3           : origin = 0x00F000, length = 0x001000
   RAMGS4           : origin = 0x010000, length = 0x001000
   RAMGS5           : origin = 0x011000, length = 0x001000
   RAMGS6           : origin = 0x012000, length = 0x001000
   RAMGS7           : origin = 0x013000, length = 0x001000
   RAMGS8           : origin = 0x014000, length = 0x001000
   RAMGS9           : origin = 0x015000, length = 0x001000
   RAMGS10          : origin = 0x016000, length = 0x001000
   RAMGS11          : origin = 0x017000, length = 0x001000
   //RAMGS12          : origin = 0x018000, length = 0x001000
   //RAMGS13          : origin = 0x019000, length = 0x001000

                    
   CLA1_MSGRAMLOW   : origin = 0x001480, length = 0x000080
   CLA1_MSGRAMHIGH  : origin = 0x001500, length = 0x000080
   
   CPU2TOCPU1RAM   : origin = 0x03F800, length = 0x000400
   CPU1TOCPU2RAM   : origin = 0x03FC00, length = 0x000400

   Z1_BOOTCTRL_OTP      : origin = 0x7801E, length = 0x2  // Yehuda: Added in order to configure boot pins.

}


SECTIONS
{
   /* Allocate program areas: */
   .cinit           : > FLASHA      PAGE = 0, ALIGN(4)
   .pinit           : > FLASHA,     PAGE = 0, ALIGN(4)
   .text            : >> FLASHE | FLASHF | FLASHG , PAGE = 0, ALIGN(4)
   codestart        : > BEGIN       PAGE = 0, ALIGN(4)
   ramfuncs         : LOAD = FLASHE | FLASHF | FLASHG,
                      RUN = RAMGS12131415,
                      LOAD_START(_RamfuncsLoadStart),
                      LOAD_SIZE(_RamfuncsLoadSize),
                      LOAD_END(_RamfuncsLoadEnd),
                      RUN_START(_RamfuncsRunStart),
                      RUN_SIZE(_RamfuncsRunSize),
                      RUN_END(_RamfuncsRunEnd),
                      PAGE = 0, ALIGN(4)

	/* Allocate CPU data */
	ramdata         : > RAMGS9      PAGE = 1
   /* Allocate uninitalized data sections: */
   .stack           : > RAMM1        PAGE = 1
   .ebss            : > RAMGS11       PAGE = 1	//RAMLS2
   .esysmem         : > RAMGS10       PAGE = 1	//RAMLS2

   /* Initalized sections go in Flash */
   .econst          : > FLASHA      PAGE = 0, ALIGN(4)
   .switch          : > FLASHA      PAGE = 0, ALIGN(4)
   
   .reset           : > RESET,     PAGE = 0, TYPE = DSECT /* not used, */

   .cio             : > RAMGS0,    PAGE = 1 //Yehuda: added for stdio (printf) buffer.


   Filter_RegsFile  : > RAMGS0,	   PAGE = 1
   
   FPUmathTables	: > FPUTABLES, PAGE = 0, TYPE = NOLOAD


/*   IQmathTables		: load = IQTABLES, type = NOLOAD, PAGE = 0
   IQmathTables2  > IQTABLES2, type = NOLOAD, PAGE = 0
   {
   		IQmath.lib<IQNexpTable.obj> (IQmathTablesRam)
   }
   IQmathTablesRam	: load = RAMGS11, PAGE = 1
*/
      /* Allocate IQ math areas: */
  // IQmath              : > FLASHC      		PAGE = 0                  /* Math Code */
  // IQmathTables        : > FLASHE           PAGE = 0 // TYPE = NOLOAD   /* Math Tables In ROM */

    /* CLA specific sections  */
   Cla1Prog     	: LOAD = FLASHCD,
                      RUN = RAMLS12345,
                      LOAD_START(_Cla1funcsLoadStart),
                      LOAD_SIZE(_Cla1funcsLoadSize),
                      LOAD_END(_Cla1funcsLoadEnd),
                      RUN_START(_Cla1funcsRunStart),
                      RUN_SIZE(_Cla1funcsRunSize),
                      RUN_END(_Cla1funcsRunEnd),
                      PAGE = 0, ALIGN(4)


 //    Cla1Prog2     	: LOAD = FLASHCD,
                     // RUN = RAMLS12345,
                     // LOAD_START(_Cla2funcsLoadStart),
                     // LOAD_SIZE(_Cla2funcsLoadSize),
                     // LOAD_END(_Cla2funcsLoadEnd),
                     // RUN_START(_Cla2funcsRunStart),
                     // RUN_SIZE(_Cla2funcsRunSize),
                     // RUN_END(_Cla2funcsRunEnd),
                     // PAGE = 0, ALIGN(4)

    // Cla1Prog3     	: LOAD = FLASHD,
                     // RUN = RAMLS12,
                     // LOAD_START(_Cla3funcsLoadStart),
                     // LOAD_SIZE(_Cla3funcsLoadSize),
                     // LOAD_END(_Cla3funcsLoadEnd),
                     // RUN_START(_Cla3funcsRunStart),
                     // RUN_SIZE(_Cla3funcsRunSize),
                     // RUN_END(_Cla3funcsRunEnd),
                    //  PAGE = 0, ALIGN(4)


   CLACounter       : > COUNTER, PAGE=1
   CLAIencoder		: > IENCODER, PAGE=1
   CLASinTableBAP	: > SINTABLEADD, PAGE = 1	// Sin Table Base Address Pointer;
   CLASinTblCrntAdd	: > CURRENTTABLEADD, PAGE = 1 // CURRENT SIN TABLE ADDRESS POINTER
   CLASinTable		: > SINTABLE, PAGE = 1 // Sin Table For commutation process
   CLADataLS0		: > RAMLS0, PAGE=1
   //CLADataLS1		: > RAMLS1, PAGE=1

   Cla1ToCpuMsgRAM  : > CLA1_MSGRAMLOW,   PAGE = 1
   CpuToCla1MsgRAM  : > CLA1_MSGRAMHIGH,  PAGE = 1

   Z1_BOOTCTRL_section : >  Z1_BOOTCTRL_OTP,  PAGE = 1 //Added by Yehuda in order to configure boot pins


   	/* The following section definitions are required when using the IPC API Drivers */
    GROUP : > CPU1TOCPU2RAM, PAGE = 1
    {
        PUTBUFFER
        PUTWRITEIDX
        GETREADIDX
    }

    GROUP : > CPU2TOCPU1RAM, PAGE = 1
    {
        GETBUFFER :    TYPE = DSECT
        GETWRITEIDX :  TYPE = DSECT
        PUTREADIDX :   TYPE = DSECT
    }
  
   /* The following section definition are for SDFM examples */		
   Filter1_RegsFile : > RAMGS1,	PAGE = 1, fill=0x1111
   Filter2_RegsFile : > RAMGS2,	PAGE = 1, fill=0x2222
   Filter3_RegsFile : > RAMGS3,	PAGE = 1, fill=0x3333
   Filter4_RegsFile : > RAMGS4,	PAGE = 1, fill=0x4444
   
#ifdef CLA_C
   /* CLA C compiler sections */
   //
   // Must be allocated to memory the CLA has write access to
   //
   CLAscratch       :
                     { *.obj(CLAscratch)
                     . += CLA_SCRATCHPAD_SIZE;
                     *.obj(CLAscratch_end) } >  RAMLS0,  PAGE = 1

   .scratchpad      : > RAMLS0,       PAGE = 1
   .bss_cla		    : > RAMLS0,       PAGE = 1
   .const_cla	    :  LOAD = FLASHB,
                       RUN = RAMLS0,
                       RUN_START(_Cla1ConstRunStart),
                       LOAD_START(_Cla1ConstLoadStart),
                       LOAD_SIZE(_Cla1ConstLoadSize),
                       PAGE = 1
#endif //CLA_C
}

/*
//===========================================================================
// End of file.
//===========================================================================
*/

/*
 * RWLK_Motor_Control.c
 *
 *  Created on: Jan 12, 2016
 *      Author: arie
 */


#include "RWLK.h"
#include "RWLK_Common.h"
//#include "RWLK_CAN_Application.h"
#include "RWLK_DRV8031_MOTOR_DRV.h"
#include "RWLK_DRV8031_Applications.h"

/*==============================================================================
                           //Global Defines
==============================================================================*/
#define   MATH_TYPE      1
#include "IQmathLib.h"
#include "RWLK_Motor_Control.h"
#include "RWLK_Abs_Encoder_Application.h"
#include "RWLK_Cla_Control.h"
#include "RWLK_Common_Par.h"
#include "RWLK_Timer_Driver.h"
#include "RWLK_ADC_Driver.h"
#include "medexo_controller.h"

extern Uint16 DRV8301_SPI_Read(volatile struct SPI_REGS *s, Uint16 address, Uint16 Axis);
extern Uint16 DRV8301_SPI_Write(volatile struct SPI_REGS *s, Uint16 address, Uint16 data, Uint16 Axis);

/* TODO: Orit - change the getting to a function */
extern sTransmitFlags TxArbitrator;

// Parameters initialization
#pragma DATA_SECTION(CLAPWM_DC,"CLADataLS0")
int16_t CLAPWM_DC = 1250;

#pragma DATA_SECTION(CurrentCommandTemporary,"CLADataLS0")
float CurrentCommandTemporary;

#pragma DATA_SECTION(yk,"CLADataLS0")
float yk = 0;

#pragma DATA_SECTION(rk,"CLADataLS0")
float rk = 0;

#pragma DATA_SECTION(uk,"CLADataLS0")
float uk = 0;

#pragma DATA_SECTION(Vk,"CLADataLS0")
float Vk = 0;

#pragma DATA_SECTION(Vk_1,"CLADataLS0")
float Vk_1 = 0;

#pragma DATA_SECTION(Ik,"CLADataLS0")
float Ik = 0;

#pragma DATA_SECTION(Ik_1,"CLADataLS0")
float Ik_1 = 0;


#pragma DATA_SECTION(fCurrentCommand,"CLADataLS0")
float fCurrentCommand;

#pragma DATA_SECTION(iCurrentCommand,"CLADataLS0")
int16_t iCurrentCommand;

#pragma DATA_SECTION(OutputCurrent,"CLADataLS0")
int16_t OutputCurrent;


#pragma DATA_SECTION(fVelocityCommand,"CLADataLS0")
float fVelocityCommand;

#pragma DATA_SECTION(iVelocityCommand,"CLADataLS0")
int16_t iVelocityCommand;

#pragma DATA_SECTION(AdjusterSwitch,"CLADataLS0")
int32_t AdjusterSwitch = 0;

#pragma DATA_SECTION(AdjusterCounter,"CLADataLS0")
int32_t AdjusterCounter;

#pragma DATA_SECTION(CLA_TABLE_ADDRESS,"CLADataLS0")
uint32_t *CLA_TABLE_ADDRESS = (uint32_t *)0xFA00;

#pragma DATA_SECTION(CLA_TABLE_DATA,"CLADataLS0")
int32_t CLA_TABLE_DATA ;

#pragma DATA_SECTION(INDEX_A_PHASE,"CLADataLS0")
int16_t INDEX_A_PHASE;

#pragma DATA_SECTION(INDEX_B_PHASE,"CLADataLS0")
int16_t INDEX_B_PHASE;

#pragma DATA_SECTION(INDEX_C_PHASE,"CLADataLS0")
int16_t INDEX_C_PHASE;

#pragma DATA_SECTION(PHASE_ADVANCE,"CLADataLS0")
int16_t PHASE_ADVANCE;

#pragma DATA_SECTION(X_PHASE_ADVANCE,"CLADataLS0")
int16_t X_PHASE_ADVANCE;

#pragma DATA_SECTION(Y_PHASE_ADVANCE,"CLADataLS0")
int16_t Y_PHASE_ADVANCE;

#pragma DATA_SECTION(XPhaseNumberOfPointsPerCount,"CLADataLS0")
float XPhaseNumberOfPointsPerCount;

#pragma DATA_SECTION(YPhaseNumberOfPointsPerCount,"CLADataLS0")
float YPhaseNumberOfPointsPerCount;


#pragma DATA_SECTION(Brshl_Command_A,"CLADataLS0")
int16_t Brshl_Command_A;
#pragma DATA_SECTION(Brshl_Command_B,"CLADataLS0")
int16_t Brshl_Command_B;
#pragma DATA_SECTION(Brshl_Command_C,"CLADataLS0")
int16_t Brshl_Command_C;

#pragma DATA_SECTION(XBrshl_Command_A,"CLADataLS0")
int16_t XBrshl_Command_A;
#pragma DATA_SECTION(XBrshl_Command_B,"CLADataLS0")
int16_t XBrshl_Command_B;
#pragma DATA_SECTION(XBrshl_Command_C,"CLADataLS0")
int16_t XBrshl_Command_C;

#pragma DATA_SECTION(YBrshl_Command_A,"CLADataLS0")
int16_t YBrshl_Command_A;
#pragma DATA_SECTION(YBrshl_Command_B,"CLADataLS0")
int16_t YBrshl_Command_B;
#pragma DATA_SECTION(YBrshl_Command_C,"CLADataLS0")
int16_t YBrshl_Command_C;

#pragma DATA_SECTION(TABLE_VALUE_A,"CLADataLS0")
float TABLE_VALUE_A;
#pragma DATA_SECTION(TABLE_VALUE_B,"CLADataLS0")
float TABLE_VALUE_B;
#pragma DATA_SECTION(TABLE_VALUE_C,"CLADataLS0")
float TABLE_VALUE_C;



#pragma DATA_SECTION(AchannelConversionTime,"CLADataLS0")
int16_t AchannelConversionTime;

#pragma DATA_SECTION(BchannelConversionTime,"CLADataLS0")
int16_t BchannelConversionTime;

#pragma DATA_SECTION(AD_ConvertionTime,"CLADataLS0")
int16_t AD_ConvertionTime ;

#pragma DATA_SECTION(ADCINT1,"CLADataLS0")
int16_t ADCINT1;

//SecondDriver
#pragma DATA_SECTION(ADCINT2,"CLADataLS0")
int16_t ADCINT2;


#pragma DATA_SECTION(EncoderResolution,"CLADataLS0")
uint16_t EncoderResolution = 1000;

#pragma DATA_SECTION(BRLS_PWM,"CpuToCla1MsgRAM")
int16_t BRLS_PWM;

//#pragma DATA_SECTION(BRLS_PWMA,"CpuToCla1MsgRAM")
//int16_t BRLS_PWMA;

//#pragma DATA_SECTION(BRLS_PWMB,"CpuToCla1MsgRAM")
//int16_t BRLS_PWMB;

//#pragma DATA_SECTION(BRLS_PWMC,"CpuToCla1MsgRAM")
//int16_t BRLS_PWMC;



#pragma DATA_SECTION(RotorPosition,"CLADataLS0")
uint32_t	RotorPosition;

#pragma DATA_SECTION(CPU_PHASE_ADVANCE,"CpuToCla1MsgRAM")
int16_t CPU_PHASE_ADVANCE;

#pragma DATA_SECTION(XZeroCalibratedAchannel,"CpuToCla1MsgRAM")
int16_t XZeroCalibratedAchannel;

#pragma DATA_SECTION(XZeroCalibratedBchannel,"CpuToCla1MsgRAM")
int16_t XZeroCalibratedBchannel;

#pragma DATA_SECTION(XZeroCalibratedCchannel,"CpuToCla1MsgRAM")
int16_t XZeroCalibratedCchannel;

#pragma DATA_SECTION(YZeroCalibratedAchannel,"CpuToCla1MsgRAM")
int16_t YZeroCalibratedAchannel;

#pragma DATA_SECTION(YZeroCalibratedBchannel,"CpuToCla1MsgRAM")
int16_t YZeroCalibratedBchannel;

#pragma DATA_SECTION(YZeroCalibratedCchannel,"CpuToCla1MsgRAM")
int16_t YZeroCalibratedCchannel;

#pragma DATA_SECTION(XBRLS_PWM,"CpuToCla1MsgRAM")
int16_t XBRLS_PWM ;

#pragma DATA_SECTION(YBRLS_PWM,"CpuToCla1MsgRAM")
int16_t YBRLS_PWM ;



//#pragma DATA_SECTION(AdjustingCurrent,"CpuToCla1MsgRAM")
//uint16_t AdjustingCurrent;

//#pragma DATA_SECTION(AdjustingVelocity,"CpuToCla1MsgRAM")
//uint16_t AdjustingVelocity;

#pragma DATA_SECTION(CheckCounter,"CLACounter")
int32_t CheckCounter = 0;

#pragma DATA_SECTION(IncrementalEncoderPosition,"CLAIencoder")
int32_t IncrementalEncoderPosition = 0xaaaaaaaa;

#pragma DATA_SECTION(FeedbackEncoderPosition,"CLAIencoder")
int32_t FeedbackEncoderPosition = 0xaaaabbbb;

#pragma DATA_SECTION(ClaSinTableBaseAddressPointer,"CLASinTableBAP")
float *ClaSinTableBaseAddressPointer = (_iq *)0x00800A;

#pragma DATA_SECTION(CurrentSinAddressPointer,"CLASinTblCrntAdd")
float *CurrentSinAddressPointer = (_iq *)0x00800A;

#pragma DATA_SECTION(CommutationFactor,"CpuToCla1MsgRAM")
float CommutationFactor;

#pragma DATA_SECTION(XCommutationFactor,"CpuToCla1MsgRAM")
float XCommutationFactor;

#pragma DATA_SECTION(YCommutationFactor,"CpuToCla1MsgRAM")
float YCommutationFactor;

#pragma DATA_SECTION(XEncoderResolution,"CpuToCla1MsgRAM")
uint16_t XEncoderResolution = 1000;

#pragma DATA_SECTION(YEncoderResolution,"CpuToCla1MsgRAM")
uint16_t YEncoderResolution = 1000;

#pragma DATA_SECTION(TablePointsNumber,"CpuToCla1MsgRAM")
uint16_t TablePointsNumber = 512;

#pragma DATA_SECTION(PiDiv3TablePointsNumber,"CpuToCla1MsgRAM")
uint16_t PiDiv3TablePointsNumber;

#pragma DATA_SECTION(PiDiv2TablePointsNumber,"CpuToCla1MsgRAM")
uint16_t PiDiv2TablePointsNumber;

#pragma DATA_SECTION(MotorOnFlag,"CpuToCla1MsgRAM")
bool MotorOnFlag = false;

#pragma DATA_SECTION(vlsof,"CpuToCla1MsgRAM")
DF22 vlsof = VLPF_DEFAULTS;

//**************************************************************

// CLA1 TO CPU1 VARIABLES
//**************************************************************
#pragma DATA_SECTION(MotorRevolutionsCounter,"CLADataLS0")
int32_t MotorRevolutionsCounter = 0;

#pragma DATA_SECTION(XMotorRevolutionsCounter,"CLADataLS0")
int32_t XMotorRevolutionsCounter = 0;

#pragma DATA_SECTION(YMotorRevolutionsCounter,"CLADataLS0")
int32_t YMotorRevolutionsCounter = 0;

//#pragma DATA_SECTION(AdcaResult0,"Cla1ToCpuMsgRAM")
//int16_t AdcaResult0;

//#pragma DATA_SECTION(AdcbResult0,"Cla1ToCpuMsgRAM")
//int16_t AdcbResult0;

//#pragma DATA_SECTION(AdccResult0,"Cla1ToCpuMsgRAM")
//int16_t AdccResult0;

//**************************************************************

// RAMDATA VARIABLES ( Only CPU1 can access the variables)
//**************************************************************

#pragma DATA_SECTION(InfCpuInterfaceRegs,"ramdata")
 volatile struct INFMCUINTERFACE_REGS InfCpuInterfaceRegs;

#pragma DATA_SECTION(XCommutState,"ramdata")
uint16_t XCommutState;

#pragma DATA_SECTION(YCommutState,"ramdata")
uint16_t YCommutState;

#pragma DATA_SECTION(SinArgument,"ramdata")
_iq SinArgument = 0;

#pragma DATA_SECTION(StartTick,"ramdata")
bool StartTick = false;

#pragma DATA_SECTION(PositionAdjusterRequestFlag,"ramdata")
bool PositionAdjusterRequestFlag = false;

#pragma DATA_SECTION(CPU_TABLE_VALUE_A,"ramdata")
float CPU_TABLE_VALUE_A;
#pragma DATA_SECTION(CPU_TABLE_VALUE_B,"ramdata")
float CPU_TABLE_VALUE_B;
#pragma DATA_SECTION(CPU_TABLE_VALUE_C,"ramdata")
float CPU_TABLE_VALUE_C;

#pragma DATA_SECTION(CPU_Brshl_Command_A,"ramdata")
int16_t CPU_Brshl_Command_A;
#pragma DATA_SECTION(CPU_Brshl_Command_B,"ramdata")
int16_t CPU_Brshl_Command_B;
#pragma DATA_SECTION(CPU_Brshl_Command_C,"ramdata")
int16_t CPU_Brshl_Command_C;

#pragma DATA_SECTION(XPolePairsNumber,"ramdata")
uint16_t XPolePairsNumber = 2;

#pragma DATA_SECTION(YPolePairsNumber,"ramdata")
uint16_t YPolePairsNumber = 2;

#pragma DATA_SECTION(CPU_RotorPosition,"ramdata")
uint16_t	CPU_RotorPosition;

#pragma DATA_SECTION(NextDesiredPosition,"ramdata")
int32_t NextDesiredPosition;

#pragma DATA_SECTION(IncEncPos,"ramdata")
int32_t IncEncPos = 0;

#pragma DATA_SECTION(AbsEncPos,"ramdata")
int32_t	AbsEncPos = 0;

#pragma DATA_SECTION(AbsEncPos,"ramdata")
int32_t	XAbsEncPos = 0;

#pragma DATA_SECTION(AbsEncPos,"ramdata")
int32_t	YAbsEncPos = 0;

#pragma DATA_SECTION(xCPUcounter,"ramdata")
int32_t xCPUcounter = 0;

#pragma DATA_SECTION(yCPUcounter,"ramdata")
int32_t yCPUcounter = 0;

#pragma DATA_SECTION(SamplingTimeQuad,"ramdata")
float SamplingTimeQuad;

#pragma DATA_SECTION(InversSamplingTimeQuad,"ramdata")
float InversSamplingTimeQuad;

#pragma DATA_SECTION(SamplingTime,"ramdata")
float SamplingTime;

#pragma DATA_SECTION(Drv8031DataReg1,"ramdata")
uint16_t Drv8031DataReg1 = 0;

#pragma DATA_SECTION(Drv8031DataReg2,"ramdata")
uint16_t Drv8031DataReg2 = 0;

#pragma DATA_SECTION(MOTOR_CONTROL_transmitFlagsPtr,"ramdata")
static sTransmitFlags *MOTOR_CONTROL_transmitFlagsPtr = NULL;

#pragma DATA_SECTION(XFrequency,"ramdata")
uint16_t XFrequency =1 ;

#pragma DATA_SECTION(YFrequency,"ramdata")
uint16_t YFrequency =1 ;

#pragma DATA_SECTION(XCycleCounter,"ramdata")
uint16_t XCycleCounter = 0 ;

#pragma DATA_SECTION(YCycleCounter,"ramdata")
uint16_t YCycleCounter = 0 ;

//****************************************************************

//******** X axis filters *****************
#pragma DATA_SECTION(xppi,"ramdata")
PI xppi = PI_DEFAULTS;

#pragma DATA_SECTION(xppi_cla,"CLADataLS0")
PI xppi_cla = PI_DEFAULTS;

#pragma DATA_SECTION(xvpi1,"ramdata")
PI xvpi1 = PI_DEFAULTS;

#pragma DATA_SECTION(xvpi1_cla,"CLADataLS0")
PI xvpi1_cla = PI_DEFAULTS;

#pragma DATA_SECTION(xvlpf,"ramdata")
DF22 xvlpf = VLPF_DEFAULTS;

#pragma DATA_SECTION(xvlpf_cla,"CLADataLS0")
DF22 xvlpf_cla = VLPF_DEFAULTS;

#pragma DATA_SECTION(xvlnf,"ramdata")
DF22 xvlnf = DF22_DEFAULTS;

#pragma DATA_SECTION(xvlnf_cla,"CLADataLS0")
DF22 xvlnf_cla = DF22_DEFAULTS;

#pragma DATA_SECTION(xdpi1,"ramdata")
PI xdpi1 = PI_DEFAULTS;

#pragma DATA_SECTION(xdpi1_cla,"CLADataLS0")
PI xdpi1_cla = PI_DEFAULTS;

#pragma DATA_SECTION(xdpi1,"ramdata")
PI xqpi1 = PI_DEFAULTS;

#pragma DATA_SECTION(xqpi1_cla,"CLADataLS0")
PI xqpi1_cla = PI_DEFAULTS;






//******** Y axis filters *****************
#pragma DATA_SECTION(yppi,"ramdata")
PI yppi = PI_DEFAULTS;

#pragma DATA_SECTION(yppi_cla,"CLADataLS0")
PI yppi_cla = PI_DEFAULTS;

#pragma DATA_SECTION(yvpi1,"ramdata")
PI yvpi1 = PI_DEFAULTS;

#pragma DATA_SECTION(yvpi1_cla,"CLADataLS0")
PI yvpi1_cla = PI_DEFAULTS;

#pragma DATA_SECTION(yvpid1_cla,"CLADataLS0")
PID yvpid1_cla = PID_DEFAULTS;

#pragma DATA_SECTION(yvlpf,"ramdata")
DF22 yvlpf = VLPF_DEFAULTS;

#pragma DATA_SECTION(yvlpf_cla,"CLADataLS0")
DF22 yvlpf_cla = VLPF_DEFAULTS;

#pragma DATA_SECTION(yrmslpf_cla,"CLADataLS0")
DF22 yrmslpf_cla = VLPF_DEFAULTS;

#pragma DATA_SECTION(yvellpf_cla,"CLADataLS0")
DF22 yvellpf_cla = VLPF_DEFAULTS;

//#pragma DATA_SECTION(yvlnf,"CpuToCla1MsgRAM")
#pragma DATA_SECTION(yvlnf,"ramdata")
DF22 yvlnf = DF22_DEFAULTS;

#pragma DATA_SECTION(yvlnf_cla,"CLADataLS0")
DF22 yvlnf_cla = DF22_DEFAULTS;

#pragma DATA_SECTION(ydpi1,"ramdata")
PI ydpi1 = PI_DEFAULTS;

#pragma DATA_SECTION(ydpi1_cla,"CLADataLS0")
PI ydpi1_cla = PI_DEFAULTS;

#pragma DATA_SECTION(ydpi1,"ramdata")
PI yqpi1 = PI_DEFAULTS;

#pragma DATA_SECTION(yqpi1_cla,"CLADataLS0")
PI yqpi1_cla = PI_DEFAULTS;






#pragma DATA_SECTION(filters,"ramdata")
PI* filters[FILTERS_NUM];

// Used to indirectly access PI module
#pragma DATA_SECTION(pif,"ramdata")
PI *pif[] = { &xdpi1,
 					 &xqpi1,
					 	 	 &xvpi1,
							 	 	 &xppi
				  };

#pragma DATA_SECTION(xsof,"ramdata")
DF22 *xsof[] = { &xvlpf,
						&xvlnf
};

#pragma DATA_SECTION(ysof,"ramdata")
DF22 *ysof[] = { &yvlpf,
						&yvlnf
};

#pragma DATA_SECTION(x_speed_estimation,"CLADataLS0")
SPEED_ESTIMATION_CLA x_speed_estimation;

#pragma DATA_SECTION(y_speed_estimation,"CLADataLS0")
SPEED_ESTIMATION_CLA y_speed_estimation;

#pragma DATA_SECTION(clarke1,"CLADataLS0")
CLARKE_CLA clarke1;

#pragma DATA_SECTION(park1,"CLADataLS0")
_PARK_CLA park1;

#pragma DATA_SECTION(ipark1,"CLADataLS0")
i_PARK_CLA ipark1;

#pragma DATA_SECTION(svgen1,"CLADataLS0")
SVGEN_CLA svgen1;


#pragma DATA_SECTION(claMotorCurrentsData,"CLADataLS0")
volatile struct MOTOR_CURRENT_DATA claMotorCurrentsData;

#pragma DATA_SECTION(X_sCpuToCla1ProfileCommandsStruct,"CpuToCla1MsgRAM")
volatile struct	CPU_TO_CLA_PROFILE_VARS X_sCpuToCla1ProfileCommandsStruct;

#pragma DATA_SECTION(Y_sCpuToCla1ProfileCommandsStruct,"CpuToCla1MsgRAM")
volatile struct	CPU_TO_CLA_PROFILE_VARS Y_sCpuToCla1ProfileCommandsStruct;

#pragma DATA_SECTION(MotorFalgsRegsArray,"CpuToCla1MsgRAM")
volatile struct MOTORFLAGS_REGS* MotorFalgsRegsArray[2];

#pragma DATA_SECTION(sCpuToCla1ProfileCommandsStructsArray,"ramdata")
volatile struct	CPU_TO_CLA_PROFILE_VARS *sCpuToCla1ProfileCommandsStructsArray[] = {&X_sCpuToCla1ProfileCommandsStruct,
																						&Y_sCpuToCla1ProfileCommandsStruct};

#pragma DATA_SECTION(sServoParametersStruct,"ramdata")
struct	 _SERVO_PARAMETERS	sServoParametersStruct;

#pragma DATA_SECTION(X_sCommonCpuProfileParametersStruct,"ramdata")
volatile struct	COMMON_CPU_PROFILE_VAR X_sCommonCpuProfileParametersStruct;

#pragma DATA_SECTION(Y_sCommonCpuProfileParametersStruct,"ramdata")
volatile struct	COMMON_CPU_PROFILE_VAR Y_sCommonCpuProfileParametersStruct;

#pragma DATA_SECTION(CommonCpuProfileParametersStructsArray,"ramdata")
volatile struct	COMMON_CPU_PROFILE_VAR *CommonCpuProfileParametersStructsArray[] = {&X_sCommonCpuProfileParametersStruct,
																															&Y_sCommonCpuProfileParametersStruct};
#pragma DATA_SECTION(X_sMotorControlVariablesStruct,"ramdata")
volatile struct _MOTOR_CONTROL_VAR X_sMotorControlVariablesStruct;

#pragma DATA_SECTION(Y_sMotorControlVariablesStruct,"ramdata")
volatile struct _MOTOR_CONTROL_VAR Y_sMotorControlVariablesStruct;

#pragma DATA_SECTION(sMotorControlVariablesStructsArray,"ramdata")
volatile struct _MOTOR_CONTROL_VAR *sMotorControlVariablesStructsArray[];

#pragma DATA_SECTION(XcCpuToClaProfileVarStruct,"ramdata")
volatile struct CPU_PROFILE_VAR XcCpuToClaProfileVarStruct;

#pragma DATA_SECTION(XFirstTargetPointProfileVarStruct,"ramdata")
volatile struct CPU_PROFILE_VAR XFirstTargetPointProfileVarStruct;

#pragma DATA_SECTION(XSecondTargetPointProfileVarStruct,"ramdata")
volatile struct CPU_PROFILE_VAR XSecondTargetPointProfileVarStruct;

#pragma DATA_SECTION(YcCpuToClaProfileVarStruct,"ramdata")
volatile struct CPU_PROFILE_VAR YcCpuToClaProfileVarStruct;

#pragma DATA_SECTION(YFirstTargetPointProfileVarStruct,"ramdata")
volatile struct CPU_PROFILE_VAR YFirstTargetPointProfileVarStruct;

#pragma DATA_SECTION(YSecondTargetPointProfileVarStruct,"ramdata")
volatile struct CPU_PROFILE_VAR YSecondTargetPointProfileVarStruct;


#pragma DATA_SECTION(ProfileVarStructsArray,"ramdata")
volatile struct CPU_PROFILE_VAR  *ProfileVarStructsArray[] = {&XcCpuToClaProfileVarStruct,
																	&XFirstTargetPointProfileVarStruct,
														  	  	  	  		&XSecondTargetPointProfileVarStruct,
																					&YcCpuToClaProfileVarStruct,
																							&YFirstTargetPointProfileVarStruct,
																									&YSecondTargetPointProfileVarStruct};

#pragma DATA_SECTION(ProfileVarStructsNum,"ramdata")
volatile struct CPU_PROFILE_VAR*	ProfileVarStructsNum[PROFILE_STRUCTS_NUM];

#pragma DATA_SECTION(cClaProfileVarStruct,"CLADataLS0")
volatile struct CLA_PROFILE_VAR cClaProfileVarStruct;

#pragma DATA_SECTION(X_pToMoveVariablesStruct,"ramdata")
volatile struct _P2MOVE_VAR X_pToMoveVariablesStruct;

#pragma DATA_SECTION(Y_pToMoveVariablesStruct,"ramdata")
volatile struct _P2MOVE_VAR Y_pToMoveVariablesStruct;

#pragma DATA_SECTION(pToMoveVarStructsArray,"ramdata")
volatile struct _P2MOVE_VAR* pToMoveVarStructsArray[] = {&X_pToMoveVariablesStruct,
														  	  	  &Y_pToMoveVariablesStruct};


#pragma DATA_SECTION(pAdjusterParametersStruct,"ramdata")
volatile struct ADJUSTER_PROFILE_PAR pAdjusterParametersStruct;

#pragma DATA_SECTION(XpAdjusterParametersStruct,"ramdata")
volatile struct ADJUSTER_PROFILE_PAR XpAdjusterParametersStruct;

#pragma DATA_SECTION(YpAdjusterParametersStruct,"ramdata")
volatile struct ADJUSTER_PROFILE_PAR YpAdjusterParametersStruct;

#pragma DATA_SECTION(pAdjusterParametersStructsPointer,"ramdata")
volatile struct ADJUSTER_PROFILE_PAR *pAdjusterParametersStructsPointer[] = {&XpAdjusterParametersStruct,
	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  &YpAdjusterParametersStruct};



#pragma DATA_SECTION(sControlVarStruct,"Cla1ToCpuMsgRAM")
volatile struct _CONTROL_VAR sControlVarStruct;

//#pragma DATA_SECTION(XsControlVarStruct,"Cla1ToCpuMsgRAM")
volatile struct _CONTROL_VAR XsControlVarStruct;

//#pragma DATA_SECTION(YsControlVarStruct,"Cla1ToCpuMsgRAM")
volatile struct _CONTROL_VAR YsControlVarStruct;

volatile struct _CONTROL_VAR *sControlVarStructsArray[]={&XsControlVarStruct,
																&YsControlVarStruct};

#pragma DATA_SECTION(sVelLoopNotchFilterStruct,"ramdata")
volatile struct NOTCH_FILTER_PAR sVelLoopNotchFilterStruct;

#pragma DATA_SECTION(sVelLoopSecondOrderFilterStruct,"ramdata")
volatile struct SECOND_ORDER_FILTER_PAR	sVelLoopSecondOrderFilterStruct = SOF_DEFAULTS;


/*****************************************************************************
 * Function name: RWLK_MotorControlInit
 *
 * Description: Initialize global structures and variables of the Motor Control module.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
void RWLK_MotorControlInit(void)
{
	MOTOR_CONTROL_transmitFlagsPtr = &TxArbitrator;
}



#pragma CODE_SECTION(ControlWordCheck,"ramfuncs")
void ControlWordCheck(void)
{
	uint8_t ErrCode = ERR_OK;
	uint16_t DriverData = 0;
	// Read Incremental Encoder Position
	//IncEncPos = sControlVarStruct.MotorPosition;
	//GpioDataRegs.GPCSET.bit.GPIO64 = 1;
#ifdef ABS_ENCODER
	ErrCode = AbsEncoder_GetData( &XAbsEncPos );
	XAbsEncPos<<=15;
	XAbsEncPos>>=16;
	//AbsEncPos = (~AbsEncPos + 1);
	if(sAbsoluteEncoderStruct.FeedbackConversionFlag == 2)
	{
		XAbsEncPos = (~XAbsEncPos + 1);
	}
	else if(sAbsoluteEncoderStruct.FeedbackConversionFlag == 3)
	{
		if(XAbsEncPos >= 0)
		{
			XAbsEncPos <<= 1;
			XAbsEncPos = ~XAbsEncPos;
			XAbsEncPos >>= 1;
			XAbsEncPos &= 0x00007fff;
		}
		else
		{
			// 1's complement  and set sign bits to one
			XAbsEncPos = ~XAbsEncPos;
			XAbsEncPos |= 0xffff8000;
		}
	}
	else if(sAbsoluteEncoderStruct.FeedbackConversionFlag == 4)
	{
		if(XAbsEncPos >= 0)
		{
			// 1's complement of 15LSB and set sign bits to zero
			XAbsEncPos = ~XAbsEncPos;
			XAbsEncPos &= 0x00007fff;
		}
		else
		{
			// 1's complement  and set sign bits to one
			XAbsEncPos = ~XAbsEncPos;
			XAbsEncPos |= 0xffff8000;
		}
		// 2's complement
		XAbsEncPos = (~XAbsEncPos + 1);

	}
	X_sCpuToCla1ProfileCommandsStruct.AbsoluteEncoderFeedback = XAbsEncPos;
	XAbsEncPos = XAbsEncPos - sAbsoluteEncoderStruct.LowSoftwareLimit;
#endif
	//DRV8031_SPI_nCS_Set();
	// DELAY_US(10);
	// GpioDataRegs.GPCCLEAR.bit.GPIO64 = 1;
	//DriverData = DRV8301_SPI_Write(&SpiaRegs,CNTRL_REG_2_ADDR,DRV8301_cntrl_reg2.all, X_Axis);
	//DRV8031_SPI_nCS_Reset();
	//DELAY_US(10);
	//DriverData = DRV8301_SPI_Read(&SpiaRegs,CNTRL_REG_2_ADDR, X_Axis);
	//DRV8031_SPI_nCS_Reset();
	// DELAY_US(10);

	// X AXIS LOGIC
	X_sCpuToCla1ProfileCommandsStruct.AdjusterRequestFlag = XpAdjusterParametersStruct.AdjusterRequestFlag;
 	if(MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.PARAMETERS_CHANGE_REQUEST == 1 && ClaCpuStatusRegs.X_MFLAGS_CLASTATUS.bit.X_PARAMETERS_CHANGED == 1)
 	{
 		MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.PARAMETERS_CHANGE_REQUEST = 0;
 	}
 	if(MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.COMMUTATION == 1)
	{
		//PwmStopDriver();
		//EALLOW;
		 //Set the EPWM1INT as the trigger for task 1
			//DmaClaSrcSelRegs.CLA1TASKSRCSEL1.bit.TASK1 = 0;
		//EDIS;
		//while(Cla1Regs.MIRUN.bit.INT1 != 0)continue;
		//GpioDataRegs.GPBCLEAR.bit.GPIO45 = 1;
		//WAITSTEP;
		//WAITSTEP;
		//asm(" ESTOP0");
		//Cla1ForceTask1andWait();
		//Argument = (float)XBRLS_PWM;
		//Cla1ForceTask2andWait();
		//PieCtrlRegs.PIEACK.all = M_INT11;
		IncEncPos = 0;
		//CommutCheck(MotorFalgsRegsArray,&XCommutState);
		switch(XCommutState)
		{
			case 1:
				MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.RESET_COMMUT = 1;
				XCommutState = 2;
				break;

			case  2:
				if(ClaCpuStatusRegs.X_MFLAGS_CLASTATUS.bit.X_COMMUT_IN_PROCESS == 0 && ClaCpuStatusRegs.X_MFLAGS_CLASTATUS.bit.X_COMMUT_DONE == 0)
				{
					MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.RESET_COMMUT = 0;
					XCommutState = 3;
				}
				break;

			case 3:
				//CalcCommutCommand
				CalcCommutCommands();
				CPU_TABLE_VALUE_A = ((float)XBRLS_PWM)*CPU_TABLE_VALUE_A;
				CPU_Brshl_Command_A = (int16_t)CPU_TABLE_VALUE_A;
				CPU_TABLE_VALUE_B = ((float)XBRLS_PWM)*CPU_TABLE_VALUE_B;
				CPU_Brshl_Command_B = (int16_t)CPU_TABLE_VALUE_B;
				CPU_TABLE_VALUE_C = ((float)XBRLS_PWM)*CPU_TABLE_VALUE_C;
				CPU_Brshl_Command_C = (int16_t)CPU_TABLE_VALUE_C;
				EPwm1Regs.CMPA.bit.CMPA = PWM_DC + CPU_Brshl_Command_A;
				EPwm2Regs.CMPA.bit.CMPA = PWM_DC + CPU_Brshl_Command_B;
				EPwm3Regs.CMPA.bit.CMPA = PWM_DC + CPU_Brshl_Command_C;
				XCommutState = 4;
				break;

			case 4:
					xCPUcounter = 0;
					XCommutState = 5;
					break;

			case 5:
				xCPUcounter++;
				if(xCPUcounter >= 4000)
				{
					xCPUcounter = 0;
					XCommutState = 6;
				}
				break;

			case 6:
				EQep1Regs.QPOSINIT = 0;
				EQep1Regs.QPOSCNT = 0;
				Cla1ForceTask3();
				XCommutState = 7;
				break;

			case 7:
				if(ClaCpuStatusRegs.X_MFLAGS_CLASTATUS.bit.X_COMMUT_IN_PROCESS == 0 && ClaCpuStatusRegs.X_MFLAGS_CLASTATUS.bit.X_COMMUT_DONE == 1)
				{
					//X_AxisServoOff();
					MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.COMMUTATION = 0;
					XBRLS_PWM = 0;
					X_AxisServoOn();
					XCommutState = 0;
					xCPUcounter = 0;
					TxArbitrator.XActionCompleteFlag = 1;
				}
				break;
		}


	}
	else if(MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.CALIBRATION_REQUEST == 1 && ClaCpuStatusRegs.X_MFLAGS_CLASTATUS.bit.X_COMMUT_DONE == 1)
	{

		// TODO
		switch(X_sCommonCpuProfileParametersStruct.CalibrationSwitch)
		{
		case 1:
			X_AxisServoOff();
				X_sCommonCpuProfileParametersStruct.CalibrationCounter = 0;
				X_sCommonCpuProfileParametersStruct.CalibrationSwitch = 2;
			break;

		case 2:
				X_sCommonCpuProfileParametersStruct.CalibrationCounter++;
			if(X_sCommonCpuProfileParametersStruct.CalibrationCounter >= 1000)
				{
				X_AxisServoOn();
					X_sCommonCpuProfileParametersStruct.CalibrationCounter = 0;
					X_sCommonCpuProfileParametersStruct.CalibrationSwitch = 3;
				}
			break;

		case 3:
			X_sCommonCpuProfileParametersStruct.CalibrationCounter++;
			if(X_sCommonCpuProfileParametersStruct.CalibrationCounter >= 1000)
			{
				X_sCommonCpuProfileParametersStruct.CalibrationCounter = 0;
				X_sCommonCpuProfileParametersStruct.CalibrationSwitch = 4;
			}
			break;

		case 4:

			if(XpAdjusterParametersStruct.AdjusterProfileVelocity < XDEFAULT_MAX_MOVE_VELOCITY_IN_COUNTS_PER_SECOND)
			{
				XpAdjusterParametersStruct.AdjusterProfileVelocity = XDEFAULT_MAX_MOVE_VELOCITY_IN_COUNTS_PER_SECOND;
			}
			X_pToMoveVariablesStruct.ProfileMaxVelocity = XpAdjusterParametersStruct.AdjusterProfileVelocity;
			X_pToMoveVariablesStruct.ProfileAcceleration = XpAdjusterParametersStruct.AdjusterProfileVelocity*10.0;//pAdjusterParametersStruct.AdjusterProfileAcc;
			X_pToMoveVariablesStruct.ProfileJerk = X_pToMoveVariablesStruct.ProfileAcceleration*10.0; //pAdjusterParametersStruct.AdjusterProfileJerk;
			X_pToMoveVariablesStruct.StartTargetPoint = sControlVarStruct.XMotorPosition;
			X_pToMoveVariablesStruct.FinalTargetPoint = X_pToMoveVariablesStruct.StartTargetPoint - YMAXIMUM_PERMISSIBLE_TRAVEL_IN_COUNTS;
			//Y_sCommonCpuProfileParametersStruct.MaxCalibrationTime = (uint32_t)((double)YMAXIMUM_PERMISSIBLE_TRAVEL_IN_COUNTS/Y_pToMoveVariablesStruct.ProfileMaxVelocity);

			X_sCommonCpuProfileParametersStruct.Sn = (float)sControlVarStruct.XMotorPosition;
			X_pToMoveVariablesStruct.DesiredPosition = X_pToMoveVariablesStruct.FinalTargetPoint;
			X_sCommonCpuProfileParametersStruct.TargetRadiusSettlingTime = 100;
			X_sCommonCpuProfileParametersStruct.TargetRadiusCounter = 0;
			X_sCommonCpuProfileParametersStruct.TargetRadius = 20;
			X_sCommonCpuProfileParametersStruct.CheckSettling = 1;
			X_pToMoveVariablesStruct.Vmax = X_pToMoveVariablesStruct.ProfileMaxVelocity;
			X_pToMoveVariablesStruct.AccelerationTime = X_pToMoveVariablesStruct.Vmax/X_pToMoveVariablesStruct.ProfileAcceleration;
			XcCpuToClaProfileVarStruct.AT =  (uint32_t)(X_pToMoveVariablesStruct.AccelerationTime*1000.0);
			X_pToMoveVariablesStruct.JerkFactor = X_pToMoveVariablesStruct.ProfileAcceleration/X_pToMoveVariablesStruct.ProfileJerk;
			X_pToMoveVariablesStruct.JerkTime = X_pToMoveVariablesStruct.JerkFactor*(double)XcCpuToClaProfileVarStruct.AT;
			XcCpuToClaProfileVarStruct.N1 = (uint32_t)(X_pToMoveVariablesStruct.JerkTime);
			XcCpuToClaProfileVarStruct.N2 = XcCpuToClaProfileVarStruct.AT;
			XcCpuToClaProfileVarStruct.N3 = XcCpuToClaProfileVarStruct.N2 + XcCpuToClaProfileVarStruct.N1;


			X_pToMoveVariablesStruct.JerkTime = (double)(XcCpuToClaProfileVarStruct.N1);
			X_pToMoveVariablesStruct.ProfileAcceleration = -X_pToMoveVariablesStruct.ProfileMaxVelocity/XcCpuToClaProfileVarStruct.AT;
			X_pToMoveVariablesStruct.ProfileJerk = X_pToMoveVariablesStruct.ProfileAcceleration/XcCpuToClaProfileVarStruct.N1;
			X_sCommonCpuProfileParametersStruct.Jn = X_pToMoveVariablesStruct.ProfileJerk;
			X_sCommonCpuProfileParametersStruct.Vn = X_pToMoveVariablesStruct.Vmax*0.001; //Counts/msec
			X_sCommonCpuProfileParametersStruct.Vn = -X_sCommonCpuProfileParametersStruct.Vn;


			//ProfileParsPrepareToMove(pToMoveVarStructsArray[Y_Axis], ProfileVarStructsArray[YCpuToClaProfileVars],CommonCpuProfileParametersStructsArray[Y_Axis]);
			X_sCommonCpuProfileParametersStruct.MaxCalibrationTime = (uint32_t)((double)XMAXIMUM_PERMISSIBLE_TRAVEL_IN_COUNTS/X_pToMoveVariablesStruct.Vmax);
			X_sCommonCpuProfileParametersStruct.MaxCalibrationTime = X_sCommonCpuProfileParametersStruct.MaxCalibrationTime*1000;

					X_sCommonCpuProfileParametersStruct.CalibrationCounter = 0;
					X_sCommonCpuProfileParametersStruct.CalibrationSwitch = 5;
			//Y_sCommonCpuProfileParametersStruct.ProfilerSwitch = 1;
			X_sCommonCpuProfileParametersStruct.ProfileCounter = 0;
			X_sCommonCpuProfileParametersStruct.OvercurrentFaultFlag = false;

			break;

		case 5:

			X_sCommonCpuProfileParametersStruct.CalibrationCounter++;
			X_sCommonCpuProfileParametersStruct.Sn = X_sCommonCpuProfileParametersStruct.Sn + X_sCommonCpuProfileParametersStruct.Vn;
			X_sCommonCpuProfileParametersStruct.NextTargetPosition = (int32_t)X_sCommonCpuProfileParametersStruct.Sn;
			X_pToMoveVariablesStruct.DesiredPosition = X_sCommonCpuProfileParametersStruct.NextTargetPosition;
			X_sCpuToCla1ProfileCommandsStruct.PositionCommand = X_pToMoveVariablesStruct.DesiredPosition;
			//Y_sCpuToCla1ProfileCommandsStruct.PositionCommand = ProfileCalc(pToMoveVarStructsArray[Y_Axis], ProfileVarStructsArray[YCpuToClaProfileVars],CommonCpuProfileParametersStructsArray[Y_Axis], Y_Axis);
			if((X_sCommonCpuProfileParametersStruct.OvercurrentFaultFlag == true) || (X_sCommonCpuProfileParametersStruct.CalibrationCounter >= X_sCommonCpuProfileParametersStruct.MaxCalibrationTime))
			{
				X_sCommonCpuProfileParametersStruct.ProfilerSwitch = 0;
				X_sCommonCpuProfileParametersStruct.Sn = (float)sControlVarStruct.YMotorPosition;
				X_sCommonCpuProfileParametersStruct.OvercurrentFaultFlag = false;
				//asm(" ESTOP0");
				X_sCommonCpuProfileParametersStruct.CalibrationSwitch = 6;
				X_sCommonCpuProfileParametersStruct.CalibrationCounter = 0;
				//Y_AxisServoOff();

			}
			break;

		case 6:

			X_sMotorControlVariablesStruct.PreviousIncrementalEncoderData = sControlVarStruct.XMotorPosition;
			X_pToMoveVariablesStruct.ProfileMaxVelocity = 65536.0;
			X_pToMoveVariablesStruct.ProfileAcceleration = XpAdjusterParametersStruct.AdjusterProfileVelocity*10.0;//pAdjusterParametersStruct.AdjusterProfileAcc;
			X_pToMoveVariablesStruct.ProfileJerk = X_pToMoveVariablesStruct.ProfileAcceleration*10.0; //pAdjusterParametersStruct.AdjusterProfileJerk;
			X_pToMoveVariablesStruct.StartTargetPoint = sControlVarStruct.XMotorPosition;
			//X_pToMoveVariablesStruct.StartTargetPoint = TempPar;
			X_sCommonCpuProfileParametersStruct.Sn = (float)sControlVarStruct.XMotorPosition;
			X_pToMoveVariablesStruct.FinalTargetPoint = X_pToMoveVariablesStruct.StartTargetPoint + 196000;
			X_pToMoveVariablesStruct.DesiredPosition = X_pToMoveVariablesStruct.FinalTargetPoint;
			X_sCommonCpuProfileParametersStruct.TargetRadiusSettlingTime = 100;
			X_sCommonCpuProfileParametersStruct.TargetRadiusCounter = 0;
			X_sCommonCpuProfileParametersStruct.TargetRadius = 20;
			X_sCommonCpuProfileParametersStruct.CheckSettling = 1;
			MotorOnFlag = 1;
			ProfileParsPrepareToMove(pToMoveVarStructsArray[X_Axis], ProfileVarStructsArray[XCpuToClaProfileVars], CommonCpuProfileParametersStructsArray[X_Axis]);
				//sControlVarStruct.YMotorPosition
			X_sCommonCpuProfileParametersStruct.CalibrationSwitch = 7;
			break;

		case 7:
			X_sCommonCpuProfileParametersStruct.CalibrationCounter++;

			if (X_sCommonCpuProfileParametersStruct.CalibrationCounter >= 1000) {

				MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.CALIBRATION_REQUEST = 0;
				X_sCommonCpuProfileParametersStruct.CalibrationCounter = 0;
			}

			break;
		}

	}
	else if(MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.POSITION_ADJUSTER == 1)
	{
		//PositionAdjusterExec();	TODO
		switch(XpAdjusterParametersStruct.AdjusterSwitch)
		{
		case 1:
			X_pToMoveVariablesStruct.ProfileMaxVelocity = XpAdjusterParametersStruct.AdjusterProfileVelocity;
			X_pToMoveVariablesStruct.ProfileAcceleration = XpAdjusterParametersStruct.AdjusterProfileAcc;
			X_pToMoveVariablesStruct.ProfileJerk = XpAdjusterParametersStruct.AdjusterProfileJerk;
			X_pToMoveVariablesStruct.FinalTargetPoint = XpAdjusterParametersStruct.AdjusterProfileTarget;
			X_pToMoveVariablesStruct.StartTargetPoint = sControlVarStruct.XMotorPosition;
			ProfileParsPrepareToMove(pToMoveVarStructsArray[X_Axis], ProfileVarStructsArray[XCpuToClaAdjusterFirstMove],CommonCpuProfileParametersStructsArray[X_Axis]);
			X_pToMoveVariablesStruct.FinalTargetPoint = X_pToMoveVariablesStruct.StartTargetPoint;
			X_pToMoveVariablesStruct.StartTargetPoint = XpAdjusterParametersStruct.AdjusterProfileTarget;
			ProfileParsPrepareToMove(pToMoveVarStructsArray[X_Axis], ProfileVarStructsArray[XCpuToClaAdjusterSecondMove],CommonCpuProfileParametersStructsArray[Y_Axis]);
			X_pToMoveVariablesStruct.StartTargetPoint = X_pToMoveVariablesStruct.FinalTargetPoint;
			X_pToMoveVariablesStruct.FinalTargetPoint = XpAdjusterParametersStruct.AdjusterProfileTarget;
			X_sCommonCpuProfileParametersStruct.Sn = sControlVarStruct.XPositionFeedback;
			X_sCommonCpuProfileParametersStruct.TargetRadiusSettlingTime = 100;
			X_sCommonCpuProfileParametersStruct.TargetRadiusCounter = 0;
			X_sCommonCpuProfileParametersStruct.TargetRadius = 20;
			XpAdjusterParametersStruct.DwellCounter = 0;
			X_sCommonCpuProfileParametersStruct.TargetRadiusCounter = 0;
			XpAdjusterParametersStruct.AdjusterSwitch = 2;
			break;

		case 2:

			XpAdjusterParametersStruct.DwellCounter++;
			if((XpAdjusterParametersStruct.DwellCounter >= XpAdjusterParametersStruct.DwellTime)&&(XpAdjusterParametersStruct.AdjusterRequestFlag == 1))
			{

				X_pToMoveVariablesStruct.ProfileMaxVelocity = XpAdjusterParametersStruct.AdjusterProfileVelocity;
				X_pToMoveVariablesStruct.ProfileAcceleration = XpAdjusterParametersStruct.AdjusterProfileVelocity*10.0;//pAdjusterParametersStruct.AdjusterProfileAcc;
				X_pToMoveVariablesStruct.ProfileJerk = X_pToMoveVariablesStruct.ProfileAcceleration*10.0; //pAdjusterParametersStruct.AdjusterProfileJerk;
				X_pToMoveVariablesStruct.StartTargetPoint = sControlVarStruct.XMotorPosition;
				//X_pToMoveVariablesStruct.StartTargetPoint = TempPar;
				X_sCommonCpuProfileParametersStruct.Sn = (float)sControlVarStruct.XMotorPosition;
				X_pToMoveVariablesStruct.FinalTargetPoint = XpAdjusterParametersStruct.AdjusterProfileTarget;
				X_pToMoveVariablesStruct.DesiredPosition = X_pToMoveVariablesStruct.FinalTargetPoint;
				X_sCommonCpuProfileParametersStruct.TargetRadiusSettlingTime = 100;
				X_sCommonCpuProfileParametersStruct.TargetRadiusCounter = 0;
				X_sCommonCpuProfileParametersStruct.TargetRadius = 20;
				X_sCommonCpuProfileParametersStruct.CheckSettling = 1;
				MotorOnFlag = 1;
				ProfileParsPrepareToMove(pToMoveVarStructsArray[X_Axis], ProfileVarStructsArray[XCpuToClaProfileVars],CommonCpuProfileParametersStructsArray[X_Axis]);
				XpAdjusterParametersStruct.AdjusterSwitch = 3;
			}
			else if(XpAdjusterParametersStruct.AdjusterRequestFlag == 0)
			{
					MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.POSITION_ADJUSTER = 0;
					XpAdjusterParametersStruct.AdjusterSwitch = 0;
					XpAdjusterParametersStruct.DwellCounter = 0;
					X_pToMoveVariablesStruct.ProfileMaxVelocity = 0;
					X_pToMoveVariablesStruct.ProfileAcceleration = 0;
					X_pToMoveVariablesStruct.ProfileJerk = 0;
					X_AxisServoOff();
					X_AxisServoOn();

			}

			break;

		case 3:
			X_sCpuToCla1ProfileCommandsStruct.PositionCommand = ProfileCalc(pToMoveVarStructsArray[X_Axis], ProfileVarStructsArray[XCpuToClaProfileVars],CommonCpuProfileParametersStructsArray[X_Axis], X_Axis);
			if((sControlVarStruct.XMotorPosition >= (X_pToMoveVariablesStruct.DesiredPosition - (int32_t)X_sCommonCpuProfileParametersStruct.TargetRadius))&&(sControlVarStruct.XMotorPosition <= (X_pToMoveVariablesStruct.DesiredPosition + (int32_t)X_sCommonCpuProfileParametersStruct.TargetRadius)))
			{
				if(X_sCommonCpuProfileParametersStruct.ProfileEnd == 1)
				{
					X_sCommonCpuProfileParametersStruct.TargetRadiusCounter++;
					if(X_sCommonCpuProfileParametersStruct.TargetRadiusCounter >= X_sCommonCpuProfileParametersStruct.TargetRadiusSettlingTime)
					{
						NextDesiredPosition = X_pToMoveVariablesStruct.FinalTargetPoint;
						X_pToMoveVariablesStruct.ProfileMaxVelocity = XpAdjusterParametersStruct.AdjusterProfileVelocity;
						X_pToMoveVariablesStruct.ProfileAcceleration = XpAdjusterParametersStruct.AdjusterProfileAcc;
						X_pToMoveVariablesStruct.ProfileJerk = XpAdjusterParametersStruct.AdjusterProfileJerk;
						//ProfileParsPrepareToMove(&NextDesiredPosition);
						X_sCommonCpuProfileParametersStruct.Sn = sControlVarStruct.XPositionFeedback;
						XpAdjusterParametersStruct.DwellCounter = 0;
						XpAdjusterParametersStruct.AdjusterSwitch = 4;
						X_sCommonCpuProfileParametersStruct.TargetRadiusCounter = 0;
						TxArbitrator.XActionCompleteFlag = 1;
					}
				}
			}
			break;


		case 4:
			XpAdjusterParametersStruct.DwellCounter++;
			if((XpAdjusterParametersStruct.DwellCounter >= XpAdjusterParametersStruct.DwellTime)&&(XpAdjusterParametersStruct.AdjusterRequestFlag == 1))
			{
				X_pToMoveVariablesStruct.ProfileMaxVelocity = XpAdjusterParametersStruct.AdjusterProfileVelocity;
				X_pToMoveVariablesStruct.ProfileAcceleration = XpAdjusterParametersStruct.AdjusterProfileVelocity*10.0;//pAdjusterParametersStruct.AdjusterProfileAcc;
				X_pToMoveVariablesStruct.ProfileJerk = X_pToMoveVariablesStruct.ProfileAcceleration*10.0; //pAdjusterParametersStruct.AdjusterProfileJerk;
				X_pToMoveVariablesStruct.FinalTargetPoint = X_pToMoveVariablesStruct.StartTargetPoint;
				X_pToMoveVariablesStruct.StartTargetPoint = sControlVarStruct.XMotorPosition;
				//X_pToMoveVariablesStruct.StartTargetPoint = TempPar;
				X_sCommonCpuProfileParametersStruct.Sn = (float)sControlVarStruct.XMotorPosition;
				X_pToMoveVariablesStruct.DesiredPosition = X_pToMoveVariablesStruct.FinalTargetPoint;
				X_sCommonCpuProfileParametersStruct.TargetRadiusSettlingTime = 100;
				X_sCommonCpuProfileParametersStruct.TargetRadiusCounter = 0;
				X_sCommonCpuProfileParametersStruct.TargetRadius = 20;
				X_sCommonCpuProfileParametersStruct.CheckSettling = 1;
				MotorOnFlag = 1;
				ProfileParsPrepareToMove(pToMoveVarStructsArray[X_Axis], ProfileVarStructsArray[XCpuToClaProfileVars],CommonCpuProfileParametersStructsArray[X_Axis]);
				XpAdjusterParametersStruct.AdjusterSwitch = 5;

			}
			else if(XpAdjusterParametersStruct.AdjusterRequestFlag == 0)
			{
					MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.POSITION_ADJUSTER = 0;
					XpAdjusterParametersStruct.AdjusterSwitch = 0;
					XpAdjusterParametersStruct.DwellCounter = 0;
					X_pToMoveVariablesStruct.ProfileMaxVelocity = 0;
					X_pToMoveVariablesStruct.ProfileAcceleration = 0;
					X_pToMoveVariablesStruct.ProfileJerk = 0;
					X_AxisServoOff();
					X_AxisServoOn();

			}
			break;



		case 5:
			X_sCpuToCla1ProfileCommandsStruct.PositionCommand = ProfileCalc(pToMoveVarStructsArray[X_Axis], ProfileVarStructsArray[XCpuToClaProfileVars], CommonCpuProfileParametersStructsArray[X_Axis], X_Axis);
			if((sControlVarStruct.XMotorPosition >= (X_pToMoveVariablesStruct.DesiredPosition - (int32_t)X_sCommonCpuProfileParametersStruct.TargetRadius))&&(sControlVarStruct.XMotorPosition <= (X_pToMoveVariablesStruct.DesiredPosition + (int32_t)X_sCommonCpuProfileParametersStruct.TargetRadius)))
			{
				if(X_sCommonCpuProfileParametersStruct.ProfileEnd == 1)
				{
					X_sCommonCpuProfileParametersStruct.TargetRadiusCounter++;
					if(X_sCommonCpuProfileParametersStruct.TargetRadiusCounter >= X_sCommonCpuProfileParametersStruct.TargetRadiusSettlingTime)
					{
						NextDesiredPosition = X_pToMoveVariablesStruct.FinalTargetPoint;
						X_pToMoveVariablesStruct.ProfileMaxVelocity = XpAdjusterParametersStruct.AdjusterProfileVelocity;
						X_pToMoveVariablesStruct.ProfileAcceleration = XpAdjusterParametersStruct.AdjusterProfileAcc;
						X_pToMoveVariablesStruct.ProfileJerk = XpAdjusterParametersStruct.AdjusterProfileJerk;
						X_sCommonCpuProfileParametersStruct.Sn = sControlVarStruct.XPositionFeedback;
						XpAdjusterParametersStruct.DwellCounter = 0;
						XpAdjusterParametersStruct.AdjusterSwitch = 2;
						X_sCommonCpuProfileParametersStruct.TargetRadiusCounter = 0;
						TxArbitrator.XActionCompleteFlag = 1;
					}
				}
			}
			break;
		}
	}
	else if(MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.VELOCITY_ADJUSTER == 1)
	{
		//VelocityAdjusterExec(); TODO
		switch(XpAdjusterParametersStruct.AdjusterSwitch)
		{
		case 1:
			X_sCpuToCla1ProfileCommandsStruct.VelocityFeedforwardCommand = (int32_t)XpAdjusterParametersStruct.AdjusterProfileVelocity;
			XpAdjusterParametersStruct.DwellCounter = 0;
			XpAdjusterParametersStruct.AdjusterSwitch = 2;
			break;

		case 2:
				XpAdjusterParametersStruct.DwellCounter++;
				if((XpAdjusterParametersStruct.DwellCounter >= XpAdjusterParametersStruct.DwellTime)&&(XpAdjusterParametersStruct.AdjusterRequestFlag == 1))
				{
					X_sCpuToCla1ProfileCommandsStruct.VelocityFeedforwardCommand = -(int32_t)XpAdjusterParametersStruct.AdjusterProfileVelocity;
					XpAdjusterParametersStruct.AdjusterSwitch = 3;
					XpAdjusterParametersStruct.DwellCounter = 0;
				}
				else if(XpAdjusterParametersStruct.AdjusterRequestFlag == 0)
				{
					MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.VELOCITY_ADJUSTER = 0;
					XpAdjusterParametersStruct.AdjusterSwitch = 0;
					X_pToMoveVariablesStruct.ProfileMaxVelocity = 0;
					X_pToMoveVariablesStruct.ProfileAcceleration = 0;
					X_pToMoveVariablesStruct.ProfileJerk = 0;
					XpAdjusterParametersStruct.AdjusterProfileVelocity = 0;
					X_sCommonCpuProfileParametersStruct.ProfilerSwitch = 0;
					X_sCpuToCla1ProfileCommandsStruct.VelocityFeedforwardCommand = 0;
					XpAdjusterParametersStruct.DwellCounter = 0;
					X_AxisServoOff();
				}

			//iVelocityCommand = (int16_t)(sControlVarStruct.VelocityCommand*0.4096); //2048/5000
			//DacbRegs.DACVALS.bit.DACVALS = 2048;
				break;

		case 3:
			XpAdjusterParametersStruct.DwellCounter++;
			if((XpAdjusterParametersStruct.DwellCounter >= XpAdjusterParametersStruct.DwellTime)&&(XpAdjusterParametersStruct.AdjusterRequestFlag == 1))
			{
				X_sCpuToCla1ProfileCommandsStruct.VelocityFeedforwardCommand = (int32_t)XpAdjusterParametersStruct.AdjusterProfileVelocity;
				XpAdjusterParametersStruct.DwellCounter = 0;
				XpAdjusterParametersStruct.AdjusterSwitch = 2;
			}
			else if(XpAdjusterParametersStruct.AdjusterRequestFlag == 0)
			{

				MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.VELOCITY_ADJUSTER = 0;
				XpAdjusterParametersStruct.AdjusterSwitch = 0;
				X_pToMoveVariablesStruct.ProfileMaxVelocity = 0;
				X_pToMoveVariablesStruct.ProfileAcceleration = 0;
				X_pToMoveVariablesStruct.ProfileJerk = 0;
				XpAdjusterParametersStruct.AdjusterProfileVelocity = 0;
				X_sCommonCpuProfileParametersStruct.ProfilerSwitch = 0;
				X_sCpuToCla1ProfileCommandsStruct.VelocityFeedforwardCommand = 0;
				XpAdjusterParametersStruct.DwellCounter = 0;
				X_AxisServoOff();
			}
			break;

		}
	}
	else if(MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.CURRENT_ADJUSTER == 1)
	{
		//CurrentAdjusterExec(); // TODO
		switch(XpAdjusterParametersStruct.AdjusterSwitch)
		{
		case 1:
			X_sCpuToCla1ProfileCommandsStruct.CurrentAdjusterCommand = XpAdjusterParametersStruct.AdjusterCurrentCommand;
			XpAdjusterParametersStruct.DwellCounter = 0;
			XpAdjusterParametersStruct.AdjusterSwitch = 2;
			break;
		case 2:
			//sCpuToCla1ProfileCommandsStruct.CurrentAdjusterCommand = pAdjusterParametersStruct.AdjusterCurrentCommand;
			XpAdjusterParametersStruct.DwellCounter++;
			if((XpAdjusterParametersStruct.DwellCounter >= XpAdjusterParametersStruct.DwellTime)&&(XpAdjusterParametersStruct.AdjusterRequestFlag == 1))
			{
				X_sCpuToCla1ProfileCommandsStruct.CurrentAdjusterCommand = -XpAdjusterParametersStruct.AdjusterCurrentCommand;
				XpAdjusterParametersStruct.DwellCounter = 0;
				XpAdjusterParametersStruct.AdjusterSwitch = 3;
			}
			else if(XpAdjusterParametersStruct.AdjusterRequestFlag == 0)
			{
				MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.CURRENT_ADJUSTER = 0;
				XpAdjusterParametersStruct.AdjusterSwitch = 0;
				X_pToMoveVariablesStruct.ProfileMaxVelocity = 0;
				X_pToMoveVariablesStruct.ProfileAcceleration = 0;
				X_pToMoveVariablesStruct.ProfileJerk = 0;
				XpAdjusterParametersStruct.AdjusterCurrentCommand = 0;
				X_sCpuToCla1ProfileCommandsStruct.CurrentAdjusterCommand = 0;
				X_sCommonCpuProfileParametersStruct.ProfilerSwitch = 0;
				EPwm1Regs.CMPA.bit.CMPA = 1250;
				EPwm2Regs.CMPA.bit.CMPA = 1250;
				EPwm3Regs.CMPA.bit.CMPA = 1250;
				DacbRegs.DACVALS.bit.DACVALS = 2048;
				DaccRegs.DACVALS.bit.DACVALS = 2048;
				DacaRegs.DACVALS.bit.DACVALS = 2048;
				MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.ENABLE = 0;
			}
			break;

		case 3:
			XpAdjusterParametersStruct.DwellCounter++;
			if((XpAdjusterParametersStruct.DwellCounter >= XpAdjusterParametersStruct.DwellTime)&&(XpAdjusterParametersStruct.AdjusterRequestFlag == 1))
			{
				X_sCpuToCla1ProfileCommandsStruct.CurrentAdjusterCommand = XpAdjusterParametersStruct.AdjusterCurrentCommand;
				XpAdjusterParametersStruct.DwellCounter = 0;
				XpAdjusterParametersStruct.AdjusterSwitch = 2;
			}
			else if(XpAdjusterParametersStruct.AdjusterRequestFlag == 0)
			{
				MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.CURRENT_ADJUSTER = 0;
				XpAdjusterParametersStruct.AdjusterSwitch = 0;
				X_pToMoveVariablesStruct.ProfileMaxVelocity = 0;
				X_pToMoveVariablesStruct.ProfileAcceleration = 0;
				X_pToMoveVariablesStruct.ProfileJerk = 0;
				XpAdjusterParametersStruct.AdjusterCurrentCommand = 0;
				X_sCpuToCla1ProfileCommandsStruct.CurrentAdjusterCommand = 0;
				X_sCommonCpuProfileParametersStruct.ProfilerSwitch = 0;
				EPwm1Regs.CMPA.bit.CMPA = 1250;
				EPwm2Regs.CMPA.bit.CMPA = 1250;
				EPwm3Regs.CMPA.bit.CMPA = 1250;
				DacbRegs.DACVALS.bit.DACVALS = 2048;
				DaccRegs.DACVALS.bit.DACVALS = 2048;
				DacaRegs.DACVALS.bit.DACVALS = 2048;
				MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.ENABLE = 0;
			}
			break;

		}
	}
	else if(MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.SERVO_ANALYSYS == 1 && ClaCpuStatusRegs.X_MFLAGS_CLASTATUS.bit.X_COMMUT_DONE == 1 && ClaCpuStatusRegs.X_MFLAGS_CLASTATUS.bit.X_ENABLED == 1)
	{
		if(XCycleCounter < 10000/XFrequency)
		{
			XCycleCounter++;
			X_sCpuToCla1ProfileCommandsStruct.PositionCommand = (int32_t)(5000 * sin(2 * Pi * XFrequency *(XCycleCounter * 0.001)));
			//X_pToMoveVariablesStruct.StartServo
			//sCpuToCla1ProfileCommandsStruct.PositionCommand = IQ16sinPU((float)Frequency/(CycleCounter * 0.001));
			//sCpuToCla1ProfileCommandsStruct.PositionCommand = 0x13880000*sCpuToCla1ProfileCommandsStruct.PositionCommand;
			//sCpuToCla1ProfileCommandsStruct.PositionCommand >>16;
			TxArbitrator.SendXServoAnalyzerMeasurementsFlag = 1;
		}
		else
		{
			XCycleCounter = 0 ;
			XFrequency++;
		}
		if(XFrequency > 10)
		{
			MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.SERVO_ANALYSYS = 0;
			X_AxisServoOff();
		}
	}
	else
	{
			X_sCpuToCla1ProfileCommandsStruct.PositionCommand = ProfileCalc(pToMoveVarStructsArray[X_Axis], ProfileVarStructsArray[XCpuToClaProfileVars], CommonCpuProfileParametersStructsArray[X_Axis], X_Axis);
			if(med_ctr.handle_mode != ACTIVE_MODE)
				{
					X_sCpuToCla1ProfileCommandsStruct.VelocityFeedforwardCommand = (int32_t)((0.04)*X_sCommonCpuProfileParametersStruct.Vn);
					X_sCpuToCla1ProfileCommandsStruct.AccelerationFeedforwardCommand = (int32_t)((0.0015)*CommonCpuProfileParametersStructsArray[X_Axis]->An);
				}
				else
				{
					X_sCpuToCla1ProfileCommandsStruct.VelocityFeedforwardCommand = (int32_t)(X_sCommonCpuProfileParametersStruct.Vn);
					X_sCpuToCla1ProfileCommandsStruct.AccelerationFeedforwardCommand = 0;
				}

			//if((sControlVarStruct.PositionFeedback >= (float)(X_pToMoveVariablesStruct.DesiredPosition - X_sCommonCpuProfileParametersStruct.TargetRadius))&&(sControlVarStruct.PositionFeedback <= (float)(X_pToMoveVariablesStruct.DesiredPosition + X_sCommonCpuProfileParametersStruct.TargetRadius)))
			if((sControlVarStruct.XMotorPosition >= (X_pToMoveVariablesStruct.DesiredPosition - X_sCommonCpuProfileParametersStruct.TargetRadius))&&(sControlVarStruct.XMotorPosition <= (X_pToMoveVariablesStruct.DesiredPosition + X_sCommonCpuProfileParametersStruct.TargetRadius)))
			{
				if((X_sCommonCpuProfileParametersStruct.ProfileEnd == 1)&&(X_sCommonCpuProfileParametersStruct.CheckSettling == 1))
				{
					X_sCommonCpuProfileParametersStruct.TargetRadiusCounter++;
					if(X_sCommonCpuProfileParametersStruct.TargetRadiusCounter >= X_sCommonCpuProfileParametersStruct.TargetRadiusSettlingTime)
					{
						XpAdjusterParametersStruct.AdjusterSwitch = 0;
						XpAdjusterParametersStruct.DwellCounter = 0;
						X_pToMoveVariablesStruct.ProfileMaxVelocity = 0;
						X_pToMoveVariablesStruct.ProfileAcceleration = 0;
						X_pToMoveVariablesStruct.ProfileJerk = 0;
						X_sCommonCpuProfileParametersStruct.ProfilerSwitch = 0;
						X_sCommonCpuProfileParametersStruct.TargetRadiusCounter = 0;
						X_sCommonCpuProfileParametersStruct.CheckSettling = 0;
						TxArbitrator.XActionCompleteFlag = 1;
					}
				}
			}
			if(MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.ENABLE == 1)
			{
				if(cClaProfileVarStruct.AbsoluteEncoderPosition >= (sAbsoluteEncoderStruct.HighSoftwareLimit - X_sCommonCpuProfileParametersStruct.TargetRadius))
				{
					//TODO STOP SAFETY
				}
				else if(cClaProfileVarStruct.AbsoluteEncoderPosition <= (sAbsoluteEncoderStruct.LowSoftwareLimit + X_sCommonCpuProfileParametersStruct.TargetRadius))
				{
					//TODO STOP SAFETY
				}

			}
	}


 	//Y AXIS LOGIC
 	if(MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.PARAMETERS_CHANGE_REQUEST == 1 && ClaCpuStatusRegs.Y_MFLAGS_CLASTATUS.bit.Y_PARAMETERS_CHANGED == 1)
 	{
 		MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.PARAMETERS_CHANGE_REQUEST = 0;
 	}
 	if(MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.COMMUTATION == 1)
	{
		IncEncPos = 0;

		//CommutCheck(MotorFalgsRegsArray,&YCommutState);
		switch(YCommutState)
		{
			case 1:
				MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.RESET_COMMUT = 1;
				YCommutState = 2;
				break;

			case  2:
				if(ClaCpuStatusRegs.Y_MFLAGS_CLASTATUS.bit.Y_COMMUT_IN_PROCESS == 0 && ClaCpuStatusRegs.Y_MFLAGS_CLASTATUS.bit.Y_COMMUT_DONE == 0)
				{
					MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.RESET_COMMUT = 0;
					YCommutState = 3;
				}
				break;

			case 3:
				//CalcCommutCommand
				CalcCommutCommands();
				CPU_TABLE_VALUE_A = ((float)YBRLS_PWM)*CPU_TABLE_VALUE_A;
				CPU_Brshl_Command_A = (int16_t)CPU_TABLE_VALUE_A;
				CPU_TABLE_VALUE_B = ((float)YBRLS_PWM)*CPU_TABLE_VALUE_B;
				CPU_Brshl_Command_B = (int16_t)CPU_TABLE_VALUE_B;
				CPU_TABLE_VALUE_C = ((float)YBRLS_PWM)*CPU_TABLE_VALUE_C;
				CPU_Brshl_Command_C = (int16_t)CPU_TABLE_VALUE_C;

				//Cla1ForceTask4();
				YCommutState = 4;
				break;

			case 4:
					EPwm10Regs.CMPA.bit.CMPA = PWM_DC + CPU_Brshl_Command_A;
					EPwm11Regs.CMPA.bit.CMPA = PWM_DC + CPU_Brshl_Command_B;
					EPwm12Regs.CMPA.bit.CMPA = PWM_DC + CPU_Brshl_Command_C;
					yCPUcounter = 0;
					YCommutState = 5;
					break;

			case 5:
				yCPUcounter++;
				if(yCPUcounter >= 4000)
				{
					YCommutState = 6;
				}
				break;

			case 6:
				EQep3Regs.QPOSINIT = 0;
				EQep3Regs.QPOSCNT = 0;
				Cla1ForceTask4();
				YCommutState = 7;
				break;

			case 7:
				if(ClaCpuStatusRegs.Y_MFLAGS_CLASTATUS.bit.Y_COMMUT_IN_PROCESS == 0 && ClaCpuStatusRegs.Y_MFLAGS_CLASTATUS.bit.Y_COMMUT_DONE == 1)
				{
					MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.COMMUTATION = 0;
					YBRLS_PWM = 0;
					//Y_AxisServoOff();
					Y_AxisServoOn();
					YCommutState = 0;
					yCPUcounter = 0;
					TxArbitrator.YActionCompleteFlag = 1;
				}
				break;
		}


	}
	else if(MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.CALIBRATION_REQUEST == 1 && ClaCpuStatusRegs.Y_MFLAGS_CLASTATUS.bit.Y_COMMUT_DONE == 1)
	{

		// TODO
		switch(Y_sCommonCpuProfileParametersStruct.CalibrationSwitch)
		{
		case 1:
			Y_AxisServoOff();
			Y_sCommonCpuProfileParametersStruct.CalibrationCounter = 0;
			Y_sCommonCpuProfileParametersStruct.CalibrationSwitch = 2;
			break;

		case 2:
			Y_sCommonCpuProfileParametersStruct.CalibrationCounter++;
			if(Y_sCommonCpuProfileParametersStruct.CalibrationCounter >= 1000)
			{
				Y_AxisServoOn();
				Y_sCommonCpuProfileParametersStruct.CalibrationCounter = 0;
				Y_sCommonCpuProfileParametersStruct.CalibrationSwitch = 3;
			}
			break;

		case 3:
			Y_sCommonCpuProfileParametersStruct.CalibrationCounter++;
			if(Y_sCommonCpuProfileParametersStruct.CalibrationCounter >= 1000)
			{
				Y_sCommonCpuProfileParametersStruct.CalibrationCounter = 0;
				Y_sCommonCpuProfileParametersStruct.CalibrationSwitch = 4;
			}
			break;


		case 4:

			if(YpAdjusterParametersStruct.AdjusterProfileVelocity < YDEFAULT_MAX_MOVE_VELOCITY_IN_COUNTS_PER_SECOND)
			{
				YpAdjusterParametersStruct.AdjusterProfileVelocity = YDEFAULT_MAX_MOVE_VELOCITY_IN_COUNTS_PER_SECOND;
			}
			Y_pToMoveVariablesStruct.ProfileMaxVelocity = YpAdjusterParametersStruct.AdjusterProfileVelocity;
			Y_pToMoveVariablesStruct.ProfileAcceleration = YpAdjusterParametersStruct.AdjusterProfileVelocity*10.0;//pAdjusterParametersStruct.AdjusterProfileAcc;
			Y_pToMoveVariablesStruct.ProfileJerk = Y_pToMoveVariablesStruct.ProfileAcceleration*10.0; //pAdjusterParametersStruct.AdjusterProfileJerk;
			Y_pToMoveVariablesStruct.StartTargetPoint = sControlVarStruct.YMotorPosition;
			Y_pToMoveVariablesStruct.FinalTargetPoint = Y_pToMoveVariablesStruct.StartTargetPoint - YMAXIMUM_PERMISSIBLE_TRAVEL_IN_COUNTS;
			//Y_sCommonCpuProfileParametersStruct.MaxCalibrationTime = (uint32_t)((double)YMAXIMUM_PERMISSIBLE_TRAVEL_IN_COUNTS/Y_pToMoveVariablesStruct.ProfileMaxVelocity);

			Y_sCommonCpuProfileParametersStruct.Sn = (float)sControlVarStruct.YMotorPosition;
			Y_pToMoveVariablesStruct.DesiredPosition = Y_pToMoveVariablesStruct.FinalTargetPoint;
			Y_sCommonCpuProfileParametersStruct.TargetRadiusSettlingTime = 100;
			Y_sCommonCpuProfileParametersStruct.TargetRadiusCounter = 0;
			Y_sCommonCpuProfileParametersStruct.TargetRadius = 20;
			X_sCommonCpuProfileParametersStruct.CheckSettling = 1;
			Y_pToMoveVariablesStruct.Vmax = Y_pToMoveVariablesStruct.ProfileMaxVelocity;
			Y_pToMoveVariablesStruct.AccelerationTime = Y_pToMoveVariablesStruct.Vmax/Y_pToMoveVariablesStruct.ProfileAcceleration;
			YcCpuToClaProfileVarStruct.AT =  (uint32_t)(Y_pToMoveVariablesStruct.AccelerationTime*1000.0);
			Y_pToMoveVariablesStruct.JerkFactor = Y_pToMoveVariablesStruct.ProfileAcceleration/Y_pToMoveVariablesStruct.ProfileJerk;
			Y_pToMoveVariablesStruct.JerkTime = Y_pToMoveVariablesStruct.JerkFactor*(double)YcCpuToClaProfileVarStruct.AT;
			YcCpuToClaProfileVarStruct.N1 = (uint32_t)(Y_pToMoveVariablesStruct.JerkTime);
			YcCpuToClaProfileVarStruct.N2 = YcCpuToClaProfileVarStruct.AT;
			YcCpuToClaProfileVarStruct.N3 = YcCpuToClaProfileVarStruct.N2 + YcCpuToClaProfileVarStruct.N1;


			Y_pToMoveVariablesStruct.JerkTime = (double)(YcCpuToClaProfileVarStruct.N1);
			Y_pToMoveVariablesStruct.ProfileAcceleration = -Y_pToMoveVariablesStruct.ProfileMaxVelocity/YcCpuToClaProfileVarStruct.AT;
			Y_pToMoveVariablesStruct.ProfileJerk = Y_pToMoveVariablesStruct.ProfileAcceleration/YcCpuToClaProfileVarStruct.N1;
			Y_sCommonCpuProfileParametersStruct.Jn = Y_pToMoveVariablesStruct.ProfileJerk;
			Y_sCommonCpuProfileParametersStruct.Vn = Y_pToMoveVariablesStruct.Vmax*0.001; //Counts/msec
			Y_sCommonCpuProfileParametersStruct.Vn = -Y_sCommonCpuProfileParametersStruct.Vn;


			//ProfileParsPrepareToMove(pToMoveVarStructsArray[Y_Axis], ProfileVarStructsArray[YCpuToClaProfileVars],CommonCpuProfileParametersStructsArray[Y_Axis]);
			Y_sCommonCpuProfileParametersStruct.MaxCalibrationTime = (uint32_t)((double)YMAXIMUM_PERMISSIBLE_TRAVEL_IN_COUNTS/Y_pToMoveVariablesStruct.Vmax);
			Y_sCommonCpuProfileParametersStruct.MaxCalibrationTime = Y_sCommonCpuProfileParametersStruct.MaxCalibrationTime*1000;

			Y_sCommonCpuProfileParametersStruct.CalibrationCounter = 0;
			Y_sCommonCpuProfileParametersStruct.CalibrationSwitch = 5;
			//Y_sCommonCpuProfileParametersStruct.ProfilerSwitch = 1;
			Y_sCommonCpuProfileParametersStruct.ProfileCounter = 0;
			Y_sCommonCpuProfileParametersStruct.OvercurrentFaultFlag = false;

			break;

		case 5:

			Y_sCommonCpuProfileParametersStruct.CalibrationCounter++;
			Y_sCommonCpuProfileParametersStruct.Sn = Y_sCommonCpuProfileParametersStruct.Sn + Y_sCommonCpuProfileParametersStruct.Vn;
			Y_sCommonCpuProfileParametersStruct.NextTargetPosition = (int32_t)Y_sCommonCpuProfileParametersStruct.Sn;
			Y_pToMoveVariablesStruct.DesiredPosition = Y_sCommonCpuProfileParametersStruct.NextTargetPosition;
			Y_sCpuToCla1ProfileCommandsStruct.PositionCommand = Y_pToMoveVariablesStruct.DesiredPosition;
			//Y_sCpuToCla1ProfileCommandsStruct.PositionCommand = ProfileCalc(pToMoveVarStructsArray[Y_Axis], ProfileVarStructsArray[YCpuToClaProfileVars],CommonCpuProfileParametersStructsArray[Y_Axis], Y_Axis);
			if((Y_sCommonCpuProfileParametersStruct.OvercurrentFaultFlag == true) || (Y_sCommonCpuProfileParametersStruct.CalibrationCounter >= Y_sCommonCpuProfileParametersStruct.MaxCalibrationTime))
			{
				Y_sCommonCpuProfileParametersStruct.ProfilerSwitch = 0;
				Y_sCommonCpuProfileParametersStruct.Sn = (float)sControlVarStruct.YMotorPosition;
				Y_sCommonCpuProfileParametersStruct.OvercurrentFaultFlag = false;
				//asm(" ESTOP0");
				Y_sCommonCpuProfileParametersStruct.CalibrationSwitch = 6;
				Y_sCommonCpuProfileParametersStruct.CalibrationCounter = 0;
				//Y_AxisServoOff();

			}

			break;

		case 6:

			Y_sMotorControlVariablesStruct.PreviousIncrementalEncoderData = sControlVarStruct.YMotorPosition;
			Y_pToMoveVariablesStruct.ProfileMaxVelocity = 65536.0;
			Y_pToMoveVariablesStruct.ProfileAcceleration = YpAdjusterParametersStruct.AdjusterProfileVelocity*10.0;//pAdjusterParametersStruct.AdjusterProfileAcc;
			Y_pToMoveVariablesStruct.ProfileJerk = Y_pToMoveVariablesStruct.ProfileAcceleration*10.0; //pAdjusterParametersStruct.AdjusterProfileJerk;
			Y_pToMoveVariablesStruct.StartTargetPoint = sControlVarStruct.YMotorPosition;
			//X_pToMoveVariablesStruct.StartTargetPoint = TempPar;
			Y_sCommonCpuProfileParametersStruct.Sn = (float)sControlVarStruct.YMotorPosition;
			Y_pToMoveVariablesStruct.FinalTargetPoint = Y_pToMoveVariablesStruct.StartTargetPoint + 196000;
			Y_pToMoveVariablesStruct.DesiredPosition = Y_pToMoveVariablesStruct.FinalTargetPoint;
			Y_sCommonCpuProfileParametersStruct.TargetRadiusSettlingTime = 100;
			Y_sCommonCpuProfileParametersStruct.TargetRadiusCounter = 0;
			Y_sCommonCpuProfileParametersStruct.TargetRadius = 20;
			Y_sCommonCpuProfileParametersStruct.CheckSettling = 1;
			MotorOnFlag = 1;
			ProfileParsPrepareToMove(pToMoveVarStructsArray[Y_Axis], ProfileVarStructsArray[YCpuToClaProfileVars], CommonCpuProfileParametersStructsArray[Y_Axis]);
				//sControlVarStruct.YMotorPosition
			Y_sCommonCpuProfileParametersStruct.CalibrationSwitch = 7;
			break;

		case 7:
			Y_sCommonCpuProfileParametersStruct.CalibrationCounter++;

			if (Y_sCommonCpuProfileParametersStruct.CalibrationCounter >= 1000) {
				MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.CALIBRATION_REQUEST = 0;
				Y_sCommonCpuProfileParametersStruct.CalibrationCounter = 0;
				}
			break;

		}

	}
	else if(MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.POSITION_ADJUSTER == 1)
	{
		//PositionAdjusterExec();	TODO
		switch(YpAdjusterParametersStruct.AdjusterSwitch)
		{
		case 1:
			Y_pToMoveVariablesStruct.ProfileMaxVelocity = YpAdjusterParametersStruct.AdjusterProfileVelocity;
			Y_pToMoveVariablesStruct.ProfileAcceleration = YpAdjusterParametersStruct.AdjusterProfileAcc;
			Y_pToMoveVariablesStruct.ProfileJerk = YpAdjusterParametersStruct.AdjusterProfileJerk;
			Y_pToMoveVariablesStruct.FinalTargetPoint = YpAdjusterParametersStruct.AdjusterProfileTarget;
			Y_pToMoveVariablesStruct.StartTargetPoint = sControlVarStruct.YMotorPosition;
			ProfileParsPrepareToMove(pToMoveVarStructsArray[Y_Axis], ProfileVarStructsArray[YCpuToClaAdjusterFirstMove],CommonCpuProfileParametersStructsArray[Y_Axis]);
			Y_pToMoveVariablesStruct.FinalTargetPoint = Y_pToMoveVariablesStruct.StartTargetPoint;
			Y_pToMoveVariablesStruct.StartTargetPoint = YpAdjusterParametersStruct.AdjusterProfileTarget;
			ProfileParsPrepareToMove(pToMoveVarStructsArray[Y_Axis], ProfileVarStructsArray[YCpuToClaAdjusterSecondMove], CommonCpuProfileParametersStructsArray[Y_Axis]);
			Y_pToMoveVariablesStruct.StartTargetPoint = Y_pToMoveVariablesStruct.FinalTargetPoint;
			Y_pToMoveVariablesStruct.FinalTargetPoint = YpAdjusterParametersStruct.AdjusterProfileTarget;
			Y_sCommonCpuProfileParametersStruct.Sn = sControlVarStruct.YPositionFeedback;
			Y_sCommonCpuProfileParametersStruct.TargetRadiusSettlingTime = 100;
			Y_sCommonCpuProfileParametersStruct.TargetRadiusCounter = 0;
			Y_sCommonCpuProfileParametersStruct.TargetRadius = 20;
			YpAdjusterParametersStruct.DwellCounter = 0;
			YpAdjusterParametersStruct.AdjusterSwitch = 2;
			break;

		case 2:

			YpAdjusterParametersStruct.DwellCounter++;
			if((YpAdjusterParametersStruct.DwellCounter >= YpAdjusterParametersStruct.DwellTime)&&(YpAdjusterParametersStruct.AdjusterRequestFlag == 1))
			{

				Y_pToMoveVariablesStruct.ProfileMaxVelocity = YpAdjusterParametersStruct.AdjusterProfileVelocity;
				Y_pToMoveVariablesStruct.ProfileAcceleration = YpAdjusterParametersStruct.AdjusterProfileVelocity*10.0;//pAdjusterParametersStruct.AdjusterProfileAcc;
				Y_pToMoveVariablesStruct.ProfileJerk = Y_pToMoveVariablesStruct.ProfileAcceleration*10.0; //pAdjusterParametersStruct.AdjusterProfileJerk;
				Y_pToMoveVariablesStruct.StartTargetPoint = sControlVarStruct.YMotorPosition;
				//X_pToMoveVariablesStruct.StartTargetPoint = TempPar;
				Y_sCommonCpuProfileParametersStruct.Sn = (float)sControlVarStruct.YMotorPosition;
				Y_pToMoveVariablesStruct.FinalTargetPoint = YpAdjusterParametersStruct.AdjusterProfileTarget;
				Y_pToMoveVariablesStruct.DesiredPosition = Y_pToMoveVariablesStruct.FinalTargetPoint;
				Y_sCommonCpuProfileParametersStruct.TargetRadiusSettlingTime = 100;
				Y_sCommonCpuProfileParametersStruct.TargetRadiusCounter = 0;
				Y_sCommonCpuProfileParametersStruct.TargetRadius = 20;
				Y_sCommonCpuProfileParametersStruct.CheckSettling = 1;
				MotorOnFlag = 1;
				ProfileParsPrepareToMove(pToMoveVarStructsArray[Y_Axis], ProfileVarStructsArray[YCpuToClaProfileVars], CommonCpuProfileParametersStructsArray[Y_Axis]);
				YpAdjusterParametersStruct.AdjusterSwitch = 3;
			}
			else if(YpAdjusterParametersStruct.AdjusterRequestFlag == 0)
			{
					MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.POSITION_ADJUSTER = 0;
					YpAdjusterParametersStruct.AdjusterSwitch = 0;
					YpAdjusterParametersStruct.DwellCounter = 0;
					Y_pToMoveVariablesStruct.ProfileMaxVelocity = 0;
					Y_pToMoveVariablesStruct.ProfileAcceleration = 0;
					Y_pToMoveVariablesStruct.ProfileJerk = 0;

			}

			break;

		case 3:
			Y_sCpuToCla1ProfileCommandsStruct.PositionCommand = ProfileCalc(pToMoveVarStructsArray[Y_Axis], ProfileVarStructsArray[YCpuToClaProfileVars],CommonCpuProfileParametersStructsArray[Y_Axis], Y_Axis);
			if((sControlVarStruct.YMotorPosition >= (Y_pToMoveVariablesStruct.DesiredPosition - (int32_t)Y_sCommonCpuProfileParametersStruct.TargetRadius))&&(sControlVarStruct.YMotorPosition <= (Y_pToMoveVariablesStruct.DesiredPosition + (int32_t)Y_sCommonCpuProfileParametersStruct.TargetRadius)))
			{
				if(Y_sCommonCpuProfileParametersStruct.ProfileEnd == 1)
				{
					Y_sCommonCpuProfileParametersStruct.TargetRadiusCounter++;
					if(Y_sCommonCpuProfileParametersStruct.TargetRadiusCounter >= Y_sCommonCpuProfileParametersStruct.TargetRadiusSettlingTime)
					{
						NextDesiredPosition = Y_pToMoveVariablesStruct.FinalTargetPoint;
						Y_pToMoveVariablesStruct.ProfileMaxVelocity = YpAdjusterParametersStruct.AdjusterProfileVelocity;
						Y_pToMoveVariablesStruct.ProfileAcceleration = YpAdjusterParametersStruct.AdjusterProfileAcc;
						Y_pToMoveVariablesStruct.ProfileJerk = YpAdjusterParametersStruct.AdjusterProfileJerk;
						//ProfileParsPrepareToMove(&NextDesiredPosition);
						Y_sCommonCpuProfileParametersStruct.Sn = sControlVarStruct.YPositionFeedback;
						YpAdjusterParametersStruct.DwellCounter = 0;
						YpAdjusterParametersStruct.AdjusterSwitch = 4;
						Y_sCommonCpuProfileParametersStruct.TargetRadiusCounter = 0;
					}
				}
			}
			break;


		case 4:
			YpAdjusterParametersStruct.DwellCounter++;
			if((YpAdjusterParametersStruct.DwellCounter >= YpAdjusterParametersStruct.DwellTime)&&(YpAdjusterParametersStruct.AdjusterRequestFlag == 1))
			{
				Y_pToMoveVariablesStruct.ProfileMaxVelocity = YpAdjusterParametersStruct.AdjusterProfileVelocity;
				Y_pToMoveVariablesStruct.ProfileAcceleration = YpAdjusterParametersStruct.AdjusterProfileVelocity*10.0;//pAdjusterParametersStruct.AdjusterProfileAcc;
				Y_pToMoveVariablesStruct.ProfileJerk = Y_pToMoveVariablesStruct.ProfileAcceleration*10.0; //pAdjusterParametersStruct.AdjusterProfileJerk;
				Y_pToMoveVariablesStruct.FinalTargetPoint = Y_pToMoveVariablesStruct.StartTargetPoint;
				Y_pToMoveVariablesStruct.StartTargetPoint = sControlVarStruct.YMotorPosition;
				//X_pToMoveVariablesStruct.StartTargetPoint = TempPar;
				Y_sCommonCpuProfileParametersStruct.Sn = (float)sControlVarStruct.YMotorPosition;
				Y_pToMoveVariablesStruct.DesiredPosition = Y_pToMoveVariablesStruct.FinalTargetPoint;
				Y_sCommonCpuProfileParametersStruct.TargetRadiusSettlingTime = 100;
				Y_sCommonCpuProfileParametersStruct.TargetRadiusCounter = 0;
				Y_sCommonCpuProfileParametersStruct.TargetRadius = 20;
				Y_sCommonCpuProfileParametersStruct.CheckSettling = 1;
				MotorOnFlag = 1;
				ProfileParsPrepareToMove(pToMoveVarStructsArray[Y_Axis], ProfileVarStructsArray[YCpuToClaProfileVars], CommonCpuProfileParametersStructsArray[Y_Axis]);
				YpAdjusterParametersStruct.AdjusterSwitch = 5;

			}
			else if(YpAdjusterParametersStruct.AdjusterRequestFlag == 0)
			{
					MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.POSITION_ADJUSTER = 0;
					YpAdjusterParametersStruct.AdjusterSwitch = 0;
					YpAdjusterParametersStruct.DwellCounter = 0;
					Y_pToMoveVariablesStruct.ProfileMaxVelocity = 0;
					Y_pToMoveVariablesStruct.ProfileAcceleration = 0;
					Y_pToMoveVariablesStruct.ProfileJerk = 0;

			}
			break;



		case 5:

			Y_sCpuToCla1ProfileCommandsStruct.PositionCommand = ProfileCalc(pToMoveVarStructsArray[Y_Axis], ProfileVarStructsArray[YCpuToClaProfileVars],CommonCpuProfileParametersStructsArray[Y_Axis], Y_Axis);
			if((sControlVarStruct.YMotorPosition >= (Y_pToMoveVariablesStruct.DesiredPosition - (int32_t)Y_sCommonCpuProfileParametersStruct.TargetRadius))&&(sControlVarStruct.XMotorPosition <= (Y_pToMoveVariablesStruct.DesiredPosition + (int32_t)Y_sCommonCpuProfileParametersStruct.TargetRadius)))
			{
				if(Y_sCommonCpuProfileParametersStruct.ProfileEnd == 1)
				{
					Y_sCommonCpuProfileParametersStruct.TargetRadiusCounter++;
					if(Y_sCommonCpuProfileParametersStruct.TargetRadiusCounter >= Y_sCommonCpuProfileParametersStruct.TargetRadiusSettlingTime)
					{
						NextDesiredPosition = Y_pToMoveVariablesStruct.FinalTargetPoint;
						Y_pToMoveVariablesStruct.ProfileMaxVelocity = YpAdjusterParametersStruct.AdjusterProfileVelocity;
						Y_pToMoveVariablesStruct.ProfileAcceleration = YpAdjusterParametersStruct.AdjusterProfileAcc;
						Y_pToMoveVariablesStruct.ProfileJerk = YpAdjusterParametersStruct.AdjusterProfileJerk;
						//ProfileParsPrepareToMove(&NextDesiredPosition);
						Y_sCommonCpuProfileParametersStruct.Sn = sControlVarStruct.YPositionFeedback;
						YpAdjusterParametersStruct.DwellCounter = 0;
						YpAdjusterParametersStruct.AdjusterSwitch = 2;
						Y_sCommonCpuProfileParametersStruct.TargetRadiusCounter = 0;
					}
				}
			}
			break;
		}
	}
	else if(MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.VELOCITY_ADJUSTER == 1)
	{
		//VelocityAdjusterExec(); TODO
		switch(YpAdjusterParametersStruct.AdjusterSwitch)
		{
		case 1:
			Y_sCpuToCla1ProfileCommandsStruct.VelocityFeedforwardCommand = (int32_t)YpAdjusterParametersStruct.AdjusterProfileVelocity;
			YpAdjusterParametersStruct.DwellCounter = 0;
			YpAdjusterParametersStruct.AdjusterSwitch = 2;
			break;

		case 2:
				YpAdjusterParametersStruct.DwellCounter++;
				if((YpAdjusterParametersStruct.DwellCounter >= YpAdjusterParametersStruct.DwellTime)&&(YpAdjusterParametersStruct.AdjusterRequestFlag == 1))
				{
					Y_sCpuToCla1ProfileCommandsStruct.VelocityFeedforwardCommand = -(int32_t)YpAdjusterParametersStruct.AdjusterProfileVelocity;
					YpAdjusterParametersStruct.AdjusterSwitch = 3;
					YpAdjusterParametersStruct.DwellCounter = 0;
				}
				else if(YpAdjusterParametersStruct.AdjusterRequestFlag == 0)
				{
					MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.VELOCITY_ADJUSTER = 0;
					YpAdjusterParametersStruct.AdjusterSwitch = 0;
					Y_pToMoveVariablesStruct.ProfileMaxVelocity = 0;
					Y_pToMoveVariablesStruct.ProfileAcceleration = 0;
					Y_pToMoveVariablesStruct.ProfileJerk = 0;
					YpAdjusterParametersStruct.AdjusterProfileVelocity = 0;
					Y_sCommonCpuProfileParametersStruct.ProfilerSwitch = 0;
					Y_sCpuToCla1ProfileCommandsStruct.VelocityFeedforwardCommand = 0;
					YpAdjusterParametersStruct.DwellCounter = 0;
					Y_AxisServoOff();
				}

			//iVelocityCommand = (int16_t)(sControlVarStruct.VelocityCommand*0.4096); //2048/5000
			//DacbRegs.DACVALS.bit.DACVALS = 2048;
				break;

		case 3:
			YpAdjusterParametersStruct.DwellCounter++;
			if((YpAdjusterParametersStruct.DwellCounter >= YpAdjusterParametersStruct.DwellTime)&&(YpAdjusterParametersStruct.AdjusterRequestFlag == 1))
			{
				Y_sCpuToCla1ProfileCommandsStruct.VelocityFeedforwardCommand = (int32_t)YpAdjusterParametersStruct.AdjusterProfileVelocity;
				YpAdjusterParametersStruct.DwellCounter = 0;
				YpAdjusterParametersStruct.AdjusterSwitch = 2;
			}
			else if(YpAdjusterParametersStruct.AdjusterRequestFlag == 0)
			{

				MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.VELOCITY_ADJUSTER = 0;
				YpAdjusterParametersStruct.AdjusterSwitch = 0;
				Y_pToMoveVariablesStruct.ProfileMaxVelocity = 0;
				Y_pToMoveVariablesStruct.ProfileAcceleration = 0;
				Y_pToMoveVariablesStruct.ProfileJerk = 0;
				YpAdjusterParametersStruct.AdjusterProfileVelocity = 0;
				Y_sCommonCpuProfileParametersStruct.ProfilerSwitch = 0;
				Y_sCpuToCla1ProfileCommandsStruct.VelocityFeedforwardCommand = 0;
				YpAdjusterParametersStruct.DwellCounter = 0;
				Y_AxisServoOff();
			}
			break;

		}
	}
	else if(MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.CURRENT_ADJUSTER == 1)
	{
		//CurrentAdjusterExec(); // TODO
		switch(YpAdjusterParametersStruct.AdjusterSwitch)
		{
		case 1:
			Y_sCpuToCla1ProfileCommandsStruct.CurrentAdjusterCommand = YpAdjusterParametersStruct.AdjusterCurrentCommand;
			YpAdjusterParametersStruct.DwellCounter = 0;
			YpAdjusterParametersStruct.AdjusterSwitch = 2;
			break;
		case 2:
			//sCpuToCla1ProfileCommandsStruct.CurrentAdjusterCommand = pAdjusterParametersStruct.AdjusterCurrentCommand;
			YpAdjusterParametersStruct.DwellCounter++;
			if((YpAdjusterParametersStruct.DwellCounter >= YpAdjusterParametersStruct.DwellTime)&&(YpAdjusterParametersStruct.AdjusterRequestFlag == 1))
			{
				Y_sCpuToCla1ProfileCommandsStruct.CurrentAdjusterCommand = -YpAdjusterParametersStruct.AdjusterCurrentCommand;
				YpAdjusterParametersStruct.DwellCounter = 0;
				YpAdjusterParametersStruct.AdjusterSwitch = 3;
			}
			else if(YpAdjusterParametersStruct.AdjusterRequestFlag == 0)
			{
				MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.CURRENT_ADJUSTER = 0;
				YpAdjusterParametersStruct.AdjusterSwitch = 0;
				Y_pToMoveVariablesStruct.ProfileMaxVelocity = 0;
				Y_pToMoveVariablesStruct.ProfileAcceleration = 0;
				Y_pToMoveVariablesStruct.ProfileJerk = 0;
				YpAdjusterParametersStruct.AdjusterCurrentCommand = 0;
				Y_sCpuToCla1ProfileCommandsStruct.CurrentAdjusterCommand = 0;
				Y_sCommonCpuProfileParametersStruct.ProfilerSwitch = 0;
				EPwm10Regs.CMPA.bit.CMPA = 1250;
				EPwm11Regs.CMPA.bit.CMPA = 1250;
				EPwm12Regs.CMPA.bit.CMPA = 1250;
				DacbRegs.DACVALS.bit.DACVALS = 2048;
				DaccRegs.DACVALS.bit.DACVALS = 2048;
				DacaRegs.DACVALS.bit.DACVALS = 2048;
				MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.ENABLE = 0;
			}
			break;

		case 3:
			YpAdjusterParametersStruct.DwellCounter++;
			if((YpAdjusterParametersStruct.DwellCounter >= YpAdjusterParametersStruct.DwellTime)&&(YpAdjusterParametersStruct.AdjusterRequestFlag == 1))
			{
				Y_sCpuToCla1ProfileCommandsStruct.CurrentAdjusterCommand = YpAdjusterParametersStruct.AdjusterCurrentCommand;
				YpAdjusterParametersStruct.DwellCounter = 0;
				YpAdjusterParametersStruct.AdjusterSwitch = 2;
			}
			else if(YpAdjusterParametersStruct.AdjusterRequestFlag == 0)
			{
				MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.CURRENT_ADJUSTER = 0;
				YpAdjusterParametersStruct.AdjusterSwitch = 0;
				Y_pToMoveVariablesStruct.ProfileMaxVelocity = 0;
				Y_pToMoveVariablesStruct.ProfileAcceleration = 0;
				Y_pToMoveVariablesStruct.ProfileJerk = 0;
				YpAdjusterParametersStruct.AdjusterCurrentCommand = 0;
				Y_sCpuToCla1ProfileCommandsStruct.CurrentAdjusterCommand = 0;
				Y_sCommonCpuProfileParametersStruct.ProfilerSwitch = 0;
				EPwm10Regs.CMPA.bit.CMPA = 1250;
				EPwm11Regs.CMPA.bit.CMPA = 1250;
				EPwm12Regs.CMPA.bit.CMPA = 1250;
				DacbRegs.DACVALS.bit.DACVALS = 2048;
				DaccRegs.DACVALS.bit.DACVALS = 2048;
				DacaRegs.DACVALS.bit.DACVALS = 2048;
				MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.ENABLE = 0;
			}
			break;

		}
	}
	else if(MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.SERVO_ANALYSYS == 1 && ClaCpuStatusRegs.Y_MFLAGS_CLASTATUS.bit.Y_COMMUT_DONE == 1 && ClaCpuStatusRegs.Y_MFLAGS_CLASTATUS.bit.Y_ENABLED == 1)
	{
		if(YCycleCounter < 10000/YFrequency)
		{
			YCycleCounter++;
			Y_sCpuToCla1ProfileCommandsStruct.PositionCommand = pToMoveVarStructsArray[Y_Axis]->StartServo + ((int32_t)(((float)40204) * sin(2 * Pi * YFrequency *(YCycleCounter * 0.001))));
			DacbRegs.DACVALS.bit.DACVALS = 2048 + 0.00002487*2048*sControlVarStruct.YMotorPosition;
			//X_pToMoveVariablesStruct.StartServo
			//sCpuToCla1ProfileCommandsStruct.PositionCommand = IQ16sinPU((float)Frequency/(CycleCounter * 0.001));
			//sCpuToCla1ProfileCommandsStruct.PositionCommand = 0x13880000*sCpuToCla1ProfileCommandsStruct.PositionCommand;
			//sCpuToCla1ProfileCommandsStruct.PositionCommand >>16;
			TxArbitrator.SendYServoAnalyzerMeasurementsFlag = 1;
		}
		else
		{
			YCycleCounter = 0 ;
			YFrequency++;
		}
		if(YFrequency > 15)
		{
			MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.SERVO_ANALYSYS = 0;
			YCycleCounter = 0 ;
			YFrequency=1;
			Y_AxisServoOff();
			TxArbitrator.YActionCompleteFlag = 1;
		}
	}
	else
	{
			Y_sCpuToCla1ProfileCommandsStruct.PositionCommand = ProfileCalc(pToMoveVarStructsArray[Y_Axis], ProfileVarStructsArray[YCpuToClaProfileVars], CommonCpuProfileParametersStructsArray[Y_Axis], Y_Axis);
			if(med_ctr.handle_mode != ACTIVE_MODE)
			{
				Y_sCpuToCla1ProfileCommandsStruct.VelocityFeedforwardCommand = (int32_t)((0.04)*Y_sCommonCpuProfileParametersStruct.Vn);
				Y_sCpuToCla1ProfileCommandsStruct.AccelerationFeedforwardCommand = (int32_t)((0.0015)*CommonCpuProfileParametersStructsArray[Y_Axis]->An);
			}
			else
			{
				Y_sCpuToCla1ProfileCommandsStruct.VelocityFeedforwardCommand = (int32_t)(Y_sCommonCpuProfileParametersStruct.Vn);
				Y_sCpuToCla1ProfileCommandsStruct.AccelerationFeedforwardCommand = 0;
			}
			//if((cClaProfileVarStruct.AbsoluteEncoderPosition >= (X_pToMoveVariablesStruct.DesiredPosition - X_sCommonCpuProfileParametersStruct.TargetRadius))&&(cClaProfileVarStruct.AbsoluteEncoderPosition <= (X_pToMoveVariablesStruct.DesiredPosition + X_sCommonCpuProfileParametersStruct.TargetRadius)))
			if((sControlVarStruct.YMotorPosition >= (Y_pToMoveVariablesStruct.DesiredPosition - Y_sCommonCpuProfileParametersStruct.TargetRadius))&&(sControlVarStruct.YMotorPosition <= (Y_pToMoveVariablesStruct.DesiredPosition + Y_sCommonCpuProfileParametersStruct.TargetRadius)))
			{
				if((Y_sCommonCpuProfileParametersStruct.ProfileEnd == 1)&&(MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.POSITION_ADJUSTER == 0)&&(Y_sCommonCpuProfileParametersStruct.ProfilerSwitch != 0))     //(Y_sCommonCpuProfileParametersStruct.CheckSettling == 1))
				{
					Y_sCommonCpuProfileParametersStruct.TargetRadiusCounter++;
					if(Y_sCommonCpuProfileParametersStruct.TargetRadiusCounter >= Y_sCommonCpuProfileParametersStruct.TargetRadiusSettlingTime)
					{
						YpAdjusterParametersStruct.AdjusterSwitch = 0;
						YpAdjusterParametersStruct.DwellCounter = 0;
						Y_pToMoveVariablesStruct.ProfileMaxVelocity = 0;
						Y_pToMoveVariablesStruct.ProfileAcceleration = 0;
						Y_pToMoveVariablesStruct.ProfileJerk = 0;
						Y_sCommonCpuProfileParametersStruct.ProfilerSwitch = 0;
						Y_sCommonCpuProfileParametersStruct.TargetRadiusCounter = 0;
						Y_sCommonCpuProfileParametersStruct.CheckSettling = 0;
						TxArbitrator.YActionCompleteFlag = 1;
					}
				}
			}
			if(MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.ENABLE == 1)
			{
				if(cClaProfileVarStruct.AbsoluteEncoderPosition >= (sAbsoluteEncoderStruct.HighSoftwareLimit - Y_sCommonCpuProfileParametersStruct.TargetRadius))
				{
					//TODO STOP SAFETY
				}
				else if(cClaProfileVarStruct.AbsoluteEncoderPosition <= (sAbsoluteEncoderStruct.LowSoftwareLimit + Y_sCommonCpuProfileParametersStruct.TargetRadius))
				{
					//TODO STOP SAFETY
				}

			}
	}

	//CurrentHandle();TODO
	//sCommonCpuProfileParametersStruct.MotorRmsCurrent = sControlVarStruct.RMSCurrent;
	//SafetyHandles();
	X_sCommonCpuProfileParametersStruct.XAxisMotorRmsCurrent = CPU_RMS_CURRENT_COEFFICIENT*sControlVarStruct.XRMSCurrent;
	Y_sCommonCpuProfileParametersStruct.YAxisMotorRmsCurrent = sControlVarStruct.YRMSCurrent;
	Y_sCommonCpuProfileParametersStruct.YAxisMotorPeakCurrent = _IQsqrt(Y_sCommonCpuProfileParametersStruct.YAxisMotorRmsCurrent);
	Y_sCommonCpuProfileParametersStruct.YAxisMotorPeakCurrent = CPU_RMS_CURRENT_COEFFICIENT*Y_sCommonCpuProfileParametersStruct.YAxisMotorPeakCurrent;
	if(Y_sCommonCpuProfileParametersStruct.YAxisMotorPeakCurrent >= Y_sCommonCpuProfileParametersStruct.YAxisMotorMaxPermitCurrent)
	{
		//yMotorFlagsReg.MFLAGS.bit.OPEN_LOOP = 0;
		//YBRLS_PWM = 0;
		//SetOpenLoopPwm(0, &YBRLS_PWM);
		//Y_AxisServoOff();
		Y_sCommonCpuProfileParametersStruct.OvercurrentFaultFlag = true;
	}

	//Y_sCommonCpuProfileParametersStruct.YAxisMotorPeakCurrent = Y_sCommonCpuProfileParametersStruct.YAxisMotorPeakCurrent * YsDigitalToAnalogCPUSensorStruct.RelativeResolutionsCoefficient;
	if(YpAdjusterParametersStruct.AdjusterRequestFlag == 0)
	{
	DacbRegs.DACVALS.bit.DACVALS = (int16_t)(Y_sCommonCpuProfileParametersStruct.YAxisMotorPeakCurrent * YsDigitalToAnalogCPUSensorStruct.RelativeResolutionsCoefficient);
	}
	//sCommonCpuProfileParametersStruct.MotorPeakCurrent = sCommonCpuProfileParametersStruct.MotorPeakCurrent*CURRENT_SENSOR_MEASUREMENT_RATE;
	//sCommonCpuProfileParametersStruct.MotorPeakCurrent = sCommonCpuProfileParametersStruct.MotorPeakCurrent*0.00048828;
/*	if((sCommonCpuProfileParametersStruct.MotorPeakCurrent >= 6.0)||(sCommonCpuProfileParametersStruct.MotorPeakCurrent <= -6.0))
	{
		ErrCode = X_AxisServoOff();

	}
*/


}

#pragma CODE_SECTION(ProfileCalc, "ramfuncs")
int32_t ProfileCalc(volatile struct _P2MOVE_VAR *pToMoveStructArray, volatile struct CPU_PROFILE_VAR *CpuProfilesArray,volatile struct COMMON_CPU_PROFILE_VAR *CpuProfileParametersStructsArray, uint16_t AxisNumber)
{

	//CAN_DisableInterrupt();
		switch(CpuProfileParametersStructsArray->ProfilerSwitch)
		{

			case(1):
					CpuProfileParametersStructsArray->ProfileCounter++;
					CpuProfileParametersStructsArray->An = CpuProfileParametersStructsArray->An + CpuProfileParametersStructsArray->Jn; //CpuProfilesArray->ProfileJerk;
					CpuProfileParametersStructsArray->Vn = CpuProfileParametersStructsArray->Vn + CpuProfileParametersStructsArray->An;
					CpuProfileParametersStructsArray->Sn = CpuProfileParametersStructsArray->Sn + CpuProfileParametersStructsArray->Vn;
              	if(pToMoveStructArray->JerkTime==0)
              	{
              		CpuProfileParametersStructsArray->An = CpuProfilesArray->ProfileAcceleration;
              		CpuProfileParametersStructsArray->ProfileCounter = 0;
              		CpuProfileParametersStructsArray->ProfilerSwitch = 2;
              	}
 				else
 				{
 				 if(CpuProfileParametersStructsArray->ProfileCounter>= CpuProfilesArray->N1)
      			 {
 					CpuProfileParametersStructsArray->ProfilerSwitch = 2;
      			 }
 				}
      			 break;

			case(2):

					CpuProfileParametersStructsArray->ProfileCounter++;
					//cCpuToClaProfileVarStruct.An = cCpuToClaProfileVarStruct.An;
					CpuProfileParametersStructsArray->Vn = CpuProfileParametersStructsArray->Vn + CpuProfileParametersStructsArray->An;
					CpuProfileParametersStructsArray->Sn = CpuProfileParametersStructsArray->Sn + CpuProfileParametersStructsArray->Vn;
  				if(CpuProfileParametersStructsArray->ProfileCounter >= CpuProfilesArray->N2)
  				{
  					if(pToMoveStructArray->JerkTime != 0)
  					{
  						CpuProfileParametersStructsArray->ProfilerSwitch = 3;
  					}
  					else
  					{
  						CpuProfileParametersStructsArray->ProfilerSwitch = 4;
  					}
  				}

  				break;

			case(3):

					CpuProfileParametersStructsArray->ProfileCounter++;
					CpuProfileParametersStructsArray->An = CpuProfileParametersStructsArray->An - CpuProfileParametersStructsArray->Jn; //CpuProfilesArray->ProfileJerk;
					CpuProfileParametersStructsArray->Vn = CpuProfileParametersStructsArray->Vn + CpuProfileParametersStructsArray->An;
					CpuProfileParametersStructsArray->Sn = CpuProfileParametersStructsArray->Sn + CpuProfileParametersStructsArray->Vn;
  				if(CpuProfileParametersStructsArray->ProfileCounter >= CpuProfilesArray->N3)
  				{
  					CpuProfileParametersStructsArray->EndOfAccelerationProfilePosition = CpuProfileParametersStructsArray->Sn;
  					CpuProfileParametersStructsArray->Vn = CpuProfilesArray->ProfileVelocity;

   					if(CpuProfilesArray->TriangularProfileFlag == 0)
  					{
   						CpuProfileParametersStructsArray->ProfilerSwitch = 4;
  					}
  					else if(CpuProfilesArray->TriangularProfileFlag == 1)
  					{
  						CpuProfileParametersStructsArray->ProfilerSwitch = 5;
  					}
  				}
  				break;

			case(4):
					CpuProfileParametersStructsArray->ProfileCounter++;
				//cCpuToClaProfileVarStruct.An = cCpuToClaProfileVarStruct.An;
				//cCpuToClaProfileVarStruct.Vn = cCpuToClaProfileVarStruct.Vn;
					CpuProfileParametersStructsArray->Sn = CpuProfileParametersStructsArray->Sn + CpuProfileParametersStructsArray->Vn;
  					if(MotorFalgsRegsArray[AxisNumber]->MFLAGS.bit.CALIBRATION_REQUEST == 0)
  					{
				if(CpuProfileParametersStructsArray->ProfileCounter >= CpuProfilesArray->N4)
				{
					CpuProfileParametersStructsArray->EndOfConstantVelocityProfilePosition = CpuProfileParametersStructsArray->Sn;
					if(pToMoveStructArray->JerkTime != 0)
					{
						CpuProfileParametersStructsArray->ProfilerSwitch = 5;

					}
					else
					{
						CpuProfileParametersStructsArray->ProfilerSwitch = 6;
					}
				}
  					}
				break;

			case(5):
					CpuProfileParametersStructsArray->ProfileCounter++;
					CpuProfileParametersStructsArray->An = CpuProfileParametersStructsArray->An + CpuProfileParametersStructsArray->Jn; //CpuProfilesArray->ProfileJerk;
					CpuProfileParametersStructsArray->Vn = CpuProfileParametersStructsArray->Vn - CpuProfileParametersStructsArray->An;
					CpuProfileParametersStructsArray->Sn = CpuProfileParametersStructsArray->Sn + CpuProfileParametersStructsArray->Vn;
				if(CpuProfileParametersStructsArray->ProfileCounter >= CpuProfilesArray->N5)
      			 {
					CpuProfileParametersStructsArray->ProfilerSwitch = 6;
      			 }
				break;

			case(6):
					CpuProfileParametersStructsArray->ProfileCounter++;
					//cCpuToClaProfileVarStruct.An = cCpuToClaProfileVarStruct.An;
					CpuProfileParametersStructsArray->Vn = CpuProfileParametersStructsArray->Vn - CpuProfileParametersStructsArray->An;
					CpuProfileParametersStructsArray->Sn = CpuProfileParametersStructsArray->Sn + CpuProfileParametersStructsArray->Vn;
				if(CpuProfileParametersStructsArray->ProfileCounter >= CpuProfilesArray->N6)
      			 {
					if(pToMoveStructArray->JerkTime != 0)
      			 	{
						CpuProfileParametersStructsArray->ProfilerSwitch = 7;
      			 	}
      			 	else
      			 	{
      			 		CpuProfileParametersStructsArray->ProfilerSwitch = 0;
      			 		CpuProfileParametersStructsArray->ProfileCounter = 0;
          			 	if(pToMoveStructArray->MotionDistance > 0)
          			 	{
          			 		CpuProfileParametersStructsArray->Sn = CpuProfileParametersStructsArray->Sn + 1.0;
          			 	}
          			 	else
          			 	{
          			 		CpuProfileParametersStructsArray->Sn = CpuProfileParametersStructsArray->Sn - 1.0;
          			 	}
          			 	CpuProfileParametersStructsArray->Vn=0;
          			 	CpuProfileParametersStructsArray->An=0;

          			}
      			 }
				break;

				 case(7):
						CpuProfileParametersStructsArray->ProfileCounter++;
				 	 	CpuProfileParametersStructsArray->An = CpuProfileParametersStructsArray->An - CpuProfileParametersStructsArray->Jn; //CpuProfilesArray->ProfileJerk;
				 	 	CpuProfileParametersStructsArray->Vn = CpuProfileParametersStructsArray->Vn - CpuProfileParametersStructsArray->An;
				 	 	CpuProfileParametersStructsArray->Sn = CpuProfileParametersStructsArray->Sn + CpuProfileParametersStructsArray->Vn;
 				 if(CpuProfileParametersStructsArray->ProfileCounter >= CpuProfilesArray->N7)
      			 {
      			 	//asm(" ESTOP0");
 					CpuProfileParametersStructsArray->Jn = 0;
 					CpuProfileParametersStructsArray->An = 0;
 					CpuProfileParametersStructsArray->Vn = 0;
 					//CpuProfileParametersStructsArray->ProfilerSwitch = 0;
 					CpuProfileParametersStructsArray->ProfileCounter = CpuProfilesArray->N7;
      			 	if(pToMoveStructArray->MotionDistance > 0)
      			 	{
      			 		//sCommonCpuProfileParametersStruct.Sn = sCommonCpuProfileParametersStruct.Sn + 1.0;
      			 	}
      			 	else
      			 	{
      			 		//sCommonCpuProfileParametersStruct.Sn = sCommonCpuProfileParametersStruct.Sn - 1.0;
      			 	}
      			 	CpuProfileParametersStructsArray->ProfileEnd = 1;
      			 }

      			 break;

 			default:

				//CpuProfilesArray[CpuToClaProfileVars]->ProfilerSwitch = 0;
				//CpuProfilesArray[CpuToClaAdjusterFirstMove]->ProfilerSwitch = 0;
				//CpuProfilesArray[CpuToClaAdjusterSecondMove]->ProfilerSwitch = 0;

				//CpuProfilesArray[CpuToClaProfileVars]->ProfileCounter = 0;
				//CpuProfilesArray[CpuToClaAdjusterFirstMove]->ProfileCounter = 0;
				//CpuProfilesArray[CpuToClaAdjusterSecondMove]->ProfileCounter = 0;

				//CpuProfilesArray[CpuToClaProfileVars]->Sn = sCpuToCla1ProfileCommandsStruct.PositionCommand;
				//CpuProfilesArray[CpuToClaAdjusterFirstMove]->Sn = sCpuToCla1ProfileCommandsStruct.PositionCommand;
				//CpuProfilesArray[CpuToClaAdjusterSecondMove]->Sn = sCpuToCla1ProfileCommandsStruct.PositionCommand;


					//cCpuToClaProfileVarStruct.Sn = cCpuToClaProfileVarStruct.Sn;
					//cCpuToClaProfileVarStruct.Vn = 0;
					//cCpuToClaProfileVarStruct.An = 0;
					//cCpuToClaProfileVarStruct.ProfileJerk = 0;
					//pAdjusterParametersStruct.ProfileEnd = 1;
 				if((med_ctr.handle_mode == ACTIVE_MODE)&&(MotorFalgsRegsArray[AxisNumber]->MFLAGS.bit.ENABLE == 1))
 				{
 					pToMoveStructArray->DesiredPosition  = (int32_t)(CpuProfileParametersStructsArray->Sn);
 				}
 				else
 				{
 					if(MotorFalgsRegsArray[AxisNumber]->MFLAGS.bit.ENABLE == 0)
 					{
 						//sCommonCpuProfileParametersStruct.Sn = sControlVarStruct.PositionFeedback;
 						if((AxisNumber == X_Axis)&&ClaCpuStatusRegs.X_MFLAGS_CLASTATUS.bit.X_ENABLED == 0)
 						{
 							CpuProfileParametersStructsArray->Sn = sControlVarStruct.XPositionFeedback;
 							pToMoveStructArray->DesiredPosition  = sControlVarStruct.XMotorPosition;
 						}
 						else if((AxisNumber == Y_Axis)&&ClaCpuStatusRegs.Y_MFLAGS_CLASTATUS.bit.Y_ENABLED == 0)
 						{
 							CpuProfileParametersStructsArray->Sn = sControlVarStruct.YPositionFeedback;
 							pToMoveStructArray->DesiredPosition  = sControlVarStruct.YMotorPosition;
 						}
 						//pToMoveVarStructsArray->DesiredPosition = sCpuToCla1ProfileCommandsStructsArray->AbsoluteEncoderFeedback;
 						//pToMoveVarStructsArray[AxisNumber].DesiredPosition = sCpuToCla1ProfileCommandsStructsArray[AxisNumber].AbsoluteEncoderFeedback;
 						//X_pToMoveVariablesStruct.DesiredPosition = (int32_t)sControlVarStruct.PositionFeedback;


 						//MotorOnFlag = 0;
 					}
 					else
 					{
 						// TODO Missing write parameters
 						//CpuProfileParametersStructsArray->Sn = CpuProfileParametersStructsArray->Sn;
 						pToMoveStructArray->DesiredPosition = (int32_t)CpuProfileParametersStructsArray->Sn;
 					}
 					pToMoveStructArray->PreviousDesiredPosition = pToMoveStructArray->DesiredPosition;
 					CpuProfileParametersStructsArray->An = 0;
 					CpuProfileParametersStructsArray->Vn = 0;
 				}
				 break;
}
				CpuProfileParametersStructsArray->NextTargetPosition = (int32_t)CpuProfileParametersStructsArray->Sn;
				//sCpuToCla1ProfileCommandsStruct.PositionCommand = (int32_t)sCommonCpuProfileParametersStruct.Sn;
	//CAN_EnableInterrupt();



				return CpuProfileParametersStructsArray->NextTargetPosition;
}




#pragma CODE_SECTION(ProfileParsPrepareToMove, "ramfuncs")
void ProfileParsPrepareToMove(volatile struct _P2MOVE_VAR* pToMoveStruct, volatile struct CPU_PROFILE_VAR *CpuProfilesArray,volatile struct COMMON_CPU_PROFILE_VAR *CpuProfileParametersStructsArray)
{
			//pToMoveStruct -> FirstTargetPoint = sControlVarStruct.MotorPosition;
			//X_pToMoveVariablesStruct.FeedbackPosition = sControlVarStruct.MotorPosition;
			//X_pToMoveVariablesStruct.MaxPosError = X_pToMoveVariablesStruct.DesiredPosition - X_pToMoveVariablesStruct.FeedbackPosition;
			//X_pToMoveVariablesStruct.MotionDistance = (float)X_pToMoveVariablesStruct.MaxPosError;
			//pToMoveStruct->MotionDistance = (float)(pToMoveStruct->FinalTargetPoint - pToMoveStruct->StartTargetPoint);
			//X_pToMoveVariablesStruct.MotionDistance = (float)(X_pToMoveVariablesStruct.DesiredPosition - X_pToMoveVariablesStruct.FeedbackPosition);
			if(pToMoveStruct->DesiredPosition != pToMoveStruct->PreviousDesiredPosition)
			{

			pToMoveStruct->MotionDistance = (double)(pToMoveStruct->FinalTargetPoint - pToMoveStruct->StartTargetPoint);


			if(pToMoveStruct->MotionDistance != 0)
			{

				if(pToMoveStruct->MotionDistance < 0)
				{
					pToMoveStruct->MotionDistance = -pToMoveStruct->MotionDistance;
					pToMoveStruct->Vmax = pToMoveStruct->ProfileMaxVelocity;
					pToMoveStruct->MotionTime = pToMoveStruct->MotionDistance/pToMoveStruct->Vmax;
					pToMoveStruct->MotionDistance = -pToMoveStruct->MotionDistance;
				}
				else
				{
					pToMoveStruct->Vmax = pToMoveStruct->ProfileMaxVelocity;
					pToMoveStruct->MotionTime = pToMoveStruct->MotionDistance/pToMoveStruct->Vmax;
				}

				// Profile Parameters Calculation

				CpuProfilesArray ->PT = (uint32_t)(pToMoveStruct->MotionTime * 1000.0); // Profile Time Calculation

				pToMoveStruct->MotionTime = (double)(CpuProfilesArray ->PT);

				pToMoveStruct->AccelerationFactor = pToMoveStruct->Vmax/pToMoveStruct->ProfileAcceleration;
				pToMoveStruct->AccelerationTime = pToMoveStruct->AccelerationFactor*(double)(CpuProfilesArray->PT);
				CpuProfilesArray->AT = (uint32_t)(pToMoveStruct->AccelerationTime);
				pToMoveStruct->AccelerationTime = (double)(CpuProfilesArray->AT);


				CpuProfilesArray->CVT = (int32_t)CpuProfilesArray->PT - (int32_t)(2*CpuProfilesArray->AT);

				if((CpuProfilesArray->CVT < 0) || (pToMoveStruct->AccelerationTime == 0))
				{
					pToMoveStruct->MotionTime = 1000.0;
					pToMoveStruct->AccelerationTime = 500.0;
					CpuProfilesArray->AT = 500;
					CpuProfilesArray->PT = 1000;
					pToMoveStruct->Vmax = pToMoveStruct->MotionDistance/pToMoveStruct->AccelerationTime;
					pToMoveStruct->ProfileAcceleration = pToMoveStruct->Vmax/pToMoveStruct->AccelerationTime;
					pToMoveStruct->ProfileJerk = 10.0*pToMoveStruct->ProfileAcceleration;
					CpuProfilesArray->TriangularProfileFlag = 1;
				}
				else
				{
					CpuProfilesArray->TriangularProfileFlag = 0;
				}


				pToMoveStruct->JerkFactor = pToMoveStruct->ProfileAcceleration/pToMoveStruct->ProfileJerk;
				pToMoveStruct->JerkTime = pToMoveStruct->JerkFactor*pToMoveStruct->AccelerationTime;

				CpuProfilesArray->N1 = (uint32_t)(pToMoveStruct->JerkTime);
				pToMoveStruct->JerkTime = (double)(CpuProfilesArray->N1);

				CpuProfilesArray->ProfileVelocity = pToMoveStruct->MotionDistance/(pToMoveStruct->MotionTime - pToMoveStruct->AccelerationTime);
				CpuProfilesArray->ProfileAcceleration = CpuProfilesArray->ProfileVelocity/(pToMoveStruct->AccelerationTime-pToMoveStruct->JerkTime);
				if(pToMoveStruct->JerkTime != 0)
				{
					CpuProfilesArray->ProfileJerk = CpuProfilesArray->ProfileAcceleration/pToMoveStruct->JerkTime;
				}
				else
				{
					CpuProfilesArray->ProfileJerk = 0;
				}
				CpuProfileParametersStructsArray->Jn = CpuProfilesArray->ProfileJerk;
				CpuProfilesArray->N2 = CpuProfilesArray->AT - CpuProfilesArray->N1;
				CpuProfilesArray->N3 = CpuProfilesArray->AT;
				CpuProfilesArray->N4 = CpuProfilesArray->PT - CpuProfilesArray->AT;
				CpuProfilesArray->N5 = CpuProfilesArray->N4 + CpuProfilesArray->N1;
				CpuProfilesArray->N6 = CpuProfilesArray->N4 + CpuProfilesArray->N2;
				CpuProfilesArray->N7 = CpuProfilesArray->PT;
				CpuProfileParametersStructsArray->ProfileCounter = 0;
				CpuProfileParametersStructsArray->ProfilerSwitch = 1;
				CpuProfileParametersStructsArray->ProfileEnd = 0;
				pToMoveStruct->PreviousDesiredPosition = pToMoveStruct->DesiredPosition;
			}
		}
}

#pragma CODE_SECTION(SinTableCalc, "ramfuncs")
void SinTableCalc(void)
{
	uint16_t i = 0;

	for(i = 0; i <= (TablePointsNumber-1); i++)
	{
		SinArgument = (float)i/(float)TablePointsNumber;
		*ClaSinTableBaseAddressPointer = _IQcosPU(SinArgument);

		ClaSinTableBaseAddressPointer++;
	}
	ClaSinTableBaseAddressPointer = (float *)0x00800A;
}



#pragma CODE_SECTION(CalcCommutCommands, "ramfuncs")
void CalcCommutCommands(void)
{
	CPU_TABLE_VALUE_A = _IQcosPU(0);
	CPU_TABLE_VALUE_B = (float)(2*PiDiv3TablePointsNumber)/(float)TablePointsNumber;
	CPU_TABLE_VALUE_B = _IQcosPU(CPU_TABLE_VALUE_B);
	CPU_TABLE_VALUE_C = (float)(PiDiv3TablePointsNumber)/(float)TablePointsNumber;
	CPU_TABLE_VALUE_C = _IQcosPU(CPU_TABLE_VALUE_C);
	//CPU_TABLE_VALUE_B = -0.5;
	//CPU_TABLE_VALUE_C = -0.5;
}

/*============================
 * Take X AXIS Motor out of servo status
 * ==========================*/
#pragma CODE_SECTION(X_AxisServoOff, "ramfuncs")
uint16_t X_AxisServoOff(void)
{
	uint16_t Err_OK = ERR_OK;
	MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.ENABLE = 0;
	DELAY_US(100);
	if(ClaCpuStatusRegs.X_MFLAGS_CLASTATUS.bit.X_ENABLED == 0)
	{
		X_sCommonCpuProfileParametersStruct.ProfilerSwitch = 0; // Switch profiler to default
	}
	else
	{
		// TODO Fault message : Motor didn't switched off
		// Err_OK = Fault Message
	}
	return Err_OK;
}


/*============================
 * Take X AXIS Motor out of servo status
 * ==========================*/
#pragma CODE_SECTION(Y_AxisServoOff, "ramfuncs")
uint16_t Y_AxisServoOff(void)
{
	uint16_t Err_OK = ERR_OK;
	MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.ENABLE = 0;
	DELAY_US(100);
	if(ClaCpuStatusRegs.Y_MFLAGS_CLASTATUS.bit.Y_ENABLED == 0)
	{
		Y_sCommonCpuProfileParametersStruct.ProfilerSwitch = 0; // Switch profiler to default
	}
	else
	{
		// TODO Fault message : Motor didn't switched off
		// Err_OK = Fault Message
	}
	return Err_OK;
}

/*============================
 * Take X Axis Motor out of servo status
 * ==========================*/
#pragma CODE_SECTION(X_AxisServoOn, "ramfuncs")
uint16_t X_AxisServoOn(void)
{
	uint16_t Err_OK = 0;
	if(MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.ENABLE == 0 && ClaCpuStatusRegs.X_MFLAGS_CLASTATUS.bit.X_ENABLED == 0 && ClaCpuStatusRegs.X_MFLAGS_CLASTATUS.bit.X_FAULT_FLAG == 0)
	{
		MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.ENABLE = 1;
		DELAY_US(100);
		if(ClaCpuStatusRegs.X_MFLAGS_CLASTATUS.bit.X_ENABLED == 1)
		{
			X_sCommonCpuProfileParametersStruct.ProfilerSwitch = 0; // Switch profiler to default
			X_pToMoveVariablesStruct.DesiredPosition = sControlVarStruct.XMotorPosition;
			X_pToMoveVariablesStruct.PreviousDesiredPosition = X_pToMoveVariablesStruct.DesiredPosition;
		}
		else
		{
			// TODO Fault message : Motor didn't switched to servo
			//ClaCpuStatusRegs.CLASTATUS.bit.FAULT_FLAG;
			//ClaCpuStatusRegs.CLASTATUS.bit.FAULT_NUMBER = ?
		}
	}
	else
	{
		if(ClaCpuStatusRegs.X_MFLAGS_CLASTATUS.bit.X_FAULT_FLAG)
		{
			Err_OK = ClaCpuStatusRegs.X_MFLAGS_CLASTATUS.bit.X_FAULT_NUMBER;
		}
		else
		{
			Err_OK = COMMON_FAULT;
		}
		X_sCommonCpuProfileParametersStruct.ProfilerSwitch = 0;

		//TODO Mismatch between Enable Parameters
	}
	return Err_OK;
}



/*============================
 * Take Y Axis Motor out of servo status
 * ==========================*/
#pragma CODE_SECTION(Y_AxisServoOn, "ramfuncs")
uint16_t Y_AxisServoOn(void)
{
	uint16_t Err_OK = 0;
	if(MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.ENABLE == 0 && ClaCpuStatusRegs.Y_MFLAGS_CLASTATUS.bit.Y_ENABLED == 0 && ClaCpuStatusRegs.Y_MFLAGS_CLASTATUS.bit.Y_FAULT_FLAG == 0)
	{
		MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.ENABLE = 1;
		DELAY_US(100);
		if(ClaCpuStatusRegs.Y_MFLAGS_CLASTATUS.bit.Y_ENABLED == 1)
		{
			Y_sCommonCpuProfileParametersStruct.ProfilerSwitch = 0; // Switch profiler to default
			Y_pToMoveVariablesStruct.DesiredPosition = sControlVarStruct.YMotorPosition;
			Y_pToMoveVariablesStruct.PreviousDesiredPosition = Y_pToMoveVariablesStruct.DesiredPosition;
		}
		else
		{
			// TODO Fault message : Motor didn't switched to servo
			//ClaCpuStatusRegs.CLASTATUS.bit.FAULT_FLAG;
			//ClaCpuStatusRegs.CLASTATUS.bit.FAULT_NUMBER = ?
		}
	}
	else
	{
		if(ClaCpuStatusRegs.Y_MFLAGS_CLASTATUS.bit.Y_FAULT_FLAG)
		{
			Err_OK = ClaCpuStatusRegs.Y_MFLAGS_CLASTATUS.bit.Y_FAULT_NUMBER;
		}
		else
		{
			Err_OK = COMMON_FAULT;
		}
		Y_sCommonCpuProfileParametersStruct.ProfilerSwitch = 0;

		//TODO Mismatch between Enable Parameters
	}
	return Err_OK;
}



/*============================
 * Commutation status
 * ==========================*/
#pragma CODE_SECTION(CommutCheck, "ramfuncs")
void CommutCheck(volatile struct MOTORFLAGS_REGS* MotorFalgsRegitersArray[], uint16_t *CommutIndex)
{
	switch(*CommutIndex)
	{
	case 1:
		MotorFalgsRegitersArray[X_Axis]->MFLAGS.bit.RESET_COMMUT = 1;
		*CommutIndex = 2;
		break;

	case  2:
		if(ClaCpuStatusRegs.X_MFLAGS_CLASTATUS.bit.X_COMMUT_IN_PROCESS == 0 && ClaCpuStatusRegs.X_MFLAGS_CLASTATUS.bit.X_COMMUT_DONE == 0)
		{
			MotorFalgsRegitersArray[X_Axis]->MFLAGS.bit.RESET_COMMUT = 0;
			*CommutIndex = 3;
		}
		break;

	case 3:
			//CalcCommutCommand();
			Cla1ForceTask3();
			*CommutIndex = 4;
		break;

	case 4:
		if(ClaCpuStatusRegs.X_MFLAGS_CLASTATUS.bit.X_COMMUT_IN_PROCESS == 0 && ClaCpuStatusRegs.X_MFLAGS_CLASTATUS.bit.X_COMMUT_DONE == 1)
		{
			MotorFalgsRegitersArray[X_Axis]->MFLAGS.bit.COMMUTATION = 0;
			X_AxisServoOff();
			*CommutIndex = 0;
			TxArbitrator.XActionCompleteFlag = 1;
		}
		break;
	}

}

__interrupt void cpu_timer0_isr(void)
{
	uint8_t ErrCode;
	//asm(" ESTOP0");
	StartTick = 1;
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
	if(sServoParametersStruct.WriteEnableFlag)
	{
		ErrCode = D_Timer_Stop(TIMER_0);
		sServoParametersStruct.ret |= ExtFlash_Erase_SubSector ( sServoParametersStruct.SubSectorNumber, 0);
		sServoParametersStruct.ret |= ExtFlash_Program_UpToPage (sServoParametersStruct.My_Addr ,sServoParametersStruct.WriteData, sServoParametersStruct.Length);
		sServoParametersStruct.WriteEnableFlag = 0;
		StartTick = 0;
		ErrCode = D_Timer_Start(TIMER_0,1000);
	}
	//asm(" ESTOP0");
}

/*==============================================================================
Function : CurrentLoopAdj

Description : This function sets current loop

Period 				: Time for current producing
CurrentAmplitude 	: Current amplitude for adjusting
==============================================================================*/
/*void CurrentLoopAdj(uint16_t Period, uint16_t CurrentAmplitude)
{

    EALLOW;
    	PieVectTable.ADCA1_INT = &adca1_isr; //function for ADCA interrupt 1
    	//enable PIE interrupt
        PieCtrlRegs.PIEIER1.bit.INTx1 = 1;
        //sync ePWM
        CpuSysRegs.PCLKCR0.bit.TBCLKSYNC = 1;
    EDIS;

	// Set the EPWM1INT as the trigger for task 1
	DmaClaSrcSelRegs.CLA1TASKSRCSEL1.bit.TASK1 = 0;
	while(Cla1Regs.MIRUN.bit.INT1 == 1);

}
*/

// No More


/*******************************************************************************
 * File name: RWLK_ServoManager.c
 *
 * Description:
 *
 *
 * Note:
 *
 * Author:	Orit Ben Horin
 * Date: Dec 2016
 *
 ******************************************************************************/

/*******************************************************************************
 *  Include
 ******************************************************************************/
#include "RWLK_ServoManager.h"
//#include "RWLK_CAN_Msg_Handler.h"

//#include "RWLK_CAN_Application.h"
//#include "RWLK_CardConfig.h"

//#include "stdio.h"
#include "RWLK_DRV8031_Applications.h"
#include "RWLK_ADC_Driver.h"
//#include "RWLK_ePWM_Driver.h"
#include "RWLK_Common.h"
#include "CpCl_MsgCtrl.h"
#include "RWLK_Motor_Control.h"

/*******************************************************************************
 *  Definitions
 ******************************************************************************/

// This struct is the working surface of this module
//typedef struct
//{
//	uint16_t MyCardId;  // Hold the card id of the MCU running the application
//	bool ManualModeFlag;
//	int16_t MaxPeakCurrentThreshold;
//	// IAM_ALIVE
//	bool ButtonPushedFlag;
//	bool IamAliveActivateFlag;
//	uint32_t IamAliveCounter;
//	bool  ByPassEnable;
//	bool FirstMoveafterBoot;
//	//static uint16_t AdcErr = 0;
//	uint16_t TestSystemCounter;
//	bool TestSystemFlag; // this flag is to mark the test shall be perfomed, it is not the flag for the message to be sent back to the MPB
//	bool DataLogFlag; // current Implementation treating only
//	bool OpenLoopModeFlag;  // TODO: shall be connected to some logic.
//	// STOP_MOTOR
//	bool MovementFlag;
//	// Filter (commands of set servo position/velocity/current)
//	PI_FILTER_MEMBERS FilterMember;
//	uint32_t TempTimeCounter;
//	bool AnotherAxisEncoderDisc;
//
//} RWLK_Logic_Variables_Type;


/*******************************************************************************
 *  Global variables
 ******************************************************************************/
//#pragma DATA_SECTION(Input_Command_Parameter_Pointer,"ramdata")
//volatile Input_Command_Parameters_Type *Input_Command_Parameter_Pointer=NULL;
//#pragma DATA_SECTION(Output_Command_Parameter_Pointer,"ramdata")
//static volatile Output_Command_Parameters_Type *Output_Command_Parameter_Pointer=NULL;
//#pragma DATA_SECTION(transmitFlagsPtr,"ramdata")
//static sTransmitFlags *transmitFlagsPtr = NULL;
//
//extern Input_Command_Parameters_Type Requested_Command_Parameters;
//
// TODO: orit - have pointers istead of extern
//extern Output_Command_Parameters_Type Calculated_Command_Parameters;
extern sTransmitFlags TxArbitrator;
extern _CPU_MsgStruct CPU_Msg;

//extern volatile struct _P2MOVE_VAR pToMoveVariablesStruct;


//RWLK_Logic_Variables_Type RWLK_Logic_Vars;

/*******************************************************************************
 *  static functions
 ******************************************************************************/
//static void ProcessReceivedCommand(void);



/*****************************************************************************
 * Function name: RWLK_ServoManagerInit
 *
 * Description: Initialize global structures and variables of the ServoManager module.
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
void RWLK_ServoManagerInit(void)
{

}

/*****************************************************************************
 * Function name: MotorEnableRequest
 *
 * Description: Enable motor
 * Parameters:  None
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(MotorEnableRequest, "ramfuncs")
uint16_t MotorEnableRequest(uint16_t Axis)
{
	uint16_t status;
	if(Axis == X_Axis)
	{
		status = X_AxisServoOn();
		if(status == ERR_OK)
		{
			TxArbitrator.XActionCompleteFlag = true;
		}
		else
		{
			TxArbitrator.ActionNotCompleteFlag = true;
		}
	}
	else if(Axis == Y_Axis)
	{

		status = Y_AxisServoOn();
		if(status == ERR_OK)
		{
			TxArbitrator.YActionCompleteFlag = true;
		}
		else
		{
			TxArbitrator.ActionNotCompleteFlag = true;
		}



	}
	return status;
}

/*****************************************************************************
 * Function name: MotorDisableRequest
 *
 * Description: Disable motor
 * Parameters:  None
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(MotorDisableRequest, "ramfuncs")
uint16_t MotorDisableRequest(uint16_t Axis)
{
	uint16_t status;
	if(Axis == X_Axis)
	{
		status = X_AxisServoOff();
		if(status == ERR_OK)
		{
			TxArbitrator.XActionCompleteFlag = true;
		}
		else
		{
			TxArbitrator.ActionNotCompleteFlag = true;
		}
	}
	else if(Axis == Y_Axis)
	{
		status = Y_AxisServoOff();
		if(status == ERR_OK)
		{
			TxArbitrator.YActionCompleteFlag = true;
		}
		else
		{
			TxArbitrator.ActionNotCompleteFlag = true;
		}
	}
	return status;
}

/*****************************************************************************
 * Function name: MotorReleaseRequest
 *
 * Description:
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(MotorReleaseRequest, "ramfuncs")
void MotorReleaseRequest(uint16_t Axis)
{
	if(Axis == X_Axis)
	{
		XEN_GATE_CLEAR = 1;
		TxArbitrator.XActionCompleteFlag = true;
	}
	else if(Axis == Y_Axis)
	{
		YEN_GATE_CLEAR = 1;
		TxArbitrator.YActionCompleteFlag = true;
	}
}

/*****************************************************************************
 * Function name: MotorUnReleaseRequest
 *
 * Description:
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(MotorUnReleaseRequest, "ramfuncs")
void MotorUnReleaseRequest(uint16_t Axis)
{
	if(Axis == X_Axis)
	{
		XEN_GATE_SET = 1;
		TxArbitrator.XActionCompleteFlag = true;
	}
	else if(Axis == Y_Axis)
	{
		YEN_GATE_SET = 1;
		TxArbitrator.YActionCompleteFlag = true;
	}
}

/*****************************************************************************
 * Function name: SetAdjusterRequest
 *
 * Description: Process Income command.
 * Parameters:  None
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(SetAdjusterRequest, "ramfuncs")
void SetAdjusterRequest(ADJUSTER_REQUEST_PARAMETERS rName,volatile struct MOTORFLAGS_REGS* MotorFlagsRegistersArray,volatile struct ADJUSTER_PROFILE_PAR *ParametersStructArray, uint16_t AxisNumber)
{
	ParametersStructArray->AdjusterRequestFlag = 1;
	ParametersStructArray->AdjusterSwitch = 1;
	//pAdjusterParametersStructsPointer[AxisNumber]->AdjusterSwitch = 1;
	//X_sCpuToCla1ProfileCommandsStruct.AdjusterRequestFlag = 1;
	switch(rName)
	{
		case POS_ADJUSTER_REQUEST:
			{
				MotorFlagsRegistersArray->MFLAGS.bit.POSITION_ADJUSTER = 1;
				//MotorFlagsRegs[AxisNumber]->POSITION_ADJUSTER = 1;
				ParametersStructArray->AdjusterSwitch = 1;
				ParametersStructArray->DwellTime = 3000;
			//pAdjusterParametersStruct.AdjusterProfileTarget = 0x3000;
			//pAdjusterParametersStruct.AdjusterProfileTarget = pAdjusterParametersStruct.AdjusterProfileTarget + sControlVarStruct.MotorPosition;
			//pAdjusterParametersStruct.AdjusterProfileVelocity = 12288.0;
			//pAdjusterParametersStruct.AdjusterProfileAcc = pAdjusterParametersStruct.AdjusterProfileVelocity*10.0;
			//pAdjusterParametersStruct.AdjusterProfileJerk = pAdjusterParametersStruct.AdjusterProfileAcc*10.0;
			}
			break;

		case VEL_ADJUSTER_REQUEST:
			{
				MotorFlagsRegistersArray->MFLAGS.bit.VELOCITY_ADJUSTER = 1;
				sDigitalToAnalogSensorStruct.DigitalToAnalogOutputResolution = (float)HALF_DAC_RESOLUTION/(float)MAX_MOTOR_VEL_MEASUREMENT;
				sDigitalToAnalogSensorStruct.RelativeResolutionsCoefficient = (float)((uint32_t)SAMPLING_FREQUENCY*(uint32_t)ONE_MINUTE_IN_SEC)/(float)XEncoderResolution;
				sDigitalToAnalogSensorStruct.RelativeResolutionsCoefficient = sDigitalToAnalogSensorStruct.RelativeResolutionsCoefficient*sDigitalToAnalogSensorStruct.DigitalToAnalogOutputResolution;
				ParametersStructArray->DwellTime = 1000;
				ParametersStructArray->AdjusterProfileVelocity = 5;
			}
			break;
		case CUR_ADJUSTER_REQUEST:
			{
				MotorFlagsRegistersArray->MFLAGS.bit.CURRENT_ADJUSTER = 1;
				MotorFlagsRegistersArray->MFLAGS.bit.CURRENT_LOOP = 1;
				sDigitalToAnalogSensorStruct.CurrentSensorResolution = (float)HALF_ADC_RESOLUTION/CURRENT_SENSOR_MEASUREMENT_RATE;
				sDigitalToAnalogSensorStruct.DigitalToAnalogOutputResolution = (float)HALF_DAC_RESOLUTION/(float)MAXIMUM_CURRENT_MEASUREMENT;
				sDigitalToAnalogSensorStruct.RelativeResolutionsCoefficient = sDigitalToAnalogSensorStruct.DigitalToAnalogOutputResolution/sDigitalToAnalogSensorStruct.CurrentSensorResolution;
				sDigitalToAnalogSensorStruct.HalfOfDigitalToAnalogResolution = (int32_t)HALF_DAC_RESOLUTION;
				ParametersStructArray->DwellTime = 10;
				ParametersStructArray->AdjusterCurrentCommand= (int32_t)(sDigitalToAnalogSensorStruct.CurrentSensorResolution);
			}
			break;

		case SERVO_ANALIZER_REQUEST:
			MotorFlagsRegistersArray->MFLAGS.bit.SERVO_ANALYSYS = 1;
			if(AxisNumber == X_Axis)
			{
				pToMoveVarStructsArray[X_Axis]->StartServo = (int32_t)sControlVarStruct.XPositionFeedback;
			}
			else if(AxisNumber == Y_Axis)
			{
				pToMoveVarStructsArray[Y_Axis]->StartServo = (int32_t)sControlVarStruct.YPositionFeedback;
			}
			break;

		case STOP_ADJUSTER_REQUEST:
			ParametersStructArray->AdjusterRequestFlag = 0;
			//pAdjusterParametersStruct.AdjusterSwitch = 0;
			break; // Switch Off adjuster request flag
	}
	/*if(AxisNumber == X_Axis)
	{
		X_sCpuToCla1ProfileCommandsStruct.AdjusterRequestFlag = 1;
		switch(rName)
		{
			case POS_ADJUSTER_REQUEST:
				{
					CpuToClaCntrlRegs->X_MFLAGS.bit.X_POSITION_ADJUSTER = 1;
					pAdjusterParametersStruct.AdjusterSwitch = 1;
					pAdjusterParametersStruct.DwellTime = 3000;
				//pAdjusterParametersStruct.AdjusterProfileTarget = 0x3000;
				//pAdjusterParametersStruct.AdjusterProfileTarget = pAdjusterParametersStruct.AdjusterProfileTarget + sControlVarStruct.MotorPosition;
				//pAdjusterParametersStruct.AdjusterProfileVelocity = 12288.0;
				//pAdjusterParametersStruct.AdjusterProfileAcc = pAdjusterParametersStruct.AdjusterProfileVelocity*10.0;
				//pAdjusterParametersStruct.AdjusterProfileJerk = pAdjusterParametersStruct.AdjusterProfileAcc*10.0;
				}
				break;

			case VEL_ADJUSTER_REQUEST:
				{
					CpuToClaCntrlRegs->X_MFLAGS.bit.X_VELOCITY_ADJUSTER = 1;
					sDigitalToAnalogSensorStruct.DigitalToAnalogOutputResolution = (float)HALF_DAC_RESOLUTION/(float)MAX_MOTOR_VEL_MEASUREMENT;
					sDigitalToAnalogSensorStruct.RelativeResolutionsCoefficient = (float)((uint32_t)SAMPLING_FREQUENCY*(uint32_t)ONE_MINUTE_IN_SEC)/(float)XEncoderResolution;
					sDigitalToAnalogSensorStruct.RelativeResolutionsCoefficient = sDigitalToAnalogSensorStruct.RelativeResolutionsCoefficient*sDigitalToAnalogSensorStruct.DigitalToAnalogOutputResolution;
					pAdjusterParametersStruct.DwellTime = 1000;
					pAdjusterParametersStruct.AdjusterProfileVelocity = 5;
				}
				break;
			case CUR_ADJUSTER_REQUEST:
				{
					CpuToClaCntrlRegs->X_MFLAGS.bit.X_CURRENT_ADJUSTER  = 1;
					CpuClaCntrlRegs.X_MFLAGS.bit.X_CURRENT_LOOP = 1;
					sDigitalToAnalogSensorStruct.CurrentSensorResolution = (float)HALF_ADC_RESOLUTION/CURRENT_SENSOR_MEASUREMENT_RATE;
					sDigitalToAnalogSensorStruct.DigitalToAnalogOutputResolution = (float)HALF_DAC_RESOLUTION/(float)MAXIMUM_CURRENT_MEASUREMENT;
					sDigitalToAnalogSensorStruct.RelativeResolutionsCoefficient = sDigitalToAnalogSensorStruct.DigitalToAnalogOutputResolution/sDigitalToAnalogSensorStruct.CurrentSensorResolution;
					sDigitalToAnalogSensorStruct.HalfOfDigitalToAnalogResolution = (int32_t)HALF_DAC_RESOLUTION;
					pAdjusterParametersStruct.DwellTime = 10;
					pAdjusterParametersStruct.AdjusterCurrentCommand = (int32_t)(sDigitalToAnalogSensorStruct.CurrentSensorResolution);
					xdpi1.Umax = 2000;
					xdpi1.Umin = -2000;
				}
				break;

			case SERVO_ANALIZER_REQUEST:
				CpuToClaCntrlRegs->X_MFLAGS.bit.X_SERVO_ANALYSYS = 1;
				X_pToMoveVariablesStruct.StartServo = (int32_t)sControlVarStruct.PositionFeedback;
				break;

			case STOP_ADJUSTER_REQUEST:
				ParametersStruct->AdjusterRequestFlag = 0;
				X_sCpuToCla1ProfileCommandsStruct.AdjusterRequestFlag = 0;
				//pAdjusterParametersStruct.AdjusterSwitch = 0;
				break; // Switch Off adjuster request flag
		}
	}
	else
	{
		Y_sCpuToCla1ProfileCommandsStruct.AdjusterRequestFlag = 1;
		switch(rName)
		{
			case POS_ADJUSTER_REQUEST:
				{
					CpuToClaCntrlRegs->Y_MFLAGS.bit.Y_POSITION_ADJUSTER = 1;
					pAdjusterParametersStruct.AdjusterSwitch = 1;
					pAdjusterParametersStruct.DwellTime = 3000;
					//pAdjusterParametersStruct.AdjusterProfileTarget = 0x3000;
					//pAdjusterParametersStruct.AdjusterProfileTarget = pAdjusterParametersStruct.AdjusterProfileTarget + sControlVarStruct.MotorPosition;
					//pAdjusterParametersStruct.AdjusterProfileVelocity = 12288.0;
					//pAdjusterParametersStruct.AdjusterProfileAcc = pAdjusterParametersStruct.AdjusterProfileVelocity*10.0;
					//pAdjusterParametersStruct.AdjusterProfileJerk = pAdjusterParametersStruct.AdjusterProfileAcc*10.0;
				}
				break;

			case VEL_ADJUSTER_REQUEST:
				{
					CpuToClaCntrlRegs->Y_MFLAGS.bit.Y_VELOCITY_ADJUSTER = 1;
					sDigitalToAnalogSensorStruct.DigitalToAnalogOutputResolution = (float)HALF_DAC_RESOLUTION/(float)MAX_MOTOR_VEL_MEASUREMENT;
					sDigitalToAnalogSensorStruct.RelativeResolutionsCoefficient = (float)((uint32_t)SAMPLING_FREQUENCY*(uint32_t)ONE_MINUTE_IN_SEC)/(float)XEncoderResolution;
					sDigitalToAnalogSensorStruct.RelativeResolutionsCoefficient = sDigitalToAnalogSensorStruct.RelativeResolutionsCoefficient*sDigitalToAnalogSensorStruct.DigitalToAnalogOutputResolution;
					pAdjusterParametersStruct.DwellTime = 1000;
					pAdjusterParametersStruct.AdjusterProfileVelocity = 5;
				}
				break;
			case CUR_ADJUSTER_REQUEST:
				{
					CpuToClaCntrlRegs->Y_MFLAGS.bit.Y_CURRENT_ADJUSTER  = 1;
					CpuClaCntrlRegs.Y_MFLAGS.bit.Y_CURRENT_LOOP = 1;
					sDigitalToAnalogSensorStruct.CurrentSensorResolution = (float)HALF_ADC_RESOLUTION/CURRENT_SENSOR_MEASUREMENT_RATE;
					sDigitalToAnalogSensorStruct.DigitalToAnalogOutputResolution = (float)HALF_DAC_RESOLUTION/(float)MAXIMUM_CURRENT_MEASUREMENT;
					sDigitalToAnalogSensorStruct.RelativeResolutionsCoefficient = sDigitalToAnalogSensorStruct.DigitalToAnalogOutputResolution/sDigitalToAnalogSensorStruct.CurrentSensorResolution;
					sDigitalToAnalogSensorStruct.HalfOfDigitalToAnalogResolution = (int32_t)HALF_DAC_RESOLUTION;
					pAdjusterParametersStruct.DwellTime = 10;
					pAdjusterParametersStruct.AdjusterCurrentCommand = (int32_t)(sDigitalToAnalogSensorStruct.CurrentSensorResolution);
					ydpi1.Umax = 2000;
					ydpi1.Umin = -2000;
				}
				break;

			case SERVO_ANALIZER_REQUEST:
				CpuToClaCntrlRegs->Y_MFLAGS.bit.Y_SERVO_ANALYSYS = 1;
				Y_pToMoveVariablesStruct.StartServo = (int32_t)sControlVarStruct.PositionFeedback;
				break;

			case STOP_ADJUSTER_REQUEST:
				ParametersStruct->AdjusterRequestFlag = 0;
				Y_sCpuToCla1ProfileCommandsStruct.AdjusterRequestFlag = 0;
				//pAdjusterParametersStruct.AdjusterSwitch = 0;
				break; // Switch Off adjuster request flag
		}
	}*/
}

/*****************************************************************************
 * Function name: SetPiGains
 *
 * Description: Handle the logic for ...
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(SetPiGains, "ramfuncs")
void SetPiGains(FILTER_NAME fName, PI *filter[], PI_FILTER_MEMBERS filterMember, uint32_t filterValue)
{
	switch(filterMember)
	{
		case KP:
			filter[fName]->Kp = (float)filterValue*0.001;
			break;
		case KI:
			filter[fName]->Ki = (float)filterValue*0.0001;
			break;
		case UMAX:
			filter[fName]->Umax = (float)filterValue;
			filter[fName]->Umin = -(float)filterValue;
			break;
		case I10:
			filter[fName]->i10 = (float)filterValue;
			break;
		case I6:
			filter[fName]->i6 = (float)filterValue;
			break;
	}
}


/*****************************************************************************
 * Function name: SetPiGains
 *
 * Description: Handle the logic for ...
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(SetClaPiGains, "ramfuncs")
void SetClaPiGains(CLA_FILTER_NAME fName, PI_FILTER_MEMBERS filterMember, uint32_t filterValue, uint16_t AxisNumber)
{
	CPU_Msg.Data[0] = fName;
	CPU_Msg.Data[1] = filterMember;
	CPU_Msg.Data[2] = (uint16_t)filterValue;
/*	switch(filterMember)
	{
		case KP:
			CPU_Msg.Data[2] = (uint16_t)filterValue;
			break;
		case KI:
			CPU_Msg.Data[2] = (uint16_t)filterValue;
			break;
		case UMAX:
		case I10:
		case I6:
			CPU_Msg.Data[2] = (uint16_t)filterValue;
			break;
	}*/
	if(AxisNumber == X_Axis){
		MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.PARAMETERS_CHANGE_REQUEST = 1;}
	else if(AxisNumber == Y_Axis){
		MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.PARAMETERS_CHANGE_REQUEST = 1;}
}


/*****************************************************************************
 * Function name: SetPiGains
 *
 * Description: Handle the logic for ...
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(SetClaPidGains, "ramfuncs")
void SetClaPidGains(CLA_FILTER_NAME fName, PID_FILTER_MEMBERS filterMember, uint32_t filterValue, uint16_t AxisNumber)
{
	CPU_Msg.Data[0] = fName;
	CPU_Msg.Data[1] = filterMember;
	CPU_Msg.Data[2] = (uint16_t)filterValue;
/*	switch(filterMember)
	{
		case KP:
			CPU_Msg.Data[2] = (uint16_t)filterValue;
			break;
		case KI:
			CPU_Msg.Data[2] = (uint16_t)filterValue;
			break;
		case UMAX:
		case I10:
		case I6:
			CPU_Msg.Data[2] = (uint16_t)filterValue;
			break;
	}*/
	if(AxisNumber == X_Axis){
		MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.PARAMETERS_CHANGE_REQUEST = 1;}
	else if(AxisNumber == Y_Axis){
		MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.PARAMETERS_CHANGE_REQUEST = 1;}
}


/*****************************************************************************
 * Function name: SetClaSofPars
 *
 * Description: Handle the logic for ...
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(SetClaSofPars, "ramfuncs")
void SetClaSofPars(SOF_FILTER_MEMBERS filterMember, uint32_t filterValue)
{

	switch(filterMember)
	{
		case VLSOFBW:
			 sVelLoopSecondOrderFilterStruct.SLVSOFBW = (float)filterValue;
			break;
		case VLSOFDR:
			 sVelLoopSecondOrderFilterStruct.SLVSOFDR = (float)filterValue*0.001;
			break;
	}
}




/*****************************************************************************
 * Function name: SetClaNfPars
 *
 * Description: Handle the logic for ...
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(SetClaNfPars, "ramfuncs")
void SetClaNfPars(NOTCH_FILTER_MEMBERS filterMember, uint32_t filterValue)
{

	switch(filterMember)
	{
		case VLNATT:
			sVelLoopNotchFilterStruct.SLVNATT = (float)filterValue;
			break;
		case VLNFREQ:
			sVelLoopNotchFilterStruct.SLVNFREQ = (float)filterValue;
			break;
		case VLNWID:
			sVelLoopNotchFilterStruct.SLVNWID = (float)filterValue;
			break;
	}
}

/*****************************************************************************
 * Function name: SetSofParametrs
 *
 * Description: Handle the logic for ...
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(SetSofParametrs, "ramfuncs")
void SetSofParametrs(VL_FILTER_NAME fName, uint16_t Axis)
{


	CPU_Msg.Data[0] = fName;
	if(fName == VLSOF)
	{
		vlsof.a1 = sVelLoopSecondOrderFilterStruct.a1;
		vlsof.a2 = sVelLoopSecondOrderFilterStruct.a2;
		vlsof.b0 = sVelLoopSecondOrderFilterStruct.b0;
		vlsof.b1 = sVelLoopSecondOrderFilterStruct.b1;
		vlsof.b2 = sVelLoopSecondOrderFilterStruct.b2;
	}
	else if(fName == VLNF)
	{
		vlsof.a1 = sVelLoopNotchFilterStruct.a1;
		vlsof.a2 = sVelLoopNotchFilterStruct.a2;
		vlsof.b0 = sVelLoopNotchFilterStruct.b0;
		vlsof.b1 = sVelLoopNotchFilterStruct.b1;
		vlsof.b2 = sVelLoopNotchFilterStruct.b2;
	}
	if(Axis == X_Axis){
		MotorFalgsRegsArray[X_Axis]->MFLAGS.bit.PARAMETERS_CHANGE_REQUEST = 1;}
	else if(Axis == Y_Axis){
		MotorFalgsRegsArray[Y_Axis]->MFLAGS.bit.PARAMETERS_CHANGE_REQUEST = 1;}
}

/*****************************************************************************
 * Function name: SetProfileParameters
 *
 * Description: SetProfileParameters
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
#pragma CODE_SECTION(SetProfileParameters, "ramfuncs")
void SetProfileParameters(POSITION_ADJUSTER_PARAMS pName,volatile struct ADJUSTER_PROFILE_PAR *ParametersStructArray,int32_t parameterValue)
{
	switch(pName)
	{
	case A_DT: 		ParametersStructArray->DwellTime = (uint32_t)parameterValue; break;
	case A_TARGET:	ParametersStructArray->AdjusterProfileTarget = parameterValue; break; //TempPar*EncoderResolution; break;
	case A_VEL:		ParametersStructArray->AdjusterProfileVelocity = (float)parameterValue; break;
	case A_ACC:		ParametersStructArray->AdjusterProfileAcc = (float)parameterValue; break;
	case A_JERK:	ParametersStructArray->AdjusterProfileJerk = (float)parameterValue; break;
	case A_CURRENT: ParametersStructArray->AdjusterCurrentCommand = parameterValue; break;
	}

}

#pragma CODE_SECTION(CalcSofParametrs, "ramfuncs")
void CalcSofParametrs(void)
{
	float A1, A2, A3, A4, A5, WnTs;
	float SLSOFBW;
	float SLSOFDR;
	float WnTs2;

	SLSOFBW = sVelLoopSecondOrderFilterStruct.SLVSOFBW;
	SLSOFDR = sVelLoopSecondOrderFilterStruct.SLVSOFDR;

	//  Wn*Ts calc
	SLSOFBW = 2*SLSOFBW;
	SLSOFBW = Pi*SLSOFBW;
	WnTs = SamplingTime*SLSOFBW;
	WnTs2 = WnTs*WnTs;
	A1 = SLSOFDR*WnTs;
	A1 = A1*4.0;

	A2 = 4+WnTs2;
	A3 = A2 - A1; //
	A3 = A3*InversSamplingTimeQuad;
	A4 = A2 + A1;
	A4 = A4*InversSamplingTimeQuad;
	A5 = ((2*WnTs2) - 8)*InversSamplingTimeQuad;
	A4=1/A4;

	sVelLoopSecondOrderFilterStruct.a2 = A3*A4;
	sVelLoopSecondOrderFilterStruct.a1 = A5*A4;
	sVelLoopSecondOrderFilterStruct.b0 = (SLSOFBW*SLSOFBW)*A4;
	sVelLoopSecondOrderFilterStruct.b1 = 2*sVelLoopSecondOrderFilterStruct.b0;
	sVelLoopSecondOrderFilterStruct.b2 = sVelLoopSecondOrderFilterStruct.b0;

}

#pragma CODE_SECTION(CalcNotchFilterParameters, "ramfuncs")
void CalcNotchFilterParameters(void)
{
	float A1, A2, WnTs;
	float QUALFACTOR;
	float SLVNATT;
	float SLVNFREQ;
	float SLVNWID;
	float WnTs2;
	float WnTs2DivQ;
	float WnTs2ATTDivQ;


	SLVNATT = sVelLoopNotchFilterStruct.SLVNATT;
	SLVNFREQ = sVelLoopNotchFilterStruct.SLVNFREQ;
	SLVNWID = sVelLoopNotchFilterStruct.SLVNWID;

	QUALFACTOR = SLVNFREQ/SLVNWID;

	SLVNFREQ = 2*SLVNFREQ;
	SLVNFREQ = Pi*SLVNFREQ;
	WnTs = SAMPLING_TIME*SLVNFREQ;
	WnTs2 = WnTs*WnTs;

	A2 = 2*WnTs/QUALFACTOR;
	WnTs2DivQ = A2;

	A2 = A2*SLVNATT;
	WnTs2ATTDivQ = A2;
	A2 = A2 + WnTs2;
	A2 = A2 + 4.0;
	A2 = 1/A2; //End of denumerator calculation

	A1 = WnTs2 + WnTs2DivQ;
	A1 = A1 + 4.0;

	sVelLoopNotchFilterStruct.b0 = A1*A2;

	A1 = WnTs2 - 4.0;
	sVelLoopNotchFilterStruct.b1 = A1*A2;

	sVelLoopNotchFilterStruct.a1 = sVelLoopNotchFilterStruct.b1;

	A1 = WnTs2 - WnTs2DivQ;
	A1 = A1 + 4.0;
	sVelLoopNotchFilterStruct.b2 = A1*A2;

	A1 = WnTs2 - WnTs2ATTDivQ;
	A1 = A1 + 4.0;
	sVelLoopNotchFilterStruct.a2 = A1*A2;

}



/*****************************************************************************
 * Function name: SetSofParameters
 *
 * Description: Set second order filter parameters (VLSOF)
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
/* TODO: orit - split into two functions - second order filter and notch filter */
void SetSofParameters(VL_FILTER_NAME fName,DF22 *filter[]/*, CAN_MSG_DATA Msg_Data*/)
{
	uint16_t TempPar = 0;
//	switch(Msg_Data.Byte7)
//	{
//	case VLSOF:
//		TempPar = Msg_Data.Byte1;
//		TempPar <<= 8;
//		TempPar = TempPar + Msg_Data.Byte2;
//		sVelLoopSecondOrderFilterStruct.SLVSOFBW = (float)TempPar;
//		sVelLoopSecondOrderFilterStruct.SLVSOFDR = 0.707;
//		CalcSofParametrs();
//		filter[fName]->a1 = sVelLoopSecondOrderFilterStruct.a1;
//		filter[fName]->a2 = sVelLoopSecondOrderFilterStruct.a2;
//		filter[fName]->b0 = sVelLoopSecondOrderFilterStruct.b0;
//		filter[fName]->b1 = sVelLoopSecondOrderFilterStruct.b1;
//		filter[fName]->b2 = sVelLoopSecondOrderFilterStruct.b2;
//		//filter[fName]->
//		break;
//	}
}

/*****************************************************************************
 * Function name: SetNotchFilterParameters
 *
 * Description: Set second order filter parameters (VLSOF)
 * Parameters:  None.
 * Returns:     None.
 ******************************************************************************/
void SetNotchFilterParameters(VL_FILTER_NAME fName,DF22 *filter[]/*, CAN_MSG_DATA Msg_Data*/)
{
//	uint16_t TempPar = 0;
//	switch(Msg_Data.Byte7)
//case VLNF:
//TempPar = Msg_Data.Byte1;
//TempPar <<= 8;
//TempPar = TempPar + Msg_Data.Byte2;
//sVelLoopNotchFilterStruct.SLVNATT = (float)TempPar;
//TempPar = Msg_Data.Byte3;
//TempPar <<= 8;
//TempPar = TempPar + Msg_Data.Byte4;
//sVelLoopNotchFilterStruct.SLVNFREQ = (float)TempPar;
//TempPar = Msg_Data.Byte5;
//TempPar <<= 8;
//TempPar = TempPar + Msg_Data.Byte6;
//sVelLoopNotchFilterStruct.SLVNWID = (float)TempPar;
//CalcNotchFilterParameters();
//filter[fName]->a1 = sVelLoopNotchFilterStruct.a1;
//filter[fName]->a2 = sVelLoopNotchFilterStruct.a2;
//filter[fName]->b0 = sVelLoopNotchFilterStruct.b0;
//filter[fName]->b1 = sVelLoopNotchFilterStruct.b1;
//filter[fName]->b2 = sVelLoopNotchFilterStruct.b2;
//break;
//}

}


/*
** ===================================================================
**     Function   :  EncoderPositionParser
**
**     Description :
**					Parser the incremental encoder current pozition and prepare it for sending
**     Parameters  :
**					int32_t *Position - pointer to the current position
**					struct _INC_ENCODER_DATA TxIncEnc
**     Returns     :
**					No returns
**     Notes       :
**
** ===================================================================
*/
#pragma CODE_SECTION(EncoderPositionParser, "ramfuncs")
void EncoderPositionParser( int32_t *Position, struct _ENCODER_DATA *TxIncEnc)
{
	int32_t TimePar;

	TimePar = *Position;
	//TimePar = sControlVarStruct.MotorPosition;
	//TimePar = IncEncPos;
	TxIncEnc -> TXEncoderData0 = (uint8_t)(TimePar & 0x00ff);
	TimePar >>= 8;
	TxIncEnc -> TXEncoderData1 = (uint8_t)(TimePar & 0x00ff);
	TimePar >>= 8;
	TxIncEnc -> TXEncoderData2 = (uint8_t)(TimePar & 0x00ff);
	TimePar >>= 8;
	TxIncEnc -> TXEncoderData3 = (uint8_t)(TimePar & 0x00ff);
}



/*
** ===================================================================
**     Function   :  EncoderPositionParser
**
**     Description :
**					Parser the incremental encoder current pozition and prepare it for sending
**     Parameters  :
**					int32_t *Position - pointer to the current position
**					struct _INC_ENCODER_DATA TxIncEnc
**     Returns     :
**					No returns
**     Notes       :
**
** ===================================================================
*/
#pragma CODE_SECTION(LoadCellDataParser, "ramfuncs")
void LoadCellDataParser( float LoadCellReadings, struct _LOAD_CELL_DATA *TxLoadCell)
{

	int16_t TimePar = 0;
	TimePar = LoadCellReadings* 100;
	//TimePar = med_ctr.my_struct_sensor_input.lc_pf ;
	TxLoadCell -> TXCellData0 = (uint8_t)(TimePar & 0x00ff);
	TimePar >>= 8;
	TxLoadCell -> TXCellData1 = (uint8_t)(TimePar & 0x00ff);
}


/*void SetPosAdjusterParameters()
{







}*/

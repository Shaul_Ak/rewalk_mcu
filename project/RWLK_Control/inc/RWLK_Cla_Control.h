/*
 * RWLK_Cla_Control.h
 *
 *  Created on: Jul 14, 2016
 *      Author: Arie
 */

#ifndef RWLK_CONTROL_INC_RWLK_CLA_CONTROL_H_
#define RWLK_CONTROL_INC_RWLK_CLA_CONTROL_H_

#include "DCL.h"



/*
struct X_MFLAGS_BITS {                    // bits description
    Uint16 ENABLE:1;                    // 0 Enable servo
    Uint16 OPEN_LOOP:1;                    // 1 If the bit is zero closed loop algorithm are calculated.If 0 open loop control
    Uint16 BRUSHLESS:1;                  // 2 Brushless motor connection, 0 - brushed motor connection
    Uint16 COMMUTATION:1;                  // 3 Commutation request
    Uint16 NOTCH_FILTER:1;                 // 4  - notch filter algorithm calculated
    Uint16 CURRENT_ADJUSTER:1;           // 5 Current Adjuster Request
    Uint16 VELOCITY_ADJUSTER:1;			// 6 Velocity Adjuster Request
    Uint16 POSITION_ADJUSTER:1;                     // 7 Reserved
    Uint16 RESET_COMMUT:1;                     // 8 Reset Commutation flag
    Uint16 RESET_FAULT:1; 				// 9 Reset all faults
    Uint16 SERVO_ANALYSYS:1;			//	10 Servo Analisys Start
    Uint16 CURRENT_LOOP:1;				// 11 CurrentLoop ENABLE
    Uint16 CALIBRATION_REQUEST:1;                     // 12 Reserved
    Uint16 PARAMETERS_CHANGE_REQUEST:1;			// 13 Parameters Change Request for control filters
    Uint16 rsvd1:2;                             // 14-15 Reserved
};


union X_MFLAGS_REG {
    Uint16  all;
    struct  X_MFLAGS_BITS  bit;
};


struct Y_MFLAGS_BITS {                    // bits description
    Uint16 ENABLE:1;                    // 0 Enable servo
    Uint16 OPEN_LOOP:1;                    // 1 If the bit is zero closed loop algorithm are calculated.If 0 open loop control
    Uint16 BRUSHLESS:1;                  // 2 Brushless motor connection, 0 - brushed motor connection
    Uint16 COMMUTATION:1;                  // 3 Commutation request
    Uint16 NOTCH_FILTER:1;                 // 4  - notch filter algorithm calculated
    Uint16 CURRENT_ADJUSTER:1;           // 5 Current Adjuster Request
    Uint16 VELOCITY_ADJUSTER:1;			// 6 Velocity Adjuster Request
    Uint16 POSITION_ADJUSTER:1;                     // 7 Reserved
    Uint16 RESET_COMMUT:1;                     // 8 Reset Commutation flag
    Uint16 RESET_FAULT:1; 				// 9 Reset all faults
    Uint16 SERVO_ANALYSYS:1;			//	10 Servo Analisys Start
    Uint16 CURRENT_LOOP:1;				// 11 CurrentLoop ENABLE
    Uint16 CALIBRATION_REQUEST:1;                     // 12 Reserved
    Uint16 PARAMETERS_CHANGE_REQUEST:1;			// 13 Parameters Change Request for control filters
    Uint16 rsvd1:2;                             // 14-15 Reserved
};

union Y_MFLAGS_REG {
    Uint16  all;
    struct  Y_MFLAGS_BITS  bit;
};


struct XCPUCLAINTERFACE_REGS {
    union   X_MFLAGS_REG           X_MFLAGS;			//CLACNTRWORD;                        // CLA Control Register
    union   Y_MFLAGS_REG           Y_MFLAGS;			//CLACNTRWORD;                        // CLA Control Register
};



//extern volatile struct MFLAGS_BITS xMotorFlags;
//struct MFLAGS_BITS xMotorFlags;

//union x_mflags_reg {
    //Uint16  all;
    //struct  XMFLAGS_BITS  bit;
//};

//union y_mflags_reg {
   // Uint16  all;
   // struct  MFLAGS_BITS  bit;
//};

//struct CPUCLAINTERFACE_REGS {
    //union   x_mflags_reg           X_MFLAGS;			//CLACNTRWORD;                        // CLA Control Register
    //union   y_mflags_reg           Y_MFLAGS;			//CLACNTRWORD;                        // CLA Control Register
//};




/*struct CPUCLAINTERFACE_REGS {
    union   X_MFLAGS_REG           X_MFLAGS;			//CLACNTRWORD;                        // CLA Control Register
    union   Y_MFLAGS_REG           Y_MFLAGS;			//CLACNTRWORD;                        // CLA Control Register
};
*/

struct MFLAGS_BITS {                    // bits description
    Uint16 ENABLE:1;                    // 0 Enable servo
    Uint16 OPEN_LOOP:1;                    // 1 If the bit is zero closed loop algorithm are calculated.If 0 open loop control
    Uint16 BRUSHLESS:1;                  // 2 Brushless motor connection, 0 - brushed motor connection
    Uint16 COMMUTATION:1;                  // 3 Commutation request
    Uint16 NOTCH_FILTER:1;                 // 4  - notch filter algorithm calculated
    Uint16 CURRENT_ADJUSTER:1;           // 5 Current Adjuster Request
    Uint16 VELOCITY_ADJUSTER:1;			// 6 Velocity Adjuster Request
    Uint16 POSITION_ADJUSTER:1;                     // 7 Reserved
    Uint16 RESET_COMMUT:1;                     // 8 Reset Commutation flag
    Uint16 RESET_FAULT:1; 				// 9 Reset all faults
    Uint16 SERVO_ANALYSYS:1;			//	10 Servo Analisys Start
    Uint16 CURRENT_LOOP:1;				// 11 CurrentLoop ENABLE
    Uint16 CALIBRATION_REQUEST:1;                     // 12 Reserved
    Uint16 PARAMETERS_CHANGE_REQUEST:1;			// 13 Parameters Change Request for control filters
    Uint16 rsvd1:2;                             // 14-15 Reserved
};

union MFLAGS_REG {
    Uint16  all;
    struct  MFLAGS_BITS  bit;
};

//union MFLAGS_REG xMotorFlags;
//union MFLAGS_REG yMotorFlags;

struct MOTORFLAGS_REGS {
    union   MFLAGS_REG           MFLAGS;			//CLACNTRWORD;                        // CLA Control Register
};





struct X_MFLAGS_BITS {                    // bits description
    Uint16 ENABLE:1;                    // 0 Enable servo
    Uint16 OPEN_LOOP:1;                    // 1 If the bit is zero closed loop algorithm are calculated.If 0 open loop control
    Uint16 BRUSHLESS:1;                  // 2 Brushless motor connection, 0 - brushed motor connection
    Uint16 COMMUTATION:1;                  // 3 Commutation request
    Uint16 NOTCH_FILTER:1;                 // 4  - notch filter algorithm calculated
    Uint16 CURRENT_ADJUSTER:1;           // 5 Current Adjuster Request
    Uint16 VELOCITY_ADJUSTER:1;			// 6 Velocity Adjuster Request
    Uint16 POSITION_ADJUSTER:1;                     // 7 Reserved
    Uint16 RESET_COMMUT:1;                     // 8 Reset Commutation flag
    Uint16 RESET_FAULT:1; 				// 9 Reset all faults
    Uint16 SERVO_ANALYSYS:1;			//	10 Servo Analisys Start
    Uint16 CURRENT_LOOP:1;				// 11 CurrentLoop ENABLE
    Uint16 CALIBRATION_REQUEST:1;                     // 12 Reserved
    Uint16 PARAMETERS_CHANGE_REQUEST:1;			// 13 Parameters Change Request for control filters
    Uint16 rsvd1:2;                             // 14-15 Reserved
};

union X_MFLAGS_REG {
    Uint16  all;
    struct  X_MFLAGS_BITS  bit;
};

struct Y_MFLAGS_BITS {
    Uint16 ENABLE:1;                    // 16 Enable servo
    Uint16 OPEN_LOOP:1;                    // 17 If the bit is zero closed loop algorithm are calculated.If 0 open loop control
    Uint16 BRUSHLESS:1;                  // 18 Brushless motor connection, 0 - brushed motor connection
    Uint16 COMMUTATION:1;                  // 19 Commutation request
    Uint16 NOTCH_FILTER:1;                 // 20  - notch filter algorithm calculated
    Uint16 CURRENT_ADJUSTER:1;           // 21 Current Adjuster Request
    Uint16 VELOCITY_ADJUSTER:1;			// 22 Velocity Adjuster Request
    Uint16 POSITION_ADJUSTER:1;                     // 23 Reserved
    Uint16 RESET_COMMUT:1;                     // 24 Reset Commutation flag
    Uint16 RESET_FAULT:1; 				// 25 Reset all faults
    Uint16 SERVO_ANALYSYS:1;			//	26 Servo Analisys Start
    Uint16 CURRENT_LOOP:1;				// 27 CurrentLoop ENABLE
    Uint16 CALIBRATION_REQUEST:1;                     // 28 Reserved
    Uint16 PARAMETERS_CHANGE_REQUEST:1;			// 29 Parameters Change Request for control filters
    Uint16 rsvd1:2;                             // 30-31 Reserved
};


union Y_MFLAGS_REG {
    Uint16  all;
    struct  Y_MFLAGS_BITS  bit;
};

struct CPUCLAINTERFACE_REGS {
    union   X_MFLAGS_REG           X_MFLAGS;			//CLACNTRWORD;                        // CLA Control Register
    union   Y_MFLAGS_REG           Y_MFLAGS;			//CLACNTRWORD;                        // CLA Control Register
};


struct X_MSTATUSWORD_BITS {                    // bits description
    Uint16 X_ENABLED:1;                    // 0 Enable servo
    Uint16 X_OPEN_LOOP:1;                    // 1 If the bit is zero closed loop algorithm are calculated.If 0 open loop control
    Uint16 X_BRUSHLESS:1;                  // 2 Brushless motor connection, 0 - brushed motor connection
    Uint16 X_COMMUT_IN_PROCESS:1;          // 3 Commutation process running
    Uint16 X_COMMUT_DONE:1;                 // 4  - notch filter algorithm calculated
    Uint16 X_FAULT_FLAG:1;				// 5 	FAULT flag bit
    Uint16 X_FAULT_NUMBER:4;                     // 9:6 FAULT number
    Uint16 X_PARAMETERS_CHANGED:1;                     // 10 Reserved
    Uint16 rsvd2:1;                     // 11 Reserved
    Uint16 rsvd3:1;						// 12 Reserved
    Uint16 rsvd4:1;						// 13 Reserved
    Uint16 rsvd5:1;						// 14 Reserved
    Uint16 rsvd6:1;						// 15 Reserved
    //Uint16 OVERCURRENT:1;                     // 6 Overcurrent bit
};


union X_MSTATUSWORD_REG {
    Uint16  all;
    struct  X_MSTATUSWORD_BITS  bit;
};

struct Y_MSTATUSWORD_BITS {                    // bits description
    Uint16 Y_ENABLED:1;                    // 0 Enable servo
    Uint16 Y_OPEN_LOOP:1;                    // 1 If the bit is zero closed loop algorithm are calculated.If 0 open loop control
    Uint16 Y_BRUSHLESS:1;                  // 2 Brushless motor connection, 0 - brushed motor connection
    Uint16 Y_COMMUT_IN_PROCESS:1;          // 3 Commutation process running
    Uint16 Y_COMMUT_DONE:1;                 // 4  - notch filter algorithm calculated
    Uint16 Y_FAULT_FLAG:1;				// 5 	FAULT flag bit
    Uint16 Y_FAULT_NUMBER:4;                     // 9:6 FAULT number
    Uint16 Y_PARAMETERS_CHANGED:1;                     // 10 Reserved
    Uint16 rsvd2:1;                     // 11 Reserved
    Uint16 rsvd3:1;						// 12 Reserved
    Uint16 rsvd4:1;						// 13 Reserved
    Uint16 rsvd5:1;						// 14 Reserved
    Uint16 rsvd6:1;						// 15 Reserved
    //Uint16 OVERCURRENT:1;                     // 6 Overcurrent bit
};

union Y_MSTATUSWORD_REG {
    Uint16  all;
    struct  Y_MSTATUSWORD_BITS  bit;
};


struct CLACPUINTERFACE_REGS {
	union X_MSTATUSWORD_REG			X_MFLAGS_CLASTATUS;							// CLA X motor status reg
	union Y_MSTATUSWORD_REG			Y_MFLAGS_CLASTATUS;							// CLA Y motor status reg
};


//extern volatile struct CPUCLAINTERFACE_REGS CpuClaCntrlRegs;
extern volatile struct CLACPUINTERFACE_REGS ClaCpuStatusRegs;

extern volatile struct MOTORFLAGS_REGS xMotorFlagsReg;
extern volatile struct MOTORFLAGS_REGS yMotorFlagsReg;
//extern volatile struct MOTORFLAGS_REGS* MotorFalgsRegsArray[];
//extern volatile struct XCPUCLAINTERFACE_REGS* CpuClaCntrlRegsArray[];
//extern volatile struct XCPUCLAINTERFACE_REGS MotorflagsRegs;


#endif /* RWLK_CONTROL_INC_RWLK_CLA_CONTROL_H_ */

/*
 * RWLK_ServoManager.h
 *
 *  Created on: 17 ����� 2016
 *      Author: orit.benhorin
 */

#ifndef RWLK_SERVOMANAGER_H_
#define RWLK_SERVOMANAGER_H_

#include "RWLK_Motor_Control.h"
#include "RWLK_Common_Par.h"
#include "RWLK_CLA_Control.h"

 typedef struct _ENCODER_DATA
 		{
 		uint8_t TXEncoderData0;
 		uint8_t TXEncoderData1;
 		uint8_t TXEncoderData2;
 		uint8_t TXEncoderData3;
 		}sEncData;
 typedef struct _LOAD_CELL_DATA
 		{
 		uint8_t TXCellData0;
 		uint8_t TXCellData1;
 		}sCellData;

void EncoderPositionParser( int32_t *Position, struct _ENCODER_DATA *TxIncEnc);
void LoadCellDataParser( float LoadCellReadings, struct _LOAD_CELL_DATA *TxLoadCell);


void RWLK_ServoManagerInit(void);

void SetPiGains(FILTER_NAME fName, PI *filter[], PI_FILTER_MEMBERS filterMember, uint32_t filterValue);
void SetClaPiGains(CLA_FILTER_NAME fName, PI_FILTER_MEMBERS filterMember, uint32_t filterValue, uint16_t AxisNumber);
void SetClaPidGains(CLA_FILTER_NAME fName, PID_FILTER_MEMBERS filterMember, uint32_t filterValue, uint16_t AxisNumber);
void SetClaSofPars(SOF_FILTER_MEMBERS filterMember, uint32_t filterValue);
void SetSofParametrs(VL_FILTER_NAME fName, uint16_t Axis);
void SetClaNfPars(NOTCH_FILTER_MEMBERS filterMember, uint32_t filterValue);
void MotorUnReleaseRequest(uint16_t Axis);
void MotorReleaseRequest(uint16_t Axis);
extern uint16_t MotorDisableRequest(uint16_t Axis);
void SetProfileParameters(POSITION_ADJUSTER_PARAMS pName,volatile struct ADJUSTER_PROFILE_PAR *ParametersStructArray,int32_t parameterValue);
void SetAdjusterRequest(ADJUSTER_REQUEST_PARAMETERS rName,volatile struct MOTORFLAGS_REGS* MotorFlagsRegistersArray,volatile struct ADJUSTER_PROFILE_PAR *ParametersStructArray, uint16_t AxisNumber);

#endif /* RWLK_SERVOMANAGER_H_ */

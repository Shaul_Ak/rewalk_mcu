/*
 * RWLK_Motor_Control.h
 *
 *  Created on: Jan 12, 2016
 *      Author: arie
 */

#ifndef RWLK_MOTOR_CONTROL_H_
#define RWLK_MOTOR_CONTROL_H_

#include "DCL.h"
#include "CLARKE_CLA.h"
#include "PARK_CLA.h"
#include "iPARK_CLA.h"
#include "SPEED_EST_CLA.h"
#include "SVGEN_CLA.h"
#include <stdint.h>
#include <stdbool.h>
#include <F2837xD_device.h>
#include "ExtFlash_Driver.h"


// Define the system frequency (MHz)
#define SYSTEM_FREQUENCY 200

// Define the ISR frequency (kHz)
#define ISR_FREQUENCY 20
#define INV_PWM_TICKS  ((SYSTEM_FREQUENCY/2.0)/ISR_FREQUENCY)*1000
#define INV_PWM_TBPRD INV_PWM_TICKS/2
#define INV_PWM_HALF_TBPRD INV_PWM_TICKS/4
#define PWM_DC	INV_PWM_HALF_TBPRD


#define WAITSTEP 	asm(" RPT #255 || NOP")

#define FILTERS_NUM 4

// Motor Data
#define PROFILE_STRUCTS_NUM 5
#define COMMON_CPU_PROFILES_NUM 2
#define XMOTOR_POLES_PAIRS   7  //12 11
#define XMOTOR_MAX_VELOCITY	10700
#define LENGTH_OF_SIN_TABLE	256 //512 // 128
#define XMOTOR_ENCODER_RESOLUTION  5000 //1000 //2048 //1600	4096 1024  //
#define XMOTOR_REV_PER_ELECTRICAL_PERIOD 	1/(float)XMOTOR_POLES_PAIRS
#define XTABLE_POINTS_NUM_PER_COUNT 		(LENGTH_OF_SIN_TABLE/(XMOTOR_REV_PER_ELECTRICAL_PERIOD*XMOTOR_ENCODER_RESOLUTION*4))
#define XMAXIMUM_PERMISSIBLE_TRAVEL_IN_COUNTS 2814530
#define XDEFAULT_MAX_MOVE_VELOCITY_IN_COUNTS_PER_SECOND	35536
#define BRUSHLESS_MOTOR	1
#define YMOTOR_POLES_PAIRS  7 //2
#define YMOTOR_ENCODER_RESOLUTION  5000 // 1000
#define YMOTOR_MAX_VELOCITY	10700
#define YMOTOR_REV_PER_ELECTRICAL_PERIOD 	1/(float)YMOTOR_POLES_PAIRS
#define YTABLE_POINTS_NUM_PER_COUNT 		(LENGTH_OF_SIN_TABLE/(YMOTOR_REV_PER_ELECTRICAL_PERIOD*YMOTOR_ENCODER_RESOLUTION*4))
#define YMAX_NUM_OF_COUNTS_PER_TICK 		((YMOTOR_MAX_VELOCITY/60*20000)*YMOTOR_ENCODER_RESOLUTION*4)
#define YMAX_NUM_TABLE_POINTS_PER_TICK		YTABLE_POINTS_NUM_PER_COUNT*YMAX_NUM_OF_COUNTS_PER_TICK
#define YMAXIMUM_PERMISSIBLE_TRAVEL_IN_COUNTS 2814530
#define YDEFAULT_MAX_MOVE_VELOCITY_IN_COUNTS_PER_SECOND	35536 //196608

#define NUMBER_OF_AVERAGED_SAMPLINGS	20
#define RMS_CURRENT_COEFFICIENT			0.75
#define INVERS_OF_NUMBERS_OF_AVERAGED_SAMPLINGS	0.05
#define CPU_RMS_CURRENT_COEFFICIENT			1.1547 //1.333333 //
//typedef unsigned char _Boolean;

// define motor faults
#define COMMON_FAULT	25
#define INCREMENTAL_ENCODER_DISCONNECTION	1
#define ABSOLUTE_ENCODER_MAGNET_LOSS		2
#define MOTOR_OVERCURRENT		3
#define POSITION_ERROR			4
#define VELOCITY_ERROR			5


#define	SOF_DEFAULTS { 4398.0f, 0.707f, 0.0356f, 0.0f, 0.0f, -1.7001f, 0.7357f } // Diffault values for second order filter 700Hz Bandwidth, 0.707 Damping ratio
#define	VLPF_DEFAULTS { 0.0356f, 0.0f, 0.0f, -1.7001f, 0.7357f, 0.0f, 0.0f }

typedef enum{A_DT=0, A_ACC, A_JERK, A_VEL, A_TARGET, A_CURRENT} POSITION_ADJUSTER_PARAMS;

typedef enum{F_DPI = 0, F_QPI, F_VPI, F_PPI} FILTER_NAME;
typedef enum{VLSOF = 5, VLNF} VL_FILTER_NAME;	// VLSOF - Second order filter, VLNF - Notch filter
typedef enum{CLAF_PPI = 1, CLAF_VPI, CLAF_DPI , CLAF_QPI, CLAVLSOF, CLAVLNF } CLA_FILTER_NAME;
//typedef enum{SOF_NATURAL_FREQ = 0, SOF_DAMPING_RATIO} VL_FILTER_NAME;
//typedef enum{KP = 0,KI, I10, UMIN, UMAX, I6} PI_FILTER_MEMBERS;
typedef enum{XCpuToClaProfileVars=0, XCpuToClaAdjusterFirstMove, XCpuToClaAdjusterSecondMove, YCpuToClaProfileVars, YCpuToClaAdjusterFirstMove, YCpuToClaAdjusterSecondMove} PROFILE_STRUCT_MEMBERS;

extern int16_t CLAPWM_DC;
extern float CurrentCommandTemporary;
extern float yk;
extern float rk;
extern float uk;
extern float Vk;
extern float Vk_1;
extern float Ik;
extern float Ik_1;
extern float fCurrentCommand;
extern int16_t iCurrentCommand;
extern int16_t OutputCurrent;
extern float fVelocityCommand;
extern int16_t iVelocityCommand;
extern int32_t AdjusterSwitch;
extern int32_t AdjusterCounter;
extern uint32_t *CLA_TABLE_ADDRESS;
extern int32_t CLA_TABLE_DATA ;
extern int16_t INDEX_A_PHASE;
extern int16_t INDEX_B_PHASE;
extern int16_t INDEX_C_PHASE;
extern int16_t PHASE_ADVANCE;
extern int16_t X_PHASE_ADVANCE;
extern int16_t Y_PHASE_ADVANCE;
extern float XPhaseNumberOfPointsPerCount;
extern float YPhaseNumberOfPointsPerCount;
extern int16_t Brshl_Command_A;
extern int16_t Brshl_Command_B;
extern int16_t Brshl_Command_C;
extern int16_t XBrshl_Command_A;
extern int16_t XBrshl_Command_B;
extern int16_t XBrshl_Command_C;
extern int16_t YBrshl_Command_A;
extern int16_t YBrshl_Command_B;
extern int16_t YBrshl_Command_C;
extern float TABLE_VALUE_A;
extern float TABLE_VALUE_B;
extern float TABLE_VALUE_C;
extern int16_t AchannelConversionTime;
extern int16_t BchannelConversionTime;
extern int16_t AD_ConvertionTime;
extern int16_t ADCINT1;
extern int16_t ADCINT2;
extern uint32_t	RotorPosition;


//extern int16_t AdcaResult0;
//extern int16_t AdcbResult0;
//extern int16_t AdccResult0;
extern int16_t CPU_PHASE_ADVANCE;
extern int16_t XZeroCalibratedAchannel;
extern int16_t XZeroCalibratedBchannel;
extern int16_t XZeroCalibratedCchannel;
extern int16_t YZeroCalibratedAchannel;
extern int16_t YZeroCalibratedBchannel;
extern int16_t YZeroCalibratedCchannel;
extern int16_t XBRLS_PWM ;
extern int16_t YBRLS_PWM ;
extern int16_t BRLS_PWM ;
//extern int16_t BRLS_PWMA;
//extern int16_t BRLS_PWMB;
//extern int16_t BRLS_PWMC;
//extern uint16_t AdjustingCurrent;
//extern uint16_t AdjustingVelocity;

extern int32_t CheckCounter;
extern int32_t IncrementalEncoderPosition;
extern int32_t FeedbackEncoderPosition;
extern float *ClaSinTableBaseAddressPointer;
extern float *CurrentSinAddressPointer;
extern float CommutationFactor;
extern float XCommutationFactor;
extern float YCommutationFactor;
extern uint16_t EncoderResolution;
extern uint16_t XEncoderResolution;
extern uint16_t YEncoderResolution;
extern uint16_t TablePointsNumber;
extern uint16_t PiDiv3TablePointsNumber;
extern uint16_t PiDiv2TablePointsNumber;
extern uint16_t XPolePairsNumber;
extern uint16_t YPolePairsNumber;
extern int16_t CPU_Brshl_Command_A;
extern int16_t CPU_Brshl_Command_B;
extern int16_t CPU_Brshl_Command_C;
extern bool MotorOnFlag;


extern int32_t IncEncPos;
extern int32_t AbsEncPos;
extern int32_t XAbsEncPos;
extern int32_t YAbsEncPos;
extern int32_t xCPUcounter;
extern int32_t yCPUcounter;
extern float SamplingTimeQuad;
extern float InversSamplingTimeQuad;
extern float SamplingTime;
extern uint16_t Drv8031DataReg1;
extern uint16_t Drv8031DataReg2;


// CLA1 TO CPU1 VARIABLES
//**************************************************************
extern int32_t MotorRevolutionsCounter;
extern int32_t XMotorRevolutionsCounter;
extern int32_t YMotorRevolutionsCounter;
//**************************************************************

// RAMDATA VARIABLES ( Only CPU1 can access the variables)
//extern _iq SinArgument;

extern bool StartTick;
extern bool PositionAdjusterRequestFlag;
extern uint16_t XCommutState;
extern uint16_t YCommutState;
extern float CPU_TABLE_VALUE_A;
extern float CPU_TABLE_VALUE_B;
extern float CPU_TABLE_VALUE_C;
//extern int16_t CPU_Brshl_Command_A;
//extern int16_t CPU_XBrshl_Command_B;
//extern int16_t CPU_Brshl_Command_C;
//extern uint16_t	CPU_RotorPosition;
//extern int32_t NextDesiredPosition;


struct INTERFACE_BITS {                    // bits description
    Uint16 ABSOLUTE_PTP_MOVE:1;            // 0 Absolute point to point move.
    Uint16 RELATIVE_PTP_MOVE:1;            // 1 Relative point to point move.
    Uint16 rsvd1:1;                  // 2 Brushless motor connection, 0 - brushed motor connection
    Uint16 rsvd2:1;                  // 3 Commutation request
    Uint16 rsvd3:1;                 // 4  - notch filter algorithm calculated
    Uint16 rsvd4:1;           // 5 Current Adjuster Request
    Uint16 rsvd5:1;			// 6 Velocity Adjuster Request
    Uint16 rsvd6:1;                     // 7 Reserved
    Uint16 rsvd7:3;                     // 10:8 Reserved
    Uint16 rsvd8:3;                     // 13:11 Reserved
    Uint16 rsvd9:2;                     // 15:14 Reserved
};

union INTERFACE_REG {
    Uint16  all;
    struct  INTERFACE_BITS  bit;
};

struct INFMCUINTERFACE_REGS {
    union   INTERFACE_REG           INFCPUINTERFACE;                        // CLA Control Register
};

typedef struct  {
				  float  d;		    	// Input : d
				  float  q;	    		// Input : q
				  float  sine;			// Input : sin(theta)
				  float  cosine;			// Input : cos(theta)
				  float  alpha;			// Output  : alpha
				  float  beta;		    // Output  : beta
				}i_PARK_CLA;

typedef struct {
				  float  alpha;			// Input : alpha
				  float  beta;		    // Input : beta
				  float  theta; 		// Input : theta
				  float  d;		    	// Output : d
				  float  q;	    		// Output : q
				  float  sine;			// Output : sin(theta)
				  float  cosine;		// Output : cos(theta)
				}_PARK_CLA;

#define _iPARK_CLA_MACRO(v)													\
	v.alpha =v.d * v.cosine - v.q*v.sine;									\
	v.beta  =v.d * v.sine + v.q*v.cosine;

#define _PARK_CLA_MACRO(v)													\
	v.d = v.alpha * v.cosine + v.beta  * v.sine;								\
	v.q = v.beta  * v.cosine - v.alpha * v.sine;

#define _PARK_CLA_INIT(v)													\
	v.cosine=0.0;																\
	v.sine=0.0;																\
	v.d=0.0;																\
	v.q=0.0;																\
	v.theta=0.0;															\
	v.alpha=0.0;															\
	v.beta=0.0;

#define _iPARK_CLA_INIT(v)													\
	v.cosine=0.0;																\
	v.sine=0.0;																\
	v.d=0.0;																\
	v.q=0.0;																\
	v.alpha=0.0;															\
	v.beta=0.0;



typedef struct NOTCH_FILTER_PAR
{
	float SLVNATT;	// Notch filter attenuation factor.
	float SLVNWID;	// Notch Filter width (diference between to -3db attenuations in Hz
	float SLVNFREQ; // Notch Filter Frequency Reduction	in Hz
	float b0;
	float b1;
	float b2;
	float a1;
	float a2;
}sNotchFilterStruct;


typedef struct SECOND_ORDER_FILTER_PAR
{
	float SLVSOFBW; //SOF Bandwidth in Hz. By diffault 700Hz
	float SLVSOFDR; //SOF Damping ratio. By diffult 0.707
	float b0;
	float b1;
	float b2;
	float a1;
	float a2;
}sSecondOrderFilterStruct;



typedef struct MOTOR_CURRENT_DATA
{
	float XAxisRMSCurrent;
	float XAxisTempCurrent;
	float YAxisRMSCurrent;
	float YAxisTempCurrent;
	float As;
	float Bs;
	float Cs;
	uint16_t RMSTimer;
}CurrentMotorData;


				typedef struct CLA_PROFILE_VAR
				{
					int32_t ProfileCounter;
					int32_t AbsoluteEncoderPosition;
					float AbsoluteEncoderFeedback;

				}mClaProfileVarStruct;

				typedef struct _P2MOVE_VAR
				{
					int32_t MaxPosError;
					int32_t DesiredPosition;
					//int32_t FeedbackPosition;
					int32_t PreviousDesiredPosition;

					double MotionDistance;
					double ProfileMaxVelocity;
					double Vmax;
					double ProfileAcceleration;
					double ProfileJerk;
					double MotionTime;
					double AccelerationFactor;
					double JerkFactor;
					double AccelerationTime;
					double JerkTime;

					int16_t MovementFlag;
					int16_t TriangularProfileFlag;
					int32_t FinalTargetPoint;
					int32_t StartTargetPoint;
					int32_t StartServo;
					bool    Pretension;

				}pToMoveVarStruct;

				typedef struct ADJUSTER_PROFILE_PAR
				{
					uint32_t DwellTime;
					uint32_t DwellCounter;
					float AdjusterProfileAcc;
					float AdjusterProfileJerk;
					float AdjusterProfileVelocity;
					int32_t AdjusterCurrentCommand;
					int32_t AdjusterVelocityCommand;
					int32_t AdjusterProfileTarget;
					uint32_t AdjusterSwitch;
					uint16_t AdjusterRequestFlag;
				}pProfilePars;


				typedef struct CPU_PROFILE_VAR
				{
					double ProfileVelocity;
					double ProfileAcceleration;
					double ProfileDeceleration;
					double ProfileJerk;
					uint16_t TriangularProfileFlag;
					uint32_t PT;		// Profile Time
					uint32_t CVT;		// Constant Velocity Time
					uint32_t AT;		// Acceleration Time
					uint32_t N1;		// The required time for first part of profiler
					uint32_t N2;		// The required time for second part of profiler
					uint32_t N3;		// 3 part
					uint32_t N4;		// 4th part required time
					uint32_t N5;		// 5th part of required time
					uint32_t N6;		// 6th part of required time
					uint32_t N7;		// 7th part of required time
				}mCpuProfileVarStruct;

				typedef struct COMMON_CPU_PROFILE_VAR
				{
				double Jn; // Calculated profile jerk for every cycle of profile calculation
				double An; // Calculated profile acceleration for every cycle of profile calculation
				double Vn; // Calculated profile velocity for every cycle of profile calculation
				double Sn; // Calculated profile position for every cycle of profile calculation
				int16_t TargetRadius;
				uint32_t TargetRadiusCounter;
				uint32_t TargetRadiusSettlingTime;
				int32_t  NextTargetPosition;
				uint32_t ProfileCounter;
				uint16_t ProfilerSwitch;
				uint16_t ProfileEnd;
				uint16_t CheckSettling;
				uint16_t CalibrationSwitch;
				uint16_t CalibrationCounter;
				uint32_t MaxCalibrationTime;
				float XAxisMotorRmsCurrent;
				float XAxisMotorPeakCurrent;
				float XAxisMotorMaxPermitCurrent;
				float YAxisMotorRmsCurrent;
				float YAxisMotorPeakCurrent;
				float YAxisMotorMaxPermitCurrent;
				float EndOfAccelerationProfilePosition;
				float EndOfConstantVelocityProfilePosition;
				float AccelerationPeakCurrent;
				float ConstantVelocityPeakCurrent;
				float DecelerationPeakCurrent;
				uint16_t ProfileStackSection;
				bool OvercurrentFaultFlag;
				}sCommonPars;


				typedef struct CPU_TO_CLA_PROFILE_VARS
				{
					int32_t  PositionCommand;
					int32_t  VelocityFeedforwardCommand;
					int32_t  AccelerationFeedforwardCommand;
					int32_t  CurrentAdjusterCommand;
					int32_t  AbsoluteEncoderFeedback;
					uint16_t AdjusterRequestFlag;
				}sCpuToClaProfileVars;




				typedef struct _CONTROL_VAR
				{

					//int32_t XDesiredPosition;
					//int32_t XFeedbackPosition;
					//int32_t XPositionError;
					//int32_t XPreviousFeedbackPosition;
					//int32_t XAbsEncoderData;
					//int32_t XAbsEncoderPosition;
					int32_t XIncrementalEncoderData;
					int32_t YIncrementalEncoderData;
					//int32_t XPreviousIncrementalEncoderData;
					int32_t XMotorPosition;
					int32_t YMotorPosition;
					int32_t XPreviousMotorPosition;
					int32_t YPreviousMotorPosition;
					int32_t XMotorVelocity;
					int32_t YMotorVelocity;
					//int32_t XVelocityError;
					int16_t iFeedbackToDAC;
					int16_t iCommandToDAC;
					//uint16_t XAdjusterRequestFlag;
					float XVelocityCommand;
					float YVelocityCommand;
					float XVelocityFeedback;
					float YVelocityFeedback;
					float XPositionCommand;
					float YPositionCommand;
					float XPositionFeedback;
					float YPositionFeedback;
					float XCurrentCommand;
					float YCurrentCommand;
					float XCurrentFeedback;		// Only Cla use
					float YCurrentFeedback;		// Only Cla use
					int16_t XAxis_APhaseCurrentFeedback;
					int16_t XAxis_BPhaseCurrentFeedback;
					int16_t XAxis_CPhaseCurrentFeedback;
					int16_t YAxis_APhaseCurrentFeedback;
					int16_t YAxis_BPhaseCurrentFeedback;
					int16_t YAxis_CPhaseCurrentFeedback;
					//float XAPhaseCommand;
					//float XBPhaseCommand;
					//float XCPhaseCommand;
					float XRMSCurrent;
					float YRMSCurrent;
					//uint16_t XRMSTimer;
					//float XTempRMSCurrent;
				}cControlVar;
				//cControlVar sControlVarStruct;

				typedef struct _MOTOR_CONTROL_VAR{
					int32_t IncrementalEncoderData;
					int32_t MotorFeedbackPosition;
					int32_t RelativeFeedbackPosition;
					int32_t PreviousIncrementalEncoderData;
					int32_t MotorPosition;

				}mControlVars;

				typedef struct _SERVO_PARAMETERS
				{
					uint32_t Length;
					uint32_t SubSectorNumber;
					uint32_t My_Addr;
					ExFlash_ReturnStatus ret;
					uint8_t WriteData[5];
					uint8_t ReadData[5];
					bool WriteEnableFlag;

				}sServoParStruct;


				extern volatile struct MOTOR_CURRENT_DATA claMotorCurrentsData;
				extern volatile struct CPU_TO_CLA_PROFILE_VARS X_sCpuToCla1ProfileCommandsStruct;
				extern volatile struct CPU_TO_CLA_PROFILE_VARS Y_sCpuToCla1ProfileCommandsStruct;
				extern volatile struct CPU_TO_CLA_PROFILE_VARS* sCpuToCla1ProfileCommandsStructsArray[];
				extern volatile struct MOTORFLAGS_REGS* MotorFalgsRegsArray[];
				extern volatile struct COMMON_CPU_PROFILE_VAR X_sCommonCpuProfileParametersStruct;
				extern volatile struct COMMON_CPU_PROFILE_VAR Y_sCommonCpuProfileParametersStruct;
				extern volatile struct COMMON_CPU_PROFILE_VAR *CommonCpuProfileParametersStructsArray[];
				extern volatile struct _MOTOR_CONTROL_VAR X_sMotorControlVariablesStruct;
				extern volatile struct _MOTOR_CONTROL_VAR Y_sMotorControlVariablesStruct;
				extern volatile struct _MOTOR_CONTROL_VAR *sMotorControlVariablesStructsArray[];
				extern volatile struct CPU_PROFILE_VAR XcCpuToClaProfileVarStruct;
				extern volatile struct CPU_PROFILE_VAR XFirstTargetPointProfileVarStruct;
				extern volatile struct CPU_PROFILE_VAR XSecondTargetPointProfileVarStruct;
				extern volatile struct CPU_PROFILE_VAR YcCpuToClaProfileVarStruct;
				extern volatile struct CPU_PROFILE_VAR YFirstTargetPointProfileVarStruct;
				extern volatile struct CPU_PROFILE_VAR YSecondTargetPointProfileVarStruct;
				extern volatile struct CPU_PROFILE_VAR* ProfileVarStructsArray[];
				extern volatile struct CPU_PROFILE_VAR*	ProfileVarStructsNum[PROFILE_STRUCTS_NUM];
				extern volatile struct CLA_PROFILE_VAR cClaProfileVarStruct;
				extern volatile struct _P2MOVE_VAR X_pToMoveVariablesStruct;
				extern volatile struct _P2MOVE_VAR Y_pToMoveVariablesStruct;
				extern volatile struct _P2MOVE_VAR*  pToMoveVarStructsArray[];
				extern volatile struct _CONTROL_VAR sControlVarStruct;
				extern volatile struct ADJUSTER_PROFILE_PAR pAdjusterParametersStruct;
				extern volatile struct ADJUSTER_PROFILE_PAR XpAdjusterParametersStruct;
				extern volatile struct ADJUSTER_PROFILE_PAR YpAdjusterParametersStruct;
				extern volatile struct ADJUSTER_PROFILE_PAR* pAdjusterParametersStructsPointer[];
				extern volatile struct NOTCH_FILTER_PAR	sVelLoopNotchFilterStruct;
				extern volatile struct SECOND_ORDER_FILTER_PAR	sVelLoopSecondOrderFilterStruct;
				extern volatile struct SECOND_ORDER_FILTER_PAR	XAxis_VelLoopSecondOrderFilterStruct;
				extern volatile struct SECOND_ORDER_FILTER_PAR	YAxis_VelLoopSecondOrderFilterStruct;
				extern struct _SERVO_PARAMETERS	sServoParametersStruct;
extern PI xppi;
extern PI xppi_cla;
extern PI xvpi1;
extern PI xvpi1_cla;
extern DF22 vlsof;
extern DF22 xvlpf;
extern DF22 xvlpf_cla;
extern DF22 xvlnf;
extern DF22 xvlnf_cla;
extern PI xdpi1;
extern PI xqpi1;
extern PI xdpi1_cla;
extern PI xqpi1_cla;
extern PI yppi;
extern PI yppi_cla;
extern PI yvpi1;
extern PI yvpi1_cla;
extern PID yvpid1_cla;
extern DF22 yvlpf;
extern DF22 yvlpf_cla;
extern DF22 yrmslpf_cla;
extern DF22 yvellpf_cla;
extern DF22 yvlnf;
extern DF22 yvlnf_cla;
extern PI ydpi1;
extern PI yqpi1;
extern PI ydpi1_cla;
extern PI yqpi1_cla;
extern PI* filters[FILTERS_NUM];
extern PI *pif[];
extern DF22 *xsof[];
extern DF22 *ysof[];
extern SPEED_ESTIMATION_CLA x_speed_estimation;
extern SPEED_ESTIMATION_CLA y_speed_estimation;
extern CLARKE_CLA clarke1;
extern _PARK_CLA park1;
extern i_PARK_CLA ipark1;
extern SVGEN_CLA svgen1;



extern void ControlWordCheck(void);
extern int32_t ProfileCalc(volatile struct _P2MOVE_VAR *pToMoveStructArray, volatile struct CPU_PROFILE_VAR *CpuProfilesArray,volatile struct COMMON_CPU_PROFILE_VAR *CpuProfileParametersStructsArray, uint16_t AxisNumber);
extern void ProfileParsPrepareToMove(volatile struct _P2MOVE_VAR* pToMoveStruct, volatile struct CPU_PROFILE_VAR *CpuProfilesArray,volatile struct COMMON_CPU_PROFILE_VAR *CpuProfileParametersStructsArray);
extern void SinTableCalc(void);
extern void CalcCommutCommands(void);
extern uint16_t X_AxisServoOff(void);
extern uint16_t Y_AxisServoOff(void);
extern uint16_t X_AxisServoOn(void);
extern uint16_t Y_AxisServoOn(void);
extern void RWLK_MotorControlInit(void);
extern void CommutCheck(volatile struct MOTORFLAGS_REGS* MotorFalgsRegitersArray[], uint16_t *CommutIndex);
//extern void CurrentLoopAdj(uint16_t Period, uint16_t CurrentAmplitude);









#endif /* RWLK_MOTOR_CONTROL_H_ */

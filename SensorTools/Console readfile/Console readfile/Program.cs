﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;
using System.ComponentModel;
using System.Windows;
using System.Windows.Forms;
using System.Collections;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Diagnostics;

namespace Console_readfile
{

    public class TextFromFile
    {
        Communication CanCommunication = new Communication();
        private void FileConverter(TextFromFile text)
        {
            StreamReader sr;
            OpenFileDialog openFile = new OpenFileDialog();

            string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments).ToString(), "ReWalk\\TestReverse1.txt");
            var fileData = new StringBuilder();

            string[] Line = new String[30];
           
            openFile.FileName = "Test.txt";
            openFile.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments).ToString();
            openFile.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            if (openFile.ShowDialog() == DialogResult.OK)
            {
                sr = new StreamReader(openFile.FileName);
            }
            else
                sr = new StreamReader(Path.Combine(openFile.InitialDirectory, openFile.FileName));

            while (!sr.EndOfStream)
            {
                // Split each line to array of cells;
                Line = sr.ReadLine().Split(',');

                int input = Convert.ToInt32(Line[2], 16);
                if (input != 0)
                {
                   // var num = new SoapHexBinary(input.).Reverse().ToArray()).ToString();
                    var num = new SoapHexBinary(SoapHexBinary.Parse(Line[2]).Value.Reverse().ToArray()).ToString();
                    var newLine = string.Format("{0}", num);
                    fileData.AppendLine(newLine);
                }
            }

                File.WriteAllText(path, fileData.ToString());

        }

        private void CreateTestfile(TextFromFile text)
        {
            long i = 0;
            int[] id = new int[12] { 0x141, 0x142, 0x143, 0x144, 0x145, 0x146, 
                               0x151, 0x152 , 0x153,0x154, 0x155 , 0x156};
            string zeros = "0000";
            int[] arr = new int[0];
            string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments).ToString(), "ReWalk\\TestInvert1.txt");
            var csv = new StringBuilder();

            for (i = 0; i <= 0xFFFF; ++i)
            {
                string input = string.Concat(i.ToString("X").PadLeft(4, '0'),zeros);
                var num = new SoapHexBinary(SoapHexBinary.Parse(input).Value.Reverse().ToArray())
                                                .ToString();
                var newLine = string.Format("{0:X},{1},{2},",  id[i % 12],833*i,num);
                csv.AppendLine(newLine);  
            }

            File.WriteAllText(path, csv.ToString());
        }

        public void Readfile(TextFromFile text1)
        {
            StreamReader sr;
            OpenFileDialog openFile = new OpenFileDialog();

            int id = 0;
            int newTime = 0, oldTime = 0, delay = 0;
            Stopwatch Timer1 = new Stopwatch();
            Stopwatch TimerTotal = new Stopwatch();

            byte[] fixBytes = new byte[8];
            byte[] newData = new byte[1];
          
            string[] Line = new String[30];

            int cellSize = 4;
            int iterationNum;
            uint cell = 0;
            int i,j;
            int sendCounter = 0;
            char repeatFlag;


            openFile.FileName = "Test.csv"; //"Records.csv"; // "data4.txt"; 
            //openFile.InitialDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments).ToString(), "ReWalk");
            openFile.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments).ToString();
            openFile.Filter = "CSV files (*.csv)|*.csv|Text files (*.txt)|*.txt|All files (*.*)|*.*";
            if (openFile.ShowDialog() == DialogResult.OK)
            {
                sr = new StreamReader(openFile.FileName);
            }
            else 
                sr = new StreamReader(Path.Combine(openFile.InitialDirectory,openFile.FileName));

            // Define Cell size:
            Console.WriteLine("Enter cell data size in bytes: ");
            cellSize = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter max num of iteration: ");
            iterationNum = Convert.ToInt32(Console.ReadLine());

        
            TimerTotal.Start();
            for (j = 0; j < iterationNum; j++)
            {
                Timer1.Reset();
                Timer1.Start();
                sendCounter = 0;
                // Start Read file lines
                while (!sr.EndOfStream)
                {
                    sendCounter++;
                    // Split each line to array of cells;
                    Line = sr.ReadLine().Split(',');

                    id = Convert.ToInt32(Line[cell], 16);
                    cell++;
                    newTime = Convert.ToInt32(Line[cell], 10);
                    cell++;

                    Array.Clear(fixBytes, 0, fixBytes.Length);

                    for (i = 0; i < 8; i += cellSize, cell++)
                    {
                        try
                        {
                            while (Line[cell].Length < 2)
                            {
                                Line[cell] = "0" + Line[cell];
                            }
                            newData = SoapHexBinary.Parse(Line[cell]).Value;
                        }
                        catch (Exception e)
                        {
                            Array.Clear(newData, 0, cellSize);
                            Console.WriteLine("Exception caught.");
                            Console.ReadKey();
                        }

                        newData.CopyTo(fixBytes, i);
                    }


                    text1.CanCommunication.SendData(id, fixBytes);

                    delay = newTime - oldTime;
                    text1.CanCommunication.DoMicroDelay(delay);
                    oldTime = newTime;

                    cell = 0;
                }
                Timer1.Stop();
                sr.DiscardBufferedData();
                sr.BaseStream.Seek(0, System.IO.SeekOrigin.Begin);
                Console.WriteLine("Interation Num: {0}, Totol Sends:{1}, Elapsed={2}. \nPress 'x' to Exit or Enter to repeat", j, sendCounter,Timer1.Elapsed);
                
                repeatFlag = (char)Console.Read();
                if (repeatFlag == 'x')
                    break;
            }

          
            sr.Close();
        }

        public bool Connect_and_askuser(TextFromFile text1)
        {


            if (text1.CanCommunication.ConnectToReWalk())
            {
                Console.WriteLine("This program simulate sensor data by using record data file \n");
                // Console.WriteLine("Are you sure?(y/n)");
                // ch = (char)Console.Read();
                // if (ch == 'y')
                return true;
                // else
                //    return false;
            }
            else
            {
                Console.WriteLine("Press any key to exit!");
                Console.ReadKey();
                return false;
            }
        }

        public void Timer(TextFromFile text1)
        {
            Stopwatch Timer = new Stopwatch();
            Timer.Start();
            CanCommunication.DoMicroDelay(833);
            //CanCommunication.DoDelay(10);
            Timer.Stop();
            Console.WriteLine("Delay time: {0}", Timer.Elapsed);
            Console.ReadKey();
        }


        [STAThread]
        public static void Main(String[] args)
        {
            TextFromFile text = new TextFromFile();



            text.Connect_and_askuser(text);
            text.Readfile(text);
            //text.CreateTestfile(text);
            //text.FileConverter(text);
            //text.Timer(text);
        }
    }
}

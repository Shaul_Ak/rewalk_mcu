﻿using System;
using System.Collections.Generic;

using System.Text;

namespace VcanCsharp
{
    class CAN_BAS_CONNECTION
    {
        private icsSpyMessage[] stMessages;
        private delay delay_;
        public int m_hObject;
        private icsSpyMessage stMessagesTx = new icsSpyMessage();
        private byte[] bSCPIDs = new byte[256];
        private bool connected = false;
        private int messageNum = 20;
        public CAN_BAS_CONNECTION()
        {
            delay_ = new delay();
            stMessages = new icsSpyMessage[messageNum];

        }
        private void ErrorMessage()
        {
            System.Windows.Forms.MessageBox.Show("some connection problem occured");
        }

        private void resetMessageBox()
        {
            for (int i = 0; i < messageNum; i++)
            {
                stMessages[i].Data2 = 0;
            }
        }



        public bool sendData(COMMAND _command, PARAMETER_NUMBER index, int parameter)
        {


            int iResult;    //Storage for the Result from function call
            Byte paramLow, paramHigh;
            resetMessageBox();
            delay_.doDelay(1);
            //Storage for TXMessage
            paramLow = (Byte)parameter;
            paramHigh = (Byte)(parameter >> 8);
            //Set the ArbID information for TX message
            stMessagesTx.ArbIDOrHeader = 816;
            //          stMessagesTx.ColorID = (Byte) 330;



            //            stMessagesTx.
            stMessagesTx.NumberBytesData = 8;
            stMessagesTx.Data1 = (Byte)_command;
            stMessagesTx.Data2 = (Byte)index;
            stMessagesTx.Data3 = paramLow;
            stMessagesTx.Data4 = paramHigh;
            stMessagesTx.Data5 = 0x48;
            stMessagesTx.Data6 = 0;
            stMessagesTx.Data7 = 0;
            stMessagesTx.Data8 = 0;
            //Clear the Status BitFields
            stMessagesTx.StatusBitField = 0;
            stMessagesTx.StatusBitField2 = 0;
            //Call the Tx funciton
            iResult = icsNeoDll.icsneoTxMessages(m_hObject, ref stMessagesTx, 1, 0);

            if (iResult != 1)
            {
                ErrorMessage();
                return false;
            }

            resetMessageBox();
            return true;

        }

        public bool Export_Parameter(PARAMETER_NUMBER index, int parameter)
        {

            long lResult;   //Storage the Result fo the function call
            long lCount = 0;				//Counter var for loops
            int lNumberOfMessages = 0;   //Storage for the number of messages
            int lNumberOfErrors = 0;
            bool ack = false;

            sendData(COMMAND.REWALK_UPDATE_PARAMETER, index, parameter);

            while (ack == false)
            {
                lResult = icsNeoDll.icsneoGetMessages(m_hObject, ref stMessages[0], ref lNumberOfMessages, ref lNumberOfErrors);
                if (lResult != 1)
                {
                    ErrorMessage();
                    return false;
                }

                for (lCount = 1; lCount <= lNumberOfMessages; lCount++)
                {
                    if (stMessages[lCount - 1].Data2 == (int)PARAMETER_NUMBER.ACKNOWLEDGE)
                    {
                        ack = true;
                        break;
                    }
                }
            }
            
            
            return true;
        }

        public int unsignedShortToInt(Byte b_0, Byte b_1)
        {
            short i = 0;
            i |= (short)b_0;
            i <<= 8;
            i |= (short)b_1;

            int j = i & 0x8000;

            if (j > 0 && i > 5)
            {
                return ~i + 1;
            }

            return i;
        }

        public int Import_Parameter(COMMAND command_, PARAMETER_NUMBER index)
        {

            long lResult;   //Storage the Result fo the function call
            long lCount = 0;				//Counter var for loops
            int lNumberOfMessages = 0;   //Storage for the number of messages

            int lNumberOfErrors = 0;
            bool ack = false;
            int paramVal = 0;

            

            sendData(command_, index, 0);
           
           

            while (ack == false){
                lResult = icsNeoDll.icsneoGetMessages(m_hObject, ref stMessages[0], ref lNumberOfMessages, ref lNumberOfErrors);

                for (lCount = 1; lCount <= lNumberOfMessages; lCount++)
                {

                    if (stMessages[lCount - 1].Data2 == (int)PARAMETER_NUMBER.ACKNOWLEDGE)
                    {
                        ack = true;
                        break;
                    }


                    if ((Byte)index == stMessages[lCount - 1].Data2)
                    {

                        paramVal = unsignedShortToInt(stMessages[lCount - 1].Data4, stMessages[lCount - 1].Data3);
                        //paramVal = stMessages[lCount - 1].Data4;
                        //paramVal = ((Byte)paramVal << 8) + stMessages[lCount - 1].Data3;

                    }
                }
            }
            resetMessageBox();
            return paramVal;
        }





        public bool ConnectToReWalk()
        {
            byte bNetworkIDs = 1;
            // Array of SCP functional IDs passed to the driver
            int iPortNumber = 0;
            int iReturnVal = 0;					//iReturn value tells status of the function call
            int iIPMSB = 0;
            int iIPLSB = 0;
            int i;
            iPortNumber = 4;   //Convert type of Textbox Value

            if (connected == true)
            {
                return true;
            }
   //         icsNeoDll.icsneoFindAllUSBDevices(int lDriverID, int lGetSerialNumbers,ref int p_lDevices,ref int p_lSerialNumbers,ref int p_lOpenedDevices,ref int lNumDevices);
            for (i = 0; i < 30; i++)
            {
                iPortNumber = i;
                iIPLSB = 57600;
                iReturnVal = icsNeoDll.icsneoOpenPortEx(iPortNumber, Convert.ToInt32(ePORT_TYPE.NEOVI_COMMTYPE_RS232), Convert.ToInt32(eDRIVER_TYPE.INTREPIDCS_DRIVER_STANDARD), iIPMSB, iIPLSB, 1, ref bNetworkIDs, ref m_hObject);
                if (iReturnVal != 0)
                {
                    connected = true;
                    return true;
                }
            }

            ErrorMessage();
            return false;



        }


        public void
        disconnectPort()
        {
             //Stroage for Status of Function Call 
            int iNumOfErrors = 0; 
            
            if(connected == true){
                 icsNeoDll.icsneoClosePort(m_hObject, ref iNumOfErrors);
                 icsNeoDll.icsneoFreeObject(m_hObject);   //Free the memory tha
            }
            return;
        }

    }
}
      

    

	
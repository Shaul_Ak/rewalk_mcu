﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;
using System.ComponentModel;



namespace Console_readfile
{

   public class TextFromFile 
   {
       
       
       communication CanCommunication = new communication();
    /*
       public bool read_file(TextFromFile text1)
       {
       const string FILE_NAME = "Rewalk_Code.txt";
       if (!File.Exists(FILE_NAME)) 
       {
        Console.WriteLine("\nfile {0} does not exist!!\n", FILE_NAME);
        Console.WriteLine("Please copy file \"Rewalk_Code.txt\" to the same \npath of this program and try again.\n");
       
       return false;
       }
       using (StreamReader sr = File.OpenText(FILE_NAME))
       {
        byte[] startloading = { 0x11, 0xf1, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7 };
        String input;
        byte b1, b2;
        Int16 b;
        int ii = 0,l=0,k=0;
        byte[] arr=new byte[8];
        input = sr.ReadLine();
        
        text1.CanCommunication.sendData(startloading);
        Console.WriteLine("Start Loading...\n");
       
        while (input != null && input != ":")
        {
            input = sr.ReadLine();
           // Console.WriteLine(input);
        }
            
        Console.WriteLine("wait........\n");
        while ((input=sr.ReadLine())!=null) 
        {
            
            Console.WriteLine(input);

            ii = 0; l = 0; 
            for (k = 0; k < 8; k++)
            {
              
                if(input[l]>='A')
                    b1 = (byte)((input[l++] - '7'));
                else
                    b1 = (byte)((input[l++] - '0'));
                if (input[l] >= 'A')
                    b2 = (byte)((input[l++] - '7'));
                else
                    b2 = (byte)((input[l++] - '0'));

                 
                b = (Int16)((b1 * 16) + b2);
                arr[ii] = (byte)b; 
               
               // Console.Write(" ,{0}\n",arr[ii]);
                ii++;

            }
            CanCommunication.sendData(arr);

            //wiat foe ACK from DSP
            while (!CanCommunication.GetAckMessage()) ;
          
           

        }
        return true;
       }
     }
       */
       public bool connect_and_askuser(TextFromFile text1)
       {
          
           
           if (text1.CanCommunication.ConnectToReWalk())
           {
               Console.WriteLine("This program will preform testing to MCU board using 'MCU TEST SYSTEM' accordingto document num 25.09!!\n");
             // Console.WriteLine("Are you sure?(y/n)");
              // ch = (char)Console.Read();
              // if (ch == 'y')
                   return true;
              // else
               //    return false;
           }
           else
           {
               Console.WriteLine("Press any key to exit!");
               Console.ReadKey();
               return false;
           }
       }

       public bool MovetoZero(TextFromFile text1, int motor_Id)
       {
           byte[] MotorMoveforward = { 0, 0, 50, 0, 30, 0, 0, 0 };
           text1.CanCommunication.sendData(motor_Id, MotorMoveforward);
           text1.CanCommunication.GetAckMessage(); 
           return false;
       }
       public bool Moveto355(TextFromFile text1, int motor_Id)
       {
           byte[] MotorMoveBackward = { 0, 0, 50, 0x01, 0x50, 0, 0, 0 }; 
           text1.CanCommunication.sendData(motor_Id, MotorMoveBackward);
           text1.CanCommunication.GetAckMessage();
           return false;
       }
       public bool SwitchtoManualMode(TextFromFile text1, int motor_Id)
       {
           byte[] SwitchtoManualMode = { 0x05, 0x03, 0, 0, 0, 0, 0, 0 };
           text1.CanCommunication.sendData(motor_Id, SwitchtoManualMode);
           text1.CanCommunication.GetAckMessage();
           return false;
       }
       public bool SwitchtoWalkMode(TextFromFile text1, int motor_Id)
       {
           byte[] SwitchtoWalkMode = { 0x05, 0x04, 0, 0, 0, 0, 0, 0 };
           text1.CanCommunication.sendData(motor_Id, SwitchtoWalkMode);
           text1.CanCommunication.GetAckMessage();
           return false;
       }
       public bool ReadFSR(TextFromFile text1, int motor_Id)
       {
           byte[] ReadFSR = { 1, 2, 0, 0, 0, 0, 0, 0 };
           text1.CanCommunication.sendData(motor_Id, ReadFSR);
           text1.CanCommunication.GetAckMessage();
           return false;
       }
       public bool ReadMotorCurrent(TextFromFile text1, int motor_Id)
       {
           byte[] ReadMotorCurrent = { 1, 1, 0, 0, 0, 0, 0, 0 };
           text1.CanCommunication.sendData(motor_Id, ReadMotorCurrent);
           text1.CanCommunication.GetAckMessage();
           return false;
       }
       public bool ReadEncoder(TextFromFile text1, int motor_Id)
       {
           byte[] ReadEncoder = { 1, 0, 0, 0, 0, 0, 0, 0 };
           text1.CanCommunication.sendData(motor_Id, ReadEncoder);
           text1.CanCommunication.GetAckMessage();
           return false;
       }
       public bool StopMotor(TextFromFile text1, int motor_Id)
       {
           byte[] StopMotor = { 9, 0, 0, 0, 0, 0, 0, 0 };
           text1.CanCommunication.sendData(motor_Id, StopMotor);
           text1.CanCommunication.GetAckMessage();
           return false;
       }

       public bool ReturnCardID(TextFromFile text1)
       {
           byte[] Preform_Card_Test = { 3, 0, 0, 0, 0, 0, 0, 0 };
           int ii;
           for(ii=0;ii<4;ii++)
           {
           text1.CanCommunication.sendData(ii, Preform_Card_Test);
           text1.CanCommunication.doDelay(500); text1.CanCommunication.doDelay(500); 
           text1.CanCommunication.GetIdMessage();
           if(text1.CanCommunication.Card_Id!=0xff)
             return true;
           }
           return false;
       }
       
       public bool PrintMenue()
        {
         Console.WriteLine("\n***********************  Tests  Menue ***************************/");
         Console.WriteLine("/***  1. Read Absolute Encoder (Rienshaw)");
         Console.WriteLine("/***  2. Read Motor Current");
         Console.WriteLine("/***  3. Read FSR Values");
         Console.WriteLine("/***  4. Switch to Manual Mode");
         Console.WriteLine("/***  5. Switch to Walk Mode");
         Console.WriteLine("/***  6. Move Motor Forward");
         Console.WriteLine("/***  7. Move Motor Backward");
         Console.WriteLine("/***  8. Stop Motor");
         Console.WriteLine("/***  9. Automatic Test (Run all tests in serial fashion)");
         Console.WriteLine("/***  E. Exit");
         Console.WriteLine("/***************************************************************\n");
         return true;
        }
       public static void Main(String[] args)

     {
         bool answer = false;
         int motor_Id=0xff,i;
         char ch='0';

         

         TextFromFile text = new TextFromFile();

         answer = text.connect_and_askuser(text);
        /* 
        Console.Write("Enter Card Id num (0-3): ");
         
         motor_Id= (int)Console.Read();

         if (motor_Id-'0' != 0 && motor_Id-'0' != 1 && motor_Id-'0' != 2 && motor_Id-'0' != 3)
         {
             Console.WriteLine("Illegal Id number .. try again\n");
             Console.WriteLine("\nPress any key to exit!");
             Console.ReadKey();

             return;
         }

        */



         if(text.ReturnCardID(text))
         {
             motor_Id = text.CanCommunication.Card_Id;
             Console.Write("\n       Card Id :{0}\n",0x330|motor_Id);
         }
         else
         {
              Console.WriteLine("Error Reading Card Id ..");
              Console.WriteLine("Check Card , Program last test software ..");
              Console.WriteLine("\nPress any key to exit!");
              Console.ReadKey();
         }

         text.PrintMenue();
//  text1.CanCommunication.Card_Id
         while ( (ch != 'E' || ch != 'e'))
         {
             
             
             
            
             Console.Write("\nEnter Option:  ");


             
           
             ch = (char)Console.Read();

             if ((ch == '\n') || (ch == 13))
             {
                 ch = (char)Console.Read();
             }
             if ((ch == '\n') || (ch == 13))
             {
                 ch = (char)Console.Read();
             }

             switch (ch)
             {
                 case '1':
                     text.ReadEncoder(text, motor_Id);
                     break;
                 case '2':
                     text.ReadMotorCurrent(text, motor_Id);
                     break;
                 case '3':
                     for (i = 0; i < 6; i++)
                     {
                         Console.WriteLine("\nPush FSR   1-3 ->  Try {0}", i+1);
                         text.CanCommunication.doDelay(500);
                         text.ReadFSR(text, motor_Id);
                     }
                     break;
                 case '4':
                     text.SwitchtoManualMode(text, motor_Id);
                     text.CanCommunication.doDelay(500); text.CanCommunication.doDelay(500); text.CanCommunication.doDelay(500);
                     text.CanCommunication.doDelay(500); text.CanCommunication.doDelay(500); text.CanCommunication.doDelay(500);
                     text.CanCommunication.doDelay(500); text.CanCommunication.doDelay(500); text.CanCommunication.doDelay(500);
                     text.SwitchtoWalkMode(text, motor_Id);
                     text.CanCommunication.doDelay(500); text.CanCommunication.doDelay(500); text.CanCommunication.doDelay(500);
                     break;
                 case '5':
                     text.SwitchtoWalkMode(text, motor_Id);
                     break;
                 case '6':
                     text.MovetoZero(text, motor_Id);
                     break;
                 case '7':
                     text.Moveto355(text, motor_Id);
                     break;
                 case '8':
                     text.StopMotor(text, motor_Id);
                     break;
                 case 'e':
                     return;
                   //  break;
                 case 'E':
                     return;
                    // break;

                 case '9':
                     text.ReadEncoder(text, motor_Id); 
                     text.ReadMotorCurrent(text, motor_Id);
                     for (i = 0; i <6 ; i++)
                     {

                         Console.WriteLine("\nPush FSR   1-3 ->  Try {0}",i+1);
                         text.CanCommunication.doDelay(500); text.CanCommunication.doDelay(500); text.CanCommunication.doDelay(500);
                         text.ReadFSR(text, motor_Id);
                     }

                     text.SwitchtoManualMode(text, motor_Id);
                     text.CanCommunication.doDelay(500); text.CanCommunication.doDelay(500); text.CanCommunication.doDelay(500);
                     text.CanCommunication.doDelay(500); text.CanCommunication.doDelay(500); text.CanCommunication.doDelay(500);
                     text.CanCommunication.doDelay(500); text.CanCommunication.doDelay(500); text.CanCommunication.doDelay(500);
                     text.CanCommunication.doDelay(500); text.CanCommunication.doDelay(500); text.CanCommunication.doDelay(500);
                     text.CanCommunication.doDelay(500); text.CanCommunication.doDelay(500); text.CanCommunication.doDelay(500);
                     text.CanCommunication.doDelay(500); text.CanCommunication.doDelay(500); text.CanCommunication.doDelay(500);
                     text.SwitchtoWalkMode(text, motor_Id);
                     text.StopMotor(text, motor_Id);
                     text.CanCommunication.doDelay(500); text.CanCommunication.doDelay(500); text.CanCommunication.doDelay(500);

                     text.Moveto355(text, motor_Id);
                     text.CanCommunication.doDelay(500); text.CanCommunication.doDelay(500); text.CanCommunication.doDelay(500);
                     text.MovetoZero(text, motor_Id);

                     Console.WriteLine("\n    /*******      Tests  Finish Successfully   ****************/");
                     break;
              }//end switch
             if ((ch == '\n') || (ch == 13))
             {
                 ch = '0';
             }
         }//end while       


         


         }

     
    



   }
}

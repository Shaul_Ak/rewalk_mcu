﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
namespace Console_readfile
{
    
    public class communication
    {
        public int m_hObject;
        private icsSpyMessage stMessagesTx;// = new icsSpyMessage();
        public icsSpyMessage[] stMessages;
        private int messageNum = 100;
        private bool connected = false;
        private DateTime firstDate;
        private DateTime newDate;
        private TimeSpan ts;
        string cCommand;
        byte[] message;// = new byte[8];
        public int Card_Id=0xff,loop=0;
        public communication()
        {
          //  stMessagesTx = new icsSpyMessage();
            message = new byte[8];
            stMessages = new icsSpyMessage[messageNum];
        }

        private void ErrorMessage()
        {
           // System.Windows.Forms.MessageBox.Show("some connection problem occured");
            Console.WriteLine("some connection problem occured");
           // Console.ReadKey();
        }

        public  bool ConnectToReWalk()
        {
            byte bNetworkIDs = 1;
            // Array of SCP functional IDs passed to the driver
            int iPortNumber = 0;
            int iReturnVal = 0;					//iReturn value tells status of the function call
            int iIPMSB = 0;
            int iIPLSB = 0;
            int i;
            iPortNumber = 4;   //Convert type of Textbox Value

            if (connected == true)
            {
                return true;
            }
            //         icsNeoDll.icsneoFindAllUSBDevices(int lDriverID, int lGetSerialNumbers,ref int p_lDevices,ref int p_lSerialNumbers,ref int p_lOpenedDevices,ref int lNumDevices);
            for (i = 0; i < 50; i++)
            {
                iPortNumber = i;
                iIPLSB = 57600;
                iReturnVal = icsNeoDll.icsneoOpenPortEx(iPortNumber, Convert.ToInt32(ePORT_TYPE.NEOVI_COMMTYPE_RS232), Convert.ToInt32(eDRIVER_TYPE.INTREPIDCS_DRIVER_STANDARD), iIPMSB, iIPLSB, 1, ref bNetworkIDs, ref m_hObject);
                if (iReturnVal != 0)
                {
                    connected = true;
                    Console.WriteLine("connected...\n");
                    //Console.ReadKey();

                    return true;
                }
            }

            ErrorMessage();
            return false;
        }


        public void disconnectPort()
        {
            //Stroage for Status of Function Call 
            int iNumOfErrors = 0;

            if (connected == true)
            {
                icsNeoDll.icsneoClosePort(m_hObject, ref iNumOfErrors);
                icsNeoDll.icsneoFreeObject(m_hObject);   //Free the memory tha
                Console.WriteLine("\nDisconnect.");
                //Console.ReadKey();
            }
            return;
        }


        public bool sendData(int Id,byte[] message )
        {
            
            stMessagesTx = new icsSpyMessage();
            int iResult;    //Storage for the Result from function call
            
           // Byte paramLow, paramHigh;
           // resetMessageBox();
           // delay_.doDelay(1);
            //Storage for TXMessage
           // paramLow = (Byte)parameter;
           // paramHigh = (Byte)(parameter >> 8);
            //Set the ArbID information for TX message


            stMessagesTx.ArbIDOrHeader = 0x330|Id ;
           



            //            stMessagesTx.
            stMessagesTx.NumberBytesData = 8;
            stMessagesTx.Data1 = message[0];
            stMessagesTx.Data2 = message[1];
            stMessagesTx.Data3 = message[2];
            stMessagesTx.Data4 = message[3];
            stMessagesTx.Data5 = message[4];
            stMessagesTx.Data6 = message[5];
            stMessagesTx.Data7 = message[6];
            stMessagesTx.Data8 = message[7];
            //Clear the Status BitFields
            stMessagesTx.StatusBitField = 0;
            stMessagesTx.StatusBitField2 = 0;
            switch (stMessagesTx.Data1)
            {
                case 0:
                    cCommand = String.Copy("Move");
                break;
                case 9:
                cCommand = String.Copy("Stop Motor");
                break;
                case 1:
                if (stMessagesTx.Data2 == 0)
                    cCommand = String.Copy("Read Encoder");

                else if (stMessagesTx.Data2 == 1)
                    cCommand = String.Copy("Read Current");
                else if (stMessagesTx.Data2 == 2)
                    cCommand = String.Copy("Read FSR");
                break;
                case 5:
                if (stMessagesTx.Data2 == 3)
                    cCommand = String.Copy("SwitchtoManual Mode");
                else if (stMessagesTx.Data2 == 4)
                    cCommand = String.Copy("SwitchtoWalk   Mode");
                break;
                case 3:
                cCommand = String.Copy("Test: Read Card Id");
                break; 

            }
            Console.Write("\nSend->'{0}' Id:{1}  Data: {2} {3} {4} {5} {6} {7} \n", cCommand, stMessagesTx.ArbIDOrHeader, stMessagesTx.Data1, stMessagesTx.Data2, stMessagesTx.Data3, stMessagesTx.Data4,stMessagesTx.Data5, stMessagesTx.Data6,stMessagesTx.Data7);
            //Call the Tx funciton
            iResult = icsNeoDll.icsneoTxMessages(m_hObject, ref stMessagesTx, 1, 0);

            if (iResult != 1)
            {
                ErrorMessage();
                return false;
            }

         
            return true;

        }

        public bool GetAckMessage()
        {
           
            long lResult;   //Storage the Result fo the function call
            long lCount = 0;				//Counter var for loops
            int lNumberOfMessages = 0;   //Storage for the number of messages
            int lNumberOfErrors = 0;
            bool ack = false;
            short encoder_value;

           

            while (ack == false)
            {
                
                lResult = icsNeoDll.icsneoGetMessages(m_hObject, ref stMessages[0], ref lNumberOfMessages, ref lNumberOfErrors);
                if (lResult != 1)
                {
                    ErrorMessage();
                    return false;
                }

                for (lCount = 1; lCount <= lNumberOfMessages; lCount++)
                {

                    if ((stMessages[lCount - 1].ArbIDOrHeader == stMessagesTx.ArbIDOrHeader) && ( (stMessages[lCount - 1].Data1 == 0x14) ||(stMessages[lCount - 1].Data1 == 0x15) ||
                                                                                                   (stMessages[lCount - 1].Data1 == 0x50) ||(stMessages[lCount - 1].Data1 == 0x1C)||(stMessages[lCount - 1].Data1 == 0x1D)))
                    {
                        ack = true;
                        Console.Write("Recv->Id:{0}  Data:  {1} {2} {3} {4} {5} {6} {7} {8}\n",stMessages[lCount - 1].ArbIDOrHeader,stMessages[lCount - 1].Data1,
                           stMessages[lCount - 1].Data2, stMessages[lCount - 1].Data3, stMessages[lCount - 1].Data4, stMessages[lCount - 1].Data5, stMessages[lCount - 1].Data6,
                           stMessages[lCount - 1].Data7,stMessages[lCount - 1].Data8);

                        switch (stMessages[lCount - 1].Data1)
                        {
                            case 0x18:
                                Card_Id = stMessages[lCount - 1].Data4;
                            break;
                            
                            case 0x50:
                                encoder_value = (short)((stMessages[lCount - 1].Data2<<8) | stMessages[lCount - 1].Data3);
                                if ( encoder_value>=0 && encoder_value<=0x1FFF)
                                {
                                    Console.Write("\n     Encoder Test -----------------  OK \n");
                                }
                                else
                                {
                                    Console.Write("\n     Encoder Test -----------------  Failed \n");
                                }
                                break;
                            case 0x1C:
                                if (stMessages[lCount - 1].Data2 == 0x01 && stMessages[lCount - 1].Data3 == 0xFF)
                                {
                                    Console.Write("\n     Current Test -----------------  OK \n");
                                }
                                else
                                {
                                    Console.Write("\n     Current Test -----------------  Failed \n");
                                }
                             break;

                            case 0x14:
                             if (stMessages[lCount - 1].Data2 == 0x0)
                             {
                                 Console.Write("\n    Move Test -----------------  OK \n");
                             }
                             if (stMessages[lCount - 1].Data2 == 0x8)
                             {
                                 Console.Write("\n    Stop Test -----------------  OK \n");
                             }
                             else if (stMessages[lCount - 1].Data2 == 0x5 && stMessages[lCount - 1].Data3 == 0x3)
                             {
                                 Console.Write("\n    Switch to Manual Test -----------------  Press Fow/Back buttons \n");
                             }
                             else if (stMessages[lCount - 1].Data2 == 0x5 && stMessages[lCount - 1].Data3 == 4)
                             {
                                 Console.Write("\n    Switch to Walk Test -----------------  OK \n");
                             }

                             break;
                            case 0x1D:
                             if (stMessages[lCount - 1].Data2 > 0x01 || stMessages[lCount - 1].Data4 > 0x01 || stMessages[lCount - 1].Data6>0x01)
                             {
                                 Console.Write("\n    FSR Test -----------------  OK \n");
                             }
                             else
                             {
                                 Console.Write("\n    FSR Test -----------------  Failed \n");
                             }
                             break;




                        }
                           

                        break;
                    }
                   
                }
               
                
                    //Console.Write("No Motor Id Matched ...Switch motor Id\n");
                   
                
            }


            return true;
        }

        public bool GetIdMessage()
        {

            long lResult;   //Storage the Result fo the function call
            long lCount = 0;				//Counter var for loops
            int lNumberOfMessages = 0;   //Storage for the number of messages
            int lNumberOfErrors = 0;
            bool ack = false;
            loop = 0;
            while (ack == false && loop<10)
            {
                loop++;
                lResult = icsNeoDll.icsneoGetMessages(m_hObject, ref stMessages[0], ref lNumberOfMessages, ref lNumberOfErrors);
                if (lResult != 1)
                {
                    ErrorMessage();
                    return false;
                }

                for (lCount = 1; lCount <= lNumberOfMessages; lCount++)
                {

                    if (stMessages[lCount - 1].Data1 == 0x18)
                    {
                        ack = true;
                        Card_Id = stMessages[lCount - 1].Data4;
                         Console.Write("Recv->Id:{0}  Data:  {1} {2} {3} {4} {5} {6} {7} {8}\n", stMessages[lCount - 1].ArbIDOrHeader, stMessages[lCount - 1].Data1,
                           stMessages[lCount - 1].Data2, stMessages[lCount - 1].Data3, stMessages[lCount - 1].Data4, stMessages[lCount - 1].Data5, stMessages[lCount - 1].Data6,
                           stMessages[lCount - 1].Data7, stMessages[lCount - 1].Data8);
                         return true;
                    }

                }
            }

            return true;
            
        }



        public void doDelay(int delayTime)
        {

            firstDate = DateTime.Now;

            while (true)
            {
                newDate = DateTime.Now;
                ts = newDate - firstDate;

                if (delayTime < ts.Milliseconds)
                {
                    break;
                }

            }
        }
    



    }
}
